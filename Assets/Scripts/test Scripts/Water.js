var lightDir : Transform;
var mat : Material;
function Start()
{
	var renderer = GetComponent(Renderer);
	mat = renderer.material;
}

function Update () 
{
	if(lightDir)
		mat.SetVector("_WorldLightDir", lightDir.forward);
	else
		mat.SetVector("_WorldLightDir", Vector3(0.7,0.7,0.0));
}
