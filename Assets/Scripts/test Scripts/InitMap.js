/*import System.Collections.Generic;

var mainPlayer : MainPlayer;
var propsTemplateValues : GameObject[];
var propsTemplateKeys : Dictionary.<String, int> = new Dictionary.<String, int>();


private var tempPropTransform : Transform;
function Start () {
	
	mainPlayer =  WorldSession.player;
	UpdateProps();
}

private function UpdateProps()
{
	var index : int = 0;
	for(var go : GameObject in propsTemplateValues)
	{
	//	Debug.Log("Make ref to default props "+go.name+"  index: "+index);
		propsTemplateKeys.Add( go.name, index++ );
	}
}	


private var tempGO : GameObject;	
function LoadProp(lodManager : LODManager)
{
	var tempPropTransform : Transform = lodManager.transform;
	//Debug.Log("Load props "+ lodManager.transform.name);
	if(propsTemplateKeys.ContainsKey(tempPropTransform.name))
	{
	//	Debug.Log(propsTemplateKeys[tempPropTransform.name]);
		
		tempGO = resources.InstantiateProps(propsTemplateValues[propsTemplateKeys[tempPropTransform.name]]);
		
		var scriptLODManager : LODManager = (tempGO.GetComponent("LODManager") as LODManager);
		
		scriptLODManager.player_pos = WorldSession.player.transformParent;//send MainPlayer ref
		scriptLODManager.cam =  WorldSession.player.cam;//send  MainCamera ref
		//calculates distance to MainPlayer and init LODManager stage 
		var d : float = Vector3.Distance(scriptLODManager.player_pos.position,scriptLODManager.transform.position) * scriptLODManager.lod_coef;
	
		scriptLODManager.stage = d/scriptLODManager.lod_distance;//calculate stage. its used to activate a version of the same piece
		scriptLODManager.new_stage = scriptLODManager.stage;
			
		for(var i : int = 0; i<scriptLODManager.Models.Length; i++)
		{
			if(i == scriptLODManager.new_stage)
				continue;
				
			scriptLODManager.Models[i].SetActive(false);
		}	
		
		//Debug.Log(" Object enter in range: "+ tempGO.name+"  ");
		lodManager.Models = scriptLODManager.Models;	
		
		Destroy(scriptLODManager);
		

						
		tempGO.transform.parent = tempPropTransform;
		tempGO.transform.localPosition = Vector3.zero;
		tempGO.transform.localRotation = Quaternion.identity;
		

				//worldProps.Add(tempGO);

	}
}
*/
private var tr : Transform;
function UnloadProp(tempPropTransform : Transform)
{
	/*for(var i : int = 0; i<worldProps.Count; ++i)
	{
		tr = worldProps[i].transform;
		if(tr.position == tempPropTransform.position){
			Debug.Log("Object exit from range: "+ tr.name +"  "+ (worldProps.Count-1));
			Destroy(tr.gameObject);
			
			//worldProps.RemoveAt(i);
			//Debug.Log("Mem: "+System.GC.GetTotalMemory(true));
			//worldProps.TrimExcess();
		}
	}*/
}

/*
function OnDestroy()
{
	//TODO
	propsTemplateKeys.Clear();
	propsTemplateKeys.Finalize();
	
	propsTemplateValues = null;
	
	//for(var go : GameObject in worldProps)
	//{
	//	Destroy(go);
	//}
	//worldProps.Clear();
//	worldProps.TrimExcess();
	
	Debug.Log("Mem: "+System.GC.GetTotalMemory(true));
}*/
