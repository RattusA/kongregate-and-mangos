
function OnGUI ()
{
	var animation:Animation = GetComponent(Animation);
	if(GUI.Button(Rect(0,0,50,20),"idle"))
	{
		animation.Blend("idle", 1, 0.3);
		//animation.CrossFadeQueued("idle", 1, QueueMode.PlayNow);
	}
	if(GUI.Button(Rect(50,0,50,20),"run"))
	{
		//animation.Blend("run", 1, 0.3);
		animation.CrossFadeQueued("run", 1, QueueMode.PlayNow);
	}
	if(GUI.Button(Rect(100,0,50,20),"hit"))
	{
		animation.Blend("hit1", 1, 0.3);
		//animation.CrossFadeQueued("hit1", 1, QueueMode.PlayNow);
	}
}