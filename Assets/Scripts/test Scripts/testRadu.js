// When processing a player, don't forget to change the pathToProject in the ProcessPlayer function
// Feel free to remove any Debug.Log in the file if you don't need it.
#if UNITY_EDITOR
//=== nicu - no longer needed ===\\
//import System;
//import System.IO;
//import UnityEditor;

static var self : testRadu;
static var armorTextureFolder:String; 


function Start()
{
 self = GameObject.Find("Main Camera").GetComponent("testRadu");
}

public class CompareByName2 extends System.Collections.Generic.Comparer.<Component> 
{
  public function Compare(x : Component, y : Component) : int
  {
	return String.Compare(x.name, y.name); 
  }
}

function GetNewMaterial(textureName : String, materialNameNew : String, prefix : String) : Material
{
	var mat : Material;
	var materialName : String = "";
	var pathh : String;
	var fileinfo : FileInfo = FileInfo(pathToProject + "Assets/Meshes/Characters/ArmorsSet/" + armorTextureFolder + "/" /*+ modelName +"/"+ modelName+".fbm/"*/+ textureName + prefix +".png");
	if(!fileinfo.Exists)
	{
		return;
	}
	Debug.Log("Create a new material "+ materialName);
	if(materialNameNew == "")
	{
		 materialName = modelName + "_" + textureName + prefix + ".mat";
	}
	else
	 	materialName = materialNameNew + prefix  +".mat";
	
	fileinfo = FileInfo(pathToProject+"Assets/Meshes/Characters/" + modelName + "/Materials/"+ materialName);
	
	if(!fileinfo.Exists)
	{
		var ta : TextAsset = UnityEngine.Resources.Load("Materials/cloneMaterial", typeof(TextAsset)) ;
		if(ta)
		{
			var file = File.Create(pathToProject+"Assets/Meshes/Characters/" + modelName + "/Materials/"+ materialName);
			file.Close();
			var bt : byte[] = ta.bytes;
			Debug.Log("Write file material size:"+bt.Length+"   "+ materialName);
		
			System.IO.File.WriteAllBytes (pathToProject+"Assets/Meshes/Characters/" + modelName + "/Materials/"+ materialName, bt);
		}
	
		AssetDatabase.Refresh();
	}
	else
	{
		Debug.Log("Material found "+materialName);
	}
	
	mat = UnityEngine.Resources.LoadAssetAtPath.<Material>("Assets/Meshes/Characters/" + modelName + "/Materials/"+ materialName) ;

	if(mat)
	{
		var apptexture = UnityEngine.Resources.LoadAssetAtPath.<Texture2D>("Assets/Meshes/Characters/ArmorsSet/" + armorTextureFolder + "/" /*+ modelName +"/"+ modelName+".fbm/"*/+ textureName + prefix +".png");
		if(apptexture)
		{
			mat.mainTexture = apptexture;
		}
		else
		{
			Debug.Log("cant find texture "+"Assets/Meshes/Characters/ArmorsSet/" + armorTextureFolder + "/" /*+ modelName +"/"+ modelName+".fbm/"*/+ textureName + prefix +".png");	
			
		}
		
		Debug.Log("shader frate:  "+ApplyShader);
		
		if(ApplyShader.Length>0)
		{
			if(UnityEngine.Shader.Find(ApplyShader))
			{
				mat.shader = UnityEngine.Shader.Find(ApplyShader);
				apptexture = UnityEngine.Resources.LoadAssetAtPath.<Texture2D>("Assets/Meshes/Characters/ArmorsSet/" + armorTextureFolder + "/" /*+ modelName +"/"+ modelName+".fbm/"*/+ textureName+"_alb_negru.png");
				if(apptexture)
				{
					mat.SetTexture("_Mask", apptexture);
				}
				else
				{
					Debug.Log("cant find texture "+"Assets/Meshes/Characters/ArmorsSet/" + armorTextureFolder + "/" /*+ modelName +"/"+ modelName+".fbm/"*/+ textureName+"_alb_negru.png");	
				}
			}
			else
			{
				Debug.Log("cant find shader "+ ApplyShader);
			}
				
		}
		
	}
	else
	{
		Debug.Log("cant find material  "+"Assets/Meshes/Characters/" + modelName + "/Materials/"+ materialName);
	}
	
	return mat;	
}

var components : System.Collections.Generic.List.<Component> = null;
function processPlayer(mPlayer : GameObject)
{
	var dir : DirectoryInfo;
	components = new System.Collections.Generic.List.<Component>();
	components.AddRange(mPlayer.GetComponentsInChildren( SkinnedMeshRenderer ));//gets all Character model childrens
	components.Sort(new CompareByName2());
	components.Reverse();
	AssetDatabase.Refresh();
	for( var i : int = 0; i < components.Count; i++ )
	{
		var rend = (components[ i ] as SkinnedMeshRenderer);
		var obj : Transform;
		var pieceName : String = ""; //get children name
		var bonesName : String = "";
		for( var j : int = 0; j < rend.bones.Length; j++ ) //gets SkinnedMeshRenderer dependence bones
		{
			bonesName += rend.bones[ j ].name+"^";
		}
		var color_prefix : String = "";
		var attachToSet : String = "";
		var fileinfo : FileInfo;
		for( j = 0; j < posible_color_prefixes.Length; j++ )
		{
			color_prefix = posible_color_prefixes[j];
			if(ApplyNewTexture.Length <= 0)
			{
				ApplyNewTexture = rend.sharedMaterial.mainTexture.name;
			}
			fileinfo = FileInfo(pathToProject+"Assets/Meshes/Characters/ArmorsSet/" + armorTextureFolder + "/" /*+ modelName +"/"+ modelName+".fbm/"*/ + ApplyNewTexture + color_prefix +".png");
			if(!fileinfo.Exists)
			{
				continue;
			}
			obj = Instantiate(rend.transform, rend.transform.position, rend.transform.rotation);
			var renderer:Renderer = obj.GetComponent(Renderer);
			pieceName = obj.gameObject.name; //get children name
			if(!pieceName.Contains(SearchingPiece))//children name dosent relate with the item piece we are looking for we skip it
			{
				DestroyImmediate(obj as GameObject);
				break;
			}
			
			var piecePrefix : String = GetPiecePrefix(pieceName);//gets item prefix 
			//(ex: pieceName = "blooddrak_mail_body_002", function will return "low")
			if(AkaPiece.length > 0)//check if we need to rename the item piece
			{
				obj.gameObject.name = AkaPiece;
			}
			else
			{
				obj.gameObject.name = SearchingPiece;
			}
			if(ApplyNewTexture.Length <= 0)
			{
				ApplyNewTexture = renderer.sharedMaterial.mainTexture.name;
			}
			Debug.Log(SearchingPiece +"   "+AkaPiece+"        "+AttachToSet);
			if(AttachToSet.length > 0)//check if item piece must be added to a Character Set folder
			{
				attachToSet = AttachToSet + color_prefix + "/";
			}
			dir = DirectoryInfo(pathToProject+pathToResources+CharName);//gets character resource folder
			if(!dir.Exists)//if dosent exist we create one
			{
				Directory.CreateDirectory(pathToProject+pathToResources + CharName );
			}
			if(AttachToSet.Length > 0)//if  item piece its added to a specific Character Set we check if that resource folder exists
			{
				dir = DirectoryInfo(pathToProject+pathToResources + CharName + "/" + AttachToSet + color_prefix);//gets Character Set folder
				if(!dir.Exists)//if dosent exist we create one
				{
					Directory.CreateDirectory(pathToProject + pathToResources + CharName + "/" + AttachToSet + color_prefix);
				}
			}
			//check if item prefab already exists;
			var ExistPrefab : GameObject = UnityEngine.Resources.Load("prefabs/chars/" + CharName +"/"+attachToSet+obj.gameObject.name) as GameObject;
			var scriptBones : BoneList;
			var shader2 : Shader;
			var matName :String;
			if(ExistPrefab == null)//if item prefab dosnt exists do the folowing
			{	
				var parentGO : GameObject = new GameObject(AkaPiece);//create a parent to attach the item piece
				
				obj.transform.position = Vector3.zero;
				rend.updateWhenOffscreen = true;
				obj.parent = parentGO.transform;//parent item piece to parentGO
				rend.updateWhenOffscreen = false;			
				obj.localPosition = Vector3.zero;
				obj.name += "_"+piecePrefix;//rename itempiece + adding prefix
				
				if(ApplyShader.Length > 0)
				{
					matName = renderer.sharedMaterial.name;
					//matName = matName.Substring(0, matName.IndexOf(" (Instance)"));
					renderer.material = GetNewMaterial(ApplyNewTexture, matName, color_prefix);
				}
				else if(ApplyNewTexture.Length > 0)
				{
					renderer.material = GetNewMaterial(ApplyNewTexture, "", color_prefix);
				}
				
				var index = int.Parse(piecePrefix);
				scriptBones = parentGO.AddComponent(Type.GetType("BoneList"));//get bones storage
				scriptBones.bonesKey[index-1] = obj.name;	//append new item piece dependence bones
				scriptBones.bonesValue[index-1] = bonesName;				
				
				//create a new prefab 
				var prefab = PrefabUtility.CreateEmptyPrefab(pathToResources + CharName +"/"+attachToSet+ parentGO.name + ".prefab");
				PrefabUtility.ReplacePrefab( parentGO, prefab); //replace prefab
				AssetDatabase.Refresh();
				
				GameObject.DestroyImmediate(parentGO);// destroy parentGO and his childrens. 
				//remove item piece from Character model. We know what item pieces were left to process
				
				//  itemPiece	(parentGO)
				//		itemPiece_prefix1   (has SkinnedMeshRenderer)
			}
			else//if item piece prefab already exists we add a new piece
			{
				//instantiate existing item piece prefab
				var go : GameObject = Instantiate(ExistPrefab);
				
				//atach to parentGO a new piece
				obj.transform.position = Vector3.zero;
				rend.updateWhenOffscreen = true;
				obj.parent = go.transform;
				rend.updateWhenOffscreen = false;	
				
				obj.name += "_"+piecePrefix;
				obj.localPosition = Vector3.zero;
	
				if(ApplyShader.Length > 0)
				{
					matName = renderer.sharedMaterial.name;
					//matName = matName.Substring(0, matName.IndexOf(" (Instance)"));
					renderer.material = GetNewMaterial(ApplyNewTexture, matName, color_prefix);
				}
				else if(ApplyNewTexture.Length > 0)
				{
					renderer.material = GetNewMaterial(ApplyNewTexture, "", color_prefix);
				}
				
				scriptBones = go.GetComponent("BoneList");
				scriptBones.bonesKey[int.Parse(piecePrefix)-1] = obj.name;	//append new item piece dependence bones
				scriptBones.bonesValue[int.Parse(piecePrefix)-1] = bonesName;	
				
				//replace the old prefab
				var prefab2 = PrefabUtility.CreateEmptyPrefab(pathToResources + CharName +"/"+attachToSet+ ExistPrefab.name + ".prefab");
				PrefabUtility.ReplacePrefab( go, prefab2); 
				AssetDatabase.Refresh();	
				GameObject.DestroyImmediate(go);// destroy parentGO and his childrens. 
				//remove item piece from Character model. We know what item pieces were left to process
						
				//  itemPiece	(parentGO)
				//		itemPiece+prefix1   (has SkinnedMeshRenderer)	
				//		itemPiece+prefix2   (has SkinnedMeshRenderer)			
				//		itemPiece+prefix3   (has SkinnedMeshRenderer)	
						
				//prefix = "low" | "mid" | "high"
			}
			DestroyImmediate(obj as GameObject);// destroy parentGO and his childrens. 
		}
	}	
}

var low_posible_prefixes : String[] = ["LOW", "2", "02", "002"];
var mid_posible_prefixes : String[] = ["MID", "MED", "1", "01", "001"];
var high_posible_prefixes : String[] = ["HIGH", "HIGHT", ""];
var posible_color_prefixes : String[] = ["", "_blue", "_gray", "_green", "_purple", "_red", "_tangerine", "_turquoise"];
//gets item piece prefix
function GetPiecePrefix(pieceName : String) : String
{
		var _pieceName : String = pieceName.ToUpper();
		pieceName = _pieceName.Remove(0, _pieceName.Length-5);
		
		for(var str : String in low_posible_prefixes)
		{
			if(_pieceName.Contains(str))
				return "03";
		}
			
		for(var str : String in mid_posible_prefixes)
		{
			if(_pieceName.Contains(str))
				return "02";
		}
		
		for(var str : String in high_posible_prefixes)
		{
			if(_pieceName.Contains(str))
				return "01";
		}
		
		return "unk" ;
}

function FindChildByName( ThisName : String, ThisGObj : Transform ) : Transform
{
    var ReturnObj : Transform;
    if( ThisGObj.name == ThisName )
    	return ThisGObj.transform;

    for (var child : Transform in ThisGObj )
    {
        ReturnObj = FindChildByName( ThisName, child );
        if( ReturnObj )
            return ReturnObj;
    }
    return null;
}



static var pathToProject : String = "" ;
static var pathToResources : String = "Assets/Resources/prefabs/chars/";
static var CharName : String = "Dwarf_Male";
static var ApplyShader : String = "DifusePaintBASE";
static var ApplyNewTexture : String = "";

static var SearchingPiece : String = "Piece";
static var AkaPiece : String = "Rename piece";
static var AttachToSet : String = "Set";
static var modelName : String = "Dwarf";

function OnGUI()
{
	
/*	var mLineHeight : int = Screen.height * 0.08;
	var currentY : int = 0;

	GUI.Label( Rect( 0, currentY, Screen.width * 0.22, Screen.height * 0.1 ), "PathToProject" );
	pathToProject = GUI.TextField( Rect(Screen.width * 0.2, currentY, Screen.width * 0.5, mLineHeight ), pathToProject );

	currentY += mLineHeight * 1.2;
	GUI.Label( Rect( 0, currentY, Screen.width * 0.22, Screen.height * 0.1 ), "PathToResources" );
	pathToResources = GUI.TextField( Rect(Screen.width * 0.2, currentY, Screen.width * 0.5, mLineHeight ), pathToResources );

	currentY += mLineHeight * 1.2;	
	GUI.Label( Rect( 0, currentY, Screen.width * 0.22, Screen.height * 0.1 ), "Character Main Folder" );
	CharName = GUI.TextField( Rect(Screen.width * 0.2, currentY, Screen.width * 0.5, mLineHeight ), CharName );

	currentY += mLineHeight * 1.2;	
	GUI.Label( Rect( 0, currentY, Screen.width * 0.22, Screen.height * 0.1 ), "Apply Shader" );
	ApplyShader = GUI.TextField( Rect(Screen.width * 0.2, currentY, Screen.width * 0.5, mLineHeight ), ApplyShader );
	currentY += mLineHeight * 1.2;	
	GUI.Label( Rect( 0, currentY, Screen.width * 0.22, Screen.height * 0.1 ), "ApplyNewTextures" );
	ApplyNewTexture = GUI.TextField( Rect(Screen.width * 0.2, currentY, Screen.width * 0.8, mLineHeight ), ApplyNewTexture );
	
	currentY += mLineHeight * 1.2;
	GUI.Label( Rect(  0, currentY, Screen.width * 0.15, Screen.height * 0.1 ), "SearchingPiece" );
	SearchingPiece = GUI.TextField( Rect( Screen.width * 0.2, currentY, Screen.width * 0.35, mLineHeight ), SearchingPiece );
	
	currentY += mLineHeight * 1.2;
	GUI.Label( Rect(  0, currentY, Screen.width * 0.15, Screen.height * 0.1 ), "AkaPiece" );
	AkaPiece = GUI.TextField( Rect( Screen.width * 0.2, currentY, Screen.width * 0.35, mLineHeight ), AkaPiece );
	
	currentY += mLineHeight * 1.2;
	GUI.Label( Rect(  0, currentY, Screen.width * 0.15, Screen.height * 0.1 ), "AttachToSet" );
	AttachToSet = GUI.TextField( Rect( Screen.width * 0.2, currentY, Screen.width * 0.35, mLineHeight ), AttachToSet );
	
	currentY += mLineHeight * 1.2;
	GUI.Label( Rect(  0, currentY, Screen.width * 0.15, Screen.height * 0.1 ), "Model Name" );
	modelName = GUI.TextField( Rect( Screen.width * 0.2, currentY, Screen.width * 0.35, mLineHeight ), modelName );				
	
	currentY += mLineHeight * 1.2;
	if ( GUI.Button( Rect( 0, currentY, Screen.width * 0.1, mLineHeight ), "Process" ) )
	{
		//find model name, and processPlayer, debug log error if model cannot be found;
		var modelToProcess : GameObject = GameObject.Find( modelName );
		if( modelToProcess == null )
		{
			Debug.Log( "Model To process could not be found" );
			return;
		}
		try{
		processPlayer( modelToProcess );
		}catch(e: Exception){Debug.Log(e);}
	}	*/
}
#endif
