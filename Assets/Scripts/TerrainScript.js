var obj : Vector3;
var myRender : Material[];
var leng : int;
function Start()
{
	obj   = GameObject.Find("player").transform.position;
	var renderer:Renderer = GetComponent(Renderer);
	myRender= renderer.materials;
	leng = renderer.materials.Length;
}

function Update ()
{
	var i : byte = 0;
	for(i = 0;i<leng;i++)
	{
		myRender[i].SetVector("_PlayerPosition", obj);
	}
}