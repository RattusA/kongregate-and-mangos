class button
{
	static var debug : boolean = false;
	var name : String = "";
var pressed : boolean = false;
var released : boolean = false;
var rect : Rect;
var tex : Texture2D;
var pressedTex : Texture2D;
var inactiveTex : Texture2D;
var angle : float;
var selectable : boolean = true;
//static var timeToRelease : float = 0.5;
var timeLeft : float;
static var backGroundTexture : Texture2D;
var backGroundRect : Rect;
//static var deltaTime : float
static var audioSource : AudioSource;
static var cameraPosition : Vector3;
var showBackGround : boolean = true;
var timeToPress : float = 0.0;
var leftTimeToPress : float = timeToPress;
var touchBeghinSenzitive : boolean = true;
var lastRect : Rect;
var touched : boolean = false;

var xLeft : float;
var yLeft : float;
var yRight : float;
var xRight : float;

static function Rotate(angle : float, targget : Vector2, pivot : Vector2) : Vector2
{
	angle=angle*Mathf.PI/180;
	var ret : Vector2;
		targget.x-=pivot.x;
		targget.y-=pivot.y;
		ret.x=Mathf.Cos(angle)*targget.x - Mathf.Sin(angle)*targget.y;
		ret.y=Mathf.Cos(angle)*targget.y + Mathf.Sin(angle)*targget.x;
		ret.x+=pivot.x;
		ret.y+=pivot.y;
	return ret;
}
function Start()
{
	//angle=0;
	showBackGround=true;
	leftTimeToPress = timeToPress;
	timeLeft=0;
	/*if(!backGroundTexture)
	{
		backGroundTexture=new Texture2D(1,1);
		backGroundTexture.SetPixel(0,0,Color(0,0,0,0.45));
		backGroundTexture.Apply();
	}*/
	//audioSource=GameObject.Find("Main Camera").audio;//transform.position;
}



function Draw(text : String, style : GUIStyle) : int
{
	var ret : int = Draw();
	if(pressed || released)
	{
		var newStyle : GUIStyle = new GUIStyle();
		newStyle.font=style.font;
		newStyle.alignment = style.alignment;
		newStyle.normal.textColor=style.active.textColor;
		GUI.Label(rect,text,newStyle);
	}
	else
		GUI.Label(rect,text,style);
	lastRect=rect;
	return ret;
}
var animationSpeed : float = 250;
//var i : int;
var beginTouch : boolean = false;
function Draw() : int
{
	var i : int;
	//print("qq"+i++);
	//pressed=false;
	GUIUtility.RotateAroundPivot(angle,Vector2(rect.x,rect.y));
	if(showBackGround)
	{
		if(pressed || released)
		{//*/
			/*if(backGroundRect.x>-50)
			backGroundRect.x-=animationSpeed*Time.deltaTime;
		if(backGroundRect.x+backGroundRect.width<530)
			backGroundRect.width+=2*animationSpeed*Time.deltaTime;*/
		/*backGroundRect=rect;
		backGroundRect.x-=10;
		backGroundRect.y-=6;
		backGroundRect.height+=12;
		backGroundRect.width+=20;//*/	
			backGroundRect.x=-50;
			backGroundRect.width=530;
		
		}
		else 
		{
			backGroundRect=rect;
			backGroundRect.x-=10;
			backGroundRect.y-=6;
			backGroundRect.height+=12;
			backGroundRect.width+=20;
		}
	}
	else 
	{
		backGroundRect=rect;
		backGroundRect.x-=10;
		backGroundRect.y-=6;
		backGroundRect.height+=12;
		backGroundRect.width+=20;
	}//*/
	if(released)
		leftTimeToPress = timeToPress;
	/*if(showBackGround)
		GUI.DrawTexture(backGroundRect,backGroundTexture);*/
	//not selectable
	if(!selectable)
	{
		if(inactiveTex)
			GUI.DrawTexture(rect,inactiveTex);
		//audioSource.PlayOneShot(
	/*	var posit : Vector2 = Vector2(iPhoneInput.touches[0].position.x*2,Screen.height)-iPhoneInput.touches[0].position;
		posit=Rotate(-angle,pos,Vector2(rect.x,rect.y));
		if(backGroundRect.Contains(pos))
			if(iPhoneInput.touches[0].phase==iPhoneTouchPhase.Ended || iPhoneInput.touches[0].phase==iPhoneTouchPhase.Canceled)
			audioSource.PlayOneShot(Resources.Load("audio/MJ -Bump 1e M 24-44"));
		*/
		var st1 : GUIStyle = GUI.skin.textField;
		st1.normal.background=null;	
		st1.hover.background=null;
		if(GUI.Button(rect,"",st1))
			if(audioSource)
				audioSource.PlayOneShot(Resources.Load("audio/MJ -Bump 1e M 24-44"));
		GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
		return;
	}
	if(!pressed && !released)
	{
		if(tex)
			GUI.DrawTexture(rect,tex);
	}
	else
	{
		//print("pressed");
		if(pressedTex)
			GUI.DrawTexture(rect,pressedTex);
	}
	xLeft =rect.x;
	yLeft=yRight=rect.y+rect.height*0.5;
	xRight=xLeft+rect.width;
	if(released)
	{
		released=false;
		
	}
	if(Input.touchCount)
	{
	for(i = 0;i<Input.touchCount;i++)
	{
		var pos : Vector2 = Vector2(Input.touches[i].position.x*2,Screen.height)-Input.touches[i].position;
		pos=Rotate(-angle,pos,Vector2(rect.x,rect.y));
		if(debug)
		{
			GUI.Label(Rect(0,(i+1)*50,400,50),""+pos);
		}
		if(backGroundRect.Contains(pos))
		{//*/
			/*if(iPhoneInput.touches[0].phase!=iPhoneTouchPhase.Began)
				beginTouch = true;*/
			if(debug)
			{
				GUI.Label(Rect(0,0,400,50),name + "touch number "+i);
			}		
			if(Input.touches[i].phase!=TouchPhase.Ended && Input.touches[i].phase!=TouchPhase.Canceled)
			{
				if(lastRect==rect)
				{//*/
					if(leftTimeToPress<=0)
					{
						pressed=true;
						touched=true;
						GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
						lastRect=rect;
						return 1;
					}
					else
					{
						if(leftTimeToPress<timeToPress-0.2)
							touched=true;
						leftTimeToPress-=Time.deltaTime;
						pressed=false;
						lastRect=rect;
						GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
						return 0;		
					}
				}
				else
				{
					leftTimeToPress = timeToPress;
					lastRect=rect;
					touched=false;
					GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
					return 0;
				}
			}
			else
			{
				//if(iPhoneInput.touches[0].phase!=iPhoneTouchPhase.Began)
				if(pressed || touched)
				{
					pressed=false;
					touched=false;
					//timeLeft=timeToRelease;
					leftTimeToPress=timeToPress;
					released=true;
					if(audioSource)
						audioSource.Play();
					GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
					beginTouch= false;
					lastRect=rect;
					return 2;
				}
				else 
				{
					GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
					lastRect=rect;
					return 0;
				}//*/
			}
		}
		else if(pressed)
		{
			pressed=false;
			//timeLeft=timeToRelease;
			touched=false;
			released=true;
		}
		GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
		lastRect=rect;
		//return 0;
	}
		return 0;
	}
	else if(pressed)
		{
			pressed=false;
			touched=false;
			//timeLeft=timeToRelease;
			released=true;
			if(audioSource)
				audioSource.Play();
			GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
			beginTouch=false;
			lastRect=rect;
			return 2;
		}
	var st : GUIStyle = GUI.skin.textField;
	st.normal.background=null;
	st.hover.background=null;
	if(GUI.RepeatButton(rect,"",st))
	{
		if(audioSource)
			audioSource.Play();
		released=true;
		GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
		lastRect=rect;
		if(GUI.Button(rect,"",st))
		{
			if(audioSource)
				audioSource.Play();
			released=true;
			GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
			lastRect=rect;
			return 2;
		}
		return 1;
	}
	GUIUtility.RotateAroundPivot(-angle,Vector2(rect.x,rect.y));
	lastRect=rect;
	leftTimeToPress = timeToPress;
	touched=false;
	return 0;
}

function DrawInUpdate() : int
{
	var i : int;
	if(released)
		leftTimeToPress = timeToPress;
	//not selectable
	if(!selectable)
	{
		return;
	}
	xLeft=rect.x;
	yLeft=yRight=rect.y+rect.height*0.5;
	xRight=xLeft+rect.width;
	if(released)
	{
		released=false;
	}
	if(Input.touchCount)
	{
	for(i = 0;i<Input.touchCount;i++)
	{
		var pos : Vector2 = Vector2(Input.touches[i].position.x*2,Screen.height)-Input.touches[i].position;
		pos=Rotate(-angle,pos,Vector2(rect.x,rect.y));
		if(debug)
		{
			GUI.Label(Rect(0,(i+1)*50,400,50),""+pos);
		}//*/
		if(rect.Contains(pos))
		{//*/
			/*if(iPhoneInput.touches[0].phase!=iPhoneTouchPhase.Began)
				beginTouch = true;*/		
			if(Input.touches[i].phase!=TouchPhase.Ended && Input.touches[i].phase!=TouchPhase.Canceled)
			{
				if(lastRect==rect)
				{//*/
					if(leftTimeToPress<=0)
					{
						pressed=true;
						touched=true;
						lastRect=rect;
						return 1;
					}
					else
					{
						if(leftTimeToPress<timeToPress-0.2)
							touched=true;
						leftTimeToPress-=Time.deltaTime;
						pressed=false;
						lastRect=rect;
						return 0;		
					}
				}
				else
				{
					leftTimeToPress = timeToPress;
					lastRect=rect;
					touched=false;
					return 0;
				}
			}
			else
			{
				//if(iPhoneInput.touches[0].phase!=iPhoneTouchPhase.Began)
				if(pressed || touched)
				{
					pressed=false;
					touched=false;
					//timeLeft=timeToRelease;
					leftTimeToPress=timeToPress;
					released=true;
					if(audioSource)
						audioSource.Play();
					beginTouch= false;
					lastRect=rect;
					return 2;
				}
				else 
				{
					lastRect=rect;
					return 0;
				}//*/
			}
		}
		else if(pressed)
		{
			pressed=false;
			//timeLeft=timeToRelease;
			touched=false;
			released=true;
		}
		lastRect=rect;
		//return 0;
	}
		return 0;
	}
	else if(pressed)
		{
			pressed=false;
			touched=false;
			//timeLeft=timeToRelease;
			released=true;
			if(audioSource)
				audioSource.Play();
			beginTouch=false;
			lastRect=rect;
			return 2;
		}
	if(Application.platform!=RuntimePlatform.IPhonePlayer && Application.platform != RuntimePlatform.Android)
	{
		var pos2d = Vector2(Input.mousePosition.x,Input.mousePosition.y);
		pos = Vector2(Input.mousePosition.x*2,Screen.height)-pos2d;
		//MonoBehaviour.print(pos);
		if(rect.Contains(pos))
		{
			lastRect=rect;
			return 1;
		}
	}
	lastRect=rect;
	leftTimeToPress = timeToPress;
	touched=false;
	return 0;
}
}

