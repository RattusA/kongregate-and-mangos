﻿#pragma strict
var skyBoxMaterial:Material;

function Start () 
{
	if(skyBoxMaterial && Camera.main)
	{
		RenderSettings.skybox = skyBoxMaterial;
		Camera.main.clearFlags = CameraClearFlags.Skybox;
	}
}

function Update () 
{

}