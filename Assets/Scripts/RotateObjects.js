@System.NonSerialized
var myRotation : Transform;

var RotationVector : Vector3 = Vector3(0,10,0);

function Start()
{
	myRotation = transform;
}

@System.NonSerialized
var contor : float = 0;

function FixedUpdate () 
{
	myRotation.Rotate(RotationVector * Time.deltaTime, Space.World);
}