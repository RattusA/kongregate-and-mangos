var lod_distance : float = 0;
var lod_coef : float;

public var Models : GameObject[];

var cam : Camera;
var stage : int = -1;
var new_stage : int;

var isAttachToServerObjects : boolean = false;
var player_pos : Transform;
private var _counter : int = 0;

var components : System.Collections.Generic.List.<Behaviour> = new System.Collections.Generic.List.<Behaviour>();
var i : int;
var d : float = 0;
var far : float = 0;
var _result : CompareByName = new CompareByName();

private var _isFromTundraProp : boolean = false;
public var IsGameObjectWithMeshRederer : boolean = false;

public class CompareByName extends System.Collections.Generic.Comparer.<Behaviour> 
{
	 public function Compare(x : Behaviour, y : Behaviour) : int
 	 {
		return String.Compare(x.name, y.name); 
 	 }
}

public function SetModels(_Models : GameObject[])
{
	Models = _Models;
}


function Start () {
	player_pos = WorldSession.player.transform.parent;
	cam = Camera.main;
	//fix for tundra props
	if(transform.root.Find("props_tundra"))
	{
		_isFromTundraProp = true;
	}
	//end fix
	
	if(!isAttachToServerObjects){ 	
 	isAttachToServerObjects = (transform.root.name == "world")? false : true;
    }
	for(i = 0; i < Models.Length; i ++){
		if(Models[i] == null){
			continue;
		}
		//add components in the List
		var behaviour:Behaviour[];
        if(isAttachToServerObjects)
        {
        	behaviour = Models[i].GetComponentsInChildren(SkinnedMeshRenderer, true) as Behaviour[];
			if(behaviour != null && behaviour.Length > 0)
			{
				components.AddRange(behaviour);
				components.Sort(_result);	
			}
			Models[i].SetActive(false);
			Models[i].active = true;        
		}
		else{
			if(_isFromTundraProp)
			{
				behaviour = Models[i].GetComponentsInChildren(MeshRenderer, true) as Behaviour[];
				if(behaviour != null && behaviour.Length > 0)
				{
					components.AddRange(behaviour);
				}
				Models[i] = Models[i].GetComponentInChildren(MeshRenderer).transform.parent.gameObject;
				Models[i].SetActive(false);
				Models[i].active = true;
			}
			else{
				Models[i].active = false;
			}
		}
	}
	//for lod coefficient
	if(WorldSession.isInstance(WorldSession.player.mapId)){
		lod_distance = (isAttachToServerObjects)? Defines.instanceMapLODDistance : (lod_distance != 0)? lod_distance : 100.0f;
    }
    else{
 	lod_distance = (isAttachToServerObjects)? Defines.normalMapLODDistance : (lod_distance != 0)? lod_distance : 100.0f;	
    }
    lod_coef = Defines.lod_coef;
}


function UpdateLodMesh () 
{
	if(!player_pos)
	{
		return;
	}
	d = Vector3.Distance(player_pos.position, transform.position) * (lod_coef+1);
	far = cam.layerCullDistances[gameObject.layer];
	if(far == 0)
		far = cam.farClipPlane;
	
	new_stage = d/lod_distance;
	//Debug.Log("new stage: " + new_stage + " distance: " + d);
	if(new_stage != stage){
		//for props only
        if(!isAttachToServerObjects)
        {
			if(_isFromTundraProp){
				//Debug.Log("Tundra map - " + transform.root.name + " - " + Time.time);
			for(var k : int = 0 ; k < Models.Length ; k++)
			{
				if(stage >= 0 && stage < 3)//most of the time components.Count == 3 
					components[stage + k*3].enabled = false;
				if(new_stage < 3){
				 	components[new_stage + k*3].enabled = true;
				}
			}
		}
			else{
				if(stage >= 0 && stage < Models.Length){
					Models[stage].active = false;
				}
				if(new_stage < Models.Length){
					Models[new_stage].active = true;
				}
			}
        }
        else
        {
			try{
				for(i = 0; i<Models.Length; i++)
				{
					if(Models[i] == null)
						continue;
					components = new System.Collections.Generic.List.<Behaviour>();
					var behaviour:Behaviour[];
					if(IsGameObjectWithMeshRederer)
					{
						behaviour = Models[i].GetComponentsInChildren(MeshRenderer, true) as Behaviour[];
					}
					else
					{
						behaviour = Models[i].GetComponentsInChildren(SkinnedMeshRenderer, true) as Behaviour[];
					}
					if(behaviour != null && behaviour.Length > 0)
					{
						components.AddRange(behaviour);
					}
					
					if(components.Count > 0)
					{
						components.Sort(_result);
					
						if(stage >= 0 && stage < components.Count)//most of the time components.Count == 3
						{
							components[stage].enabled = false;
						}
					 
						if(new_stage < components.Count)
						{
						 	components[new_stage].enabled = true;
						}
						else
						{
							components[components.Count - 1].enabled = true;
						}
					}
				}
			}catch(exception : Exception){Debug.Log("LOD exception:  " + exception);}
		}
		stage = new_stage;
	}
	
}


function UpdateWhenOffscreen(_mesh : SkinnedMeshRenderer) : IEnumerator{
	Debug.Log("enabling.... " + _mesh.name + " @time: " + Time.time);
	_mesh.updateWhenOffscreen = true;
	yield WaitForSeconds(10);
	Debug.Log("disabling.... " + _mesh.name + " @time: " + Time.time);
	_mesh.updateWhenOffscreen = false;
		
	/*
	for(var go : SkinnedMeshRenderer in transformParent.GetComponentsInChildren(SkinnedMeshRenderer)){
		Debug.Log("name of the skinned mesh renderer: " + go.name + " time of enable/disable: " + Time.time);
		go.updateWhenOffscreen = true;
	}		
	
	yield WaitForSeconds(1);
	
	for(var go : SkinnedMeshRenderer in transformParent.GetComponentsInChildren(SkinnedMeshRenderer)){
		Debug.Log("name of the skinned mesh renderer: " + go.name + " time of enable/disable: " + Time.time);
		go.updateWhenOffscreen = false;
	}
	*/
}
	
function FixedUpdate()
{
    if(!isAttachToServerObjects)
    {
 		return;
    }
    UpdateLodMesh() ; 		
}