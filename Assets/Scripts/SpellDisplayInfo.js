#if UNITY_EDITOR
import System;
import System.IO;

static function DisplayInfoOnly()
{
	var fileName : String = "Spell_display_info_only.csv";
	if (File.Exists(fileName)) 
	{
		Debug.Log(fileName+" already exists.");
		return;
	}
	var source = File.CreateText(fileName);
	var spellDisplayInf : SpellIconEntry;
	
	Debug.Log("Begin read SpellIcons DBC");
	source.WriteLine ("display ID, altas, icon name");
	for(var i : uint = 1; i < 10000; i++)
	{
		spellDisplayInf = new SpellIconEntry();
		spellDisplayInf = dbs.sSpellIcons.GetRecord(i);
		if(spellDisplayInf.ID == 0)
		{
			continue;
		}
		else
		{
			source.WriteLine ("{0}, {1}, {2}", spellDisplayInf.ID, spellDisplayInf.BigTexture, spellDisplayInf.Icon);
		}
	}
	source.Close();
	Debug.Log("End read SpellIcons DBC");
}

static function GetAllTextsForTranslate()
{
	var fileName : String = "Spell_DBC_text.csv";
	if (File.Exists(fileName)) 
	{
		Debug.Log(fileName+" already exists.");
		return;
	}
	var source = File.CreateText(fileName);
	var spellEntry : SpellEntry;
	
	Debug.Log("Begin read Spell DBC");
	source.WriteLine ("Spell ID, English name, Japanese name, English Rank, Japanese Rank, English Description, Japanese Description");
	for(var i : uint = 1; i < 100000; i++)
	{
		spellEntry = new SpellEntry();
		spellEntry = dbs.sSpells.GetRecord(i);
		if(spellEntry.ID != 0)
		{
			source.WriteLine ("{0}, {1}, {2}, {3}, {4}, {5}, {6}", spellEntry.ID, spellEntry.SpellName[0], "", spellEntry.Rank[0], "", spellEntry.Description[1].Replace(",", "^"), "");
		}
	}
	source.Close();
	Debug.Log("End read Spell DBC");
}
#endif
