#if UNITY_EDITOR
import System;
import System.IO;

static function GetAllTextsForTranslate()
{
	var fileName : String = "Enchanment DBC text.csv";
	if (File.Exists(fileName)) 
	{
		Debug.Log(fileName+" already exists.");
		return;
	}
	var source = File.CreateText(fileName);
	var enchantmentEntry : SpellItemEnchantmentEntry;
	
	Debug.Log("Begin read Enchanment DBC");
	source.WriteLine ("ID, English Description, Japanese Description");
	for(var i : uint = 1; i < 100000; i++)
	{
		enchantmentEntry = new SpellItemEnchantmentEntry();
		enchantmentEntry = dbs.sSpellItemEnchantment.GetRecord(i);
		if(enchantmentEntry.ID != 0)
		{
			source.WriteLine ("{0}, {1}, {2}", enchantmentEntry.ID, ReplaceStats(enchantmentEntry.description[0]).Replace(",", "^"), "");
		}
	}
	source.Close();
	Debug.Log("End read Enchanment DBC");
}

static function ReplaceStats(str:String):String
{
	var ret:String = str;
	ret = ret.Replace("Strength", "Might");
	ret = ret.Replace("Spell Power", "Spell Strength");
	ret = ret.Replace("Health Regen", "Health Regen");
	ret = ret.Replace("Mana Regen", "Mana Regen");
	ret = ret.Replace("Haste Spell Rating", "Spell Rush Score");
	ret = ret.Replace("Hit Melee Rating", "Melee Strike Score");
	ret = ret.Replace("Critical Melee Rating", "Melee Crit Score");
	ret = ret.Replace("Defense Rating", "Aegis Score");
	ret = ret.Replace("Dodge Rating", "Divert Score");
	ret = ret.Replace("Parry Rating", "Deflection Score");
	ret = ret.Replace("Block Rating", "Block Score");
	ret = ret.Replace("Hit Rating", "Strike Score");
	ret = ret.Replace("Critical Rating", "Crit Score");
	ret = ret.Replace("Haste Rating", "Rush Score");
	ret = ret.Replace("Expertise Rating", "Mastery Score");
	ret = ret.Replace("Ranger Hit Rating", "Ranged Strike Score");
	ret = ret.Replace("Spell Hit Rating", "Spell Strike Score");
	ret = ret.Replace("Critical Ranger Rating", "Ranged Crit Score");
	ret = ret.Replace("Critical Spell Rating", "Spell Crit Score");
	ret = ret.Replace("Hit Taken Melee Rating", "Strike Taken Melee Score");
	ret = ret.Replace("Hit Taken Ranger Rating", "Strike Taken Ranged Score");
	ret = ret.Replace("Hit Taken Spell Rating", "Strike Taken Spell Score");
	ret = ret.Replace("Critical Taken Melee Rating", "Crit Taken Melee Score");
	ret = ret.Replace("Critical Taken Ranger Rating", "Crit Taken Ranged Score");
	ret = ret.Replace("Critical Taken Spell Rating", "Crit Taken Spell Score");
	ret = ret.Replace("Haste Melee Rating", "Melee Rush Score");
	ret = ret.Replace("Ranger Haste Rating", "Ranged Rush Score");
	ret = ret.Replace("Hit Taken Rating", "Strike Taken Score");
	ret = ret.Replace("Critical Taken Rating", "Crit Taken Score");
	ret = ret.Replace("Resilience Rating", "Resistance Score");
	ret = ret.Replace("Resistance", "Resistance");
	ret = ret.Replace("Ranged Attack Power", "Ranged Attack Strength");
	ret = ret.Replace("Feral  Attack Power", "Wild Attack Strength");
	ret = ret.Replace("Spell Healing Done", "Spell Alleviate Performed");
	ret = ret.Replace("Spell Damage Done", "Spell Damage Performed");
	ret = ret.Replace("Armor Penetration Rating", "Armor Perforation Score");
	ret = ret.Replace("Spell Penetration", "Spell infiltration Score");
	ret = ret.Replace("Block Value", "Block Amount");
	ret = ret.Replace("Attack Power", "Attack Strength");
	ret = ret.Replace("Health", "HP");
	ret = ret.Replace("Agility", "Dex");
	ret = ret.Replace("Stamina", "Vit");
	ret = ret.Replace("Intellect", "Wis");
	ret = ret.Replace("Spirit", "Will");
	return ret;
}
#endif
