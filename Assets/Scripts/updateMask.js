
class UpdateMask
{
	private var mCount : System.UInt32 = 0;
	private var mBlocks : System.UInt32 = 0;
	private var mUpdateMask : System.UInt32[] = null;// la ei era un array de uint32
	function GetBit(index : System.UInt32) : byte
	{
		var shift1 : byte = 3;
    	var shift2 : byte = 1;
    	var si : byte = 0x7;
    	//MonoBehaviour.print("citire bit de pe pozitia " + index + "  " +(index>>shift1) +">="+mCount);
		if((index>>3) >= mCount)
			return 0;
			
		var bit : byte = ((mUpdateMask[index>>3] & (shift2<<(index & si)) ));
		
		//MonoBehaviour.print(bit);
		return bit;//((mUpdateMask[index>>shift1]& (shift2<<index & si) ));
	}
	function UnsetBit (index : System.UInt32)
    {
       // mUpdateMask[ index >> 3 ] &= (0xff ^ (1 <<  ( index & 0x7 ) ) );
    }
    function SetBit (index : System.UInt32)
    {
    	var shift1 : byte = 3;
    	var shift2 : byte = 1;
    	var si : byte = 0x7;
        mUpdateMask[ index >> shift1 ] |= shift2 << ( index & si );
    }
    function GetBlockCount() : System.UInt32
    {
    	return mBlocks;
    }
    function GetLength() : System.UInt32
    {
    	var shift : byte = 2;
    	return mBlocks << shift;
    }
    function GetCount() : System.UInt32
    {
    	return mCount;
    }
    function GetMask() : System.UInt32[]
    {
    	return mUpdateMask;
    }
	function SetMask(updateMask : System.UInt32[]) 
	{ 
		mUpdateMask = updateMask; 
	}
	function SetCount (valuesCount : System.UInt32)
    {
    	    	
        mCount = valuesCount;
        mBlocks = (valuesCount + 31) / 32;
        mUpdateMask = new System.UInt32[mBlocks];
        for(var i : int = 0;i < mBlocks; i++)
			mUpdateMask[i] = 0;
    }

    function Clear()
    {
       if (mUpdateMask)
          for(var i : int = 0;i<mCount;i++)
          	mUpdateMask[i]=0;
    }
}