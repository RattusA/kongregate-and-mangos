﻿#pragma strict

function Start () 
{
	RenderSettings.ambientLight = Color(0.5058, 0.5058, 0.5058);
	var lightObject = GameObject.Find("Directional light");
	if(lightObject)
	{
		(lightObject.GetComponent(Light) as Light).intensity = 0.3;
	}
}