var water_force : Vector3;

var player : Player;
//var obj : GameObject;

var oldFlag : boolean = false;

function ChangeSwimFlag(flag : Boolean, flag_uw : Boolean)
{
    if(player){
		if(oldFlag != flag){
			oldFlag = flag;
			player.ShowHideWeapons(!flag);
		}	
      
        player.isUnderWater = flag_uw;
        
    	if(flag){
        	player.StartSwimming();
            
            //Debug.Log("CURR ANIM: " + player.aHandler.currentMovementAnimation);
            
            if(player.aHandler.currentMovementAnimation == "swim_idle"){
            	player.transform.localPosition.y = 0.0;
            }
            else{
            	player.transform.localPosition.y = 0.75;
            }
        }
        else{
        	player.StopSwimming();
            player.transform.localPosition.y = 0.0;
            //player.parentRB.AddForce (0, 0, 0);
        }
        if(player.dive){
        	Debug.Log("se face dive");
			var collider = GetComponent(Collider);
            collider.isTrigger = true;
            player.dive = false;
        }
        //player.parentRB.AddForce(water_force);
    }
        
}

function OnCollisionEnter (other : Collision)
{
	//Debug.Log("Enter collision");
	player = other.gameObject.GetComponentInChildren(Player);
	//obj = other.gameObject;
	
    ChangeSwimFlag(true, false); 
}

function OnCollisionStay (other : Collision)
{
   // Debug.Log("Stay on collision");
    ChangeSwimFlag(true, false); 
}

function OnCollisionExit (other : Collision)
{
    ChangeSwimFlag(false, false);
}

function OnTriggerEnter (other : Collider)
{
    if(isFishingLine(other))
    {
    	return;
    }
    ChangeSwimFlag(true, true); 
    RenderSettings.fogEndDistance = 50;
}

function OnTriggerStay (other : Collider)
{
    if(isFishingLine(other))
    {
    	return;
    }
    ChangeSwimFlag(true, true); 
}

function OnTriggerExit (other : Collider)
{
    if(isFishingLine(other))
    {
    	return;
    }
   // var playerScript : MainPlayer = other.gameObject.GetComponentInChildren(MainPlayer);
   	if(!player){
   		return;	
  	}
    if(player.collControl.currentCollider == GetComponent(Collider))
  	  ChangeSwimFlag(true, false);
    else
   	   ChangeSwimFlag(false, false);
   	   
	var collider = GetComponent(Collider);
    collider.isTrigger = !collider.isTrigger;
    player.TransformParent.rotation.eulerAngles.x = player.TransformParent.rotation.eulerAngles.z = 0;
    
    RenderSettings.fogEndDistance = 300;
    Debug.Log("Exit trigger");
}

function isFishingLine(other : Collider):boolean
{
	if(other.name.Contains("Joint_"))
    {
    	return true;
    }
    return false;
}
