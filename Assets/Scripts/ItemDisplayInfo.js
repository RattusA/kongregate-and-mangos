#if UNITY_EDITOR
import System;
import System.IO;

static function ItemAndDisplayInfo()
{
	var fileName : String = "Display_info_for_items.csv";
	if (File.Exists(fileName)) 
	{
		Debug.Log(fileName+" already exists.");
		return;
	}
	var source = File.CreateText(fileName);
	var itemEntry : ItemEntry;
	var itemDisplayInf : ItemDisplayInfoEntry;
	
	Debug.Log("Begin read item DBC");
	source.WriteLine ("WoW item ID, WoM item ID, item name, display ID, prefab, altas, icon name");
	for(var i : uint = 1; i < 300000; i++)
	{
		itemEntry = new ItemEntry();
		itemEntry = AppCache.sItems.GetItemEntry(i);
		if(itemEntry.ID == 0)
		{
			continue;
		}
		itemDisplayInf = new ItemDisplayInfoEntry();
		itemDisplayInf = dbs.sItemDisplayInfo.GetRecord(itemEntry.DisplayID);
		if(itemDisplayInf.ID != 0)
		{
			source.WriteLine ("{0}, {1}, {2}, {3}, {4}, {5}, {6}", i, dbs.staticWOWToWOM(i), itemEntry.Name, itemEntry.DisplayID, itemDisplayInf.ModelMesh, itemDisplayInf.BigTexture, itemDisplayInf.InventoryIcon);
		}
		else
		{
			source.WriteLine ("{0}, {1}, {2}, {3}, null, null, null", i, dbs.staticWOWToWOM(i), itemEntry.Name, itemEntry.DisplayID);
		}
	}
	source.Close();
	Debug.Log("End read item DBC");
}

static function DisplayInfoOnly()
{
	var fileName : String = "All_display_info.csv";
	if (File.Exists(fileName)) 
	{
		Debug.Log(fileName+" already exists.");
		return;
	}
	var source = File.CreateText(fileName);
	var itemDisplayInf : ItemDisplayInfoEntry;
	
	Debug.Log("Begin read item DBC");
	source.WriteLine ("display ID, prefab, altas, icon name");
	for(var i : uint = 1; i < 500000; i++)
	{
		itemDisplayInf = new ItemDisplayInfoEntry();
		itemDisplayInf = dbs.sItemDisplayInfo.GetRecord(i);
		if(itemDisplayInf.ID == 0)
		{
			continue;
		}
		else
		{
			source.WriteLine ("{0}, {1}, {2}, {3}", itemDisplayInf.ID, itemDisplayInf.ModelMesh, itemDisplayInf.BigTexture, itemDisplayInf.InventoryIcon);
		}
	}
	source.Close();
	Debug.Log("End read item DBC");
}

static function GetAllTextsForTranslate()
{
	var fileName : String = "Item_DBC_text.csv";
	if (File.Exists(fileName)) 
	{
		Debug.Log(fileName+" already exists.");
		return;
	}
	var source = File.CreateText(fileName);
	var itemEntry : ItemEntry;
	
	Debug.Log("Begin read item DBC");
	source.WriteLine ("WoW item ID, English item name, Japanese item name");
	for(var i : uint = 1; i < 300000; i++)
	{
		itemEntry = new ItemEntry();
		itemEntry = AppCache.sItems.GetItemEntry(i);
		if(itemEntry.ID != 0)
		{
			source.WriteLine ("{0}, {1}, {2}", i, itemEntry.Name, "");
		}
	}
	source.Close();
	Debug.Log("End read item DBC");
}
#endif
