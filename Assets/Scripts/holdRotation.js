var rot : Quaternion;
var holdReposition : boolean = false;
var lpos : Vector3;

static function ABS(vec : Vector3) : Vector3
{
	var ret : Vector3;
	ret.x = Mathf.Abs(vec.x);
	ret.y = Mathf.Abs(vec.y);
	ret.z = Mathf.Abs(vec.z);
	return ret;
}
function Start()
{
	rot = transform.rotation;
	if(holdReposition)
		lpos = transform.localPosition;
}

function LateUpdate ()
{
	transform.rotation = rot;
	//if(holdReposition)
	//	transform.position = transform.parent.position+lpos;
}