class TestPlayer extends MonoBehaviour
{
	var object_name : String;
	var object : GameObject;
	var obj : GameObject;
	var current_animation : String = null;
	var current_anim_mode : byte = 0;
	var current_weapon : String = null;
	var current_armor_set : String = null;
	var weapon_obj : GameObject = null;
	var anim : Animation = null;
	var leftHand : Transform;
var rightHand : Transform;
var rightFinger : Transform;
var leftFinger : Transform;
var rightFinger1 : Transform;
var leftFinger1 : Transform;

var rightThumb1 : Transform;
var leftThumb1 : Transform;
var ischar : Boolean;
function Start()
{
	object = transform.gameObject;
	obj = object.transform.Find("obj").gameObject;
	anim = obj.GetComponent(Animation);
	Debug.Log("Player created");
	try
	{
	leftHand = object.transform.Find    ("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L ForeArm/Bip01 L Hand");
	if(!leftHand)//unele modele au denumita altfel structura de oase
		leftHand = object.transform.Find("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Neck/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L Forearm/Bip01 L Hand");
		
	rightHand = object.transform.Find("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R ForeArm/Bip01 R Hand");
	if(!rightHand)
		rightHand = object.transform.Find("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R Forearm/Bip01 R Hand");
		rightFinger = rightHand.Find("Bip01 R Finger0");
		leftFinger = rightHand.Find("Bip01 R Finger1");
		rightFinger1 = rightFinger.Find("Bip01 R Finger01");
		leftFinger1 = leftFinger.Find("Bip01 R Finger11");

		rightThumb1 = rightFinger1.Find("Bip01 R Finger0Nub");
		leftThumb1 = leftFinger1.Find("Bip01 R Finger1Nub");
		Debug.Log("Player created2");
		
		EquipArmor("Standard");
	}catch(e : Exception){Debug.Log("Not all components initialized");}
	
	
}
/*
function SendCreatedMessage()
{
	var tt : Transform = gameObject.transform;
	var opc : System.UInt16 = OpCodes.CMSG_TESTPLAYER_CREATED;
	var wp : WorldPacket = new WorldPacket(opc);
	var l : byte = object_name.length;
	wp.Append(l);
	wp.appendSTR(object_name+"\0");
	if(ischar)
	l=1;
	else
	l=0;
	wp.Append(l);
	wp.Append(tt.position.x);
	wp.Append(tt.position.y);
	wp.Append(tt.position.z);
	wp.SetOpcode(opc);
	RealmSocket.outQueue.Add(wp);
}*/
function EquipArmor(val : String)
{
	Debug.Log("Equipping "+val);
	
	var obj_transf = object.transform.Find("obj");
	if(!obj_transf)
		return;
	
	obj_transf.gameObject.SetActive(false);
	obj_transf.gameObject.active = true;
	
	var transf : Transform = object.transform.Find("obj/Bip01");
	if(transf)
	transf.gameObject.SetActive(true);
	
	transf = object.transform.Find("obj/Standard Face");
	if(transf)
	transf.gameObject.SetActive(true);
	
	transf = object.transform.Find("obj/Standard Hair");
	if(transf)
	transf.gameObject.SetActive(true);
	try
	{
		transf = object.transform.Find("obj/Standard Body");
		if(transf)
			transf.gameObject.SetActive(true);
			
		transf = object.transform.Find("obj/Standard Arms");
		if(transf)
		transf.gameObject.SetActive(true);
		transf = object.transform.Find("obj/Standard Belt");
		if(transf)
			transf.gameObject.SetActive(true);
			
		transf = object.transform.Find("obj/Standard Belt1");
		if(transf)
		transf.gameObject.SetActive(true);
			transf = object.transform.Find("obj/Standard Belt2");
		if(transf)
		transf.gameObject.SetActive(true);
	}catch(e : Exception){Debug.Log(e);}
	try{
		object.transform.Find("obj/Armory/"+val).gameObject.SetActive(true);
	}catch(e: Exception){Debug.Log(e+"   "+val); /*object.transform.Find("obj/"+val).gameObject.SetActive(true);*/}
	current_armor_set = val;
	
}

function EquipWeapon(_trLeftHand : Transform, _trRightHand : Transform, item : Object)
{
	if(weapon_obj)
	Destroy(weapon_obj);
	Debug.Log("Equipping");
	//var go : GameObject = null;
	var rot = Quaternion.identity;
	rot.eulerAngles = Vector3.zero;
	var pos : Vector3 = Vector3.zero;
			
		//Loading Weapons
	weapon_obj = GameObject.Instantiate(item);
		 if(!weapon_obj)
		 {
		 	Debug.Log("no weapon");
		 	return;	
		 }
		 
	if(_trLeftHand)
	weapon_obj.transform.parent = _trLeftHand;  
	else
	if(_trRightHand)  
	weapon_obj.transform.parent = _trRightHand;  
	
	if(rightFinger && rightFinger1 && leftFinger && leftFinger1)
	weapon_obj.transform.localPosition = (2*rightFinger.localPosition+rightFinger1.localPosition + 2*leftFinger.localPosition+leftFinger1.localPosition)*0.25;
	//go.transform.localPosition = rightFinger.localPosition + rightFinger1.localPosition;
	//go.transform.localPosition = (rightFinger.localPosition+rightFinger1.localPosition+rightThumb1.localPosition + 2*leftFinger.localPosition+leftFinger1.localPosition+leftThumb1.localPosition)*0.25;
	weapon_obj.transform.localRotation = rot;
	
}
function ChangeAnimation(anim_string : String, anim_mode : int)
{
	if(!anim)
		return;
		
	if(anim_string)
	{
		if(anim_mode >= 0)
		anim[anim_string].wrapMode = anim_mode;
		anim.CrossFadeQueued(anim_string, 0.3, QueueMode.PlayNow);
	}
	current_animation = anim_string;
	current_anim_mode = anim_mode;
	
}
/*
function SendUpdateData()
{
	var opc : System.UInt16 = OpCodes.CMSG_TESTPLAYER_UPDATED;
	var wp : WorldPacket = new WorldPacket(opc);
	//var l : byte;
	wp.SetOpcode(opc);
	wp.appendSTR(current_animation+"\0");
	wp.appendSTR(current_weapon+"\0");
	wp.appendSTR(current_armor_set+"\0");
	wp.Append(current_anim_mode);
	RealmSocket.outQueue.Add(wp);
	Debug.Log("Message Sent: "+current_animation+" "+current_weapon+" "+current_armor_set+" "+current_anim_mode);
}*/
function Update () {
	
}
}