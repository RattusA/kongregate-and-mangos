﻿class SendPacket
{
/**
 * Send information about spell position on action bar
 * 
 * Packet structure:
 * Paket size 5 bytes
 * uint8 << slotNumbet
 * uint32 << spellID 
 */
	static function SetActionButton(position:byte, spellID:UInt32, flag:UInt32)
	{
		var pkt:WorldPacket = new WorldPacket(5);
										
		flag = (flag << 24);
		var data : System.UInt32 = spellID | flag;
		
		pkt.SetOpcode(OpCodes.CMSG_SET_ACTION_BUTTON);
		pkt.Append(position);
		pkt.Add(data);
		RealmSocket.outQueue.Add(pkt);
	}
	
	static function SetPetActionButton(petGuid:UInt64, position:uint, spellID:uint)
	{
		var pkt:WorldPacket = new WorldPacket(8+4+4);
		
		position += 3;
		pkt.SetOpcode(OpCodes.CMSG_PET_SET_ACTION);
		pkt.Add(petGuid);
		pkt.Add(position);
		pkt.Add(spellID);
		RealmSocket.outQueue.Add(pkt);
	}
	
	static function SetPetActionButton(petGuid:UInt64, positionOne:uint, spellIDOne:uint, positionTwo:uint, spellIDTwo:uint)
	{
		var pkt:WorldPacket = new WorldPacket(8+4+4+4+4);
		
		positionOne += 3;
		positionTwo += 3;
		
		pkt.SetOpcode(OpCodes.CMSG_PET_SET_ACTION);
		pkt.Add(petGuid);
		pkt.Add(positionOne);
		pkt.Add(spellIDOne);
		pkt.Add(positionTwo);
		pkt.Add(spellIDTwo);
		RealmSocket.outQueue.Add(pkt);
	}
}