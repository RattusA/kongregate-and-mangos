#pragma strict
import System;
import System.IO;

class LogMessage extends System.ValueType
{
	var message : String;
	var stack : String;
	var type : LogType;
	
	public function LogMessage(m : String, s : String, t : LogType)
	{
		message = m;
		stack = s;
		type = t;
	}
	public function ToString() : String
	{
		var returnString : String = "";
		returnString += message + "\n";
		returnString += stack + "\n\n";
		return returnString;
	}
	
}

private var messages : System.Collections.Generic.List.<LogMessage>  = new System.Collections.Generic.List.<LogMessage>();
private var output : String = "";
private var logFileName : String = "";
public var useInterceptLog : Boolean = false;
public var useGUI : Boolean = false;
function Start()
{
	if(!useInterceptLog)
		return;
	if(logFileName == "")
		logFileName = "worldofmidgard.log.txt";
	var root:String;
	root = Application.persistentDataPath;
	logFileName = String.Format("{0}/{1}", root, logFileName);
	DontDestroyOnLoad(this);
}

function OnEnable ()
{
	Application.logMessageReceived += HandleLog;
}

function OnDisable ()
{
	// Remove callback when object goes out of scope
	Application.logMessageReceived -= HandleLog;
}

function HandleLog (logString : String, stackTrace : String, type : LogType)
{
	if(messages.Count == 0 && File.Exists(logFileName))
		File.Delete(logFileName);
	var source : System.IO.StreamWriter = new System.IO.StreamWriter(logFileName, true);	
	source.WriteLine(logString);
	source.WriteLine(stackTrace);
	source.WriteLine("");
	source.Flush();
	source.Close();
	
	if(messages.Count > 14)
		messages.RemoveAt(0);
	messages.Add(new LogMessage(logString, stackTrace, type));
	output = "";
	for(var i = 0; i < messages.Count; i++)
	{
		output += messages[i].ToString();
	}
}

private var scrollViewVector : Vector2;
function OnGUI()
{
	if(useInterceptLog)
	{
		if(GUI.Button(Rect(Screen.width * 0.01, Screen.height - 21, 80, 20), "Log Window"))
		{
			useGUI = !useGUI;
		}
		if(useGUI)
		{
			GUI.Box(new Rect(5, 5, Screen.width-10, Screen.height-10), "");
			GUILayout.BeginArea(new Rect(5, 5, Screen.width-10, Screen.height-10));
				scrollViewVector = GUILayout.BeginScrollView(scrollViewVector);
					GUILayout.Label(output);
				GUILayout.EndScrollView();
			GUILayout.EndArea();
		}
	}
}