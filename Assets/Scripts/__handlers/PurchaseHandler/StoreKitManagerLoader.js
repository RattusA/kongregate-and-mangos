#pragma strict

class StoreKitManagerLoader extends MonoBehaviour
{
#if UNITY_IOS
	public var storeKitManagerGO : GameObject = null;
	var storeKitManagerComponent : Prime31.StoreKitManager = null;
	
	function Awake()
	{
		if(!storeKitManagerGO)
		{
			storeKitManagerGO = GameObject.Find("StoreKitManager");
			//DontDestroyOnLoad(storeKitManagerGO);
		}
		if(!storeKitManagerComponent)//on logout dont create another DBSHolder 
		{
			storeKitManagerComponent = storeKitManagerGO.AddComponent.<Prime31.StoreKitManager> ();
		}
	}
#endif
}