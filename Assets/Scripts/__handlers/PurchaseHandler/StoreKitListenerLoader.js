#pragma strict

class StoreKitListenerLoader extends MonoBehaviour
{
#if UNITY_IOS
	public var storeKitListenerGO : GameObject = null;
	var storeKitListenerComponent : Prime31.StoreKitEventListener = null;
	
	function Awake()
	{
		if(!storeKitListenerGO)
		{
			storeKitListenerGO = GameObject.Find("StoreKitEventListener");
			//DontDestroyOnLoad(storeKitManagerGO);
		}
		if(!storeKitListenerComponent)//on logout dont create another DBSHolder 
		{
			storeKitListenerComponent = storeKitListenerGO.AddComponent.<Prime31.StoreKitEventListener> ();
		}
	}
#endif
}