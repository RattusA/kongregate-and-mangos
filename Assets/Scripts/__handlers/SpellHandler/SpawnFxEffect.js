﻿#pragma strict

public var effectObject:GameObject;

function Awake ()
{
	if(effectObject)
	{
		var instantiatedEffect:GameObject = GameObject.Instantiate(effectObject, gameObject.transform.position, gameObject.transform.rotation);
		if(instantiatedEffect)
			instantiatedEffect.name = effectObject.name;
	}
}