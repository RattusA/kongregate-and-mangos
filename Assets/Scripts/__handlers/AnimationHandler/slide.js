var scrollSpeed : float = 0.5;
var materialNumber : byte[];
var offsetDirection : boolean = false;
var Direction : boolean = true;


private var offset : float = 0;
function Update()
{
	if(Direction){
		offset = Time.time * scrollSpeed;
	}
	else{
		offset = -Time.time*scrollSpeed;
	}
	
	if(offset > 200)
		offset = 0;

	var offsetVector : Vector2;
	
	if(offsetDirection == false)
		offsetVector = Vector2 (0,offset);
	else
		offsetVector = Vector2 (offset,0);
		
	for(var materialIndex : byte in materialNumber)
	{
		var renderer:Renderer = GetComponent(Renderer);
		renderer.materials[materialIndex].mainTextureOffset = offsetVector;
	}
}