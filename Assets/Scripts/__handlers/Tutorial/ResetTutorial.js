﻿#pragma strict

function OnGUI ()
{
	if(GUI.Button(Rect(0, Screen.height/2, 200, 20), "Reset Tutorial"))
	{
		ResetTutorialFlag();
	}
}

function ResetTutorialFlag()
{
	var accountName:String = PlayerPrefs.GetString("AccountStorage");
//	PlayerPrefs.SetInt(accountName+"_TutorialStage", 0);
	var pktt:WorldPacket;
	pktt = new WorldPacket();
	pktt.SetOpcode(OpCodes.CMSG_TUTORIAL_RESET);		
	RealmSocket.outQueue.Add(pktt);
	
	Debug.Log("Tutorial flag reset.. ");
}
	
