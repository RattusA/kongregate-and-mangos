﻿#pragma strict
class ButtonInfo
{
	var positionX:int;
	var positionY:int;
	var sizeX:int;
	var sizeY:int;
	
	function ButtonInfo(posX:int, posY:int, szX:int, szY:int)
	{
		positionX = posX;
		positionY = posY;
		sizeX = szX;
		sizeY = szY;
	}
}