#pragma strict

var player : Transform;
var arrow : GameObject;


function Update () 
{
	if(!player)
	{
		if(WorldSession.player)
			player = WorldSession.player.TransformParent;
	}
	else
	{
		arrow.transform.localEulerAngles.y = player.eulerAngles.y;
	}
}