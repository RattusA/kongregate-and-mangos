#pragma strict

public var trapName : String = "";
public var trapLifeTime : float = 30.0f;

private var timeOut : boolean = false; 


public function Start()
{	
	var trapFXName : String = "";
	var effectPrefab : GameObject = null;
	var castingEffect : GameObject = null;
	
	try
	{
		trapFXName = trapName + "Ground";
		effectPrefab = Resources.Load("prefabs/SpellEffects/Traps/" + trapFXName, GameObject);
	}
	catch(exception : Exception)
	{
		Debug.Log("No prefab");
		return;
	}
	
	castingEffect = GameObject.Instantiate(effectPrefab, gameObject.transform.position, gameObject.transform.rotation);
	castingEffect.name = trapFXName;
	castingEffect.transform.parent = gameObject.transform;
}

function Update ()
{
	if(trapLifeTime <= 1.0f)
	{
		timeOut = true;
		
	}
	else
	{
		trapLifeTime -= Time.deltaTime;
	}
}

function OnDestroy()
{
	if(timeOut)
	{
		return;
	}
	
	var trapFXName : String = "";
	var effectPrefab : GameObject = null;
	var castingEffect : GameObject = null;
	
	try
	{
		trapFXName = trapName + "Cast";
		effectPrefab = Resources.Load("prefabs/SpellEffects/Traps/" + trapFXName, GameObject);
	}
	catch(exception : Exception)
	{
		Debug.Log("No prefab");
		return;
	}
	
	if(effectPrefab == null)
	{
		return;
	}
	else
	{
		castingEffect = GameObject.Instantiate(effectPrefab, gameObject.transform.position, gameObject.transform.rotation);
		castingEffect.name = trapFXName;
	}
}
