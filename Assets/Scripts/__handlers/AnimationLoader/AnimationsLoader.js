import System.Collections.Generic;

private var _animationNames : String = "";
private var _playerAnimReff : Animation;
public var _typeOfWeapon : String;

public var mainHand : String;
public var offHand : String;

function Awake()
{
	_playerAnimReff = transform.GetComponent(Animation);
	if(!_playerAnimReff)
	{
		Debug.LogError("AnimationsLoader no have animation clips");
	}
}

function Start ()
{
}

function Update () {
	//Debug.Log("CURR ANIM: " + gameObject.animation.clip.name);
}

public function HandleAnimationsLoading (paramData:Dictionary.<String, System.Object>)
{
	var slot:byte = paramData["slot"];
	HandleAnimationsLoading(paramData["animHolder"] as GameObject, slot);
}

public function HandleAnimationsLoading (_holderReff : GameObject, _slot : byte)
{
	var _animHolder:AnimationsHolder;
	_typeOfWeapon = _holderReff.name;
	//Debug.Log("we handle animation loading for: " + _holderReff.name);
	if(_slot == EquipmentSlots.EQUIPMENT_SLOT_MAINHAND)
	{
		//Debug.Log("MAIN HAND");
		//if we equip main hand and have offhand, we remove all animations, we keep only dual hand animations which are default
		RemoveAnimations(1);
		//if we do not have offhand equiped, we add animations for mainhand
		if(offHand == ""){
			//Debug.Log("WE HAVE NO OFFHAND");
			_animHolder = _holderReff.GetComponent(AnimationsHolder);
			AddAnimations(_animHolder);
		}
		else{
			//Debug.Log("WE HAVE OFFHAND");
		}
		//mainhand slot
		mainHand = _holderReff.name;	
	}
	else if(_slot == EquipmentSlots.EQUIPMENT_SLOT_RANGED)
	{
		RemoveAnimations(1);
		_animHolder = _holderReff.GetComponent(AnimationsHolder);
		AddAnimations(_animHolder);
		mainHand = _holderReff.name;
	}
	else
	{
		//Debug.Log("OFF HAND");
		if(mainHand != ""){
			//Debug.Log("WE HAVE MAINHAND");
			//offhand slot, we remove all animations since we need to use dual
			RemoveAnimations(2);
		}
		offHand = _holderReff.name;
	}
	//we need todelete the animation holder GameObject
	//Debug.Log("deleting the animation holder prefab named: " + _holderReff.name);
	GameObject.Destroy(_holderReff);
}

public function RemoveAnimations (isMainHand : int)
{
	var _anim:String[] = _animationNames.Split(","[0]);
	var index:byte;
	var clipName:String;
	switch(isMainHand)
	{
		case 1 : // we remove main hand, we remove animations
			if(_animationNames != "")
			{
				for(index = 0; index < _anim.length; index++)
				{
					clipName = _anim[index];
					try
					{
						_playerAnimReff.RemoveClip(clipName);
					}
					catch(exception : Exception)
					{
						Debug.Log("exception, that animation clip no longer on player: " + exception);
					}
				}
				_animationNames = "";
			}
			else
			{
				//Debug.Log("no animations for mainHand...1");
			}
			mainHand = "";
			break;
			
		case 2 : // we have off hand, we have main hand so we remove animations (toggled when we use off hand)
			if(_animationNames != "")
			{
				for(index = 0; index < _anim.length; index++)
				{
					clipName = _anim[index];
					try{
						_playerAnimReff.RemoveClip(clipName);
					}
					catch(exception : Exception)
					{
						Debug.Log("Clip named: " + clipName + " not found! errmsg: " + exception);
					}
				}
				_animationNames = "";
			}
			else
			{
				//Debug.Log("no animations for mainHand...2");
			}
			offHand = "";
			break;
			
		case 3 :
			//add animations for main hand
			if(mainHand != "")
			{
				//Debug.Log("we need to add animations for mainHand weapon    " + mainHand);
				
				var _player : Player = transform.GetComponent(MainPlayer) as Player;
				if(!_player)
				{
					_player = transform.GetComponent(OtherPlayer) as Player;
				}
				
				if(_player)
				{
					var playerRace:int = _player.race;
					var playerRaceName : String = dbs.sCharacterRace.GetRecord(playerRace).Name;
					var playerGender:int = _player.gender;
					var playerGenderName : String  = dbs.sCharacterGender.GetRecord(playerGender).Name;
				}
				var ResourceString : String = "prefabs/chars/" + playerRaceName + "_" + playerGenderName + "/Animations/" + mainHand.Split("(Clone)"[0])[0];
				
				var _animHolder : GameObject = Instantiate.<GameObject>(GameResources.Load.<GameObject>(ResourceString));
				if(_animHolder)
				{
					AddAnimations(_animHolder.GetComponent(AnimationsHolder));
				}
				
				GameObject.Destroy(_animHolder);
			}
			offHand = "";
			break;
			
		default : 
			Debug.Log("some error...");
			break;
			
	}
}


public function AddAnimations (_animHolder : AnimationsHolder)
{
	for(var index:byte; index < _animHolder.Animations.length; index++)
	{
		var _clip:AnimationClip = _animHolder.Animations[index];
		_playerAnimReff.AddClip(_clip, _clip.name);
		//Debug.Log("adding clip named: " + _clip);
		_animationNames += "," + _clip.name;		
	}
	for (var state : AnimationState in _playerAnimReff)
	{
		if(state.name.Contains("atk") && !(state.name.Contains("bow") || state.name.Contains("crossbow") || state.name.Contains("throw") || state.name.Contains("basic")))
		{
	    	state.speed = 2.0f;
	    }
	    else if(state.name.Contains("bow") || state.name.Contains("crossbow"))
	    {
	    	state.speed = 2.5f;
	    }
	}
	_animationNames = _animationNames.Trim(","[0]);
	mainHand = _animHolder.transform.name;
}