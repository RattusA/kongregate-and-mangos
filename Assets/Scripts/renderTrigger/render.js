var rendering : GameObject[];

function Start()
{
	for(var go : GameObject in rendering)
	{
		var renderer:Renderer = go.GetComponent(Renderer);
		if(renderer)
		{
			renderer.enabled = false;
		}
	}
}

function OnTriggerEnter (other : Collider)
{
	//print("intra");
	if(other.name!="player")
	{
		//print("return name = " + other.name);
		return;
	}
	for(var go : GameObject in rendering)
	{
		var renderer:Renderer = go.GetComponent(Renderer);
		if(renderer)
		{
			renderer.enabled = true;
		}
	}
}
function OnTriggerExit (other : Collider)
{
	if(other.name!="player")
		return;
	for(var go : GameObject in rendering)
	{
		var renderer:Renderer = go.GetComponent(Renderer);
		if(renderer)
		{
			renderer.enabled = false;
		}
	}
}