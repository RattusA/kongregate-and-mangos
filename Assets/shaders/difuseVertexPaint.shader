Shader "DifusePaint" {
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader
	{
		BindChannels
		{
			Bind "vertex", vertex
			Bind "Color", color
			Bind "texcoord", texcoord
		}
		Cull Off
		Tags { "RenderType"="Opaque" }
		LOD 200
		Pass
		{
			Color[color]
			SetTexture[_MainTex]
			{
				Combine texture*previous DOUBLE
			}
		}
		/*Pass
		{
			Color[color]
		}*/
	} 
	//FallBack "Diffuse"
}
