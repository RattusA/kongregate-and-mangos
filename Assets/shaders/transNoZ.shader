Shader "DifusePaintTransNoZ" {
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader
	{
		BindChannels
		{
			Bind "vertex", vertex
			Bind "Color", color
			Bind "texcoord", texcoord
		}
		Cull Off
		Tags { "Queue"="Transparent" }
		LOD 200
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			AlphaTest Greater 0.3
			ColorMask RGB
			
			Color[color]
			SetTexture[_MainTex]
			{
				Combine texture*primary DOUBLE
			}
		}
	} 
	FallBack "Diffuse"
}