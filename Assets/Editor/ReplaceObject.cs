﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class ReplaceObject:ScriptableWizard
{
	public Transform rootObject;
	public Transform replaceableObject;
	string nameReplaceableObject;
	public GameObject newPrefab;
	List<Transform> childList = new List<Transform>();

	[MenuItem ("Tools/Replace object to new prefab")]
	static void CreateWizard ()
	{
		// ScriptableWizard.DisplayWizard<ReplaceObject>("Create Light", "Create", "Apply");
		//If you don't want to use the secondary button simply leave it out:
		
		ScriptableWizard.DisplayWizard<ReplaceObject>("Replace object to new prefab", "", "Procces");
	}
	void OnWizardCreate ()
	{
		// TODO
	}

	void OnWizardUpdate ()
	{
		nameReplaceableObject = (replaceableObject) ? replaceableObject.name:"";
	}

	void OnWizardOtherButton ()
	{
		if(rootObject && !string.IsNullOrEmpty(nameReplaceableObject) && newPrefab)
		{
			FindChilds(rootObject);
			Vector3 pos = new Vector3();
			Quaternion rot = new Quaternion();
			Vector3 scale = new Vector3();
			GameObject newChild;

			foreach(Transform child in childList)
			{
				pos = child.localPosition;
				rot = child.localRotation;
				scale = child.localScale;
				newChild = PrefabUtility.InstantiatePrefab(newPrefab) as GameObject;
				newChild.name = newPrefab.name;
				newChild.transform.parent = child.parent;
				newChild.transform.localPosition = pos;
				newChild.transform.localRotation = rot;
				newChild.transform.localScale = scale;
				child.parent = null;
				GameObject.DestroyImmediate(child.gameObject);
			}
			Debug.Log ("Done");
		}
		else
		{
			Debug.Log("Need fill all fields");
		}
	}

	void FindChilds(Transform root)
	{
		Transform child;
		for(int inx = 0; inx < root.childCount; inx++)
		{
			child = root.GetChild(inx);
			if(!child)
			{
				continue;
			}
			if(child.name == nameReplaceableObject)
			{
				childList.Add(child);
			}
			else
			{
				FindChilds(child);
			}
		}
	}
}
