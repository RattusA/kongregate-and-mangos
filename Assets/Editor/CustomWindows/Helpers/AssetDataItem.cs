﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class AssetDataItem
{
	public string name;
	public string guid;
	// hierarchy tree
	public string parentGuid;
	public List<string> childGuids;
	// graf of dependency
	public List<string> dependencyGuids;
	public List<string> backwardDependencies;

	private bool isExpanded = false;
	private bool isExcluded = false;
	private bool isFolder = false;
	private int hierarhyLevel = 0;
	private long count = 0;

	public bool IsExpanded {
		get {
			return isExpanded;
		}
		set {
			isExpanded = value;
		}
	}

	public bool IsExcluded {
		get {
			return isExcluded;
		}
		set {
			isExcluded = value;
		}
	}

	public bool IsFolder {
		get {
			return isFolder;
		}
		set {
			isFolder = value;
		}
	}

	public int HierarhyLevel {
		get {
			return hierarhyLevel;
		}
		set {
			hierarhyLevel = value;
		}
	}

	public long Count {
		get {
			return count;
		}
		set {
			count = value;
		}
	}

	public AssetDataItem()
	{
		name = string.Empty;
		guid = null;
		parentGuid = null;
		childGuids = new List<string>();
		dependencyGuids = new List<string>();
		backwardDependencies = new List<string>();
	}

	public AssetDataItem(string _guid)
	{
		guid = _guid;
		string path = AssetDatabase.GUIDToAssetPath(guid);
		isFolder = AssetDatabase.IsValidFolder(path);
		dependencyGuids = new List<string>();
		if(!isFolder)
		{
			string[] dependencies = AssetDatabase.GetDependencies(new string[]{path});
			foreach(string dependencyPath in dependencies)
			{
				string dependencyGuid = AssetDatabase.AssetPathToGUID(dependencyPath);
				if(!guid.Equals(dependencyGuid))
				{
					dependencyGuids.Add(dependencyGuid);
				}
			}
		}
		List<string> pathElements = new List<string>(path.Split('/'));
		name = pathElements[pathElements.Count-1];
		pathElements.RemoveAt(pathElements.Count-1);
		string parentPath = string.Join("/", pathElements.ToArray());
		parentGuid = AssetDatabase.AssetPathToGUID(parentPath);
		childGuids = new List<string>();
		backwardDependencies = new List<string>();
	}

	public AssetDataItem(object dataObj)
	{
		Hashtable dictionary = (Hashtable)dataObj;
		name = (string)dictionary["name"];
		guid = (string)dictionary["guid"];
		parentGuid = (string)dictionary["parentGuid"];
		childGuids = (dictionary ["childGuids"] as ArrayList).ToList<string> ();
		dependencyGuids = (dictionary ["dependencyGuids"] as ArrayList).ToList<string> ();
		backwardDependencies = (dictionary ["backwardDependencies"] as ArrayList).ToList<string> ();
		
		isExpanded = bool.Parse((string)dictionary["IsExpanded"]);
		isExcluded = bool.Parse((string)dictionary["IsExcluded"]);
		isFolder = bool.Parse((string)dictionary["IsFolder"]);
		hierarhyLevel = (int)(double)dictionary["HierarhyLevel"];
		count = (long)(double)dictionary["Count"];
		dictionary.Clear();
	}

	public Hashtable ToHashtable()
	{
		Hashtable dictionary = new Hashtable();
		dictionary["name"] = name;
		dictionary["guid"] = guid;
		dictionary["parentGuid"] = parentGuid;
		ClearNullFromLists();
		dictionary["childGuids"] = childGuids.ToArray();
		dictionary["dependencyGuids"] = dependencyGuids.ToArray();
		dictionary["backwardDependencies"] = backwardDependencies.ToArray();
		
		dictionary["IsExpanded"] = isExpanded.ToString();
		dictionary["IsExcluded"] = isExcluded.ToString();
		dictionary["IsFolder"] = isFolder.ToString();
		dictionary["HierarhyLevel"] = hierarhyLevel;
		dictionary["Count"] = count;
		return dictionary;
	}

	public void AddChildGuid(string childGuid)
	{
		if(!childGuids.Contains(childGuid))
		{
			childGuids.Add (childGuid);
		}
	}
	
	public void AddInDependency(string _guid)
	{
		if(!backwardDependencies.Contains(_guid))
		{
			backwardDependencies.Add (_guid);
		}
	}
	
	public void ClearNullFromLists()
	{
		for(int inx = 0; inx < childGuids.Count; inx++)
		{
			if(string.IsNullOrEmpty(childGuids[inx]))
			{
				childGuids.RemoveAt(inx);
				inx--;
			}
		}
		for(int inx = 0; inx < dependencyGuids.Count; inx++)
		{
			if(string.IsNullOrEmpty(dependencyGuids[inx]))
			{
				dependencyGuids.RemoveAt(inx);
				inx--;
			}
		}
		for(int inx = 0; inx < backwardDependencies.Count; inx++)
		{
			if(string.IsNullOrEmpty(backwardDependencies[inx]))
			{
				backwardDependencies.RemoveAt(inx);
				inx--;
			}
		}
	}
}
