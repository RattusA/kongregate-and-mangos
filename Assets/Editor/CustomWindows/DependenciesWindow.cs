﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class DependenciesWindow : EditorWindow
{
	private static string rootGuid = string.Empty;
	private static GUIStyle style = new GUIStyle();

//	SerializableDictionary<string, AssetDataItem> resourcesDatabase = new SerializableDictionary<string, AssetDataItem>();
	Dictionary<string, AssetDataItem> resourcesDatabase = new Dictionary<string, AssetDataItem>();
	private Vector2 leftAreaScrollPosition = Vector2.zero;
	private Vector2 middleAreaScrollPosition = Vector2.zero;
	private Vector2 rightAreaScrollPosition = Vector2.zero;

	private List<string> dependencies = new List<string>();
	private string dependencyFilter = string.Empty;
	private string mainFilter = string.Empty;
	private string databaseFilter = "Assets";

	private List<string> checkedForExclude = new List<string>();

	private static string pathToAssets = "D://Project/Unity/WoM_Unity5/Assets";
	private string pathToItemDisplayInfoDbc = "Resources/DBS/ItemDisplayInfo.bytes";
	private bool needShiftItemDisplayInfoDBC = true;
	private string pathToGameObjectDisplayInfoDbc = "Resources/DBS/GameObjectDisplayInfo.bytes";
	private bool needShiftGameObjectDisplayInfoDBC = true;
	private string pathToCreatureDisplayInfoDbc = "Resources/DBS/CreatureDisplayInfo.bytes";
	private bool needShiftCreatureDisplayInfoDBC = true;

	[MenuItem("Window/Dependencies")]
	static void Init()
	{
		InitStartData();
		// Get existing open window or if none, make a new one:  
		DependenciesWindow window = EditorWindow.GetWindow<DependenciesWindow>();
		window.Show();
	}

	static void InitStartData()
	{
		rootGuid = AssetDatabase.AssetPathToGUID("Assets");
		style.normal.background = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Editor/Textures/white_pixel.png");
		style.alignment = TextAnchor.LowerLeft;
	}

	void OnGUI()
	{
		GUI.backgroundColor = Color.white;
		databaseFilter = GUILayout.TextField(databaseFilter);
		if (GUILayout.Button("Fill Database"))
		{
			Debug.Log ("Start processing " + DateTime.Now.ToString());
			FillDatabase();
			Debug.Log ("End processing " + DateTime.Now.ToString());
			dependencies.Clear ();
		}
		if (GUILayout.Button("Load"))
		{
			dependencies.Clear ();
			InitStartData();
			ReadXML();
			ReadJSON();
			Debug.Log ("Finish read");
		}
		if (GUILayout.Button("Save"))
		{
			dependencies.Clear ();
			InitStartData();
			WriteXML();
			WriteJSON();
		}
		if (GUILayout.Button("Save dependencies"))
		{
			SaveListOfDependencies();
		}
		if (GUILayout.Button("Exclude Bundles"))
		{
			Debug.Log ("Start processing " + DateTime.Now.ToString());
			ExcludeBundles();
			Debug.Log ("End processing " + DateTime.Now.ToString());
		}
		if (GUILayout.Button("Exclude Worked Scenes"))
		{
			Debug.Log ("Start processing " + DateTime.Now.ToString());
			ExcludeWorkedScenes();
			Debug.Log ("End processing " + DateTime.Now.ToString());
		}
		if (GUILayout.Button("Exclude Item Models"))
		{
			Debug.Log ("Start processing " + DateTime.Now.ToString());
			ExcludeItemModels();
			Debug.Log ("End processing " + DateTime.Now.ToString());
		}
		if (GUILayout.Button("Exclude GameObject Models"))
		{
			Debug.Log ("Start processing " + DateTime.Now.ToString());
			ExcludeGameObjectModels();
			Debug.Log ("End processing " + DateTime.Now.ToString());
		}
		if (GUILayout.Button("Exclude Creature Models"))
		{
			Debug.Log ("Start processing " + DateTime.Now.ToString());
			ExcludeCreatureModels();
			Debug.Log ("End processing " + DateTime.Now.ToString());
		}
		GUILayout.Space(10);

		if(resourcesDatabase.Count > 0)
		{
			GUILayout.BeginHorizontal();
			
			GUI.backgroundColor = Color.white;
			GUILayout.BeginVertical(GUILayout.Width(position.width*0.4f - 10));
			DrawFilter(ref dependencyFilter);
			GUI.backgroundColor = Color.white;
			rightAreaScrollPosition = GUILayout.BeginScrollView(rightAreaScrollPosition);
			DrawListOfDependencies(dependencies);
			GUILayout.EndScrollView();
			GUILayout.EndVertical();
			
			GUI.backgroundColor = Color.white;
			GUILayout.BeginVertical();
			DrawFilter(ref mainFilter);
			GUI.backgroundColor = Color.white;
			middleAreaScrollPosition = GUILayout.BeginScrollView(middleAreaScrollPosition);
			DrawHierarhyItem(rootGuid);
			GUILayout.EndScrollView();
			GUILayout.EndVertical();

			GUILayout.EndHorizontal();
		}
	}

	void FillDatabase()
	{
		InitStartData();
		resourcesDatabase = new SerializableDictionary<string, AssetDataItem>();
		string[] assetGuids = AssetDatabase.FindAssets("", new string[]{databaseFilter});
		foreach(string guid in assetGuids)
		{
			AddAssetToDatabase(guid);
		}
		FillBackwardDependencies();

		Debug.Log ("");
	}

	void AddAssetToDatabase(string guid)
	{
		if(!resourcesDatabase.ContainsKey(guid))
		{
			AssetDataItem item = new AssetDataItem(guid);
			resourcesDatabase.Add (guid, item);
			if(!string.IsNullOrEmpty(item.parentGuid))
			{
				AddAssetToDatabase(item.parentGuid);
				AssetDataItem parent = resourcesDatabase[item.parentGuid];
				item.HierarhyLevel = parent.HierarhyLevel + 1;
				parent.AddChildGuid(guid);
			}
		}
	}

	void FillBackwardDependencies()
	{
		foreach(AssetDataItem item in resourcesDatabase.Values)
		{
			foreach(string dependencyGuid in item.dependencyGuids)
			{
				AssetDataItem dependencyItem;
				if(resourcesDatabase.TryGetValue(dependencyGuid, out dependencyItem))
				{
					dependencyItem.backwardDependencies.Add (item.guid);
				}
			}
		}
	}

	void DrawHierarhyItem(string guid)
	{
		try
		{
			AssetDataItem item;
			if(resourcesDatabase.TryGetValue(guid, out item))
			{
				if(item.IsFolder)
				{
					GUILayout.BeginHorizontal();
					GUILayout.Space(item.HierarhyLevel * 20);
					Color color = (resourcesDatabase[guid].IsExcluded) ? Color.red : Color.cyan;
					GUI.backgroundColor = color;
					string expandSymbol = (item.IsExpanded) ? "- " : "+ ";
					if(GUILayout.Button(expandSymbol + item.name, style, GUILayout.Height(16)))
					{
						item.IsExpanded = !item.IsExpanded;
					}
					DrawExcludeButton(guid);
					DrawSelectButton(guid);
					DrawRemoveAssetButton(guid);
					GUILayout.EndHorizontal();
					if(item.IsExpanded)
					{
						var enumerator = item.childGuids.GetEnumerator();
						while(enumerator.MoveNext())
						{
							string childGuid = enumerator.Current;
							DrawHierarhyItem(childGuid);
						}
					}
				}
				else if(AssetDatabase.GUIDToAssetPath(guid).Contains(mainFilter))
				{
					GUILayout.BeginHorizontal();
					GUILayout.Space(item.HierarhyLevel * 20);
					Color color = (resourcesDatabase[guid].IsExcluded) ? Color.red : Color.white;
					GUI.backgroundColor = color;
					if(GUILayout.Button("> " + item.name, style, GUILayout.Height(16)))
					{
						string path = AssetDatabase.GUIDToAssetPath(guid);
						Selection.objects = new UnityEngine.Object[]{AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path)};
					}
					DrawSeeDependenciesButton(guid);
					DrawSeeBackwardDependenciesButton(guid);
					DrawExcludeButton(guid);
					DrawSelectButton(guid);
					DrawRemoveAssetButton(guid);
					GUILayout.EndHorizontal();
				}
			}
		}
		catch(Exception exception)
		{
			Debug.LogException(exception);
		}
	}

	private void DrawSelectButton(string guid)
	{
		GUI.backgroundColor = Color.green;
		if( GUILayout.Button("Select", GUILayout.Width(60)) )
		{
			string path = AssetDatabase.GUIDToAssetPath(guid);
			Selection.objects = new UnityEngine.Object[]{AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path)};
		}
	}
	
	private void DrawSeeDependenciesButton(string guid)
	{
		GUI.backgroundColor = Color.yellow;
		if( GUILayout.Button("See\nDependencies", GUILayout.Width(100), GUILayout.Height(30)) )
		{
			AssetDataItem item = resourcesDatabase[guid];
			dependencies = AssetGuidsToAssetPaths(item.dependencyGuids);
			dependencies.Sort ();
		}
	}

	private void DrawSeeBackwardDependenciesButton(string guid)
	{
		GUI.backgroundColor = Color.yellow;
		if( GUILayout.Button("See Back\nDependencies", GUILayout.Width(100), GUILayout.Height(30)) )
		{
			AssetDataItem item = resourcesDatabase[guid];
			dependencies = AssetGuidsToAssetPaths(item.backwardDependencies);
			dependencies.Sort ();
		}
	}
	
	private void DrawExcludeButton(string guid)
	{
		GUI.backgroundColor = Color.yellow;
		if( GUILayout.Button("Exclude", GUILayout.Width(60)) )
		{
			checkedForExclude.Clear();
			Exclude(guid);
		}
	}
	
	private void DrawRemoveAssetButton(string guid)
	{
		GUI.backgroundColor = Color.red;
		if( GUILayout.Button("X", GUILayout.Width(18)) )
		{
			RemoveAsset(guid);
		}
	}

	List<string> AssetGuidsToAssetPaths(List<string> guids)
	{
		List<string> ret = new List<string>();
		foreach(string guid in guids)
		{
			ret.Add (AssetDatabase.GUIDToAssetPath(guid));
		}
		return ret;
	}

	void DrawListOfDependencies(List<string> list)
	{
		foreach(string path in list)
		{
			if(path.Contains(dependencyFilter))
			{
				string guid = AssetDatabase.AssetPathToGUID(path);
				Color color = (resourcesDatabase[guid].IsExcluded) ? Color.red : Color.white;
				GUI.backgroundColor = color;
				GUILayout.Label(path, style);
			}
		}
	}

	void DrawFilter(ref string filter)
	{
		GUILayout.BeginHorizontal();
		GUILayout.Label("Filter: ", GUILayout.Width(60));
		filter = GUILayout.TextField(filter);
		GUI.backgroundColor = Color.red;
		if(GUILayout.Button("X", GUILayout.Width(20)))
		{
			filter = string.Empty;
		}
		GUILayout.EndHorizontal();
	}

	private void Exclude(string guid)
	{
		if(!checkedForExclude.Contains(guid))
		{
			checkedForExclude.Add (guid);
			AssetDataItem item;
			if(resourcesDatabase.TryGetValue(guid, out item))
			{
				item.ClearNullFromLists();
				if( item.IsFolder )
				{
					foreach(string childGuid in item.childGuids)
					{
						Exclude(childGuid);
					}
				}
				else
				{
					foreach(string dependencyGuid in item.dependencyGuids)
					{
						if(!string.IsNullOrEmpty(dependencyGuid))
						{
							Exclude(dependencyGuid);
						}
					}

//					// Remove item form backward dependency items
//					foreach(string backDependencyGuid in item.backwardDependencies)
//					{
//						AssetDataItem backItem;
//						if(resourcesDatabase.TryGetValue(backDependencyGuid, out backItem))
//						{
//							int index = backItem.dependencyGuids.IndexOf(guid);
//							backItem.dependencyGuids[index] = null;
//						}
//					}
				}
//				if(!string.IsNullOrEmpty(item.parentGuid))
//				{
//					AssetDataItem parentItem;
//					if(resourcesDatabase.TryGetValue(item.parentGuid, out parentItem))
//					{
//						parentItem.childGuids.Remove(guid);
//					}
//				}
				item.IsExcluded = true;
			}
		}
	}

	private void ReadXML()
	{
//		string path = Application.dataPath;
//		path = path.Substring(0, path.LastIndexOf("Assets"));
//		path += "ResourceList.xml";
//		string xmlString = string.Empty;
//		if( File.Exists(path) )
//		{
//			xmlString = File.ReadAllText(path);
//		}
//		using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
//		{
//			resourcesDatabase.ReadXml(reader);
//		}
	}

	private void WriteXML()
	{
//		string path = Application.dataPath;
//		path = path.Substring(0, path.LastIndexOf("Assets"));
//		path += "ResourceList.xml";
//		XmlWriterSettings settings = new XmlWriterSettings();
//		settings.Indent = true;
//		settings.NewLineOnAttributes = true;
//		XmlWriter writer = XmlWriter.Create(path, settings);
//		resourcesDatabase.WriteXml(writer);
//		writer.Flush();
//		writer = null;
	}

	private void ReadJSON()
	{
		string path = Application.dataPath;
		path = path.Substring(0, path.LastIndexOf("Assets"));
		path += "ResourceList.json.txt";
		if( File.Exists(path) )
		{
			string json = File.ReadAllText(path);
			Hashtable dataBase = (Hashtable)MiniJSON.jsonDecode( json );
			resourcesDatabase.Clear ();
			foreach(string key in dataBase.Keys)
			{
				resourcesDatabase[key] = new AssetDataItem(dataBase[key]);
			}
			Debug.Log ("Loaded from " + path);
		}
		else
		{
			Debug.Log ("File not found " + path);
		}
	}

	private void WriteJSON()
	{
		Hashtable dictionary = new Hashtable();
		foreach(KeyValuePair<string, AssetDataItem> pair in resourcesDatabase)
		{
			if(pair.Value != null)
			{
				dictionary[pair.Key] = pair.Value.ToHashtable();
			}
		}
		string json = MiniJSON.jsonEncode( dictionary );
		string path = Application.dataPath;
		path = path.Substring(0, path.LastIndexOf("Assets"));
		path += "ResourceList.json.txt";
		File.WriteAllText(path, json);
		
		Debug.Log ("Saved to " + path);
	}

	private void ExcludeBundles()
	{
		string[] bundleNames = AssetDatabase.GetAllAssetBundleNames();
		foreach(string bundlename in bundleNames)
		{
			string[] assetPaths = AssetDatabase.GetAssetPathsFromAssetBundle(bundlename);
			foreach(string path in assetPaths)
			{
				string guid = AssetDatabase.AssetPathToGUID(path);
				checkedForExclude.Clear();
				Exclude (guid);
			}
		}
	}

	private void ExcludeWorkedScenes()
	{
		EditorBuildSettingsScene[] usedScenes = EditorBuildSettings.scenes;
		foreach(EditorBuildSettingsScene scene in usedScenes)
		{
			string guid = AssetDatabase.AssetPathToGUID(scene.path);
			checkedForExclude.Clear();
			Exclude (guid);
		}
	}

	private void RemoveAsset(string guid)
	{
		AssetDataItem item;
		if(resourcesDatabase.TryGetValue(guid, out item))
		{
			while(item.childGuids.Count > 0)
			{
				string childGuid = item.childGuids[0];
				RemoveAsset(childGuid);
			}
			AssetDataItem parentItem;
			if(resourcesDatabase.TryGetValue(item.parentGuid, out parentItem))
			{
				parentItem.childGuids.Remove(guid);
			}
			foreach(string dependencyGuid in item.dependencyGuids)
			{
				AssetDataItem dependencyItem;
				if(resourcesDatabase.TryGetValue(dependencyGuid, out dependencyItem))
				{
					dependencyItem.backwardDependencies.Remove(guid);
				}
			}
			foreach(string backDependencyGuid in item.backwardDependencies)
			{
				AssetDataItem backDependencyItem;
				if(resourcesDatabase.TryGetValue(backDependencyGuid, out backDependencyItem))
				{
					backDependencyItem.dependencyGuids.Remove(guid);
				}
			}
			resourcesDatabase.Remove(guid);
		}

		string assetPath = AssetDatabase.GUIDToAssetPath(guid);
		string path = Application.dataPath;
		path = path.Substring(0, path.LastIndexOf("Assets"));
		path += assetPath;
		if(File.Exists(path))
		{
			AssetDatabase.MoveAssetToTrash(assetPath);
		}
	}

	private void ExcludeItemModels()
	{
		ItemDisplayInfoDBS tableWoMItemDisplayInfo;
		DBC.needShift = needShiftItemDisplayInfoDBC;
		tableWoMItemDisplayInfo = new ItemDisplayInfoDBS(pathToItemDisplayInfoDbc, pathToAssets+"/");
		if(tableWoMItemDisplayInfo.fName == "")
		{
			Debug.LogError("ItemDisplayInfoDbc file " + pathToItemDisplayInfoDbc + " not exist");
			return;
		}

		foreach(ItemDisplayInfoEntry element in tableWoMItemDisplayInfo.colection)
		{
			if(string.IsNullOrEmpty(element.ModelMesh)
			   || element.ModelMesh.Equals("null")
			   || element.ModelMesh.Equals("Null")
			   || element.ModelMesh.Equals("NULL"))
			{
				continue;
			}
			string path = "Assets/Resources/" + element.ModelMesh;
			path += ".prefab";
			string guid = AssetDatabase.AssetPathToGUID(path);
			if(resourcesDatabase.ContainsKey(guid))
			{
				Exclude(guid);
			}
			else
			{
				CheckWhetherOutfitModel(element.ModelMesh);
			}
		}
	}

	private void CheckWhetherOutfitModel(string elementModelMesh)
	{
		string[] raceNames = new string[]{"BloodDrak", "Dwarf", "DarkElf", "Elf", "Human", "Orc"};
		string[] genderNames = new string[]{"Female", "Male"};
		string basePath = "Assets/Resources/prefabs/chars";
		string[] splitedModelPath = elementModelMesh.Split('.');
		string modelName = elementModelMesh;
		if(splitedModelPath.Length >= 4)
		{
			modelName = splitedModelPath[0];
		}
		foreach(string race in raceNames)
		{
			foreach(string gender in genderNames)
			{
				string path = string.Format ("{0}/{1}_{2}/{3}.prefab", basePath, race, gender, modelName);
				string guid = AssetDatabase.AssetPathToGUID (path);
				if(resourcesDatabase.ContainsKey(guid))
				{
					Exclude(guid);
				}
				else
				{
					Debug.Log ("Not exist asset "+path);
				}
			}
		}
	}

	private void ExcludeGameObjectModels()
	{
		GameObjectDisplayInfoDBS tableWoMGameObjectDisplayInfo;
		DBC.needShift = needShiftGameObjectDisplayInfoDBC;
		tableWoMGameObjectDisplayInfo = new GameObjectDisplayInfoDBS(pathToGameObjectDisplayInfoDbc, pathToAssets+"/");
		if(tableWoMGameObjectDisplayInfo.fName == "")
		{
			Debug.LogError("GameObjectDisplayInfoDbc file " + pathToGameObjectDisplayInfoDbc + " not exist");
			return;
		}

		foreach(GameObjectDisplayInfoEntry element in tableWoMGameObjectDisplayInfo.colection)
		{
			string path = "Assets/Resources/" + element.ModelMesh;
			path += ".prefab";
			string guid = AssetDatabase.AssetPathToGUID(path);
			if(resourcesDatabase.ContainsKey(guid))
			{
				Exclude(guid);
			}
			else
			{
				Debug.Log ("Not exist asset "+path);
			}
		}
	}

	private void ExcludeCreatureModels()
	{
		CreatureDisplayInfoDBS tableWoMCreatureDisplayInfo;
		DBC.needShift = needShiftCreatureDisplayInfoDBC;
		tableWoMCreatureDisplayInfo = new CreatureDisplayInfoDBS(pathToCreatureDisplayInfoDbc, pathToAssets+"/");
		if(tableWoMCreatureDisplayInfo.fName == "")
		{
			Debug.LogError("CreatureDisplayInfoDbc file " + pathToCreatureDisplayInfoDbc + " not exist");
			return;
		}
		
		foreach(CreatureDisplayInfoEntry element in tableWoMCreatureDisplayInfo.colection)
		{
			string path = "Assets/Resources/" + element.ModelMesh;
			path += ".prefab";
			string guid = AssetDatabase.AssetPathToGUID(path);
			if(resourcesDatabase.ContainsKey(guid))
			{
				Exclude(guid);
			}
			else
			{
				Debug.Log ("Not exist asset "+path);
			}
		}
	}
	
	private void SaveListOfDependencies()
	{
		string json = MiniJSON.jsonEncode( dependencies.ToArray() );
		string path = Application.dataPath;
		path = path.Substring(0, path.LastIndexOf("Assets"));
		path += "Dependencies.json.txt";
		File.WriteAllText(path, json);
	}
}
