﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

public class SynchronizationDBCAndCreatingSpellVisual : ScriptableWizard
{
	public string pathToAssets = "D://Project/Unity/wom_android_low/Assets";
	public string pathToChangedbc = "Resources/DBS/Spell.bytes";
	public bool needShiftDBCToChange = true;
	public string pathToWoWdbc = "D://Project/WoWdbc/Spell.dbc";
	public bool needShiftWoWdbc = false;
	
	[MenuItem ("Assets/DBC/Synchronization DBC with creating SpellVisual prefabs")]
	static void CreateWizard ()
	{
		ScriptableWizard.DisplayWizard<SynchronizationDBCAndCreatingSpellVisual>("Synchronization DBC with creating SpellVisual prefabs", "Procces");
	}
	
	void OnWizardCreate () 
	{
		Debug.Log ("Started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		SpellDBS tableSpellsToChange;
		SpellDBS tableWoWSpells;
		SpellVisualPrefabBuilder spellVisualBuilder = new SpellVisualPrefabBuilder();
		if(spellVisualBuilder == null)
		{
			Debug.LogError("On MainCamera not found component SpellVisualBuilder");
			return;
		}
		spellVisualBuilder.pathToAssets = pathToAssets;

		if(!File.Exists(pathToWoWdbc))
		{
			Debug.LogError("WoW dbc file " + pathToWoWdbc + " not exist");
			return;
		}
		Debug.Log ("Loading DBC");
		DBC.needShift = needShiftDBCToChange;
		tableSpellsToChange = new SpellDBS(pathToAssets + "/" + pathToChangedbc);
		if(tableSpellsToChange.fName == "")
		{
			Debug.LogError("To change dbc file " + pathToChangedbc + " not exist");
			return;
		}
		DBC.needShift = needShiftWoWdbc;
		tableWoWSpells = new SpellDBS(pathToWoWdbc);
		
		//SpellEntry spellToChange;
		SpellEntry spellWoW;
		Hashtable effectDB = new Hashtable();
		List<string> noEditableFields = new List<string>();
		noEditableFields.Add("ID");
		noEditableFields.Add("SpellIconID");
		noEditableFields.Add("SpellName");
		noEditableFields.Add("Rank");
		noEditableFields.Add("Description");
		noEditableFields.Add("ToolTip");
		noEditableFields.Add("spellDescriptionVariableID");
		int countSpellVisual = 0;
		foreach(SpellEntry spellToChange in tableSpellsToChange.colection)
		{
			//spellToChange = tableSpellsToChange.GetRecord(id);
			if(spellToChange.ID <= 0)
				continue;
			spellWoW = tableWoWSpells.GetRecord(spellToChange.ID);
			if(spellToChange.ID == 145)
			{
				Debug.Log("");
			}
            foreach (FieldInfo field in typeof(SpellEntry).GetFields())
            {
				if(noEditableFields.Contains(field.Name))
					continue;
				if(field.Name == "Effect")
				{
					int key = CalculateKey((Int3)field.GetValue(spellToChange));
					if(key != 0)
					{
						bool hasSpellVisual = false;
						if(!effectDB.ContainsKey(key))
						{
							countSpellVisual++;
							effectDB.Add (key, countSpellVisual);//.ToString() + "-" + spellToChange.SpellName.Strings[0]
						}
					}
				}
				if(field.Name == "SpellVisual")
				{
					int key = CalculateKey(spellToChange.Effect);
					if(effectDB.ContainsKey(key))
					{
						int effectNumber = (int)effectDB[key];
						spellToChange.SpellVisual.array[0] = effectNumber;
						try
						{
							spellVisualBuilder.CreateSpellVisual(effectNumber, spellToChange.SpellName.Strings[0], spellToChange.Effect);
						}
						catch(Exception e)
						{
							Debug.LogException(e);
						}
					}
					else
					{
						spellToChange.SpellVisual.array[0] = 0;
					}
				}
				field.SetValue(spellToChange, field.GetValue(spellToChange));
			}
		}
		
		foreach(SpellEntry spellToChange in tableSpellsToChange.colection)
		{
			//spellToChange = tableSpellsToChange.GetRecord(id);
			if(spellToChange.ID <= 0)
				continue;
			foreach (FieldInfo field in typeof(SpellEntry).GetFields())
			{
				if(field.FieldType == typeof(uint))
				{
					field.SetValue(spellToChange, dbs.staticWOWToWOM((uint)field.GetValue(spellToChange)));
				}
			}
			SpellEntry spellToTest = tableSpellsToChange.GetRecord(spellToChange.ID);
			Debug.Log ("");
		}
		Debug.Log ("Write DBC started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		tableSpellsToChange.Write(pathToAssets + "/" + pathToChangedbc);
		Debug.Log ("Ended in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
	}
	
	int CalculateKey(Int3 array)
	{
		return ((((0 + array.array[0]) << 8) + array.array[1]) << 8) + array.array[2];
	}
}
