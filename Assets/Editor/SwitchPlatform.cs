﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class SwitchPlatform : MonoBehaviour
{
	static void Log(string message)
	{
		Debug.Log("SwitchPlatform: " + message);
	}
	static void ToMac()
	{
		if(EditorUserBuildSettings.activeBuildTarget != BuildTarget.StandaloneOSXIntel)
		{
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneOSXIntel);
			Log("platform switched to OSX");
		}
	}
	static void ToPC()
	{
		if(EditorUserBuildSettings.activeBuildTarget != BuildTarget.StandaloneWindows)
		{
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneWindows);
			Log("platform switched to Windows");
		}
	}
	static void ToAndroid()
	{
		if(EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android)
		{
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
			Log("platform switched to Android");
		}
	}
	static void ToIOS()
	{
		if(EditorUserBuildSettings.activeBuildTarget != BuildTarget.iOS)
		{
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.iOS);
			Log("platform switched to iOS");
		}
	}
	static void ToLinux()
	{
		if(EditorUserBuildSettings.activeBuildTarget != BuildTarget.StandaloneLinux)
		{
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneLinux);
			Log("platform switched to Linux");
		}
	}
	static void ToWeb()
	{
		if(EditorUserBuildSettings.activeBuildTarget != BuildTarget.WebPlayer)
		{
			EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.WebPlayer);
			Log("platform switched to WebPlayer");
		}
	}
}
