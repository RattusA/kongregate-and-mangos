﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class NameForBundle
{
	[MenuItem ("Custom Scripts/Set Name For Bundles")]
	public static void SetNameForBundles()
	{
		Debug.Log ("Started SetNameForBundles");
		TextAsset jsonFile = Resources.Load<TextAsset> ("config_json/BundlesInit_json");
		IDictionary bundleList = MiniJSON.jsonDecode(jsonFile.text) as IDictionary;
		jsonFile = null;
		List<string> prefabPath = new List<string>();
		Dictionary<string, int> resourceCounter = new Dictionary<string, int>();
		foreach(string key in bundleList.Keys)
		{
			string pathToPrefab = ExportAssetBundles.FullPath(key);
			string path = string.Format("{0}{1}.prefab", pathToPrefab, ExportAssetBundles.PrefabName(key));
			if(AssetDatabase.LoadAssetAtPath<Object>(path) != null)
			{
				prefabPath.Add(path);
				resourceCounter[path] = 2;
			}
		}
		bundleList.Clear();
		foreach(string prefab in prefabPath)
		{
			string[] prefabList = {prefab};
			List<string> dependencies = new List<string>(AssetDatabase.GetDependencies(prefabList));
			foreach(string dependenciePath in dependencies)
			{
				Object dependencie = AssetDatabase.LoadAssetAtPath<Object>(dependenciePath);
				string dependencieType = dependencie.GetType().ToString();
				dependencie = null;
				if((dependencieType.Contains("GameObject") && dependencieType.Contains(".prefab"))
				   || dependencieType.Contains("Texture")
//				   || dependencieType.Contains("Material")
				   || dependencieType.Contains("AudioClip"))
				{
					int count = 0;
					if(resourceCounter.TryGetValue(dependenciePath, out count))
					{
						count++;
					}
					else
					{
						count = 1;
					}
					resourceCounter[dependenciePath] = count;
				}
			}
		}
		prefabPath.Clear();

		int repeateAssetCount = 0;
		foreach(string assetPath in resourceCounter.Keys)
		{
			if(resourceCounter[assetPath] > 1)
			{
				repeateAssetCount++;
				AssetImporter asssetImporter = AssetImporter.GetAtPath(assetPath);
				string path = assetPath.Replace(".", "_");
				path = path.Substring(7, path.Length - 7); // Remove "Assets/"
				path = string.Format("{0}.{1}", path, "unity3d");
				asssetImporter.assetBundleName = path;
			}
		}
		resourceCounter.Clear();
		Debug.Log ("Finished SetNameForBundles _____ Count = "+repeateAssetCount);
	}

	[MenuItem ("Custom Scripts/Remove All Names For Bundles")]
	public static void RemoveAllNameForBundles()
	{
		Debug.Log ("Started RemoveAllNameForBundles");
		string[] bundleNames = AssetDatabase.GetAllAssetBundleNames();
		Debug.Log("bundleNames count = "+bundleNames.Length);
		foreach(string name in bundleNames)
		{
			AssetDatabase.RemoveAssetBundleName(name, true);
		}
		bundleNames = new string[0];
		Debug.Log ("Finished RemoveAllNameForBundles");
	}
}
