using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class WizardProccesWorld : ScriptableWizard 
{
	public GameObject world;
	
    [MenuItem ("Tools/World Processing")]
    static void CreateWizard () {
       // ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create", "Apply");
        //If you don't want to use the secondary button simply leave it out:

		ScriptableWizard.DisplayWizard<WizardProccesWorld>("World Processing", "Procces");

	
    }
    void OnWizardCreate () 
    {
		ProcessWorld();
    }  
    void OnWizardUpdate () {
    }   
    // When the user pressed the "Refresh" button OnWizardOtherButton is called.
    void OnWizardOtherButton () 
    {

    }
	
	LODManager[] components = null;
	Transform[] childs = null;
	string trName =  "";
	void ProcessWorld()
	{
		components = world.GetComponentsInChildren<LODManager>();//gets all Character model childrens
		
		for( int i  = 0; i < components.Length; i++ )
		{
			
			LODManager comp = (components[i]);//.GetComponent("LODManager") as LODManager);
			if(comp)
			{
				childs = comp.transform.GetComponentsInChildren<Transform>();
				
				for(int j = 0; j < childs.Length; j++ )
				{
					if(childs[j].name == comp.transform.name)
						continue;
						
					Debug.Log("Destroy  mesh  "+trName);
					Destroy(childs[j].gameObject);
				}
			}
		}
	}
	
	string [] sufixs = {"_LOW", "_HIGH", "_HI", "_MID", "_MED", "_COLLISION", "_COLLIDER", "_COLISSION"};
	bool ContainsSufix(string str)
	{
		str = str.ToUpper();
		Debug.Log(str);
		for(int i = 0; i<sufixs.Length; ++i)
		{
			if(str.Contains(sufixs[i]))
				return true;
		}	
		
		return false;
	}
}
