﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class CreatePrefabListForBundles
{
	[MenuItem("Assets/CreatePrefabListForBundles")]
	static void CreateList()
	{
		string dirName = Selection.activeObject ? Selection.activeObject.name : "";
		string dirNameCorrected = Selection.activeObject ? Selection.activeObject.name+Path.DirectorySeparatorChar : "";
		string pathToSave = EditorUtility.SaveFilePanel ("Save List", "", dirName, "txt");
		string pathToResource = Application.dataPath+"/Resources/prefabs/"+dirNameCorrected;
		string[] fileList = Directory.GetFiles(pathToResource, "*.prefab", 
		                                       SearchOption.AllDirectories);
		string text = "";
		foreach (string fileName in fileList)
		{
			string temp = fileName.Replace(Application.dataPath+"/Resources/prefabs/", "");
			temp = temp.Replace(".prefab", "");
			temp = temp.Replace(""+Path.DirectorySeparatorChar, "/");
			text += "\t\t\""+temp+"\": \""+ConvertResourceNameToBundleName(temp)+"\",\n";
		}
		Debug.Log(text);
		if(!string.IsNullOrEmpty(text))
		{
			System.IO.File.WriteAllText(pathToSave, text);
		}
	}

	static string ConvertResourceNameToBundleName(string fileName)
	{
		string ret;
		ret = fileName.Replace("/", "_");
		return ret;
	}
}
