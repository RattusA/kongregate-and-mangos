﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class CreateRandomArmorDisplayID : ScriptableWizard
{
	private static string pathToAssets = "D://Project/Unity/WoM_Unity4/Assets";
	public string pathToItemDbc = "Resources/DBS/Item.bytes";
	public bool needShiftItemDBC = true;
	
	public string pathToItemDisplayInfoDbc = "Resources/DBS/ItemDisplayInfo.bytes";
	public bool needShiftItemDisplayInfoDBC = true;
	public string pathToServerItemDbc = "D://Project/WOM/Armor/ItemServer.dbc";
	public bool needShiftServerItemDbc = false;

	public string pathArmorIcons = "D://Project/WOM/Armor/Armor_icons_json.txt";

	public string pathToItemSql = "D://Project/WOM/Armor/item_templeate_sql.csv";

	public string pathToResultFile = "D://Project/WOM/Armor/result_item.txt";
//	public string pathToResultServerItemDbc = "D://Project/WOM/Armor/Item.dbc"; // server dbc
//	public string pathToResultClientItemBytes = "D://Project/WOM/Armor/Item.bytes"; // clien dbc
//	public string pathToResultSqlQuery = "D://Project/WOM/Armor/SqlQuery.sql";
	
	[MenuItem ("Custom Scripts/CreateRandomArmorDisplayID")]
	static void CreateWizard ()
	{
		pathToAssets = Application.dataPath.Replace(":/", "://");
		ScriptableWizard.DisplayWizard<CreateRandomArmorDisplayID>("CreateRandomArmorDisplayID", "Procces");
	}
	
	void OnWizardCreate () 
	{
		Debug.Log ("Started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		ItemDBS tableWoMItems;
		ItemDisplayInfoDBS tableWoMItemDisplayInfo;
		ServerItemDBS tableServerItems;

		// **************ItemDBC
		DBC.needShift = needShiftItemDBC;
		tableWoMItems = new ItemDBS(pathToItemDbc, pathToAssets+"/");
		if(tableWoMItems.fName == "")
		{
			throw new Exception("ItemDBS file " + pathToItemDbc + " not exist");
		}
		
		// *************ItemDisplayInfoDBC
		DBC.needShift = needShiftItemDisplayInfoDBC;
		tableWoMItemDisplayInfo = new ItemDisplayInfoDBS(pathToItemDisplayInfoDbc, pathToAssets+"/");
		if(tableWoMItemDisplayInfo.fName == "")
		{
			throw new Exception("ItemDisplayInfoDbc file " + pathToItemDisplayInfoDbc + " not exist");
		}
		
		// *************ItemDBC from server
		if(!File.Exists(pathToServerItemDbc))
		{
			throw new Exception("Server item dbc file " + pathToServerItemDbc + " not exist");
		}
		DBC.needShift = needShiftServerItemDbc;
		int pathEndIndex = pathToServerItemDbc.LastIndexOf("/")+1;
		string fileName = pathToServerItemDbc.Substring(pathEndIndex, pathToServerItemDbc.Length-pathEndIndex);
		tableServerItems = new ServerItemDBS(fileName, pathToServerItemDbc.Substring(0, pathEndIndex));

		// *************item_template
		ItemTemplateFromSQL itemFromSql = new ItemTemplateFromSQL(pathToItemSql);

		// *************Armor_icons_json
		var streamReader = new StreamReader(pathArmorIcons);
		var jsonFile = streamReader.ReadToEnd();
		IDictionary armorIcons = (IDictionary)JSONUtil.DecodeString(jsonFile);

		string[] colorsPoor = {"204.204.204", "255.255.255", "194.255.209", "255.255.0"};
		string[] colorsNormal = {"153.153.153", "172.128.0", "255.195.0", "0.255.0"};
		string[] colorsUncommon = {"102.255.204", "255.255.0", "0.204.255", "255.127.0", "0.59.191", "172.128.0", "255.0.214"};
		string[] colorsRare = {"255.255.0", "255.0.214", "255.0.214", "0.255.0", "255.127.0"};
		string[] colorsEpic = {"255.0.0"};
		int[] arraySetWithHelmet = {1, 3, 4, 5, 6, 9, 10, 13, 16, 17, 18};
		List<int> setListWithHelmet = new List<int>(arraySetWithHelmet);
		int[] misc = {19, 1};
		int[] cloth = {1, 2, 9, 13};
		int[] leather = {8, 18, 6, 11};
		int[] mail = {12, 5, 7, 16};
		int[] plate = {10, 17, 3, 4};
		System.Random randomGenerator = new System.Random(DateTime.UtcNow.Millisecond);

		List<CraftElement> modifiedItems = new List<CraftElement>();

		// ************************Processing
		var enumerator  = itemFromSql.itemListFromSql.GetEnumerator();
		while(enumerator.MoveNext())
		{
			if(enumerator.Current.Key > 0)
			{
				var current = enumerator.Current.Value;
				var displayInfo = tableWoMItemDisplayInfo.GetRecord(current.itemEntry.DisplayID);
				if(displayInfo.ID <= 0)
				{
					CraftElement item = new CraftElement();
					item.id = (int)current.itemEntry.ID;
					item.nameOrigin = current.itemEntry.Name;
					item.nameNew = current.itemEntry.Name;
					item.description = current.description;
					item.tooltip = "";

					// set model
					int[] setList;
					string armorMaterial;
					switch(current.itemEntry.SubClass)
					{
					case 1:
						setList = cloth;
						armorMaterial = "cloth";
						break;
					case 2:
						setList = leather;
						armorMaterial = "leather";
						break;
					case 3:
						setList = mail;
						armorMaterial = "mail";
						break;
					case 4:
						setList = plate;
						armorMaterial = "plate";
						break;
					default:
						setList = misc;
						armorMaterial = "cloth";
						break;
					}
					int setNumber = 0;
					while(setNumber == 0 || (current.itemEntry.InventorySlot == 1 && !setListWithHelmet.Contains(setNumber)))
					{
						setNumber = setList[randomGenerator.Next(0, setList.Length-1)];
					}

					string armorPart = "Chest";
					switch(current.itemEntry.InventorySlot)
					{
					case 1:
						armorPart = "Head";
						break;
					case 5:
						armorPart = "Chest";
						break;
					case 7:
						armorPart = "Pants";
						break;
					case 8:
						armorPart = "Boots";
						break;
					case 10:
						armorPart = "Gloves";
						break;
					}

					// set color
					string[] colorList;
					switch(current.quality)
					{
					case 0:
						colorList = colorsPoor;
						break;
					case 1:
						colorList = colorsNormal;
						break;
					case 2:
						colorList = colorsUncommon;
						break;
					case 3:
						colorList = colorsRare;
						break;
					case 4:
						colorList = colorsEpic;
						break;
					default:
						colorList = colorsEpic;
						break;
					}
					string color = colorList[randomGenerator.Next(0, colorList.Length-1)];
					item.rankOrModel = "Set"+setNumber+"/"+armorPart+"."+color;

					// set icon
					IDictionary dictionary = (IDictionary)armorIcons[armorMaterial];
					IList listOfIcons = (IList)dictionary[armorPart];
					item.iconName = (string)listOfIcons[randomGenerator.Next(0, listOfIcons.Count-1)];

					modifiedItems.Add(item);
				}
			}
		}

		// save result
		if (File.Exists(pathToResultFile)) 
		{
			Debug.Log(pathToResultFile+" already exists.");
			return;
		}
		StreamWriter source = File.CreateText(pathToResultFile);
		source.WriteLine ("item ID, original item name, new item name, icon name, model, description, tooltip");
		foreach(CraftElement item in modifiedItems)
		{
			source.WriteLine ("{0},\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\"", item.id, item.nameOrigin,
			                  item.nameNew, item.iconName, item.rankOrModel, item.description, item.tooltip);
		}
		source.Close();


		Debug.Log ("Ended in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
	}
}