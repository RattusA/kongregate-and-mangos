using UnityEditor;
using UnityEngine;
using Array = System.Collections.Generic;

[System.Serializable]
public class LightItem
{
	public string name;
	
	public GameObject target;
	public Texture2D newMaterial;
	public string newShader;
	[HideInInspector]
	public GameObject oldTarget;
	
	public LightItem()
	{
		name = "Item1";
		newShader = "Custom/TwoMasksCharacterCustomization";	
	}
	public LightItem(string _name)
	{
		name = _name;
		newShader = "Custom/TwoMasksCharacterCustomization";
	}
	
}
[System.Serializable]
public class Set
{
	public string name;
	public LightItem[] pieces = new LightItem[6];

	
	public Set()
	{
		name = "Set1";	
		
		pieces[0] = new LightItem("Tattoo1");
		pieces[1] = new LightItem("Tattoo2");
		pieces[2] = new LightItem("Tattoo3");
		pieces[3] = new LightItem("Tattoo4");
		pieces[4] = new LightItem("Tattoo5");
		pieces[5] = new LightItem("Face");
	}
	
	public Set(string _name)
	{		
		name = _name;	
		
		pieces[0] = new LightItem("Head");
		pieces[1] = new LightItem("Cloak");
		pieces[2] = new LightItem("Chest");
		pieces[3] = new LightItem("Gloves");
		pieces[4] = new LightItem("Pants");
		pieces[5] = new LightItem("Boots");
	}
}
[System.Serializable]
public class Armory
{
	public string name = "";
	public  Set[] sets = new Set[6];
	
	public Armory(string _name)
	{
		
		name = _name;
		
		sets[0] = new Set("Standard");
		sets[1] = new Set("Trainee");
		sets[2] = new Set("Recruit");
		sets[3] = new Set("Veteran");
		sets[4] = new Set();
		sets[4].name = "Customizations/FacialHair";
	}
	
	public Armory()
	{
		name = "Character";
		
		sets[0] = new Set("Standard");
		sets[1] = new Set("Trainee");
		sets[2] = new Set("Recruit");
		sets[3] = new Set("Veteran");
		sets[4] = new Set();
		sets[4].name = "Customizations/FacialHair";
	}
}

public class WizardCreateLight : ScriptableWizard {
	public GameObject model;
	public string pathToProject = "/Users/BYELIK/wom-low-bundle/";
	public string pathToResources = "Assets/Resources/prefabs/chars/";
	public string pathToAssets = "/Assets/Meshes/Characters";
	public testRadu procces = testRadu.self;
	public string armorTextureFolder;
	[SerializeField]
	public bool bArmory = true;
	public Armory armory = new Armory();
	public bool bAets = false;
	public Set[] sets = new Set[1];
	public bool bItems = false;	
	public LightItem[] items = new LightItem[1];
	
	
    [MenuItem ("Tools/Characater Processing")]
    static void CreateWizard () {
       // ScriptableWizard.DisplayWizard<WizardCreateLight>("Create Light", "Create", "Apply");
        //If you don't want to use the secondary button simply leave it out:

		ScriptableWizard.DisplayWizard<WizardCreateLight>("Characater Processing", "Procces", "Refresh");

	
    }
    void OnWizardCreate () 
    {
		
		if(armorTextureFolder == null || armorTextureFolder == "")
		{
			EditorUtility.DisplayDialog("Error",
										"You must set folder name with texture in the label 'Armor Texture Folder'",
										"OK");			
			return;
				
		}
		testRadu.pathToProject = pathToProject;
		testRadu.pathToResources = pathToResources;
		armory.name = model.name;
		testRadu.CharName = armory.name;
		testRadu.modelName = model.name;
		testRadu.armorTextureFolder = armorTextureFolder;
		if(bArmory == true)
        {
        	foreach(Set set in armory.sets)
        	{
        		testRadu.AttachToSet = set.name;
        		foreach(LightItem item in set.pieces)
        		{
        			if(item.target != null)
        			{
						string pieceName = item.target.name;
						
						testRadu.SearchingPiece = pieceName;
						if(item.newMaterial)
						{
							testRadu.ApplyNewTexture = item.newMaterial.name;//AssetDatabase.GetAssetPath(item.newMaterial);
						}
						testRadu.ApplyShader = item.newShader;
						
						testRadu.AkaPiece = item.name;
						try
						{
							procces.processPlayer(model);
						}
						catch(System.Exception e)
						{
							Debug.Log(e);
						}
        			}
        		}
        	}
        }
	
    }  
    void OnWizardUpdate () {
        helpString = "";
        
       
		
    }   
    // When the user pressed the "Refresh" button OnWizardOtherButton is called.
    void OnWizardOtherButton () 
    {
		/*if(bArmory == true)
        {
        	foreach(Set set in armory.sets)
        	{
        		foreach(Item item in set.pieces)
        		{
        			if(item.target != item.oldTarget)
        			{
        				item.oldTarget = item.target; 
        				item.mesh2 = null;
        				item.mesh3 = null;      					
        				//testRadu.FindMeshes(item.oldTarget, );
        				
        				if(!item.mesh2 || !item.mesh3)
        				{
        					throw new Exception();
        				}
        			}
        		}
        	}
        }*/

    }
}