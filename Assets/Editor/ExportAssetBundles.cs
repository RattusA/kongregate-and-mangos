﻿// Builds an asset bundle from the selected objects in the project view.
// Once compiled go to "Menu" -> "Assets" and select one of the choices
// to build the Asset Bundle
	
using UnityEngine;
using UnityEditor;
using System.IO;

public class ExportAssetBundles
{
//	[MenuItem("Assets/Bundle/Build From Groupe Selection - Track dependencies")]
//	static void ExportGroupResource ()
//	{
//		// Bring up save panel
//		string defaultPath = PlayerPrefs.GetString("LastBundleFolder", "");
//		int last = defaultPath.LastIndexOf("/")-1;
//		string defaultFolder = defaultPath;
//		if(last > -1)
//			defaultFolder = defaultPath.Substring(defaultPath.LastIndexOf("/", last) + 2, defaultPath.Length - last + 2);
//		Debug.Log("1. " + defaultPath);
//		string path = EditorUtility.SaveFolderPanel ("Save Resource", defaultPath, defaultFolder) + "/";
//		Debug.Log("2. " + path);
//		if (path.Length != 0)
//		{
//			defaultPath = path.Substring(0, path.LastIndexOf("/")+1);
//			Debug.Log("3. " + defaultFolder);
//			PlayerPrefs.SetString("LastBundleFolder", defaultPath);
//			// Build the resource file from the active selection.
//			UnityEngine.Object[] selection = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets);
//			foreach(UnityEngine.Object selectedObj in selection)
//			{
//				Selection.activeObject = selectedObj;
//				BuildPipeline.BuildAssetBundle(selectedObj, Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets), path + selectedObj.name + ".unity3d", BuildAssetBundleOptions.None, BuildTarget.Android);
//			}
//			Selection.objects = selection;
//		}
//	}

//	[MenuItem("Assets/Bundle/Build From Selection - Track dependencies")]
//	static void ExportResourceUseTrack ()
//	{
//		// Bring up save panel
//		string defaultFolder = PlayerPrefs.GetString("LastBundleFolder", "");
//		Debug.Log("1. " + defaultFolder);
//		string defName = Selection.activeObject ? Selection.activeObject.name : "";
//		string path = EditorUtility.SaveFilePanel ("Save Resource", defaultFolder, defName, "unity3d");
//		Debug.Log("2. " + path);
//		if (path.Length != 0)
//		{
//			defaultFolder = path.Substring(0, path.LastIndexOf("/"));
//			Debug.Log("3. " + defaultFolder);
//			PlayerPrefs.SetString("LastBundleFolder", defaultFolder);
//			// Build the resource file from the active selection.
//			UnityEngine.Object[] selection = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets);
//			BuildPipeline.BuildAssetBundle(Selection.activeObject, selection, path, BuildAssetBundleOptions.None, BuildTarget.Android);
//			Selection.objects = selection;
//		}
//	}

//	[MenuItem("Assets/Bundle/Build From Selection - No dependency tracking")]
//	static void ExportResourceNoTrack ()
//	{
//		// Bring up save panel
//		string path = EditorUtility.SaveFilePanel ("Save Resource", "", "New Resource", "unity3d");
//		if (path.Length != 0)
//		{
//			// Build the resource file from the active selection.
//			BuildPipeline.BuildAssetBundle(Selection.activeObject, Selection.objects, path);
//		}
//	}

	/// <summary>
	/// Manuals export all asset bundle.
	/// </summary>
	[MenuItem("Assets/Bundle/Build All - Track dependencies")]
	static void ManualExportAllAssetBundle ()
	{
		// Bring up save panel
		string defaultPath = PlayerPrefs.GetString("LastBundleFolder", Application.streamingAssetsPath);
		string path = EditorUtility.SaveFolderPanel ("Save Resource", defaultPath, "");
		if (path.Length != 0)
		{
			PlayerPrefs.SetString("LastBundleFolder", path);
			// Build the resource file
			ExportAllAssetBundle(path, BuildTarget.Android);
		}
	}

	[MenuItem("Assets/Bundle/Build Android - Track dependencies")]
	/// <summary>
	/// Exports all asset bundle for android.
	/// </summary>
	/// <param name='buildTarget'>
	/// Build target.
	/// </param>
	static void ExportAllAssetBundleForAndroid ()
	{
		AutoExportAllAssetBundle(BuildTarget.Android);
	}

	/// <summary>
	/// Exports all asset bundle for iOS.
	/// </summary>
	/// <param name='buildTarget'>
	/// Build target.
	/// </param>
	static void ExportAllAssetBundleForIOS ()
	{
		AutoExportAllAssetBundle(BuildTarget.iOS);
	}

	/// <summary>
	/// Exports all asset bundle for Mac.
	/// </summary>
	/// <param name='buildTarget'>
	/// Build target.
	/// </param>
	[MenuItem("Assets/Bundle/Build Mac OSX - Track dependencies")]
	static void ExportAllAssetBundleForMac ()
	{
		AutoExportAllAssetBundle(BuildTarget.StandaloneOSXIntel);
	}

	/// <summary>
	/// Exports all asset bundle for PC.
	/// </summary>
	/// <param name='buildTarget'>
	/// Build target.
	/// </param>
	[MenuItem("Assets/Bundle/Build PC - Track dependencies")]
	static void ExportAllAssetBundleForPC ()
	{
		AutoExportAllAssetBundle(BuildTarget.StandaloneWindows);
	}

	/// <summary>
	/// Exports all asset bundle for WebPlayer.
	/// </summary>
	[MenuItem("Assets/Bundle/Build WebPlayer - Track dependencies")]
	static void ExportAllAssetBundleForWeb ()
	{
		AutoExportAllAssetBundle(BuildTarget.WebPlayer);
	}
	
	/// <summary>
	/// Automatically export all asset bundle.
	/// </summary>
	static void AutoExportAllAssetBundle (BuildTarget buildTarget)
	{
		AssetDatabase.Refresh();
		string path = "AssetBundles"; //Application.streamingAssetsPath;
		// Build the resource file
		ExportAllAssetBundle(path, buildTarget);
	}

	/// <summary>
	/// Exports all asset bundle from json files.
	/// </summary>
	/// <param name='savePath'>
	/// Save path.
	/// </param>
	static void ExportAllAssetBundle (string savePath, BuildTarget buildTarget)
	{
		if (savePath.Length != 0)
		{
			// Choose the output path according to the build target.
			string outputPath = Path.Combine(savePath, AssetBundleManager.GetPlatformFolderForAssetBundles(EditorUserBuildSettings.activeBuildTarget) );
			if (!Directory.Exists(outputPath) )
				Directory.CreateDirectory (outputPath);
			
			BuildPipeline.BuildAssetBundles (outputPath, 0, buildTarget);

//			// Create dictionary from json files.
//			TextAsset jsonFile = Resources.Load("config_json/BundlesInit_json") as TextAsset;
//			IDictionary bundleList = MiniJSON.jsonDecode(jsonFile.text) as IDictionary;
//
//			string pathToPrefab;
//			string pathToNewPrefab;
//            List<UnityEngine.Object> toInclude = new List<UnityEngine.Object>();
//            List<string> toIncludeNames = new List<string>();
//
//			foreach(string key in bundleList.Keys)
//			{
//				pathToNewPrefab = PathMinusFileName(key);
//				pathToPrefab = FullPath(key);
//				GameObject prefab = (UnityEngine.GameObject)AssetDatabase.LoadAssetAtPath(pathToPrefab + PrefabName(key) + ".prefab", typeof(UnityEngine.GameObject));
//				Debug.Log(pathToPrefab + PrefabName(key) + ".prefab");
//				if(prefab)
//				{
//					toInclude.Add ((UnityEngine.Object)prefab);
//					toIncludeNames.Add ((String)bundleList[key]);
//				}
//				if(toInclude.Count > 0)
//				{
//					ExportPrefab(toInclude.ToArray(), toIncludeNames.ToArray(), savePath + "/" + (String)bundleList[key] + ".unity3d", buildTarget);
//				}
//				toInclude.Clear();
//				toIncludeNames.Clear();
//			}
		}
	}

	public static string FullPath(string path)
	{
		path = PathMinusFileName(path);
		path = Path.Combine ("Assets/Resources/prefabs/", path);
		return path;
	}

	static string PathMinusFileName(string path)
	{
		path = path.Replace(Path.GetFileName (path), "");
		return path;
	}

	public static string PrefabName(string path)
	{
		path = Path.GetFileNameWithoutExtension (path);
		return path;
	}
}