﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class ItemDisplayCheker : EditorWindow
{
	private class ItemRecord
	{
		public int id = 0;
		public string oldName = "";
		public string name = "";
		public string model = "";
		public Texture2D modelPreview;
		public string defaultModel = "";
		public Texture2D defaultModelPreview;
		public string icon = "";
		public Texture2D iconPreview;
		public string defaultIcon = "";
		public Texture2D defaultIconPreview;

		bool useModel = false;
		bool useDefaultModel = true;

		public bool UseModel
		{
			get { return useModel; }
			set
			{
				useModel = value;
				useDefaultModel = !value;
			}
		}

		public bool UseDefaultModel
		{
			get { return useDefaultModel; }
			set
			{
				useModel = !value;
				useDefaultModel = value;
			}
		}

		bool useIcon = true;
		bool useDefaultIcon = false;
		
		public bool UseIcon
		{
			get { return useIcon; }
			set
			{
				useIcon = value;
				useDefaultIcon = !value;
			}
		}
		
		public bool UseDefaultIcon
		{
			get { return useDefaultIcon; }
			set
			{
				useIcon = !value;
				useDefaultIcon = value;
			}
		}

		public void Draw()
		{
			GUILayout.BeginHorizontal();
			{
				GUILayout.Label (id.ToString(), GUILayout.Width(50));
				GUILayout.Label (oldName, GUILayout.Width(200));
				GUILayout.Label (name, GUILayout.Width(200));
				GUILayout.BeginVertical();
				{
					UseModel = GUILayout.Toggle (useModel, model, GUILayout.Width(200));
					GUILayout.Box (modelPreview, GUILayout.Width(128), GUILayout.Height(128));
				}
				GUILayout.EndVertical();
				GUILayout.BeginVertical();
				{
					UseDefaultModel = GUILayout.Toggle (useDefaultModel, defaultModel, GUILayout.Width(200));
					GUILayout.Box (defaultModelPreview, GUILayout.Width(128), GUILayout.Height(128));
				}
				GUILayout.EndVertical();
				GUILayout.BeginVertical();
				{
					UseIcon = GUILayout.Toggle (useIcon, icon, GUILayout.Width(200));
					GUILayout.Box (iconPreview, GUILayout.Width(128), GUILayout.Height(128));
				}
				GUILayout.EndVertical();
				GUILayout.BeginVertical();
				{
					UseDefaultIcon = GUILayout.Toggle (useDefaultIcon, defaultIcon, GUILayout.Width(200));
					GUILayout.Box (defaultIconPreview, GUILayout.Width(128), GUILayout.Height(128));
				}
				GUILayout.EndVertical();
			}
			GUILayout.EndHorizontal();
		}
	}
	
	private static string pathToAssets = "D://Project/Unity/WoM_Unity4/Assets";

	private string pathToItemDbc = "Resources/DBS/Item.bytes";
	private bool needShiftItemDBC = true;
	
	private string pathToItemDisplayInfoDbc = "Resources/DBS/ItemDisplayInfo.bytes";
	private bool needShiftItemDisplayInfoDBC = true;

	private string pathToCraftingItemList = "D://Project/WOM/Armor/result_item.txt";
	private string pathToNewItemNames = "D://Project/WOM/Armor/item_templeate_sql.csv";
	private string pathToItemResultFile = "D://Project/WOM/Armor/result_item2.txt";

	private Vector2 scrollPosition;

	private Dictionary<int, ItemRecord> recordList;

	[MenuItem ("Custom Scripts/Craft/***Item Display Cheker***")]
	static void Init ()
	{
		pathToAssets = Application.dataPath.Replace(":/", "://");
		// Get existing open window or if none, make a new one:
		ItemDisplayCheker window = (ItemDisplayCheker)EditorWindow.GetWindow (typeof (ItemDisplayCheker));
	}

	void OnGUI()
	{
		GUILayout.BeginVertical();
		{
			float halfWith = Screen.width * 0.5f;
			GUILayout.BeginHorizontal();
			{
				GUILayout.Label ("Path to assets", GUILayout.Width(halfWith));
				pathToAssets = GUILayout.TextField(pathToAssets, GUILayout.Width(halfWith));
			}
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			{
				GUILayout.Label ("pathToItemDbc", GUILayout.Width(halfWith));
				pathToItemDbc = GUILayout.TextField(pathToItemDbc, GUILayout.Width(halfWith));
				needShiftItemDBC = GUILayout.Toggle(needShiftItemDBC, "needShiftItemDBC", GUILayout.Width(halfWith));
			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			{
				GUILayout.Label ("pathToItemDisplayInfoDbc", GUILayout.Width(halfWith));
				pathToItemDisplayInfoDbc = GUILayout.TextField(pathToItemDisplayInfoDbc, GUILayout.Width(halfWith));
				needShiftItemDisplayInfoDBC = GUILayout.Toggle(needShiftItemDisplayInfoDBC, "needShiftItemDisplayInfoDBC", GUILayout.Width(halfWith));
			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			{
				GUILayout.Label ("pathToCraftingItemList", GUILayout.Width(halfWith));
				pathToCraftingItemList = GUILayout.TextField(pathToCraftingItemList, GUILayout.Width(halfWith));
			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			{
				GUILayout.Label ("pathToNewItemNames", GUILayout.Width(halfWith));
				pathToNewItemNames = GUILayout.TextField(pathToNewItemNames, GUILayout.Width(halfWith));
			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			{
				GUILayout.Label ("pathToItemResultFile", GUILayout.Width(halfWith));
				pathToItemResultFile = GUILayout.TextField(pathToItemResultFile, GUILayout.Width(halfWith));
			}
			GUILayout.EndHorizontal();
			
			if( GUILayout.Button("Read") )
			{
				ReadItemList();
			}

			if( GUILayout.Button("Save") )
			{
				SaveItemList();
			}
			scrollPosition = GUILayout.BeginScrollView(scrollPosition);
			{
				if(recordList != null)
				{
					var enumerator = recordList.GetEnumerator();
					while(enumerator.MoveNext())
					{
						enumerator.Current.Value.Draw();
					}
				}
			}
			GUILayout.EndScrollView();
		}
		GUILayout.EndVertical();
	}

	void ReadItemList()
	{
		ItemDBS tableWoMItems;
		ItemDisplayInfoDBS tableWoMItemDisplayInfo;

		Dictionary<int, CraftElement> craftItemList = new Dictionary<int, CraftElement>();
		CraftElement.GetCraftingElementList(pathToCraftingItemList, ref craftItemList);

		Dictionary<int, QualityItemEntry> newItemNames = new Dictionary<int, QualityItemEntry>();
		GetNewItemNames(pathToNewItemNames, ref newItemNames);
		
		Debug.Log ("Loading DBC");
		// **************ItemDBC
		DBC.needShift = needShiftItemDBC;
		tableWoMItems = new ItemDBS(pathToItemDbc, pathToAssets+"/");
		if(tableWoMItems.fName == "")
		{
			Debug.LogError("ItemDBS file " + pathToItemDbc + " not exist");
			return;
		}
		
		// *************ItemDisplayInfoDBC
		DBC.needShift = needShiftItemDisplayInfoDBC;
		tableWoMItemDisplayInfo = new ItemDisplayInfoDBS(pathToItemDisplayInfoDbc, pathToAssets+"/");
		if(tableWoMItemDisplayInfo.fName == "")
		{
			Debug.LogError("ItemDisplayInfoDbc file " + pathToItemDisplayInfoDbc + " not exist");
			return;
		}

		recordList = new Dictionary<int, ItemRecord>();
		var enumerator = craftItemList.GetEnumerator();
		while(enumerator.MoveNext())
		{
			CraftElement element = enumerator.Current.Value;
			ItemRecord record = new ItemRecord();
			record.id = element.id;
			record.oldName = element.nameOrigin;

			QualityItemEntry item; 
			ItemEntry clientItem = tableWoMItems.GetRecord(element.id);
			ItemDisplayInfoEntry displayInfo = new ItemDisplayInfoEntry();
			if( newItemNames.TryGetValue(element.id, out item) )
			{
				displayInfo = tableWoMItemDisplayInfo.GetRecord(item.itemEntry.DisplayID);
			}
			else if( clientItem.ID > 0 )
			{
				item = new QualityItemEntry();
				item.itemEntry = clientItem;
			}
			else
			{
				Debug.LogError("Not found item "+ element.id);
				continue;
			}

			if(displayInfo.ID <= 0)
			{
				displayInfo = tableWoMItemDisplayInfo.GetRecord(clientItem.DisplayID);
			}

			int defaultDisplayID;
			try
			{
				defaultDisplayID = (int)DefaultModels.GetDefaultModel ((uint)item.itemEntry.Class,
				                                                       (uint)item.itemEntry.SubClass,
				                                                       item.itemEntry.InventorySlot, (uint)item.quality);
			}
			catch
			{
				defaultDisplayID = 0;
			}
			string icon = "";
			string model = "";
			if(defaultDisplayID > 0)
			{
				var tmp = tableWoMItemDisplayInfo.GetRecord(defaultDisplayID);
				icon = tmp.InventoryIcon;
				model = tmp.ModelMesh;
			}
			else
			{
				icon = DefaultIcons.GetDefaultIcon(item.itemEntry.Class, item.itemEntry.SubClass, item.itemEntry.InventorySlot);
			}

			record.defaultIcon = icon;
			if(!string.IsNullOrEmpty(icon))
			{
				record.defaultIconPreview = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Resources/Icons/"+icon, typeof(Texture2D));
			}
			if(record.defaultIconPreview == null)
			{
				record.defaultIconPreview = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Resources/Icons/noicon.png", typeof(Texture2D));
			}

			record.defaultModel = model;
			if(!string.IsNullOrEmpty(model))
			{
				Object prefab = AssetDatabase.LoadAssetAtPath("Assets/Resources/prefabs/"+model, typeof(Object));
				if(prefab != null)
				{
					record.defaultModelPreview = AssetPreview.GetAssetPreview(prefab);
				}
			}
			if(record.defaultModelPreview == null)
			{
				record.defaultModelPreview = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Resources/Icons/noicon.png", typeof(Texture2D));
			}

			if(!string.IsNullOrEmpty(element.iconName))
			{
				record.icon = element.iconName;
			}
			else if(displayInfo.ID > 0 && !string.IsNullOrEmpty(displayInfo.InventoryIcon))
			{
				record.icon = displayInfo.InventoryIcon;
			}
			else
			{
				record.icon = icon;
			}
			record.iconPreview = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Resources/Icons/"+record.icon, typeof(Texture2D));
			if(record.iconPreview == null)
			{
				record.iconPreview = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Resources/Icons/noicon.png", typeof(Texture2D));
			}

			if(string.IsNullOrEmpty(displayInfo.ModelMesh))
			{
				displayInfo = tableWoMItemDisplayInfo.GetRecord(clientItem.DisplayID);
			}
			record.model = displayInfo.ModelMesh;
			if(!string.IsNullOrEmpty(element.rankOrModel))
			{
				record.model = element.rankOrModel;
			}
			if(!string.IsNullOrEmpty(record.model))
			{
				Object prefab = AssetDatabase.LoadAssetAtPath("Assets/Resources/prefabs/"+record.model+".prefab", typeof(Object));
				if(prefab != null)
				{
					record.modelPreview = AssetPreview.GetAssetPreview(prefab);
				}
			}
			if(record.modelPreview == null)
			{
				record.modelPreview = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Resources/Icons/noicon.png", typeof(Texture2D));
			}

			if(string.IsNullOrEmpty(element.nameNew))
			{
				record.name = string.IsNullOrEmpty(newItemNames[element.id].itemEntry.Name) ? newItemNames[element.id].itemEntry.Name : clientItem.Name;
			}
			else
			{
				record.name = element.nameNew;
			}
			recordList.Add(record.id, record);
		}
	}

	void SaveItemList()
	{
		if (File.Exists(pathToItemResultFile)) 
		{
			Debug.Log(pathToItemResultFile+" already exists.");
			return;
		}
		StreamWriter source = File.CreateText(pathToItemResultFile);
		source.WriteLine ("item ID, original item name, new item name, icon name, model");
		var enumerator = recordList.GetEnumerator();
		while(enumerator.MoveNext())
		{
			ItemRecord element = enumerator.Current.Value;
			if(element.id > 0)
			{
				string icon = element.UseIcon ? element.icon : element.defaultIcon;
				string model = element.UseModel ? element.model : element.defaultModel;
				source.WriteLine ("{0},{1},{2},{3},{4}", element.id, element.oldName, element.name, icon, model);
			}
		}
		source.Close();

		Debug.Log("Result Saved");
	}

	void GetNewItemNames(string path, ref Dictionary<int, QualityItemEntry> itemNewNames)
	{
		if(!File.Exists(path))
		{
			Debug.LogError("Not found pathToCraftingSpellsDBC " + path);
			return;
		}
		var streamReader = new StreamReader(path);
		var fileContents = streamReader.ReadToEnd();
		streamReader.Close();
		string[,] csvGrid = CSVReader.SplitCsvGrid(fileContents);
		ItemEntry item;
		QualityItemEntry itemRow;
		for(int inx = 0; inx < csvGrid.GetUpperBound(1); inx++)
		{
			itemRow = new QualityItemEntry();
			item = new ItemEntry();
			// ID
			if(!uint.TryParse(csvGrid[0, inx], out item.ID))
			{
				item.ID = 0;
			}
			// Name
			item.Name = csvGrid[1, inx];
			// item class
			if(!int.TryParse(csvGrid[2, inx], out item.Class))
			{
				item.Class = 0;
			}
			// item subclass
			if(!int.TryParse(csvGrid[3, inx], out item.SubClass))
			{
				item.SubClass = 0;
			}
			// item inventory type(slot)
			if(!int.TryParse(csvGrid[4, inx], out item.InventorySlot))
			{
				item.InventorySlot = 0;
			}
			// item quality
			if(!int.TryParse(csvGrid[5, inx], out itemRow.quality))
			{
				itemRow.quality = 0;
			}
			// item display id
			if(!uint.TryParse(csvGrid[6, inx], out item.DisplayID))
			{
				item.DisplayID = 0;
			}
			itemRow.itemEntry = item;
			itemNewNames[(int)itemRow.itemEntry.ID] = itemRow;
		}
		
	}

}
