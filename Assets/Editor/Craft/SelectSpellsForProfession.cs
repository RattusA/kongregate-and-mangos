using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class SelectSpellsForProfession : ScriptableWizard
{
	public string pathToAssets = "D://Project/Unity/WoM_Unity4/Assets";
	public string pathToSpellDbc = "Resources/DBS/Spell.bytes";
	public bool needShiftSpellDBC = true;
	
	public string pathToSpellIconDbc = "Resources/DBS/SpellIcon.bytes";
	public bool needShiftSpellIconDBC = true;
	
	public string pathToSkillLineAbility = "Resources/DBS/SkillLineAbility.bytes";
	public bool needShiftSkillLineAbility = true;
	
	public string pathToWoWSpellDbc = "D://Project/WoWdbc/Spell.dbc";
	public bool needShiftWoWSpellDbc = false;
	
	public string pathToSpellResultFile = "D://Project/WOM/Craft/spell.csv";

	public CraftSkill profession; 
	
	[MenuItem ("Custom Scripts/Craft/Select Spells For Profession")]
	static void CreateWizard ()
	{
		ScriptableWizard.DisplayWizard<SelectSpellsForProfession>("Select Spells For Profession", "Procces");
	}
	
	void OnWizardCreate () 
	{
		Debug.Log ("Started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		SpellDBS tableWoMSpells;
		SpellIconDBS tableWoMSpellIcons;
		SkillLineAbilityDBS tableSkillLineAbility;
		SpellDBS tableWoWSpells;

		if(!File.Exists(pathToWoWSpellDbc))
		{
			Debug.LogError("WoW dbc file " + pathToWoWSpellDbc + " not exist");
			return;
		}
		
		Debug.Log ("Loading DBC");
		// **************SpellDBC
		DBC.needShift = needShiftSpellDBC;
		tableWoMSpells = new SpellDBS(pathToSpellDbc, pathToAssets+"/");
		if(tableWoMSpells.fName == "")
		{
			Debug.LogError("SpellDBS file " + pathToSpellDbc + " not exist");
			return;
		}
		
		// *************SpellIconDBC
		DBC.needShift = needShiftSpellIconDBC;
		tableWoMSpellIcons = new SpellIconDBS(pathToSpellIconDbc, pathToAssets+"/");
		if(tableWoMSpells.fName == "")
		{
			Debug.LogError("SpellIconDBC file " + pathToSpellIconDbc + " not exist");
			return;
		}
		
		// *************SkillLineAbilityDBC
		DBC.needShift = needShiftSkillLineAbility;
		tableSkillLineAbility = new SkillLineAbilityDBS(pathToSkillLineAbility, pathToAssets+"/");
		if(tableSkillLineAbility.fName == "")
		{
			Debug.LogError("SkillLineAbilityDBS file " + pathToSkillLineAbility + " not exist");
			return;
		}

		DBC.needShift = needShiftWoWSpellDbc;
		int pathEndIndex = pathToWoWSpellDbc.LastIndexOf("/")+1;
		string fileName = pathToWoWSpellDbc.Substring(pathEndIndex, pathToWoWSpellDbc.Length-pathEndIndex);
		tableWoWSpells = new SpellDBS(fileName, pathToWoWSpellDbc.Substring(0, pathEndIndex));
		
		SpellEntry spell;
		CraftElement craftSpell;
		SpellIconEntry spellIcon;
		Dictionary<int, CraftElement> provisionalSpells = new Dictionary<int, CraftElement>();
		IEnumerator wowSpellEnumerator = tableWoWSpells.colection.GetEnumerator();
		while(wowSpellEnumerator.MoveNext())
		{
			spell = (SpellEntry)wowSpellEnumerator.Current;
			if(spell.ID > 0)
			{
				SkillLineAbilityEntry skillLine = tableSkillLineAbility.colection.Find(element =>(element.m_spell == (int)spell.ID));
				if(skillLine != null && (skillLine.m_skillLine == (int)profession))
				{
					craftSpell = new CraftElement(spell);
					provisionalSpells[craftSpell.id] = craftSpell;
				}
				else
				{
					SpellEntry childSpell;
					for(int inx = 0; inx < 3; inx++)
					{
						if(spell.EffectTriggerSpell[inx] > 0)
						{
							childSpell = tableWoWSpells.GetRecord(spell.EffectTriggerSpell[inx]);
							if(childSpell.ID > 0)
							{
								skillLine = tableSkillLineAbility.colection.Find(element =>(element.m_spell == (int)childSpell.ID));
								if(skillLine != null && (skillLine.m_skillLine == (int)profession))
								{
									craftSpell = new CraftElement(spell);
									provisionalSpells[craftSpell.id] = craftSpell;
									break;
								}
							}
						}
					}
				}
			}
		}

		Dictionary<int, CraftElement> spellForSave = new Dictionary<int, CraftElement>();
		IEnumerator craftSpellEnumerator = provisionalSpells.Values.GetEnumerator();
		while(craftSpellEnumerator.MoveNext())
		{
			craftSpell = (CraftElement)craftSpellEnumerator.Current;
			if(craftSpell.id > 0)
			{
				spell = tableWoMSpells.GetRecord(craftSpell.id);
				if(spell.ID > 0)
				{
					craftSpell.nameNew = spell.SpellName[0];
					craftSpell.rankOrModel = spell.Rank[0];
					craftSpell.description = spell.Description[0];
					craftSpell.tooltip = spell.ToolTip[0];
					spellIcon = tableWoMSpellIcons.GetRecord(spell.SpellIconID);
					if(spellIcon.ID > 0)
					{
						craftSpell.iconName = spellIcon.Icon;
					}
				}
				else
				{
					craftSpell.nameNew = "";
					craftSpell.description = "";
					craftSpell.tooltip = "";
				}
			}
			spellForSave[craftSpell.id] = craftSpell;
		}

		if (File.Exists(pathToSpellResultFile)) 
		{
			Debug.Log(pathToSpellResultFile+" already exists.");
			return;
		}
		StreamWriter source = File.CreateText(pathToSpellResultFile);
		source.WriteLine ("spell ID, original spell name, new spell name, icon name, rank, description, tooltip");
		craftSpellEnumerator = spellForSave.Values.GetEnumerator();
		while(craftSpellEnumerator.MoveNext())
		{
			source.WriteLine( ((CraftElement)craftSpellEnumerator.Current).ToString() );
		}
		source.Close();
		
		Debug.Log ("Ended in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
	}
}
