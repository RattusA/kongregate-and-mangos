﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;

/// <summary>
/// Fill item DBC with icons.
/// </summary>
public class FillItemDBCWithIcons : ScriptableWizard
{
	class DisplayInfo
	{
		public string icon = "";
		public string model = "";
		public uint id = 0;
		public DisplayInfo (string _icon, string _model)
		{
			icon = _icon;
			model = _model;
		}
	}

	public static string pathToAssets = "D://Project/Unity/WoM_Unity4/Assets";
	public string pathToItemDbc = "Resources/DBS/Item.bytes";
	public bool needShiftItemDBC = true;
	
	public string pathToItemDisplayInfoDbc = "Resources/DBS/ItemDisplayInfo.bytes";
	public bool needShiftItemDisplayInfoDBC = true;
	
	public string pathToWoWItemDbc = "D://Project/WoWdbc/Item.dbc";
	public bool needShiftWoWItemDbc = false;

	public string pathToServerItemDbc = "D://Project/WOM/Armor/ItemServer.dbc";
	public bool needShiftServerItemDbc = false;

	public string pathToCraftingItemList = "D://Project/WOM/Armor/result_item.txt";

	public string pathToResultItemDisplayInfo = "D://Project/WOM/Armor/ItemDisplayInfo.bytes";
	public string pathToResultServerItemDbc = "D://Project/WOM/Armor/Item.dbc"; // server dbc
	public string pathToResultClientItemBytes = "D://Project/WOM/Armor/Item.bytes"; // clien dbc
	public string pathToResultSqlQuery = "D://Project/WOM/Armor/SqlQuery.sql";

	[MenuItem ("Custom Scripts/Craft/Fill Item.dbc and icons")]
	static void CreateWizard ()
	{
		pathToAssets = Application.dataPath.Replace(":/", "://");
		ScriptableWizard.DisplayWizard<FillItemDBCWithIcons>("Fill Item.dbc and icons", "Procces");
	}
	
	void OnWizardCreate () 
	{
		Debug.Log ("Started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		ItemDBS tableWoMItems;
		ItemDisplayInfoDBS tableWoMItemDisplayInfo;
		ServerItemDBS tableWoWItems;
		ServerItemDBS tableServerItems;
		Dictionary<int, CraftElement> craftItemList = new Dictionary<int, CraftElement>();
		CraftElement.GetCraftingElementList(pathToCraftingItemList, ref craftItemList);
		
		if(!File.Exists(pathToWoWItemDbc))
		{
			Debug.LogError("WoW item dbc file " + pathToWoWItemDbc + " not exist");
			return;
		}

		if(!File.Exists(pathToServerItemDbc))
		{
			Debug.LogError("Server item dbc file " + pathToServerItemDbc + " not exist");
			return;
		}
		
		Debug.Log ("Loading DBC");
		// **************ItemDBC
		DBC.needShift = needShiftItemDBC;
		tableWoMItems = new ItemDBS(pathToItemDbc, pathToAssets+"/");
		if(tableWoMItems.fName == "")
		{
			Debug.LogError("ItemDBS file " + pathToItemDbc + " not exist");
			return;
		}
		
		// *************ItemDisplayInfoDBC
		DBC.needShift = needShiftItemDisplayInfoDBC;
		tableWoMItemDisplayInfo = new ItemDisplayInfoDBS(pathToItemDisplayInfoDbc, pathToAssets+"/");
		if(tableWoMItemDisplayInfo.fName == "")
		{
			Debug.LogError("ItemDisplayInfoDbc file " + pathToItemDisplayInfoDbc + " not exist");
			return;
		}
		
		DBC.needShift = needShiftWoWItemDbc;
		int pathEndIndex = pathToWoWItemDbc.LastIndexOf("/")+1;
		string fileName = pathToWoWItemDbc.Substring(pathEndIndex, pathToWoWItemDbc.Length-pathEndIndex);
		tableWoWItems = new ServerItemDBS(fileName, pathToWoWItemDbc.Substring(0, pathEndIndex));

		DBC.needShift = needShiftServerItemDbc;
		pathEndIndex = pathToServerItemDbc.LastIndexOf("/")+1;
		fileName = pathToServerItemDbc.Substring(pathEndIndex, pathToServerItemDbc.Length-pathEndIndex);
		tableServerItems = new ServerItemDBS(fileName, pathToServerItemDbc.Substring(0, pathEndIndex));

		// Create dictionary with  old display ID
		List<ItemDisplayInfoEntry> displayIdList = new List<ItemDisplayInfoEntry>();
		foreach(ItemDisplayInfoEntry info in tableWoMItemDisplayInfo.colection)
		{
			if(displayIdList.FindIndex(element => (element.InventoryIcon.Equals(info.InventoryIcon) &&
			                                       element.ModelMesh.Equals(info.ModelMesh))) < 0)
			{
				displayIdList.Add (info);
			}
		}
			
		// Preparing the dictionary to fill the display id number
		List<DisplayInfo> iconsWithDisplayID = new List<DisplayInfo>();
		CraftElement[] craftItemListValues = new CraftElement[craftItemList.Count];
		craftItemList.Values.CopyTo(craftItemListValues, 0);
		foreach(CraftElement craftItem in craftItemListValues)
		{
			if( !string.IsNullOrEmpty(craftItem.iconName) || !string.IsNullOrEmpty(craftItem.rankOrModel))
			{
				string icon = (craftItem.iconName == null) ? "": craftItem.iconName;
				string model = (craftItem.rankOrModel == null) ? "": craftItem.rankOrModel;
				if(displayIdList.FindIndex(element => (element.InventoryIcon.Equals(icon)
				                                       && element.ModelMesh.Equals(model))) < 0)
				{
					if(iconsWithDisplayID.FindIndex(element => (element.icon.Equals(icon)
					                                            && element.model.Equals(model))) < 0)
					{
						DisplayInfo iconAndModel = new DisplayInfo(icon, model);
						iconsWithDisplayID.Add (iconAndModel);
					}
				}
			}
		}

		// Filling dictionary free numbers display id
		ItemDisplayInfoEntry displayInfo;
		List<DisplayInfo>.Enumerator iconListEnumerator = iconsWithDisplayID.GetEnumerator();
		for(uint inx = 1; inx < 500000; inx++)
		{
			displayInfo = tableWoMItemDisplayInfo.GetRecord(inx);
			if(displayInfo.ID <= 0)
			{
				if(!iconListEnumerator.MoveNext())
				{
					break;
				}
				iconListEnumerator.Current.id = inx;

				ItemDisplayInfoEntry newDisplayInfo = new ItemDisplayInfoEntry();
				newDisplayInfo.ID = inx;
				newDisplayInfo.InventoryIcon = iconListEnumerator.Current.icon;
				newDisplayInfo.ModelMesh = iconListEnumerator.Current.model;
				displayIdList.Add(newDisplayInfo);
			}
		}

		// Filling ItemDisplayInfo.dbc values of the resulting dictionary.
		var itemDisplayIDEnumerator = iconsWithDisplayID.GetEnumerator();
		while(itemDisplayIDEnumerator.MoveNext())
		{
			var pair = itemDisplayIDEnumerator.Current;
			displayInfo = new ItemDisplayInfoEntry();
			displayInfo.ID = pair.id;
			displayInfo.InventoryIcon = pair.icon;
			displayInfo.ModelMesh = pair.model;
			tableWoMItemDisplayInfo.colection.Add(displayInfo);
			tableWoMItemDisplayInfo.AddKey(pair.id);
		}

		// Filling client and server Item.dbc
		ServerItemEntry itemWoW;
		ItemEntry itemWoM;
		ServerItemEntry itemFromServer;
		string sqlQueryText = "";
		foreach(CraftElement craftItem in craftItemListValues)
		{
			if( craftItem.id > 0 )
			{
				string icon = (craftItem.iconName == null) ? "": craftItem.iconName;
				string model = (craftItem.rankOrModel == null) ? "": craftItem.rankOrModel;
				ItemDisplayInfoEntry iconAndModel = displayIdList.Find(element => (element.InventoryIcon.Equals(icon)
				                                                                   && element.ModelMesh.Equals(model)));
				uint displayID = (iconAndModel == null) ? 0 : iconAndModel.ID;
				itemWoW = tableWoWItems.GetRecord((uint)craftItem.id);
				itemWoM = tableWoMItems.GetRecord(craftItem.id);
				itemWoM.Class = (int)itemWoW.classId;
				itemWoM.SubClass = (int)itemWoW.subClass;
				itemWoM.DisplayID = displayID;
				itemWoM.InventorySlot = (int)itemWoW.inventoryType;
				itemWoM.Name = craftItem.nameNew;
				if(itemWoM.ID <= 0)
				{
					itemWoM.ID = itemWoW.ID;
					tableWoMItems.colection.Add(itemWoM);
					tableWoMItems.AddKey(itemWoM.ID);
				}

				itemFromServer = tableServerItems.GetRecord((uint)craftItem.id);
				if(itemFromServer.ID > 0)
				{
					itemFromServer.displayID = displayID;
				}
				else
				{
					itemWoW.displayID = displayID;
					Debug.Log ("WoW item "+itemWoW.ID);
					tableServerItems.colection.Add(itemWoW);
					tableServerItems.AddKey(itemWoW.ID);
				}

				sqlQueryText += "UPDATE `mangos_pb`.`item_template` SET `displayid`="+displayID+" WHERE `entry`="+craftItem.id+";\n";
			}
		}
		// Converting uint values of fields to WoM values.
		foreach(ItemEntry element in tableWoMItems.colection)
		{
			if(element.ID <= 0)
				continue;
			foreach (FieldInfo field in typeof(ItemEntry).GetFields())
			{
				if(field.FieldType == typeof(uint))
				{
					field.SetValue(element, dbs.staticWOWToWOM((uint)field.GetValue(element)));
				}
			}
		}
		// Save client Items to file
		tableWoMItems.Write(pathToResultClientItemBytes);

		// Converting uint values of fields to WoM values.
		foreach(ItemDisplayInfoEntry element in tableWoMItemDisplayInfo.colection)
		{
			if(element.ID <= 0)
				continue;
			foreach (FieldInfo field in typeof(ItemDisplayInfoEntry).GetFields())
			{
				if(field.FieldType == typeof(uint))
				{
					field.SetValue(element, dbs.staticWOWToWOM((uint)field.GetValue(element)));
				}
			}
		}
		// Save ItemDisplayInfo to file
		tableWoMItemDisplayInfo.Write(pathToResultItemDisplayInfo);
		
		// Save server Items to file
		tableServerItems.Write(pathToResultServerItemDbc);
		// Save sql querry script
		File.WriteAllText(pathToResultSqlQuery, sqlQueryText);

		Debug.Log ("Ended in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
	}
}
