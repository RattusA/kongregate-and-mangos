﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public struct CraftElement
{
	public int id;
	public string nameOrigin;
	public string nameNew;
	public string iconName;
	public string rankOrModel; // model for item, and rank for spell
	public string description;
	public string tooltip;

	public CraftElement (SpellEntry spell)
	{
		CraftElement element;
		if((spell != null) && (spell.ID > 0))
		{
			element = new CraftElement((int)spell.ID,
			                           spell.SpellName[0],
			                           spell.SpellName[0],
			                           "",
			                           spell.Rank[0],
			                           spell.Description[0],
			                           spell.ToolTip[0]);
		}
		else
		{
			element = new CraftElement();
		}
		this = element;
	}

	public CraftElement(string _id, string _nameOrigin, string _nameNew, string _iconName,  string _rank, string _description, string _tooltip)
	{
		if(int.TryParse(_id, out id))
		{
			nameOrigin = _nameOrigin;
			nameNew = _nameNew;
			iconName = _iconName;
			rankOrModel = _rank;
			description = _description;
			tooltip = _tooltip;
		}
		else
		{
			id = 0;
			nameOrigin = "";
			nameNew = "";
			iconName = "";
			rankOrModel = "";
			description = "";
			tooltip = "";
		}
	}

	public CraftElement(int _id, string _nameOrigin, string _nameNew, string _iconName,  string _rank, string _description, string _tooltip)
	{
		if(_id > 0)
		{
			id = _id;
			nameOrigin = _nameOrigin;
			nameNew = _nameNew;
			iconName = _iconName;
			rankOrModel = _rank;
			description = _description;
			tooltip = _tooltip;
		}
		else
		{
			id = 0;
			nameOrigin = "";
			nameNew = "";
			iconName = "";
			rankOrModel = "";
			description = "";
			tooltip = "";
		}
	}

	public static void GetCraftingElementList(string path, ref Dictionary<int, CraftElement> craftSpellList)
	{
		if(!File.Exists(path))
		{
			throw new Exception("Not found path " + path);
		}
		var streamReader = new StreamReader(path);
		var fileContents = streamReader.ReadToEnd();
		streamReader.Close();
		string[,] csvGrid = CSVReader.SplitCsvGrid(fileContents);
		CraftElement craftSpellRow;
		int id;
		for(int inx = 0; inx < csvGrid.GetUpperBound(1); inx++)
		{
			if(int.TryParse(csvGrid[0, inx], out id))
			{
				switch(csvGrid.GetUpperBound(0))
				{
				case 3:
					craftSpellRow = new CraftElement(id, csvGrid[1, inx], csvGrid[2, inx], "", "", "", "");
					break;
				case 4:
					craftSpellRow = new CraftElement(id, csvGrid[1, inx], csvGrid[2, inx], csvGrid[3, inx], "", "", "");
					break;
				case 5:
					craftSpellRow = new CraftElement(id, csvGrid[1, inx], csvGrid[2, inx], csvGrid[3, inx], csvGrid[4, inx], "", "");
					break;
				case 6:
					craftSpellRow = new CraftElement(id, csvGrid[1, inx], csvGrid[2, inx], csvGrid[3, inx], csvGrid[4, inx], csvGrid[5, inx], "");
					break;
				case 7:
					craftSpellRow = new CraftElement(id, csvGrid[1, inx], csvGrid[2, inx], csvGrid[3, inx], csvGrid[4, inx], csvGrid[5, inx], csvGrid[6, inx]);
					break;
				default:
					if(csvGrid.GetUpperBound(0) > 7)
						craftSpellRow = new CraftElement(id, csvGrid[1, inx], csvGrid[2, inx], csvGrid[3, inx], csvGrid[4, inx], csvGrid[5, inx], csvGrid[6, inx]);
					else
						craftSpellRow = new CraftElement(id, "", "", "", "", "", "");
					break;
				}
				craftSpellList[craftSpellRow.id] = craftSpellRow;
			}
		}
	}

	public string ToString()
	{
		string ret = "";
		ret += (id + ",");
		ret += (nameOrigin + ",");
		ret += (nameNew + ",");
		ret += (iconName + ",");
		ret += (rankOrModel + ",");
		ret += (description + ",");
		ret += (tooltip);
		return ret;
	}
}

public class ItemsForCrafting : ScriptableWizard
{
	public string pathToAssets = "D://Project/Unity/WoM_Unity4/Assets";
	public string pathToItemDbc = "Resources/DBS/Item.bytes";
	public bool needShiftItemDBC = true;

	public string pathToWoWSpellDbc = "D://Project/WoWdbc/Spell.dbc";
	public bool needShiftWoWSpellDbc = false;

	public string pathToWoWItemDbc = "D://Project/WoWdbc/Item.dbc";
	public bool needShiftWoWItemDbc = false;

	public string pathToItemDisplayInfoDbc = "Resources/DBS/ItemDisplayInfo.bytes";
	public bool needShiftItemDisplayInfoDBC = true;

	public string pathToNewItemNames = "D://Project/WOM/Craft/item_templeate_sql.csv";
	public string pathToCraftingSpellsList = "D://Project/WOM/Craft/spell.csv";
	public string pathToCraftingItemList = "D://Project/WOM/Craft/item.csv";
	public string pathToItemResultFile = "D://Project/WOM/Craft/item2.csv";

	[MenuItem ("Custom Scripts/Craft/Items For Crafting")]
	static void CreateWizard ()
	{
		ScriptableWizard.DisplayWizard<ItemsForCrafting>("Items For Crafting", "Procces");
	}
	
	void OnWizardCreate () 
	{
		Debug.Log ("Started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		ItemDBS tableWoMItems;
		ItemDisplayInfoDBS tableWoMItemDisplayInfo;
		ServerItemDBS tableWoWItems;
		SpellDBS tableWoWSpells;
		Dictionary<int, CraftElement> craftSpellList = new Dictionary<int, CraftElement>();
		CraftElement.GetCraftingElementList(pathToCraftingSpellsList, ref craftSpellList);

		if(!File.Exists(pathToWoWSpellDbc))
		{
			Debug.LogError("WoW dbc file " + pathToWoWSpellDbc + " not exist");
			return;
		}

		if(!File.Exists(pathToWoWItemDbc))
		{
			Debug.LogError("WoW dbc file " + pathToWoWItemDbc + " not exist");
			return;
		}

		Debug.Log ("Loading DBC");
		// **************ItemDBC
		DBC.needShift = needShiftItemDBC;
		tableWoMItems = new ItemDBS(pathToItemDbc, pathToAssets+"/");
		if(tableWoMItems.fName == "")
		{
			Debug.LogError("ItemDBS file " + pathToItemDbc + " not exist");
			return;
		}
		
		// *************ItemDisplayInfoDBC
		DBC.needShift = needShiftItemDisplayInfoDBC;
		tableWoMItemDisplayInfo = new ItemDisplayInfoDBS(pathToItemDisplayInfoDbc, pathToAssets+"/");
		if(tableWoMItemDisplayInfo.fName == "")
		{
			Debug.LogError("ItemDisplayInfoDbc file " + pathToItemDisplayInfoDbc + " not exist");
			return;
		}

		DBC.needShift = needShiftWoWItemDbc;
		int pathEndIndex = pathToWoWItemDbc.LastIndexOf("/")+1;
		string fileName = pathToWoWItemDbc.Substring(pathEndIndex, pathToWoWItemDbc.Length-pathEndIndex);
		tableWoWItems = new ServerItemDBS(fileName, pathToWoWItemDbc.Substring(0, pathEndIndex));
		
		DBC.needShift = needShiftWoWSpellDbc;
		pathEndIndex = pathToWoWSpellDbc.LastIndexOf("/")+1;
		fileName = pathToWoWSpellDbc.Substring(pathEndIndex, pathToWoWSpellDbc.Length-pathEndIndex);
		tableWoWSpells = new SpellDBS(fileName, pathToWoWSpellDbc.Substring(0, pathEndIndex));

		Dictionary<int, CraftElement> craftItemList = new Dictionary<int, CraftElement>();
		CraftElement.GetCraftingElementList(pathToCraftingItemList, ref craftItemList);
		
		Dictionary<int, ItemEntry> newItemNames = new Dictionary<int, ItemEntry>();
		GetNewItemNames(pathToNewItemNames, ref newItemNames);

		SpellEntry spellWoW;
		ItemEntry itemWoW;
		ItemEntry itemWoM;
		ItemEntry serverItem;
		Dictionary<uint, CraftElement> itemsForSave = new Dictionary<uint, CraftElement>();
		foreach(CraftElement craftSpell in craftSpellList.Values)
		{
			spellWoW = tableWoWSpells.GetRecord(craftSpell.id);
			if(spellWoW.ID <= 0)
			{
				Debug.Log ("Unknown spell "+craftSpell.id);
				continue;
			}
			string newName;
			for(int inx = 0; inx < 8; inx++)
			{
				serverItem = null;
				if(spellWoW.Reagent[inx] > 0 && spellWoW.ReagentCount[inx] > 0)
				{
					if(newItemNames.ContainsKey((int)spellWoW.Reagent[inx]))
					{
						serverItem = newItemNames[(int)spellWoW.Reagent[inx]];
						newName = serverItem.Name;
					}
					else
					{
						serverItem = new ItemEntry();
						serverItem.ID = (uint)spellWoW.Reagent[inx];
						newName = string.Empty;
						Debug.LogError("in servr db not found item with id " + spellWoW.Reagent[inx]);
					}
					itemWoW = tableWoWItems.GetRecord(spellWoW.Reagent[inx]).ToItemEntry();
					if(itemWoW.ID > 0)
					{
						itemWoM = tableWoMItems.GetRecord(spellWoW.Reagent[inx]);
						string itemIconName = string.Empty; 
						CraftElement itemCSVRow = new CraftElement();
						if(itemWoM.ID <= 0)
						{
							var displayInfo = tableWoMItemDisplayInfo.GetRecord(serverItem.DisplayID);
							itemCSVRow = new CraftElement(itemWoW.ID.ToString(), itemWoW.Name, newName,
							                              displayInfo.InventoryIcon, displayInfo.ModelMesh, "", "");
							if(craftItemList.ContainsKey((int)itemWoW.ID))
							{
								CraftElement craftItem = craftItemList[(int)itemWoW.ID];
								itemCSVRow.nameOrigin = (string.IsNullOrEmpty(craftItem.nameOrigin)) ? itemCSVRow.nameOrigin : craftItem.nameOrigin;
								itemCSVRow.nameNew = (string.IsNullOrEmpty(craftItem.nameNew)) ? itemCSVRow.nameNew : craftItem.nameNew;
								itemCSVRow.iconName = (string.IsNullOrEmpty(craftItem.iconName)) ? itemCSVRow.iconName : craftItem.iconName;
								if(string.IsNullOrEmpty(itemCSVRow.iconName))
								{
									itemCSVRow.iconName = DefaultIcons.GetDefaultIcon(serverItem.Class, serverItem.SubClass, serverItem.InventorySlot);
								}
								itemCSVRow.rankOrModel = (string.IsNullOrEmpty(craftItem.rankOrModel)) ? itemCSVRow.rankOrModel : craftItem.rankOrModel;
							}
						}
						else
						{
							var displayInfo = tableWoMItemDisplayInfo.GetRecord(itemWoM.DisplayID);
							string iconName = (itemWoM.DisplayID > 0) ? displayInfo.InventoryIcon:"";
							string modelName = (itemWoM.DisplayID > 0) ? displayInfo.ModelMesh:"";
							itemCSVRow = new CraftElement(itemWoM.ID.ToString(), itemWoW.Name, itemWoM.Name,
							                              iconName, modelName, "", "");
							if(craftItemList.ContainsKey((int)itemWoM.ID))
							{
								CraftElement craftItem = craftItemList[(int)itemWoM.ID];
								itemCSVRow.nameOrigin = (string.IsNullOrEmpty(craftItem.nameOrigin)) ? itemCSVRow.nameOrigin : craftItem.nameOrigin;
								itemCSVRow.nameNew = (string.IsNullOrEmpty(craftItem.nameNew)) ? itemCSVRow.nameNew : craftItem.nameNew;
								itemCSVRow.nameNew = (string.IsNullOrEmpty(itemCSVRow.nameNew)) ? newName : itemCSVRow.nameNew;
								itemCSVRow.iconName = (string.IsNullOrEmpty(craftItem.iconName)) ? itemCSVRow.iconName : craftItem.iconName;
								if(string.IsNullOrEmpty(itemCSVRow.iconName))
								{
									itemCSVRow.iconName = DefaultIcons.GetDefaultIcon(serverItem.Class, serverItem.SubClass, serverItem.InventorySlot);
								}
								itemCSVRow.rankOrModel = (string.IsNullOrEmpty(craftItem.rankOrModel)) ? itemCSVRow.rankOrModel : craftItem.rankOrModel;
							}
						}
						if(itemCSVRow.id > 0)
						{
							itemsForSave[(uint)itemCSVRow.id] = itemCSVRow;
						}
					}
					else
					{
						Debug.LogWarning ("Unknown reagent " + spellWoW.Reagent[inx]);
					}
				}
			}
			for(int inx = 0; inx < 3; inx++)
			{
				serverItem = null;
				if(spellWoW.EffectItemType[inx] > 0)
				{
					if(newItemNames.ContainsKey((int)spellWoW.EffectItemType[inx]))
					{
						serverItem = newItemNames[(int)spellWoW.EffectItemType[inx]];
						newName = serverItem.Name;
					}
					else
					{
						serverItem = new ItemEntry();
						serverItem.ID = (uint)spellWoW.Reagent[inx];
						newName = string.Empty;
						Debug.LogError("in server db not found item with id " + spellWoW.EffectItemType[inx]);
					}
					itemWoW = tableWoWItems.GetRecord(spellWoW.EffectItemType[inx]).ToItemEntry();
					if(itemWoW.ID > 0)
					{
						itemWoM = tableWoMItems.GetRecord(spellWoW.EffectItemType[inx]);
						string itemIconName = string.Empty; 
						CraftElement itemCSVRow = new CraftElement();
						if(itemWoM.ID <= 0)
						{
							var displayInfo = tableWoMItemDisplayInfo.GetRecord(serverItem.DisplayID);
							itemCSVRow = new CraftElement(itemWoW.ID.ToString(), itemWoW.Name, newName,
							                              displayInfo.InventoryIcon, displayInfo.ModelMesh, "", "");
							if(craftItemList.ContainsKey((int)itemWoW.ID))
							{
								CraftElement craftItem = craftItemList[(int)itemWoW.ID];
								itemCSVRow.nameOrigin = (string.IsNullOrEmpty(craftItem.nameOrigin)) ? itemCSVRow.nameOrigin : craftItem.nameOrigin;
								itemCSVRow.nameNew = (string.IsNullOrEmpty(craftItem.nameNew)) ? itemCSVRow.nameNew : craftItem.nameNew;
								itemCSVRow.iconName = (string.IsNullOrEmpty(craftItem.iconName)) ? itemCSVRow.iconName : craftItem.iconName;
								if(string.IsNullOrEmpty(itemCSVRow.iconName))
								{
									itemCSVRow.iconName = DefaultIcons.GetDefaultIcon(serverItem.Class, serverItem.SubClass, serverItem.InventorySlot);
								}
								itemCSVRow.rankOrModel = (string.IsNullOrEmpty(craftItem.rankOrModel)) ? itemCSVRow.rankOrModel : craftItem.rankOrModel;
							}
						}
						else
						{
							var displayInfo = tableWoMItemDisplayInfo.GetRecord(itemWoM.DisplayID);
							string iconName = (itemWoM.DisplayID > 0) ? displayInfo.InventoryIcon:"";
							string modelName = (itemWoM.DisplayID > 0) ? displayInfo.ModelMesh:"";
							itemCSVRow = new CraftElement(itemWoM.ID.ToString(), itemWoW.Name, itemWoM.Name,
							                              iconName, modelName, "", "");
							if(craftItemList.ContainsKey((int)itemWoM.ID))
							{
								CraftElement craftItem = craftItemList[(int)itemWoM.ID];
								itemCSVRow.nameOrigin = (string.IsNullOrEmpty(craftItem.nameOrigin)) ? itemCSVRow.nameOrigin : craftItem.nameOrigin;
								itemCSVRow.nameNew = (string.IsNullOrEmpty(craftItem.nameNew)) ? itemCSVRow.nameNew : craftItem.nameNew;
								itemCSVRow.nameNew = (string.IsNullOrEmpty(itemCSVRow.nameNew)) ? newName : itemCSVRow.nameNew;
								itemCSVRow.iconName = (string.IsNullOrEmpty(craftItem.iconName)) ? itemCSVRow.iconName : craftItem.iconName;
								if(string.IsNullOrEmpty(itemCSVRow.iconName))
								{
									itemCSVRow.iconName = DefaultIcons.GetDefaultIcon(serverItem.Class, serverItem.SubClass, serverItem.InventorySlot);
								}
								itemCSVRow.rankOrModel = (string.IsNullOrEmpty(craftItem.rankOrModel)) ? itemCSVRow.rankOrModel : craftItem.rankOrModel;
							}
						}
						if(itemCSVRow.id > 0)
						{
							itemsForSave[(uint)itemCSVRow.id] = itemCSVRow;
						}
					}
					else
					{
						Debug.Log ("Unknown item " + spellWoW.Reagent[inx]);
					}
				}
			}
		}

		if (File.Exists(pathToItemResultFile)) 
		{
			Debug.Log(pathToItemResultFile+" already exists.");
			return;
		}
		StreamWriter source = File.CreateText(pathToItemResultFile);
		source.WriteLine ("item ID, original item name, new item name, icon name, model");
		foreach(CraftElement item in itemsForSave.Values)
		{
//			if(tableWoMItems.GetRecord(item.id).ID <= 0 || string.IsNullOrEmpty(item.iconName) || string.IsNullOrEmpty(item.nameNew))
//			{
				source.WriteLine ("{0},{1},{2},{3},{4}", item.id, item.nameOrigin, item.nameNew, item.iconName, item.rankOrModel);
//			}
		}
		source.Close();

		Debug.Log ("Ended in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
	}

	void GetNewItemNames(string path, ref Dictionary<int, ItemEntry> itemNewNames)
	{
		if(!File.Exists(path))
		{
			Debug.LogError("Not found pathToCraftingSpellsDBC " + path);
			return;
		}
		var streamReader = new StreamReader(path);
		var fileContents = streamReader.ReadToEnd();
		streamReader.Close();
		string[,] csvGrid = CSVReader.SplitCsvGrid(fileContents);
		ItemEntry itemRow;
		for(int inx = 0; inx < csvGrid.GetUpperBound(1); inx++)
		{
			itemRow = new ItemEntry();
			// ID
			if(!uint.TryParse(csvGrid[0, inx], out itemRow.ID))
			{
				itemRow.ID = 0;
			}
			// Name
			itemRow.Name = csvGrid[1, inx];
			// item class
			if(!int.TryParse(csvGrid[2, inx], out itemRow.Class))
			{
				itemRow.Class = 0;
			}
			// item subclass
			if(!int.TryParse(csvGrid[3, inx], out itemRow.SubClass))
			{
				itemRow.SubClass = 0;
			}
			// item inventory type(slot)
			if(!int.TryParse(csvGrid[5, inx], out itemRow.InventorySlot))
			{
				itemRow.InventorySlot = 0;
			}
			// item display id
			if(!uint.TryParse(csvGrid[4, inx], out itemRow.DisplayID))
			{
				itemRow.DisplayID = 0;
			}
			
			itemNewNames[(int)itemRow.ID] = itemRow;
		}
	}
}
