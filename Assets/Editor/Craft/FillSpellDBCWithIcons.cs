﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

/// <summary>
/// Fill spell DBC with icons.
/// </summary>
public class FillSpellDBCWithIcons : ScriptableWizard
{
	public string pathToAssets = "D://Project/Unity/WoM_Unity4/Assets";
	public string pathToSpellDbc = "Resources/DBS/Spell.bytes";
	public bool needShiftSpellDBC = true;
	
	public string pathToSpellIconDbc = "Resources/DBS/SpellIcon.bytes";
	public bool needShiftSpellIconDBC = true;
	
	public string pathToWoWSpellDbc = "D://Project/WoWdbc/Spell.dbc";
	public bool needShiftWoWSpellDbc = false;
	
	public string pathToCraftingSpellList = "D://Project/WOM/Craft/Tailoring spell.csv";
	
	public string pathToResultSpellIcon = "D://Project/WOM/Craft/SpellIcon.bytes";
	public string pathToResultSpellBytes = "D://Project/WOM/Craft/Spell.bytes"; // clien dbc

	[MenuItem ("Custom Scripts/Craft/Fill Spell.dbc and icons")]
	static void CreateWizard ()
	{
		ScriptableWizard.DisplayWizard<FillSpellDBCWithIcons>("Fill Spell.dbc and icons", "Procces");
	}
	
	void OnWizardCreate () 
	{
		Debug.Log ("Started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		SpellDBS tableWoMSpell;
		SpellDBS tableWoWSpell;
		SpellIconDBS tableWoMSpellIcon;
		Dictionary<int, CraftElement> craftSpellList = new Dictionary<int, CraftElement>();
		CraftElement.GetCraftingElementList(pathToCraftingSpellList, ref craftSpellList);
		
		if(!File.Exists(pathToWoWSpellDbc))
		{
			Debug.LogError("WoW item dbc file " + pathToWoWSpellDbc + " not exist");
			return;
		}
		
		Debug.Log ("Loading DBC");
		// **************SpellDBS
		DBC.needShift = needShiftSpellDBC;
		tableWoMSpell = new SpellDBS(pathToSpellDbc, pathToAssets+"/");
		if(tableWoMSpell.fName == "")
		{
			Debug.LogError("ItemDBS file " + pathToSpellDbc + " not exist");
			return;
		}
		
		// *************SpellIconDBS
		DBC.needShift = needShiftSpellIconDBC;
		tableWoMSpellIcon = new SpellIconDBS(pathToSpellIconDbc, pathToAssets+"/");
		if(tableWoMSpell.fName == "")
		{
			Debug.LogError("ItemDisplayInfoDbc file " + pathToSpellIconDbc + " not exist");
			return;
		}
		
		DBC.needShift = needShiftWoWSpellDbc;
		int pathEndIndex = pathToWoWSpellDbc.LastIndexOf("/")+1;
		string fileName = pathToWoWSpellDbc.Substring(pathEndIndex, pathToWoWSpellDbc.Length-pathEndIndex);
		tableWoWSpell= new SpellDBS(fileName, pathToWoWSpellDbc.Substring(0, pathEndIndex));
		
		// Create dictionary with  old display ID
		List<SpellIconEntry> displayIdList = new List<SpellIconEntry>();
		foreach(SpellIconEntry info in tableWoMSpellIcon.colection)
		{
			if(displayIdList.FindIndex(element => (element.Icon.Equals(info.Icon))) < 0)
			{
				displayIdList.Add (info);
			}
		}

		// Preparing the dictionary to fill the display id number
		Dictionary<string, int> iconsWithDisplayID = new Dictionary<string, int>();
		foreach(CraftElement craftSpell in craftSpellList.Values)
		{
			if(displayIdList.FindIndex(element => (element.Icon.Equals(craftSpell.iconName))) < 0)
			{
				if( !string.IsNullOrEmpty(craftSpell.iconName) )
				{
					iconsWithDisplayID[craftSpell.iconName] = 0;
				}
			}
		}
		
		// Filling dictionary free numbers display id
		SpellIconEntry displayInfo;
		List<string> iconList = new List<string>(iconsWithDisplayID.Keys);
		List<string>.Enumerator iconListEnumerator = iconList.GetEnumerator();
		for(int inx = 1; inx < 500000; inx++)
		{
			displayInfo = tableWoMSpellIcon.GetRecord(inx);
			if(displayInfo.ID <= 0)
			{
				if(!iconListEnumerator.MoveNext())
				{
					break;
				}
				string icon = iconListEnumerator.Current;
				iconsWithDisplayID[icon] = inx;

				SpellIconEntry newDisplayInfo = new SpellIconEntry();
				newDisplayInfo.ID = inx;
				newDisplayInfo.Icon = iconListEnumerator.Current;
				displayIdList.Add(newDisplayInfo);
			}
		}
		
		// Filling SpellIcon.dbc values of the resulting dictionary.
		var itemDisplayIDEnumerator = iconsWithDisplayID.GetEnumerator();
		while(itemDisplayIDEnumerator.MoveNext())
		{
			var pair = itemDisplayIDEnumerator.Current;
			displayInfo = new SpellIconEntry();
			displayInfo.ID = pair.Value;
			displayInfo.Icon = pair.Key;
			tableWoMSpellIcon.colection.Add(displayInfo);
			tableWoMSpellIcon.AddKey((uint)pair.Value);
		}

		// Filling Spell.dbc
		SpellEntry spellWoW;
		SpellEntry spellWoM;
		foreach(CraftElement craftItem in craftSpellList.Values)
		{
			if( craftItem.id > 0 )
			{
//				int displayID = (string.IsNullOrEmpty(craftItem.iconName)) ? 0 : iconsWithDisplayID[craftItem.iconName];
				spellWoW = tableWoWSpell.GetRecord(craftItem.id);
				spellWoM = tableWoMSpell.GetRecord(craftItem.id);
				string icon = (craftItem.iconName == null) ? "": craftItem.iconName;
				SpellIconEntry iconInfo = displayIdList.Find(element => (element.Icon.Equals(icon)));
				int displayID = (iconInfo == null) ? 0 : iconInfo.ID;
				if(spellWoM.ID <= 0)
				{
					spellWoM.SpellName = new LocalizedString();
					spellWoM.SpellName.Strings = new string[16];
					spellWoM.SpellName.stringOffset = new int[16]; 
					spellWoM.Rank = new LocalizedString();
					spellWoM.Rank.Strings = new string[16];
					spellWoM.Rank.stringOffset = new int[16]; 
					spellWoM.Description = new LocalizedString();
					spellWoM.Description.Strings = new string[16];
					spellWoM.Description.stringOffset = new int[16]; 
					spellWoM.ToolTip = new LocalizedString();
					spellWoM.ToolTip.Strings = new string[16];
					spellWoM.ToolTip.stringOffset = new int[16]; 
					spellWoM.SpellVisual = new Int2();
					spellWoM.SpellVisual.array = new int[2];
				}
				spellWoW.SpellIconID = displayID;
				spellWoW.SpellName.Strings[0] = craftItem.nameNew;
				spellWoW.Rank = spellWoM.Rank;
				spellWoW.Rank.Strings[0] = craftItem.rankOrModel;
				spellWoW.Description = spellWoM.Description;
				spellWoW.Description.Strings[0] = craftItem.description;
				spellWoW.ToolTip = spellWoM.ToolTip;
				spellWoW.ToolTip.Strings[0] = craftItem.tooltip;
				spellWoW.spellDescriptionVariableID = spellWoM.spellDescriptionVariableID;
				spellWoW.SpellVisual = spellWoM.SpellVisual;
				if(spellWoM.ID > 0)
				{
					foreach (FieldInfo field in typeof(SpellEntry).GetFields())
					{
						if(field.Name.Equals("SpellName"))
						{
							int x = 1;
						}
						field.SetValue(spellWoM, field.GetValue(spellWoW));
					}
				}
				else
				{
					tableWoMSpell.colection.Add(spellWoW);
					tableWoMSpell.AddKey(spellWoW.ID);
				}
			}
		}
		// Converting uint values of fields to WoM values.
		foreach(SpellEntry element in tableWoMSpell.colection)
		{
			if(element.ID <= 0)
				continue;
			foreach (FieldInfo field in typeof(SpellEntry).GetFields())
			{
				if(field.FieldType == typeof(uint))
				{
					field.SetValue(element, dbs.staticWOWToWOM((uint)field.GetValue(element)));
				}
			}
		}
		// Save spells to file
		tableWoMSpell.Write(pathToResultSpellBytes);

		
		// Converting uint values of fields to WoM values.
		foreach(SpellIconEntry element in tableWoMSpellIcon.colection)
		{
			if(element.ID <= 0)
				continue;
			foreach (FieldInfo field in typeof(SpellIconEntry).GetFields())
			{
				if(field.FieldType == typeof(uint))
				{
					field.SetValue(element, dbs.staticWOWToWOM((uint)field.GetValue(element)));
				}
			}
		}
		// Save SpellIcon to file
		tableWoMSpellIcon.Write(pathToResultSpellIcon);

		Debug.Log ("Ended in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
	}
}
