﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;

public class SyncDisplayIDServerItemDBC : ScriptableWizard
{
	public string pathToAssets = "D://Project/Unity/WoM_Unity4/Assets";
	public string pathToItemDbc = "Resources/DBS/Item.bytes";
	public bool needShiftItemDBC = true;

	public string pathToServerItemDbc = "D://Project/WOM/Craft/ItemServer.dbc";
	public bool needShiftServerItemDbc = false;

	public string pathToNewItemNames = "D://Project/WOM/Craft/item_templeate_sql.csv";
	
	// Result files
	public string pathToResultServerItemDbc = "D://Project/WOM/Craft/Item.dbc"; // server dbc
	public string pathToResultSqlQuery = "D://Project/WOM/Craft/SqlQuery.sql";

	[MenuItem ("Assets/Sync displayID for server ItemDBC")]
	static void CreateWizard ()
	{
		ScriptableWizard.DisplayWizard<SyncDisplayIDServerItemDBC>("Sync displayID for server ItemDBC", "Procces");
	}

	void OnWizardCreate () 
	{
		ItemDBS tableWoMItems;
		ServerItemDBS tableServerItems;

		Debug.Log ("Started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		Dictionary<int, QualityItemEntry> newItemNames = new Dictionary<int, QualityItemEntry>();
		GetNewItemNames(pathToNewItemNames, ref newItemNames);

		Debug.Log ("Loading DBC");
		// **************ItemDBC
		DBC.needShift = needShiftItemDBC;
		tableWoMItems = new ItemDBS(pathToItemDbc, pathToAssets+"/");
		if(tableWoMItems.fName == "")
		{
			Debug.LogError("ItemDBS file " + pathToItemDbc + " not exist");
			return;
		}

		DBC.needShift = needShiftServerItemDbc;
		int pathEndIndex = pathToServerItemDbc.LastIndexOf("/")+1;
		string fileName = pathToServerItemDbc.Substring(pathEndIndex, pathToServerItemDbc.Length-pathEndIndex);
		tableServerItems = new ServerItemDBS(fileName, pathToServerItemDbc.Substring(0, pathEndIndex));

		string sqlQueryText = "";
		var enumerator = tableWoMItems.colection.GetEnumerator();
		while(enumerator.MoveNext())
		{
			ItemEntry womItem = enumerator.Current;
			ServerItemEntry wowItem = tableServerItems.GetRecord(womItem.ID);
			if(wowItem.ID == 0)
			{
				Debug.Log ("Not found wow item with ID "+womItem.ID);
				continue;
			}
			if(womItem.DisplayID != wowItem.displayID)
			{
				QualityItemEntry sqlItem;
				if(!newItemNames.TryGetValue((int)womItem.ID, out sqlItem))
				{
					Debug.Log ("Not found sql item with ID "+womItem.ID);
					continue;
				}
				if(sqlItem.itemEntry.DisplayID != wowItem.displayID)
				{
					if(sqlItem.itemEntry.DisplayID > 0 || wowItem.displayID > 0)
					{
						wowItem.displayID = sqlItem.itemEntry.DisplayID;
					}
				}
				else
				{
					if(womItem.DisplayID > 0 || wowItem.displayID > 0)
					{
						wowItem.displayID = womItem.DisplayID;
						sqlQueryText += "UPDATE `mangos_pb`.`item_template` SET `displayid`="+womItem.DisplayID+" WHERE `entry`="+womItem.ID+";\n";
					}
				}
			}
		}
		// Save server Items to file
		tableServerItems.Write(pathToResultServerItemDbc);
		// Save sql querry script
		File.WriteAllText(pathToResultSqlQuery, sqlQueryText);

		Debug.Log ("Ended in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
	}

	void GetNewItemNames(string path, ref Dictionary<int, QualityItemEntry> itemNewNames)
	{
		if(!File.Exists(path))
		{
			Debug.LogError("Not found pathToCraftingSpellsDBC " + path);
			return;
		}
		var streamReader = new StreamReader(path);
		var fileContents = streamReader.ReadToEnd();
		streamReader.Close();
		string[,] csvGrid = CSVReader.SplitCsvGrid(fileContents);
		ItemEntry item;
		QualityItemEntry itemRow;
		for(int inx = 0; inx < csvGrid.GetUpperBound(1); inx++)
		{
			itemRow = new QualityItemEntry();
			item = new ItemEntry();
			// ID
			if(!uint.TryParse(csvGrid[0, inx], out item.ID))
			{
				item.ID = 0;
			}
			// Name
			item.Name = csvGrid[1, inx];
			// item class
			if(!int.TryParse(csvGrid[2, inx], out item.Class))
			{
				item.Class = 0;
			}
			// item subclass
			if(!int.TryParse(csvGrid[3, inx], out item.SubClass))
			{
				item.SubClass = 0;
			}
			// item inventory type(slot)
			if(!int.TryParse(csvGrid[4, inx], out item.InventorySlot))
			{
				item.InventorySlot = 0;
			}
			// item quality
			if(!int.TryParse(csvGrid[5, inx], out itemRow.quality))
			{
				itemRow.quality = 0;
			}
			// item display id
			if(!uint.TryParse(csvGrid[6, inx], out item.DisplayID))
			{
				item.DisplayID = 0;
			}
			itemRow.itemEntry = item;
			itemNewNames[(int)itemRow.itemEntry.ID] = itemRow;
		}
	}
}
