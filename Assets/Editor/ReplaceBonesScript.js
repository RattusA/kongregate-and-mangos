﻿#pragma strict
import UnityEngine;

class ReplaceBonesScript extends ScriptableObject
{
	@MenuItem ("Assets/Replace Bones Script")
	static function ReplaceScript()
	{
		var prfabs:Object[] = GetSelectedObjects() as Object[];
		Selection.objects = new UnityEngine.Object[0];
		for (var prefab:UnityEngine.Object in prfabs)
		{
			if(prefab == null)
			{
				continue;
			}
			var go:GameObject = (prefab as GameObject);
			if(go == null)
			{
				continue;
			}
			var bones:Bones[] = go.GetComponentsInChildren.<Bones>(true);
			if(bones.Length < 1)
			{
				continue;
			}
			for(var boneScript:Bones in bones)
			{
				var bonesKeys:String[] = boneScript.mBonesKey;
				var bonesValues:String[] = boneScript.mBonesValue;
				var path:String = AssetDatabase.GetAssetPath(boneScript.gameObject);
				Debug.Log("path: " + path);
				var newBoneScript:BoneList = boneScript.gameObject.AddComponent.<BoneList>();
				newBoneScript.bonesKey = bonesKeys;
				newBoneScript.bonesValue = bonesValues;
				DestroyImmediate(boneScript, true);
			}
//			TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
//			textureImporter.textureType = TextureImporterType.GUI; 
//			textureImporter.npotScale = TextureImporterNPOTScale.None;
//			TextureImporterSettings st = new TextureImporterSettings();
//			textureImporter.ReadTextureSettings(st);
//			st.wrapMode = TextureWrapMode.Clamp;
//			textureImporter.SetTextureSettings(st);
//			AssetDatabase.ImportAsset(path);
		}
	}

	static function GetSelectedObjects():Object[]
	{
		return Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets);
	}
}
