using UnityEngine;
using UnityEditor;

public class TexturePostProcessor : AssetPostprocessor
{
	private bool Enabled = false;
	
    void OnPostprocessTexture(Texture2D texture)
    {
		
		if(!Enabled){
                        return;
                }

        TextureImporter importer = (TextureImporter)assetImporter;
                importer.textureType = TextureImporterType.Image;
                importer.filterMode = FilterMode.Trilinear;
                importer.wrapMode = TextureWrapMode.Repeat;

                //compression method
                if(importer.DoesSourceTextureHaveAlpha()){
                        importer.textureFormat = TextureImporterFormat.PVRTC_RGBA4;
                }
                else{
                        importer.textureFormat = TextureImporterFormat.PVRTC_RGB4;
                }

                //setting the size
                int _size = (texture.width > texture.height)? texture.width : texture.height;
                //importer.maxTextureSize = 256;//Mathf.ClosestPowerOfTwo(_size/2);
                if(_size > 256){
                        importer.maxTextureSize = 256;
                }
                else{
                        if(_size > 64){
                                importer.maxTextureSize = 128;
                        }
                        
                }
		/*
		if(!Enabled){
			return;
		}
		
        TextureImporter importer = (TextureImporter)assetImporter;
		importer.textureType = TextureImporterType.Image;
		importer.filterMode = FilterMode.Trilinear;
		importer.wrapMode = TextureWrapMode.Repeat;
		
		//compression method
		if(importer.DoesSourceTextureHaveAlpha()){
			importer.textureFormat = TextureImporterFormat.PVRTC_RGBA4;
		}
		else{
			importer.textureFormat = TextureImporterFormat.PVRTC_RGB4;
		}
		
		//setting the size
		int _size = (texture.width > texture.height)? texture.width : texture.height;
		importer.maxTextureSize = Mathf.ClosestPowerOfTwo(_size/2);
		*/
		
		
		//set dirty asset
		Object asset = AssetDatabase.LoadAssetAtPath(importer.assetPath, typeof(Texture2D));
        if (asset){
		    EditorUtility.SetDirty(asset);
        }
		else{
			Debug.LogError("nu avem asset....");
		}
	}
	
}