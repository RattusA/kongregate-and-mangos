using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
     
public class AnimsImporter : ScriptableWizard {
        public GameObject doll;
        public TextAsset animFile;
        private ModelImporter modelImporter;
     
        [MenuItem("Tools/Import Anims")]
        static void Import()
        {
            ScriptableWizard.DisplayWizard(
                "Import anims", typeof(AnimsImporter),
                "Apply & Close");
        }
        void OnWizardCreate()
        {
            apply();   
        }
        void apply()
        {
            string path = AssetDatabase.GetAssetPath(doll);
            modelImporter = AssetImporter.GetAtPath(path) as ModelImporter;
            loadAnimationsFromText(animFile.text);
            AssetDatabase.ImportAsset(path);
        }
        
        public void loadAnimationsFromText(string animationsDescriptorFile)
        {
            char[]   delimiters = "\n".ToCharArray();
            string[] lines = animationsDescriptorFile.Split(delimiters);
           // modelImporter.splitAnimations = true;

     		ModelImporterClipAnimation[] animations = new ModelImporterClipAnimation[lines.Length];
            for(int i=0;i<lines.Length;i++)
            {
                string line = lines[i];
                AnimClip aux = parse(line);
                
                animations[i] = new  ModelImporterClipAnimation();
                if(aux.replace)
                { 
                	animations[i] = aux.anim;
                }
                else
                {
                	bool foundSomething = false;
                	foreach(ModelImporterClipAnimation clipAnim in modelImporter.clipAnimations)
                	{
                		if(clipAnim.name == aux.anim.name)
                		{
                			animations[i] = clipAnim;
                			foundSomething = true;
                			break;
                		}
                	}
                	
                	if(foundSomething == false)//anim dosent exist in clipAnimation using new anim
                	{
                		animations[i] = aux.anim;
                	}
                }
              }
            
            
            modelImporter.clipAnimations = animations;
        }
     
     	public class AnimClip
     	{
     		public ModelImporterClipAnimation anim;
     		public bool replace;
     		
     		public AnimClip()
     		{
     			anim = new ModelImporterClipAnimation();
     			replace = false;
     		}
     		
     	}
     	public AnimClip parse(string textEncoding)
        {
        	if(textEncoding.Length==0 || textEncoding == "")
        		return new AnimClip();
        		
            AnimClip newClip = new AnimClip();
           
            char[] delimiters = ",".ToCharArray();
     
            //--- Fix the tokens (two spaces makes a blank token, not what we want)
            List<string> tokens = new List<string>();
            string[] badTokens = textEncoding.Split(delimiters);
            foreach (string token in badTokens)
            {
                if (!token.Equals(string.Empty))
                    tokens.Add(token.ToString());
            }
     
            newClip.anim.name        = tokens[0];
            newClip.anim.firstFrame  = int.Parse(tokens[1]);
            newClip.anim.lastFrame   = int.Parse(tokens[2]);
            newClip.anim.wrapMode 	= (WrapMode)int.Parse(tokens[3]);           
            newClip.anim.loop        = (int.Parse(tokens[4]) >0 ? true : false);
            newClip.replace 		= true;//(int.Parse(tokens[5]) >0 ? true : false);

     		
            return newClip;
        }
    }