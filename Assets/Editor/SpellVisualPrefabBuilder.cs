﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
	
public class SpellVisualPrefabBuilder/* : MonoBehaviour*/
{
	public string pathToAssets = "";
	private string PathToSave = "Resources/prefabs/SpellEffects/SpellVisual/";
	public void CreateSpellVisual (int spellVisualNumber, string spellName, Int3 effectNames)
	{
		DirectoryInfo dirInfo = new DirectoryInfo(pathToAssets + "/" + PathToSave);
		if(!dirInfo.Exists)
		{
			dirInfo.Create();
		}
		spellName = spellName.Replace(":", "");
		GameObject go = new GameObject("" + spellVisualNumber + "-" + spellName);
		go.AddComponent<SpellVisual>();
		effectNames.array.CopyTo(go.GetComponent<SpellVisual> ().effect, 0);
		PrefabUtility.CreatePrefab (string.Format ("Assets/{0}{1}.prefab", PathToSave, go.name), go, ReplacePrefabOptions.ConnectToPrefab);
		AssetDatabase.Refresh();
		GameObject.DestroyImmediate(go);
	}
}
