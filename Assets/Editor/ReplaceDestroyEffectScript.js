﻿#pragma strict
import UnityEngine;
import UnityEditor;

class ReplaceDestroyEffectScript extends ScriptableObject
{
	@MenuItem ("Custom Scripts/OneUse/Replace destoyEffect scripts")
	static function ReplaceScript()
	{
		var guids = AssetDatabase.FindAssets("t:prefab");
		Debug.Log("Length = "+guids.Length);
		for (var guid in guids)
		{
			var path:String = AssetDatabase.GUIDToAssetPath(guid);
			var go:GameObject = AssetDatabase.LoadAssetAtPath(path, GameObject) as GameObject;
			if(go == null)
			{
				continue;
			}
			var newScript:DestroyThisTimed;
			var destroyThisTimedComponents:Component[] = go.GetComponentsInChildren(destroyThisTimed, true);
			if(destroyThisTimedComponents != null)
			{
				for(var component:destroyThisTimed in destroyThisTimedComponents)
				{
					newScript = component.gameObject.AddComponent.<DestroyThisTimed>();
					newScript.destroyTime = component.destroyTime;
					DestroyImmediate(component, true);
				}
			}
			
			var sm_destroyThisTimedComponents:Component[] = go.GetComponentsInChildren(SM_destroyThisTimed, true);
			if(destroyThisTimedComponents != null)
			{
				for(var component:SM_destroyThisTimed in sm_destroyThisTimedComponents)
				{
					newScript = component.gameObject.AddComponent.<DestroyThisTimed>();
					newScript.destroyTime = component.destroyTime;
					DestroyImmediate(component, true);
				}
			}
			
			var fgt_destroyThisTimedComponents:Component[] = go.GetComponentsInChildren(Fgt_destroyThisTimed, true);
			if(destroyThisTimedComponents != null)
			{
				for(var component:Fgt_destroyThisTimed in fgt_destroyThisTimedComponents)
				{
					newScript = component.gameObject.AddComponent.<DestroyThisTimed>();
					newScript.destroyTime = component.destroyTime;
					DestroyImmediate(component, true);
				}
			}
		}
	}
}
