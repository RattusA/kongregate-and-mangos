﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

public class SynchronizationDBC : ScriptableWizard
{
	public string pathToAssets = "D://Project/Unity/wom_android_low/Assets";
	public string pathToChangedbc = "Resources/DBS/Spell.bytes";
	public bool needShiftDBCToChange = true;
	public string pathToWoWdbc = "D://Project/WoWdbc/Spell.dbc";
	public bool needShiftWoWdbc = false;
	
	[MenuItem ("Assets/DBC/Synchronization DBC")]
	static void CreateWizard ()
	{
		ScriptableWizard.DisplayWizard<SynchronizationDBC>("Synchronization DBC", "Procces");
	}
	
	void OnWizardCreate () 
	{
		Debug.Log ("Started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		SpellDBS tableSpellsToChange;
		SpellDBS tableWoWSpells;

		if(!File.Exists(pathToWoWdbc))
		{
			Debug.LogError("WoW dbc file " + pathToWoWdbc + " not exist");
			return;
		}
		Debug.Log ("Loading DBC");
		DBC.needShift = needShiftDBCToChange;
		tableSpellsToChange = new SpellDBS(pathToAssets + "/" + pathToChangedbc);
		if(tableSpellsToChange.fName == "")
		{
			Debug.LogError("To change dbc file " + pathToChangedbc + " not exist");
			return;
		}
		DBC.needShift = needShiftWoWdbc;
		tableWoWSpells = new SpellDBS(pathToWoWdbc);
		
		//SpellEntry spellToChange;
		SpellEntry spellWoW;
		Hashtable effectDB = new Hashtable();
		List<string> noEditableFields = new List<string>();
		noEditableFields.Add("ID");
		noEditableFields.Add("SpellIconID");
		noEditableFields.Add("SpellName");
		noEditableFields.Add("Rank");
		noEditableFields.Add("Description");
		noEditableFields.Add("ToolTip");
		noEditableFields.Add("spellDescriptionVariableID");
		noEditableFields.Add("SpellVisual");
		int countSpellVisual = 0;
		foreach(SpellEntry spellToChange in tableSpellsToChange.colection)
		{
			//spellToChange = tableSpellsToChange.GetRecord(id);
			if(spellToChange.ID <= 0)
				continue;
			spellWoW = tableWoWSpells.GetRecord(spellToChange.ID);
			foreach (FieldInfo field in typeof(SpellEntry).GetFields())
            {
				if(noEditableFields.Contains(field.Name))
					continue;
				if(spellWoW.ID == 0)
					field.SetValue(spellToChange, field.GetValue(spellToChange));
				else
					field.SetValue(spellToChange, field.GetValue(spellWoW));
			}
		}
		
		foreach(SpellEntry spellToChange in tableSpellsToChange.colection)
		{
			//spellToChange = tableSpellsToChange.GetRecord(id);
			if(spellToChange.ID <= 0)
				continue;
			foreach (FieldInfo field in typeof(SpellEntry).GetFields())
			{
				if(field.FieldType == typeof(uint))
				{
					field.SetValue(spellToChange, dbs.staticWOWToWOM((uint)field.GetValue(spellToChange)));
				}
			}
			SpellEntry spellToTest = tableSpellsToChange.GetRecord(spellToChange.ID);
			Debug.Log ("");
		}
		Debug.Log ("Write DBC started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		tableSpellsToChange.Write(pathToAssets + "/" + pathToChangedbc + "1");
		Debug.Log ("Ended in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
	}
}
