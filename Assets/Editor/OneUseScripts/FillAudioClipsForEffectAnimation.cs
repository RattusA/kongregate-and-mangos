﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class FillAudioClipsForEffectAnimation : ScriptableWizard
{
	[MenuItem ("Custom Scripts/OneUse/Fill AudioClips for EffectAnimation")]
	static void CreateWizard ()
	{
		ScriptableWizard.DisplayWizard<FillAudioClipsForEffectAnimation>("FillAudioClipsForEffectAnimation", "Procces");
	}

	void OnWizardCreate () 
	{
		string[] pathToFind = new string[]{"Assets/Resources/Sounds/SoundFX"};
		char[] separator = new char[]{'/'};
		List<string> prefabList = ReadJSON("Dependencies(EffectAnimation).json.txt");
		foreach(string resourcePath in prefabList)
		{
//			string resourcePath = "Assets/Resources/prefabs/SpellEffects/12.prefab";
			if(resourcePath.Contains(".prefab"))
			{
				Object prefab = AssetDatabase.LoadAssetAtPath<Object>(resourcePath);
				GameObject go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
				List<EffectAnimation> scripts = new List<EffectAnimation>(go.GetComponents<EffectAnimation>());
				EffectAnimation[] tmp = go.GetComponentsInChildren<EffectAnimation>();
				scripts.AddRange(tmp);
				foreach(EffectAnimation script in scripts)
				{
					string[] assetGuids;
					string soundPath;
					AudioClip sound;
					// SoundName
					if(!string.IsNullOrEmpty(script.SoundName))
					{
						assetGuids = AssetDatabase.FindAssets(script.SoundName+" t:AudioClip", pathToFind);
						soundPath = string.Empty;
						foreach(string guid in assetGuids)
						{
							string tmpPath = AssetDatabase.GUIDToAssetPath(guid);
							string[] splitPath = tmpPath.Split (separator, System.StringSplitOptions.None);
							tmpPath = splitPath[splitPath.Length-1];
							tmpPath = tmpPath.Substring(0, tmpPath.LastIndexOf("."));
							if(script.SoundName.Equals(tmpPath))
							{
								soundPath = AssetDatabase.GUIDToAssetPath(guid);
								break;
							}
						}
						sound = AssetDatabase.LoadAssetAtPath<AudioClip> (soundPath);
					}
					else
					{
						sound = null;
					}
					script.audioSoundName = sound;
					// FemaleSoundName
					if(!string.IsNullOrEmpty(script.FemaleSoundName))
					{
						assetGuids = AssetDatabase.FindAssets(script.FemaleSoundName+" t:AudioClip", pathToFind);
						soundPath = string.Empty;
						foreach(string guid in assetGuids)
						{
							string tmpPath = AssetDatabase.GUIDToAssetPath(guid);
							string[] splitPath = tmpPath.Split (separator, System.StringSplitOptions.None);
							tmpPath = splitPath[splitPath.Length-1];
							tmpPath = tmpPath.Substring(0, tmpPath.LastIndexOf("."));
							if(script.FemaleSoundName.Equals(tmpPath))
							{
								soundPath = AssetDatabase.GUIDToAssetPath(guid);
								break;
							}
						}
						sound = AssetDatabase.LoadAssetAtPath<AudioClip> (soundPath);
					}
					else
					{
						sound = null;
					}
					script.audioFemaleSoundName = sound;
					// MaleSoundName
					if(!string.IsNullOrEmpty(script.MaleSoundName))
					{
						assetGuids = AssetDatabase.FindAssets(script.MaleSoundName+" t:AudioClip", pathToFind);
						soundPath = string.Empty;
						foreach(string guid in assetGuids)
						{
							string tmpPath = AssetDatabase.GUIDToAssetPath(guid);
							string[] splitPath = tmpPath.Split (separator, System.StringSplitOptions.None);
							tmpPath = splitPath[splitPath.Length-1];
							tmpPath = tmpPath.Substring(0, tmpPath.LastIndexOf("."));
							if(script.MaleSoundName.Equals(tmpPath))
							{
								soundPath = AssetDatabase.GUIDToAssetPath(guid);
								break;
							}
						}
						sound = AssetDatabase.LoadAssetAtPath<AudioClip> (soundPath);
					}
					else
					{
						sound = null;
					}
					script.audioMaleSoundName = sound;
				}
				PrefabUtility.ReplacePrefab(go, prefab);
				Object.DestroyImmediate(go, false);
			}
		}
		Debug.Log ("Finish");
	}

	private List<string> ReadJSON(string fileName)
	{
		List<string> ret = new List<string>();
		string path = Application.dataPath;
		path = path.Substring(0, path.LastIndexOf("Assets"));
		path += fileName;
		if( File.Exists(path) )
		{
			string json = File.ReadAllText(path);
			ret = (MiniJSON.jsonDecode( json ) as ArrayList).ToList<string>();
			Debug.Log ("Loaded from " + path);
		}
		else
		{
			Debug.Log ("File not found " + path);
		}
		return ret;
	}

}
