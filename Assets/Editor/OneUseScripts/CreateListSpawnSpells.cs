﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;

public class CreateListSpawnSpells : ScriptableWizard
{
	class SaveElement
	{
		public int spellID = 0;
		public string name = "";
		public Int3 spawnObjectID = new Int3();

		public string ToString()
		{
			string ret = "";
			ret += (spellID + ",");
			ret += (name + ",");
			ret += spawnObjectID[0] + ",";
			ret += spawnObjectID[1] + ",";
			ret += spawnObjectID[2];
			return ret;
		}
	}

	public string pathToAssets = "D://Project/Unity/WoM_Unity4/Assets";
	public string pathToItemDisplayInfoDbc = "Resources/DBS/ItemDisplayInfo.bytes";
	public bool needShiftItemDisplayInfoDBC = true;
	
	public string pathToWowSpell = "D://Project/WoWdbc/Spell.dbc";
	public bool needShiftWowSpell = false;

	public string pathToResultFile = "D://Project/WOM/Craft/result.csv";

	[MenuItem ("Custom Scripts/OneUse/CreateListSpawnSpells")]
	static void CreateWizard ()
	{
		ScriptableWizard.DisplayWizard<CreateListSpawnSpells>("CreateListSpawnSpells", "Procces");
	}
	
	void OnWizardCreate () 
	{
		Debug.Log ("Started in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
		
		// Save
		if (File.Exists(pathToResultFile)) 
		{
			Debug.Log(pathToResultFile+" already exists.");
			return;
		}

//		Dictionary<int, SpellTemplateDummy> newItemNames = new Dictionary<int, SpellTemplateDummy>();
//		GetNewSpellNames(pathToNewSpellNames, ref newItemNames);
		
		// *************ItemDisplayInfoDBC
//		DBC.needShift = needShiftItemDisplayInfoDBC;
//		ItemDisplayInfoDBS tableWoMItemDisplayInfo = new ItemDisplayInfoDBS(pathToItemDisplayInfoDbc, pathToAssets+"/");
//		if(tableWoMItemDisplayInfo.fName == "")
//		{
//			Debug.LogError("ItemDisplayInfoDbc file " + pathToItemDisplayInfoDbc + " not exist");
//			return;
//		}
		
		DBC.needShift = needShiftWowSpell;
		int pathEndIndex = pathToWowSpell.LastIndexOf("/")+1;
		string fileName = pathToWowSpell.Substring(pathEndIndex, pathToWowSpell.Length-pathEndIndex);
		SpellDBS wowSpellTable = new SpellDBS(fileName, pathToWowSpell.Substring(0, pathEndIndex));

		// ***********Processing
		List<SaveElement> listForSave = new List<SaveElement>();
		foreach(SpellEntry spell in wowSpellTable.colection)
		{
			int effectInx = -1;
			int spellEffect = 50;// __SpellEffects.SPELL_EFFECT_SPAWN
			if(spell.Effect[0] == spellEffect)
			{
				effectInx = 0;
			}
			else if(spell.Effect[1] == spellEffect)
			{
				effectInx = 1;
			}
			else if(spell.Effect[2] == spellEffect)
			{
				effectInx = 2;
			}
			if(effectInx > -1)
			{
				SaveElement row = new SaveElement();
				row.spellID = (int)spell.ID;
				row.name = spell.SpellName[0];
				row.spawnObjectID = spell.EffectMiscValue;
				listForSave.Add (row);
			}
		}
		StreamWriter source = File.CreateText(pathToResultFile);
		source.WriteLine ("spell ID, spell name, spawn object ID0, spawn object ID1, spawn object ID2");
		var saveSpellEnumerator = listForSave.GetEnumerator();
		while(saveSpellEnumerator.MoveNext())
		{
			source.WriteLine( ((SaveElement)saveSpellEnumerator.Current).ToString() );
		}
		source.Close();

		Debug.Log ("Ended in = " + DateTime.Now.Hour + ":" + DateTime.Now.Minute);
	}
}
