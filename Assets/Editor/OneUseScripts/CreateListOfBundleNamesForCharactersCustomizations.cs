﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class CreateListOfBundleNamesForCharactersCustomizations
{
    [MenuItem("Custom Scripts/Create List Of Bundle Names For Characters Customizations")]
    static void CreateList()
    {
        string[] assetGuids = AssetDatabase.FindAssets("t:GameObject", new string[] { "Assets/Resources/prefabs/chars" });
        List<string> assetPaths = new List<string>();
        string text = string.Empty;
        foreach (string guid in assetGuids)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);
            List<string> splitedPath = new List<string>(path.Split(new char[] { '/' }));
            if (splitedPath.Contains("Customizations") && !splitedPath.Contains("Old"))
            {
                if (!AssetDatabase.IsValidFolder(path))
                {
                    path = path.Replace("Assets/Resources/prefabs/", string.Empty);
                    path = path.Replace(".prefab", string.Empty);
                    text += "\t\t\"" + path + "\": \"" + ConvertResourceNameToBundleName(path) + "\",\n";
                }
            }
        }
        string pathToSave = Application.dataPath;
        pathToSave = pathToSave.Substring(0, pathToSave.LastIndexOf("Assets"));
        pathToSave += "Customizations.txt";
        if (!string.IsNullOrEmpty(text))
        {
            System.IO.File.WriteAllText(pathToSave, text);
        }
    }

    static string ConvertResourceNameToBundleName(string fileName)
    {
        string ret;
        ret = fileName.Replace("/", "_");
        return ret;
    }
}
