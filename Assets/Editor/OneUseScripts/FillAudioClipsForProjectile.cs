﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class FillAudioClipsForProjectile : ScriptableWizard
{
	[MenuItem ("Custom Scripts/OneUse/Fill AudioClips for Projectile")]
	static void CreateWizard ()
	{
		ScriptableWizard.DisplayWizard<FillAudioClipsForProjectile>("FillAudioClipsForProjectile", "Procces");
	}
	
	void OnWizardCreate () 
	{
		string[] pathToFind = new string[]{"Assets/Resources/Sounds/SoundFX"};
		char[] separator = new char[]{'/'};
		List<string> prefabList = ReadJSON("Dependencies(Projectile).json.txt");
		foreach(string resourcePath in prefabList)
		{
			if(resourcePath.Contains(".prefab"))
			{
				Object prefab = AssetDatabase.LoadAssetAtPath<Object>(resourcePath);
				GameObject go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
				List<Projectile> scripts = new List<Projectile>(go.GetComponents<Projectile>());
				Projectile[] tmp = go.GetComponentsInChildren<Projectile>();
				scripts.AddRange(tmp);
				foreach(Projectile script in scripts)
				{
					string[] assetGuids;
					string soundPath;
					AudioClip sound;
					// SoundEffectName
					if(!string.IsNullOrEmpty(script.SoundEffectName))
					{
						assetGuids = AssetDatabase.FindAssets(script.SoundEffectName+" t:AudioClip", pathToFind);
						soundPath = string.Empty;
						foreach(string guid in assetGuids)
						{
							string tmpPath = AssetDatabase.GUIDToAssetPath(guid);
							string[] splitPath = tmpPath.Split (separator, System.StringSplitOptions.None);
							tmpPath = splitPath[splitPath.Length-1];
							tmpPath = tmpPath.Substring(0, tmpPath.LastIndexOf("."));
							if(script.SoundEffectName.Equals(tmpPath))
							{
								soundPath = AssetDatabase.GUIDToAssetPath(guid);
								break;
							}
						}
						sound = AssetDatabase.LoadAssetAtPath<AudioClip> (soundPath);
					}
					else
					{
						sound = null;
					}
					script.audioSoundEffectName = sound;
				}
				PrefabUtility.ReplacePrefab(go, prefab);
				Object.DestroyImmediate(go, false);
			}
		}
		Debug.Log ("Finish");
	}

	private List<string> ReadJSON(string fileName)
	{
		List<string> ret = new List<string>();
		string path = Application.dataPath;
		path = path.Substring(0, path.LastIndexOf("Assets"));
		path += fileName;
		if( File.Exists(path) )
		{
			string json = File.ReadAllText(path);
			ret = (MiniJSON.jsonDecode( json ) as ArrayList).ToList<string>();
			Debug.Log ("Loaded from " + path);
		}
		else
		{
			Debug.Log ("File not found " + path);
		}
		return ret;
	}
}
