﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ItemTemplateFromSQL
{
	public Dictionary<int, QualityItemEntry> itemListFromSql;

	public ItemTemplateFromSQL (string path)
	{
		itemListFromSql = new Dictionary<int, QualityItemEntry>();
		GetNewItemNames(path, ref itemListFromSql);
	}

	void GetNewItemNames(string path, ref Dictionary<int, QualityItemEntry> itemsFromSql)
	{
		if(!File.Exists(path))
		{
			throw new Exception("Not found path for ItemTemplateFromSQL " + path);
		}
		var streamReader = new StreamReader(path);
		var fileContents = streamReader.ReadToEnd();
		streamReader.Close();
		string[,] csvGrid = CSVReader.SplitCsvGrid(fileContents);
		ItemEntry item;
		QualityItemEntry itemRow;
		for(int inx = 0; inx < csvGrid.GetUpperBound(1); inx++)
		{
			itemRow = new QualityItemEntry();
			item = new ItemEntry();
			// ID
			if(!uint.TryParse(csvGrid[0, inx], out item.ID))
			{
				item.ID = 0;
			}
			// Name
			item.Name = csvGrid[1, inx];
			// item class
			if(!int.TryParse(csvGrid[2, inx], out item.Class))
			{
				item.Class = 0;
			}
			// item subclass
			if(!int.TryParse(csvGrid[3, inx], out item.SubClass))
			{
				item.SubClass = 0;
			}
			// item inventory type(slot)
			if(!int.TryParse(csvGrid[4, inx], out item.InventorySlot))
			{
				item.InventorySlot = 0;
			}
			// item quality
			if(!int.TryParse(csvGrid[5, inx], out itemRow.quality))
			{
				itemRow.quality = 0;
			}
			// item display id
			if(!uint.TryParse(csvGrid[6, inx], out item.DisplayID))
			{
				item.DisplayID = 0;
			}
			// item requiredLevel
			if(csvGrid.GetLength(0) < 7 || !int.TryParse(csvGrid[7, inx], out itemRow.requiredLevel))
			{
				itemRow.requiredLevel = 0;
			}
			// item itemlevel
			if(csvGrid.GetLength(0) < 8 || !int.TryParse(csvGrid[8, inx], out itemRow.itemlevel))
			{
				itemRow.itemlevel = 0;
			}
			// item description
			itemRow.description = "";
			if(csvGrid.GetLength(0) > 9 && string.IsNullOrEmpty(csvGrid[9, inx]))
			{
				itemRow.description = csvGrid[9, inx];
			}

			itemRow.itemEntry = item;
			itemsFromSql[(int)itemRow.itemEntry.ID] = itemRow;
		}
	}

}
