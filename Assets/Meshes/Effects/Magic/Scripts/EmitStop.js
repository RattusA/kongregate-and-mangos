
function Start()
{
	var particleEmitter:ParticleEmitter = GetComponent(ParticleEmitter);
	// Emit particles for 2 seconds
	particleEmitter.emit = true;
	yield WaitForSeconds(2);
	// Then stop
	particleEmitter.emit = false;
}