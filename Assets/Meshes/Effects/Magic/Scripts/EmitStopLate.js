
function Start()
{
	var particleEmitter:ParticleEmitter = GetComponent(ParticleEmitter);
	// Emit particles for 3 seconds
	particleEmitter.emit = true;
	yield WaitForSeconds(3);
	// Then stop
	particleEmitter.emit = false;
}