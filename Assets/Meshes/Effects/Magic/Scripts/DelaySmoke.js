
function Start()
{
	var particleEmitter:ParticleEmitter = GetComponent(ParticleEmitter);
	// Emit particles delay of 1.05 seconds
	particleEmitter.emit = false;
	yield WaitForSeconds(1.05);
	// Then start
	particleEmitter.emit = true;
	// Then stop
	yield WaitForSeconds(0.7);
	particleEmitter.emit = false;
}