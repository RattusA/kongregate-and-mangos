
function Start()
{
	var particleEmitter:ParticleEmitter = GetComponent(ParticleEmitter);
	// Emit particles for 1 seconds
	particleEmitter.emit = true;
	yield WaitForSeconds(1);
	// Then stop
	particleEmitter.emit = false;
}