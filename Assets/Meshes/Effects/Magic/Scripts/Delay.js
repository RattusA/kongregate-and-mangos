
function Start()
{
	var particleEmitter = GetComponent.<ParticleEmitter>();
	// Emit particles delay of 0.5 seconds
	particleEmitter.emit = false;
	yield WaitForSeconds(0.5);
	// Then start
	particleEmitter.emit = true;
	// Then stop
	yield WaitForSeconds(1);
	particleEmitter.emit = false;
}