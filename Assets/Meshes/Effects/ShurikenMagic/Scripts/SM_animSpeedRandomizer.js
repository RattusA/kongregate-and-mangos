
var minSpeed:float=0.7;
var maxSpeed:float=1.5;

function Start ()
{
	var animation:Animation = GetComponent(Animation);
	animation[animation.clip.name].speed = Random.Range(minSpeed, maxSpeed);
}