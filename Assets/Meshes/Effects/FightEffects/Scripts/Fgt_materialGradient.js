var Gradient : Gradient;
var timeMultiplier:float=1;

private var curColor:Color;
private var time:float=0;



function Start ()
{
	var renderer:Renderer = GetComponent(Renderer);
	renderer.material.SetColor ("_TintColor", Color(0, 0, 0, 0));
}


function Update ()
{
	time+=Time.deltaTime*timeMultiplier;
	curColor=Gradient.Evaluate(time);
	var renderer:Renderer = GetComponent(Renderer);
	renderer.material.SetColor ("_TintColor", curColor);
}

