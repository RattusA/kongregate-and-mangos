using UnityEngine;
using System.Collections;

public class WarrierManager : MonoBehaviour {
	
	public int ibtnNum = 10;
	public int ibtnInitPosY = 50;
	
	public float hSliderValue = 0.0F;
	
	
	private int ibtnPositionX = 0;
	private int ibtnPositionY = 50;
	private int ibtnPositionSizeX = 100;
	private int ibtnPositionSizeY = 30;
	

	
	// Use this for initialization
	void Start () {

		ibtnPositionX = (int)Screen.width - 120;
			
		GetComponent<Animation>().wrapMode = WrapMode.Loop; 
		GetComponent<Animation>().Play("idle");
			
	}
	
	// Update is called once per frame
	void Update () {
		
	
		transform.eulerAngles = new Vector3(0, hSliderValue, 0);
		
	}
	
	
	void OnGUI()
	{
		
		
		
		
		if (GUI.Button(new Rect(Screen.width / 2 - (ibtnPositionSizeX * 2), Screen.height - 70, ibtnPositionSizeX, ibtnPositionSizeY), "RIGHT"))
		{
			hSliderValue += 45;
			
		}
		if (GUI.Button(new Rect(Screen.width / 2 + ibtnPositionSizeX, Screen.height - 70, ibtnPositionSizeX, ibtnPositionSizeY), "LEFT"))
		{
			
			hSliderValue -=45;
		}
		
		if (GUI.Button(new Rect(ibtnPositionX, ibtnInitPosY + (1 * ibtnPositionY), ibtnPositionSizeX, ibtnPositionSizeY), "Idle"))
		{
			GetComponent<Animation>().CrossFade("idle", 0.1f);
			GetComponent<Animation>().wrapMode = WrapMode.Loop;
	    	
		}
		
		
		if (GUI.Button(new Rect(ibtnPositionX, ibtnInitPosY + (2 * ibtnPositionY), ibtnPositionSizeX, ibtnPositionSizeY), "Walk"))
		{
			GetComponent<Animation>().CrossFade("Walk", 0.1f);
			GetComponent<Animation>().wrapMode = WrapMode.Loop;
	    	
		}

		if (GUI.Button(new Rect(ibtnPositionX, ibtnInitPosY + (3 * ibtnPositionY), ibtnPositionSizeX, ibtnPositionSizeY), "Run"))
		{
	    	GetComponent<Animation>().CrossFade("Run", 0.1f);
			GetComponent<Animation>().wrapMode = WrapMode.Loop;
		}
		
		if (GUI.Button(new Rect(ibtnPositionX, ibtnInitPosY + (4 * ibtnPositionY), ibtnPositionSizeX, ibtnPositionSizeY), "Attack"))
		{
	    	GetComponent<Animation>().CrossFade("Attack", 0.1f);
			GetComponent<Animation>().wrapMode = WrapMode.Default;
		}
		
		if (GUI.Button(new Rect(ibtnPositionX, ibtnInitPosY + (5 * ibtnPositionY), ibtnPositionSizeX, ibtnPositionSizeY), "Die1"))
		{
	    	GetComponent<Animation>().CrossFade("Die01", 0.1f);
			GetComponent<Animation>().wrapMode = WrapMode.Default;
		}
		
		if (GUI.Button(new Rect(ibtnPositionX, ibtnInitPosY + (6 * ibtnPositionY), ibtnPositionSizeX, ibtnPositionSizeY), "Die2"))
		{
	    	GetComponent<Animation>().CrossFade("Die02", 0.1f);
			GetComponent<Animation>().wrapMode = WrapMode.Default;
		}
	}

	
	
}
