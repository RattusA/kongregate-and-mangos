/*
Character control
Q = back to idle
W = walk
E = run

R = attack1
T = attack2

A = light damage on the character
S = heavy damage on the character
D = get up when downed

F = blocking while holding F

Z = jump
X = reach floor while airborne
*/

enum CharacterState
{
	GROUNDED = 1,
	AIRBORNE,
	NORMAL,
	HITTED_LIGHT,
	HITTED_HEAVY,
	DOWNED,
	COUNT_STATES
}

enum ActionState
{
	NORMAL = 100,
	BLOCKING,
	ATTACKING,
	COUNT_STATES
}

private var groundState = CharacterState.GROUNDED;
private var hitState =  CharacterState.NORMAL;
private var actionState = ActionState.NORMAL;

//private var stunTime = 1.0;

function OnGUI () {
	GUI.Label(Rect(10,280,200,100), "groundState = " + groundState);
	GUI.Label(Rect(10,300,200,100), "hitState       = " + hitState);
	GUI.Label(Rect(10,320,200,100), "actionState    = " + actionState);
}

/////////////////////////////////////////////////////////// MAIN START

function FixedUpdate () {
	
	// while the character is not in downed state
	if(hitState != CharacterState.DOWNED)
	{
		//If character got hit
		hitted();
		//getting hit take precedent whether on ground or air
		hitCharacter();
		
		//Check whether character is on ground or in the air
		if(groundState == CharacterState.GROUNDED) grounded();
		if(groundState == CharacterState.AIRBORNE) airborne();
		
		//blend in Block motion
	}
	
	//Play if character is downed
	if(hitState == CharacterState.DOWNED)
	{
		downed();
	}
}

//When the character is grounded
function grounded()
{
	//If character condition is normal
	if(hitState == CharacterState.NORMAL) normal();
	//If character condition is being hitted lightly
	else if(hitState == CharacterState.HITTED_LIGHT) hittedLight();
	//If character condition is being hitted heavily
	else if(hitState == CharacterState.HITTED_LIGHT) hittedHeavy();
	//if character is currently downed on the floor
	else if(hitState == CharacterState.DOWNED) downed();
}

function airborne()
{
	var animation:Animation = GetComponent(Animation);
	//Press X to get back to ground
	if (Input.GetKeyDown (KeyCode.X))
	{
		groundState = CharacterState.GROUNDED;
		//start playing landing animation
		animation.CrossFadeQueued("jump_03ReachGround", 0.3, QueueMode.PlayNow);
		return;
	}
	
	//Hitted while airborne -> automatically become Heavy hit
	if(hitState == CharacterState.HITTED_LIGHT || hitState == CharacterState.HITTED_HEAVY)
	{
		hittedHeavy();
		return;
	}
	
	//Play airborne animation automatically after jump motion ended
	//Adjust time to blend with aerial attack
	animation.CrossFadeQueued("jump_02Airborne", 1.0, QueueMode.CompleteOthers);
	
	//Attacking
	attack();
	
	//blocking
	//if(isBlockable()) blocking();
}

function normal()
{
	var animation:Animation = GetComponent(Animation);
	//jumping or not
	if (Input.GetKeyDown (KeyCode.Z))
	{
		//go into the air
		groundState = CharacterState.AIRBORNE;
		//Need fade time for walking and running
		animation.CrossFadeQueued("jump_01Jumping", 0.5, QueueMode.PlayNow);
		//need return or else idle animation will play
		return;
	}
	
	// Return to idle once other non loop animation ended.
	animation.CrossFadeQueued("idle", 0.3, QueueMode.CompleteOthers);

	///////////////////////////////////////MOTION
	if(actionState != ActionState.ATTACKING)
	{
		moveMotion();
	}
	
	//attacking
	attack();
	
	//blocking
	if(isBlockable()) blocking();
}

/////////////////////////////////////////////////////////// MAIN ENDED

/////////////////////////////////////////////////////////// CHARACTER GOT HITTED FUNCTION START
function hitCharacter()
{
	var animation:Animation = GetComponent(Animation);
	//If character got hit again while being lightly hitted, treat as heavy damange
	if (Input.GetKeyDown (KeyCode.A) && animation.IsPlaying("fall_light"))
	{	
		hitState = CharacterState.HITTED_HEAVY;
		return;
	}

	if (Input.GetKeyDown (KeyCode.A))
	{	
		hitState = CharacterState.HITTED_LIGHT;
		return;
	}
		
	if (Input.GetKeyDown (KeyCode.S))
	{
		hitState = CharacterState.HITTED_HEAVY;
		return;
	}
}

function hitted()
{
	if(hitState == CharacterState.HITTED_LIGHT)
	{
		hittedLight();
		return;
	}
	if(hitState == CharacterState.HITTED_HEAVY)
	{
		hittedHeavy();
		return;
	}
}

function hittedLight()
{
	var animation:Animation = GetComponent(Animation);
	animation.CrossFade("fall_light",0.3);
	//after animation ended, return state to normal
	yield WaitForSeconds (animation.clip.length);
	if(hitState == CharacterState.HITTED_LIGHT)hitState = CharacterState.NORMAL;
}

function hittedHeavy()
{
	var animation:Animation = GetComponent(Animation);
	animation.CrossFade("fall_heavy");
	hitState = CharacterState.DOWNED;
	groundState = CharacterState.GROUNDED;
}

/////////////////////////////////////////////////////////// CHARACTER GOT HITTED FUNCTION ENDED

function downed()
{
	var animation:Animation = GetComponent(Animation);
	//Play animation if getting up
	if(Input.GetKeyDown (KeyCode.D))
	{
		yield animation.CrossFadeQueued("getup", 0.3, QueueMode.PlayNow);
		hitState = CharacterState.NORMAL;
	}
}

function moveMotion()
{
	var animation:Animation = GetComponent(Animation);
	//remain still
	if (Input.GetKeyDown (KeyCode.Q)) animation.CrossFade("idle");
	//walk
	if (Input.GetKeyDown (KeyCode.W)) animation.CrossFade("walk");
	//run
	if (Input.GetKeyDown (KeyCode.E)) animation.CrossFade("run");
}

function attack()
{
	var animation:Animation = GetComponent(Animation);
	//attack motions // return to normal state after clip ended
	//attack 1
	if (Input.GetKeyDown (KeyCode.R) && actionState != ActionState.ATTACKING)
	{
		animation.CrossFadeQueued("attack_strike", 0.3, QueueMode.PlayNow);
		actionState = ActionState.ATTACKING;
		yield WaitForSeconds(animation.clip.length);
		actionState = ActionState.NORMAL;
	}
	//attack 2
	if (Input.GetKeyDown (KeyCode.T) && actionState != ActionState.ATTACKING)
	{
		animation.CrossFadeQueued("attack_thrust", 0.3, QueueMode.PlayNow);
		actionState = ActionState.ATTACKING;
		yield WaitForSeconds(animation.clip.length);
		actionState = ActionState.NORMAL;
	}
	
}

function blocking()
{
	var animation:Animation = GetComponent(Animation);
	//Lifted Key
	if(Input.GetKeyUp(KeyCode.F))
	{
		print("blocking");
		actionState = ActionState.NORMAL;
		//animation.Blend("block_02Blocking", 0.0, 0.2);
		animation.Blend("block_01Start", 0.0, 0.2);
		return;
	}
	
	//Blocking motion add
	
	//animation["block_01Start"].AddMixingTransform(shoulder);
	//animation.Blend("block_02Blocking", 1.0, 0.2);
	animation.Blend("block_01Start", 1.0, 0.2);
}

// return blockable if character is in normal state, or is already blocking
function isBlockable()
{
	// cannot block if attacking
	if(actionState == ActionState.ATTACKING) return false;
	
	//cannot block if condition is not normal
	if(hitState != CharacterState.NORMAL) return false;
	
	if(actionState == ActionState.BLOCKING) return true;
	
	if(Input.GetKey(KeyCode.F))
	{
		//print("blocking");
		actionState = ActionState.BLOCKING;
		return true;
	}
	
}