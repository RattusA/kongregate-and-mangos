/* comment here for pc
using UnityEngine;
using System.Collections;

public class Test2 : MonoBehaviour {
	public Texture BGTexture;
	
	void Start () {
		if (!GooglePlayDownloader.RunningOnAndroid()){
#if UNITY_ANDROID
			Application.LoadLevel ("login");
#endif
#if UNITY_IOS
			Application.LoadLevel("test");
#endif
		}
		string expPath = GooglePlayDownloader.GetExpansionFilePath();
		if (expPath == null){
			Debug.Log("External storage is not available!");
		}
		else{
			string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
			if (mainPath == null)   {       
				Debug.Log("fetching OBB file");
				GooglePlayDownloader.FetchOBB();
			} 
			StartCoroutine(CoroutineLoadLevel());
		}
	}
	
	void OnGUI () {
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), BGTexture);
	}
	
	protected IEnumerator CoroutineLoadLevel() {
		Debug.Log("loading level...");
		bool testResourceLoaded = false;
		while(!testResourceLoaded) { 
		   yield return new WaitForSeconds(1.5f); //Let's not constantly check, but give a buffer time to the loading
		   if(Resources.Load("prefabs/LoadingScreen") != null) {
			  testResourceLoaded = true;
		   }
		}
		Application.LoadLevel("login");
	}
}
*/ //uncomment here for pc 
