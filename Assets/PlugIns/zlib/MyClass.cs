using System.Collections;
using System;
using System.IO;
using UnityEngine;

public class MyClass{
	
public static void CopyStream(System.IO.Stream input, System.IO.Stream output){
		byte[] buffer = new byte[input.Length];
		input.Read(buffer, 0, (int)input.Length);
		
		MonoBehaviour.print("buffer size: " + buffer.Length);
		
		
		for(var _j = 0; _j < buffer.Length; _j ++ )
		{
			output.WriteByte(buffer[_j]);
		}
		output.Flush();
	}
	
	/*
	public static void decompressFile(MemoryStream inFile, string outFile){
			System.IO.FileStream outFileStream = new System.IO.FileStream(outFile, System.IO.FileMode.Create);
			ComponentAce.Compression.Libs.zlib.ZOutputStream outZStream = new ComponentAce.Compression.Libs.zlib.ZOutputStream(outFileStream);		
			try
			{
				CopyStream(inFile, outZStream);
			}
			finally
			{
				outZStream.Close();
				outFileStream.Close();
			}
		}
	*/
	
	
	public static void decompressFile(string inFile, string outFile)
		{
			System.IO.FileStream outFileStream = new System.IO.FileStream(outFile, System.IO.FileMode.Create);
			ComponentAce.Compression.Libs.zlib.ZOutputStream outZStream = new ComponentAce.Compression.Libs.zlib.ZOutputStream(outFileStream);
			System.IO.FileStream inFileStream = new System.IO.FileStream(inFile, System.IO.FileMode.Open);			
			try
			{
				CopyStream(inFileStream, outZStream);
		
			}
			finally
			{
				outZStream.Close();
				outFileStream.Close();
				inFileStream.Close();
			}
		}
	
	/*
	public static Array writeStorage(string inFile){
		System.IO.FileStream inFileStream = new System.IO.FileStream(inFile, System.IO.FileMode.Open);
		Array _arr = new Array[inFileStream.Length];
		for(int i = 0; i < inFileStream.Length; i ++){
			_arr.Push((int)inFileStream.ReadByte());
		}
		return _arr;		
	}	
	*/	
}