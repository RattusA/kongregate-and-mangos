
public class BuildConfig
{
	public static string platformName = "streamed";
	public static bool hasFreeMithrilButton = true;
	public static bool hasPurchaseMithrilButton = true;
	public static bool hasItemShopButton = true;
	public static bool hasHelpButton = true;
}
		