using UnityEngine;
using System.Runtime.InteropServices;
using System;

/*!
    MonoBehaviour to create appropriate native package (Android/iOS).
 */
public class TrialpayIntegration: MonoBehaviour {

	private static ITrialpayIntegration instance = null;
	private static string objectName;

	void Awake()
	{
		objectName = this.name;
	}

/*!
     Get the Trialpay Manager instance.
     @returns The Trialpay Manager instance.
 */
	public static ITrialpayIntegration getInstance()
	{
		if (null == instance) {
			#if UNITY_IPHONE
				instance = new IosTrialpayIntegration();
			#elif UNITY_ANDROID
				instance = new AndroidTrialpayIntegration();
			#else
				throw new Exception ("Trialpay Integration Supports Android and iOS only");
			#endif
		}
		return instance;
	}
	
/*!
    Open the Trialpay Offer Wall for a given touchpoint.
    @param vic The campaign identifier.
    @param sid The device user identifier.
    @deprecated use ITrialpayIntegration.openForTouchpoint()
*/
	public static void openOfferwall(string vic, string sid)
	{
		Debug.Log ("openOfferwall 1");
		ITrialpayIntegration trialpayIntegration = TrialpayIntegration.getInstance();
		Debug.Log ("openOfferwall 2 " + trialpayIntegration.ToString());
		trialpayIntegration.registerVic(vic, vic);
		try
		{
			Debug.Log ("trialpay isAvailable = " + trialpayIntegration.isAvailable(vic));
		}
		catch(Exception ex)
		{
			Debug.Log ("trialpay isAvailable exception");
		}
		trialpayIntegration.registerUnitySendMessage(objectName, "TrialpayMessage");
		Debug.Log ("openOfferwall 3");
		trialpayIntegration.openOfferwall(vic, sid);
		Debug.Log ("openOfferwall 4");
	}

	void OnGUI()
	{

	}

	void TrialpayMessage(string message)
	{
		Debug.Log ("TrialpayMessage = " + message);
		switch (message)
		{
		case "offerwall_open":
			// Do something when offerwall opens
			break;
		case "offerwall_close":
			// Do something when offerwall closes
			break;
		case "balance_update":
			// *** ONLY IF YOU HAVE ENABLED BALANCE API ***
			// balance = trialpayIntegration.withdrawBalance(offerwallTouchpoint); // Use the same currency name you used in registerVic
			// ... Store credit and display message ...
			break;
		}
	}
}
