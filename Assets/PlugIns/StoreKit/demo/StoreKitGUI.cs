using UnityEngine;
using System.Collections.Generic;
using Prime31;


public class StoreKitGUI : MonoBehaviourGUI
{
#if UNITY_STANDALONE_OSX || UNITY_IPHONE
	private List<StoreKitProduct> _products;


	void Start()
	{
		// you cannot make any purchases until you have retrieved the products from the server with the requestProductData method
		// we will store the products locally so that we will know what is purchaseable and when we can purchase the products
		StoreKitManager.productListReceivedEvent += allProducts =>
		{
			Debug.Log( "received total products: " + allProducts.Count );
			_products = allProducts;
		};
	}



	void OnGUI()
	{
		beginColumn();


		if( GUILayout.Button( "Get Can Make Payments" ) )
		{
			var canMakePayments = StoreKitBinding.canMakePayments();
			Debug.Log( "StoreKit canMakePayments: " + canMakePayments );
		}


		if( GUILayout.Button( "Request Product Data" ) )
		{
			// comma delimited list of product ID's from iTunesConnect.  MUST match exactly what you have there!
			var productIdentifiers = new string[] { "com.prime31.anotherProduct", "com.prime31.nonConsumable" };
			StoreKitBinding.requestProductData( productIdentifiers );
		}


		if( GUILayout.Button( "Restore Completed Transactions" ) )
		{
			StoreKitBinding.restoreCompletedTransactions();
		}


		if( GUILayout.Button( "Enable High Detail Logs" ) )
		{
			StoreKitBinding.enableHighDetailLogs( true );
		}


#if UNITY_STANDALONE_OSX
		// Mac only methods
		if( GUILayout.Button( "Validate App Store Receipt" ) )
		{
			Debug.Log( "is receipt valid: " + StoreKitBinding.validateMacAppStoreReceipt() );
		}


		if( GUILayout.Button( "Fetch In App Purchase Receipts" ) )
		{
			var receipts = StoreKitBinding.fetchInAppPurchasesFromReceipt();
			foreach( var r in receipts )
				Debug.Log( r );
		}


		if( GUILayout.Button( "Remotely Validate App Store Receipt" ) )
		{
			StartCoroutine( StoreKitManager.validateMacAppStoreReceiptRemotely( true, ht =>
			{
				if( ht != null )
					Prime31.Utils.logObject( ht );
			}));
		}
#endif


		endColumn( true );


		// enforce the fact that we can't purchase products until we retrieve the product data
		if( _products != null && _products.Count > 0 )
		{
			if( GUILayout.Button( "Purchase Random Product" ) )
			{
				var productIndex = Random.Range( 0, _products.Count );
				var product = _products[productIndex];

				Debug.Log( "preparing to purchase product: " + product.productIdentifier );
				StoreKitBinding.purchaseProduct( product.productIdentifier, 1 );
			}
		}


		if( GUILayout.Button( "Get Saved Transactions" ) )
		{
			var transactionList = StoreKitBinding.getAllSavedTransactions();

			// Print all the transactions to the console
			Debug.Log( "\ntotal transaction received: " + transactionList.Count );

			foreach( var transaction in transactionList )
				Debug.Log( transaction.ToString() + "\n" );
		}

		endColumn();
	}
#endif
}
