﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillInfo {

	public string SkillName = "";
	public string SkillValue = "0/0";
	public uint SkillCathegory = 0;
	public uint StartValue = 0;
	public uint MaxValue = 0;
}

public class PlayerSkills
{	
	private static Dictionary<uint, SkillInfo> SkillValueDictionary = new Dictionary<uint, SkillInfo>();
	
	public static void AddSkill(uint skillID, uint startValue, uint maxValue)
	{
		SkillInfo skillInfoValue = new SkillInfo();
		Hashtable currentSkill = (Hashtable)DefaultTabel.playerSkill[skillID.ToString()];
		if(currentSkill != null)
		{
			skillInfoValue.SkillName = currentSkill["Name"].ToString();
			skillInfoValue.SkillValue = startValue + "/" + maxValue;
			skillInfoValue.SkillCathegory = System.Convert.ToUInt32(currentSkill["Cathegory"]);
			skillInfoValue.StartValue = startValue;
			skillInfoValue.MaxValue = maxValue;
			
			SkillValueDictionary[skillID] = skillInfoValue;
		}
	}
	
	public static string GetSkillInfo(SkillType skillID)
	{
		string ret = "";
		SkillInfo skillInfoValue = new SkillInfo();
		SkillValueDictionary.TryGetValue((uint)skillID, out skillInfoValue);
		
		if(skillInfoValue != null)
		{
			ret = "\n<b>" + skillInfoValue.SkillName + "</b>: " + skillInfoValue.SkillValue;
		}
		return ret;
	}
	
	public static SkillInfo GetSkillInfoRecord(SkillType skillID)
	{
		SkillInfo ret = new SkillInfo();
		SkillValueDictionary.TryGetValue((uint)skillID, out ret);
		return ret;
	}
	
	public static void ClearSkillValueDictionary()
	{	
		SkillValueDictionary.Clear();
	}
}
