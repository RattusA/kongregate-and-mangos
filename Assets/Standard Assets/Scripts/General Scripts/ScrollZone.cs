﻿using UnityEngine;

public class ScrollZone
{
	private Rect statsRect;
	private string descriptionText = "";
	public bool  showZone = false;
	
	private float fontSize = 22;
	private string mainFontColor = "#000000ff";
	private GUIStyle style;
	
	float DEFAULT_WIDTH = 1024.0f;
	Vector2 scrollView;
	Vector2 startPosition = Vector2.zero;
	
	public void  SetFontSizeByRatio ( float ratio  )
	{
		if(dbs._sizeMod == 2)
		{
			fontSize = 65.0f;
		}
		else
		{
			fontSize = 22.0f;
		}
		fontSize = fontSize * ratio;
		if(dbs._sizeMod != 2)
		{
			fontSize = fontSize * (Screen.width / DEFAULT_WIDTH);
		}
	}
	
	public void  InitSkinInfo ( GUIStyle newstyle  )
	{
		style = newstyle;
	}
	
	public void  CreateScrollZoneWithText ( Rect zone ,   string text ,   string fontColor  )
	{
		statsRect = zone;
		showZone = true;
		mainFontColor = fontColor;
		descriptionText = "<size=" + (uint)fontSize + "><color=" + mainFontColor + ">" + text + "</color></size>";
	}
	
	public void  RemoveScrollZone ()
	{
		showZone = false;
	}
	
	public void  SetNewText ( string text  )
	{
		descriptionText = "<size=" + (uint)fontSize + "><color=" + mainFontColor + ">" + text + "</color></size>";
	}
	
	private bool IsTouchInTextBox ( Vector2 touchPos  )
	{
		Vector2 fixTouchPos = new Vector2(touchPos.x, Screen.height - touchPos.y);
		return statsRect.Contains(fixTouchPos);
	}
	
	public void  DrawTraitsInfo ()
	{
		Touch touch;
		bool  selected = false;
		bool  fInsideList = false;
		
		if(Input.touchCount > 0)
		{
			touch = Input.touches[0];
			fInsideList = IsTouchInTextBox(touch.position);
			
			if (touch.phase == TouchPhase.Began && fInsideList)
			{
				selected = true;
			}
			else if (touch.phase == TouchPhase.Canceled || !fInsideList)
			{
				selected = false;
			}
			else if (touch.phase == TouchPhase.Moved && fInsideList)
			{
				scrollView.y += (touch.deltaPosition.y * 2);
			}
			else if (touch.phase == TouchPhase.Ended)
			{
				selected = false;
			}
		}
		else if(Application.platform != RuntimePlatform.IPhonePlayer && Application.platform != RuntimePlatform.Android)
		{
			if(Input.GetMouseButtonDown(0) && fInsideList)
			{
				selected = true;
			}
			else if(Input.GetMouseButtonUp(0) || !fInsideList)
			{
				selected = false;
			}
			
			if(selected == true)
			{	
				scrollView.y += (Input.GetAxis("Vertical") * 2);
			}
		}
		
		GUILayout.BeginArea(statsRect);
		scrollView = GUILayout.BeginScrollView(scrollView);
		GUILayout.Label(descriptionText, style);
		GUILayout.EndScrollView();	
		GUILayout.EndArea();
	}
}