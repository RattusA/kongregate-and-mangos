﻿using UnityEngine;
using System;

public class OptionsManager
{
	private static string _playerName;
	
	public static float viewDistance;
	public static bool allDamageNumbers;
	public static bool music;
	public static bool soundFX;
	public static float musicVolume;
	public static float fxVolume;
	public static int selfPortraitTouch;
	public static int targetPortraitTouch;
	public static int partyPortraitTouch;
	public static int textFieldSize;
	public static bool showSkillsBackground;
	public static bool useVibration;
	public static bool playerFx;
	public static bool otherPlayerFx;
	public static bool showQuestArrow;
	public static float cameraSpeed;

	public static string PlayerName
	{
		get { return _playerName;}
	}

	public static float ViewDistance
	{
		get { return viewDistance; }
		set
		{
			if(viewDistance != value)
			{
				viewDistance = value;
				PlayerPrefs.SetFloat("ViewDistanceOption", viewDistance);
			}
		}
	}

	public static bool AllDamageNumbers
	{
		get { return allDamageNumbers; }
		set
		{
			if(allDamageNumbers != value)
			{
				allDamageNumbers = value;
				PlayerPrefs.SetInt("allDmgButtonOption", allDamageNumbers ? 1 : 0);
			}
		}
	}
	
	public static bool Music
	{
		get { return music; }
		set
		{
			if(music != value)
			{
				music = value;
				PlayerPrefs.SetInt("musicButtonOption", music ? 1 : 0);
			}
		}
	}
	
	public static bool SoundFX
	{
		get { return soundFX; }
		set
		{
			if(soundFX != value)
			{
				soundFX = value;
				PlayerPrefs.SetInt("soundFXButtonOption", soundFX ? 1 : 0);
			}
		}
	}
	
	public static float MusicVolume
	{
		get { return musicVolume; }
		set
		{
			if(musicVolume != value)
			{
				musicVolume = value;
				PlayerPrefs.SetFloat("musicVolumeLevel", musicVolume);
			}
		}
	}
	
	public static float FxVolume
	{
		get { return fxVolume; }
		set
		{
			if(fxVolume != value)
			{
				fxVolume = value;
				PlayerPrefs.SetFloat("soundFXVolumeLevel", fxVolume);
			}
		}
	}
	
	public static int SelfPortraitTouch
	{
		get { return selfPortraitTouch; }
		set
		{
			if(selfPortraitTouch != value)
			{
				selfPortraitTouch = value;
				if(!string.IsNullOrEmpty(PlayerName))
				{
					PlayerPrefs.SetInt(PlayerName+"selfPortraitButton", selfPortraitTouch);
				}
			}
		}
	}
	
	public static int TargetPortraitTouch
	{
		get { return targetPortraitTouch; }
		set
		{
			if(targetPortraitTouch != value)
			{
				targetPortraitTouch = value;
				if(!string.IsNullOrEmpty(PlayerName))
				{
					PlayerPrefs.SetInt(PlayerName+"targetPortraitButton", targetPortraitTouch);
				}
			}
		}
	}
	
	public static int PartyPortraitTouch
	{
		get { return partyPortraitTouch; }
		set
		{
			if(partyPortraitTouch != value)
			{
				partyPortraitTouch = value;
				if(!string.IsNullOrEmpty(PlayerName))
				{
					PlayerPrefs.SetInt(PlayerName+"partyPortraitButton", partyPortraitTouch);
				}
			}
		}
	}
	
	public static int TextFieldSize
	{
		get { return textFieldSize; }
		set
		{
			if(textFieldSize != value)
			{
				textFieldSize = value;
				if(!string.IsNullOrEmpty(PlayerName))
				{
					PlayerPrefs.SetInt(PlayerName+"ChatSize", textFieldSize);
				}
			}
		}
	}
	
	public static bool ShowSkillsBackground
	{
		get { return showSkillsBackground; }
		set
		{
			if(showSkillsBackground != value)
			{
				showSkillsBackground = value;
				PlayerPrefs.SetInt("showWoodenBackGround", showSkillsBackground ? 1 : 0);
			}
		}
	}
	
	public static bool UseVibration
	{
		get { return useVibration; }
		set
		{
			if(useVibration != value)
			{
				useVibration = value;
				PlayerPrefs.SetInt("vibroButtonOption", useVibration ? 1 : 0);
			}
		}
	}
	
	public static bool PlayerFx
	{
		get { return playerFx; }
		set
		{
			if(playerFx != value)
			{
				playerFx = value;
				PlayerPrefs.SetInt("ShowPlayerFx", playerFx ? 1 : 0);
			}
		}
	}
	
	public static bool OtherPlayerFx
	{
		get { return otherPlayerFx; }
		set
		{
			if(otherPlayerFx != value)
			{
				otherPlayerFx = value;
				PlayerPrefs.SetInt("ShowOtherPlayerFx", otherPlayerFx ? 1 : 0);
			}
		}
	}
	
	public static bool ShowQuestArrow
	{
		get { return showQuestArrow; }
		set
		{
			if(showQuestArrow != value)
			{
				showQuestArrow = value;
				PlayerPrefs.SetInt("UseQuestArrow", showQuestArrow ? 1 : 0);
			}
		}
	}
	
	public static float CameraSpeed
	{
		get { return cameraSpeed; }
		set
		{
			if(cameraSpeed != value)
			{
				cameraSpeed = value;
				PlayerPrefs.SetFloat("CameraSpeedOption", cameraSpeed);
			}
		}
	}
	
	public static void Initialize(string playerName)
	{
		_playerName = playerName;

		ReadPrefs();
		ReadPlayerPrefs();
	}

	private static void ReadPrefs()
	{
		ViewDistance = PlayerPrefs.GetFloat("ViewDistanceOption", 0);
		MusicVolume = PlayerPrefs.GetFloat("musicVolumeLevel", 0.8f);
		FxVolume = PlayerPrefs.GetFloat("soundFXVolumeLevel", 1.0f);
		CameraSpeed = PlayerPrefs.GetFloat("CameraSpeedOption", SetDefaultCameraSpeed());

		AllDamageNumbers = Convert.ToBoolean(PlayerPrefs.GetInt("allDmgButtonOption", 0));
		Music = Convert.ToBoolean(PlayerPrefs.GetInt("musicButtonOption", 1));
		SoundFX = Convert.ToBoolean(PlayerPrefs.GetInt("soundFXButtonOption", 1));
		ShowSkillsBackground = Convert.ToBoolean(PlayerPrefs.GetInt("showWoodenBackGround", 1));
		UseVibration = Convert.ToBoolean(PlayerPrefs.GetInt("vibroButtonOption", 0));
		PlayerFx = Convert.ToBoolean(PlayerPrefs.GetInt("ShowPlayerFx", 1));
		OtherPlayerFx = Convert.ToBoolean(PlayerPrefs.GetInt("ShowOtherPlayerFx", 1));
		ShowQuestArrow = Convert.ToBoolean(PlayerPrefs.GetInt("UseQuestArrow", 1));
	}

	private static void ReadPlayerPrefs()
	{
		if(_playerName == string.Empty)
		{
			return;
		}

		SelfPortraitTouch = PlayerPrefs.GetInt(_playerName + "selfPortraitButton", 0);
		TargetPortraitTouch = PlayerPrefs.GetInt(_playerName + "targetPortraitButton", 0);
		PartyPortraitTouch = PlayerPrefs.GetInt(_playerName + "partyPortraitButton", 0);
		TextFieldSize = PlayerPrefs.GetInt(_playerName + "ChatSize", 3);
	}

	private static float SetDefaultCameraSpeed()
	{
		float returnValue = 0.0f;		
	
		if (Screen.dpi < 0.01f)
		{
			returnValue = 0.5f;
		}
		else
		{
			float diag = Mathf.Sqrt(Mathf.Pow(Screen.width, 2) + Mathf.Pow(Screen.height, 2));
			diag = diag / Screen.dpi;
			
			if(diag < 3.5f)
			{
				returnValue = 0.1f;
			}
			else if(diag < 5.5f)
			{
				returnValue = 0.4f;
			}
			else if(diag < 11.0f)
			{
				returnValue = 0.8f;
			}
			else
			{
				returnValue = 1f;
			}
		}
		return returnValue;
	}
}
