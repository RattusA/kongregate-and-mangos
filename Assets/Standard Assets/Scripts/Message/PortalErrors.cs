﻿using UnityEngine;

public enum PortalRequirementType
{
	LEVEL	=	15,
	GEAR	=	32,
}

public enum PortalConditionType
{
	EQUAL		=	0,
	OVER_EQUAL	=	1,
	LESS_EQUAL	=	2,
}

public class PortalErrors : MonoBehaviour
{
	public class PortalRequirementStruct
	{
		public byte errorType;
		public uint level;
		public PortalConditionType conditionType;
	}
	
	GUIStyle guiStyle;
	string message = "";
	
	Texture backgroundTexture;
	Texture buttonTexture;
	
	protected uint sw = (uint)Screen.width;
	protected uint sh = (uint)Screen.height;
	
	void  Awake ()
	{
		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		guiStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, Screen.height * 0.06f, 
		                                   Color.white, true, FontStyle.Normal);
		
		
		backgroundTexture = Resources.Load<Texture>("GUI/InGame/PortalError/Background");
		buttonTexture = Resources.Load<Texture>("GUI/InGame/PortalError/OkButton");
	}
	
	public void  ReadPacket ( WorldPacket packet )
	{
		PortalRequirementStruct cond_1 = new PortalRequirementStruct();
		cond_1.errorType = packet.Read();
		cond_1.level = packet.ReadReversed32bit();
		cond_1.conditionType = (PortalConditionType)packet.Read();
		if(cond_1.errorType == 0)
		{
			LoadSceneManager.LoadingStatus = LoadSceneManager.LOADING.LOADING_INPROGRESS;
			GameObject.Instantiate(Resources.Load<GameObject>("GUI/LoadingDummy1"));	
			Debug.Log("Teleport Done");
			DestroySelf();
		}
		else
		{
			message = ConditionToString(cond_1);
			WorldSession.interfaceManager.hideInterface = true;
			//WorldSession.player.isMoving = false;	//player can move again  the teleport was susccesfull;
			//WorldSession.player.isRooted = true;
			WorldSession.player.MoveStop();
		}
	}
	
	string ConditionToString ( PortalRequirementStruct condition )
	{
		string message = "";
		switch((PortalRequirementType)condition.errorType)
		{
		case PortalRequirementType.LEVEL:
			switch(condition.conditionType)
			{
			case PortalConditionType.OVER_EQUAL:
				message += "You need to be above level ";
				message += (condition.level - 1).ToString() + " to access this portal.";
				break;
			case PortalConditionType.LESS_EQUAL:
				message += "You need to be below level ";
				message += (condition.level + 1).ToString() + " to access this portal.";
				break;
			}
			break;
		case PortalRequirementType.GEAR:
			message += "Equip gear rating must be min. "+condition.level.ToString()+" to access this portal.";
			break;
		}
		return message;
	}
	
	void  OnGUI ()
	{
		if(message.Length > 0)
		{
			ErrorMessage();
		}
	}
	
	void  ErrorMessage ()
	{
		GUI.DrawTexture( new Rect(sw * 0.25f, sh * 0.3f, sw * 0.5f, sh * 0.4f), backgroundTexture);
		GUI.Label( new Rect(sw * 0.28f, sh * 0.33f, sw * 0.44f, sh * 0.2f), message, guiStyle);
		
		if(GUI.Button( new Rect(sw * 0.41f, sh * 0.55f, sw * 0.18f, sh * 0.13f), buttonTexture, guiStyle))
		{
			WorldSession.interfaceManager.hideInterface = false;
			//WorldSession.player.isRooted = false;
			DestroySelf();
		}
	}
	
	void  DestroySelf ()
	{
		GameObject.DestroyImmediate(gameObject);
		//		Resources.UnloadUnusedAssets();
	}
}