using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public enum ChatNotify
{
	CHAT_JOINED_NOTICE                = 0x00,               //+ "%s joined channel.";
	CHAT_LEFT_NOTICE                  = 0x01,               //+ "%s left channel.";
	//CHAT_SUSPENDED_NOTICE             = 0x01,             // "%s left channel.";
	CHAT_YOU_JOINED_NOTICE            = 0x02,               //+ "Joined Channel: [%s]"; -- You joined
	//CHAT_YOU_CHANGED_NOTICE           = 0x02,             // "Changed Channel: [%s]";
	CHAT_YOU_LEFT_NOTICE              = 0x03,               //+ "Left Channel: [%s]"; -- You left
	CHAT_WRONG_PASSWORD_NOTICE        = 0x04,               //+ "Wrong password for %s.";
	CHAT_NOT_MEMBER_NOTICE            = 0x05,               //+ "Not on channel %s.";
	CHAT_NOT_MODERATOR_NOTICE         = 0x06,               //+ "Not a moderator of %s.";
	CHAT_PASSWORD_CHANGED_NOTICE      = 0x07,               //+ "[%s] Password changed by %s.";
	CHAT_OWNER_CHANGED_NOTICE         = 0x08,               //+ "[%s] Owner changed to %s.";
	CHAT_PLAYER_NOT_FOUND_NOTICE      = 0x09,               //+ "[%s] Player %s was not found.";
	CHAT_NOT_OWNER_NOTICE             = 0x0A,               //+ "[%s] You are not the channel owner.";
	CHAT_CHANNEL_OWNER_NOTICE         = 0x0B,               //+ "[%s] Channel owner is %s.";
	CHAT_MODE_CHANGE_NOTICE           = 0x0C,               //?
	CHAT_ANNOUNCEMENTS_ON_NOTICE      = 0x0D,               //+ "[%s] Channel announcements enabled by %s.";
	CHAT_ANNOUNCEMENTS_OFF_NOTICE     = 0x0E,               //+ "[%s] Channel announcements disabled by %s.";
	CHAT_MODERATION_ON_NOTICE         = 0x0F,               //+ "[%s] Channel moderation enabled by %s.";
	CHAT_MODERATION_OFF_NOTICE        = 0x10,               //+ "[%s] Channel moderation disabled by %s.";
	CHAT_MUTED_NOTICE                 = 0x11,               //+ "[%s] You do not have permission to speak.";
	CHAT_PLAYER_KICKED_NOTICE         = 0x12,               //? "[%s] Player %s kicked by %s.";
	CHAT_BANNED_NOTICE                = 0x13,               //+ "[%s] You are banned from that channel.";
	CHAT_PLAYER_BANNED_NOTICE         = 0x14,               //? "[%s] Player %s banned by %s.";
	CHAT_PLAYER_UNBANNED_NOTICE       = 0x15,               //? "[%s] Player %s unbanned by %s.";
	CHAT_PLAYER_NOT_BANNED_NOTICE     = 0x16,               //+ "[%s] Player %s is not banned.";
	CHAT_PLAYER_ALREADY_MEMBER_NOTICE = 0x17,               //+ "[%s] Player %s is already on the channel.";
	CHAT_INVITE_NOTICE                = 0x18,               //+ "%2$s has invited you to join the channel '%1$s'.";
	CHAT_INVITE_WRONG_FACTION_NOTICE  = 0x19,               //+ "Target is in the wrong alliance for %s.";
	CHAT_WRONG_FACTION_NOTICE         = 0x1A,               //+ "Wrong alliance for %s.";
	CHAT_INVALID_NAME_NOTICE          = 0x1B,               //+ "Invalid channel name";
	CHAT_NOT_MODERATED_NOTICE         = 0x1C,               //+ "%s is not moderated";
	CHAT_PLAYER_INVITED_NOTICE        = 0x1D,               //+ "[%s] You invited %s to join the channel";
	CHAT_PLAYER_INVITE_BANNED_NOTICE  = 0x1E,               //+ "[%s] %s has been banned.";
	CHAT_THROTTLED_NOTICE             = 0x1F,               //+ "[%s] The number of messages that can be sent to this channel is limited, please wait to send another message.";
	CHAT_NOT_IN_AREA_NOTICE           = 0x20,               //+ "[%s] You are not in the correct area for this channel."; -- The user is trying to send a chat to a zone specific channel, and they're not physically in that zone.
	CHAT_NOT_IN_LFG_NOTICE            = 0x21,               //+ "[%s] You must be queued in looking for group before joining this channel."; -- The user must be in the looking for group system to join LFG chat channels.
	CHAT_VOICE_ON_NOTICE              = 0x22,               //+ "[%s] Channel voice enabled by %s.";
	CHAT_VOICE_OFF_NOTICE             = 0x23                //+ "[%s] Channel voice disabled by %s.";
	// 0x24 enable voice?
};

public class CHANNEL_DEFINES
{
	public static int MAX_CHANNELS = 15;
	public static int START_COUNTING_IDS = 5;
}

public class channel
{
	public uint id;
	public string name;
	private bool  active = false;
	
	public void  SetActiveRecursively ()
	{
		active = true;
	}

	bool GetActive ()
	{
		return active;
	}
	
	public channel ( uint ID , string s )
	{
		id = ID;
		name = s;
		//	addChanel();
	}
	//private function addChanel()
	//{
	//	channels.channels.Add(this);
	//}
	
	public string MakeString ()
	{
		string Str = name + " id: " + id;
		return Str;
	}
	
}

public class ChannelManagement
{
	public List<channel> channels = new List<channel>();
	
	int CountIDs = (int)CHANNEL_DEFINES.START_COUNTING_IDS;
	
	bool CheckIfChannelIsLocal (  string channel )
	{
		for(int i=0; i<channels.Count; i++)
			if ( channel == channels[i].name )
				return true;
		return false;
	}
	
	private int getNextID ()
	{
		//Debug.Log("COUNT: " + CountIDs);
		int i;
		int j;
		bool  tmp = false;
		
		for(i = CHANNEL_DEFINES.START_COUNTING_IDS; i<CountIDs; i++)
		{
			tmp = false;
			for(j = 0; j<channels.Count; j++)
			{
				if ( channels[j].id == i )
				{
					tmp = true;
				}
			}
			if ( !tmp )
			{
				return i;
			}
		}
		if ( CountIDs > CHANNEL_DEFINES.START_COUNTING_IDS + CHANNEL_DEFINES.MAX_CHANNELS )
		{
			throw new InvalidOperationException("You have too many channels opened.");
		}
		CountIDs++;
		return CountIDs - 1;
	}
	
	private void  LoadChannelsToPlayerPrefs (  string player_name )
	{
		string ChannelPref = player_name + "$channels";
		int NumberOfChannels;
		string StringOfChannels;
		if ( PlayerPrefs.HasKey( ChannelPref + "$count" ) )
		{
			NumberOfChannels = PlayerPrefs.GetInt( ChannelPref + "$count" );
			StringOfChannels = PlayerPrefs.GetString( ChannelPref +"$tostring" );
			
			Debug.Log("Return: " + NumberOfChannels + "   :  " + StringOfChannels);
			string[] vector= Regex.Split(StringOfChannels, " ");
			uint id;
			string name;
			if(vector.Length > 1)
			{
				for(int i= 0; i < NumberOfChannels; i++)
				{
					Debug.Log("-----------" + NumberOfChannels + "----" + vector[i] + "-------" + vector[i*2] + "------" + vector.Length);
					if(vector[i*2] != "")
					{
						id = uint.Parse(vector[i*2]);
						name = vector[i*2 + 1];
						SendJoinChannelToServer( id, name);
					}
					else
					{
						Debug.Log("SSSSSSSSSSSSSSSSSSSS");
					}
				}
			}
		}
		else
		{
			//NumberOfChannels = 3;
			//StringOfChannels = "1 general 3 trade 4 lfg";
		}
	}
	
	public void  SendJoinChannelToServer (  uint id ,   string channel )
	{
		string channelGet = UpdateKeyNameChannel( channel );
//		channel ch = getChannel(channelGet);
		WorldPacket pkt = new WorldPacket();
		//channels.LogChannels();
		pkt.SetOpcode(OpCodes.CMSG_JOIN_CHANNEL);
		pkt.Append( ByteBuffer.Reverse( id ));
		byte unk = 0;
		pkt.Append(unk);
		pkt.Append(unk);
		pkt.Append( channelGet );
		pkt.Append("");   // password
		RealmSocket.outQueue.Add(pkt);	
	}
	
	public void  LeaveAllChannels ()
	{
		for(int i=0; i<channels.Count; i++)
		{
			LeaveChannel(channels[i].name);
		}
		channels.Clear();
	}
	
	void  CheckLastChannelUpdate (  string name )
	{
		if ( MainPlayer.LastChannel != null )
		{
			channel find_channel= FindChannelById( int.Parse( MainPlayer.LastChannel.Substring(1) ) );
			if ( find_channel != null )
			{
				if ( name == find_channel.name )
				{
					MainPlayer.LastChannel = null;
				}
			}
		}
	}
	
	public void  LeaveChannel (  string name )
	{
		string channelName = UpdateKeyNameChannel( name );
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_LEAVE_CHANNEL);
		uint aux32 = 0;
		pkt.Append(aux32);
		CheckLastChannelUpdate( channelName );
		pkt.Append( channelName );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SaveChannelsToPlayerPrefs (  string player_name )
	{
		string ChannelPref = player_name + "$channels";
		string SaveStr = "";
		
		if ( PlayerPrefs.HasKey( ChannelPref + "$count" ) )
		{
			PlayerPrefs.DeleteKey( ChannelPref + "$count" );
			PlayerPrefs.DeleteKey( ChannelPref + "$tostring" );
		}
		
		for(int i=0;i<channels.Count;i++)
		{
			if ( channels[i].id > 4 )
				SaveStr = SaveStr + channels[i].id + " " + channels[i].name + " ";
		}
		//Debug.Log("S:   " + SaveStr);
		
		//if ( PlayerPrefs.HasKey( player_name + "$channels" ) )
		PlayerPrefs.SetInt( ChannelPref + "$count", channels.Count);
		PlayerPrefs.SetString( ChannelPref + "$tostring", SaveStr);
	}
	
	public void  SortChannelsById ()
	{
		channels.Sort( HigherChannel );
	}
	
	private int  HigherChannel (  channel ch1 ,   channel ch2 )
	{
		if ( ch1.id > ch2.id )
			return 1;
		else
			if ( ch1.id == ch2.id )
				return 0;
		return -1;
	}
	
	public void  InitChannels (  string name )
	{
		//Debug.Log("------------------------------NAME STRING: " + name);
		BaseChannels();
		LoadChannelsToPlayerPrefs( name );
	}
	
	void  initBaseChannels ()
	{
		// TODO: join the 3 channels
		
		//new channel(1,"general");
		//new channel(3,"trade");
		//new channel(4,"lfg");
	}
	
	void  BaseChannels ()
	{
		// TODO: join the 3 channels  
		
		SendJoinChannelToServer(1,"Zone");   //TURNED OFF AUTOJOIN for market - Sebek
		//SendJoinChannelToServer(3,"Market");
		SendJoinChannelToServer(4,"gf");
	}
	
	public channel getChannel ( string s )
	{
		if(this.channels.Count > 0)
			foreach(channel ch in this.channels)
		{
			if(ch == null) // mabe a deleted channel
				continue;
			if(ch.name == s)
				return ch;
		}
		uint nextid = (uint)getNextID();
		channel newCh = new channel(nextid, s);
		channels.Add( newCh );
		return newCh;
	}
	
	public void  DeleteChannel (  string ChannelName )
	{
		channel ch = null;
		for(int i=0;i<channels.Count;i++)
			if ( channels[i].name == ChannelName )
				ch = channels[i];
		if ( ch != null )
			channels.Remove( ch );
	}
	
	void  LogChannels ()
	{
		int i;
		Debug.Log("START");
		for(i = 0 ; i<channels.Count;i++)
		{
			if(channels[i] == null)
				continue;
			Debug.Log("C id: " + channels[i].id + "  name: " + channels[i].name);
		}
		Debug.Log("END");
	}
	
	channel FindChannelById (  int id )
	{
		for(int i= 0 ; i<channels.Count;i++)
		{
			if(channels[i] == null)
				continue;
			if ( channels[i].id == id )
				return channels[i];
		}
		return null;
	}
	
	public string UpdateKeyNameChannel (  string name )
	{
		string returnValue;
		switch ( name )
		{
		case "a":
			returnValue = "alliance";
			break;
		case "f":
			returnValue = "fury";
			break;
		default:
			returnValue = name;
			break;
		}
		return returnValue;
	}
}