﻿using UnityEngine;
using System.Threading;
using System.Collections.Generic;

public class MessageWnd 
{
	public string title = "";
	public string text = "";
	private byte returnValue = 0;/// no message have been returned
	// 1 for Yes 2 for NO
	public Rect rect = new Rect(Screen.width*0.15f,Screen.height*0.2f,Screen.width*0.7f,Screen.height*0.6f);
	public float counterValues = 0;
	public byte NumberOfButtons = 2;
	public OpCodes[] opcodes = new OpCodes[3];/// default value for 2 buttons
	
	public byte Value()
	{
		return returnValue;
	}
	public void  Yes()
	{
		returnValue = 1;
	}
	public void  No()
	{
		returnValue = 2;
	}

//	void  setValue ( byte val )
//	{
//		returnValue = val;
//	}

	public OpCodes  getOpcode ()
	{
		OpCodes ret = OpCodes.MSG_NULL_ACTION;
		if(returnValue < opcodes.Length)
		{
			ret = opcodes[returnValue];
		}
		return ret;
	}
	public void  resetValues ()
	{
		returnValue = 0;
		rect = new Rect(Screen.width*0.15f,Screen.height*0.2f,Screen.width*0.7f,Screen.height*0.6f);
		counterValues = 0;
		NumberOfButtons = 2;
		OnGUI = null;
	}

//	void  resetRect ()
//	{
//		rect = new Rect(Screen.width*0.15f,Screen.height*0.2f,Screen.width*0.7f,Screen.height*0.6f);
//	}
	public System.Action<MessageWnd> OnGUI = null;
}


public class WaitingMessagesManager
{
	List<NextMessage> WaitingMessages = new List<NextMessage>();
	
	public void  PushMessageToWaitingList (  WorldPacket pkt, ulong source_guid, Message ms, ChatMsg type, uint lang, string source_name )
	{
		NextMessage msg = new NextMessage(pkt, source_guid, ms, type, lang, source_name);
		WaitingMessages.Add(msg);
	}
	
//	void  LogAllWaitingMessages ()
//	{
//		Debug.Log("LOG");
//		for( var i=0;i<WaitingMessages.Count;i++)
//			Debug.Log("Msg: " + WaitingMessages[i].source_guid + "  msg: " + WaitingMessages[i].ms.message);
//	}
	
	public NextMessage  PopMessageFromWaitingList (  ulong source_guid )
	{
		NextMessage Msg = FindWaitingMessage( source_guid );
		if ( Msg != null )
		{
			WaitingMessages.Remove( Msg );
		}
		return Msg;
	}
	
	private NextMessage FindWaitingMessage (  ulong source_guid )
	{
		for( var i=0;i<WaitingMessages.Count;i++)
		{
			if ( WaitingMessages[i].source_guid == source_guid )
			{
				return WaitingMessages[i];
			}
		}
		return null;
	}
}

public class NextMessage
{
	//pkt : WorldPacket, ms : message, type : byte, lang : uint, source_name : string
	public WorldPacket pkt;
	public Message ms;
	public ChatMsg type;
	public uint lang;
	string source_name;
	public ulong source_guid;
	public NextMessage (WorldPacket pkt, ulong sourceGuid, Message ms, ChatMsg type, uint lang, string source_name)
	{
		this.pkt = pkt;
		this.ms = ms;
		this.type = type;
		this.lang = lang;
		this.source_name = source_name;
		this.source_guid = sourceGuid;
	}
}


public class Message
{
	public Color messageColor = new Color(1,1,1); //-Sebek
	public string message;
	public string sender;
	public string channel = "";
	ulong senderGuid;
	public ChatMsg type;

	public Message ()
	{
	}

	public Message (string sender, string channel, string msg)
	{
		this.sender = sender;
		this.channel = channel;
		this.message = msg;
	}

	public string  getAllText ()
	{	
		if ( sender == "Guild") 
			return sender + ": " + message;
		if ( sender == "Debug")
			return message;
		if(sender=="") 
			return "World: "+message;
		if(channel=="")
			channel="World";
		switch(channel)	
		{
		case "Guild": messageColor=Color.green; break;
		case "Yell": messageColor=Color.red;	break;
		case "Party": messageColor=Color.cyan;	break;
		case "Whisper": messageColor=Color.magenta;	break;
		case "Battleground": messageColor=Color.red;	break;
		default: messageColor=Color.white;	break;
		}
		return  sender + ":" + channel + ": " + message;
	}

	string getText ()
	{
		if(sender=="")
			return message;
		return sender + ": "  + message;
	}

	string getText2() //-Sebek
	{
		return message;
	}
	
	public bool CheckIfItsShowable ()
	{
		if ( channel == "World" || channel == "" )
		{
			if (message != null && message.Contains( "achievement" ) )
			{
				Debug.Log("ACHIEVEMENT NEAFISAT: " + message);
				return false;
			}
			//if ( message.Contains( "GM" ) )
			//	return false;
		}
		return true;
	}
	
}