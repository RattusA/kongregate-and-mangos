using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour
{
//	public enum LEVELTYPE
//	{
//		LEVELTYPE_FIRSTTIME = 0,
//		LEVELTYPE_LOGIN 	= 1,
//		LEVELTYPE_OUTDOOR	= 2,
//		LEVELTYPE_INDOOR	= 3
//	}
//	
//	public enum LOADING
//	{
//		LOADING_IDLE		= 0,
//		LOADING_INPROGRESS	= 1,
//		LOADING_DONE		= 2
//	}
	
	AsyncOperation async;

	public static GameObject purchaseGO = null;

	void Awake ()
	{
		LoadSceneManager.LoadingStatus = LoadSceneManager.LOADING.LOADING_INPROGRESS;
		MapLoadDebug.loadingScreenStartTime = Time.realtimeSinceStartup;
		if(PlayerPrefs.GetInt("FirstTimeEnteringWorld", 0) == 1)
		{
			Debug.Log ("we are entering in the world not for the first time");
		}

		//Create Purchasing object
		if(!purchaseGO)//on logout dont create another PurchaseGO 
		{
			purchaseGO = Instantiate (Resources.Load ("prefabs/loginScene/PurchaseGO"), Vector3.zero, Quaternion.identity) as GameObject;
			purchaseGO.name = "PurchaseGO";
			DontDestroyOnLoad(purchaseGO);
		}

		GameObject loadingDummy = GameObject.Find("LoadingDummy1(Clone)");

		if(loadingDummy != null)
		{
			Object.Destroy(loadingDummy);
		}

		Object.Instantiate<GameObject>(Resources.Load<GameObject>("GUI/LoadingDummy1"));

	}
	
	IEnumerator LoadingLevel ()
	{
		Debug.Log("************* start loading async - time: " + Time.time);
		switch(LoadSceneManager.LevelType)
		{
			case LoadSceneManager.LEVELTYPE.LEVELTYPE_LOGIN:
				//yield return StartCoroutine(LoadScene("login"));
				switch(LoadSceneManager.LoginState)
				{
					case LoadSceneManager.LOGINSTATE.LOGINSTATE_LOGIN:
						yield return StartCoroutine(LoadScene("LoginWindow"));
						break;
					case LoadSceneManager.LOGINSTATE.LOGINSTATE_SELECT:
						yield return StartCoroutine(LoadScene("SelectCharacterWindow"));
						break;
				}
				
				break;
			case LoadSceneManager.LEVELTYPE.LEVELTYPE_OUTDOOR:
				yield return StartCoroutine(LoadScene("OutDoorScene"));
				break;
			case LoadSceneManager.LEVELTYPE.LEVELTYPE_INDOOR:
				yield return StartCoroutine(LoadScene("InDoorScene"));
				break;
		}	
	}
	
	IEnumerator UnloadAssets ()
	{
		Debug.Log("start unloading - time: " + Time.time);
		//dbs.LoadingStatus = LOADING.LOADING_INPROGRESS;
		yield return Resources.UnloadUnusedAssets();	
		Debug.Log("done unloading - time: " + Time.time);
		yield return StartCoroutine(LoadingLevel());
		Debug.Log("************* done loading async - time1: " + Time.time);
	}

	IEnumerator Start()
	{
		yield return StartCoroutine(UnloadAssets());
		Debug.Log("************* done loading async - time2: " + Time.time);
	}

	IEnumerator LoadScene(string sceneName)
	{
		Application.backgroundLoadingPriority = ThreadPriority.Low;
		Debug.Log ("Start loading " + sceneName + " scene");
		while( !Application.CanStreamedLevelBeLoaded(sceneName) )
		{
			yield return false;
		}
		AsyncOperation operation = Application.LoadLevelAsync(sceneName);
		do
		{
			Debug.Log("=" + sceneName + " progress =>" + operation.progress);
			yield return false;
		}
		while(!operation.isDone);
		Debug.Log ("Level "+sceneName+" is loaded. "+Time.time);
	}
}
