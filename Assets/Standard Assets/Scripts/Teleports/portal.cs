using UnityEngine;

public enum LocalTeleportType//this enum was created for this script only for a bether understanding of the code
{
	ONE_WAY_FOR_EVERYONE			= 1,//if we have one option the its a portal for everyone that send you to a comune location (mapid)
	ONE_WAY_FOR_SPECIFIC_RACE		= 6//if we have multiple gossip options (6 in our case because  we have 6 races) we send player to a his specific mapid 
	//(eg: human has mapid = 0, orc has mapid = 35, for the rest read loadZone.js)
}

public class portal : MonoBehaviour
{
	int mapId = 0;//obsolute
	Vector3 positionToSpawn;//obsolute
	float orientation = 0;//obsolute
	ulong portalGUID;//obsolute
	bool  doTeleport = false;
	public NPC npc;
	MainPlayer playerScript = null;
//	Texture2D _loadingTexture; 
	
	public void  setDoTeleport ( bool val )
	{
		doTeleport = val;
	}
	
	void  Start ()
	{
//		_loadingTexture = Resources.Load<Texture2D>("LoadingScreen/LoadScreen");
	}
	
	void  DoTeleport ()
	{
		playerScript = WorldSession.player;
		if(playerScript == null || npc.gossip == null)
		{
			return;
		}
		
		if(npc.gossip.menuCount == 0) //if gossip is not ready we wait (we didn't recive SMSG_GOSSIP_MESSAGE Opcode) . Or we recive 0 MenuItems for this gossip 
		{
			return;
		}
		
		LocalTeleportType teleportType = (LocalTeleportType)npc.gossip.menuCount; //gets number of teleport options from gossip. 
		int selectedOption = 0;
		
		if(teleportType == LocalTeleportType.ONE_WAY_FOR_EVERYONE)
		{
			selectedOption = 0;//Send Selected Menu Item. this menu item will run a script on server that will teleport the MainPlayer
		}
		else if(teleportType >= LocalTeleportType.ONE_WAY_FOR_SPECIFIC_RACE)
		{
			switch(playerScript.race)
			{
			case Races.RACE_HUMAN: //human
				selectedOption = 0;
				break;
			case Races.RACE_ORC: //orc
				selectedOption = 1;
				break;
			case Races.RACE_DWARF: //dwarf
				selectedOption = 2;
				break;
			case Races.RACE_TROLL: //blood drak
				selectedOption = 3;
				break;
			case Races.RACE_NIGHTELF: //elf
				selectedOption = 4;
				break;
			case Races.RACE_BLOODELF: //dark elf
				selectedOption = 5;								
				break;												 						 						 
			}
		}
		
		if((playerScript.m_deathState == DeathState.DEAD))//if we are dead we can teleport only to instance we died or to any normap map
		{
			//		Debug.Log("Esti mort si te teleportez daca am eu chef!   "+npc.gossip.m_MenuItems[selectedOption].m_BoxMessage);
			
			//the mapid that this gossip_script is using is writen in "box_text" from table "mangos.gossip_menu_option" table
			string message = npc.gossip.GetMenuItemMessageBox(selectedOption);
			string mapid;
			if(message.Length > 0 && message.Contains("i"))//if message start with a "i" character, then is a instance map id and we need to resurrect player after teleportation
			{
				
				mapid = message.Substring(1, message.Length-1);//after first character is the map id itself
				Debug.Log("INSTANCE"+ mapid);
				//compare the mapid with player corpse location. 
				if(mapid == ""+MainPlayer.corpseLocation.mapid /*&& npc.IsTriggeredReviveAltar() */)
				{  
					SendAreaTrigger(playerScript.enteringAnAreaTrigger);
					Debug.Log("JackPot!!!!!"+playerScript.enteringAnAreaTrigger);
				}
				else
				{
					Debug.Log("ghinion frateeee! chiar imi pare rau.. not! "+MainPlayer.corpseLocation.mapid);
				}
			}
			else if(message.Contains("n"))//else is a normal map id and we can teleport in ghost mode
			{
				mapid = message.Substring(1, message.Length-1);
				Debug.Log("NORMAL Hai, poti sa treci! Fie!......!"+mapid);
				if(mapid == ""+MainPlayer.corpseLocation.mapid /*&& npc.IsTriggeredReviveAltar() */ )
				{
					Debug.Log("JackPot!!!!!");
					SendAreaTrigger(playerScript.enteringAnAreaTrigger);
					//npc.gossip.SendGossipSelectedOption(selectedOption);
				}
				else
				{
					Debug.Log("Tzeapaaaaaaaa! muhahahahaaha!");
				}
			}
		}
		else //we are alive or npc has no revivealtar flag
		{	
			Debug.Log("Ai toate actele in regula."+ selectedOption + " time: " + Time.time);
			npc.gossip.SendGossipSelectedOption(selectedOption);
		}
		
		playerScript.isMoving = true;	//player can move again  the teleport was susccesfull;
		playerScript.isRooted = false;
		
		//after we set the location to teleport to, we make sure it will not enter in this function
		playerScript = null; //delete MainPlayer ref
		if(npc != null)
			npc.gossip = null;	//delete gossip on trigger exit. he will be recreated on trigger enter
		setDoTeleport(false);
	}
	
	void  SendAreaTrigger ( uint _areaid )
	{
		Debug.Log("(Int) procces "+_areaid);
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_AREATRIGGER, 4);
		pkt.Append(ByteBuffer.Reverse(_areaid));
		RealmSocket.outQueue.Add(pkt); 
		playerScript.interf.reviveWindow.reset();//reset revive window. hide all ui elements
		playerScript.StopUpdateTimerCoroutine();//stop update timer coroutine is 
	}
	
	
	void  Update ()
	{
		if(doTeleport)
		{
			DoTeleport();
		}
	}
	
	void  OnGUI ()
	{
		if(LoadSceneManager.LoadingStatus == LoadSceneManager.LOADING.LOADING_INPROGRESS)
		{
			//GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height), _loadingTexture, ScaleMode.StretchToFill);
		}
	}
	
	void  OnTriggerEnter ( Collider other )
	{
		playerScript = other.gameObject.GetComponentInChildren<MainPlayer>();// get MainPlayer referince (ref)
		
		if(!playerScript) // no MainPlayer ref
			return;
		
		playerScript.MoveStop();
		
		if(doTeleport)
			return;
		
		if(npc)
		{
			playerScript.isMoving = false;
			playerScript.isSwimming = false;
			npc.gossip = new GossipManager(npc.guid);
			npc.gossip.GetGossipMenu();
			setDoTeleport(true);
		}
	}
	
	public void  SetPortalGuid ( ulong guid )
	{
		portalGUID = guid;
	}
	
}