using UnityEngine;
using System.Collections;

public class LoadingAnimation : MonoBehaviour
{
	[SerializeField]
	private float timeBetweenImages = 0.075f;
	[SerializeField]
	private bool IsLoadingScreen = true;
	[SerializeField]
	private Object[] animLoadingTexture;
	[SerializeField]
	private Texture2D backgroundTexture;

	GameObject logInCamera = null;
	int _currentAnimatedImage;
	float timer = 0f;
	Renderer _renderer;
	
	GameObject _player;
	
	void Awake () {
		GameObject.DontDestroyOnLoad(gameObject);
//		animLoadingTexture = Resources.LoadAll("GUI/Loading", typeof(Texture));
//		backgroundTexture = (Texture2D)Resources.Load("LoadingScreen/LoadScreen");
	}
	
	// Use this for initialization
	void Start () {

		if(UID.skin == null){
			//UID.skin = (GUISkin)Resources.Load("GUI/skins");
			//UID.InitIntefaceDimensions();
		}
		//BOOYAH!!!
		if(IsLoadingScreen){
			_player = GameObject.Find("player");
			if(_player){
				_player.GetComponent<Rigidbody>().useGravity = false;
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		timer += Time.deltaTime;
		
		if(timer >= timeBetweenImages){
			_currentAnimatedImage ++;
			timer = 0;
			if(_currentAnimatedImage >= animLoadingTexture.Length){
				_currentAnimatedImage = 0;
			}
		}
	}
	
	void OnGUI ()
	{
		if(IsLoadingScreen)
		{
			if(backgroundTexture)
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height),backgroundTexture,ScaleMode.StretchToFill,true);
		}
		GUI.depth = -1;
		GUI.DrawTexture(new Rect(Screen.width/2 - 64, Screen.height - 200, 128, 128), (Texture)animLoadingTexture[_currentAnimatedImage]);
	}
	
	void LateUpdate ()
	{
		//Debug.Log(dbs.LoadingStatus + " " + IsLoadingScreen);
		if(IsLoadingScreen)
		{
			if(LoadSceneManager.LoadingStatus != LoadSceneManager.LOADING.LOADING_INPROGRESS){
				//BOOOYAH!!!!
				if(IsLoadingScreen)
				{
					//GameObject _player = GameObject.Find("player");
					if(_player)
					{
						_player.GetComponent<Rigidbody>().useGravity = true;
					}
				}
                Debug.Log("==================DESTROY LOADING DUMMY=================");
                GameObject.Destroy(gameObject);
			}
		}
	}
	
	
}
