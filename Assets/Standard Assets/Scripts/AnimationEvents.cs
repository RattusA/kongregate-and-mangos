﻿using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
	public void BuildFishingRope()
	{
		Player player = (Player)gameObject.GetComponent<MainPlayer>();
		if(player == null)
		{
			player = (Player)gameObject.GetComponent<OtherPlayer>();
		}
		SpellEntry fishingSpell = HasFishingAura(player);
		if(fishingSpell.ID > 0)
		{
			GameObject[] bobberList = GameObject.FindGameObjectsWithTag("FishingBobber");
			foreach(GameObject bobber in bobberList)
			{
				FishingFloatController floatController = bobber.GetComponentInChildren<FishingFloatController>();
				if(floatController != null)
				{
					floatController.BuildRope(player);
				}
			}
		}
	}

	private SpellEntry HasFishingAura(Player player)
	{
		SpellEntry ret = new SpellEntry();
		foreach(Aura aura in player.auraManager.auras)
		{
			SpellEntry spell = dbs.sSpells.GetRecord(aura.spellID);
			if(player.craftManager != null && player.craftManager.IsSpellRelevantCraft(spell, CraftSkill.SKILL_FISHING))
			{
				ret = spell;
				break;
			}
		}
		return ret;
	}
}
