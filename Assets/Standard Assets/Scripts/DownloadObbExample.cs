using UnityEngine;
using System.Collections;
 
public class DownloadObbExample : MonoBehaviour{
    public bool                     IsNormalBuild = false;
	private string                  expPath;
	private string                  logtxt;
	private bool                    alreadyLogged = false;
    private string                  nextScene = "LoadingScreen";
	private bool                    downloadStarted;
	 
	public Texture2D background;
	//public GUISkin mySkin;
	 
	void log( string t )
	{
		logtxt += t + "\n";
		print("MYLOG " + t);
	}

    IEnumerator SpawnLoadingDummy()
	{
		yield return GameObject.Instantiate(Resources.Load<GameObject>("GUI/LoadingDummy1"));
    }

	void Start ()
	{
        //settings for loading the next level which is LOGIN (but we go trough loading screen first)
		Debug.Log("BEFORE: ---> level type: " + LoadSceneManager.LevelType + " loading status: " + LoadSceneManager.LoadingStatus);
//		dbs.LevelType = LoadingScreen.LEVELTYPE.LEVELTYPE_LOGIN;
//		dbs.LoadingStatus = LoadingScreen.LOADING.LOADING_INPROGRESS;
		LoadSceneManager.LevelType = LoadSceneManager.LEVELTYPE.LEVELTYPE_LOGIN;
		LoadSceneManager.LoadingStatus = LoadSceneManager.LOADING.LOADING_INPROGRESS;
		LoadSceneManager.LoginState = LoadSceneManager.LOGINSTATE.LOGINSTATE_LOGIN;
//		Debug.Log("AFTER: ---> level type: " + dbs.LevelType + " loading status: " + dbs.LoadingStatus);
		Debug.Log("AFTER: ---> level type: " + LoadSceneManager.LevelType + " loading status: " + LoadSceneManager.LoadingStatus);
        //done setting the upper mentioned stuff
//		if(mySkin != null){
//			GUI.skin = mySkin;
//		}
#if UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
        StartCoroutine(SpawnLoadingDummy());
        Application.LoadLevel("LoadingScreen");
#endif
        if (IsNormalBuild)
		{
            StartCoroutine(SpawnLoadingDummy());
			//GameObject.Instantiate(Resources.Load<GameObject>("GUI/LoadingDummy1"));
			if(GameManager.Instance != null)
			{
				GameManager.Instance.LoadScene("LoadingScreen");
			}
			else
			{
            	Application.LoadLevel("LoadingScreen");
			}
        }
	}

#if UNITY_ANDROID
	void OnGUI()
	{
        if (IsNormalBuild)
		{
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), background, ScaleMode.StretchToFill);
            return;
        }
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), background, ScaleMode.StretchToFill);
		if (!GooglePlayDownloader.RunningOnAndroid())
		{
			GUI.Label(new Rect(10, 10, Screen.width-10, 20), "Use GooglePlayDownloader only on Android device!");
            StartCoroutine(SpawnLoadingDummy());
			//GameObject.Instantiate(Resources.Load<GameObject>("GUI/LoadingDummy1"));
            Application.LoadLevel(nextScene);
			return;
		}
		 
		expPath = GooglePlayDownloader.GetExpansionFilePath();
		if (expPath == null)
		{
			GUI.Label(new Rect(10, 10, Screen.width-10, 20), "External storage is not available!");
		}
		else
		{
			string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
			string patchPath = GooglePlayDownloader.GetPatchOBBPath(expPath);
			if( alreadyLogged == false && mainPath != null )
			{
				alreadyLogged = true;
				log( "expPath = " + expPath );
				log( "Main = " + mainPath );
				log( "Main = " + mainPath.Substring(expPath.Length));
				 
				StartCoroutine(loadLevel());
		 
			}
			//GUI.Label(new Rect(10, 10, Screen.width-10, Screen.height-10), logtxt );
		 
			if (mainPath == null)
			{
				GUI.Label(new Rect(Screen.width-600, Screen.height-230, 430, 60), "The game needs to download more than 200 MB of game content. It's recommanded to use WIFI connexion.");
				if (GUI.Button(new Rect(Screen.width-500, Screen.height-170, 250, 60), "Start Download !"))
				{
					GooglePlayDownloader.FetchOBB();
					StartCoroutine(loadLevel());
				}
			}		 
		}		 
	}

	protected IEnumerator loadLevel()
	{
		string mainPath;
		do
		{
			yield return new WaitForSeconds(0.5f);
			mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
			log("waiting mainPath "+mainPath);
		}
		while( mainPath == null);
		
		//Romanians load in memory entire OBB file. 
//		if( downloadStarted == false )
//		{
//			downloadStarted = true;
//		 
//			string uri = "file://" + mainPath;
//			log("downloading " + uri);
//			WWW www = WWW.LoadFromCacheOrDownload(uri , 0);
//			 
//			// Wait for download to complete
//			yield return www;
//		 
//			if (www.error != null){
//				log ("wwww error " + www.error);
//			}
//			else
//			{
//                StartCoroutine(SpawnLoadingDummy());
//                //GameObject.Instantiate(Resources.Load<GameObject>("GUI/LoadingDummy1"));
//                Application.LoadLevel(nextScene);
//			}
//		}
		Application.LoadLevelAsync(nextScene);
	}
#endif 
}
