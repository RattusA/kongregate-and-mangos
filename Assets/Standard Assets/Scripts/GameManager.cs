﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
	private static GameManager instance;
	public bool useMapLoadingDebug = false;
	public float timoutCleanResurcesInSeconds = 30f;
	private float startCleanResurces = 0f;
	private static float endCleanResurces = 0f;

	public static GameManager Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		instance = this;
	}

	// Use this for initialization
	void Start ()
	{
		DontDestroyOnLoad(this);
		// TODO GameManager Start
		if(useMapLoadingDebug)
		{
			gameObject.AddComponent<MapLoadDebug>();
		}
		endCleanResurces = Time.time;
		startCleanResurces = endCleanResurces;
	}
	
	// Update is called once per frame
	void Update ()
	{
		// TODO GameManager Update
//		if((Time.time > (endCleanResurces + timoutCleanResurcesInSeconds)) && (startCleanResurces <= endCleanResurces))
//		{
//			StartCoroutine("CleanResurces");
//			Debug.Log("Start CleanResurces in "+Time.time+" ("+Time.time+" "+startCleanResurces+" "+endCleanResurces+")");
//			startCleanResurces = endCleanResurces + timoutCleanResurcesInSeconds;
//		}
	}

	IEnumerator CleanResurces()
	{
		yield return Resources.UnloadUnusedAssets();
		endCleanResurces = Time.time;
	}

	void OnApplicationQuit()
	{
		Caching.CleanCache();
    }

	/// <summary>
	/// Loads the scene.
	/// </summary>
	/// <param name="sceneName">Scene name.</param>
	/// <param name="isAdditive">If set to <c>true</c> is additive.</param>
	public void LoadScene(string sceneName, bool isAdditive = false)
	{
		StartCoroutine(LoadSceneAsync(sceneName, isAdditive));
	}

	public IEnumerator LoadSceneAsync(string sceneName, bool isAdditive)
	{
		while( !Application.CanStreamedLevelBeLoaded(sceneName) )
		{
			yield return false;
		}
		AsyncOperation operation;
		if( !isAdditive )
		{
			operation =  Application.LoadLevelAsync(sceneName);
		}
		else
		{
			operation = Application.LoadLevelAdditiveAsync(sceneName);
		}
		do
		{
			yield return false;
		}
		while(!operation.isDone);
	}
}
