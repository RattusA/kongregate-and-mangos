﻿
public class LoadSceneManager
{
	public enum LEVELTYPE
	{
		LEVELTYPE_FIRSTTIME = 0,
		LEVELTYPE_LOGIN 	= 1,
		LEVELTYPE_OUTDOOR	= 2,
		LEVELTYPE_INDOOR	= 3
	}
	
	public enum LOADING
	{
		LOADING_IDLE		= 0,
		LOADING_INPROGRESS	= 1,
		LOADING_DONE		= 2
	}

	public enum LOGINSTATE
	{
		LOGINSTATE_LOGIN	= 0,
		LOGINSTATE_REALM	= 1,
		LOGINSTATE_SELECT	= 2,
		LOGINSTATE_CREATE	= 3
	}

	public static LEVELTYPE LevelType = LEVELTYPE.LEVELTYPE_LOGIN;
	public static LOADING LoadingStatus = LOADING.LOADING_IDLE;
	public static LOGINSTATE LoginState = LOGINSTATE.LOGINSTATE_LOGIN;
}
