﻿using UnityEngine;

public class GUIAtlas : MonoBehaviour
{
	public static TexturePacker interfMix3;
	public static TexturePacker kitchenSinkSheet;
	
	public static Texture2D blackIcon;
	public static GameObject instance;
	
	void Awake ()
	{
		if(instance)
		{
			GameObject.Destroy(gameObject);
		}
		else
		{
			interfMix3 = null;
			kitchenSinkSheet = null;
			
			instance = gameObject;
			GameObject.DontDestroyOnLoad(gameObject);
		}
	}
	
	void Start ()
	{
		interfMix3 = new TexturePacker("interf_mix3");
		kitchenSinkSheet = new TexturePacker("kitchenSinkSheet");
	}

	public static GUIStyle CreateGuiStyle ( Font font, TextAnchor textAnchor, float fontSize, Color textColor, bool wordWrap, FontStyle fontStyle )
	{
		return CreateGuiStyle ( font, textAnchor, (int)fontSize, textColor, wordWrap, fontStyle);
	}
	
	public static GUIStyle CreateGuiStyle ( Font font, TextAnchor textAnchor, int fontSize, Color textColor, bool wordWrap, FontStyle fontStyle )
	{
		GUIStyle newStyle = new GUIStyle();
		
		newStyle.font = font;
		newStyle.alignment = textAnchor;
		newStyle.fontSize = fontSize;
		newStyle.normal.textColor = textColor;
		newStyle.wordWrap = wordWrap;
		newStyle.fontStyle = fontStyle;
		
		#if UNITY_IPHONE
		newStyle.fontSize = fontSize * 0.8f;
		#endif
		return newStyle;
	}
}