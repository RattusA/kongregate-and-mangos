﻿using UnityEngine;

public class MapLoadDebug:MonoBehaviour
{
	public static float loadingScreenStartTime = 0;
	public static float loadingScreenEndTime = 0;
	public static float mapBundleLoadingStartTime = 0;
	public static float mapBundleLoadingEndTime = 0;
	public static float worldInstanceEndTime = 0;
	public static float assableMainPlayerBegin = 0;
	public static float assableMainPlayerEnd = 0;
	public static float instanceMapEnd = 0;

	void Awake()
	{
		GameObject.DontDestroyOnLoad(gameObject);
		loadingScreenStartTime = 0f;
		loadingScreenEndTime = 0f;
		mapBundleLoadingStartTime = 0f;
		mapBundleLoadingEndTime = 0f;
		worldInstanceEndTime = 0f;
	}

	void OnGUI()
	{
		Rect labelRect = new Rect(Screen.width*0.25f, Screen.height*0.25f,Screen.width*0.5f, Screen.height*0.5f);
		string debugText = " loading Screen Life Time = "+(loadingScreenEndTime-loadingScreenStartTime);
		debugText += "\n\t Bundle loaded for = "+(mapBundleLoadingEndTime-mapBundleLoadingStartTime);
		debugText += "\n\t Instantiated map, game models within sight, the initial data set = "+(worldInstanceEndTime-mapBundleLoadingEndTime);
		debugText += "\n\t Instantiate map start = "+mapBundleLoadingEndTime+"\n\t delay = "+(instanceMapEnd-mapBundleLoadingEndTime);
		debugText += "\n\t Instantiate main player start = "+assableMainPlayerBegin+"\n\t delay = "+(assableMainPlayerEnd-assableMainPlayerBegin);

		GUI.Label(labelRect, debugText);
	}
}