﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Org.BouncyCastle.Math;

public class Global {

	public static BigInteger sessionKey = BigInteger.Zero;
	public static bool goToCameraLook = true;
	
#if UNITY_IPHONE || UNITY_ANDROID
	public static bool sleepMode = false;
#endif
	
	public static void InvertBytes(byte[] inBytes)
	{
		byte t1 = 0;
		int j1 = inBytes.Length - 1;
		for(int i1 = 0; i1 < inBytes.Length / 2; i1++)
		{
			t1 = inBytes[i1];
			inBytes[i1] = inBytes[j1];
			inBytes[j1] = t1;
			j1--;
		}
	}
	
	public static bool In_array(int needle, List<int> haystack)
	{
		if(haystack.Contains(needle))
		{
			return true;
		}
		return false;
	}
	
	public static string GetClassAsString(Classes classid)
	{
		string ret = "Unknown class";
		switch (classid)
		{
		case Classes.CLASS_WARRIOR:
			ret = "Fighter";
			break;
		case Classes.CLASS_PALADIN:
			ret =  "Templar";
			break;
		case Classes.CLASS_ROGUE:
			ret =  "Rogue";
			break;
		case Classes.CLASS_MAGE:
			ret =  "Mage";
			break;
		case Classes.CLASS_PRIEST:
			ret =  "Confessor";
			break;
		case Classes.CLASS_WARLOCK:
			ret =  "Necromancer";
			break;
		case Classes.CLASS_HUNTER:
			ret =  "Ranger";
			break;
		}
		return ret;
	}
	
	public static string GetMessageTypeAsString(ChatMsg type)
	{
		string ret;
		switch(type)
		{
		case ChatMsg.CHAT_MSG_SAY:
			ret = "Say";
			break;
		case ChatMsg.CHAT_MSG_PARTY:
			ret = "Party";
			break;
		case ChatMsg.CHAT_MSG_YELL:
			ret = "Yell";
			break;
		case ChatMsg.CHAT_MSG_WHISPER:
			ret = "Whisper";
			break;
		case ChatMsg.CHAT_MSG_GUILD:
			ret = "Guild";
			break;
		default:
			ret = "Whisper";				// This might be a temporary fix...or a permanent one for chat whisper not working right. -Sebek
			break;
		}
		return ret;
	}
	
	public static string GetTimeAsString(uint secs, bool extended)
	{
		string strTime;
		
		if(secs >= 60)
		{
			uint minutes = secs / 60;
			secs = secs % 60;
			
			if(minutes >= 60)
			{
				uint hours = minutes / 60;
				minutes = minutes % 60;
				
				if(hours >= 24)
				{
					uint days = hours / 24;
					hours = hours % 24;
					
					if(days >= 30)
					{
						uint months = days / 30;
						days = days % 30;
						
						if(months >= 12)
						{
							uint years = months / 12;
							months = months % 12;
							if(extended)
							{
								strTime = years + " years " + months + " months " + days + " days " + 
											hours + " hours " + minutes + " minutes";
							}
							else
							{
								strTime = years + "y " + months + "m " + days + "d " + 
											hours + "h " + minutes + "m";
							}
						}
						else
						{
							if(extended)
							{
								strTime = months + " months " + days + " days " + hours + " hours " + minutes + " minutes";
							}
							else
							{
								strTime = months + "m " + days + "d " + hours + "h " + minutes + "m";
							}
						}
					}
					else
					{
						if(extended)
						{
							strTime = days + " days " + hours + " hours " + minutes + " minutes";
						}
						else
						{
							strTime = days + "d " + hours + "h " + minutes + "m";
						}
					}
				}
				else
				{
					if(extended)
					{
						strTime = hours + " hours " + minutes + " minutes";
					}
					else
					{
						strTime = hours + "h " + minutes + "m";
					}
				}
			}
			else
			{
				if(extended)
				{
					strTime = minutes + " minutes"; 
				}
				else
				{
					strTime = minutes + "m"; 
				}
			}
		}
		else
		{
			if(extended)
			{
				strTime = secs + " seconds";
			}
			else
			{
				strTime = secs + "s";
			}
		}
		return strTime;
	}
	
	public static string GetFormatTimeAsString(uint secs)
	{
		string time = "0";
		
		if(secs >= 60)		// minutes
		{
			secs = secs / 60;
			
			if(secs >= 60)		// hours
			{
				secs = secs / 60;
				
				if(secs >= 24)		// days
				{
					secs = secs / 24;
					if(secs >= 30)		// months
					{
						secs = secs / 30;
						if(secs >= 12)		// years
						{
							secs = secs / 12;
						}
						else
						{
							time = secs + " months";
						}
					}
					else
					{
						time = secs + " days";
					}
				}
				else
				{
					time = secs + " hours";
				}
			}
			else
			{
				time = secs + " minutes";
			}
		}
		return time;
	}
}
