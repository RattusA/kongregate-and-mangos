﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Gender
{
	GENDER_MALE			= 0,
	GENDER_FEMALE		= 1,
	GENDER_NONE			= 2
};

public enum FACTION {

	ALLIANCE,
	FURY,
	NOT_ASSIGNED
};

public enum JoystickReturn
{
	UNKNOWN			= 0,
	FORWARD_A		= 1,
	STOP			= 2,
	FORWARD_B		= 3,
	BACKWARD		= 4,
	SIDE_LEFT		= 5,
	SIDE_RIGHT		= 6,
	FORWARD_LEFT	= 7,
	FORWARD_RIGHT	= 8,
	BACKWARD_LEFT	= 9,
	BACKWARD_RIGHT	= 10
};

// Race value is index in ChrRaces.dbc
public enum Races : int
{
	RACE_NONE			= 0,
	RACE_HUMAN          = 1,	// Human
	RACE_ORC            = 2,	// Orc
	RACE_DWARF          = 3,	// Dwarf
	RACE_NIGHTELF       = 4,	// Elf
	RACE_UNDEAD_PLAYER  = 5,
	RACE_TAUREN         = 6,
	RACE_GNOME          = 7,
	RACE_TROLL          = 8,	// Blood Drak
	RACE_GOBLIN         = 9,
	RACE_BLOODELF       = 10,	// Dark Elf
	RACE_DRAENEI        = 11,
	RACE_FEL_ORC        = 12,
	RACE_NAGA           = 13,
	RACE_BROKEN         = 14,
	RACE_SKELETON       = 15,
	MAX_RACES           = 16
};

// Class value is index in ChrClasses.dbc
public enum Classes : int
{
	CLASS_WARRIOR		= 1,
	CLASS_PALADIN		= 2,
	CLASS_HUNTER		= 3,
	CLASS_ROGUE			= 4,
	CLASS_PRIEST		= 5,
	CLASS_DEATH_KNIGHT	= 6,
	CLASS_SHAMAN		= 7,
	CLASS_MAGE			= 8,
	CLASS_WARLOCK		= 9,
	CLASS_UNK2			= 10,
	CLASS_DRUID			= 11,
	MAX_CLASSES			= 12
};

public enum ClassesMask : int
{
	WARRIOR			= 1,
	PALADIN			= 2,
	HUNTER			= 4,
	ROGUE			= 8,
	PRIEST			= 16,
	DEATH_KNIGHT	= 32,
	SHAMAN			= 64,
	MAGE			= 128,
	WARLOCK			= 256,
//	UNK2			= 512,
	DRUID			= 1024,
	ALL_CLASSES		= 1535
};

public enum ChatMsg
{
	//   CHAT_MSG_ADDON                  = 0xFFFFFFFF,
	CHAT_MSG_SYSTEM                 = 0x00,
	CHAT_MSG_SAY                    = 0x01,
	CHAT_MSG_PARTY                  = 0x02,
	CHAT_MSG_RAID                   = 0x03,
	CHAT_MSG_GUILD                  = 0x04,
	CHAT_MSG_OFFICER                = 0x05,
	CHAT_MSG_YELL                   = 0x06,
	CHAT_MSG_WHISPER                = 0x07,
	CHAT_MSG_WHISPER_INFORM         = 0x08,
	CHAT_MSG_REPLY                  = 0x09,
	CHAT_MSG_EMOTE                  = 0x0A,
	CHAT_MSG_TEXT_EMOTE             = 0x0B,
	CHAT_MSG_MONSTER_SAY            = 0x0C,
	CHAT_MSG_MONSTER_PARTY          = 0x0D,
	CHAT_MSG_MONSTER_YELL           = 0x0E,
	CHAT_MSG_MONSTER_WHISPER        = 0x0F,
	CHAT_MSG_MONSTER_EMOTE          = 0x10,
	CHAT_MSG_CHANNEL                = 0x11,
	CHAT_MSG_CHANNEL_JOIN           = 0x12,
	CHAT_MSG_CHANNEL_LEAVE          = 0x13,
	CHAT_MSG_CHANNEL_LIST           = 0x14,
	CHAT_MSG_CHANNEL_NOTICE         = 0x15,
	CHAT_MSG_CHANNEL_NOTICE_USER    = 0x16,
	CHAT_MSG_AFK                    = 0x17,
	CHAT_MSG_DND                    = 0x18,
	CHAT_MSG_IGNORED                = 0x19,
	CHAT_MSG_SKILL                  = 0x1A,
	CHAT_MSG_LOOT                   = 0x1B,
	CHAT_MSG_MONEY                  = 0x1C,
	CHAT_MSG_OPENING                = 0x1D,
	CHAT_MSG_TRADESKILLS            = 0x1E,
	CHAT_MSG_PET_INFO               = 0x1F,
	CHAT_MSG_COMBAT_MISC_INFO       = 0x20,
	CHAT_MSG_COMBAT_XP_GAIN         = 0x21,
	CHAT_MSG_COMBAT_HONOR_GAIN      = 0x22,
	CHAT_MSG_COMBAT_FACTION_CHANGE  = 0x23,
	CHAT_MSG_BG_SYSTEM_NEUTRAL      = 0x24,
	CHAT_MSG_BG_SYSTEM_ALLIANCE     = 0x25,
	CHAT_MSG_BG_SYSTEM_HORDE        = 0x26,
	CHAT_MSG_RAID_LEADER            = 0x27,
	CHAT_MSG_RAID_WARNING           = 0x28,
	CHAT_MSG_RAID_BOSS_WHISPER      = 0x29,
	CHAT_MSG_RAID_BOSS_EMOTE        = 0x2A,
	CHAT_MSG_FILTERED               = 0x2B,
	CHAT_MSG_BATTLEGROUND           = 0x2C,
	CHAT_MSG_BATTLEGROUND_LEADER    = 0x2D,
	CHAT_MSG_RESTRICTED             = 0x2E,
	CHAT_MSG_BN                     = 0x2F,
	CHAT_MSG_ACHIEVEMENT            = 0x30,
	CHAT_MSG_GUILD_ACHIEVEMENT      = 0x31
};

public class PlayerEnumItem
{
	public uint displayId;
	public InventoryType inventoryType;
	public uint subClass;
};

public class PlayerEnum
{
	public ulong guid;
	public string name;
	public Races race = Races.RACE_HUMAN;
	public Classes classType;
	public Gender gender;
	public int skinColor;
	public int face;
	public int hairStyle;
	public int hairColor;
	public int facialHair;
	public byte outfitId;
	public byte _level;
	public uint _zoneId;
	public uint _mapId;
	public float _x;
	public float _y;
	public float _z;
	public uint _guildId;
	public uint _flags;
	public uint _petInfoId;
	public uint _petLevel;
	public uint _petFamilyId;
	
	public Dictionary<int, PlayerEnumItem> items;
	FACTION faction = FACTION.NOT_ASSIGNED;
	
	public PlayerEnum()
	{
		items = new Dictionary<int, PlayerEnumItem>();
		for(EquipmentSlots slot = EquipmentSlots.EQUIPMENT_SLOT_START; slot < EquipmentSlots.EQUIPMENT_SLOT_END; slot++)
		{
			items[(int)slot] = new PlayerEnumItem();
		}
	}

	public FACTION GetPlayerFaction()
	{
		if(faction == FACTION.NOT_ASSIGNED)
		{
			faction = GetFactionByRace(race);
		}
		return faction;
	}
	
	public static FACTION GetFactionByRace(Races race) 
	{
		FACTION ret = FACTION.NOT_ASSIGNED;
		switch(race)
		{
		case Races.RACE_HUMAN:
		case Races.RACE_NIGHTELF:
		case Races.RACE_DWARF:
			ret = FACTION.ALLIANCE;
			break;
		case Races.RACE_BLOODELF:
		case Races.RACE_ORC:
		case Races.RACE_TROLL:
			ret = FACTION.FURY;
			break;
		}
		return ret;
	}
};

//class CharacterListExt
//{
//	var p : PlayerEnum;
//	var zone : String;
//	var class_ : String;
//	var race : String;
//	var map_ : String;
//	function CharacterListExt()
//	{
//		p = new PlayerEnum();
//	}
//};

public class WhoListEntry
{
	public string name;
	public string gname;
	public uint level;
	public uint classId;
	public uint raceId;
	public uint zoneId;
	
	public void Copy(WorldPacket pkt)
	{
		name = pkt.ReadString();
		gname = pkt.ReadString();
		level = (uint)pkt.ReadType(4);
		classId = (uint)pkt.ReadType(4);
		raceId = (uint)pkt.ReadType(4);
		zoneId = (uint)pkt.ReadType(4);
	}
};

public class MovementInfo {
	
	public MovementFlags flags;
	public MovementFlags unkFlags;
	public uint time;
	public Vector3 pos = new Vector3();
	public float o;
	// transport
	public ulong t_guid;
	public Vector3 tpos = new Vector3();
	public float t_o;
	public uint t_time;
	public byte t_seat;
	// swimming and unk
	public float s_angle;
	// last fall time
	public uint fallTime;
	// jumping
	public float j_unk;
	public float j_sinAngle;
	public float j_cosAngle;
	public float j_xyspeed;
	// spline
	public float u_unk1;
	
	public MovementInfo()
	{
		flags = unkFlags = MovementFlags.MOVEMENTFLAG_NONE;
		time = t_time = fallTime = 0;
		t_seat = 0;
		o = t_o = s_angle = j_unk = j_sinAngle = j_cosAngle = j_xyspeed = u_unk1 = 0.0f;
		t_guid = 0;
	}
	
	public void SetMovementFlags(MovementFlags _flags)
	{
		flags = _flags;
	}
};

public class Bind
{
	public Vector3 homeBind;
	public uint homeBindMapId;
	public int homeBindAreaId;

	public void SetBind(WorldPacket pkt)
	{
		homeBind = new Vector3(pkt.ReadFloat(), pkt.ReadFloat(), pkt.ReadFloat());
		homeBindMapId = (uint)pkt.ReadReversed32bit();
		homeBindAreaId = pkt.ReadInt();
	}
	
	public string GetMapName()
	{
		string zoneName = "Unknown Map";
		switch(homeBindMapId)
		{
		case 1:
			zoneName = "Desert";
			break;
		case 13:
			zoneName = "Sand City";
			break;
		case 37:
			zoneName = "Battleground";
			break;
		case 209:
			zoneName = "Yaxche";
			break;
		case 42:
			zoneName = "Jungle";
			break;
		case 0:
			zoneName = "Tree Island";
			break;
		}
		return zoneName;
	}
}

public enum RaidGroupError {

	ERR_RAID_GROUP_NONE                 = 0,
	ERR_RAID_GROUP_LOWLEVEL             = 1,
	ERR_RAID_GROUP_ONLY                 = 2,
	ERR_RAID_GROUP_FULL                 = 3,
	ERR_RAID_GROUP_REQUIREMENTS_UNMATCH = 4
};

public enum PlayerMovementType {

	MOVE_ROOT       = 1,
	MOVE_UNROOT     = 2,
	MOVE_WATER_WALK = 3,
	MOVE_LAND_WALK  = 4
};

public enum DrunkenState {

	DRUNKEN_SOBER   = 0,
	DRUNKEN_TIPSY   = 1,
	DRUNKEN_DRUNK   = 2,
	DRUNKEN_SMASHED = 3
};

//#define MAX_DRUNKEN   4

public enum PlayerFlags {

	PLAYER_FLAGS_NONE              = 0x00000000,
	PLAYER_FLAGS_GROUP_LEADER      = 0x00000001,
	PLAYER_FLAGS_AFK               = 0x00000002,
	PLAYER_FLAGS_DND               = 0x00000004,
	PLAYER_FLAGS_GM                = 0x00000008,
	PLAYER_FLAGS_GHOST             = 0x00000010,
	PLAYER_FLAGS_RESTING           = 0x00000020,
	PLAYER_FLAGS_UNK7              = 0x00000040,            // admin?
	PLAYER_FLAGS_UNK8              = 0x00000080,            // pre-3.0.3 PLAYER_FLAGS_FFA_PVP flag for FFA PVP state
	PLAYER_FLAGS_CONTESTED_PVP     = 0x00000100,            // Player has been involved in a PvP combat and will be attacked by contested guards
	PLAYER_FLAGS_IN_PVP            = 0x00000200,
	PLAYER_FLAGS_HIDE_HELM         = 0x00000400,
	PLAYER_FLAGS_HIDE_CLOAK        = 0x00000800,
	PLAYER_FLAGS_PARTIAL_PLAY_TIME = 0x00001000,            // played long time
	PLAYER_FLAGS_NO_PLAY_TIME      = 0x00002000,            // played too long time
	PLAYER_FLAGS_IS_OUT_OF_BOUNDS  = 0x00004000,            // Lua_IsOutOfBounds
	PLAYER_FLAGS_DEVELOPER         = 0x00008000,            // <Dev> chat tag, name prefix
	PLAYER_FLAGS_UNK17             = 0x00010000,            // pre-3.0.3 PLAYER_FLAGS_SANCTUARY flag for player entered sanctuary
	PLAYER_FLAGS_TAXI_BENCHMARK    = 0x00020000,            // taxi benchmark mode (on/off) (2.0.1)
	PLAYER_FLAGS_PVP_TIMER         = 0x00040000,            // 3.0.2, pvp timer active (after you disable pvp manually)
	PLAYER_FLAGS_COMMENTATOR       = 0x00080000,
	PLAYER_FLAGS_UNK21             = 0x00100000,
	PLAYER_FLAGS_UNK22             = 0x00200000,
	PLAYER_FLAGS_COMMENTATOR2      = 0x00400000,            // something like COMMENTATOR_CAN_USE_INSTANCE_COMMAND
	PLAYER_FLAGS_UNK24             = 0x00800000,            // EVENT_SPELL_UPDATE_USABLE and EVENT_UPDATE_SHAPESHIFT_USABLE, disabled all abilitys on tab except autoattack
	PLAYER_FLAGS_UNK25             = 0x01000000,            // EVENT_SPELL_UPDATE_USABLE and EVENT_UPDATE_SHAPESHIFT_USABLE, disabled all melee ability on tab include autoattack
	PLAYER_FLAGS_XP_USER_DISABLED  = 0x02000000,
};

// used for PLAYER__FIELD_KNOWN_TITLES field (uint64), (1<<bit_index) without (-1)
// can't use enum for uint64 values
/*#define PLAYER_TITLE_DISABLED              UI64LIT(0x0000000000000000)
#define PLAYER_TITLE_NONE                  UI64LIT(0x0000000000000001)
#define PLAYER_TITLE_PRIVATE               UI64LIT(0x0000000000000002) // 1
#define PLAYER_TITLE_CORPORAL              UI64LIT(0x0000000000000004) // 2
#define PLAYER_TITLE_SERGEANT_A            UI64LIT(0x0000000000000008) // 3
#define PLAYER_TITLE_MASTER_SERGEANT       UI64LIT(0x0000000000000010) // 4
#define PLAYER_TITLE_SERGEANT_MAJOR        UI64LIT(0x0000000000000020) // 5
#define PLAYER_TITLE_KNIGHT                UI64LIT(0x0000000000000040) // 6
#define PLAYER_TITLE_KNIGHT_LIEUTENANT     UI64LIT(0x0000000000000080) // 7
#define PLAYER_TITLE_KNIGHT_CAPTAIN        UI64LIT(0x0000000000000100) // 8
#define PLAYER_TITLE_KNIGHT_CHAMPION       UI64LIT(0x0000000000000200) // 9
#define PLAYER_TITLE_LIEUTENANT_COMMANDER  UI64LIT(0x0000000000000400) // 10
#define PLAYER_TITLE_COMMANDER             UI64LIT(0x0000000000000800) // 11
#define PLAYER_TITLE_MARSHAL               UI64LIT(0x0000000000001000) // 12
#define PLAYER_TITLE_FIELD_MARSHAL         UI64LIT(0x0000000000002000) // 13
#define PLAYER_TITLE_GRAND_MARSHAL         UI64LIT(0x0000000000004000) // 14
#define PLAYER_TITLE_SCOUT                 UI64LIT(0x0000000000008000) // 15
#define PLAYER_TITLE_GRUNT                 UI64LIT(0x0000000000010000) // 16
#define PLAYER_TITLE_SERGEANT_H            UI64LIT(0x0000000000020000) // 17
#define PLAYER_TITLE_SENIOR_SERGEANT       UI64LIT(0x0000000000040000) // 18
#define PLAYER_TITLE_FIRST_SERGEANT        UI64LIT(0x0000000000080000) // 19
#define PLAYER_TITLE_STONE_GUARD           UI64LIT(0x0000000000100000) // 20
#define PLAYER_TITLE_BLOOD_GUARD           UI64LIT(0x0000000000200000) // 21
#define PLAYER_TITLE_LEGIONNAIRE           UI64LIT(0x0000000000400000) // 22
#define PLAYER_TITLE_CENTURION             UI64LIT(0x0000000000800000) // 23
#define PLAYER_TITLE_CHAMPION              UI64LIT(0x0000000001000000) // 24
#define PLAYER_TITLE_LIEUTENANT_GENERAL    UI64LIT(0x0000000002000000) // 25
#define PLAYER_TITLE_GENERAL               UI64LIT(0x0000000004000000) // 26
#define PLAYER_TITLE_WARLORD               UI64LIT(0x0000000008000000) // 27
#define PLAYER_TITLE_HIGH_WARLORD          UI64LIT(0x0000000010000000) // 28
#define PLAYER_TITLE_GLADIATOR             UI64LIT(0x0000000020000000) // 29
#define PLAYER_TITLE_DUELIST               UI64LIT(0x0000000040000000) // 30
#define PLAYER_TITLE_RIVAL                 UI64LIT(0x0000000080000000) // 31
#define PLAYER_TITLE_CHALLENGER            UI64LIT(0x0000000100000000) // 32
#define PLAYER_TITLE_SCARAB_LORD           UI64LIT(0x0000000200000000) // 33
#define PLAYER_TITLE_CONQUEROR             UI64LIT(0x0000000400000000) // 34
#define PLAYER_TITLE_JUSTICAR              UI64LIT(0x0000000800000000) // 35
#define PLAYER_TITLE_CHAMPION_OF_THE_NAARU UI64LIT(0x0000001000000000) // 36
#define PLAYER_TITLE_MERCILESS_GLADIATOR   UI64LIT(0x0000002000000000) // 37
#define PLAYER_TITLE_OF_THE_SHATTERED_SUN  UI64LIT(0x0000004000000000) // 38
#define PLAYER_TITLE_HAND_OF_ADAL          UI64LIT(0x0000008000000000) // 39
#define PLAYER_TITLE_VENGEFUL_GLADIATOR    UI64LIT(0x0000010000000000) // 40

#define KNOWN_TITLES_SIZE   3
#define MAX_TITLE_INDEX     (KNOWN_TITLES_SIZE*64)          // 3 uint64 fields
*/
// used in PLAYER_FIELD_BYTES values
public enum PlayerFieldByteFlags {

	PLAYER_FIELD_BYTE_TRACK_STEALTHED   = 0x00000002,
	PLAYER_FIELD_BYTE_RELEASE_TIMER     = 0x00000008,       // Display time till auto release spirit
	PLAYER_FIELD_BYTE_NO_RELEASE_WINDOW = 0x00000010        // Display no "release spirit" window at all
};

// used in byte (PLAYER_FIELD_BYTES2,3) values
public enum PlayerFieldByte2Flags {

	PLAYER_FIELD_BYTE2_NONE              = 0x00,
	PLAYER_FIELD_BYTE2_STEALTH           = 0x20,
	PLAYER_FIELD_BYTE2_INVISIBILITY_GLOW = 0x40
};

public enum ActivateTaxiReplies {

	ERR_TAXIOK                      = 0,
	ERR_TAXIUNSPECIFIEDSERVERERROR  = 1,
	ERR_TAXINOSUCHPATH              = 2,
	ERR_TAXINOTENOUGHMONEY          = 3,
	ERR_TAXITOOFARAWAY              = 4,
	ERR_TAXINOVENDORNEARBY          = 5,
	ERR_TAXINOTVISITED              = 6,
	ERR_TAXIPLAYERBUSY              = 7,
	ERR_TAXIPLAYERALREADYMOUNTED    = 8,
	ERR_TAXIPLAYERSHAPESHIFTED      = 9,
	ERR_TAXIPLAYERMOVING            = 10,
	ERR_TAXISAMENODE                = 11,
	ERR_TAXINOTSTANDING             = 12
};

public enum MirrorTimerType {

	FATIGUE_TIMER      = 0,
	BREATH_TIMER       = 1,
	FIRE_TIMER         = 2
};
//#define MAX_TIMERS      3

// 2^n values
public enum PlayerExtraFlags {

	// gm abilities
	PLAYER_EXTRA_GM_ON              = 0x0001,
	PLAYER_EXTRA_GM_ACCEPT_TICKETS  = 0x0002,
	PLAYER_EXTRA_ACCEPT_WHISPERS    = 0x0004,
	PLAYER_EXTRA_TAXICHEAT          = 0x0008,
	PLAYER_EXTRA_GM_INVISIBLE       = 0x0010,
	PLAYER_EXTRA_GM_CHAT            = 0x0020,               // Show GM badge in chat messages
	PLAYER_EXTRA_AUCTION_NEUTRAL    = 0x0040,
	PLAYER_EXTRA_AUCTION_ENEMY      = 0x0080,               // overwrite PLAYER_EXTRA_AUCTION_NEUTRAL
	
	// other states
	PLAYER_EXTRA_PVP_DEATH          = 0x0100                // store PvP death status until corpse creating.
};

// 2^n values
public enum AtLoginFlags {

	AT_LOGIN_NONE              = 0x00,
	AT_LOGIN_RENAME            = 0x01,
	AT_LOGIN_RESET_SPELLS      = 0x02,
	AT_LOGIN_RESET_TALENTS     = 0x04,
	AT_LOGIN_CUSTOMIZE         = 0x08,
	AT_LOGIN_RESET_PET_TALENTS = 0x10,
	AT_LOGIN_FIRST             = 0x20,
};

public enum SkillUpdateState {

	SKILL_UNCHANGED     = 0,
	SKILL_CHANGED       = 1,
	SKILL_NEW           = 2,
	SKILL_DELETED       = 3
};

public enum EquipmentSetUpdateState
{
	EQUIPMENT_SET_UNCHANGED = 0,
	EQUIPMENT_SET_CHANGED   = 1,
	EQUIPMENT_SET_NEW       = 2,
	EQUIPMENT_SET_DELETED   = 3
};

//#define MAX_EQUIPMENT_SET_INDEX 10                          // client limit


public enum TradeSlots
{
	TRADE_SLOT_COUNT            = 7,
	TRADE_SLOT_TRADED_COUNT     = 6,
	TRADE_SLOT_NONTRADED        = 6
};

public enum TransferAbortReason
{
	TRANSFER_ABORT_NONE                         = 0x00,
	TRANSFER_ABORT_ERROR                        = 0x01,
	TRANSFER_ABORT_MAX_PLAYERS                  = 0x02,     // Transfer Aborted: instance is full
	TRANSFER_ABORT_NOT_FOUND                    = 0x03,     // Transfer Aborted: instance not found
	TRANSFER_ABORT_TOO_MANY_INSTANCES           = 0x04,     // You have entered too many instances recently.
	TRANSFER_ABORT_ZONE_IN_COMBAT               = 0x06,     // Unable to zone in while an encounter is in progress.
	TRANSFER_ABORT_INSUF_EXPAN_LVL              = 0x07,     // You must have <TBC,WotLK> expansion installed to access this area.
	TRANSFER_ABORT_DIFFICULTY                   = 0x08,     // <Normal,Heroic,Epic> difficulty mode is not available for %s.
	TRANSFER_ABORT_UNIQUE_MESSAGE               = 0x09,     // Until you've escaped TLK's grasp, you cannot leave this place!
	TRANSFER_ABORT_TOO_MANY_REALM_INSTANCES     = 0x0A,     // Additional instances cannot be launched, please try again later.
	TRANSFER_ABORT_NEED_GROUP                   = 0x0B,     // 3.1
	TRANSFER_ABORT_NOT_FOUND2                   = 0x0C,     // 3.1
	TRANSFER_ABORT_NOT_FOUND3                   = 0x0D,     // 3.1
	TRANSFER_ABORT_NOT_FOUND4                   = 0x0E,     // 3.2
	TRANSFER_ABORT_REALM_ONLY                   = 0x0F,     // All players on party must be from the same realm.
	TRANSFER_ABORT_MAP_NOT_ALLOWED              = 0x10,     // Map can't be entered at this time.
};



// PLAYER_FIELD_ARENA_TEAM_INFO_1_1 offsets
public enum ArenaTeamInfoType
{
	ARENA_TEAM_ID               = 0,
	ARENA_TEAM_TYPE             = 1,                        // new in 3.2 - team type?
	ARENA_TEAM_MEMBER           = 2,                        // 0 - captain, 1 - member
	ARENA_TEAM_GAMES_WEEK       = 3,
	ARENA_TEAM_GAMES_SEASON     = 4,
	ARENA_TEAM_WINS_SEASON      = 5,
	ARENA_TEAM_PERSONAL_RATING  = 6,
	ARENA_TEAM_END              = 7
};

public enum RestType
{
	REST_TYPE_NO        = 0,
	REST_TYPE_IN_TAVERN = 1,
	REST_TYPE_IN_CITY   = 2
};

public enum DuelCompleteType
{
	DUEL_INTERUPTED = 0,
	DUEL_WON        = 1,
	DUEL_FLED       = 2
};

public enum TeleportToOptions
{
	TELE_TO_GM_MODE             = 0x01,
	TELE_TO_NOT_LEAVE_TRANSPORT = 0x02,
	TELE_TO_NOT_LEAVE_COMBAT    = 0x04,
	TELE_TO_NOT_UNSUMMON_PET    = 0x08,
	TELE_TO_SPELL               = 0x10,
};

/// Type of environmental damages
public enum EnviromentalDamage
{
	DAMAGE_EXHAUSTED = 0,
	DAMAGE_DROWNING  = 1,
	DAMAGE_FALL      = 2,
	DAMAGE_LAVA      = 3,
	DAMAGE_SLIME     = 4,
	DAMAGE_FIRE      = 5,
	DAMAGE_FALL_TO_VOID = 6                                 // custom case for fall without durability loss
};

public enum PlayedTimeIndex
{
	PLAYED_TIME_TOTAL = 0,
	PLAYED_TIME_LEVEL = 1
};

//#define MAX_PLAYED_TIME_INDEX 2

// used at player loading query list preparing, and later result selection
public enum PlayerLoginQueryIndex
{
	PLAYER_LOGIN_QUERY_LOADFROM,
	PLAYER_LOGIN_QUERY_LOADGROUP,
	PLAYER_LOGIN_QUERY_LOADBOUNDINSTANCES,
	PLAYER_LOGIN_QUERY_LOADAURAS,
	PLAYER_LOGIN_QUERY_LOADSPELLS,
	PLAYER_LOGIN_QUERY_LOADQUESTSTATUS,
	PLAYER_LOGIN_QUERY_LOADDAILYQUESTSTATUS,
	PLAYER_LOGIN_QUERY_LOADREPUTATION,
	PLAYER_LOGIN_QUERY_LOADINVENTORY,
	PLAYER_LOGIN_QUERY_LOADITEMLOOT,
	PLAYER_LOGIN_QUERY_LOADACTIONS,
	PLAYER_LOGIN_QUERY_LOADSOCIALLIST,
	PLAYER_LOGIN_QUERY_LOADHOMEBIND,
	PLAYER_LOGIN_QUERY_LOADSPELLCOOLDOWNS,
	PLAYER_LOGIN_QUERY_LOADDECLINEDNAMES,
	PLAYER_LOGIN_QUERY_LOADGUILD,
	PLAYER_LOGIN_QUERY_LOADARENAINFO,
	PLAYER_LOGIN_QUERY_LOADACHIEVEMENTS,
	PLAYER_LOGIN_QUERY_LOADCRITERIAPROGRESS,
	PLAYER_LOGIN_QUERY_LOADEQUIPMENTSETS,
	PLAYER_LOGIN_QUERY_LOADBGDATA,
	PLAYER_LOGIN_QUERY_LOADACCOUNTDATA,
	PLAYER_LOGIN_QUERY_LOADSKILLS,
	PLAYER_LOGIN_QUERY_LOADGLYPHS,
	PLAYER_LOGIN_QUERY_LOADMAILS,
	PLAYER_LOGIN_QUERY_LOADMAILEDITEMS,
	PLAYER_LOGIN_QUERY_LOADTALENTS,
	PLAYER_LOGIN_QUERY_LOADWEEKLYQUESTSTATUS,
	PLAYER_LOGIN_QUERY_LOADMONTHLYQUESTSTATUS,
	
	MAX_PLAYER_LOGIN_QUERY
};

public enum PlayerDelayedOperations
{
	DELAYED_SAVE_PLAYER         = 0x01,
	DELAYED_RESURRECT_PLAYER    = 0x02,
	DELAYED_SPELL_CAST_DESERTER = 0x04,
	DELAYED_BG_MOUNT_RESTORE    = 0x08,                     ///< Flag to restore mount state after teleport from BG
	DELAYED_BG_TAXI_RESTORE     = 0x10,                     ///< Flag to restore taxi state after teleport from BG
	DELAYED_END
};

public enum ReputationSource
{
	REPUTATION_SOURCE_KILL,
	REPUTATION_SOURCE_QUEST,
	REPUTATION_SOURCE_SPELL
};

// Player summoning auto-decline time (in secs)
//#define MAX_PLAYER_SUMMON_DELAY                   (2*MINUTE)
//#define MAX_MONEY_AMOUNT                       (0x7FFFFFFF-1)

