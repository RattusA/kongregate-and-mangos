﻿
public enum ReputationRank
{
	REP_HATED       = 0,
	REP_HOSTILE     = 1,
	REP_UNFRIENDLY  = 2,
	REP_NEUTRAL     = 3,
	REP_FRIENDLY    = 4,
	REP_HONORED     = 5,
	REP_REVERED     = 6,
	REP_EXALTED     = 7
};

public enum MoneyConstants
{
	COPPER = 1,
	SILVER = 100,
	GOLD   = 10000
};

public enum Stats
{
	STAT_STRENGTH                      = 0,
	STAT_AGILITY                       = 1,
	STAT_STAMINA                       = 2,
	STAT_INTELLECT                     = 3,
	STAT_SPIRIT                        = 4
};

public enum Powers
{
	POWER_MANA                         = 0,
	POWER_RAGE                         = 1,
	POWER_FOCUS                        = 2,
	POWER_ENERGY                       = 3,
	POWER_HAPPINESS                    = 4,
	POWER_RUNE                         = 5,
	POWER_RUNIC_POWER                  = 6,
//	POWER_HEALTH                       = 0xFFFFFFFE
};

public enum sheathTypes
{
	SHEATHETYPE_NONE                   = 0,
	SHEATHETYPE_MAINHAND               = 1,
	SHEATHETYPE_OFFHAND                = 2,
	SHEATHETYPE_LARGEWEAPONLEFT        = 3,
	SHEATHETYPE_LARGEWEAPONRIGHT       = 4,
	SHEATHETYPE_HIPWEAPONLEFT          = 5,
	SHEATHETYPE_HIPWEAPONRIGHT         = 6,
	SHEATHETYPE_SHIELD                 = 7
};

public enum Language
{
	LANG_GLOBAL         = 0,
	LANG_UNIVERSAL      = 0,
	LANG_ORCISH         = 1,
	LANG_DARNASSIAN     = 2,
	LANG_TAURAHE        = 3,
	LANG_DWARVISH       = 6,
	LANG_COMMON         = 7,
	LANG_DEMONIC        = 8,
	LANG_TITAN          = 9,
	LANG_THALASSIAN     = 10,
	LANG_DRACONIC       = 11,
	LANG_KALIMAG        = 12,
	LANG_GNOMISH        = 13,
	LANG_TROLL          = 14,
	LANG_GUTTERSPEAK    = 33,
	LANG_DRAENEI        = 35,
	//LANG_ADDON          = 0xFFFFFFFF   // used by addons
};

public enum InstanceResetWarningType
{
	RAID_INSTANCE_WARNING_HOURS     = 1,                    // WARNING! %s is scheduled to reset in %d hour(s).
	RAID_INSTANCE_WARNING_MIN       = 2,                    // WARNING! %s is scheduled to reset in %d minute(s)!
	RAID_INSTANCE_WARNING_MIN_SOON  = 3,                    // WARNING! %s is scheduled to reset in %d minute(s). Please exit the zone or you will be returned to your bind location!
	RAID_INSTANCE_WELCOME           = 4,                    // Welcome to %s. This raid instance is scheduled to reset in %s.
	RAID_INSTANCE_EXPIRED           = 5
};

public enum CharacterStates
{
	STATE_SITTING                          = 1,
	STATE_SITTINGCHAIR                     = 2,
	STATE_SLEEPING                         = 3,
	STATE_SITTINGCHAIRLOW                  = 4,
	STATE_SITTINGCHAIRMEDIUM               = 5,
	STATE_SITTINGCHAIRHIGH                 = 6,
	STATE_DEAD                             = 7,
	STATE_KNEEL                            = 8
};

public enum TextEmote
{
	TEXTEMOTE_AGREE                = 1,
	TEXTEMOTE_AMAZE                = 2,
	TEXTEMOTE_ANGRY                = 3,
	TEXTEMOTE_APOLOGIZE            = 4,
	TEXTEMOTE_APPLAUD              = 5,
	TEXTEMOTE_BASHFUL              = 6,
	TEXTEMOTE_BECKON               = 7,
	TEXTEMOTE_BEG                  = 8,
	TEXTEMOTE_BITE                 = 9,
	TEXTEMOTE_BLEED                = 10,
	TEXTEMOTE_BLINK                = 11,
	TEXTEMOTE_BLUSH                = 12,
	TEXTEMOTE_BONK                 = 13,
	TEXTEMOTE_BORED                = 14,
	TEXTEMOTE_BOUNCE               = 15,
	TEXTEMOTE_BRB                  = 16,
	TEXTEMOTE_BOW                  = 17,
	TEXTEMOTE_BURP                 = 18,
	TEXTEMOTE_BYE                  = 19,
	TEXTEMOTE_CACKLE               = 20,
	TEXTEMOTE_CHEER                = 21,
	TEXTEMOTE_CHICKEN              = 22,
	TEXTEMOTE_CHUCKLE              = 23,
	TEXTEMOTE_CLAP                 = 24,
	TEXTEMOTE_CONFUSED             = 25,
	TEXTEMOTE_CONGRATULATE         = 26,
	TEXTEMOTE_COUGH                = 27,
	TEXTEMOTE_COWER                = 28,
	TEXTEMOTE_CRACK                = 29,
	TEXTEMOTE_CRINGE               = 30,
	TEXTEMOTE_CRY                  = 31,
	TEXTEMOTE_CURIOUS              = 32,
	TEXTEMOTE_CURTSEY              = 33,
	TEXTEMOTE_DANCE                = 34,
	TEXTEMOTE_DRINK                = 35,
	TEXTEMOTE_DROOL                = 36,
	TEXTEMOTE_EAT                  = 37,
	TEXTEMOTE_EYE                  = 38,
	TEXTEMOTE_FART                 = 39,
	TEXTEMOTE_FIDGET               = 40,
	TEXTEMOTE_FLEX                 = 41,
	TEXTEMOTE_FROWN                = 42,
	TEXTEMOTE_GASP                 = 43,
	TEXTEMOTE_GAZE                 = 44,
	TEXTEMOTE_GIGGLE               = 45,
	TEXTEMOTE_GLARE                = 46,
	TEXTEMOTE_GLOAT                = 47,
	TEXTEMOTE_GREET                = 48,
	TEXTEMOTE_GRIN                 = 49,
	TEXTEMOTE_GROAN                = 50,
	TEXTEMOTE_GROVEL               = 51,
	TEXTEMOTE_GUFFAW               = 52,
	TEXTEMOTE_HAIL                 = 53,
	TEXTEMOTE_HAPPY                = 54,
	TEXTEMOTE_HELLO                = 55,
	TEXTEMOTE_HUG                  = 56,
	TEXTEMOTE_HUNGRY               = 57,
	TEXTEMOTE_KISS                 = 58,
	TEXTEMOTE_KNEEL                = 59,
	TEXTEMOTE_LAUGH                = 60,
	TEXTEMOTE_LAYDOWN              = 61,
	TEXTEMOTE_MESSAGE              = 62,
	TEXTEMOTE_MOAN                 = 63,
	TEXTEMOTE_MOON                 = 64,
	TEXTEMOTE_MOURN                = 65,
	TEXTEMOTE_NO                   = 66,
	TEXTEMOTE_NOD                  = 67,
	TEXTEMOTE_NOSEPICK             = 68,
	TEXTEMOTE_PANIC                = 69,
	TEXTEMOTE_PEER                 = 70,
	TEXTEMOTE_PLEAD                = 71,
	TEXTEMOTE_POINT                = 72,
	TEXTEMOTE_POKE                 = 73,
	TEXTEMOTE_PRAY                 = 74,
	TEXTEMOTE_ROAR                 = 75,
	TEXTEMOTE_ROFL                 = 76,
	TEXTEMOTE_RUDE                 = 77,
	TEXTEMOTE_SALUTE               = 78,
	TEXTEMOTE_SCRATCH              = 79,
	TEXTEMOTE_SEXY                 = 80,
	TEXTEMOTE_SHAKE                = 81,
	TEXTEMOTE_SHOUT                = 82,
	TEXTEMOTE_SHRUG                = 83,
	TEXTEMOTE_SHY                  = 84,
	TEXTEMOTE_SIGH                 = 85,
	TEXTEMOTE_SIT                  = 86,
	TEXTEMOTE_SLEEP                = 87,
	TEXTEMOTE_SNARL                = 88,
	TEXTEMOTE_SPIT                 = 89,
	TEXTEMOTE_STARE                = 90,
	TEXTEMOTE_SURPRISED            = 91,
	TEXTEMOTE_SURRENDER            = 92,
	TEXTEMOTE_TALK                 = 93,
	TEXTEMOTE_TALKEX               = 94,
	TEXTEMOTE_TALKQ                = 95,
	TEXTEMOTE_TAP                  = 96,
	TEXTEMOTE_THANK                = 97,
	TEXTEMOTE_THREATEN             = 98,
	TEXTEMOTE_TIRED                = 99,
	TEXTEMOTE_VICTORY              = 100,
	TEXTEMOTE_WAVE                 = 101,
	TEXTEMOTE_WELCOME              = 102,
	TEXTEMOTE_WHINE                = 103,
	TEXTEMOTE_WHISTLE              = 104,
	TEXTEMOTE_WORK                 = 105,
	TEXTEMOTE_YAWN                 = 106,
	TEXTEMOTE_BOGGLE               = 107,
	TEXTEMOTE_CALM                 = 108,
	TEXTEMOTE_COLD                 = 109,
	TEXTEMOTE_COMFORT              = 110,
	TEXTEMOTE_CUDDLE               = 111,
	TEXTEMOTE_DUCK                 = 112,
	TEXTEMOTE_INSULT               = 113,
	TEXTEMOTE_INTRODUCE            = 114,
	TEXTEMOTE_JK                   = 115,
	TEXTEMOTE_LICK                 = 116,
	TEXTEMOTE_LISTEN               = 117,
	TEXTEMOTE_LOST                 = 118,
	TEXTEMOTE_MOCK                 = 119,
	TEXTEMOTE_PONDER               = 120,
	TEXTEMOTE_POUNCE               = 121,
	TEXTEMOTE_PRAISE               = 122,
	TEXTEMOTE_PURR                 = 123,
	TEXTEMOTE_PUZZLE               = 124,
	TEXTEMOTE_RAISE                = 125,
	TEXTEMOTE_READY                = 126,
	TEXTEMOTE_SHIMMY               = 127,
	TEXTEMOTE_SHIVER               = 128,
	TEXTEMOTE_SHOO                 = 129,
	TEXTEMOTE_SLAP                 = 130,
	TEXTEMOTE_SMIRK                = 131,
	TEXTEMOTE_SNIFF                = 132,
	TEXTEMOTE_SNUB                 = 133,
	TEXTEMOTE_SOOTHE               = 134,
	TEXTEMOTE_STINK                = 135,
	TEXTEMOTE_TAUNT                = 136,
	TEXTEMOTE_TEASE                = 137,
	TEXTEMOTE_THIRSTY              = 138,
	TEXTEMOTE_VETO                 = 139,
	TEXTEMOTE_SNICKER              = 140,
	TEXTEMOTE_STAND                = 141,
	TEXTEMOTE_TICKLE               = 142,
	TEXTEMOTE_VIOLIN               = 143,
	TEXTEMOTE_SMILE                = 163,
	TEXTEMOTE_RASP                 = 183,
	TEXTEMOTE_PITY                 = 203,
	TEXTEMOTE_GROWL                = 204,
	TEXTEMOTE_BARK                 = 205,
	TEXTEMOTE_SCARED               = 223,
	TEXTEMOTE_FLOP                 = 224,
	TEXTEMOTE_LOVE                 = 225,
	TEXTEMOTE_MOO                  = 226,
	TEXTEMOTE_OPENFIRE             = 327,
	TEXTEMOTE_FLIRT                = 328,
	TEXTEMOTE_JOKE                 = 329,
	TEXTEMOTE_COMMEND              = 243,
	TEXTEMOTE_WINK                 = 363,
	TEXTEMOTE_PAT                  = 364,
	TEXTEMOTE_SERIOUS              = 365,
	TEXTEMOTE_MOUNTSPECIAL         = 366
};

public enum Emote
{
	EMOTE_ONESHOT_NONE                 = 0,
	EMOTE_ONESHOT_TALK                 = 1,
	EMOTE_ONESHOT_BOW                  = 2,
	EMOTE_ONESHOT_WAVE                 = 3,
	EMOTE_ONESHOT_CHEER                = 4,
	EMOTE_ONESHOT_EXCLAMATION          = 5,
	EMOTE_ONESHOT_QUESTION             = 6,
	EMOTE_ONESHOT_EAT                  = 7,
	EMOTE_STATE_DANCE                  = 10,
	EMOTE_ONESHOT_LAUGH                = 11,
	EMOTE_STATE_SLEEP                  = 12,
	EMOTE_STATE_SIT                    = 13,
	EMOTE_ONESHOT_RUDE                 = 14,
	EMOTE_ONESHOT_ROAR                 = 15,
	EMOTE_ONESHOT_KNEEL                = 16,
	EMOTE_ONESHOT_KISS                 = 17,
	EMOTE_ONESHOT_CRY                  = 18,
	EMOTE_ONESHOT_CHICKEN              = 19,
	EMOTE_ONESHOT_BEG                  = 20,
	EMOTE_ONESHOT_APPLAUD              = 21,
	EMOTE_ONESHOT_SHOUT                = 22,
	EMOTE_ONESHOT_FLEX                 = 23,
	EMOTE_ONESHOT_SHY                  = 24,
	EMOTE_ONESHOT_POINT                = 25,
	EMOTE_STATE_STAND                  = 26,
	EMOTE_STATE_READYUNARMED           = 27,
	EMOTE_STATE_WORK                   = 28,
	EMOTE_STATE_POINT                  = 29,
	EMOTE_STATE_NONE                   = 30,
	EMOTE_ONESHOT_WOUND                = 33,
	EMOTE_ONESHOT_WOUNDCRITICAL        = 34,
	EMOTE_ONESHOT_ATTACKUNARMED        = 35,
	EMOTE_ONESHOT_ATTACK1H             = 36,
	EMOTE_ONESHOT_ATTACK2HTIGHT        = 37,
	EMOTE_ONESHOT_ATTACK2HLOOSE        = 38,
	EMOTE_ONESHOT_PARRYUNARMED         = 39,
	EMOTE_ONESHOT_PARRYSHIELD          = 43,
	EMOTE_ONESHOT_READYUNARMED         = 44,
	EMOTE_ONESHOT_READY1H              = 45,
	EMOTE_ONESHOT_READYBOW             = 48,
	EMOTE_ONESHOT_SPELLPRECAST         = 50,
	EMOTE_ONESHOT_SPELLCAST            = 51,
	EMOTE_ONESHOT_BATTLEROAR           = 53,
	EMOTE_ONESHOT_SPECIALATTACK1H      = 54,
	EMOTE_ONESHOT_KICK                 = 60,
	EMOTE_ONESHOT_ATTACKTHROWN         = 61,
	EMOTE_STATE_STUN                   = 64,
	EMOTE_STATE_DEAD                   = 65,
	EMOTE_ONESHOT_SALUTE               = 66,
	EMOTE_STATE_KNEEL                  = 68,
	EMOTE_STATE_USESTANDING            = 69,
	EMOTE_ONESHOT_WAVE_NOSHEATHE       = 70,
	EMOTE_ONESHOT_CHEER_NOSHEATHE      = 71,
	EMOTE_ONESHOT_EAT_NOSHEATHE        = 92,
	EMOTE_STATE_STUN_NOSHEATHE         = 93,
	EMOTE_ONESHOT_DANCE                = 94,
	EMOTE_ONESHOT_SALUTE_NOSHEATH      = 113,
	EMOTE_STATE_USESTANDING_NOSHEATHE  = 133,
	EMOTE_ONESHOT_LAUGH_NOSHEATHE      = 153,
	EMOTE_STATE_WORK_NOSHEATHE         = 173,
	EMOTE_STATE_SPELLPRECAST           = 193,
	EMOTE_ONESHOT_READYRIFLE           = 213,
	EMOTE_STATE_READYRIFLE             = 214,
	EMOTE_STATE_WORK_NOSHEATHE_MINING  = 233,
	EMOTE_STATE_WORK_NOSHEATHE_CHOPWOOD= 234,
	EMOTE_zzOLDONESHOT_LIFTOFF         = 253,
	EMOTE_ONESHOT_LIFTOFF              = 254,
	EMOTE_ONESHOT_YES                  = 273,
	EMOTE_ONESHOT_NO                   = 274,
	EMOTE_ONESHOT_TRAIN                = 275,
	EMOTE_ONESHOT_LAND                 = 293,
	EMOTE_STATE_AT_EASE                = 313,
	EMOTE_STATE_READY1H                = 333,
	EMOTE_STATE_SPELLKNEELSTART        = 353,
	EMOTE_STATE_SUBMERGED              = 373,
	EMOTE_ONESHOT_SUBMERGE             = 374,
	EMOTE_STATE_READY2H                = 375,
	EMOTE_STATE_READYBOW               = 376,
	EMOTE_ONESHOT_MOUNTSPECIAL         = 377,
	EMOTE_STATE_TALK                   = 378,
	EMOTE_STATE_FISHING                = 379,
	EMOTE_ONESHOT_FISHING              = 380,
	EMOTE_ONESHOT_LOOT                 = 381,
	EMOTE_STATE_WHIRLWIND              = 382,
	EMOTE_STATE_DROWNED                = 383,
	EMOTE_STATE_HOLD_BOW               = 384,
	EMOTE_STATE_HOLD_RIFLE             = 385,
	EMOTE_STATE_HOLD_THROWN            = 386,
	EMOTE_ONESHOT_DROWN                = 387,
	EMOTE_ONESHOT_STOMP                = 388,
	EMOTE_ONESHOT_ATTACKOFF            = 389,
	EMOTE_ONESHOT_ATTACKOFFPIERCE      = 390,
	EMOTE_STATE_ROAR                   = 391,
	EMOTE_STATE_LAUGH                  = 392,
	EMOTE_ONESHOT_CREATURE_SPECIAL     = 393,
	EMOTE_ONESHOT_JUMPLANDRUN          = 394,
	EMOTE_ONESHOT_JUMPEND              = 395,
	EMOTE_ONESHOT_TALK_NOSHEATHE       = 396,
	EMOTE_ONESHOT_POINT_NOSHEATHE      = 397,
	EMOTE_STATE_CANNIBALIZE            = 398,
	EMOTE_ONESHOT_JUMPSTART            = 399,
	EMOTE_STATE_DANCESPECIAL           = 400,
	EMOTE_ONESHOT_DANCESPECIAL         = 401,
	EMOTE_ONESHOT_CUSTOMSPELL01        = 402,
	EMOTE_ONESHOT_CUSTOMSPELL02        = 403,
	EMOTE_ONESHOT_CUSTOMSPELL03        = 404,
	EMOTE_ONESHOT_CUSTOMSPELL04        = 405,
	EMOTE_ONESHOT_CUSTOMSPELL05        = 406,
	EMOTE_ONESHOT_CUSTOMSPELL06        = 407,
	EMOTE_ONESHOT_CUSTOMSPELL07        = 408,
	EMOTE_ONESHOT_CUSTOMSPELL08        = 409,
	EMOTE_ONESHOT_CUSTOMSPELL09        = 410,
	EMOTE_ONESHOT_CUSTOMSPELL10        = 411,
	EMOTE_STATE_EXCLAIM                = 412,
	EMOTE_STATE_SIT_CHAIR_MED          = 415,
	EMOTE_STATE_SPELLEFFECT_HOLD       = 422
};

public enum Anim
{
	ANIM_STAND                     = 0x0,
	ANIM_DEATH                     = 0x1,
	ANIM_SPELL                     = 0x2,
	ANIM_STOP                      = 0x3,
	ANIM_WALK                      = 0x4,
	ANIM_RUN                       = 0x5,
	ANIM_DEAD                      = 0x6,
	ANIM_RISE                      = 0x7,
	ANIM_STANDWOUND                = 0x8,
	ANIM_COMBATWOUND               = 0x9,
	ANIM_COMBATCRITICAL            = 0xA,
	ANIM_SHUFFLE_LEFT              = 0xB,
	ANIM_SHUFFLE_RIGHT             = 0xC,
	ANIM_WALK_BACKWARDS            = 0xD,
	ANIM_STUN                      = 0xE,
	ANIM_HANDS_CLOSED              = 0xF,
	ANIM_ATTACKUNARMED             = 0x10,
	ANIM_ATTACK1H                  = 0x11,
	ANIM_ATTACK2HTIGHT             = 0x12,
	ANIM_ATTACK2HLOOSE             = 0x13,
	ANIM_PARRYUNARMED              = 0x14,
	ANIM_PARRY1H                   = 0x15,
	ANIM_PARRY2HTIGHT              = 0x16,
	ANIM_PARRY2HLOOSE              = 0x17,
	ANIM_PARRYSHIELD               = 0x18,
	ANIM_READYUNARMED              = 0x19,
	ANIM_READY1H                   = 0x1A,
	ANIM_READY2HTIGHT              = 0x1B,
	ANIM_READY2HLOOSE              = 0x1C,
	ANIM_READYBOW                  = 0x1D,
	ANIM_DODGE                     = 0x1E,
	ANIM_SPELLPRECAST              = 0x1F,
	ANIM_SPELLCAST                 = 0x20,
	ANIM_SPELLCASTAREA             = 0x21,
	ANIM_NPCWELCOME                = 0x22,
	ANIM_NPCGOODBYE                = 0x23,
	ANIM_BLOCK                     = 0x24,
	ANIM_JUMPSTART                 = 0x25,
	ANIM_JUMP                      = 0x26,
	ANIM_JUMPEND                   = 0x27,
	ANIM_FALL                      = 0x28,
	ANIM_SWIMIDLE                  = 0x29,
	ANIM_SWIM                      = 0x2A,
	ANIM_SWIM_LEFT                 = 0x2B,
	ANIM_SWIM_RIGHT                = 0x2C,
	ANIM_SWIM_BACKWARDS            = 0x2D,
	ANIM_ATTACKBOW                 = 0x2E,
	ANIM_FIREBOW                   = 0x2F,
	ANIM_READYRIFLE                = 0x30,
	ANIM_ATTACKRIFLE               = 0x31,
	ANIM_LOOT                      = 0x32,
	ANIM_SPELL_PRECAST_DIRECTED    = 0x33,
	ANIM_SPELL_PRECAST_OMNI        = 0x34,
	ANIM_SPELL_CAST_DIRECTED       = 0x35,
	ANIM_SPELL_CAST_OMNI           = 0x36,
	ANIM_SPELL_BATTLEROAR          = 0x37,
	ANIM_SPELL_READYABILITY        = 0x38,
	ANIM_SPELL_SPECIAL1H           = 0x39,
	ANIM_SPELL_SPECIAL2H           = 0x3A,
	ANIM_SPELL_SHIELDBASH          = 0x3B,
	ANIM_EMOTE_TALK                = 0x3C,
	ANIM_EMOTE_EAT                 = 0x3D,
	ANIM_EMOTE_WORK                = 0x3E,
	ANIM_EMOTE_USE_STANDING        = 0x3F,
	ANIM_EMOTE_EXCLAMATION         = 0x40,
	ANIM_EMOTE_QUESTION            = 0x41,
	ANIM_EMOTE_BOW                 = 0x42,
	ANIM_EMOTE_WAVE                = 0x43,
	ANIM_EMOTE_CHEER               = 0x44,
	ANIM_EMOTE_DANCE               = 0x45,
	ANIM_EMOTE_LAUGH               = 0x46,
	ANIM_EMOTE_SLEEP               = 0x47,
	ANIM_EMOTE_SIT_GROUND          = 0x48,
	ANIM_EMOTE_RUDE                = 0x49,
	ANIM_EMOTE_ROAR                = 0x4A,
	ANIM_EMOTE_KNEEL               = 0x4B,
	ANIM_EMOTE_KISS                = 0x4C,
	ANIM_EMOTE_CRY                 = 0x4D,
	ANIM_EMOTE_CHICKEN             = 0x4E,
	ANIM_EMOTE_BEG                 = 0x4F,
	ANIM_EMOTE_APPLAUD             = 0x50,
	ANIM_EMOTE_SHOUT               = 0x51,
	ANIM_EMOTE_FLEX                = 0x52,
	ANIM_EMOTE_SHY                 = 0x53,
	ANIM_EMOTE_POINT               = 0x54,
	ANIM_ATTACK1HPIERCE            = 0x55,
	ANIM_ATTACK2HLOOSEPIERCE       = 0x56,
	ANIM_ATTACKOFF                 = 0x57,
	ANIM_ATTACKOFFPIERCE           = 0x58,
	ANIM_SHEATHE                   = 0x59,
	ANIM_HIPSHEATHE                = 0x5A,
	ANIM_MOUNT                     = 0x5B,
	ANIM_RUN_LEANRIGHT             = 0x5C,
	ANIM_RUN_LEANLEFT              = 0x5D,
	ANIM_MOUNT_SPECIAL             = 0x5E,
	ANIM_KICK                      = 0x5F,
	ANIM_SITDOWN                   = 0x60,
	ANIM_SITTING                   = 0x61,
	ANIM_SITUP                     = 0x62,
	ANIM_SLEEPDOWN                 = 0x63,
	ANIM_SLEEPING                  = 0x64,
	ANIM_SLEEPUP                   = 0x65,
	ANIM_SITCHAIRLOW               = 0x66,
	ANIM_SITCHAIRMEDIUM            = 0x67,
	ANIM_SITCHAIRHIGH              = 0x68,
	ANIM_LOADBOW                   = 0x69,
	ANIM_LOADRIFLE                 = 0x6A,
	ANIM_ATTACKTHROWN              = 0x6B,
	ANIM_READYTHROWN               = 0x6C,
	ANIM_HOLDBOW                   = 0x6D,
	ANIM_HOLDRIFLE                 = 0x6E,
	ANIM_HOLDTHROWN                = 0x6F,
	ANIM_LOADTHROWN                = 0x70,
	ANIM_EMOTE_SALUTE              = 0x71,
	ANIM_KNEELDOWN                 = 0x72,
	ANIM_KNEELING                  = 0x73,
	ANIM_KNEELUP                   = 0x74,
	ANIM_ATTACKUNARMEDOFF          = 0x75,
	ANIM_SPECIALUNARMED            = 0x76,
	ANIM_STEALTHWALK               = 0x77,
	ANIM_STEALTHSTAND              = 0x78,
	ANIM_KNOCKDOWN                 = 0x79,
	ANIM_EATING                    = 0x7A,
	ANIM_USESTANDINGLOOP           = 0x7B,
	ANIM_CHANNELCASTDIRECTED       = 0x7C,
	ANIM_CHANNELCASTOMNI           = 0x7D,
	ANIM_WHIRLWIND                 = 0x7E,
	ANIM_BIRTH                     = 0x7F,
	ANIM_USESTANDINGSTART          = 0x80,
	ANIM_USESTANDINGEND            = 0x81,
	ANIM_HOWL                      = 0x82,
	ANIM_DROWN                     = 0x83,
	ANIM_DROWNED                   = 0x84,
	ANIM_FISHINGCAST               = 0x85,
	ANIM_FISHINGLOOP               = 0x86,
	ANIM_FLY                       = 0x87,
	ANIM_EMOTE_WORK_NO_SHEATHE     = 0x88,
	ANIM_EMOTE_STUN_NO_SHEATHE     = 0x89,
	ANIM_EMOTE_USE_STANDING_NO_SHEATHE= 0x8A,
	ANIM_SPELL_SLEEP_DOWN          = 0x8B,
	ANIM_SPELL_KNEEL_START         = 0x8C,
	ANIM_SPELL_KNEEL_LOOP          = 0x8D,
	ANIM_SPELL_KNEEL_END           = 0x8E,
	ANIM_SPRINT                    = 0x8F,
	ANIM_IN_FIGHT                  = 0x90,
	
	ANIM_GAMEOBJ_SPAWN             = 145,
	ANIM_GAMEOBJ_CLOSE             = 146,
	ANIM_GAMEOBJ_CLOSED            = 147,
	ANIM_GAMEOBJ_OPEN              = 148,
	ANIM_GAMEOBJ_OPENED            = 149,
	ANIM_GAMEOBJ_DESTROY           = 150,
	ANIM_GAMEOBJ_DESTROYED         = 151,
	ANIM_GAMEOBJ_REBUILD           = 152,
	ANIM_GAMEOBJ_CUSTOM0           = 153,
	ANIM_GAMEOBJ_CUSTOM1           = 154,
	ANIM_GAMEOBJ_CUSTOM2           = 155,
	ANIM_GAMEOBJ_CUSTOM3           = 156,
	ANIM_GAMEOBJ_DESPAWN           = 157,
	ANIM_HOLD                      = 158,
	ANIM_DECAY                     = 159,
	ANIM_BOWPULL                   = 160,
	ANIM_BOWRELEASE                = 161,
	ANIM_SHIPSTART                 = 162,
	ANIM_SHIPMOVEING               = 163,
	ANIM_SHIPSTOP                  = 164,
	ANIM_GROUPARROW                = 165,
	ANIM_ARROW                     = 166,
	ANIM_CORPSEARROW               = 167,
	ANIM_GUIDEARROW                = 168,
	ANIM_SWAY                      = 169,
	ANIM_DRUIDCATPOUNCE            = 170,
	ANIM_DRUIDCATRIP               = 171,
	ANIM_DRUIDCATRAKE              = 172,
	ANIM_DRUIDCATRAVAGE            = 173,
	ANIM_DRUIDCATCLAW              = 174,
	ANIM_DRUIDCATCOWER             = 175,
	ANIM_DRUIDBEARSWIPE            = 176,
	ANIM_DRUIDBEARBITE             = 177,
	ANIM_DRUIDBEARMAUL             = 178,
	ANIM_DRUIDBEARBASH             = 179,
	ANIM_DRAGONTAIL                = 180,
	ANIM_DRAGONSTOMP               = 181,
	ANIM_DRAGONSPIT                = 182,
	ANIM_DRAGONSPITHOVER           = 183,
	ANIM_DRAGONSPITFLY             = 184,
	ANIM_EMOTEYES                  = 185,
	ANIM_EMOTENO                   = 186,
	ANIM_JUMPLANDRUN               = 187,
	ANIM_LOOTHOLD                  = 188,
	ANIM_LOOTUP                    = 189,
	ANIM_STANDHIGH                 = 190,
	ANIM_IMPACT                    = 191,
	ANIM_LIFTOFF                   = 192,
	ANIM_HOVER                     = 193,
	ANIM_SUCCUBUSENTICE            = 194,
	ANIM_EMOTETRAIN                = 195,
	ANIM_EMOTEDEAD                 = 196,
	ANIM_EMOTEDANCEONCE            = 197,
	ANIM_DEFLECT                   = 198,
	ANIM_EMOTEEATNOSHEATHE         = 199,
	ANIM_LAND                      = 200,
	ANIM_SUBMERGE                  = 201,
	ANIM_SUBMERGED                 = 202,
	ANIM_CANNIBALIZE               = 203,
	ANIM_ARROWBIRTH                = 204,
	ANIM_GROURARROWBIRTH           = 205,
	ANIM_CORPSEARROWBIRTH          = 206,
	ANIM_GUIDEARROWBIRTH           = 207
};

public enum LockType
{
	LOCKTYPE_PICKLOCK              = 1,
	LOCKTYPE_HERBALISM             = 2,
	LOCKTYPE_MINING                = 3,
	LOCKTYPE_DISARM_TRAP           = 4,
	LOCKTYPE_OPEN                  = 5,
	LOCKTYPE_TREASURE              = 6,
	LOCKTYPE_CALCIFIED_ELVEN_GEMS  = 7,
	LOCKTYPE_CLOSE                 = 8,
	LOCKTYPE_ARM_TRAP              = 9,
	LOCKTYPE_QUICK_OPEN            = 10,
	LOCKTYPE_QUICK_CLOSE           = 11,
	LOCKTYPE_OPEN_TINKERING        = 12,
	LOCKTYPE_OPEN_KNEELING         = 13,
	LOCKTYPE_OPEN_ATTACKING        = 14,
	LOCKTYPE_GAHZRIDIAN            = 15,
	LOCKTYPE_BLASTING              = 16,
	LOCKTYPE_SLOW_OPEN             = 17,
	LOCKTYPE_SLOW_CLOSE            = 18,
	LOCKTYPE_FISHING               = 19,
	LOCKTYPE_INSCRIPTION           = 20,
	LOCKTYPE_OPEN_FROM_VEHICLE     = 21
};

public enum TrainerType
{
	TRAINER_TYPE_CLASS             = 0,
	TRAINER_TYPE_MOUNTS            = 1,
	TRAINER_TYPE_TRADESKILLS       = 2,
	TRAINER_TYPE_PETS              = 3
};



public enum CreatureFamily
{
	CREATURE_FAMILY_WOLF           = 1,
	CREATURE_FAMILY_CAT            = 2,
	CREATURE_FAMILY_SPIDER         = 3,
	CREATURE_FAMILY_BEAR           = 4,
	CREATURE_FAMILY_BOAR           = 5,
	CREATURE_FAMILY_CROCILISK      = 6,
	CREATURE_FAMILY_CARRION_BIRD   = 7,
	CREATURE_FAMILY_CRAB           = 8,
	CREATURE_FAMILY_GORILLA        = 9,
	CREATURE_FAMILY_RAPTOR         = 11,
	CREATURE_FAMILY_TALLSTRIDER    = 12,
	CREATURE_FAMILY_FELHUNTER      = 15,
	CREATURE_FAMILY_VOIDWALKER     = 16,
	CREATURE_FAMILY_SUCCUBUS       = 17,
	CREATURE_FAMILY_DOOMGUARD      = 19,
	CREATURE_FAMILY_SCORPID        = 20,
	CREATURE_FAMILY_TURTLE         = 21,
	CREATURE_FAMILY_IMP            = 23,
	CREATURE_FAMILY_BAT            = 24,
	CREATURE_FAMILY_HYENA          = 25,
	CREATURE_FAMILY_BIRD_OF_PREY   = 26,
	CREATURE_FAMILY_WIND_SERPENT   = 27,
	CREATURE_FAMILY_REMOTE_CONTROL = 28,
	CREATURE_FAMILY_FELGUARD       = 29,
	CREATURE_FAMILY_DRAGONHAWK     = 30,
	CREATURE_FAMILY_RAVAGER        = 31,
	CREATURE_FAMILY_WARP_STALKER   = 32,
	CREATURE_FAMILY_SPOREBAT       = 33,
	CREATURE_FAMILY_NETHER_RAY     = 34,
	CREATURE_FAMILY_SERPENT        = 35,
	CREATURE_FAMILY_SEA_LION       = 36,
	CREATURE_FAMILY_MOTH           = 37,
	CREATURE_FAMILY_CHIMAERA       = 38,
	CREATURE_FAMILY_DEVILSAUR      = 39,
	CREATURE_FAMILY_GHOUL          = 40,
	CREATURE_FAMILY_SILITHID       = 41,
	CREATURE_FAMILY_WORM           = 42,
	CREATURE_FAMILY_RHINO          = 43,
	CREATURE_FAMILY_WASP           = 44,
	CREATURE_FAMILY_CORE_HOUND     = 45,
	CREATURE_FAMILY_SPIRIT_BEAST   = 46
};

public enum CreatureEliteType
{
	CREATURE_ELITE_NORMAL          = 0,
	CREATURE_ELITE_ELITE           = 1,
	CREATURE_ELITE_RAREELITE       = 2,
	CREATURE_ELITE_WORLDBOSS       = 3,
	CREATURE_ELITE_RARE            = 4
};

public enum CreatureType
{
	CREATURE_TYPE_BEAST            = 1,
	CREATURE_TYPE_DRAGON           = 2,
	CREATURE_TYPE_DEMON            = 3,
	CREATURE_TYPE_ELEMENTAL        = 4,
	CREATURE_TYPE_GIANT            = 5,
	CREATURE_TYPE_UNDEAD           = 6,
	CREATURE_TYPE_HUMANOID         = 7,
	CREATURE_TYPE_CRITTER          = 8,
	CREATURE_TYPE_MECHANICAL       = 9,
	CREATURE_TYPE_NOTSPECIFIED     = 10,
	CREATURE_TYPE_TOTEM            = 11
};

public enum QuestSort
{
	QUEST_SORT_EPIC                = 1,
	QUEST_SORT_WAILING_CAVERNS_OLD = 21,
	QUEST_SORT_SEASONAL            = 22,
	QUEST_SORT_UNDERCITY_OLD       = 23,
	QUEST_SORT_HERBALISM           = 24,
	QUEST_SORT_BATTLEGROUNDS       = 25,
	QUEST_SORT_ULDAMN_OLD          = 41,
	QUEST_SORT_WARLOCK             = 61,
	QUEST_SORT_WARRIOR             = 81,
	QUEST_SORT_SHAMAN              = 82,
	QUEST_SORT_FISHING             = 101,
	QUEST_SORT_BLACKSMITHING       = 121,
	QUEST_SORT_PALADIN             = 141,
	QUEST_SORT_MAGE                = 161,
	QUEST_SORT_ROGUE               = 162,
	QUEST_SORT_ALCHEMY             = 181,
	QUEST_SORT_LEATHERWORKING      = 182,
	QUEST_SORT_ENGINERING          = 201,
	QUEST_SORT_TREASURE_MAP        = 221,
	QUEST_SORT_SUNKEN_TEMPLE_OLD   = 241,
	QUEST_SORT_HUNTER              = 261,
	QUEST_SORT_PRIEST              = 262,
	QUEST_SORT_DRUID               = 263,
	QUEST_SORT_TAILORING           = 264,
	QUEST_SORT_SPECIAL             = 284,
	QUEST_SORT_COOKING             = 304,
	QUEST_SORT_FIRST_AID           = 324,
	QUEST_SORT_LEGENDARY           = 344,
	QUEST_SORT_DARKMOON_FAIRE      = 364,
	QUEST_SORT_AHN_QIRAJ_WAR       = 365,
	QUEST_SORT_LUNAR_FESTIVAL      = 366,
	QUEST_SORT_REPUTATION          = 367,
	QUEST_SORT_INVASION            = 368,
	QUEST_SORT_MIDSUMMER           = 369,
	QUEST_SORT_BREWFEST            = 370,
	QUEST_SORT_INSCRIPTION         = 371,
	QUEST_SORT_DEATH_KNIGHT        = 372,
	QUEST_SORT_JEWELCRAFTING       = 373
};

public enum SkillType
{
	SKILL_FROST                    = 6,
	SKILL_FIRE                     = 8,
	SKILL_ARMS                     = 26,
	SKILL_COMBAT                   = 38,
	SKILL_SUBTLETY                 = 39,
	SKILL_SWORDS                   = 43,
	SKILL_AXES                     = 44,
	SKILL_BOWS                     = 45,
	SKILL_GUNS                     = 46,
	SKILL_BEAST_MASTERY            = 50,
	SKILL_SURVIVAL                 = 51,
	SKILL_MACES                    = 54,
	SKILL_2H_SWORDS                = 55,
	SKILL_HOLY                     = 56,
	SKILL_SHADOW                   = 78,
	SKILL_DEFENSE                  = 95,
	SKILL_LANG_COMMON              = 98,
	SKILL_RACIAL_DWARVEN           = 101,
	SKILL_LANG_ORCISH              = 109,
	SKILL_LANG_DWARVEN             = 111,
	SKILL_LANG_DARNASSIAN          = 113,
	SKILL_LANG_TAURAHE             = 115,
	SKILL_DUAL_WIELD               = 118,
	SKILL_RACIAL_TAUREN            = 124,
	SKILL_ORC_RACIAL               = 125,
	SKILL_RACIAL_NIGHT_ELF         = 126,
	SKILL_FIRST_AID                = 129,
	SKILL_FERAL_COMBAT             = 134,
	SKILL_STAVES                   = 136,
	SKILL_LANG_THALASSIAN          = 137,
	SKILL_LANG_DRACONIC            = 138,
	SKILL_LANG_DEMON_TONGUE        = 139,
	SKILL_LANG_TITAN               = 140,
	SKILL_LANG_OLD_TONGUE          = 141,
	SKILL_SURVIVAL2                = 142,
	SKILL_RIDING_HORSE             = 148,
	SKILL_RIDING_WOLF              = 149,
	SKILL_RIDING_RAM               = 152,
	SKILL_RIDING_TIGER             = 150,
	SKILL_SWIMING                  = 155,
	SKILL_2H_MACES                 = 160,
	SKILL_UNARMED                  = 162,
	SKILL_MARKSMANSHIP             = 163,
	SKILL_BLACKSMITHING            = 164,
	SKILL_LEATHERWORKING           = 165,
	SKILL_ALCHEMY                  = 171,
	SKILL_2H_AXES                  = 172,
	SKILL_DAGGERS                  = 173,
	SKILL_THROWN                   = 176,
	SKILL_HERBALISM                = 182,
	SKILL_GENERIC_DND              = 183,
	SKILL_RETRIBUTION              = 184,
	SKILL_COOKING                  = 185,
	SKILL_MINING                   = 186,
	SKILL_PET_IMP                  = 188,
	SKILL_PET_FELHUNTER            = 189,
	SKILL_TAILORING                = 197,
	SKILL_ENGINERING               = 202,
	SKILL_PET_SPIDER               = 203,
	SKILL_PET_VOIDWALKER           = 204,
	SKILL_PET_SUCCUBUS             = 205,
	SKILL_PET_INFERNAL             = 206,
	SKILL_PET_DOOMGUARD            = 207,
	SKILL_PET_WOLF                 = 208,
	SKILL_PET_CAT                  = 209,
	SKILL_PET_BEAR                 = 210,
	SKILL_PET_BOAR                 = 211,
	SKILL_PET_CROCILISK            = 212,
	SKILL_PET_CARRION_BIRD         = 213,
	SKILL_PET_CRAB                 = 214,
	SKILL_PET_GORILLA              = 215,
	SKILL_PET_RAPTOR               = 217,
	SKILL_PET_TALLSTRIDER          = 218,
	SKILL_RACIAL_UNDED             = 220,
	SKILL_CROSSBOWS                = 226,
	SKILL_WANDS                    = 228,
	SKILL_POLEARMS                 = 229,
	SKILL_PET_SCORPID              = 236,
	SKILL_ARCANE                   = 237,
	SKILL_PET_TURTLE               = 251,
	SKILL_ASSASSINATION            = 253,
	SKILL_FURY                     = 256,
	SKILL_PROTECTION               = 257,
	SKILL_PROTECTION2              = 267,
	SKILL_PET_TALENTS              = 270,
	SKILL_PLATE_MAIL               = 293,
	SKILL_LANG_GNOMISH             = 313,
	SKILL_LANG_TROLL               = 315,
	SKILL_ENCHANTING               = 333,
	SKILL_DEMONOLOGY               = 354,
	SKILL_AFFLICTION               = 355,
	SKILL_FISHING                  = 356,
	SKILL_ENHANCEMENT              = 373,
	SKILL_RESTORATION              = 374,
	SKILL_ELEMENTAL_COMBAT         = 375,
	SKILL_SKINNING                 = 393,
	SKILL_MAIL                     = 413,
	SKILL_LEATHER                  = 414,
	SKILL_CLOTH                    = 415,
	SKILL_SHIELD                   = 433,
	SKILL_FIST_WEAPONS             = 473,
	SKILL_RIDING_RAPTOR            = 533,
	SKILL_RIDING_MECHANOSTRIDER    = 553,
	SKILL_RIDING_UNDEAD_HORSE      = 554,
	SKILL_RESTORATION2             = 573,
	SKILL_BALANCE                  = 574,
	SKILL_DESTRUCTION              = 593,
	SKILL_HOLY2                    = 594,
	SKILL_DISCIPLINE               = 613,
	SKILL_LOCKPICKING              = 633,
	SKILL_PET_BAT                  = 653,
	SKILL_PET_HYENA                = 654,
	SKILL_PET_BIRD_OF_PREY         = 655,
	SKILL_PET_WIND_SERPENT         = 656,
	SKILL_LANG_GUTTERSPEAK         = 673,
	SKILL_RIDING_KODO              = 713,
	SKILL_RACIAL_TROLL             = 733,
	SKILL_RACIAL_GNOME             = 753,
	SKILL_RACIAL_HUMAN             = 754,
	SKILL_JEWELCRAFTING            = 755,
	SKILL_RACIAL_BLOODELF          = 756,
	SKILL_PET_EVENT_RC             = 758,
	SKILL_LANG_DRAENEI             = 759,
	SKILL_RACIAL_DRAENEI           = 760,
	SKILL_PET_FELGUARD             = 761,
	SKILL_RIDING                   = 762,
	SKILL_PET_DRAGONHAWK           = 763,
	SKILL_PET_NETHER_RAY           = 764,
	SKILL_PET_SPOREBAT             = 765,
	SKILL_PET_WARP_STALKER         = 766,
	SKILL_PET_RAVAGER              = 767,
	SKILL_PET_SERPENT              = 768,
	SKILL_INTERNAL                 = 769,
	SKILL_DK_BLOOD                 = 770,
	SKILL_DK_FROST                 = 771,
	SKILL_DK_UNHOLY                = 772,
	SKILL_INSCRIPTION              = 773,
	SKILL_PET_MOTH                 = 775,
	SKILL_RUNEFORGING              = 776,
	SKILL_MOUNTS                   = 777,
	SKILL_COMPANIONS               = 778,
	SKILL_PET_EXOTIC_CHIMAERA      = 780,
	SKILL_PET_EXOTIC_DEVILSAUR     = 781,
	SKILL_PET_GHOUL                = 782,
	SKILL_PET_EXOTIC_SILITHID      = 783,
	SKILL_PET_EXOTIC_WORM          = 784,
	SKILL_PET_WASP                 = 785,
	SKILL_PET_EXOTIC_RHINO         = 786,
	SKILL_PET_EXOTIC_CORE_HOUND    = 787,
	SKILL_PET_EXOTIC_SPIRIT_BEAST  = 788
};


public enum SkillCategory
{
	SKILL_CATEGORY_ATTRIBUTES    =  5,
	SKILL_CATEGORY_WEAPON        =  6,
	SKILL_CATEGORY_CLASS         =  7,
	SKILL_CATEGORY_ARMOR         =  8,
	SKILL_CATEGORY_SECONDARY     =  9,                      // secondary professions
	SKILL_CATEGORY_LANGUAGES     = 10,
	SKILL_CATEGORY_PROFESSION    = 11,                      // primary professions
	SKILL_CATEGORY_NOT_DISPLAYED = 12
};

public enum TotemCategory
{
	TC_SKINNING_SKIFE_OLD          = 1,
	TC_EARTH_TOTEM                 = 2,
	TC_AIR_TOTEM                   = 3,
	TC_FIRE_TOTEM                  = 4,
	TC_WATER_TOTEM                 = 5,
	TC_COPPER_ROD                  = 6,
	TC_SILVER_ROD                  = 7,
	TC_GOLDEN_ROD                  = 8,
	TC_TRUESILVER_ROD              = 9,
	TC_ARCANITE_ROD                = 10,
	TC_MINING_PICK_OLD             = 11,
	TC_PHILOSOPHERS_STONE          = 12,
	TC_BLACKSMITH_HAMMER_OLD       = 13,
	TC_ARCLIGHT_SPANNER            = 14,
	TC_GYROMATIC_MA                = 15,
	TC_MASTER_TOTEM                = 21,
	TC_FEL_IRON_ROD                = 41,
	TC_ADAMANTITE_ROD              = 62,
	TC_ETERNIUM_ROD                = 63,
	TC_HOLLOW_QUILL                = 81,
	TC_RUNED_AZURITE_ROD           = 101,
	TC_VIRTUOSO_INKING_SET         = 121,
	TC_DRUMS                       = 141,
	TC_GNOMISH_ARMY_KNIFE          = 161,
	TC_BLACKSMITH_HAMMER           = 162,
	TC_MINING_PICK                 = 165,
	TC_SKINNING_KNIFE              = 166,
	TC_HAMMER_PICK                 = 167,
	TC_BLADED_PICKAXE              = 168,
	TC_FLINT_AND_TINDER            = 169,
	TC_RUNED_COBALT_ROD            = 189,
	TC_RUNED_TITANIUM_ROD          = 190
};

public enum UnitDynFlags
{
	//    UNIT_DYNFLAG_LOOTABLE          = 0x0001,
	//    UNIT_DYNFLAG_TRACK_UNIT        = 0x0002,
	//    UNIT_DYNFLAG_OTHER_TAGGER      = 0x0004,
	//    UNIT_DYNFLAG_ROOTED            = 0x0008,
	//    UNIT_DYNFLAG_SPECIALINFO       = 0x0010,
	//    UNIT_DYNFLAG_DEAD              = 0x0020,
	//    UNIT_DYNFLAG_REFER_A_FRIEND    = 0x0040
	UNIT_DYNFLAG_NONE                       = 0x0000,
	UNIT_DYNFLAG_LOOTABLE                   = 0x0001,
	UNIT_DYNFLAG_TRACK_UNIT                 = 0x0002,
	UNIT_DYNFLAG_TAPPED                     = 0x0004,       // Lua_UnitIsTapped
	UNIT_DYNFLAG_TAPPED_BY_PLAYER           = 0x0008,       // Lua_UnitIsTappedByPlayer
	UNIT_DYNFLAG_SPECIALINFO                = 0x0010,
	UNIT_DYNFLAG_DEAD                       = 0x0020,
	UNIT_DYNFLAG_REFER_A_FRIEND             = 0x0040,
	UNIT_DYNFLAG_TAPPED_BY_ALL_THREAT_LIST  = 0x0080        // Lua_UnitIsTappedByAllThreatList
};

public enum GameObjectDynamicLowFlags
{
	GO_DYNFLAG_LO_ACTIVATE          = 0x01,                 // enables interaction with GO
	GO_DYNFLAG_LO_ANIMATE           = 0x02,                 // possibly more distinct animation of GO
	GO_DYNFLAG_LO_NO_INTERACT       = 0x04,                 // appears to disable interaction (not fully verified)
	GO_DYNFLAG_LO_SPARKLE           = 0x08,                 // makes GO sparkle
};

public enum UnitFlags1
{
	UNIT_FLAG_NOT_ATTACKABLE       = 0x0002,
	UNIT_FLAG_ATTACKABLE           = 0x0008,
	UNIT_FLAG_NOT_ATTACKABLE_1     = 0x0080,
	//UNIT_FLAG_NON_PVP_PLAYER       = (UNIT_FLAG_ATTACKABLE + UNIT_FLAG_NOT_ATTACKABLE_1),
	UNIT_FLAG_ANIMATION_FROZEN     = 0x0400,
	UNIT_FLAG_WAR_PLAYER           = 0x1000
};

public enum ResponseCodes
{
	RESPONSE_SUCCESS                                       = 0x00,
	RESPONSE_FAILURE                                       = 0x01,
	RESPONSE_CANCELLED                                     = 0x02,
	RESPONSE_DISCONNECTED                                  = 0x03,
	RESPONSE_FAILED_TO_CONNECT                             = 0x04,
	RESPONSE_CONNECTED                                     = 0x05,
	RESPONSE_VERSION_MISMATCH                              = 0x06,
	
	CSTATUS_CONNECTING                                     = 0x07,
	CSTATUS_NEGOTIATING_SECURITY                           = 0x08,
	CSTATUS_NEGOTIATION_COMPLETE                           = 0x09,
	CSTATUS_NEGOTIATION_FAILED                             = 0x0A,
	CSTATUS_AUTHENTICATING                                 = 0x0B,
	
	AUTH_OK                                                = 0x0C,
	AUTH_FAILED                                            = 0x0D,
	AUTH_REJECT                                            = 0x0E,
	AUTH_BAD_SERVER_PROOF                                  = 0x0F,
	AUTH_UNAVAILABLE                                       = 0x10,
	AUTH_SYSTEM_ERROR                                      = 0x11,
	AUTH_BILLING_ERROR                                     = 0x12,
	AUTH_BILLING_EXPIRED                                   = 0x13,
	AUTH_VERSION_MISMATCH                                  = 0x14,
	AUTH_UNKNOWN_ACCOUNT                                   = 0x15,
	AUTH_INCORRECT_PASSWORD                                = 0x16,
	AUTH_SESSION_EXPIRED                                   = 0x17,
	AUTH_SERVER_SHUTTING_DOWN                              = 0x18,
	AUTH_ALREADY_LOGGING_IN                                = 0x19,
	AUTH_LOGIN_SERVER_NOT_FOUND                            = 0x1A,
	AUTH_WAIT_QUEUE                                        = 0x1B,
	AUTH_BANNED                                            = 0x1C,
	AUTH_ALREADY_ONLINE                                    = 0x1D,
	AUTH_NO_TIME                                           = 0x1E,
	AUTH_DB_BUSY                                           = 0x1F,
	AUTH_SUSPENDED                                         = 0x20,
	AUTH_PARENTAL_CONTROL                                  = 0x21,
	AUTH_LOCKED_ENFORCED                                   = 0x22,
	
	REALM_LIST_IN_PROGRESS                                 = 0x23,
	REALM_LIST_SUCCESS                                     = 0x24,
	REALM_LIST_FAILED                                      = 0x25,
	REALM_LIST_INVALID                                     = 0x26,
	REALM_LIST_REALM_NOT_FOUND                             = 0x27,
	
	ACCOUNT_CREATE_IN_PROGRESS                             = 0x28,
	ACCOUNT_CREATE_SUCCESS                                 = 0x29,
	ACCOUNT_CREATE_FAILED                                  = 0x2A,
	
	CHAR_LIST_RETRIEVING                                   = 0x2B,
	CHAR_LIST_RETRIEVED                                    = 0x2C,
	CHAR_LIST_FAILED                                       = 0x2D,
	
	CHAR_CREATE_IN_PROGRESS                                = 0x2E,
	CHAR_CREATE_SUCCESS                                    = 0x2F,
	CHAR_CREATE_ERROR                                      = 0x30,
	CHAR_CREATE_FAILED                                     = 0x31,
	CHAR_CREATE_NAME_IN_USE                                = 0x32,
	CHAR_CREATE_DISABLED                                   = 0x33,
	CHAR_CREATE_PVP_TEAMS_VIOLATION                        = 0x34,
	CHAR_CREATE_SERVER_LIMIT                               = 0x35,
	CHAR_CREATE_ACCOUNT_LIMIT                              = 0x36,
	CHAR_CREATE_SERVER_QUEUE                               = 0x37,
	CHAR_CREATE_ONLY_EXISTING                              = 0x38,
	CHAR_CREATE_EXPANSION                                  = 0x39,
	CHAR_CREATE_EXPANSION_CLASS                            = 0x3A,
	CHAR_CREATE_LEVEL_REQUIREMENT                          = 0x3B,
	CHAR_CREATE_UNIQUE_CLASS_LIMIT                         = 0x3C,
	
	CHAR_DELETE_IN_PROGRESS                                = 0x3D,
	CHAR_DELETE_SUCCESS                                    = 0x3E,
	CHAR_DELETE_FAILED                                     = 0x3F,
	CHAR_DELETE_FAILED_LOCKED_FOR_TRANSFER                 = 0x40,
	CHAR_DELETE_FAILED_GUILD_LEADER                        = 0x41,
	CHAR_DELETE_FAILED_ARENA_CAPTAIN                       = 0x42,
	
	CHAR_LOGIN_IN_PROGRESS                                 = 0x43,
	CHAR_LOGIN_SUCCESS                                     = 0x44,
	CHAR_LOGIN_NO_WORLD                                    = 0x45,
	CHAR_LOGIN_DUPLICATE_CHARACTER                         = 0x46,
	CHAR_LOGIN_NO_INSTANCES                                = 0x47,
	CHAR_LOGIN_FAILED                                      = 0x48,
	CHAR_LOGIN_DISABLED                                    = 0x49,
	CHAR_LOGIN_NO_CHARACTER                                = 0x4A,
	CHAR_LOGIN_LOCKED_FOR_TRANSFER                         = 0x4B,
	CHAR_LOGIN_LOCKED_BY_BILLING                           = 0x4C,
	
	CHAR_NAME_SUCCESS                                      = 0x4D,
	CHAR_NAME_FAILURE                                      = 0x4E,
	CHAR_NAME_NO_NAME                                      = 0x4F,
	CHAR_NAME_TOO_SHORT                                    = 0x50,
	CHAR_NAME_TOO_LONG                                     = 0x51,
	CHAR_NAME_INVALID_CHARACTER                            = 0x52,
	CHAR_NAME_MIXED_LANGUAGES                              = 0x53,
	CHAR_NAME_PROFANE                                      = 0x54,
	CHAR_NAME_RESERVED                                     = 0x55,
	CHAR_NAME_INVALID_APOSTROPHE                           = 0x56,
	CHAR_NAME_MULTIPLE_APOSTROPHES                         = 0x57,
	CHAR_NAME_THREE_CONSECUTIVE                            = 0x58,
	CHAR_NAME_INVALID_SPACE                                = 0x59,
	CHAR_NAME_CONSECUTIVE_SPACES                           = 0x5A,
	CHAR_NAME_RUSSIAN_CONSECUTIVE_SILENT_CHARACTERS        = 0x5B,
	CHAR_NAME_RUSSIAN_SILENT_CHARACTER_AT_BEGINNING_OR_END = 0x5C,
	CHAR_NAME_DECLENSION_DOESNT_MATCH_BASE_NAME            = 0x5D
};

public enum GuildDefaultRanks
{
	//these ranks can be modified, but they cannot be deleted
	GR_GUILDMASTER  = 0,
	GR_OFFICER      = 1,
	GR_VETERAN      = 2,
	GR_MEMBER       = 3,
	GR_INITIATE     = 4,
	//When promoting member server does: rank--;!
	//When demoting member server does: rank++;!
};

public enum GuildRankRights
{
	GR_RIGHT_EMPTY              = 0x00000040,
	GR_RIGHT_GCHATLISTEN        = 0x00000041,
	GR_RIGHT_GCHATSPEAK         = 0x00000042,
	GR_RIGHT_OFFCHATLISTEN      = 0x00000044,
	GR_RIGHT_OFFCHATSPEAK       = 0x00000048,
	GR_RIGHT_PROMOTE            = 0x000000C0,
	GR_RIGHT_DEMOTE             = 0x00000140,
	GR_RIGHT_INVITE             = 0x00000050,
	GR_RIGHT_REMOVE             = 0x00000060,
	GR_RIGHT_SETMOTD            = 0x00001040,
	GR_RIGHT_EPNOTE             = 0x00002040,
	GR_RIGHT_VIEWOFFNOTE        = 0x00004040,
	GR_RIGHT_EOFFNOTE           = 0x00008040,
	GR_RIGHT_MODIFY_GUILD_INFO  = 0x00010040,
	GR_RIGHT_WITHDRAW_GOLD_LOCK = 0x00020000,               // remove money withdraw capacity
	GR_RIGHT_WITHDRAW_REPAIR    = 0x00040000,               // withdraw for repair
	GR_RIGHT_WITHDRAW_GOLD      = 0x00080000,               // withdraw gold
	GR_RIGHT_CREATE_GUILD_EVENT = 0x00100000,               // wotlk
	GR_RIGHT_ALL                = 0x001DF1FF
};

public enum Typecommand
{
	GUILD_CREATE_S  = 0x00,
	GUILD_INVITE_S  = 0x01,
	GUILD_QUIT_S    = 0x03,
	// 0x05?
	GUILD_FOUNDER_S = 0x0E,
	GUILD_UNK1      = 0x13,
	GUILD_UNK2      = 0x14
};

public enum CommandErrors
{
	ERR_PLAYER_NO_MORE_IN_GUILD     = 0x00,
	ERR_GUILD_INTERNAL              = 0x01,
	ERR_ALREADY_IN_GUILD            = 0x02,
	ERR_ALREADY_IN_GUILD_S          = 0x03,
	ERR_INVITED_TO_GUILD            = 0x04,
	ERR_ALREADY_INVITED_TO_GUILD_S  = 0x05,
	ERR_GUILD_NAME_INVALID          = 0x06,
	ERR_GUILD_NAME_EXISTS_S         = 0x07,
	ERR_GUILD_LEADER_LEAVE          = 0x08,
	ERR_GUILD_PERMISSIONS           = 0x08,
	ERR_GUILD_PLAYER_NOT_IN_GUILD   = 0x09,
	ERR_GUILD_PLAYER_NOT_IN_GUILD_S = 0x0A,
	ERR_GUILD_PLAYER_NOT_FOUND_S    = 0x0B,
	ERR_GUILD_NOT_ALLIED            = 0x0C,
	ERR_GUILD_RANK_TOO_HIGH_S       = 0x0D,
	ERR_GUILD_RANK_TOO_LOW_S        = 0x0E,
	ERR_GUILD_RANKS_LOCKED          = 0x11,
	ERR_GUILD_RANK_IN_USE           = 0x12,
	ERR_GUILD_IGNORING_YOU_S        = 0x13,
	ERR_GUILD_UNK1                  = 0x14,
	ERR_GUILD_WITHDRAW_LIMIT        = 0x19,
	ERR_GUILD_NOT_ENOUGH_MONEY      = 0x1A,
	ERR_GUILD_BANK_FULL             = 0x1C,
	ERR_GUILD_ITEM_NOT_FOUND        = 0x1D
};

public enum GuildEvents
{
	GE_PROMOTION                    = 0x00,
	GE_DEMOTION                     = 0x01,
	GE_MOTD                         = 0x02,
	GE_JOINED                       = 0x03,
	GE_LEFT                         = 0x04,
	GE_REMOVED                      = 0x05,
	GE_LEADER_IS                    = 0x06,
	GE_LEADER_CHANGED               = 0x07,
	GE_DISBANDED                    = 0x08,
	GE_TABARDCHANGE                 = 0x09,
	GE_UNK1                         = 0x0A,                 // string, string EVENT_GUILD_ROSTER_UPDATE tab content change?
	GE_UNK2                         = 0x0B,                 // EVENT_GUILD_ROSTER_UPDATE
	GE_SIGNED_ON                    = 0x0C,                 // ERR_FRIEND_ONLINE_SS
	GE_SIGNED_OFF                   = 0x0D,                 // ERR_FRIEND_OFFLINE_S
	GE_GUILDBANKBAGSLOTS_CHANGED    = 0x0E,                 // EVENT_GUILDBANKBAGSLOTS_CHANGED
	GE_BANKTAB_PURCHASED            = 0x0F,                 // EVENT_GUILDBANK_UPDATE_TABS
	GE_UNK5                         = 0x10,                 // EVENT_GUILDBANK_UPDATE_TABS
	GE_GUILDBANK_UPDATE_MONEY       = 0x11,                 // EVENT_GUILDBANK_UPDATE_MONEY, string 0000000000002710 is 1 gold
	GE_GUILD_BANK_MONEY_WITHDRAWN   = 0x12,                 // MSG_GUILD_BANK_MONEY_WITHDRAWN
	GE_GUILDBANK_TEXT_CHANGED       = 0x13                  // EVENT_GUILDBANK_TEXT_CHANGED
};

public enum PetitionTurns
{
	PETITION_TURN_OK                    = 0,
	PETITION_TURN_ALREADY_IN_GUILD      = 2,
	PETITION_TURN_NEED_MORE_SIGNATURES  = 4,
};

public enum PetitionSigns
{
	PETITION_SIGN_OK                = 0,
	PETITION_SIGN_ALREADY_SIGNED    = 1,
	PETITION_SIGN_ALREADY_IN_GUILD  = 2,
	PETITION_SIGN_CANT_SIGN_OWN     = 3,
	PETITION_SIGN_NOT_SERVER        = 4,
};

public enum GuildBankRights
{
	GUILD_BANK_RIGHT_VIEW_TAB       = 0x01,
	GUILD_BANK_RIGHT_PUT_ITEM       = 0x02,
	GUILD_BANK_RIGHT_UPDATE_TEXT    = 0x04,
	
	GUILD_BANK_RIGHT_DEPOSIT_ITEM   = 0x03,		//ON SERVER:   GUILD_BANK_RIGHT_VIEW_TAB | GUILD_BANK_RIGHT_PUT_ITEM,
	GUILD_BANK_RIGHT_FULL           = 0xFF,
};

public enum GuildBankEventLogTypes
{
	GUILD_BANK_LOG_DEPOSIT_ITEM     = 1,
	GUILD_BANK_LOG_WITHDRAW_ITEM    = 2,
	GUILD_BANK_LOG_MOVE_ITEM        = 3,
	GUILD_BANK_LOG_DEPOSIT_MONEY    = 4,
	GUILD_BANK_LOG_WITHDRAW_MONEY   = 5,
	GUILD_BANK_LOG_REPAIR_MONEY     = 6,
	GUILD_BANK_LOG_MOVE_ITEM2       = 7,
	GUILD_BANK_LOG_UNK1             = 8,
	GUILD_BANK_LOG_UNK2             = 9,
};

public enum GuildEventLogTypes
{
	GUILD_EVENT_LOG_INVITE_PLAYER     = 1,
	GUILD_EVENT_LOG_JOIN_GUILD        = 2,
	GUILD_EVENT_LOG_PROMOTE_PLAYER    = 3,
	GUILD_EVENT_LOG_DEMOTE_PLAYER     = 4,
	GUILD_EVENT_LOG_UNINVITE_PLAYER   = 5,
	GUILD_EVENT_LOG_LEAVE_GUILD       = 6,
};

public enum GuildEmblem
{
	ERR_GUILDEMBLEM_SUCCESS               = 0,
	ERR_GUILDEMBLEM_INVALID_TABARD_COLORS = 1,
	ERR_GUILDEMBLEM_NOGUILD               = 2,
	ERR_GUILDEMBLEM_NOTGUILDMASTER        = 3,
	ERR_GUILDEMBLEM_NOTENOUGHMONEY        = 4,
	ERR_GUILDEMBLEM_INVALIDVENDOR         = 5
};

// Max values for Guild & Guild Bank
public class GuildDefines
{
	public uint GUILD_BANK_MAX_TABS			= 6;                       // send by client for money log also
	public uint GUILD_BANK_MAX_SLOTS		= 98;
	public uint GUILD_BANK_MAX_LOGS			= 25;
	public uint GUILD_BANK_MONEY_LOGS_TAB	= 100;                     // used for money log in DB
	public uint GUILD_EVENTLOG_MAX_RECORDS	= 100;
	public uint GUILD_RANKS_MIN_COUNT		= 5;
	public uint GUILD_RANKS_MAX_COUNT		= 10;
}

public class TimeConstants
{
	public static uint MINUTE			= 60;
	public static uint HOUR				= 3600;
	public static uint DAY				= 86400;
	public static uint WEEK				= 604800;
	public static uint MONTH			= 2592000;
	public static uint YEAR				= 31104000;
	public static uint IN_MILLISECONDS	= 1000;
};

public enum AccountTypes
{
	SEC_PLAYER         = 0,
	SEC_MODERATOR      = 1,
	SEC_GAMEMASTER     = 2,
	SEC_ADMINISTRATOR  = 3,
	SEC_CONSOLE        = 4                                  // must be always last in list, accounts must have less security level always also
};


public enum LockKeyType
{
	LOCK_KEY_NONE  = 0,
	LOCK_KEY_ITEM  = 1,
	LOCK_KEY_SKILL = 2
};

public enum SkillSpell
{
	SKILL_LOCKPICKING              = 633,
	SKILL_HERBALISM                = 182,
	SKILL_MINING                   = 186,
	SKILL_FISHING                  = 356,
	SKILL_INSCRIPTION              = 773
};

public class SharedDefines
{
	public static byte MAX_TALENT_SPEC_COUNT = 2;
	public static byte MAX_GLYPH_SLOT_INDEX = 6;
	// Max values for Guild & Guild Bank
	public static byte GUILD_BANK_MAX_TABS =        6;                       // send by client for money log also
	public static byte GUILD_BANK_MAX_SLOTS =       98;
	public static byte GUILD_BANK_MAX_LOGS =        25;
	public static byte GUILD_BANK_MONEY_LOGS_TAB =  100;                     // used for money log in DB
	public static byte GUILD_EVENTLOG_MAX_RECORDS = 100;
	public static byte GUILD_RANKS_MIN_COUNT =      5;
	public static byte GUILD_RANKS_MAX_COUNT =      10;
}
