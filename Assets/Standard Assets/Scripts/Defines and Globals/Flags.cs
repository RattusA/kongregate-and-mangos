﻿using UnityEngine;
using System.Collections;

public enum AuthCmd
{
	AUTH_NO_CMD                 = 0xFF,
	AUTH_LOGON_CHALLENGE        = 0x00,
	AUTH_LOGON_PROOF            = 0x01,
	AUTH_RECONNECT_CHALLENGE    = 0x02,
	AUTH_RECONNECT_PROOF        = 0x03,
	REALM_LIST                  = 0x10,
	XFER_INITIATE               = 0x30,
	XFER_DATA                   = 0x31,
	XFER_ACCEPT                 = 0x32,
	XFER_RESUME                 = 0x33,
	XFER_CANCEL                 = 0x34,
	CMD_CREATE_ACC_CHALLENGE    = 0x35,
	CMD_CREATE_ACC_PROOF		= 0x36,
	CMD_TEMPORARY_ACC           = 0x37,
	CMD_TEMPORARY_ACC_PROOF     = 0x38,
	CMD_AUTH_KONGREGATE			= 0x40,
	CMD_BIND_KONGREGATE			= 0x41,
};

public enum eAuthResults
{
	REALM_AUTH_SUCCESS 							= 0,
	REALM_AUTH_FAILURE							= 0x01,		//< Unable to connect
	REALM_AUTH_UNKNOWN1							= 0x02,		//< Unable to connect
	REALM_AUTH_ACCOUNT_BANNED					= 0x03,		//< This <game> account has been closed and is no longer available for use. Please go to <site>/banned.html for further information.
	REALM_AUTH_NO_MATCH							= 0x04,		//< The information you have entered is not valid. Please check the spelling of the account name and password. If you need help in retrieving a lost or stolen password, see <site> for more information
	REALM_AUTH_UNKNOWN2							= 0x05,		//< The information you have entered is not valid. Please check the spelling of the account name and password. If you need help in retrieving a lost or stolen password, see <site> for more information
	REALM_AUTH_ACCOUNT_IN_USE					= 0x06,		//< This account is already logged into <game>. Please check the spelling and try again.
	REALM_AUTH_PREPAID_TIME_LIMIT				= 0x07,		//< You have used up your prepaid time for this account. Please purchase more to continue playing
	REALM_AUTH_SERVER_FULL						= 0x08,		//< Could not log in to <game> at this time. Please try again later.
	REALM_AUTH_WRONG_BUILD_NUMBER				= 0x09,		//< Unable to validate game version. This may be caused by file corruption or interference of another program. Please visit <site> for more information and possible solutions to this issue.
	REALM_AUTH_UPDATE_CLIENT					= 0x0a,		//< Downloading
	REALM_AUTH_UNKNOWN3							= 0x0b,		//< Unable to connect
	REALM_AUTH_ACCOUNT_FREEZED					= 0x0c,		//< This <game> account has been temporarily suspended. Please go to <site>/banned.html for further information
	REALM_AUTH_UNKNOWN4							= 0x0d,		//< Unable to connect
	REALM_AUTH_UNKNOWN5							= 0x0e,		//< Connected.
	REALM_AUTH_PARENTAL_CONTROL					= 0x0f,		//< Access to this account has been blocked by parental controls. Your settings may be changed in your account preferences at <site>
	WOW_FAIL_LOCKED_ENFORCED        			= 0x10,     //< You have applied a lock to your account. You can change your locked status by calling your account lock phone number.
	WOW_FAIL_TRIAL_ENDED            			= 0x11,		//< Your trial subscription has expired. Please visit <site> to upgrade your account.
	WOW_FAIL_USE_BATTLENET          			= 0x12,		//< WOW_FAIL_OTHER This account is now attached to a Battle.net account. Please login with your Battle.net account email address and password.
	REALM_AUTH_ACCOUNT_ALREADY_EXIST			= 0x13,
	REALM_AUTH_REGISTRATION_NOT_AVALIABLE		= 0x14,
	REALM_AUTH_REGISTRATION_WRONG_LOGIN_NAME	= 0x15,		//< Wrong email format
	WOW_FAIL_REGISTRATION_SYSTEM_ERROR			= 0x16,
	WOW_FAIL_KONGRIDATE_AUTH					= 0x17,		//
	WOW_FAIL_KONGRIDATE_ALREADY_BOUND			= 0x18,		//
	WOW_FAIL_KONGRIDATE_ID						= 0x19,		//
};

public enum AccountConvertResult
{
	CONVERT_SUCCESS 				= 0,
	CONVERT_NOT_TEMPORARY 			= 1,
	CONVERT_NOT_AVAILABLE 			= 2,
	CONVERT_INVALID_LOGIN_FORMAT 	= 3,
	CONVERT_ACCOUNT_EXIST 			= 4,
	CONVERT_SYSTEM_ERROR 			= 5
}


public enum SceneLoginDataIndexes
{
	ISCENE_LOGIN_CONN_STATUS 		= 1,
	ISCENE_LOGIN_MSGBOX_DUMMY 		= 2,	// text
	ISCENE_LOGIN_LABELS 			= 3,	// text
	ISCENE_LOGIN_BUTTONS			= 4,	// text
	ISCENE_LOGIN_END
};

public enum SceneLoginConnStatus
{
	DSCENE_LOGIN_NOT_CONNECTED		= 0,
	DSCENE_LOGIN_CONN_ATTEMPT		= 1,
	DSCENE_LOGIN_CONN_FAILED		= 2,
	DSCENE_LOGIN_ACC_NOT_FOUND		= 3,
	DSCENE_LOGIN_ALREADY_CONNECTED	= 4,
	DSCENE_LOGIN_WRONG_VERSION		= 5,
	DSCENE_LOGIN_LOGGING_IN			= 6,
	DSCENE_LOGIN_AUTHENTICATING		= 7,
	DSCENE_LOGIN_AUTH_FAILED		= 8,
	DSCENE_LOGIN_REQ_REALM			= 9,
	DSCENE_LOGIN_UNK_ERROR			= 10,
	DSCENE_LOGIN_FILE_TRANSFER		= 11,
};

public enum SceneLoginLabels
{
	DSCENE_LOGIN_LABEL_ACC			= 0,
	DSCENE_LOGIN_LABEL_PASS			= 1,
};

public enum SceneLoginButtons
{
	DSCENE_LOGIN_BUTTON_LOGIN		= 0,
	DSCENE_LOGIN_BUTTON_QUIT		= 1,
	DSCENE_LOGIN_BUTTON_SITE		= 2,
};

public enum SceneCharSelectDataIndexes
{
	ISCENE_CHARSEL_BUTTONS 			= 1,	// text
	ISCENE_CHARSEL_LABELS			= 2,	// text
	ISCENE_CHARSEL_ERRMSG			= 3,	// uint32 response code, see enum ResponseCodes in SharedDefines.h for IDs
	ISCENE_CHARSEL_REALMFIRST		= 255,	// flag that is set when connecting to a realm wasnt possible and the realm list must be shown first
	ISCENE_CHARSEL_END
};

public enum SceneCharSelectButtons
{
	DSCENE_CHARSEL_BUTTON_ENTERWORLD	= 0,
	DSCENE_CHARSEL_BUTTON_NEWCHAR		= 1,
	DSCENE_CHARSEL_BUTTON_DELCHAR		= 2,
	DSCENE_CHARSEL_BUTTON_CHANGEREALM	= 3,
	DSCENE_CHARSEL_BUTTON_BACK			= 4,
	DSCENE_CHARSEL_REALMWIN_OK			= 5,
	DSCENE_CHARSEL_REALMWIN_CANCEL		= 6,
	DSCENE_CHARSEL_NEWCHARWIN_OK		= 7,
	DSCENE_CHARSEL_NEWCHARWIN_CANCEL	= 8,
};

public enum SceneCharSelectLabels
{
	DSCENE_CHARSEL_LABEL_REALMWIN		= 0,
	DSCENE_CHARSEL_LABEL_NEWCHARWIN		= 1,
};

public enum TYPE
{
	TYPE_OBJECT         = 1,
	TYPE_ITEM           = 2,
	TYPE_CONTAINER      = 6, // a container is ALWAYS an item!
	TYPE_UNIT           = 8,
	TYPE_PLAYER         = 16,
	TYPE_GAMEOBJECT     = 32,
	TYPE_DYNAMICOBJECT  = 64,
	TYPE_CORPSE         = 128,
	TYPE_AIGROUP        = 256,
	TYPE_AREATRIGGER    = 512
};

public enum TYPEID
{
	TYPEID_OBJECT        = 0,
	TYPEID_ITEM          = 1,
	TYPEID_CONTAINER     = 2,
	TYPEID_UNIT          = 3,
	TYPEID_PLAYER        = 4,
	TYPEID_GAMEOBJECT    = 5,
	TYPEID_DYNAMICOBJECT = 6,
	TYPEID_CORPSE        = 7,
	TYPEID_AIGROUP       = 8,
	TYPEID_AREATRIGGER   = 9,
	MAIN_PLAYER			 = 31
};

public enum SplineType
{
	SPLINETYPE_NORMAL       = 0,
	SPLINETYPE_STOP         = 1,
	SPLINETYPE_FACINGSPOT   = 2,
	SPLINETYPE_FACINGTARGET = 3,
	SPLINETYPE_FACINGANGLE  = 4
};

enum SplineMode
{
	SPLINEMODE_LINEAR       = 0,
	SPLINEMODE_CATMULLROM   = 1,
	SPLINEMODE_BEZIER3      = 2
};

public enum SplineFlags
{
	SPLINEFLAG_NONE         = 0x00000000,
	SPLINEFLAG_FORWARD      = 0x00000001,
	SPLINEFLAG_BACKWARD     = 0x00000002,
	SPLINEFLAG_STRAFE_LEFT  = 0x00000004,
	SPLINEFLAG_STRAFE_RIGHT = 0x00000008,
	SPLINEFLAG_LEFT         = 0x00000010,
	SPLINEFLAG_RIGHT        = 0x00000020,
	SPLINEFLAG_PITCH_UP     = 0x00000040,
	SPLINEFLAG_PITCH_DOWN   = 0x00000080,
	SPLINEFLAG_DONE         = 0x00000100,
	SPLINEFLAG_FALLING      = 0x00000200,
	SPLINEFLAG_NO_SPLINE    = 0x00000400,
	SPLINEFLAG_TRAJECTORY   = 0x00000800,
	SPLINEFLAG_WALKMODE     = 0x00001000,
	SPLINEFLAG_FLYING       = 0x00002000,
	SPLINEFLAG_KNOCKBACK    = 0x00004000,
	SPLINEFLAG_FINALPOINT   = 0x00008000,
	SPLINEFLAG_FINALTARGET  = 0x00010000,
	SPLINEFLAG_FINALFACING  = 0x00020000,
	SPLINEFLAG_CATMULLROM   = 0x00040000,
	SPLINEFLAG_UNKNOWN1     = 0x00080000,
	SPLINEFLAG_UNKNOWN2     = 0x00100000,
	SPLINEFLAG_UNKNOWN3     = 0x00200000,
	SPLINEFLAG_UNKNOWN4     = 0x00400000,
	SPLINEFLAG_UNKNOWN5     = 0x00800000,
	SPLINEFLAG_UNKNOWN6     = 0x01000000,
	SPLINEFLAG_UNKNOWN7     = 0x02000000,
	SPLINEFLAG_UNKNOWN8     = 0x04000000,
	SPLINEFLAG_UNKNOWN9     = 0x08000000,
	SPLINEFLAG_UNKNOWN10    = 0x10000000,
	SPLINEFLAG_UNKNOWN11    = 0x20000000,
	SPLINEFLAG_UNKNOWN12    = 0x40000000
};
