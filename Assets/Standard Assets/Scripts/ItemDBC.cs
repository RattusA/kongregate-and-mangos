﻿using UnityEngine;
using System.Collections.Generic;

public class itemField
{
	public uint entry;
	public uint displayId;
	public byte inventoryType;
	public byte classType;
	public byte subClass;
	public string item_name;
	public   itemField ( uint e ,   uint di ,   byte it ,   byte ct ,   byte sc ,   string iname  )
	{
		entry = e;
		displayId = di;
		inventoryType = it;
		classType = ct;
		subClass = sc;
		item_name = iname;
	}
}

public class ItemDBC
{
	static List<itemField> DBC = new List<itemField>();
	
	static void  init ()
	{
		//trainee
		DBC.Add(new itemField(39, 9892, 7, 4, 0,null));
		DBC.Add(new itemField(203, 16779, 10, 4, 0,null));
		DBC.Add(new itemField(40, 10141, 8, 4, 0,null));
		DBC.Add(new itemField(38, 9891, 4, 4, 0,null));
		
		//recruit
		DBC.Add(new itemField(239, 14475, 10, 4, 0, null));
		DBC.Add(new itemField(44, 9937, 7, 4, 0, null));
		DBC.Add(new itemField(45, 3265, 4, 4, 0, null));
		DBC.Add(new itemField(43, 9938, 8, 4, 0, null));
		
		//veteran
		DBC.Add(new itemField(714, 14445, 10, 4, 0, null));
		DBC.Add(new itemField(52, 9945, 7, 4, 0, null));
		DBC.Add(new itemField(53, 9944, 4, 4, 0, null));
		DBC.Add(new itemField(51, 9946, 8, 4, 0, null));
		
		//officer
		DBC.Add(new itemField(711, 16581, 10, 4, 0, null));
		DBC.Add(new itemField(48, 9913, 7, 4, 0, null));
		DBC.Add(new itemField(49, 9906, 4, 4, 0, null));
		DBC.Add(new itemField(47, 9915, 8, 4, 0, null));
		
		//elite
		DBC.Add(new itemField(718, 6986, 10, 4, 0, null));
		DBC.Add(new itemField(61, 16953, 7, 4, 0, null));
		DBC.Add(new itemField(56, 12647, 4, 4, 0, null));
		DBC.Add(new itemField(55, 9929, 8, 4, 0, null));
		
		//swords
		DBC.Add(new itemField(1925, 1, 13, 2, 7, "Concord-Match Rapier"));
		DBC.Add(new itemField(5191, 2, 13, 2, 7, "Sand-Dweller's Greeting"));
		DBC.Add(new itemField(5192, 3, 13, 2, 7, "Pathway Raider"));
		DBC.Add(new itemField(1482, 4, 13, 2, 7, "GhostDamage Sword"));
		DBC.Add(new itemField(6633, 5, 13, 2, 7, "Meatcleaving Knife"));
		DBC.Add(new itemField(1951, 6, 13, 2, 7, "Desert Tar Cutlass"));
		DBC.Add(new itemField(2018, 20088, 13, 2, 7, "Furyswipe Longsword"));
		DBC.Add(new itemField(2850, 3855, 13, 2, 7, "Rough-Smelted Shortsword"));
		
		DBC.Add(new itemField(3487, 7, 17, 2, 8, "Alliance Issue Broadsword"));
		DBC.Add(new itemField(4939, 8, 17, 2, 8, "Balance-Forged Bastard Sword"));
		DBC.Add(new itemField(3188, 14, 17, 2, 8, "Oceanbreeze Broadsword"));
		
		//axes
		DBC.Add(new itemField(768, 9, 21, 2, 0,"Shoresman Wood Axe"));
		DBC.Add(new itemField(826, 13, 13, 2, 0,"Hunters Broad Axe"));
		DBC.Add(new itemField(811, 19137, 13, 2, 0,"Elvenmere Axe"));
		DBC.Add(new itemField(871, 10, 13, 2, 0,"Mountain Ranger Light Axe"));
		DBC.Add(new itemField(872, 11, 17, 2, 0,"Mountain Ranger Heavy Axe"));
		DBC.Add(new itemField(885, 8494, 13, 2, 0,"Standard Orcish Battle Axe"));
		
		DBC.Add(new itemField(790, 12, 21, 2, 1,"Elven Travelswipe Axe"));
		DBC.Add(new itemField(870, 14, 17, 2, 1,"Fury Berserker Axe"));
		DBC.Add(new itemField(926, 22108, 17, 2, 1,"Basic Dwarven Battle Axe"));
		DBC.Add(new itemField(927, 22106, 13, 2, 1, "Double-Edged Dwarf Axe"));
		
		//maces
		DBC.Add(new itemField(2821, 15, 13, 2, 4, "Gnawing Macehammer"));
		DBC.Add(new itemField(2844, 16, 21, 2, 4, "Fine-Smelted Mace"));
		
		DBC.Add(new itemField(2848, 17, 21, 2, 5, "Rough-Smelted Mace"));
		
		//shield
		DBC.Add(new itemField(3450, 29, 14, 4, 6, "Glastonclan Shield"));
		DBC.Add(new itemField(3650, 30, 14, 4, 6, "Minion Shield"));
		DBC.Add(new itemField(3651, 31, 14, 4, 6, "Battlement Shield"));
		DBC.Add(new itemField(3652, 32, 14, 4, 6, "Forestmarker Buckler"));
		DBC.Add(new itemField(3653, 33, 14, 4, 6, "Altar Guard's Buckler"));
		DBC.Add(new itemField(3654, 34, 14, 4, 6, "Fish-Spear Shield"));
		DBC.Add(new itemField(3655, 35, 14, 4, 6, "Bulwark-Ware Shield"));
		DBC.Add(new itemField(3656, 36, 14, 4, 6, "Lizardman-Skin Shield"));
		DBC.Add(new itemField(3761, 37, 14, 4, 6, "Orcish-Bone Shield"));
		DBC.Add(new itemField(3816, 38, 14, 4, 6, "Polished Ore Shield"));
		DBC.Add(new itemField(3817, 18483, 14, 4, 6, "Costruct-Weight Buckler"));
		
		//crossbow
		DBC.Add(new itemField(6315, 11247, 26, 2, 18, "Steelarrow Crossbow"));
		
		//daggers
		DBC.Add(new itemField(7683, 15720, 13, 2, 15, "Splinter Facesmashers"));
		DBC.Add(new itemField(7298, 20435, 13, 2, 15, "Career-Bandit's Standby"));
		DBC.Add(new itemField(7682, 6555, 13, 2, 15, "Torturing Poker"));
		DBC.Add(new itemField(7683, 15720, 13, 2, 15, "Splinter Facesmashers"));
		DBC.Add(new itemField(22266, 6555, 13, 2, 15, null));
		DBC.Add(new itemField(30504, 20425, 13, 2, 15, null));
		
		//spears
		DBC.Add(new itemField(15810, 26500, 17, 2, 6, null));
		DBC.Add(new itemField(1406, 18, 17, 2, 6, "Righteous Phalanx Spear"));
		DBC.Add(new itemField(1522, 19, 17, 2, 6, "Headpike Spire"));
		DBC.Add(new itemField(1726, 20, 17, 2, 6, "Furystrike Venom Spear"));
		DBC.Add(new itemField(15811, 5636, 17, 2, 6, null));
		
		//satves
		
		DBC.Add(new itemField(3227, 39, 17, 2, 10, "Caster's Star Staff"));
		DBC.Add(new itemField(3327, 20434, 17, 2, 10, "Darkshore Seer Staff"));
		DBC.Add(new itemField(3415, 20339, 17, 2, 10, "Elghic-Rune War Staff"));
		DBC.Add(new itemField(3446, 20419, 17, 2, 10, "Duskcaster Staff"));
		DBC.Add(new itemField(4437, 20390, 17, 2, 10, "Spiritcaster Staff"));
		DBC.Add(new itemField(4566, 20420, 17, 2, 10, "Reinforced Quarterstaff"));
		DBC.Add(new itemField(3708, 10275, 17, 2, 10, "Antalla's Staff"));
		
		//bows
		DBC.Add(new itemField(2504, 22, 15, 2, 2, "Slackened Shortbow"));
		DBC.Add(new itemField(2505, 23, 15, 2, 2,"Furbished Shortbow"));
		DBC.Add(new itemField(2506, 24, 15, 2, 2, "Mahogany Curved Bow"));
		DBC.Add(new itemField(2507, 25, 15, 2, 2, "Enamelled Curved Bow"));
		DBC.Add(new itemField(2773, 26, 15, 2, 2, "Compromised Small Bow"));
		DBC.Add(new itemField(2777, 28, 15, 2, 2,"Weak-Born Small Bow"));
		
		//arrows
		DBC.Add(new itemField(2512, 5996, 24, 6, 2, "Bodytear Arrow"));
		DBC.Add(new itemField(2515, 5996, 24, 6, 2, "Truepierce Arrow"));
		DBC.Add(new itemField(3030, 5996, 24, 6, 2, "Justice Arrow"));
		DBC.Add(new itemField(3464, 5996, 24, 6, 2, "Helstonclan-Carved Arrow"));
		
	}
	
	
	/*(
		[39, 9892, 7, 4, 0],
		[203, 16779, 10, 4, 0],
		[40, 10141, 8, 4, 0],
		[38, 9891, 4, 4, 0]
	);*/
	
	static itemField getField ( uint entry  )
	{
		itemField ret = null;
		foreach(itemField field in DBC)
		{
			////Debug.Log("the entry: "+field.entry+" the real entry: "+entry);
			if(field.entry == entry)
			{
				ret = field;
				break;
			}
		}
		return ret;
	}

	public static string getName ( uint dId  )
	{
		Debug.Log(dId);
		foreach(itemField field in DBC)
		{
			if(field.displayId == dId)
				return field.item_name;
		}
		return "Undefined";
	}
	
	static Item getItemPrototype ( uint entry  )
	{
		Item itm = null;
		itemField field = getField(entry);
		if(field != null)
		{
			itm = new Item();
			itm.entry = field.entry;
			itm.displayInfoID = field.displayId;
			itm.inventoryType = (InventoryType)field.inventoryType;
			itm.subClass = (int)field.subClass;
			itm.classType = (ItemClass)field.classType;
		}
		return itm; 
	}
	
	static string getNameByEntry ( uint entry  )
	{
		itemField field = getField(entry);
		if(field != null)
		{
			return field.item_name;
		}
		return "Undefined";
	}
}