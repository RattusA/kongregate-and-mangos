﻿
public class SendPacket
{
	/**
 * Send information about spell position on action bar
 * 
 * Packet structure:
 * Paket size 5 bytes
 * uint8 << slotNumbet
 * uint32 << spellID 
 */
	static public void  SetActionButton ( byte position, uint spellID, uint flag )
	{
		WorldPacket pkt = new WorldPacket(5);
		
		flag = (flag << 24);
		uint data = spellID | flag;
		
		pkt.SetOpcode(OpCodes.CMSG_SET_ACTION_BUTTON);
		pkt.Append(position);
		pkt.Add(data);
		RealmSocket.outQueue.Add(pkt);
	}
	
	static public void  SetPetActionButton ( ulong petGuid ,   uint position ,   uint spellID  )
	{
		WorldPacket pkt = new WorldPacket(8+4+4);
		
		position += 3;
		pkt.SetOpcode(OpCodes.CMSG_PET_SET_ACTION);
		pkt.Add(petGuid);
		pkt.Add(position);
		pkt.Add(spellID);
		RealmSocket.outQueue.Add(pkt);
	}
	
	static public void  SetPetActionButton ( ulong petGuid ,   uint positionOne ,   uint spellIDOne ,   uint positionTwo ,   uint spellIDTwo  )
	{
		WorldPacket pkt = new WorldPacket(8+4+4+4+4);
		
		positionOne += 3;
		positionTwo += 3;
		
		pkt.SetOpcode(OpCodes.CMSG_PET_SET_ACTION);
		pkt.Add(petGuid);
		pkt.Add(positionOne);
		pkt.Add(spellIDOne);
		pkt.Add(positionTwo);
		pkt.Add(spellIDTwo);
		RealmSocket.outQueue.Add(pkt);
	}
}