using UnityEngine;

public class joyStick
{
	static private float radiusMultiply;
	public Texture2D outerTexture;
	private Texture2D innerTexture;
	float radius = 0;
	private Rect rect;
	private Vector2 center;
	private Rect inRect;
	private Vector2 inCenter;
	private int fingerNumber = -1;
	private Color globalColor;
	
	static float pipe8 = Mathf.PI*0.125f;
	static float pipe4 = Mathf.PI*0.25f;
	
	/*float raportX;
	float raportY;*/
	public float angle = 0;
	public bool  HasChangedMovement = false;
	int ReturnValue = -1;
	Vector2 pos2d;

	Rect fakeRect = new Rect(Screen.width * 0.05f, Screen.height * 0.7f, Screen.width * 0.13f, Screen.width * 0.13f);
	Rect fakeInRect = new Rect(Screen.width * 0.07f, Screen.height * 0.73f, Screen.width * 0.09f, Screen.width * 0.09f);
	public static int fingerID = -1; //for memorizing joystick touch ID
	
	/*function get4Pos : byte
	{
		if(angle<-Mathf.PI+pipe8 || angle> Mathf.PI-pipe8) // spate
			return MovementFlags.MOVEMENTFLAG_BACKWARD;
		if(angle<pipe8 && angle>-pipe8) /// fata
			return MovementFlags.MOVEMENTFLAG_FORWARD;
	}*/
	public joyStick ( Rect rec, Texture2D tx )
	{
		rect = rec;
		if(rect.width>rect.height)
			rect.width = rect.height;
		else rect.height = rect.width;
		outerTexture = tx;
		center = rectCenter();
		inRect = new Rect(0,0,rect.width*0.8f,rect.height*0.8f);
		radiusMultiply = 10*Screen.width/480;
		globalColor = new Color(1,1,1,0.1f);
	}
	
	void  setRectPos ( float x ,   float y )
	{
		rect.x = x;
		rect.y = y;
	} 
	void  setRectPos2 ( float x ,   float y )
	{
		rect.x = x;
		rect.y = y; 
		//Debug.Log("x: " + x + " y: " + y + " width: " + rect.width + "  height: " + rect.height);
	}
	
	public bool isTouched ()
	{
		if(fingerNumber == -1)
			return false;
		else
			return true;
	}
	
	public void  fixedUpdate ()
	{
		if(fingerNumber == -1 && globalColor.a > 0 )
		{
			globalColor.a -= 0.05f;
		}
	}
	
//	byte getPos ()
//	{
//		
//		if(angle<-Mathf.PI+pipe8 || angle> Mathf.PI-pipe8) // spate
//			return MovementFlags.MOVEMENTFLAG_BACKWARD;
//		if(angle<pipe8 && angle>-pipe8) /// fata
//			return MovementFlags.MOVEMENTFLAG_FORWARD;
//		
//		//byte ret ;
//		
//		if(angle>pipe8 && angle<pipe8+pipe4)//// fata stanga
//		{
//			//ret = MovementFlags.MOVEMENTFLAG_FORWARD;
//			//ret = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_FORWARD;
//			return MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_FORWARD;
//		}
//		
//		if(angle>pipe8+pipe4 && angle<2*pipe4 + pipe8)/// stafe stanga
//			return MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
//		
//		if(angle<3*pipe4 + pipe8 && angle>2*pipe4 + pipe8) //spate stanga
//			return MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_BACKWARD ;
//		
//		
//		if(angle<-pipe8 && angle>-pipe4-pipe8)/// dreapta fata
//			return MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT | MovementFlags.MOVEMENTFLAG_FORWARD;
//		
//		if(angle<-pipe4 - pipe8 && angle>-2*pipe4-pipe8) // dreapta
//			return MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
//		//MonoBehaviour.print(""+angle);
//		if(angle<-2*pipe4 - pipe8 && angle>-Mathf.PI + pipe8)// spade dreapta
//			return MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT | MovementFlags.MOVEMENTFLAG_BACKWARD ;
//		
//		return 0;
//		
//	}
	
	public void  setInTexture ( Texture2D texture )
	{
		innerTexture = texture;
		//inRect = new Rect(0,0,inTxt.width,inTxt.height);
	}	
	void  setRect ( Rect rec )
	{
		rect = rec;
		if(rect.width>rect.height)
			rect.width = rect.height;
		else rect.height = rect.width;
		center = rectCenter();
		inRect = new Rect(0,0,rect.width*0.65f,rect.height*0.65f);
		//MonoBehaviour.print(center);
	}
	Rect getRect ()
	{
		return rect;
	}
	
	Vector2 rectCenter ()
	{
		return new Vector2(rect.x+rect.width*0.5f,rect.y+rect.width*0.5f);
	}
	Vector2 inRectCenter ()
	{
		return new Vector2(inRect.x+inRect.width*0.5f,inRect.y+inRect.width*0.5f);
	}
	
	/*void  FakeDraw ()
	{
		if(txt)
			GUI.DrawTexture(fakeRect,txt);
		if(inTxt)
			GUI.DrawTexture(fakeInRect,inTxt);
	}
*/	
	//// return 0 for no action, 1 for is pressed, 2 for released, 3 just pressed
	public JoystickReturn Draw ()
	{
		JoystickReturn ret = DrawInUpdate();
		GUI.color = globalColor;
		if(outerTexture)
		{
			GUI.DrawTexture(rect,outerTexture);
		}
		if(innerTexture)
		{
			if(ret != null)
			{
				center = rectCenter();
				inRect.x = -radiusMultiply*Mathf.Sin(angle) + center.x - inRect.width*0.5f;
				inRect.y = -radiusMultiply*Mathf.Cos(angle) + center.y - inRect.height*0.5f;
				GUI.DrawTexture(inRect,innerTexture);
			}
			else
			{
				inRect.x = center.x - inRect.width*0.5f;
				inRect.y = center.y - inRect.height*0.5f;
				GUI.DrawTexture(inRect,innerTexture);
			}
		}
		GUI.color = new Color(1,1,1,1);
		//MonoBehaviour.print(inRect);
		return ret;
	}

	JoystickReturn DrawInUpdate ()
	{
		Vector2 pos = Vector2.zero;
		foreach(var touch in Input.touches)
		{
			if (touch.fingerId != Swipe.touchId1 && touch.fingerId != Swipe.touchId2) 
				// to make sure that zoom touches don't get mixed up with the joystick touch
			{ 
				if (touch.fingerId ==  fingerNumber)
				{  
					pos = new Vector2(touch.position.x*2,Screen.height)-touch.position;
					pos -= rectCenter();
					//Debug.Log(pos);
					radius = pos.magnitude;
					angle = Mathf.Acos(-pos.y/radius);
					if(pos.x>0)
						angle = -angle; 
					
					if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
					{
						
						pos = new Vector2(touch.position.x*2, Screen.height) - touch.position;
						if (Vector2.Distance(rectCenter(), pos) > 70.0f)
						{ 
							Vector2 postt = rectCenter();
							postt = Vector2.MoveTowards(postt, pos, Time.deltaTime * 100); 
							setRectPos2(postt.x - rect.width*0.5f, postt.y - rect.width*0.5f); 
						}
						
						if ( Mathf.Abs(angle) > 2.5f )
						{
							ReturnValue = 4;     // backwards
							HasChangedMovement = true; 
							//Debug.Log("backwards");
							return JoystickReturn.BACKWARD; //4;
							
						}
						else
						{
							
							if ( angle > 2 && angle < 2.5f )
							{
								if (ReturnValue != 9)
									HasChangedMovement = true;
								ReturnValue = 9; 
								//Debug.Log("backward_left");
								return JoystickReturn.BACKWARD_LEFT; 
								
							}
							
							if ( angle > -2.5f && angle < -2 )
							{
								if (ReturnValue != 10)
									HasChangedMovement = true;
								ReturnValue = 10; 
								//Debug.Log("backward_right");
								return JoystickReturn.BACKWARD_RIGHT;
								
							}
							
							if ( angle < 2 && angle > 1 )
							{
								if ( ReturnValue != 5 )
									HasChangedMovement = true;
								ReturnValue = 5;   // straif left
								//Debug.Log("side_left");
								return JoystickReturn.SIDE_LEFT; //5; 
								
							}
							
							if ( angle > 0.5f && angle < 1 )
							{
								if (ReturnValue != 7)
									HasChangedMovement = true;
								ReturnValue = 7; 
								//Debug.Log("forward_left");
								return JoystickReturn.FORWARD_LEFT; 
								
							}
							
							if ( angle < -0.5f && angle > -1 )
							{
								if (ReturnValue != 8)
									HasChangedMovement = true;
								ReturnValue = 8; 
								//Debug.Log("forward_right");
								return JoystickReturn.FORWARD_RIGHT; 
								
							}
							
							if ( angle > -2 && angle < -1 )
							{
								if ( ReturnValue != 6 )
									HasChangedMovement = true;
								ReturnValue = 6;   // straif right
								//Debug.Log("side_right");
								return JoystickReturn.SIDE_RIGHT; //6; 
								
							}
							
							if ( ReturnValue != 1 )
								HasChangedMovement = true;
							ReturnValue = 1; 
							//Debug.Log("forward");
							return JoystickReturn.FORWARD_A; //1; 
							
							
						}
					} 
				}
				
				if(touch.position.y<=Screen.height*0.40f && touch.position.x<=Screen.width*0.24f)
				{ 
					pos = new Vector2(touch.position.x*2,Screen.height)-touch.position;
					if(touch.phase == TouchPhase.Began && touch.fingerId != fingerID) // second condition just to be safe
						setRectPos(pos.x-rect.width*0.5f, pos.y-rect.height*0.4f);
					globalColor.a = 1.0f;
					pos -= rectCenter();
					radius = pos.magnitude;
					angle = Mathf.Acos(-pos.y/radius);
					if(pos.x>0)
						angle = -angle;
					if((touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) && fingerID == touch.fingerId)
					{
						fingerNumber = -1;
						fingerID = -1; //release fingerID (the joystick touch)
						return JoystickReturn.STOP;  //2 
					}
					else if(touch.phase == TouchPhase.Began && fingerID != touch.fingerId)
					{
						fingerNumber = touch.fingerId;
						fingerID = touch.fingerId; //register joystick touch 
						return JoystickReturn.FORWARD_B; //3;
					}
				}
				else if(fingerNumber==touch.fingerId)
				{
					
					//				if (touch.position.y>Screen.height*0.40f || touch.position.x>Screen.width*0.24f)
					//                {
					//                	fingerNumber = -1;
					//					fingerID = -1; // again, "release" joystick touch
					//					return JoystickReturn.STOP; //2; 
					//                }
					globalColor.a = 1.0f;
					pos -= rectCenter();
					radius = pos.magnitude;
					angle = Mathf.Acos(-pos.y/radius);
					if(pos.x>0)
						angle = -angle;
					if(touch.phase == TouchPhase.Ended)
					{
						fingerNumber = -1;
						fingerID = -1; // again, "release" joystick touch
						ReturnValue = 2;
						return JoystickReturn.STOP; //2;
					}
					else 
						return JoystickReturn.FORWARD_A; // 1;
				}
			}
		}
		if(Application.platform!=RuntimePlatform.IPhonePlayer && Application.platform != RuntimePlatform.Android)
		{
			if(Input.GetMouseButton(0))
			{
				pos2d = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
				pos = new Vector2(Input.mousePosition.x*2,Screen.height)-pos2d;
				if (Vector2.Distance(rectCenter(), pos) > 70.0f)
				{ 
					Vector2 post = rectCenter();
					post = Vector2.MoveTowards(post, pos, Time.deltaTime * 100); 
					setRectPos2(post.x - rect.width*0.5f, post.y - rect.width*0.5f); 
				}
				if(fingerNumber > -1)//(rect.Contains(pos))
				{
					pos -= rectCenter();
					//Debug.Log(pos);
					radius = pos.magnitude;
					angle = Mathf.Acos(-pos.y/radius);
					if(pos.x>0)
						angle = -angle;
					
					//Debug.Log("unghi " + angle);
					if ( Mathf.Abs(angle) > 2.5f )
					{
						ReturnValue = 4;     // backwards
						HasChangedMovement = true; 
						//Debug.Log("backwards");
						return JoystickReturn.BACKWARD; //4;
						
					}
					else
					{
						
						if ( angle > 2 && angle < 2.5f )
						{
							if (ReturnValue != 9)
								HasChangedMovement = true;
							ReturnValue = 9; 
							//Debug.Log("backward_left");
							return JoystickReturn.BACKWARD_LEFT; 
							
						}
						
						if ( angle > -2.5f && angle < -2 )
						{
							if (ReturnValue != 10)
								HasChangedMovement = true;
							ReturnValue = 10; 
							//Debug.Log("backward_right");
							return JoystickReturn.BACKWARD_RIGHT;
							
						}
						
						if ( angle < 2 && angle > 1 )
						{
							if ( ReturnValue != 5 )
								HasChangedMovement = true;
							ReturnValue = 5;   // straif left
							//Debug.Log("side_left");
							return JoystickReturn.SIDE_LEFT; //5; 
							
						}
						
						if ( angle > 0.5f && angle < 1 )
						{
							if (ReturnValue != 7)
								HasChangedMovement = true;
							ReturnValue = 7; 
							//Debug.Log("forward_left");
							return JoystickReturn.FORWARD_LEFT; 
							
						}
						
						if ( angle < -0.5f && angle > -1 )
						{
							if (ReturnValue != 8)
								HasChangedMovement = true;
							ReturnValue = 8; 
							//Debug.Log("forward_right");
							return JoystickReturn.FORWARD_RIGHT; 
							
						}
						
						if ( angle > -2 && angle < -1 )
						{
							if ( ReturnValue != 6 )
								HasChangedMovement = true;
							ReturnValue = 6;	//strafe right
							//Debug.Log("side_right");
							return JoystickReturn.SIDE_RIGHT; //6; 
							
						}
						
						if ( ReturnValue != 1 )
							HasChangedMovement = true;
						ReturnValue = 1; 
						//Debug.Log("forward");
						return JoystickReturn.FORWARD_A; //1; 
					}
				}
			}
			if(Input.GetMouseButtonUp(0))
			{
				pos2d = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
				pos = new Vector2(Input.mousePosition.x*2,Screen.height)-pos2d;
				//MonoBehaviour.print(pos);
				if(fingerNumber > -1)//(rect.Contains(pos))
				{
					fingerNumber = -1;
					pos -= rectCenter();
					//MonoBehaviour.print(-pos.y);
					radius = pos.magnitude;
					angle = Mathf.Acos(-pos.y/radius);
					if(pos.x>0)
						angle = -angle;
					//MonoBehaviour.print("released");
					ReturnValue = 2;
					return JoystickReturn.STOP; //2;
				}
			}
			if(Input.GetMouseButtonDown(0))
			{
				pos2d = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
				pos = new Vector2(Input.mousePosition.x*2,Screen.height)-pos2d;
				//MonoBehaviour.print(pos);
				//if(rect.Contains(pos))
				
				if((Input.mousePosition.y <= Screen.height*0.40f) && (Input.mousePosition.x <= Screen.width*0.24f))
					
				{
					setRectPos(pos.x-rect.width*0.5f, pos.y-rect.height*0.4f);
					//Debug.Log("pos: " + rectCenter() + " " + Input.mousePosition.x + " " + (Screen.height - Input.mousePosition.y));
					fingerNumber = 0;
					pos -= rectCenter();
					//MonoBehaviour.print(-pos.y);
					radius = pos.magnitude;
					angle = Mathf.Acos(-pos.y/radius);
					if(pos.x>0)
						angle = -angle;
					globalColor.a = 1.0f;
					//MonoBehaviour.print("released");
					return JoystickReturn.FORWARD_B; //3;
				}
			}
		}
		if((ReturnValue != 2 && Input.touchCount == 0) && ReturnValue > 0)
		{
			fingerNumber = -1;
			fingerID = -1;
			globalColor.a = 1.0f;
			ReturnValue = 2;
			return JoystickReturn.STOP; //2;
		}
		return 0;
	}
}