using UnityEngine;

//declare a new dimension var. create a property for it. init var in InitInterfaceDimensions using property
public class UID
{
	public static GUISkin skin;
	public static bool isHD ;
	static void SetFont(string path)
	{
		skin.textField.font = Resources.Load<Font> (path);	
		skin.textField.fontSize = 0;
		skin.label.font = skin.textArea.font;
		skin.button.font = skin.textArea.font;
		//skin.toggle.font = skin.textArea.font;
		//skin.box.font = skin.textArea.font;
		skin.textArea.font = skin.textField.font;
		skin.textArea.fontSize = 0;
	}
	
	public static void InitIntefaceDimensions()
	{
		LINE_SPACEING = Screen.height/10 * TEXT_SIZE + 1;
		CELL_SIZE = Screen.width * 0.08f < 64 ? Screen.width * 0.08f : 64;

		MITHRIL_WIDTH = Screen.width * 0.25f;	
		MITHRIL_HEIGHT = Screen.height * 0.1f;
		
        if (Screen.width < 768){
			UID.SetFont("fonts/Fontin-Regular");
		}
		else
		{
			if(Screen.width < 1024){
				UID.SetFont("fonts/Fontin-Regular_35");
			}
			else{
				if(Screen.width<1500){
					UID.SetFont("fonts/Fontin-Regular_40");
				}
				else{
					UID.SetFont("fonts/Fontin-Regular_50");
					Debug.Log("is iPad 3");
					isHD = true;
				}
			}
		}
	}
	
#region	UICell Dimensions
	

	public static float TAB = Screen.width*0.01f;
	public static float TAB_UP = Screen.height*0.25f;
	public static float TEXT_SIZE = Screen.height*0.001f;
	
	static float mithrilWidth;
	public static float MITHRIL_WIDTH
	{
		set{	mithrilWidth = value;	}
		get{	return mithrilWidth;	}
	}

	static float mithrilHeight;
	public static float MITHRIL_HEIGHT
	{
		set{	mithrilHeight = value;	}
		get{	return mithrilHeight;	}
	}
	
	static float lineSpacing; 
	public static float LINE_SPACEING
	{
		//corection based on used platform. 
		set 
		{ 
#if UNITY_IPHONE			
			/*
			iPhone.generation == iPhoneGeneration.iPhone 
			iPhone.generation == iPhoneGeneration.iPhone3G 
			iPhone.generation == iPhoneGeneration.iPodTouch1Gen 
			iPhone.generation == iPhoneGeneration.iPodTouch2Gen
			 */
			lineSpacing = value;
#endif
#if UNITY_ANDROID			
			lineSpacing = value*1.0f;//just for test
#endif
			
#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER		
			lineSpacing = value*1.0f;//just for test
#endif			
		
		}
		
		get { return lineSpacing; }
	}

	static float cellSize;
	public static float CELL_SIZE
	{
	//corection based on used platform
		set 
		{ 
#if UNITY_IPHONE			
			cellSize = value;
#endif
#if UNITY_ANDROID			
			cellSize = value*1.0f;//just for test
#endif
			
#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER		
			cellSize = value*1.0f;//just for test
#endif			
		}
		
		get { return cellSize; }
	}	
	
	
#endregion
	
}