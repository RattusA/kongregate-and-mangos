using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
	public float ProjectileSpeed = 0f;
	public string SoundEffectName = "";
	public AudioClip audioSoundEffectName;
	//public string SoundName = "";
	
	//private vars
	private Transform _target;
	private float _timer = 0f;
	private float _timeToDie = 25f;
	private bool _isReady = false;
	private AudioSource _audio;
	private bool _hitTarget = false;
	
	
	//Vector3 velo;
	
	public Transform Target
	{
		set
		{
			_target = value;
			transform.LookAt(_target.position);
		}
		get 
		{
			return _target;
		}
		
	}
	
	public float DelayTime
	{
		set
		{
			Debug.Log("we are setting the timer: " + value);
			_timer = value;
		}
		get
		{
			return _timer;
		}
	}
	
	public void EnableRenderer (bool val)
	{
		foreach(MeshRenderer go in gameObject.GetComponentsInChildren(typeof(MeshRenderer)))
		{
			go.enabled = val;
		}
	}
	
	public void Launch ()
	{
		if(_isReady)
		{
			return;
		}
		ProjectileSpeed = ProjectileSpeed == 0 ? 15 : ProjectileSpeed;
		EnableRenderer(true);
		//Debug.Log("LANSEAZA PROIECTILUL!");
		_isReady = true;
		Debug.Log("Time when the projectile flies towards the targetis ... " + Time.time + " and the distance is: " + Vector3.Distance(transform.position, Target.GetComponent<Collider>().bounds.center));
	}
	
	// Use this for initialization
	void Start ()
	{
		if(audioSoundEffectName == null)
		{
			audioSoundEffectName = Resources.Load<AudioClip> ("Sounds/SoundFX/default");
		}
		if(MusicManager.EffectsEnabled)
		{
			if(!gameObject.GetComponent<AudioSource>())
			{
				_audio = gameObject.AddComponent<AudioSource> ();
				_audio.volume = MusicManager.EffectVolume;
				_audio.clip = audioSoundEffectName;
				_audio.playOnAwake = false;
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(_timer > 0)
		{
			_timer -= Time.deltaTime;
		}
		else
		{
			Launch();
		}
		
		if(!_isReady)
		{
			return;
		}
		//Debug.Log("collider properties: " + " name: " + _target.name + " " + _target.collider.bounds.center + " size: " + _target.collider.bounds.size);
		
		if(_target == null)
		{
			DestroyImmediate(gameObject);
		}
		
		transform.position = Vector3.MoveTowards(transform.position, _target.GetComponent<Collider>().bounds.center, Time.deltaTime * ProjectileSpeed);
		
		if(Vector3.Distance(transform.position, _target.GetComponent<Collider>().bounds.center) <= _target.GetComponent<Collider>().bounds.size.x)
		{
			if(MusicManager.EffectsEnabled)
			{
				if(!_hitTarget)
				{
					EnableRenderer(false);
					_audio.Play();
					_hitTarget = true;
				}
			}
			
			DestroyImmediate(gameObject);
		}
	}
	
	void LateUpdate ()
	{
		if(_hitTarget && MusicManager.EffectsEnabled && !_audio.isPlaying)
		{
			DestroyImmediate(gameObject);
		}
	}
}
