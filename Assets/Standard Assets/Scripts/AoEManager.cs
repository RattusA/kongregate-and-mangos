﻿using UnityEngine;

public class AoEManager : MonoBehaviour
{
	private Swipe swipeObject;
	public Vector3 lastPosition;
	private float startTimer = 0;
	public Transform marker;

	void Awake()
	{
//		lastPosition = gameObject.transform.position;
		swipeObject = Camera.main.GetComponent<Swipe>();
	}

	void  LateUpdate ()
	{
		if(!swipeObject.isUsedSelectingAoEPosition() || ((startTimer > 0) && (Time.time - startTimer > 2)))
		{
			swipeObject.DisableSelectingAoEPosition();
			GameObject.Destroy(gameObject);
		}
	}
	
	public void  MoveToPosition ( Vector3 pos  )
	{
		gameObject.transform.position = pos;
	}
	
	public void  DisableMarker ()
	{
		marker.gameObject.SetActive(false);
	}
	
	public void  StartTimer ()
	{
		startTimer = Time.time;
	}
}