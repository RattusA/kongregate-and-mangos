﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Macs;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.Utilities.Encoders;



public class AuthCrypt {

	public static byte SEED_KEY_SIZE = 16;

	private bool _initialized = false;
	
	private IStreamCipher _decrypt = new RC4Engine();
	private IStreamCipher _encrypt = new RC4Engine();
	private byte[] _syncBuf;
	
	public void Init(BigInteger K)
	{
		byte[] ClientDecryptionKey = Hex.Decode("CC98AE04E897EACA12DDC09342915357");
		byte[] ServerEncryptionKey = Hex.Decode("C2B3723CC6AED9B5343C53EE2F4367CE");
		
		IDigest seDigest = new Sha1Digest();
		IDigest cdDigest = new Sha1Digest();
		
		IMac serverEncryptHmac = new HMac(seDigest);
		IMac clientDecryptHmac = new HMac(cdDigest);
		
		KeyParameter cdKey = new KeyParameter(ClientDecryptionKey);
		KeyParameter seKey = new KeyParameter(ServerEncryptionKey);
		
		serverEncryptHmac.Init(cdKey);
		clientDecryptHmac.Init(seKey);
		
		byte[] decryptHash = new byte[20];
		byte[] encryptHash = new byte[20];
		
		byte[] K_bytes = K.getBytes();
		
		serverEncryptHmac.BlockUpdate(K_bytes, 0, K_bytes.Length);
		serverEncryptHmac.DoFinal(decryptHash, 0);
		
		clientDecryptHmac.BlockUpdate(K_bytes, 0, K_bytes.Length);
		clientDecryptHmac.DoFinal(encryptHash, 0);
		
		ICipherParameters dKey = new KeyParameter(decryptHash);
		ICipherParameters eKey = new KeyParameter(encryptHash);
		
		_decrypt.Init(false, dKey);
		_encrypt.Init(true, eKey);
		
		_syncBuf = new byte[1024];

		ClearSynkBuf();

		_encrypt.ProcessBytes(_syncBuf, 0, 1024, _syncBuf, 0);
		
		ClearSynkBuf();
		
		_decrypt.ProcessBytes(_syncBuf, 0, 1024, _syncBuf, 0);
		
		_initialized = true;
	}

	private void ClearSynkBuf()
	{
		for(int index = 0; index < 1024; index++)
		{
			_syncBuf[index] = 0;
		}
	}
	
	public byte[] DecryptRecv(byte[] data, int inOff, int inLen)
	{
		if (!_initialized)
		{
			return data;
		}
		
		byte[] outData = new byte[data.Length];    
		_decrypt.ProcessBytes(data, inOff, inLen, outData, inOff);
		return outData;
	}
	
	public byte[] EncryptSend(byte[] data, int inOff, int inLen)
	{
		if (!_initialized)
		{
			return data;
		}

		byte[] outData = new byte[data.Length];    
		_encrypt.ProcessBytes(data, inOff, inLen, outData, inOff);
		return outData;
	}
	
	public bool IsInitialized()
	{
		return _initialized;
	}
}
