﻿using System.Net.Sockets;
using System.Collections.Generic;

// State object for receiving data from remote device.
public class SocketStateObject
{
	// Client socket.
	public Socket workSocket = null;
	// Receive buffer.
	public const int bufferSize = 128;
	public byte[] buffer = new byte[bufferSize];
	public List<byte> bBuffer = new List<byte>();

	public ServerPktHeader pktHeader;
}