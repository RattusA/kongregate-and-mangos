using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Agreement.Srp;

public enum RealmFlags
{
	REALM_FLAG_NONE         = 0x00,
	REALM_FLAG_INVALID      = 0x01,
	REALM_FLAG_OFFLINE      = 0x02,
	REALM_FLAG_SPECIFYBUILD = 0x04,	// client will show realm version in RealmList screen in form "RealmName (major.minor.revision.build)"
	REALM_FLAG_UNK1         = 0x08,
	REALM_FLAG_UNK2         = 0x10,
	REALM_FLAG_NEW_PLAYERS  = 0x20,
	REALM_FLAG_RECOMMENDED  = 0x40,
	REALM_FLAG_FULL         = 0x80
}

public class SRealmInfo
{
	public byte icon;			// icon near realm
	public byte locked;			// added in 2.0.x
	public RealmFlags flags;			// realm flags (text color etc)
	public string name;			// Text zero terminated name of Realm in c++
	public string addr_port;	// Text zero terminated address of Realm ("ip:port") in c++
	public float population;	// 1.6 -> population value. lower == lower population and vice versa
	public byte chars_here;		// number of characters on this server
	public byte timezone;		// timezone
	public byte unknown;
	
	// optional, if REALM_FLAG_SPECIFYBUILD (0x04) is set
	public byte major_version;
	public byte minor_version;
	public byte bugfix_version;
	public ushort version_build;

	public void Copy(ByteBuffer pkt)
	{
		icon = pkt.Read();
		locked = pkt.Read();
		flags = (RealmFlags)pkt.Read();
		name = pkt.ReadString();
		addr_port = pkt.ReadString();
		population = pkt.ReadFloat();
		chars_here = pkt.Read();
		timezone = pkt.Read();
		unknown = pkt.Read();
	}
}

public class sAuthLogonChallenge_S
{
	public byte cmd;
	public byte unk2;
	public byte error;
	public byte[] B = new byte[32];
	public byte g_len;
	public byte[] g = new byte[1];
	public byte N_len;
	public byte[] N = new byte[32];
	public byte[] salt = new byte[32];
	public byte[] unk3 = new byte[16];
	
	public void Copy(ByteBuffer pkt)
	{
		int index;
		cmd = pkt.Read();
		unk2 = pkt.Read();
		error = pkt.Read();

		// reading B[]
		for(index = 0; index < 32; index++)
		{
			B[32 - index - 1] = pkt.Read();
		}

		g_len = pkt.Read();
		if(g_len > 1)
		{
			g = new byte[g_len];
		}

		//reading g[];
		for(index = 0; index < g_len; index++)
		{
			g[g_len - index - 1] = pkt.Read();
		}

		N_len = pkt.Read();

		// reading N;
		for(index = 0; index < N_len; index++)
		{
			N[N_len - index - 1] = pkt.Read();
		}

		//reading salt
		for(index = 0;index < 32; index++)
		{
			salt[32 - index - 1] = pkt.Read();
		}

		//reading unk3
		for(index = 0; index < 16; index++)
		{
			unk3[16 - index - 1] = pkt.Read();
		}
	}
}

public class sAuthLogonProof_S
{
	public byte cmd;
	public byte error;
	public byte[] M2 = new byte[20];
	public uint unk1;
	public uint unk2;
	public ushort unk3;

	public void Copy(ByteBuffer bb)
	{
		cmd = bb.Read();
		error = bb.Read();
		for(int index = 0; index < 20; index++)
		{
			M2[index] = bb.Read();
		}
		unk1 = (uint)bb.ReadType(4);
		unk2 = (uint)bb.ReadType(4);
		unk3 = (ushort)bb.ReadType(2);
	}
}


public class RealmSession : MonoBehaviour
{
	public static ArrayList Realms = new ArrayList();
	string _accname;
	string _accpass;
	byte[] _m2 = new byte[20];
	
	public byte[] InvertBytes(byte[] inBytes)
	{
		byte temp = 0;
		int selectedIndex = inBytes.Length - 1;
		for(int index = 0; index < inBytes.Length / 2; index++)
		{
			temp = inBytes[index];
			inBytes[index] = inBytes[selectedIndex];
			inBytes[selectedIndex] = temp;
			selectedIndex--;
		}
		return inBytes;
	}
	
	public void HandleRealmList(ByteBuffer pkt)
	{
		uint unk32;
		ushort listSize;

		// read packet id
		pkt.Read();
		// read size of packet data
		pkt.ReadReversed16bit();
		unk32 = (uint)pkt.ReadType(4);
		listSize = (ushort)pkt.ReadReversed16bit();

		if(listSize == 0)
		{
			return;
		}

		if(Realms != null)
		{
			Realms.Clear();
		}
		else
		{
			Realms = new ArrayList();
		}

		for(ushort index = 0; index < listSize; index++)
		{
			SRealmInfo realm = new SRealmInfo();
			realm.icon = pkt.Read();
			realm.locked = pkt.Read();
			realm.flags = (RealmFlags)pkt.Read();
			realm.name = pkt.ReadString();
			realm.addr_port = pkt.ReadString();
			realm.population = pkt.ReadFloat();
			realm.chars_here = pkt.Read();
			realm.timezone = pkt.Read();
			pkt.Read();

			if((realm.flags & RealmFlags.REALM_FLAG_SPECIFYBUILD) == RealmFlags.REALM_FLAG_SPECIFYBUILD)
			{
				realm.major_version = pkt.Read();
				realm.minor_version = pkt.Read();
				realm.bugfix_version = pkt.Read();
				realm.version_build = (ushort)pkt.ReadReversed16bit();
			}
			Realms.Add(realm);
		}
		pkt.ReadType(2);

		AuthorizationSocket.Instance.CloseSocket();
		if(GameManager.Instance != null)
		{
			GameManager.Instance.LoadScene("RealmWindow");
		}
		else
		{
			Application.LoadLevel("RealmWindow");
		}
		//LI.setRealms(_realms);
		//sockets.closeSocket();
	}

	public void HandleLogonChallenge(ByteBuffer pkt)
	{
		if(pkt.Size() < 3)
		{
			return;
		}
		
		sAuthLogonChallenge_S lc = new sAuthLogonChallenge_S();
		lc.error = (byte)pkt.Read(2);

		switch(lc.error)
		{
		case (byte)eAuthResults.REALM_AUTH_NO_MATCH:
			// log, ceva de gui
			break;
		case (byte)eAuthResults.REALM_AUTH_ACCOUNT_IN_USE:
			// idem
			break;
		case (byte)eAuthResults.REALM_AUTH_WRONG_BUILD_NUMBER:
			// idem
			break;
		case (byte)eAuthResults.REALM_AUTH_SUCCESS:	
			// se citesc date in lc
			lc.Copy(pkt);
			
			//After calling of the logIn() function we have account name here
			string username = LoginWindow.accountLogin.ToUpper();
			string userpass = LoginWindow.accountPassword.ToUpper();
			string _authstr = username + ":" + userpass;
			
			LoginWindow.passHash = LoginWindow.CalculateSHA1Hash(_authstr);
			
			BigInteger N = new BigInteger(1, lc.N);
			BigInteger B = new BigInteger(1, lc.B);
			BigInteger salt = new BigInteger(1, lc.salt);
			BigInteger unk1 = new BigInteger(1, lc.unk3);
			BigInteger g = new BigInteger(1, lc.g);
			
			BigInteger k = BigInteger.Three;
			
			byte[] I = Encoding.UTF8.GetBytes(username);
			byte[] UP = Encoding.UTF8.GetBytes(_authstr);
			
			SecureRandom random = new SecureRandom();
			byte[] sr = random.GenerateSeed(19);
			BigInteger a = new BigInteger(1, sr);
			
			IDigest userhash = new Sha1Digest();
			IDigest xhash = new Sha1Digest();
			IDigest uhash = new Sha1Digest();
			
			byte[] userhash_output = new byte[userhash.GetDigestSize()];
			byte[] xhash_output = new byte[xhash.GetDigestSize()];
			byte[] uhash_output = new byte[uhash.GetDigestSize()];
			
			userhash.BlockUpdate(UP);
			userhash.DoFinal(userhash_output, 0);
			
			xhash.BlockUpdate(salt.getBytes());
			xhash.BlockUpdate(userhash_output);
			xhash.DoFinal(xhash_output, 0);
			
			BigInteger x = new BigInteger(1, InvertBytes(xhash_output));
			BigInteger v = g.ModPow(x, N);
			BigInteger A = g.ModPow(a, N);
			
			byte[] A_inv = A.getBytes();												
			byte[] B_inv = B.getBytes();												
			
			uhash.BlockUpdate(A.getBytes());
			uhash.BlockUpdate(B.getBytes());
			uhash.DoFinal(uhash_output, 0);
			
			BigInteger u = new BigInteger(1, InvertBytes(uhash_output), 0, 20);
			BigInteger S = (B.Subtract(k.Multiply(v))).ModPow(a.Add(u.Multiply(x)), N);
			
			uint inx = 0;
			byte[] S1 = new byte[17];
			byte[] S2 = new byte[17];
			byte[] SB = S.getBytes();
			int index = (SB.Length < 16 * 2) ? (SB.Length / 2) : 16; //Anton Strul

			for(inx = 0; inx < index; inx++)
			{
				S1[inx] = SB[inx * 2];
				S2[inx] = SB[inx * 2 + 1];
			}
			
			Sha1Digest S1hash = new Sha1Digest();
			Sha1Digest S2hash = new Sha1Digest();
			
			byte[] S1hash_output = new byte[S1hash.GetDigestSize()];
			byte[] S2hash_output = new byte[S2hash.GetDigestSize()];
			
			S1hash.BlockUpdate(S1, 0, 16);
			S1hash.DoFinal(S1hash_output, 0);
			
			S2hash.BlockUpdate(S2, 0, 16);
			S2hash.DoFinal(S2hash_output, 0);
			
			byte[] S_hash = new byte[40];
			for(index = 0; index<20; index++)
			{
				S_hash[index * 2] = S1hash_output[index];
				S_hash[index * 2 + 1] = S2hash_output[index];
			}
			
			Global.sessionKey = new BigInteger(1, InvertBytes(S_hash), 0, 40); // used later when authing to world

			byte[] Ng_hash = new byte[20];
			
			Sha1Digest userhash2 = new Sha1Digest();
			Sha1Digest Nhash = new Sha1Digest();
			Sha1Digest ghash = new Sha1Digest();
			
			byte[] userhash2_output = new byte[userhash2.GetDigestSize()];
			byte[] Nhash_output = new byte[Nhash.GetDigestSize()];
			byte[] ghash_output = new byte[ghash.GetDigestSize()];
			
			userhash2.BlockUpdate(I);
			userhash2.DoFinal(userhash2_output, 0);
			
			Nhash.BlockUpdate(N.getBytes());
			Nhash.DoFinal(Nhash_output, 0);
			
			ghash.BlockUpdate(g.getBytes());
			ghash.DoFinal(ghash_output, 0);
			
			for(index = 0; index < 20; index++)
			{
				Ng_hash[index] = (byte)(Nhash_output[index] ^ ghash_output[index]);
			}
			
			BigInteger t_acc = new BigInteger(1, InvertBytes(userhash2_output));
			BigInteger t_Ng_hash = new BigInteger(1, InvertBytes(Ng_hash), 0, 20);
			
			Sha1Digest M1hash = new Sha1Digest();
			Sha1Digest M2hash = new Sha1Digest();
			
			byte[] M1hash_output = new byte[M1hash.GetDigestSize()];
			byte[] M2hash_output = new byte[M2hash.GetDigestSize()];
			
			M1hash.BlockUpdate(t_Ng_hash.getBytes());
			M1hash.BlockUpdate(t_acc.getBytes());
			M1hash.BlockUpdate(salt.getBytes());
			M1hash.BlockUpdate(A.getBytes());
			M1hash.BlockUpdate(B.getBytes());
			M1hash.BlockUpdate(InvertBytes(S_hash), 0, 40);
			M1hash.DoFinal(M1hash_output, 0);
			
			M2hash.BlockUpdate(A.getBytes());
			M2hash.BlockUpdate(M1hash_output);
			M2hash.BlockUpdate(S_hash, 0, 40);
			M2hash.DoFinal(M2hash_output, 0);
			
			// save M2 to an extern var to check it later
			for(index = 0; index < M2hash_output.Length; index++)
			{
				_m2[index] = M2hash_output[index];
			}
			
			ByteBuffer packet = new ByteBuffer();
			byte aux = (byte)AuthCmd.AUTH_LOGON_PROOF;
			packet.Append(aux);
			byte[] A_bytes = A.getBytes();
			
			packet.Append(A_bytes, A_bytes.Length);
			packet.Append(M1hash_output, M1hash_output.Length);
			aux = 0;		
			for(index = 0; index < 20; index++)
			{
				packet.Append(aux);
			}
			packet.Append(aux);
			packet.Append(aux);
			AuthorizationSocket.Instance.SendData(packet);
			break;
		}
	}
	
	public void HandleLogonProof(ByteBuffer pkt)
	{
		if(pkt.Size() < 2)
		{
			return;
		}

		byte error = (byte)pkt.Read(1);
		if(error != (byte)eAuthResults.REALM_AUTH_SUCCESS)
		{
			LoginWindow.SetInventoryMessage("Invalid login or password");
			LoginWindow.StopLogin = true;
			AuthorizationSocket.Instance.CloseSocket();
			return;
		}

		sAuthLogonProof_S lp = new sAuthLogonProof_S();
		lp.Copy(pkt);

		if(BitConverter.ToString(_m2) != BitConverter.ToString(lp.M2))
		{
			Debug.LogError("Auth failed, M2 differs!");
			AuthorizationSocket.Instance.CloseSocket();
			return;
		}
		
		ByteBuffer packet = new ByteBuffer();
		byte aux = (byte)AuthCmd.REALM_LIST;
		packet.Append(aux);
		uint intAux = 0;
		packet.Append(intAux);
		AuthorizationSocket.Instance.SendData(packet);

		if(KongregateManager.Instance != null && !KongregateManager.Instance.isBindedAccount)
		{
			BindAccountWithCongregate();
		}
	}

	public void HandleTransferInit(ByteBuffer pkt)
	{
		return;/// no need for this function now
	}

	public void HandleTransferData(ByteBuffer pkt)
	{
		return;/// no need for this function now
	}
	
	public void HandleCreateAccData(ByteBuffer pkt)
	{
		int size_test = pkt.Size();
		if(pkt.Size() < 3)
		{
			return;
		}
		
		sAuthLogonChallenge_S lc = new sAuthLogonChallenge_S();
		lc.error = (byte)pkt.Read(2);
		LoginWindow.Instance.registerHandler.ChangeRegisterState((eAuthResults)lc.error);
		switch(lc.error)
		{
		case (byte)eAuthResults.REALM_AUTH_REGISTRATION_NOT_AVALIABLE:
			//TODO  eAuthResults.REALM_AUTH_REGISTRATION_NOT_AVALIABLE
			break;
		case (byte)eAuthResults.REALM_AUTH_ACCOUNT_ALREADY_EXIST:
			//name is busy
			//TODO eAuthResults.REALM_AUTH_ACCOUNT_ALREADY_EXIST
			break;
		case (byte)eAuthResults.REALM_AUTH_NO_MATCH:
			// log, ceva de gui
			break;
		case (byte)eAuthResults.REALM_AUTH_ACCOUNT_IN_USE:
			// idem
			break;
		case (byte)eAuthResults.REALM_AUTH_WRONG_BUILD_NUMBER:
			// idem
			break;
		case (byte)eAuthResults.REALM_AUTH_SUCCESS:	
			// se citesc date in lc
			lc.Copy(pkt);
			string username = LoginWindow.accountLogin.ToUpper();
			string userpass = LoginWindow.accountPassword.ToUpper();
			LoginWindow.accountLogin = "";
			LoginWindow.accountPassword = "";
			string _authstr = username + ":" + userpass;
			
			LoginWindow.passHash = LoginWindow.CalculateSHA1Hash(_authstr);
			ByteBuffer packet = new ByteBuffer();
			byte aux = (byte)AuthCmd.CMD_CREATE_ACC_PROOF;
			packet.Append(aux);
			
			aux = (byte)LoginWindow.passHash.Length;
			packet.Append(aux);
			packet.AppendSTR(LoginWindow.passHash);
			AuthorizationSocket.Instance.SendData(packet);
			break;
		}
	}
	
	public void HandleCreateAccProof(ByteBuffer pkt)
	{
		int size_test = pkt.Size();
		if(pkt.Size() < 3)
		{
			return;
		}
		
		sAuthLogonChallenge_S lc = new sAuthLogonChallenge_S();
		lc.error = (byte)pkt.Read(2);
		LoginWindow.Instance.registerHandler.ChangeRegisterState((eAuthResults)lc.error);
		switch(lc.error)
		{
		case (byte)eAuthResults.REALM_AUTH_ACCOUNT_ALREADY_EXIST:
			//name is busy
			//TODO eAuthResults.REALM_AUTH_ACCOUNT_ALREADY_EXIST
			break;
		case (byte)eAuthResults.REALM_AUTH_SUCCESS:	
			//ok
			//TODO Close reg form and start game
			Debug.Log("Create acc OK");				
			break;
		}
	}
	
	public void HandleTemporaryAcc(ByteBuffer pkt)
	{
		int size_test = pkt.Size();
		if(pkt.Size() < 3)
		{
			return;
		}
		
		byte bt1 = pkt.Read(); // packet id
		byte bt2 = pkt.Read(); // unk2
		byte error = pkt.Read();  // error
		
		switch(error)
		{
		case (byte)eAuthResults.REALM_AUTH_ACCOUNT_ALREADY_EXIST:
			//name is busy
			//TODO eAuthResults.REALM_AUTH_ACCOUNT_ALREADY_EXIST
			break;
		case (byte)eAuthResults.REALM_AUTH_NO_MATCH:
			// log, ceva de gui
			break;
		case (byte)eAuthResults.REALM_AUTH_ACCOUNT_IN_USE:
			// idem
			break;
		case (byte)eAuthResults.REALM_AUTH_WRONG_BUILD_NUMBER:
			// idem
			break;
		case (byte)eAuthResults.REALM_AUTH_SUCCESS:	
			// se citesc date in lc
			byte nameSize = pkt.Read(); // name size
			byte[] buffer = new byte[nameSize];
			pkt.Read (buffer, nameSize);
			string accName = Encoding.Default.GetString(buffer);

			byte passSize = pkt.Read(); // password size
			buffer = new byte[passSize];
			pkt.Read (buffer, passSize);
			string password = Encoding.Default.GetString(buffer);
			uint intAux = 0;
			
			LoginWindow.accountLogin = accName;
			LoginWindow.accountPassword = password;
			
			Debug.Log( "_HandleCreateTempAccData NAME = " + accName );
			
			PlayerPrefs.SetString(LoginWindow.TEMP_ACC_NAME, accName);
			PlayerPrefs.SetString(LoginWindow.TEMP_ACC_PASS, password);
			PlayerPrefs.Save();
			
			ByteBuffer packetSend = new ByteBuffer();
			byte aux = (byte)AuthCmd.CMD_TEMPORARY_ACC_PROOF;
			packetSend.Append(aux);
			packetSend.Append(intAux);
			AuthorizationSocket.Instance.SendData(packetSend);
			
			LoginWindow.Instance.HandleOnLogInButtonClocked();
			break;
		}
	}

	public void HandleAuthKongregate(ByteBuffer pkt)
	{
		KongregateManager.Log ("Start HandleAuthKongregate");
		int size_test = pkt.Size();
		if(pkt.Size() < 3)
		{
			return;
		}
		byte bt1 = pkt.Read(); // packet id
		byte bt2 = pkt.Read(); // unk2
		byte error = pkt.Read();  // error
		
		switch(error)
		{
		case (byte)eAuthResults.WOW_FAIL_KONGRIDATE_AUTH:
			LoginWindow.SetInventoryMessage("Auth: FAIL_KONGRIDATE_AUTH");
			if(KongregateManager.Instance != null)
			{
				KongregateManager.Instance.isBindedAccount = false;
			}
			LoginWindow.Instance.loginPanel.SetActive(true);
			LoginWindow.Instance.kongregatePanel.SetActive(false);
			break;
		case (byte)eAuthResults.WOW_FAIL_KONGRIDATE_ALREADY_BOUND:
			LoginWindow.SetInventoryMessage("Auth: FAIL_KONGRIDATE_ALREADY_BOUND");
			break;
		case (byte)eAuthResults.WOW_FAIL_KONGRIDATE_ID:
			LoginWindow.SetInventoryMessage("Auth: FAIL_KONGRIDATE_ID");
			if(KongregateManager.Instance != null)
			{
				KongregateManager.Instance.isBindedAccount = false;
			}
			LoginWindow.Instance.loginPanel.SetActive(true);
			LoginWindow.Instance.kongregatePanel.SetActive(false);
			break;
		case (byte)eAuthResults.REALM_AUTH_SUCCESS:
			if(LoginWindow.Instance != null)
			{
				LoginWindow.Instance.LogIn("", "");
			}
			break;
		}
	}
	
	public void HandleBindKongregate(ByteBuffer pkt)
	{
		KongregateManager.Log ("Start HandleBindKongregate");
		int size_test = pkt.Size();
		if(pkt.Size() < 3)
		{
			return;
		}
		byte bt1 = pkt.Read(); // packet id
		byte bt2 = pkt.Read(); // unk2
		byte error = pkt.Read();  // error
		
		switch(error)
		{
		case (byte)eAuthResults.WOW_FAIL_KONGRIDATE_AUTH:
			LoginWindow.SetInventoryMessage("Bind: FAIL_KONGRIDATE_AUTH");
			break;
		case (byte)eAuthResults.WOW_FAIL_KONGRIDATE_ALREADY_BOUND:
			LoginWindow.SetInventoryMessage("Bind: FAIL_KONGRIDATE_ALREADY_BOUND");
			break;
		case (byte)eAuthResults.WOW_FAIL_KONGRIDATE_ID:
			LoginWindow.SetInventoryMessage("Bind: FAIL_KONGRIDATE_ID");
			break;
		case (byte)eAuthResults.REALM_AUTH_SUCCESS:	
			break;
		}
	}

	void BindAccountWithCongregate()
	{
		KongregateManager.Log ("Start CMD_BIND_KONGREGATE");
		ByteBuffer packet = new ByteBuffer();
		uint userId = uint.Parse(KongregateManager.userId);
		string token = KongregateManager.gameAuthToken;
		byte tokenLength = (byte)token.Length;
		ushort packetSize = (ushort)(sizeof(uint) + sizeof(byte) + tokenLength);
		byte aux = (byte)AuthCmd.CMD_BIND_KONGREGATE;
		packet.Append(aux);
		packet.Append(packetSize);
		packet.Append(userId);
		packet.Append(tokenLength);
		packet.AppendSTR(token);
		AuthorizationSocket.Instance.SendData(packet);
	}

	public void ManagePkt(ByteBuffer pkt)
	{
		byte flag = (byte)pkt.Read(0);
		switch(flag)
		{
		case (byte)AuthCmd.AUTH_LOGON_CHALLENGE :
			HandleLogonChallenge(pkt);
			break;
		case (byte)AuthCmd.AUTH_LOGON_PROOF :
			HandleLogonProof(pkt);
			break;
		case (byte)AuthCmd.REALM_LIST :
			HandleRealmList(pkt);
			break;
		case (byte)AuthCmd.XFER_INITIATE :
			HandleTransferInit(pkt);
			break;
		case (byte)AuthCmd.XFER_DATA :
			HandleTransferData(pkt);
			break;
		case (byte)AuthCmd.CMD_CREATE_ACC_CHALLENGE :
			HandleCreateAccData(pkt);
			break;
		case (byte)AuthCmd.CMD_CREATE_ACC_PROOF :
			HandleCreateAccProof(pkt);
			break;
		case (byte)AuthCmd.CMD_TEMPORARY_ACC:
			HandleTemporaryAcc(pkt);
			break;
		case (byte)AuthCmd.CMD_AUTH_KONGREGATE:
			HandleAuthKongregate(pkt);
			break;
		case (byte)AuthCmd.CMD_BIND_KONGREGATE:
			HandleBindKongregate(pkt);
			break;
		}
	}
}
