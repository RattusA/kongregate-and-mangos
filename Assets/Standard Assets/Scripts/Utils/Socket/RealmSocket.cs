using System;
using System.Collections;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using UnityEngine;

using Org.BouncyCastle.Math;

public class RealmSocket : MonoBehaviour
{
	public static ArrayList inQueue = new ArrayList();
	public static ArrayList outQueue = new ArrayList();
	public static bool initCrypt = false;
	public static BigInteger cryptKey = BigInteger.Zero;

	private static Socket client;
	public static WorldSession world;

	public static bool IsRaidState = false;
	public static bool IsGuildState = false;
	public static bool IsFriendState = false;

	protected static int FIRST_INDEX = 0;
	
	private static AuthCrypt _crypt;
	
	// Async connect variables
	// ManualResetEvent instances signal completion.
	private static ManualResetEvent connectDone;

	private void Awake()
	{
		connectDone = new ManualResetEvent(false);
		if(WorldSession.istance == null)
		{
			world = gameObject.AddComponent<WorldSession>();
		}
		else
		{
			world = WorldSession.istance;
		}
	}
	
	private static void InitValues()
	{
		_crypt = new AuthCrypt();
	}
	
	public void Start()
	{
		string nume = name;
		name = "ion";
		if(GameObject.Find(nume))
		{
			Destroy(gameObject);
		}
		else
		{
			name = nume;
			DontDestroyOnLoad(gameObject);
		}
	}
	
	void Update() 
	{
		try
		{
			world.Update();
		}
		catch(System.Exception e)
		{
			Debug.Log(e.ToString());
		}

		while(inQueue.Count > 0 && !Defines.loading)
		{
			world.handlePkt((WorldPacket)inQueue[FIRST_INDEX]);
			inQueue.RemoveAt(FIRST_INDEX);
		}

		while(outQueue.Count > 0 && !Defines.loading)
		{
			SendPkt((WorldPacket)outQueue[FIRST_INDEX]);
			outQueue.RemoveAt(FIRST_INDEX);
		}

		if(initCrypt)
		{
			_crypt.Init(cryptKey);
			initCrypt = false;
		}
	}

	public bool IsLoseConnect()
	{
		bool ret = false;
		if(client != null && !client.Connected)
		{
//			CloseSocket();
			ret = true;
		}
		return ret;
	}

	public static WorldPacket GetInQueuePacket()
	{
		WorldPacket ret = null;
		if(inQueue.Count > 0 && !Defines.loading)
		{
			ret = (WorldPacket)inQueue[FIRST_INDEX];
			inQueue.RemoveAt(FIRST_INDEX);
		}
		return ret;
	}
	
	public void Connect(string address)
	{
		CloseSocket();
		// Connect to the remote endpoint.
		client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		string[] aux = Regex.Split(address, ":");
		string ip = aux[0];
		int port = int.Parse(aux[1]);
		Security.PrefetchSocketPolicy(ip, port);
		client.BeginConnect( ip, port, new AsyncCallback(OnConnected), client);
		connectDone.WaitOne();
	}
	
	private void OnConnected(IAsyncResult ar)
	{
		try
		{
			// Retrieve the socket from the state object.
			Socket workSocket = (Socket) ar.AsyncState;
			
			// Complete the connection.
			workSocket.EndConnect(ar);
			InitValues();
		}
		catch (Exception e)
		{
			print(e);
//			Debug.LogException(e);
			if(client != null)
			{
				client.Shutdown(SocketShutdown.Both);
				client.Close();
				client = null;
			}
			LoginWindow.SetInventoryMessage("Not connected");
		}
		
		// Signal that the connection has been made.
		connectDone.Set();
		
		Receive();
	}
	
	private void Receive()
	{
		if(client != null && client.Connected)
		{
			try
			{
				// Create the state object.
				SocketStateObject state = new SocketStateObject();
				state.workSocket = client;
				
				// Begin receiving the data from the remote device.
				WaitDataFromStream(state);
			}
			catch (Exception e)
			{
				print(e);
//				Debug.LogException(e);
			}
		}

	}
	
	private void OnReceived( IAsyncResult ar )
	{
		if(client != null && client.Connected)
		{
			try
			{
				SocketError errorCode;
				// Retrieve the state object and the client socket 
				// from the asynchronous state object.
				SocketStateObject state = (SocketStateObject) ar.AsyncState;
				Socket workSocket = state.workSocket;
				
				// Read data from the remote device.
				int bytesRead = workSocket.EndReceive(ar, out errorCode);

				if (bytesRead > 0)
				{
					// read stream buffer
					byte[] shortBuffer = new byte[bytesRead];
					Array.Copy(state.buffer, shortBuffer, bytesRead);
					state.bBuffer.AddRange(shortBuffer);

					PacketProcessing(state);
				}
			}
			catch (Exception e)
			{
				Debug.LogException(e);
			}
		}
	}

	private void WaitDataFromStream(SocketStateObject state)
	{
		client.BeginReceive(state.buffer, 0, SocketStateObject.bufferSize, SocketFlags.None,
		                    new AsyncCallback(OnReceived), state);
	}
	
	private void PacketProcessing(SocketStateObject state)
	{
		if((state.bBuffer.Count < 4) || (state.pktHeader != null && state.pktHeader.GetRealSize() > state.bBuffer.Count))
		{
			WaitDataFromStream(state);
		}
		else
		{
			if(state.pktHeader == null)
			{
				byte firstSizeByte = state.bBuffer[0];
				
				byte[] firstSizeByte_b = new byte[1];
				firstSizeByte_b[0] = firstSizeByte;
				firstSizeByte_b = _crypt.DecryptRecv(firstSizeByte_b, 0 ,1);
				firstSizeByte = firstSizeByte_b[0];
				
				ServerPktHeader hdr = new ServerPktHeader();
				state.pktHeader = hdr;
				if((byte)(firstSizeByte & 0x80) != 0)	//big pkt
				{
					byte[] data_b = new byte[4];
					
					data_b[0] = state.bBuffer[1];
					data_b[1] = state.bBuffer[2];
					data_b[2] = state.bBuffer[3];
					data_b[3] = state.bBuffer[4];

					data_b = _crypt.DecryptRecv(data_b, 0, 4);
					hdr.SetSize((byte)(firstSizeByte & 0x7F), data_b[0], data_b[1]);
					hdr.cmd = BitConverter.ToUInt16(data_b, 2);
				}
				else // normal pkt
				{
					byte[] data = new byte[3];
					
					data[0] = state.bBuffer[1];
					data[1] = state.bBuffer[2];
					data[2] = state.bBuffer[3];

					data = _crypt.DecryptRecv(data, 0, 3);
					hdr.SetSize(0, firstSizeByte, data[0]);
					hdr.cmd = BitConverter.ToUInt16(data, 1);
				}
			}

			int remaining = state.pktHeader.GetRealSize();
			if(state.bBuffer.Count < remaining)
			{
				WaitDataFromStream(state);
			}
			else
			{
				WorldPacket pkt = new WorldPacket();
				if(remaining > 0)
				{
					byte aux;
					int index = (state.pktHeader.isLargePacket ()) ? 5 : 4;
					for(; index < remaining; index++)
					{
						aux = state.bBuffer[index];
						pkt.Append(aux);
					}
				}
				ushort _opcode = state.pktHeader.cmd;
				pkt.SetOpcode(_opcode);
				inQueue.Add(pkt);
				state.pktHeader = null;
				state.bBuffer.RemoveRange(0, remaining);
				PacketProcessing(state);
			}
		}
	}

	private void Send(ByteBuffer data)
	{
		if(client != null && client.Connected)
		{
			byte[] byteData = data.Contents();
			
			// Begin sending the data to the remote device.
			client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(OnSended), client);
		}
	}
	
	private void OnSended(IAsyncResult ar)
	{
		if(client != null && client.Connected)
		{
			try
			{
				// Retrieve the socket from the state object.
				Socket workSocket = (Socket) ar.AsyncState;
				
				// Complete sending the data to the remote device.
				int bytesSent = workSocket.EndSend(ar);
			}
			catch (Exception e)
			{
				Debug.LogException(e);
				if(GameManager.Instance != null)
				{
					GameManager.Instance.LoadScene("LoginWindow");
				}
				else
				{
					Application.LoadLevel("LoginWindow");
				}
			}
		}
	}
	
	private void SendPkt(WorldPacket pkt)
	{
		if(client != null)
		{
			ClientPktHeader hdr = new ClientPktHeader();
			ushort auxUI16 = (ushort)(pkt.Size() + 4);
			hdr.size = ByteBuffer.ntohs(auxUI16);
			hdr.cmd = pkt.GetOpcode();
			hdr.nil = 0;
			
			byte[] data = new byte[6];
			byte[] ba_size = BitConverter.GetBytes(hdr.size);
			byte[] ba_cmd = BitConverter.GetBytes(hdr.cmd);
			byte[] ba_nil = BitConverter.GetBytes(hdr.nil);
			data[0] = ba_size[0];
			data[1] = ba_size[1];
			data[2] = ba_cmd[0];
			data[3] = ba_cmd[1];
			data[4] = ba_nil[0];
			data[5] = ba_nil[1];
			data = _crypt.EncryptSend(data, 0, 6);
			hdr.size = BitConverter.ToUInt16(data, 0);
			hdr.cmd = BitConverter.ToUInt16(data, 2);
			hdr.nil = BitConverter.ToUInt16(data, 4);
			
			ByteBuffer final = new ByteBuffer();
			final.Append(hdr.size);
			final.Append(hdr.cmd);
			final.Append(hdr.nil);

			if(pkt.Size() > 0)
			{
				final.Append(pkt.Contents(), pkt.Size());
			}
			Send(final);
		}
	}
	
	public void OnApplicationQuit()
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_LOGOUT_REQUEST, 0);
		SendPkt(pkt);
		CloseSocket();
	}
	
	public void CloseSocket()
	{
		// Release the socket.
		if(client != null && client.Connected)
		{
			client.Shutdown(SocketShutdown.Both);
			client.Close();
		}
		client = null;
	}
}