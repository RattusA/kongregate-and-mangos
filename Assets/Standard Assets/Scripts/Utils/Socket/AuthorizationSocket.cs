﻿using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class AuthorizationSocket : MonoBehaviour
{
	private static RealmSession Realm;
	
	private static Socket client;					//udp

	private static bool Connected = false;
	private static ArrayList Vector = new ArrayList();

	private delegate void OnConnect();
	private OnConnect connectCallback;
	public delegate void OnLoginCallback(string account, string password);
	private static OnLoginCallback LoginCallback;
	private static string LoginCallbackAccount = "";
	private static string LoginCallbackPassword = "";
	public delegate void OnEndRefreshConnection(string objName);
	private static OnEndRefreshConnection EndRefreshConnection;
	private static string EndRefreshConnectionParametr = "";

	// Async connect variables
	// ManualResetEvent instances signal completion.
	private static ManualResetEvent connectDone;
	private static bool isReciveCall;

	private static AuthorizationSocket instance;
	public static AuthorizationSocket Instance
	{
		get
		{
			if(instance == null)
			{
				GameObject go = new GameObject("AuthorizationSocket");
				instance = go.AddComponent<AuthorizationSocket>();
			}
			return instance;
		}
	}

	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		Realm = gameObject.AddComponent<RealmSession>();
		connectDone = new ManualResetEvent(false);
		isReciveCall = false;
	}

	private void Update()
	{
		int startIndex = 0;
		while(Vector.Count > 0)
		{
			ByteBuffer bb = (ByteBuffer)Vector[startIndex];
			Vector.RemoveAt(startIndex);
			Realm.ManagePkt(bb);
			bb.RPos(0);
		}
	}

	public long GetLongLocalIpAddress()
	{
		try
		{
			long intAddress = BitConverter.ToInt32(((IPEndPoint)client.LocalEndPoint).Address.GetAddressBytes(), 0);
			return intAddress;
		}
		catch(Exception exception)
		{
			LoginWindow.SetInventoryMessage("Not connected");
			Debug.Log("CONNECTION EXCEPTION: " + exception);
			return 0;
		}
	}

	public void Connect()
	{
		KongregateManager.Log ("Start connect");

		CloseSocket();
		// Connect to the remote endpoint.
		LoginWindow.SetInventoryMessage("Waiting to connect", 3f);
		Security.PrefetchSocketPolicy(Defines.connectIp, Defines.connectPort);
		client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		client.BeginConnect( Defines.connectIp, Defines.connectPort, new AsyncCallback(OnConnected), client);
		connectDone.WaitOne();
		KongregateManager.Log ("End connect");
	}
	
	public void SendData(ByteBuffer info)
	{
		Send (info);
	}

	public void OnApplicationQuit()
	{
		CloseSocket();
	}

	public void CloseSocket()
	{
		// Release the socket.
		if(client != null && client.Connected)
		{
			client.Shutdown(SocketShutdown.Both);
			client.Close();
		}
		client = null;
	}

	public void SetLoginCallback(OnLoginCallback callback, string account, string password)
	{
		LoginCallbackAccount = account;
		LoginCallbackPassword = password;
		LoginCallback = callback;
	}

	public void SetRefreshConnectCallback(OnEndRefreshConnection callback, string objName)
	{
		EndRefreshConnectionParametr = objName;
		EndRefreshConnection = callback;
	}

	private void OnConnected(IAsyncResult ar)
	{
		KongregateManager.Log ("Start OnConnected");
		try
		{
			LoginWindow.SetInventoryMessage("Connected", 0.1f);
			// Retrieve the socket from the state object.
			Socket workSocket = (Socket) ar.AsyncState;
			
			// Complete the connection.
			workSocket.EndConnect(ar);
		}
		catch (Exception e)
		{
			Debug.LogException(e);
			Connected = false;
			if(client != null)
			{
				client.Shutdown(SocketShutdown.Both);
				client.Close();
				client = null;
			}
			LoginWindow.SetInventoryMessage("Not connected");
		}
		
		// Signal that the connection has been made.
		connectDone.Set();

		if(EndRefreshConnection != null)
		{
			EndRefreshConnection(EndRefreshConnectionParametr);
		}
		EndRefreshConnection = null;

		Receive();

		KongregateManager.Log ("Start LoginCallback");
		if(LoginCallback != null)
		{
			LoginCallback(LoginCallbackAccount, LoginCallbackPassword);
		}
		LoginCallback = null;
		KongregateManager.Log ("End OnConnected");
	}

	private void Receive()
	{
		if( !isReciveCall )
		{
			if(client != null && client.Connected)
			{
				try
				{
					// Create the state object.
					SocketStateObject state = new SocketStateObject();
					state.workSocket = client;
					
					// Begin receiving the data from the remote device.
					WaitDataFromStream(state);
				}
				catch (Exception e)
				{
					Debug.LogException(e);
				}
			}
		}
	}
	
	private void OnReceived( IAsyncResult ar )
	{
		if(client != null && client.Connected)
		{
			try
			{
				SocketError errorCode;
				// Retrieve the state object and the client socket 
				// from the asynchronous state object.
				SocketStateObject state = (SocketStateObject) ar.AsyncState;
				Socket workSocket = state.workSocket;
				
				// Read data from the remote device.
				int bytesRead = workSocket.EndReceive(ar, out errorCode);

				if (bytesRead > 0)
				{
					// read stream buffer
					byte[] shortBuffer = new byte[bytesRead];
					Array.Copy(state.buffer, shortBuffer, bytesRead);
					state.bBuffer.AddRange(shortBuffer);
					
					PacketProcessing(state);
				}
			}
			catch (Exception e)
			{
				Debug.LogException(e);
			}
		}
	}
	
	private void WaitDataFromStream(SocketStateObject state)
	{
		client.BeginReceive(state.buffer, 0, SocketStateObject.bufferSize, SocketFlags.None,
		                    new AsyncCallback(OnReceived), state);
	}
	
	private void Send(ByteBuffer data)
	{
		if(client != null && client.Connected)
		{
			byte[] byteData = data.Contents();

			// Begin sending the data to the remote device.
			client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(OnSended), client);
		}
	}
	
	private void OnSended(IAsyncResult ar)
	{
		if(client != null && client.Connected)
		{
			try
			{
				// Retrieve the socket from the state object.
				Socket workSocket = (Socket) ar.AsyncState;
				
				// Complete sending the data to the remote device.
				int bytesSent = workSocket.EndSend(ar);
			}
			catch (Exception e)
			{
				Debug.LogException(e);
			}
		}
	}

	private void PacketProcessing(SocketStateObject state)
	{
		bool isReaded = true;
		while(isReaded && (state.bBuffer.Count > 0))
		{
			ByteBuffer buffer = new ByteBuffer();
			switch(state.bBuffer[0])
			{
			case (byte)AuthCmd.AUTH_LOGON_CHALLENGE :
//				LoginWindow.SetInventoryMessage("Invalid username or password. Please try again.");
				LoginWindow.SetInventoryMessage("Connecting to server");
				isReaded = ReadAuthLogonChallenge_S(state, ref buffer);
				break;
			case (byte)AuthCmd.AUTH_LOGON_PROOF :
				LoginWindow.SetInventoryMessage("Read Logon Proof");
				isReaded = ReadAuthLogonProof_S(state, ref buffer);
				break;
			case (byte)AuthCmd.REALM_LIST :
				LoginWindow.SetInventoryMessage("ReadRealmList");
				isReaded = ReadRealmList(state, ref buffer);
				break;
			case (byte)AuthCmd.XFER_INITIATE :
				LoginWindow.SetInventoryMessage("ReadTransferInit");
				break;
			case (byte)AuthCmd.XFER_DATA :
				LoginWindow.SetInventoryMessage("ReadTransferData");
				break;
			case (byte)AuthCmd.CMD_CREATE_ACC_CHALLENGE :
				LoginWindow.SetInventoryMessage("Create Account challenge");
				isReaded = ReadAuthLogonChallenge_S(state, ref buffer);
				break;
			case (byte)AuthCmd.CMD_CREATE_ACC_PROOF :
				LoginWindow.SetInventoryMessage("Create Account challenge");
				isReaded = ReadCreateAccProof(state, ref buffer);
				break;
			case (byte)AuthCmd.CMD_TEMPORARY_ACC:
				LoginWindow.SetInventoryMessage("Create Temporary Account");
				break;
			case (byte)AuthCmd.CMD_AUTH_KONGREGATE:
				KongregateManager.Log ("Check AUTH_KONGREGATE");
				LoginWindow.SetInventoryMessage("AUTH_KONGREGATE");
				isReaded = ReadAuthKongregate(state, ref buffer);
				break;
			case (byte)AuthCmd.CMD_BIND_KONGREGATE:
				KongregateManager.Log ("Check BIND_KONGREGATE");
				LoginWindow.SetInventoryMessage("BIND_KONGREGATE");
				isReaded = ReadBindKongregate(state, ref buffer);
				break;
			default:
				LoginWindow.SetInventoryMessage("Unknown packet");
				isReaded = false;
				break;
			}
			if(isReaded)
			{
				Vector.Add(buffer);
			}
		}
		WaitDataFromStream(state);
	}

	private bool ReadAuthLogonChallenge_S(SocketStateObject state, ref ByteBuffer buffer)
	{
		bool isReaded = true;
		int byteIndex = 0;
		int bytesNeed = 3;

		byte error = 255;
		if(isReaded && state.bBuffer.Count >= bytesNeed)
		{
			buffer.Append(state.bBuffer[byteIndex++]); //cmd
			buffer.Append(state.bBuffer[byteIndex++]); //unk2
			error = state.bBuffer[byteIndex++];
			buffer.Append(error);
		}
		else
		{
			isReaded = false;
		}

		if(error == 0)
		{
			int index;
			byte g_len = 0;
			bytesNeed += 33;
			if(isReaded && state.bBuffer.Count >= bytesNeed)
			{
					// reading B[]
				for(index = 0; index < 32; index++)
				{
					buffer.Append(state.bBuffer[byteIndex++]);
				}

				g_len = state.bBuffer[byteIndex++];
				buffer.Append(g_len);
			}
			else
			{
				isReaded = false;
			}
			
			bytesNeed += (g_len+1);
			byte N_len = 0;
			if(isReaded && state.bBuffer.Count >= bytesNeed)
			{
				//reading g[];
				for(index = 0; index < g_len; index++)
				{
					buffer.Append(state.bBuffer[byteIndex++]);
				}
				
				N_len = state.bBuffer[byteIndex++];
				buffer.Append(N_len);
			}
			else if (error != 0)
			{
				isReaded = false;
			}
			
			bytesNeed += (N_len+49);
			if(isReaded && state.bBuffer.Count >= bytesNeed)
			{
				// reading N;
				for(index = 0; index < N_len; index++)
				{
					buffer.Append(state.bBuffer[byteIndex++]);
				}
				
				//reading salt
				for(index = 0;index < 32; index++)
				{
					buffer.Append(state.bBuffer[byteIndex++]);
				}
				
				//reading unk3
				for(index = 0; index < 16; index++)
				{
					buffer.Append(state.bBuffer[byteIndex++]);
				}
				buffer.Append(state.bBuffer[byteIndex++]); //security flag
			}
			else
			{
				isReaded = false;
			}
		}

		if(isReaded)
		{
			state.bBuffer.RemoveRange(0, bytesNeed);
		}
		
		return isReaded;
	}

	private bool ReadAuthLogonProof_S(SocketStateObject state, ref ByteBuffer buffer)
	{
		bool isReaded = true;
		int byteIndex = 0;
		int bytesNeed = 2;
		
		byte error = 255;
		if(isReaded && state.bBuffer.Count >= bytesNeed)
		{
			buffer.Append(state.bBuffer[byteIndex++]); //cmd
			error = state.bBuffer[byteIndex++];
			buffer.Append(error);
		}
		else
		{
			isReaded = false;
		}

		if(error == 0)
		{
			bytesNeed += 30;
			if(isReaded && state.bBuffer.Count >= bytesNeed)
			{
				int index;
				for(index = 0; index < 20; index++)
				{
					buffer.Append(state.bBuffer[byteIndex++]);
				}
				// accountFlags
				for(index = 0; index < 4; index++)
				{
					buffer.Append(state.bBuffer[byteIndex++]);
				}
				// surveyId
				for(index = 0; index < 4; index++)
				{
					buffer.Append(state.bBuffer[byteIndex++]);
				}
				// unkFlags
				for(index = 0; index < 2; index++)
				{
					buffer.Append(state.bBuffer[byteIndex++]);
				}
			}
			else
			{
				isReaded = false;
			}
		}

		if(isReaded)
		{
			state.bBuffer.RemoveRange(0, bytesNeed);
		}
		
		return isReaded;
	}

	private bool ReadRealmList(SocketStateObject state, ref ByteBuffer buffer)
	{
		bool isReaded = true;
		int byteIndex = 0;
		int bytesNeed = 3;
		
		int size = 0;
		if(isReaded && state.bBuffer.Count >= bytesNeed)
		{
			buffer.Append(state.bBuffer[byteIndex++]); //cmd
			buffer.Append(state.bBuffer[byteIndex++]); // minor byte of size
			buffer.Append(state.bBuffer[byteIndex++]); // major byte of size
			size = state.bBuffer[1] + (state.bBuffer[2] << 8);
		}
		else
		{
			isReaded = false;
		}

		if(size > 0)
		{
			bytesNeed += size;
			if(isReaded && (state.bBuffer.Count >= bytesNeed))
			{
				for(int index = 0; index < size; index++)
				{
					buffer.Append(state.bBuffer[byteIndex++]);
				}
			}
			else
			{
				isReaded = false;
			}
		}

		if(isReaded)
		{
			state.bBuffer.RemoveRange(0, bytesNeed);
		}
		
		return isReaded;
	}

	private bool ReadCreateAccProof(SocketStateObject state, ref ByteBuffer buffer)
	{
		bool isReaded = true;
		int byteIndex = 0;
		int bytesNeed = 3;
		
		if(isReaded && state.bBuffer.Count >= bytesNeed)
		{
			buffer.Append(state.bBuffer[byteIndex++]); // cmd
			buffer.Append(state.bBuffer[byteIndex++]); // zero
			buffer.Append(state.bBuffer[byteIndex++]); // error
		}
		else
		{
			isReaded = false;
		}
		
		if(isReaded)
		{
			state.bBuffer.RemoveRange(0, bytesNeed);
		}
		
		return isReaded;
	}

	private bool ReadAuthKongregate(SocketStateObject state, ref ByteBuffer buffer)
	{
		bool isReaded = true;
		int byteIndex = 0;
		int bytesNeed = 3;
		
		byte error = 255;
		if(isReaded && state.bBuffer.Count >= bytesNeed)
		{
			buffer.Append(state.bBuffer[byteIndex++]); //cmd
			buffer.Append(state.bBuffer[byteIndex++]); //unk2
			error = state.bBuffer[byteIndex++];
			buffer.Append(error);
		}
		else
		{
			isReaded = false;
		}
		return isReaded;
	}
	
	private bool ReadBindKongregate(SocketStateObject state, ref ByteBuffer buffer)
	{
		bool isReaded = true;
		int byteIndex = 0;
		int bytesNeed = 3;
		
		byte error = 255;
		if(isReaded && state.bBuffer.Count >= bytesNeed)
		{
			buffer.Append(state.bBuffer[byteIndex++]); //cmd
			buffer.Append(state.bBuffer[byteIndex++]); //unk2
			error = state.bBuffer[byteIndex++];
			buffer.Append(error);
		}
		else
		{
			isReaded = false;
		}
		return isReaded;
	}
}