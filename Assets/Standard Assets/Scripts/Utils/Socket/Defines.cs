﻿using UnityEngine;
using System;
using System.Collections;

public class Defines {

	public static int connectPort = 3720;
	public static string connectIp = "login.worldofmidgard.com"; //;//"86.120.166.18"; //"192.168.0.147";
	public static string version = "200";
	public static bool SPLITBUILD = true;

	public static string realmName = "MaNGOS";
	public static UInt16 clientBuild = 12340;
	public static byte[] clientVersion = new byte[3];
	public static string clientLang = "enGB";
	public static int MAX_OPCODE_ID = 1228;
	public static byte MAX_PLAYERNAME_LENGTH = 12;
	public static byte MIN_PLAYERNAME_LENGTH = 2;
	public static byte MAX_REPUTATION_RANK = 8;
	public static byte MAX_STATS = 5;
	public static byte MAX_POWERS = 5;
	public static byte MAX_SPELL_SCHOOL = 7;
	public static byte MAX_AURAS = 64;
	public static bool loading = false;
	
	//for ipad 3 and 4
	public static int _sizeMod = 1;

	public static float instanceMapLODDistance = 40; //	between 10 and 100
	public static float normalMapLODDistance = 40;
	public static float lod_coef = 2.0f;

	public static bool showInterf = false;

	public static void clientVersionInit()
	{
		IsUInt64Field.InitVars();
		IsFloatField.InitVars();
		clientVersion[0] = clientVersion[1] = 3;
		clientVersion[2] = 5;
	}

}
