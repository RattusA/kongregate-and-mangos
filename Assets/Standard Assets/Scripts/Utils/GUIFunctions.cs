﻿using UnityEngine;
using System.Collections;

public class GUIFunctions
{
	public static GUIStyle CreateGuiStyle(Font font, TextAnchor textAnchor, int fontSize, Color textColor, 
	                               bool wordWrap, FontStyle fontStyle)
	{
		GUIStyle newStyle = new GUIStyle();
		
		newStyle.font = font;
		newStyle.alignment = textAnchor;
		newStyle.fontSize = fontSize;
		newStyle.normal.textColor = textColor;
		newStyle.wordWrap = wordWrap;
		newStyle.fontStyle = fontStyle;
		
#if UNITY_IPHONE
		newStyle.fontSize = (int)(fontSize * 0.8f);
#endif
		return newStyle;
	}

	public static Rect RectWH(float xPos, float yPos, float width, float height)
	{
		return new Rect(Screen.width * xPos, Screen.height * yPos, Screen.width * width, Screen.height * height);
	}
}

public class GUIItemButton
{

	public Item ItemInSlot;
	public Texture IconTexture;
	public Rect IconPosition;
}