﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

public class ByteBufferException : System.Exception {

	public string action;
	public int rPos;
	public int wPos;
	public int readSize;
	public int curSize;

	public ByteBufferException(string action, int rPos, int wPos, int readSize, int curSize)
	{
		this.action = action;
		this.rPos = rPos;
		this.wPos = wPos;
		this.readSize = readSize;
		this.curSize = curSize;
	}
}
	
public class ClientPktHeader {

	public ushort size;
	public ushort cmd;
	public ushort nil;
}
	
public class ServerPktHeader {

	public ushort cmd;
	public int size;

	public ServerPktHeader()
	{
		cmd = 0;
		size = 0;
	}

	public int GetRealSize()
	{
		int index = isLargePacket() ? 5 : 4;
		int realsize = size - 2 + index;
		return realsize;
	}

	public int SetSize(byte high, byte medium, byte low)
	{
		size = ((high << 16) | (medium << 8) | low);
		return size;
	}

	public bool isLargePacket()
	{
		return size > 0x7FFF;
	}
}

public class ByteBuffer {

	public static int DEFAULT_SIZE = 0xFF;
	protected int _rPos;
	protected int _wPos;
	public List<byte> _storage;
	
	public static ushort ntohs(ushort ceva)
	{
		ushort ret;
		byte[] aux = System.BitConverter.GetBytes(ceva);

		byte aux1 = aux[0];
		aux[0] = aux[1];
		aux[1] = aux1;

		ret = System.BitConverter.ToUInt16(aux, 0);
		return ret;
	}
	
	public static ushort Reverse(ushort ceva)
	{
		byte[] bytes = System.BitConverter.GetBytes(ceva);
		byte[] b = new byte[bytes.Length];
		for(int index = bytes.Length - 1; index >= 0; index--)
		{
			b[bytes.Length - (index + 1)] = bytes[index];
		}
		return BitConverter.ToUInt16(b, 0);
	}

	public static uint Reverse(int ceva)
	{
		return Reverse((uint) ceva);
	}

	public static uint Reverse(uint ceva)
	{
		byte[] bytes = System.BitConverter.GetBytes(ceva);
		byte[] b = new byte[bytes.Length];
		for(int index = bytes.Length - 1; index >= 0; index--)
		{
			b[bytes.Length - (index + 1)] = bytes[index];
		}
		return BitConverter.ToUInt32(b, 0);
	}

	public static ulong Reverse(ulong ceva)
	{
		byte[] bytes = System.BitConverter.GetBytes(ceva);
		byte[] b = new byte[bytes.Length];
		for(int index = bytes.Length - 1; index >= 0; index--)
		{
			b[bytes.Length - (index + 1)] = bytes[index];
		}
		return BitConverter.ToUInt64(b, 0);
	}
	
	public void InterChange(int from, int to, int leng)
	{
		for(int index = 0; index < leng; index++)
		{
			byte aux = (byte)_storage[from + index];
			_storage[from + index] = _storage[to + index];
			_storage[to + index] = aux;
		}
	}
	public void EndianConvert(int from, int leng, byte nrBytes)
	{
		return;
	}
	
	public ByteBuffer()
	{
		_rPos = 0;
		_wPos = 0;
		_storage = new List<byte>();
	}

	public ByteBuffer(int res)
	{
		_rPos = 0;
		_wPos = 0;
		_storage = new List<byte>();
	}

	public ByteBuffer(ByteBuffer buf)
	{
		_rPos = buf._rPos;
		_wPos = buf._wPos;
		_storage.AddRange(buf._storage);
	}

	public void Clear()
	{
		_rPos = 0;
		_wPos = 0;
		_storage.Clear();
	}

	public int RPos()
	{
		return _rPos;
	}

	public int RPos(int rPos)
	{
		_rPos = rPos < _storage.Count ? rPos : _storage.Count;
		return _rPos;
	}

	public int WPos()
	{
		return _wPos;
	}

	public int WPos(int wPos)
	{
		_wPos = wPos < _storage.Count ? wPos : _storage.Count;
		return _wPos;
	}

	public byte Read()
	{
		if(_rPos >= Size())
		{
			return 0;
		}

		byte ret = Read(_rPos);
		_rPos++;
		return ret;
	}

	public ulong ReadTypeReversed(byte type)
	{
		ulong ret = 0;
		if(Size() - _rPos < type)
		{
			return ret;
		}

		byte shift = 8;
		int readPos = _rPos;
		for(int index = type - 1; index >= 0 ; index--)
		{
			ret = ret << shift;
			byte b = (byte)_storage[readPos + index];
			ret = ret | b;
			_rPos++;
		}
		return ret;
	}

	public ulong ReadReversed64bit()
	{
		int type = 8;
		ulong ret = 0;
		if(Size() - _rPos < type)
		{
			return ret;
		}
		
		byte shift = 8;
		int readPos = _rPos;
		for(int index = type - 1; index >= 0 ; index--)
		{
			ret = ret << shift;
			byte b = (byte)_storage[readPos + index];
			ret = ret | b;
			_rPos++;
		}
		return ret;
	}

	public uint ReadReversed32bit()
	{
		int type = 4;
		uint ret = 0;
		if(Size() - _rPos < type)
		{
			return ret;
		}
		
		byte shift = 8;
		int readPos = _rPos;
		for(int index = type - 1; index >= 0 ; index--)
		{
			ret = ret << shift;
			byte b = (byte)_storage[readPos + index];
			ret = ret | b;
			_rPos++;
		}
		return ret;
	}

	public ushort ReadReversed16bit()
	{
		int type = 2;
		ushort ret = 0;
		if(Size() - _rPos < type)
		{
			return ret;
		}
		
		byte shift = 8;
		int readPos = _rPos;
		for(int index = type - 1; index >= 0 ; index--)
		{
			ret = (ushort)(ret << shift);
			byte b = (byte)_storage[readPos + index];
			ret = (ushort)(ret | b);
			_rPos++;
		}
		return ret;
	}

	public ulong ReadType(byte type)
	{
		ulong ret = 0;
		if(Size() - _rPos < type)
		{
			return ret;
		}

		byte shift = 8;

		for(int index = type - 1; index >= 0 ; index--)
		{
			ret = ret << shift;
			byte b = (byte)_storage[_rPos];
			ret = ret | b;
			_rPos++;
		}
		return ret;
	}

	public ulong GetPacketGuid()
	{
		ulong guid = 0;
		byte mask = Read();
		ulong bit = 0;
		for(int index = 0; index < 8; index++)
		{
			if((mask & (1 << index)) != 0)
			{
				bit = Read();
				guid = guid | (bit << (index * 8));
			}
		}
		return guid;
	}

	public int ReadInt()
	{
		if(Size() - _rPos < 4)
		{
			return 0;
		}

		int ret = 0;
		byte shift = 8;
		int readPos = _rPos;
		for(int index = 3; index >= 0; index--)
		{
			ret = ret << shift;
			byte b = (byte)_storage[readPos + index];
			ret = ret | b;
			_rPos++;
		}
		return ret;
	}

	public string ReadString()
	{
		string ret;
		ByteBuffer pkt = new ByteBuffer();
		byte aux = Read();
		while(aux != 0)
		{
			pkt.Append(aux);
			aux = Read();
		}
		ret = Encoding.ASCII.GetString(pkt.Contents());
		return ret;
	}
	
	public string ReadChatString(uint messageLength)
	{
		string ret;
		ByteBuffer buffer = new ByteBuffer();
		ushort aux = ReadReversed16bit(); 
		while(aux != 0)
		{
			buffer.Append(aux);
			aux = ReadReversed16bit();
		}
		ret = Encoding.UTF8.GetString(buffer.Contents());
		return ret;
	}
	
	public float ReadFloat()
	{
		if(Size() - _rPos<4)
		{
			return 0;
		}
		float ret;
		ret = System.BitConverter.ToSingle(Contents(), _rPos);
		_rPos += 4;
		return ret;
	}
	
	public byte Read(int index)
	{
		if(index >= _storage.Count)
		{
			return 0;
		}
		return (byte)_storage[index];
	}
	
	public void Read(uint[] dest, int length)
	{
		if((_rPos + length) < _storage.Count)
		{
			for(int index = 0; index < length; index++)
			{
				dest[index] = (uint)_storage[_rPos + index];
			}
		}
		else
		{
			throw new ByteBufferException("read-into", _rPos, _wPos, length, _storage.Count);
		}
		_rPos += length;
	}
	
	public void Read(byte[] dest, int length)
	{
		if((_rPos + length) <= _storage.Count)
		{
			for(int index = 0; index < length; index++)
			{
				dest[index] = (byte)_storage[_rPos + index];
			}
		}
		else
		{
			throw new ByteBufferException("read-into", _rPos, _wPos, length, _storage.Count);
		}
		_rPos+=length;
	}
	
	public byte[] Contents()
	{
		byte[] ret;
		if(_storage.Count == 0 || _storage.Count == 0)
		{
			ret = new byte[1];
			ret[0] = 0;
		}
		else
		{
			ret = new byte[_storage.Count];
			for(int index = 0; index < ret.Length; index++)
			{
				ret[index] = (byte)_storage[index];
			}
		}
		return ret;
	}

	public int Size()
	{
		return _storage.Count;
	}

	public void Resize(int newSize)
	{
		_rPos = 0;
		_wPos = _storage.Count;
	}

	public void Reserve(int ressize)
	{
		//acelasi motiv ca mai sus
	}
	
	public void Append(string str)
	{
		Append(Encoding.ASCII.GetBytes(str), str.Length);
		byte aux = 0;
		Append(aux);
	}

	public void AppendChatString(string str)
	{
		str = str + "\0";
		byte[] buffer = Encoding.UTF8.GetBytes(str);
		Append(buffer, buffer.Length);
	}

	public void AppendSTR(string str)
	{
		Append(Encoding.ASCII.GetBytes(str), str.Length);
	}

	public void Append(float info)
	{
		byte[] bytes = System.BitConverter.GetBytes(info);
		Append(bytes, bytes.Length);
	}

	public void Append(ushort info)
	{
		ushort inf = info;
		byte aux = (byte)(info % 256);
		_storage.Add(aux);
		
		aux = (byte)(inf >> 8);
		_storage.Add(aux);
	}

	public void Append(ulong info)
	{
		byte[] bytes = System.BitConverter.GetBytes(info);
		for(int index = bytes.Length - 1; index >= 0; index--)
		{
			Append(bytes[index]);
		}
	}

	public void Append(uint info)
	{
		byte[] bytes = System.BitConverter.GetBytes(info);
		for(int index = bytes.Length - 1; index >= 0; index--)
		{
			Append(bytes[index]);
		}
	}
	public void Append(byte[] src, int cnd)
	{
		if(cnd == 0)
		{
			return;
		}

		for(int index = 0; index < cnd; index++)
		{
			_storage.Add(src[index]);
		}
		_wPos = _storage.Count;
	}

	public void Append(byte[] src, int from, int cnd)
	{
		if((from > src.Length) || cnd == 0)
		{
			return;
		}

		for(int index = from; index < (from + cnd); index++)
		{
			if((index + from) >= src.Length)
			{
				break;
			}
			else
			{
				_storage.Add(src[index]);
			}
		}
		_wPos = _storage.Count;
	}

	public void Append(byte src)
	{
		_wPos++;
		_storage.Add(src);
	}
	
	public void Append(bool src)
	{
		byte tmp = (byte)(src ? 1 : 0);
		_wPos++;
		_storage.Add(tmp);
	}
	
	public void Append(ByteBuffer buffer)
	{
		if(buffer.Size() != 0)
		{
			Append(buffer.Contents(), buffer.Size());
		}
	}

	public void AppendPackGUID(ulong guid)
	{
		int mask_position = WPos();
		byte[] aux = new byte[1];
		byte shift = 8;
		byte ff = 0xFF;
		aux[0] = 0;
		Append(aux, 1);

		for(int index = 0; index < 8; ++index)
		{
			if((guid & ff) != 0)
			{
				aux[0] = (byte)_storage[mask_position];
				_storage[mask_position] = (byte)(aux[0] | (1 << index));
				aux[0] = (byte)(guid & ff);
				Append(aux[0]);
			}
			guid = guid >> shift;
		}
	}
}
