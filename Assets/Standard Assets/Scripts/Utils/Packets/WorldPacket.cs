﻿using System.Collections.Generic;
using System.IO;

public class WorldPacket : ByteBuffer {
	
	private ushort _opcode;
	public WorldPacket()
	{
		new ByteBuffer(10);
		_opcode = 0;
	}

	public WorldPacket(int r)
	{
		new ByteBuffer(10);
		Reserve(r);
		_opcode = 0;
	}

	public WorldPacket(OpCodes opcode, int r)
	{
		new ByteBuffer(r);
		_opcode = (ushort)opcode;
		Reserve(r);
	}

	public WorldPacket(OpCodes opcode)
	{
		new ByteBuffer(10);
		_opcode = (ushort)opcode;
		Reserve(10);
	}

	public void SetOpcode(OpCodes opcode)
	{
		SetOpcode((ushort)opcode);
	}
	public void SetOpcode(ushort opcode)
	{
		_opcode = opcode;
	}

	public ushort GetOpcode()
	{
		return _opcode;
	}
	
	public int remainingData()
	{
		return _wPos - _rPos;
	}
	
	public bool Decompress()
	{
		//size of the uncompressed package
		long _size = (long)ReadReversed32bit();
		
		MemoryStream _finalOutput = new MemoryStream();
		ComponentAce.Compression.Libs.zlib.ZOutputStream outZStream = new ComponentAce.Compression.Libs.zlib.ZOutputStream(_finalOutput);
		byte aux8;
		for(int _i = 4; _i < _storage.Count; _i++)
		{
			aux8 = (byte)_storage[_i];
			outZStream.WriteByte(aux8);
		}
		outZStream.Flush();
		
		if(_finalOutput.Length != _size)
		{
			return false;
		}
		else
		{
			_finalOutput.Position = 0;
			_storage = new List<byte>();		
			for(int index = 0; index < _size; index++)
			{
				_storage.Add((byte)_finalOutput.ReadByte());
			}
			_rPos = 0;
			WPos((int)_size); 
			_finalOutput.Flush();
			
			return true;
		}
	}
	
	public void Add(ushort arg)
	{
		Append(Reverse(arg));
	}
	
	public void Add(uint arg)
	{
		Append(Reverse(arg));
	}
	
	public void Add(ulong arg)
	{
		Append(Reverse(arg));
	}
}
