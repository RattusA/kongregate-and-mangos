﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AtlasTextureInfo
{
	public Rect uvRect;
	public Rect frame;
	public Rect sourceSize;
}

public class TexturePacker
{
	private Vector2 textureSize = Vector2.zero;
	private Texture2D atlasTexture = null;
	private Dictionary<string, AtlasTextureInfo> textureDetails = new Dictionary<string, AtlasTextureInfo>();
	
	/********************/
	/* Public functions */
	/********************/

	public TexturePacker(string atlasName)
	{
		atlasTexture = Resources.Load<Texture2D> (atlasName);
		if(atlasTexture == null)
		{
			Debug.LogError("Unable to find texture file.");
			return;
		}
		textureSize = new Vector2(atlasTexture.width, atlasTexture.height);

		TextAsset atlasInfo = Resources.Load<TextAsset> (atlasName);
		textureDetails = LoadTexturesFromTexturePackerJSON(atlasInfo);
	}

	public TexturePacker(Texture2D atlasTexture, TextAsset atlasInfo)
	{
		if(atlasTexture == null)
		{
			Debug.LogError("Unable to find texture file.");
			return;
		}

		this.atlasTexture = atlasTexture;
		textureSize = new Vector2(atlasTexture.width, atlasTexture.height);
		textureDetails = LoadTexturesFromTexturePackerJSON(atlasInfo);
	}
	
	public void Destroy()
	{
		atlasTexture = null;
		textureSize = Vector2.zero;
		textureDetails.Clear();
	}
	
	/*********************/
	/* Private functions */
	/*********************/
	private Dictionary<string, AtlasTextureInfo> LoadTexturesFromTexturePackerJSON(TextAsset atlasInfo)
	{
		if(atlasInfo == null)
		{
			Debug.LogError("Unable to find info file.");
			return null;
		}

		Dictionary<string, AtlasTextureInfo> textures = new Dictionary<string, AtlasTextureInfo>();		
		Hashtable decodedHash = (Hashtable)(JSONUtil.DecodeString(atlasInfo.text) as Hashtable);
		Hashtable frames = (Hashtable)(decodedHash["frames"] as Hashtable);
		
		foreach(System.Collections.DictionaryEntry item in frames)
		{
			Hashtable frame = (Hashtable)(item.Value as Hashtable)["frame"];
			int frameX = int.Parse(frame["x"].ToString());
			int frameY = int.Parse(frame["y"].ToString());
			int frameW = int.Parse(frame["w"].ToString());
			int frameH = int.Parse(frame["h"].ToString());
			
			Hashtable sourceSize = (Hashtable)(item.Value as Hashtable)["spriteSourceSize"];
			int sourceSizeX = int.Parse(sourceSize["x"].ToString());
			int sourceSizeY = int.Parse(sourceSize["y"].ToString());
			int sourceSizeW = int.Parse(sourceSize["w"].ToString());
			int sourceSizeH = int.Parse(sourceSize["h"].ToString());
			
			AtlasTextureInfo textureInfo = new AtlasTextureInfo();
			textureInfo.frame = new Rect(frameX, frameY, frameW, frameH);
			Vector2 lowerLeftUV = new Vector2(frameX / textureSize.x, 1.0f - ((frameY + frameH ) / textureSize.y ) );
			Vector2 uvDimensions = new Vector2(frameW / textureSize.x, frameH / textureSize.y );
			textureInfo.uvRect = new Rect(lowerLeftUV.x, lowerLeftUV.y, uvDimensions.x, uvDimensions.y);
			textureInfo.sourceSize = new Rect(sourceSizeX, sourceSizeY, sourceSizeW, sourceSizeH);
			
			textures.Add(item.Key.ToString(), textureInfo);
		}
		atlasInfo = null;

		return textures;
	}
	
	private Rect GetTextureUVInfoByImageName(string imageName)
	{
		AtlasTextureInfo returnValue = new AtlasTextureInfo();
		if(textureDetails.ContainsKey(imageName))
		{
			returnValue = textureDetails[imageName];
		}
		else
		{
			throw new System.Exception("Can't find texture details for texture packer sprite:" + imageName);
		}
		return returnValue.uvRect;
	}	
	
	/******************/
	/* User functions */
	/******************/
	public bool IsAtlasHaveTexture(string imageName)
	{
		bool ret = false;
		if(textureDetails.ContainsKey(imageName))
		{
			ret = true;
		}
		
		return ret;
	}	
	
	public void DrawTexture(Rect positionRect, string imageName)
	{
		Rect uvRect = GetTextureUVInfoByImageName(imageName);		
		GUI.DrawTextureWithTexCoords(positionRect, atlasTexture, uvRect);
	}
	
	public void DrawActionTexture(Rect positionRect, string imageName)
	{
		bool touchIsOver = false;	
		foreach(Touch touch in Input.touches)
		{
			if((touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary) 
			   && positionRect.Contains(new Vector2(touch.position.x, (Screen.height - touch.position.y))))
			{
				touchIsOver = true;
				break;
			}
		}
		
#if (UNITY_STANDALONE || UNITY_EDITOR)
		// check mouse only on PC, Mac and Linux platforms
		if(touchIsOver || (Input.GetMouseButton(0) && positionRect.Contains(Event.current.mousePosition)))
#else
			if(touchIsOver)
#endif
		{
			positionRect = new Rect((positionRect.x - (positionRect.width * 0.2f)), 
			                        (positionRect.y - (positionRect.height * 0.2f)), 
			                        (positionRect.width * 1.4f), (positionRect.height * 1.4f));
		}
		
		Rect uvRect = GetTextureUVInfoByImageName(imageName);		
		GUI.DrawTextureWithTexCoords(positionRect, atlasTexture, uvRect);
	}
	
}