﻿
/// <summary>
/// Word processing.
/// $B - line break,
/// $N / CHAR_NAME - name,
/// $R / CHAR_RACE - race,
/// $C / CHAR_CLASS - class,
/// $Gmale:female / boy:girl / man:woman / sir:madam / dude:chick
/// <craft_name> - craft name
/// </summary>
public class WordProcessing 
{
	/// <summary>
	/// Replaces the defined words.
	/// </summary>
	/// <returns>The defined words.</returns>
	/// <param name="sourceText">Source text.</param>
	public static string ReplaceDefinedWords(string sourceText)
	{
		string playerName = "";
		string playerRace = "";
		string playerClass = "";
		string playerGender = "";
		BaseObject playerTarget;
		if(InterfaceManager.Instance.PlayerScript != null)
		{
			Player player = InterfaceManager.Instance.PlayerScript;
			playerName = player.Name;
			playerRace = dbs.sCharacterRace.GetRecord((int)player.race).Name;
			playerGender = dbs.sCharacterGender.GetRecord((int)player.gender).Name;

			playerClass = Global.GetClassAsString((Classes)player.playerClass);
			playerTarget = player.target;
			if(playerTarget != null)
			{
				string craftName = "";
				int flagTrainerProfession = (int)NPCFlags.UNIT_NPC_FLAG_TRAINER_PROFESSION;
				if((playerTarget.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flagTrainerProfession) > 0)
				{
					craftName = playerTarget.name;
					craftName = craftName.Substring(0, craftName.IndexOf(" "));
				}
				sourceText = sourceText.Replace("<craft_name>", craftName);
			}
		}
		sourceText = sourceText.Replace("$N", playerName);
		sourceText = sourceText.Replace("CHAR_NAME", playerName);
		sourceText = sourceText.Replace("$R", playerRace);
		sourceText = sourceText.Replace("CHAR_RACE", playerRace);
		sourceText = sourceText.Replace("$C", playerClass);
		sourceText = sourceText.Replace("CHAR_CLASS", playerClass);
		sourceText = sourceText.Replace("$Gmale:female", playerGender);
		sourceText = sourceText.Replace("$Gman:woman", playerGender);
		return sourceText;
	}
}
