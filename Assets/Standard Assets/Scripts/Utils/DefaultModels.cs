﻿using System;
using System.Collections;

public class DefaultModels
{
	public static uint GetModel(ulong ID)
	{
		Item item = new Item(AppCache.sItems.GetItemEntry(ID));
		return GetModel(item);
	}
	
	public static uint GetModel(Item item)
	{
		uint displayID = 0;
		displayID = GetModelUsedCache(item.entry);
		return displayID;
	}
	
	public static uint GetModelUsedCache(ulong ID)
	{
		uint entry = (uint)ID;
		Item item;
		ItemDisplayInfoEntry itemDisplayInfo;
		uint displayID = 0;
		
		try
		{
			item = AppCache.sItems.GetItem(entry);
			itemDisplayInfo = dbs.sItemDisplayInfo.GetRecord((int)item.displayInfoID, false);
			displayID = itemDisplayInfo.ID;
			if(displayID == 0)
			{
				displayID = DefaultModels.GetDefaultModel((uint)item.classType, (uint)item.subClass, (int)item.inventoryType, (uint)item.quality);
			}
		}
		catch(Exception exception)
		{
			displayID = 0;
		}
		return displayID;
	}

	public static uint GetDefaultModel(uint classID, uint subclassID, int inventorySlot, uint quality)
	{
		uint displayID = 0;
		switch(classID)
		{
		case 2: // Weapon
			displayID = GetWeaponModel(subclassID);
			break;
		case 4: // Armor
			displayID = GetArmorModel(subclassID, inventorySlot, quality);
			break;
		}
		if(displayID == 0)
		{
			throw new System.Exception("Default model not found! (class "+classID+", subclass "+subclassID+
			                           ", inventorySlot "+inventorySlot+", quality "+quality+")");
		}
		return displayID;
	}
	
	private static uint GetWeaponModel(uint subclassID)
	{
		uint displayID = 0;
		switch(subclassID)
		{
		case 0: // Axe One handed
			displayID = 167;
			break;
		case 1: // Axe Two handed
			displayID = 170;
			break;
		case 2: // Bow
			displayID = 28;
			break;
		case 3: // Gun
			displayID = 40;
			break;
		case 4: // Mace One handed
			displayID = 16;
			break;
		case 5: // Mace Two handed
			displayID = 17;
			break;
		case 6: // Polearm
			displayID = 18;
			break;
		case 7: // Sword One handed
			displayID = 4;
			break;
		case 8: // Sword Two handed
			displayID = 8;
			break;
		case 9: // Obsolete
			displayID = 0;
			break;
		case 10: // Staff
			displayID = 241;
			break;
		case 11: // Exotic
			displayID = 0;
			break;
		case 12: // Exotic
			displayID = 0;
			break;
		case 13: // Fist Weapon
			displayID = 174;
			break;
		case 14: // Miscellaneous   (Blacksmith Hammer, Mining Pick, etc.)
			displayID = 0;
			break;
		case 15: // Dagger
			displayID = 41;
			break;
		case 16: // Thrown
			displayID = 41;
			break;
		case 17: // Spear
			displayID = 18;
			break;
		case 18: // Crossbow
			displayID = 162;
			break;
		case 19: // Wand
			displayID = 241;
			break;
		case 20: // Fishing Pole
			displayID = 0;
			break;
		default: // unknown
			displayID = 0;
			break;
		}
		return displayID;
	}
	
	static uint GetArmorModel(uint subclassID, int invSlot, uint quality)
	{
		uint displayID = 0;
		string subClassName = (string)DefaultTabel.itemSubclassArmor[""+subclassID];
		string invTypeName = (string)DefaultTabel.inventoryType[""+invSlot];
		string qualiyName = (string)DefaultTabel.itemQualities[""+quality];
		IDictionary dictionary;
		dictionary = (IDictionary)DefaultTabel.itemDefaultDisplayID[subClassName];
		dictionary = (IDictionary)dictionary[invTypeName];
		displayID = (uint)dictionary[qualiyName];
		return displayID;
	}
}
