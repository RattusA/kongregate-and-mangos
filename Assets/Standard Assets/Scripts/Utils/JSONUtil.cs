﻿using UnityEngine;
using System.Collections;

public class JSONUtil
{
	/**
	 * Parses the string json into a value
	 */
	public static object DecodeResource(string path)
	{
		TextAsset jsonFile = Resources.Load<TextAsset> (path);
		if(!jsonFile)
		{
			Debug.LogError("Failed to load resource '" + path + "'");
			return null;
		}
		
		object ret = DecodeString(jsonFile.text);
		if(ret == null)
		{
			Debug.LogWarning("Failed to decode JSON resource '" + path + "'.");
		}
		return ret;
	}
	
	/**
	 * Parses the string json into a value
	 */
	public static object DecodeString(string json)
	{
		object ret = MiniJSON.jsonDecode(json);
		if(ret == null)
		{			
			Debug.LogError("Failed to decode JSON string. Reason: " + MiniJSON.getLastErrorSnippet());
			Debug.LogError("Error contains in line number - " + JSONUtil.GetErrorLineNumber(json));
		}
		return ret;
	}
	
	/**
	 * Find line with error
	 */
	public static int GetErrorLineNumber(string json) 
	{
		int lineNumber = 1;
		int errorIndex = MiniJSON.getLastErrorIndex();
		for(int i = 0; i < errorIndex; i++)
		{
			if(json[i] == '\n')
			{
				lineNumber++;
			}
		}
		return lineNumber;
	}
}
