﻿using UnityEngine;
using System;
using System.Collections;

public class IsUInt64Field {

	public static UInt32 minus1UI32 = 0xFFFFFFFF;
	public static UInt32[] u64_object = new UInt32[2];
	public static UInt32[] u64_item = new UInt32[5];
	public static UInt32[] u64_container = new UInt32[2];
	public static UInt32[] u64_unit = new UInt32[9];
	public static UInt32[] u64_player = new UInt32[14];
	public static UInt32[] u64_gameobject = new UInt32[2];
	public static UInt32[] u64_dynobject = new UInt32[2];
	public static UInt32[] u64_corpse = new UInt32[3];

	public static void InitVars()
	{
		u64_object[0] = (uint)UpdateFields.EObjectFields.OBJECT_FIELD_GUID;
		u64_object[1] = minus1UI32;
		
		u64_item[0] = 0x0006;
		u64_item[1] = 0x0008;
		u64_item[2] = 0x000A;
		u64_item[3]	= 0x000C;
		u64_item[4] = minus1UI32;
		
		u64_container[0] = 0x42;
		u64_container[1] = minus1UI32;
		
		u64_unit[0] = 0x6;
		u64_unit[1] = 0x8;
		u64_unit[2] = 0xA;
		u64_unit[3] = 0xC;
		u64_unit[4] = 0xE;
		u64_unit[5] = 0x10;
		u64_unit[6] = 0x12;
		u64_unit[7] = 0x14;
		u64_unit[8] = minus1UI32;
		
		u64_player[0] = 0x94;
		u64_player[1] = 0x144;
		u64_player[2] = 0x172;
		u64_player[3] = 0x196;
		u64_player[4] = 0x1CA;
		u64_player[5] = 0x1D8;
		u64_player[6] = 0x1F0;
		u64_player[7] = 0x230;
		u64_player[8] = 0x270;
		u64_player[9] = 0x272;
		u64_player[10] = 0x274;
		u64_player[11] = 0x276;
		u64_player[12] = 0x278;
		u64_player[13] = minus1UI32;
		
		u64_gameobject[0] = 0x6;
		u64_gameobject[1] = minus1UI32;
		
		u64_dynobject[0] = 0x6;
		u64_dynobject[1] = minus1UI32;
		
		u64_corpse[0] = 0x6;
		u64_corpse[1] = 0x8;
		u64_corpse[2] = minus1UI32;
	}
}
