﻿
public class IsFloatField {

	public static uint[] floats_object = new uint[2] ;
	public static uint[] floats_unit = new uint[17];
	public static uint[] floats_player = new uint[12];
	public static uint[] floats_gameobject = new uint[5];
	public static uint[] floats_dynobject = new uint[2];
	
	public static void InitVars()
	{
		floats_object[0] = (uint)UpdateFields.EObjectFields.OBJECT_FIELD_SCALE_X;
		floats_object[1] = IsUInt64Field.minus1UI32;
		
		floats_unit[0] = 0x41;
		floats_unit[1] = 0x42;
		floats_unit[2] = 0x46;
		floats_unit[3] = 0x47;
		floats_unit[4] = 0x48;
		floats_unit[5] = 0x49;
		floats_unit[6] = 0x50;
		floats_unit[7] = 0x80;
		floats_unit[8] = 0x7D;
		floats_unit[9] = 0x81;
		floats_unit[10] = 0x82;
		floats_unit[11] = 0x8A;
		floats_unit[12] = 0x28;
		floats_unit[13] = 0x2F;
		floats_unit[14] = 0x91;
		floats_unit[15] = 0x92;
		floats_unit[16] = IsUInt64Field.minus1UI32;
		
		floats_player[0] = 0x400;
		floats_player[1] = 0x401;
		floats_player[2] = 0x402;
		floats_player[3] = 0x406;
		floats_player[4] = 0x407;
		floats_player[5] = 0x408;
		floats_player[6] = 0x405;
		floats_player[7] = 0x410;
		floats_player[8] = 0x4A9;
		floats_player[9] = 0x4AA;
		floats_player[10] = 0x519;
		floats_player[11] = IsUInt64Field.minus1UI32;
		
		floats_gameobject[0] = 0xA;
		floats_gameobject[1] = 0xB;
		floats_gameobject[2] = 0xC;
		floats_gameobject[3] = 0xD;
		floats_gameobject[4] = IsUInt64Field.minus1UI32;
		
		floats_dynobject[0] = 0xA;
		floats_dynobject[1] = IsUInt64Field.minus1UI32;
	}
}
