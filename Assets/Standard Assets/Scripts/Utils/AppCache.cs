﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class AppCache : MonoBehaviour
{
	static public ItemCache sItems;

	void Awake ()
	{
		sItems = new ItemCache();
	}
	
	void OnDestroy()
	{
		Debug.LogWarning("Application cache destoyed");
	}
}

public class RequesterGuidList
{
	protected List<UInt64> mGuidList = new List<UInt64>();
	public List<UInt64> requesterGuid
	{
		set
		{
			mGuidList = value;
		}
		get
		{
			List<UInt64> ret = new List<UInt64>(mGuidList);
			return ret;
		}
	}

	public void Add(UInt64 value)
	{
		if((value > 0) && (!mGuidList.Contains(value)))
		{
			mGuidList.Add(value);
		}
	}
}

public class ItemCache
{
	protected Dictionary<int, Item> itemCache = new Dictionary<int, Item>();
	// <item entry, requested player guid>
	protected Dictionary<uint, RequesterGuidList> requestItemQueue = new Dictionary<uint, RequesterGuidList>(); 
	
	public Item GetRecord(UInt64 ID)
	{
		int intID = (int)ID;
		return GetRecord(intID, true);
	}
	
	public Item GetRecord(int ID) //return a certain record base on the id
	{
		return GetRecord(ID, true);
	}
	public Item GetRecord(uint ID) //return a certain record base on the id
	{
		int intID = (int)ID;
		return GetRecord(intID, true);
	}
	
	public Item GetRecord(int ID, bool isWOWID) //return a certain record base on the id
	{
		Item resultItem;
		if(ID == 0)
		{
			resultItem = new Item();
		}
		else if(HaveRecord(ID))
		{
			resultItem = itemCache[ID];
		}
		else
		{
			resultItem = new Item();
			SendQueryItem(ID);
		}
		return resultItem;
	}
	
	public bool HaveRecord(UInt64 ID)
	{
		int intID = (int)ID;
		return HaveRecord(intID, true);
	}
	
	public bool HaveRecord(int ID)
	{
		return HaveRecord(ID, true);
	}
	
	public bool HaveRecord(uint ID)
	{
		int intID = (int)ID;
		return HaveRecord(intID, true);
	}
	
	public bool HaveRecord(int ID, bool isWOWID)
	{
		bool result = false;
		if((ID == 0) || (itemCache.ContainsKey(ID)))
		{
			result = true;
		}
		return result;
	}
	
	public void AddRecord(Item item)
	{
		if((item.entry != 0) && !HaveRecord(item.entry))
		{
			itemCache.Add((int)item.entry, item);
			UpdateRequestQueue(item);
			#if UNITY_EDITOR
			Debug.Log("New item " + item.entry + " " + item.name + " added to cache");
			#endif
		}
	}
	
	public void SendQueryItem(int ID)
	{
		uint entry = (uint)ID;
		SendQueryItem(entry);
	}
	
	public void SendQueryItem(uint ID)
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_ITEM_QUERY_SINGLE);
		pkt.Add(ID);
		RealmSocket.outQueue.Add(pkt);//SendWorldPacket(packet);
	}
	
	public Item GetItem(uint ID)
	{
		return GetRecord((int)ID, true);
	}
	
	public ItemEntry GetItemEntry(UInt64 ID)
	{
		int intID = (int)ID;
		return GetItemEntry(intID);
	}
	
	public ItemEntry GetItemEntry(uint ID) //return a certain record base on the id
	{
		int intID = (int)ID;
		return GetItemEntry(intID);
	}
	
	public ItemEntry GetItemEntry(int ID)
	{
		ItemEntry item = GetRecord(ID, true).ToItemEntry();
		if(item.ID == 0)
		{
			item = dbs.sItems.GetRecord(ID, true);
		}
		return item;
	}
	
	public void AddRequestToQueue(uint entry, UInt64 guid)
	{
		RequesterGuidList guidList = new RequesterGuidList();
		if(requestItemQueue.ContainsKey(entry))
		{
			requestItemQueue.TryGetValue(entry, out guidList);
			if(guidList != null)
			{
				guidList.Add(guid);
				requestItemQueue[entry] = guidList;
			}
		}
		else
		{
			guidList.Add(guid);
			requestItemQueue.Add(entry, guidList);
		}
	}
	
	void UpdateRequestQueue(Item item)
	{
		if(requestItemQueue.ContainsKey(item.entry))
		{
			RequesterGuidList guidList = new RequesterGuidList();
			requestItemQueue.TryGetValue(item.entry, out guidList);
			if(guidList != null)
			{
				foreach(UInt64 guid in guidList.requesterGuid)
				{
					List<GameObject> characterList = new List<GameObject>(GameObject.FindGameObjectsWithTag("player"));
					foreach(GameObject character in characterList)
					{
						character.SendMessage("DeactivateRange", guid);
					}
				}
				requestItemQueue.Remove(item.entry);
			}
		}
	}

}
