﻿using UnityEngine;
using System.Collections;

public class ScreenController {

	private static uint DEFAULT_WIDTH = 1024;
	private static uint DEFAULT_HEIGHT = 768;
	private static float _msScale;
	private static float _msScaleMax;
	private static bool _msInitialized = false;
	private static float _msScaleWidth;
	private static float _msScaleHeight;
	public static int FONT_SIZE = 23;
	public static int WARNING_FONT_SIZE = 30;
	public static int YOU_LOST_FONT_SIZE = 60;
	public static int TOP_ELEMENTS_FONT_SIZE = 28;
	public static int BUTTON_FONT_SIZE = 54;
	public static int BUY_BTN_FONT = 40;
	public static int ACTIVATION_EPISODE_BTN_FONT = 38;
	public static int BANK_LABEL_FONT = 30;
	public static int PUZZLE_FINISH_WINDOW_FONT = 60;
	
	private static void Initialize()
	{
		_msInitialized = true;
		_msScaleWidth = (Screen.width * 1.0f) / DEFAULT_WIDTH;
		_msScaleHeight = (Screen.height * 1.0f) / DEFAULT_HEIGHT;
		_msScale = Mathf.Min(_msScaleWidth, _msScaleHeight);
		_msScaleMax = Mathf.Max(_msScaleWidth, _msScaleHeight);
	}	
	
	/**
	 * Retrieve scale factor
	 */
	public static float GetScaleFactor()
	{
		return _msScale;
	}
	
	/**
	 * Converting UI size
	 */
	public static Rect ConvertRectToMax(Rect rect)
	{
#if UNITY_EDITOR
		Initialize();
#else
		if(!_msInitialized)
		{
			Initialize();
		}
#endif
		rect.x *= _msScaleMax;
		rect.y *= _msScaleMax;
		
		rect.height *= _msScaleMax;
		rect.width *= _msScaleMax;
		return rect;
	}
	
	/**
	 * Converting UI size
	 */
	public static Rect ConvertRect(Rect rect)
	{
#if UNITY_EDITOR
		Initialize();
#else
		if(!_msInitialized)
		{
			Initialize();
		}
#endif
		rect.x *= _msScale;
		rect.y *= _msScale;
		
		rect.height *= _msScale;
		rect.width *= _msScale;
		return rect;
	}
	
	/**
	 * Converting Vector2
	 */
	public static Vector2 ConvertVector(Vector2 vector)
	{
#if UNITY_EDITOR
		Initialize();
#else
		if(!_msInitialized)
		{
			Initialize();
		}
#endif
		vector.x *= _msScale;
		vector.y *= _msScale;
		return vector;
	}
	
	/**
	 * Converting font size
	 */
	public static int ConvertFontSize(int fontSize)
	{
#if UNITY_EDITOR
		Initialize();
#else
		if(!_msInitialized)
		{
			Initialize();
		}
#endif 
		return (int)(fontSize * _msScale);
	}
	
	/**
	 * Convert width
	 */
	public static float ConvertWidth(float size)
	{
#if UNITY_EDITOR
		Initialize();
#else
		if(!_msInitialized)
		{
			Initialize();
		}
#endif 
		return (size * _msScaleWidth);
	}
	
	/**
	  * Convert height
	  */
	public static float ConvertHeight(float size)
	{
#if UNITY_EDITOR
		Initialize();
#else
		if(!_msInitialized)
		{
			Initialize();
		}
#endif 
		return (size * _msScaleHeight);
	}
	
	/**
	  * Get GUIStyle for different devices
	  */
	public static GUIStyle GetGUIStyle(GUIStyle retinaStyle, GUIStyle notRetina)
	{	 	
		GUIStyle style = notRetina;
#if UNITY_IPHONE
		//Retina
		if(Screen.dpi == 326)
		{
			style = retinaStyle;
		}
#else
		//TODO: check Android dpi for hdpi (~240dpi) and xhdpi (~320dpi).
		if(Screen.dpi >= 320)
		{
			style = retinaStyle;
		}
#endif
		return style;
	}
	
	/**
	  * Validate device for support retina images
	  */
	public static bool IsRetina()
	{
		bool result = false;
#if UNITY_IPHONE
		//Retina
		if(Screen.dpi == 326)
		{
			result = true;
		}
#else
		//TODO: check Android dpi for hdpi (~240dpi) and xhdpi (~320dpi).
		if(Screen.dpi >= 320)
		{
			result = true;
		}
#endif
		return result;
	}
}
