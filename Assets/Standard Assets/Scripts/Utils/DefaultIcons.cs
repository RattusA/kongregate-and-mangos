﻿using UnityEngine;
using System;

public class DefaultIcons
{
	public static string GetSpellIcon(int entry)
	{
		return GetSpellIcon((uint)entry);
	}

	public static string GetSpellIcon(uint entry)
	{
		SpellEntry spell = dbs.sSpells.GetRecord(entry);
		string iconName = "";

		if(spell.ID != 0 && spell.ID != 6603)
		{
			SpellIconEntry spellIcon  = dbs.sSpellIcons.GetRecord(spell.SpellIconID, false);
			if(spellIcon.ID != 0)
			{
				iconName = spellIcon.Icon;
				if(iconName == "" || iconName == null)
				{
					iconName = "noicon.png";
				}
			}
			else
			{
				iconName = "noicon.png";
			}
		}
//		else if(spell.ID == 6603)
//		{
//			iconName = GetAttackIcon();
//		}
		else
		{
			iconName = "noicon.png";
		}
		return iconName;
	}

	public static string GetItemIcon(uint ID)
	{
		Item item = new Item(AppCache.sItems.GetItemEntry(ID));
		return GetItemIcon(item);
	}
	
	public static string GetItemIcon(Item item)
	{
		string imageName = "noicon.png";
		imageName = GetItemIconUsedCache(item.entry);
		return imageName;
	}
	
	public static string GetItemIconUsedCache(uint ID)
	{
		uint entry = ID;
		ItemEntry item;
		ItemDisplayInfoEntry itemDisplayInfo;
		string image = "noicon.png";
		
		try
		{
			item = AppCache.sItems.GetItemEntry(entry);
			itemDisplayInfo = dbs.sItemDisplayInfo.GetRecord((int)item.DisplayID, false);
			image = itemDisplayInfo.InventoryIcon;
			if(item.ID == 0)
			{
				image = "noicon.png";
			}
			else if(string.IsNullOrEmpty(image))
			{
				image = GetDefaultIcon(item.Class, item.SubClass, item.InventorySlot);
			}
		}
		catch(Exception exception)
		{
			Debug.Log ("EXCEPTION " + exception);
			image = "noicon.png";
		}
		
		return image;
	}

	public static string GetDefaultIcon(int classID, int subclassID, int inventorySlot)
	{
		string iconName = "noicon.png";
		switch(classID)
		{
		case 0: // Consumable
			iconName = GetConsumableIcon(subclassID);
			break;
		case 1: // Container
			iconName = GetContainerIcon(subclassID);
			break;
		case 2: // Weapon
			iconName = GetWeaponIcon(subclassID);
			break;
		case 3: // Gem
			iconName = GetGemIcon(subclassID);
			break;
		case 4: // Armor
			iconName = GetArmorIcon(subclassID, inventorySlot);
			break;
		case 5: // Reagent
			iconName = GetReagentIcon(subclassID);
			break;
		case 6: // Projectile
			iconName = GetProjectileIcon(subclassID);
			break;
		case 7: // Trade Goods
			iconName = GetTradeGoodsIcon(subclassID);
			break;
		case 8: // Generic(OBSOLETE)
			iconName = GetGenericIcon(subclassID);
			break;
		case 9: // Recipe
			iconName = GetRecipeIcon(subclassID);
			break;
		case 10: // Money(OBSOLETE)
			iconName = GetMoneyIcon(subclassID);
			break;
		case 11: // Quiver
			iconName = GetQuiverIcon(subclassID);
			break;
		case 12: // Quest
			iconName = GetQuestIcon(subclassID);
			break;
		case 13: // Key
			iconName = GetKeyIcon(subclassID);
			break;
		case 14: // Permanent(OBSOLETE)
			iconName = GetPermanentIcon(subclassID);
			break;
		case 15: // Miscellaneous
			iconName = GetMiscellaneousIcon(subclassID);
			break;
		case 16: // Glyph
			iconName = GetGlyphIcon(subclassID);
			break;
		}
		if(iconName == "")
		{
			iconName = "noicon.png";
		}
		return iconName;
	}
	
	static string GetConsumableIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch(subclassID)
		{
		case 0: // Consumable
			iconName = "Consumable.png";
			break;
		case 1: // Potion
			iconName = "potion.png";
			break;
		case 2: // Elixir
			iconName = "elixir.png";
			break;
		case 3: // Flask
			iconName = "Flask.png";
			break;
		case 4: // Scroll
			iconName = "Scroll.png";
			break;
		case 5: // Food & Drink
			iconName = "Food & Drink.png";
			break;
		case 6: //Item Enhancement
			iconName = "Item Enhancement.png";
			break;
		case 7: // Bandage
			iconName = "bandage.png";
			break;
		case 8: // Other
			iconName = "Other.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetContainerIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch(subclassID)
		{
		case 0: // Bag
			iconName = "Bag.png";
			break;
		case 1: // Soul Bag
			iconName = "Soul Bag.png";
			break;
		case 2: // Herb Bag
			iconName = "Herb Bag.png";
			break;
		case 3: // Enchanting Bag
			iconName = "Enchanting Bag.png";
			break;
		case 4: // Engineering Bag
			iconName = "Engineering Bag.png";
			break;
		case 5: // Gem Bag
			iconName = "Gem Bag.png";
			break;
		case 6: // Mining Bag
			iconName = "Mining Bag.png";
			break;
		case 7: // Leatherworking Bag
			iconName = "Leatherworking Bag.png";
			break;
		case 8: // Inscription Bag
			iconName = "Inscription Bag.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetWeaponIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch(subclassID)
		{
		case 0: // Axe One handed
			iconName = "AxeOne handed.png";
			break;
		case 1: // Axe Two handed
			iconName = "AxeTwo handed.png";
			break;
		case 2: // Bow
			iconName = "Bow.png";
			break;
		case 3: // Gun
			iconName = "Gun.jpg";
			break;
		case 4: // Mace One handed
			iconName = "MaceOne handed.png";
			break;
		case 5: // Mace Two handed
			iconName = "MaceTwo handed.png";
			break;
		case 6: // Polearm
			iconName = "Polearm.png";
			break;
		case 7: // Sword One handed
			iconName = "SwordOne handed.png";
			break;
		case 8: // Sword Two handed
			iconName = "SwordTwo handed.png";
			break;
		case 9: // Obsolete
			iconName = "Obsolete.png";
			break;
		case 10: // Staff
			iconName = "Staff.png";
			break;
		case 11: // Exotic
			iconName = "Exotic (use same for both).png";
			break;
		case 12: // Exotic
			iconName = "Exotic (use same for both).png";
			break;
		case 13: // Fist Weapon
			iconName = "Fist Weapon.png";
			break;
		case 14: // Miscellaneous   (Blacksmith Hammer, Mining Pick, etc.)
			iconName = "Miscellaneous.png";
			break;
		case 15: // Dagger
			iconName = "Dagger.png";
			break;
		case 16: // Thrown
			iconName = "Thrown.png";
			break;
		case 17: // Spear
			iconName = "Spear.png";
			break;
		case 18: // Crossbow
			iconName = "Crossbow.png";
			break;
		case 19: // Wand
			iconName = "Wand.png";
			break;
		case 20: // Fishing Pole
			iconName = "Fishing Pole.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetGemIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch(subclassID)
		{
		case 0: // Red
			iconName = "Red.png";
			break;
		case 1: // Blue
			iconName = "Blue.png";
			break;
		case 2: // Yellow
			iconName = "Yellow.png";
			break;
		case 3: // Purple
			iconName = "Purple.png";
			break;
		case 4: // Green
			iconName = "Green.png";
			break;
		case 5: // Orange
			iconName = "Orange.png";
			break;
		case 6: // Meta
			iconName = "Meta.png";
			break;
		case 7: // Simple
			iconName = "Simple.png";
			break;
		case 8: // Prismatic
			iconName = "saphire.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetArmorIcon(int subclassID, int invSlot)
	{
		string iconName  = "noicon.png";
		switch(subclassID)
		{
		case 0: // Miscellaneous
			iconName = GetMiscellaneousArmorSlotName(invSlot);
			if(iconName == "")
			{
				iconName = GetAdditionalArmorSlotName(invSlot);
			}
			if(iconName == "")
			{
				iconName = "MiscellaneousArmor.png";
			}
			break;
		case 1: // Cloth
			iconName = GetArmorSlotName(invSlot);
			if(iconName == "")
			{
				iconName = GetAdditionalArmorSlotName(invSlot);
			}
			else
			{
				iconName = "Cloth" + iconName + ".png";
			}
			break;
		case 2: // Leather
			iconName = GetArmorSlotName(invSlot);
			if(iconName == "")
			{
				iconName = GetAdditionalArmorSlotName(invSlot);
			}
			else
			{
				iconName = "Leather" + iconName + ".png";
			}
			break;
		case 3: // Mail
			iconName = GetArmorSlotName(invSlot);
			if(iconName == "")
			{
				iconName = GetAdditionalArmorSlotName(invSlot);
			}
			else
			{
				iconName = "Mail" + iconName + ".png";
			}
			break;
		case 4: // Plate
			iconName = GetArmorSlotName(invSlot);
			if(iconName == "")
			{
				iconName = GetAdditionalArmorSlotName(invSlot);
			}
			else
			{
				iconName = "Plate" + iconName + ".png";
			}
			break;
		case 5: // Buckler(OBSOLETE)
			iconName = "Buckler.png";
			break;
		case 6: // Shield
			iconName = "ShieldArmor.png";
			break;
		case 7: // Libram
			iconName = "Libram.png";
			break;
		case 8: // Idol
			iconName = "Idol.png";
			break;
		case 9: // Totem
			iconName = "Totem.png";
			break;
		case 10: // Sigil
			iconName = "Sigil.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetReagentIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch(subclassID)
		{
		case 0: // Reagent
			iconName = "Reagent.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetProjectileIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch(subclassID)
		{
		case 0: // Wand(OBSOLETE)
			iconName = "WandProjectile.png";
			break;
		case 1: // Bolt(OBSOLETE) 
			iconName = "Bolt.png";
			break;
		case 2: // Arrow
			iconName = "Arrow.png";
			break;
		case 3: // Bullet
			iconName = "Bullet.png";
			break;
		case 4: // Thrown(OBSOLETE)
			iconName = "Thrown.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetTradeGoodsIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch(subclassID)
		{
		case 0: // Trade Goods
			iconName = "Trade Goods.png";
			break;
		case 1: // Parts
			iconName = "Parts.png";
			break;
		case 2: // Explosives
			iconName = "Explosives.png";
			break;
		case 3: // Devices
			iconName = "Devices.png";
			break;
		case 4: // Jewelcrafting
			iconName = "Jewelcrafting.png";
			break;
		case 5: // Cloth
			iconName = "Cloth.png";
			break;
		case 6: // Leather
			iconName = "Leather.png";
			break;
		case 7: // Metal & Stone
			iconName = "Metal & Stone.png";
			break;
		case 8: // Meat
			iconName = "Meat.png";
			break;
		case 9: // Herb
			iconName = "Herb.png";
			break;
		case 10: // Elemental
			iconName = "Elemental.png";
			break;
		case 11: // Other
			iconName = "OtherTradeGoods.png";
			break;
		case 12: // Enchanting
			iconName = "Enchanting.png";
			break;
		case 13: // Materials
			iconName = "Materials.png";
			break;
		case 14: // Armor Enchantment
			iconName = "Armor Enchantment.png";
			break;
		case 15: // Weapon Enchantment
			iconName = "Weapon Enchantment.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetGenericIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch(subclassID)
		{
		case 0: // Generic(OBSOLETE)
			iconName = "Generic.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetRecipeIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch(subclassID)
		{
		case 0: // Book
			iconName = "Book.png";
			break;
		case 1: // Leatherworking
			iconName = "Leatherworking.png";
			break;
		case 2: // Tailoring
			iconName = "Tailoring.png";
			break;
		case 3: // Engineering
			iconName = "Engineering.png";
			break;
		case 4: // Blacksmithing
			iconName = "Blacksmithing.png";
			break;
		case 5: // Cooking
			iconName = "Cooking.png";
			break;
		case 6: // Alchemy
			iconName = "Alchemy.png";
			break;
		case 7: // First Aid
			iconName = "First Aid.png";
			break;
		case 8: // Enchanting
			iconName = "Enchanting.png";
			break;
		case 9: // Fishing
			iconName = "Fishing.png";
			break;
		case 10: // Jewelcrafting
			iconName = "Jewelcrafting.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetMoneyIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch((ItemSubclassMoney)subclassID)
		{
		case ItemSubclassMoney.ITEM_SUBCLASS_MONEY: // Money(OBSOLETE)
			iconName = "money.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetQuiverIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch((ItemSubclassQuiver)subclassID)
		{
		case ItemSubclassQuiver.ITEM_SUBCLASS_QUIVER0: // Quiver(OBSOLETE)
			iconName = "Quiver.jpg";
			break;
		case ItemSubclassQuiver.ITEM_SUBCLASS_QUIVER1: // Quiver(OBSOLETE) 
			iconName = "Quiver.jpg";
			break;
		case ItemSubclassQuiver.ITEM_SUBCLASS_QUIVER: // Quiver   Can hold arrows
			iconName = "Quiver.jpg";
			break;
		case ItemSubclassQuiver.ITEM_SUBCLASS_AMMO_POUCH: // Ammo Pouch   Can hold bullets
			iconName = "Ammo Pouch.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetQuestIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch((ItemSubclassQuest)subclassID)
		{
		case ItemSubclassQuest.ITEM_SUBCLASS_QUEST: // Quest
			iconName = "Quest.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetKeyIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch((ItemSubclassKey)subclassID)
		{
		case ItemSubclassKey.ITEM_SUBCLASS_KEY: // Key
			iconName = "Key.png";
			break;
		case ItemSubclassKey.ITEM_SUBCLASS_LOCKPICK: // Lockpick
			iconName = "Lockpick.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetPermanentIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch((ItemSubclassPermanent)subclassID)
		{
		case ItemSubclassPermanent.ITEM_SUBCLASS_PERMANENT: // Permanent
			iconName = "Permanent.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetMiscellaneousIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch((ItemSubclassJunk)subclassID)
		{
		case ItemSubclassJunk.ITEM_SUBCLASS_JUNK: // Junk
			iconName = "Junk.png";
			break;
		case ItemSubclassJunk.ITEM_SUBCLASS_JUNK_REAGENT: // Reagent 
			iconName = "ReagentMiscellaneous.png";
			break;
		case ItemSubclassJunk.ITEM_SUBCLASS_JUNK_PET: // Pet
			iconName = "Pet.png";
			break;
		case ItemSubclassJunk.ITEM_SUBCLASS_JUNK_HOLIDAY: // Holiday
			iconName = "Holiday.png";
			break;
		case ItemSubclassJunk.ITEM_SUBCLASS_JUNK_OTHER: // Other
			iconName = "OtherMiscellaneous.png";
			break;
		case ItemSubclassJunk.ITEM_SUBCLASS_JUNK_MOUNT: // Mount
			iconName = "Mount.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetGlyphIcon(int subclassID)
	{
		string iconName = "noicon.png";
		switch((ItemSubclassGlyph)subclassID)
		{
		case ItemSubclassGlyph.ITEM_SUBCLASS_GLYPH_WARRIOR: // Warrior
			iconName = "Warrior.png";
			break;
		case ItemSubclassGlyph.ITEM_SUBCLASS_GLYPH_PALADIN: // Paladin
			iconName = "Paladin.png";
			break;
		case ItemSubclassGlyph.ITEM_SUBCLASS_GLYPH_HUNTER: // Hunter
			iconName = "Hunter.png";
			break;
		case ItemSubclassGlyph.ITEM_SUBCLASS_GLYPH_ROGUE: // Rogue
			iconName = "Rogue.png";
			break;
		case ItemSubclassGlyph.ITEM_SUBCLASS_GLYPH_PRIEST: // Priest
			iconName = "Priest.png";
			break;
		case ItemSubclassGlyph.ITEM_SUBCLASS_GLYPH_DEATH_KNIGHT: // Death Knight
			iconName = "Death Knight.png";
			break;
		case ItemSubclassGlyph.ITEM_SUBCLASS_GLYPH_SHAMAN: // Shaman
			iconName = "Shaman.png";
			break;
		case ItemSubclassGlyph.ITEM_SUBCLASS_GLYPH_MAGE: // Mage
			iconName = "Mage.png";
			break;
		case ItemSubclassGlyph.ITEM_SUBCLASS_GLYPH_WARLOCK: // Warlock
			iconName = "Warlock.png";
			break;
		case ItemSubclassGlyph.ITEM_SUBCLASS_GLYPH_DRUID: // Druid
			iconName = "Druid.png";
			break;
		default: // unknown
			iconName = "noicon.png";
			break;
		}
		return iconName;
	}
	
	static string GetMiscellaneousArmorSlotName(int invSlot)
	{
		string iconName = "";
		switch((InventoryType)invSlot)
		{
		case InventoryType.INVTYPE_NECK:
			iconName = "Neck.png";
			break;
		case InventoryType.INVTYPE_FINGER:
			iconName = "Ring.png";
			break;
		default:
			iconName = "";
			break;
		}
		return iconName;
	}
	
	static string GetArmorSlotName(int invSlot)
	{
		string iconName = "";
		switch((InventoryType)invSlot)
		{
		case InventoryType.INVTYPE_HEAD:
			iconName = "-Head";
			break;
		case InventoryType.INVTYPE_SHOULDERS:
			iconName = "-Shoulder";
			break;
		case InventoryType.INVTYPE_CHEST:
		case InventoryType.INVTYPE_ROBE:
			iconName = "-Chest";
			break;
		case InventoryType.INVTYPE_WAIST:
			iconName = "-Belt";
			break;
		case InventoryType.INVTYPE_LEGS:
			iconName = "-Legs";
			break;
		case InventoryType.INVTYPE_FEET:
			iconName = "-Feet";
			break;
		case InventoryType.INVTYPE_WRISTS:
			iconName = "-Wrist";
			break;
		case InventoryType.INVTYPE_HANDS:
			iconName = "-Hands";
			break;
		default:
			iconName = "";
			break;
		}
		return iconName;
	}
	
	static string GetAdditionalArmorSlotName(int invSlot)
	{
		string iconName = "";
		switch((InventoryType)invSlot)
		{
		case InventoryType.INVTYPE_CLOAK:
		case InventoryType.INVTYPE_TABARD:
			iconName = "Cloak.png";
			break;
		case InventoryType.INVTYPE_TRINKET:
			iconName = "trinket1_04.png";
			break;
		default:
			iconName = "";
			break;
		}
		return iconName;
	}

}
