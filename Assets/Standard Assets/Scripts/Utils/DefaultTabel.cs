using System.Collections;

static public class DefaultTabel
{
	static public IDictionary itemSubclassArmor;
	static public IDictionary inventoryType;
	static public IDictionary itemQualities;
	static public IDictionary itemDefaultDisplayID;
	static public IDictionary petFamilyName;
	static public IDictionary petFoodName;
	static public IDictionary fractionName;
	static public IDictionary fractionEntry;
	static public IDictionary playerSkill;
	static public IDictionary factionTemplate;
	static public IDictionary mapTemplate;
	
	static DefaultTabel ()
	{
		itemSubclassArmor = (IDictionary)JSONUtil.DecodeResource("config_json/ItemSubclassArmor_json");
		inventoryType  = (IDictionary)JSONUtil.DecodeResource("config_json/InventoryType_json");
		itemQualities = (IDictionary)JSONUtil.DecodeResource("config_json/ItemQualities_json");
		itemDefaultDisplayID = (IDictionary)JSONUtil.DecodeResource("config_json/ItemDefaultDisplayID_json");
		petFoodName = (IDictionary)JSONUtil.DecodeResource("config_json/PetFood_json");
		petFamilyName = (IDictionary)JSONUtil.DecodeResource("config_json/PetName_json");
		fractionName = (IDictionary)JSONUtil.DecodeResource("config_json/FractionName_json");
		fractionEntry = (IDictionary)JSONUtil.DecodeResource("config_json/FractionEntry_json");
		playerSkill = (IDictionary)JSONUtil.DecodeResource("config_json/PlayerSkill_json");
		factionTemplate = (IDictionary)JSONUtil.DecodeResource("config_json/FactionTemplate_json");
		mapTemplate = (IDictionary)JSONUtil.DecodeResource("config_json/MapTemplate_json");
	}
}
