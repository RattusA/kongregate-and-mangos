﻿using UnityEngine;
using System;

public class TestPlayer : MonoBehaviour
{
	public string object_name;
	GameObject obj2;
	GameObject obj;
	public string current_animation = null;
	public int current_anim_mode = 0;
	public string current_weapon = null;
	public string current_armor_set = null;
	GameObject weapon_obj = null;
	Animation anim = null;
	public Transform leftHand;
	public Transform rightHand;
	public Transform rightFinger;
	public Transform leftFinger;
	public Transform rightFinger1;
	public Transform leftFinger1;
	
	Transform rightThumb1;
	Transform leftThumb1;
	public bool ischar;

	void  Start ()
	{
		obj2 = transform.gameObject;
		obj = obj2.transform.Find("obj").gameObject;
		anim = obj.GetComponent<Animation>();
		Debug.Log("Player created");
		try
		{
			leftHand = obj2.transform.Find    ("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L ForeArm/Bip01 L Hand");
			if(!leftHand)//unele modele au denumita altfel structura de oase
				leftHand = obj2.transform.Find("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Neck/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L Forearm/Bip01 L Hand");
			
			rightHand = obj2.transform.Find("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R ForeArm/Bip01 R Hand");
			if(!rightHand)
				rightHand = obj2.transform.Find("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R Forearm/Bip01 R Hand");
			rightFinger = rightHand.Find("Bip01 R Finger0");
			leftFinger = rightHand.Find("Bip01 R Finger1");
			rightFinger1 = rightFinger.Find("Bip01 R Finger01");
			leftFinger1 = leftFinger.Find("Bip01 R Finger11");
			
			rightThumb1 = rightFinger1.Find("Bip01 R Finger0Nub");
			leftThumb1 = leftFinger1.Find("Bip01 R Finger1Nub");
			Debug.Log("Player created2");
			
			EquipArmor("Standard");
		}catch( Exception e  ){Debug.Log("Not all components initialized");}
		
		
	}
	/*
void  SendCreatedMessage (){
	Transform tt = gameObject.transform;
	System.UInt16 opc = OpCodes.CMSG_TESTPLAYER_CREATED;
	WorldPacket wp = new WorldPacket(opc);
	byte l = object_name.length;
	wp.Append(l);
	wp.appendSTR(object_name+"\0");
	if(ischar)
	l=1;
	else
	l=0;
	wp.Append(l);
	wp.Append(tt.position.x);
	wp.Append(tt.position.y);
	wp.Append(tt.position.z);
	wp.SetOpcode(opc);
	RealmSocket.outQueue.Add(wp);
}*/
	public void  EquipArmor ( string val  )
	{
		Debug.Log("Equipping "+val);
		
		Transform obj_transf= obj2.transform.Find("obj");
		if(!obj_transf)
			return;
		
		obj_transf.gameObject.SetActive(false);
		obj_transf.gameObject.SetActive(true);
		
		Transform transf = obj2.transform.Find("obj/Bip01");
		if(transf)
			transf.gameObject.SetActive(true);
		
		transf = obj2.transform.Find("obj/Standard Face");
		if(transf)
			transf.gameObject.SetActive(true);
		
		transf = obj2.transform.Find("obj/Standard Hair");
		if(transf)
			transf.gameObject.SetActive(true);
		try
		{
			transf = obj2.transform.Find("obj/Standard Body");
			if(transf)
				transf.gameObject.SetActive(true);
			
			transf = obj2.transform.Find("obj/Standard Arms");
			if(transf)
				transf.gameObject.SetActive(true);
			transf = obj2.transform.Find("obj/Standard Belt");
			if(transf)
				transf.gameObject.SetActive(true);
			
			transf = obj2.transform.Find("obj/Standard Belt1");
			if(transf)
				transf.gameObject.SetActive(true);
			transf = obj2.transform.Find("obj/Standard Belt2");
			if(transf)
				transf.gameObject.SetActive(true);
		}catch( Exception e  ){Debug.Log(e);}
		try{
			obj2.transform.Find("obj/Armory/"+val).gameObject.SetActive(true);
		}catch( Exception e  ){Debug.Log(e+"   "+val); /*obj2.transform.Find("obj/"+val).gameObject.SetActive(true);*/}
		current_armor_set = val;
		
	}
	
	public void  EquipWeapon ( Transform _trLeftHand ,   Transform _trRightHand ,   GameObject item  )
	{
		if(weapon_obj)
			Destroy(weapon_obj);
		Debug.Log("Equipping");
		//GameObject go = null;
		Quaternion rot= Quaternion.identity;
		rot.eulerAngles = Vector3.zero;

		//Loading Weapons
		weapon_obj = GameObject.Instantiate(item);
		if(!weapon_obj)
		{
			Debug.Log("no weapon");
			return;	
		}
		
		if(_trLeftHand)
		{
			weapon_obj.transform.parent = _trLeftHand;
		}
		else
		{
			if(_trRightHand)
			{
				weapon_obj.transform.parent = _trRightHand;
			}
		}
		
		if(rightFinger && rightFinger1 && leftFinger && leftFinger1)
		{
			weapon_obj.transform.localPosition = (2*rightFinger.localPosition+rightFinger1.localPosition + 2*leftFinger.localPosition+leftFinger1.localPosition)*0.25f;
		}
		//go.transform.localPosition = rightFinger.localPosition + rightFinger1.localPosition;
		//go.transform.localPosition = (rightFinger.localPosition+rightFinger1.localPosition+rightThumb1.localPosition + 2*leftFinger.localPosition+leftFinger1.localPosition+leftThumb1.localPosition)*0.25f;
		weapon_obj.transform.localRotation = rot;
	}

	public void  ChangeAnimation ( string anim_string, int anim_mode )
	{
		if(!anim)
			return;
		
		if(anim_string != null)
		{
			if(anim_mode >= 0)
				anim[anim_string].wrapMode = (WrapMode)anim_mode;
			anim.CrossFadeQueued(anim_string, 0.3f, QueueMode.PlayNow);
		}
		current_animation = anim_string;
		current_anim_mode = anim_mode;
		
	}
	/*
void  SendUpdateData (){
	System.UInt16 opc = OpCodes.CMSG_TESTPLAYER_UPDATED;
	WorldPacket wp = new WorldPacket(opc);
	//byte l;
	wp.SetOpcode(opc);
	wp.appendSTR(current_animation+"\0");
	wp.appendSTR(current_weapon+"\0");
	wp.appendSTR(current_armor_set+"\0");
	wp.Append(current_anim_mode);
	RealmSocket.outQueue.Add(wp);
	Debug.Log("Message Sent: "+current_animation+" "+current_weapon+" "+current_armor_set+" "+current_anim_mode);
}*/
}