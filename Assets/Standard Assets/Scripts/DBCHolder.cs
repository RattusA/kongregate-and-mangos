using UnityEngine;
using System;
using System.Runtime.InteropServices;
using System.Reflection;

public struct LocalizedString
{
	public string this [int index] {
		get {
			return Strings [index];
		}
	}
	
	public int[] stringOffset;
	public string[] Strings;
	public int mask;

	/*    public override string ToString()
        {
            string s = "Ls(";
            foreach(string t in Strings)
                s+=t+",";
            s+=mask.ToString("x")+")";
            return s;
        }*/
}
 
public struct Int2
{
	public int this [int index] {
		get {
			return array [index];
		}
	}
        
	public int[] array;
}

public struct Int3
{
	public int this [int index] {
		get {
			return array [index];
		}
	}
        
	public int[] array;
}

public struct Float2
{
	public float this [int index] {
		get {
			return array [index];
		}
	}
        
	public float[] array;
}

public struct Float3
{
	public float this [int index] {
		get {
			return array [index];
		}
	}
        
	public float[] array;
}

public struct Int8
{
	public int this [int index] {
		get {
			return array [index];
		}
	}
        
	public int[] array;
}
  
[StructLayout(LayoutKind.Sequential)]
public class ItemEntry
{
	public uint ID;
	public int Class;
	public int SubClass;
	public uint DisplayID;
	public int InventorySlot;
	public string Name;
}

[StructLayout(LayoutKind.Sequential)]
public class QualityItemEntry
{
	public ItemEntry itemEntry;
	public int quality;
	public int requiredLevel;
	public int itemlevel;
	public string description;
}

[StructLayout(LayoutKind.Sequential)]
public class ServerItemEntry
{
	public uint ID;
	public uint classId;
	public uint subClass;
	public int unk0;
	public int material;
	public uint displayID;
	public uint inventoryType;
	public uint sheath;

	public ItemEntry ToItemEntry()
	{
		ItemEntry ret = new ItemEntry();
		ret.ID = ID;
		ret.Class = (int)classId;
		ret.SubClass = (int)subClass;
		ret.DisplayID = displayID;
		ret.InventorySlot = (int)inventoryType;
		ret.Name = "";
		return ret;
	}
}

[StructLayout(LayoutKind.Sequential)]
  public class ItemClassEntry
{
	public uint ID;
	public LocalizedString Name;
	//public int ceeva;
}
 
[StructLayout(LayoutKind.Sequential)]
  public class ItemSubClassEntry
{
	public uint ID;
	public int SubClass;
	public string Name;
}
 
[StructLayout(LayoutKind.Sequential)]
  public class ItemInventoryTypeEntry
{
	public uint ID;
	public string Name;
         
}
  
[StructLayout(LayoutKind.Sequential)]
  public class ItemDisplayInfoEntry
{
	public uint ID;
	public string ModelMesh;
	public string BigTexture;
	public string InventoryIcon;
	public string SpellVisual;
	public int unk1;
	public int unk2;
	public int unk3;      
}
   
[StructLayout(LayoutKind.Sequential)]
  public class CreatureDisplayInfoEntry
{
	public uint ID;
	public float Scale;
	public byte Opacity;
	public uint NPCSoundID;
	public uint SoundID;
	public string ModelMesh;
	public string BigTexture;
	public string Portrait;
	public int unk1;
	public int unk2;
}
  
[StructLayout(LayoutKind.Sequential)]
  public class GameObjectDisplayInfoEntry
{
	public uint ID;
	public float Scale;
	public byte Opacity;
	public int SoundID;
	public string ModelMesh;
	public string BigTexture;
	public string Portrait;
	public int unk1;
	public int unk2;
}
  
[StructLayout(LayoutKind.Sequential)]
  public class CreatureTypeEntry
{
	public uint ID;
	public string Name;
}
  
[StructLayout(LayoutKind.Sequential)]
  public class SpellDurationEntry
{
	public uint ID;
	public int unk;
	public int unk1;
	public int unk2;
}
  
[StructLayout(LayoutKind.Sequential)]
  public class SpellIconEntry
{
	public int ID;
	public string BigTexture;
	public string Icon;
}
    
[StructLayout(LayoutKind.Sequential)]
  public class SpellCastTimesEntry
{
	public uint ID;
	public int unk;
	public int unk1;
	public int unk2;
}
  
[StructLayout(LayoutKind.Sequential)]
  public class CharacterRaceEntry
{
	public uint ID;
	public string Name;
}
   
[StructLayout(LayoutKind.Sequential)]
  public class CharacterClassEntry
{
	public uint ID;
	public string Name;
}
  
[StructLayout(LayoutKind.Sequential)]
  public class CharacterGenderEntry
{
	public uint ID;
	public string Name;
}
  
[StructLayout(LayoutKind.Sequential)]
  public class AreaTriggerEntry
{
	public uint ID;
	public int MapID;
	public float x;
	public float y;
	public float z;
	public float Radius;//in yards; 1 yards = 0.9144 meters
	public float BoxX;
	public float BoxY;
	public float BoxZ;
	public float BoxO;
}

[StructLayout(LayoutKind.Sequential)] 
	public class TalentEntry
{
	public int ID;			//ID
	public int TabID;
	public int Column;
	public int Row;
	public int SpellID1;
	public int SpellID2;
	public int SpellID3;
	public int SpellID4;
	public int SpellID5;
	public int SpellID6;
	public int SpellID7;
	public int SpellID8;
	public int SpellID9;
	public int ReqTalentID1;
	public int ReqTalentID2;
	public int ReqTalentID3;
	public int TalentCount1;
	public int TalentCount2;
	public int TalentCount3;
	public int ActiveSpell;
	public int ReqSpellID;
	public int AllowForPetFlags1;
	public int AllowForPetFlags2;
	
	
}

[StructLayout(LayoutKind.Sequential)]
	public class TalentTabEntry
{
	public int ID;
	public LocalizedString Name;
	public int SpellIconID;
	public int Races;
	public int Classes;
	public int CreatureFamily;
	public int TabNumber;
	public String NameInternal;
	
}
  
[StructLayout(LayoutKind.Sequential)]   
  public class SpellEntry
{
	//private const int MAX_SPELL_REAGENTS = 8;
	//private const int MAX_SPELL_TOTEMS = 2;
	//private const int MAX_SPELL_TOTEM_CATEGORIES = 2;
	//private const int MAX_EFFECT_INDEX = 3;
  	
	public uint    ID;                                           // 0        m_ID				
	public int    Category;                                     // 1        m_category      not important
	public int    Dispel;                                       // 2        m_dispelType	(ex. magic, curse, none ... )
	public int    Mechanic;                                     // 3        m_mechanic	(ex charnedm disarmed, fleeing, ... )
	public int    Attributes;                                   // 4        m_attribute		
	public int    AttributesEx;                                 // 5        m_attributesEx
	public int    AttributesEx2;                                // 6        m_attributesExB
	public int    AttributesEx3;                                // 7        m_attributesExC
	public int    AttributesEx4;                                // 8        m_attributesExD
	public int    AttributesEx5;                                // 9        m_attributesExE
	public int    AttributesEx6;                                // 10       m_attributesExF
	public int    AttributesEx7;                                // 11       m_attributesExG (0x20 - totems, 0x4 - paladin auras, etc...)
	public int    Stances;                                      // 12       m_shapeshiftMask
	public int    unk_320_2;                                    // 13       3.2.0
	public int    StancesNot;                                   // 14       m_shapeshiftExclude
	public int    unk_320_3;                                    // 15       3.2.0
	public int    Targets;                                      // 16       m_targets
	public int    TargetCreatureType;                           // 17       m_targetCreatureType
	public int    RequiresSpellFocus;                           // 18       m_requiresSpellFocus
	public int    FacingCasterFlags;                            // 19       m_facingCasterFlags
	public int    CasterAuraState;                              // 20       m_casterAuraState
	public int    TargetAuraState;                              // 21       m_targetAuraState
	public int    CasterAuraStateNot;                           // 22       m_excludeCasterAuraState
	public int    TargetAuraStateNot;                           // 23       m_excludeTargetAuraState
	public int    casterAuraSpell;                              // 24       m_casterAuraSpell
	public int    targetAuraSpell;                              // 25       m_targetAuraSpell
	public int    excludeCasterAuraSpell;                       // 26       m_excludeCasterAuraSpell
	public int    excludeTargetAuraSpell;                       // 27       m_excludeTargetAuraSpell
	public int    CastingTimeIndex;                             // 28       m_castingTimeIndex
	public int    RecoveryTime;                                 // 29       m_recoveryTime
	public int    CategoryRecoveryTime;                         // 30       m_categoryRecoveryTime
	public int    InterruptFlags;                               // 31       m_interruptFlags
	public int    AuraInterruptFlags;                           // 32       m_auraInterruptFlags
	public int    ChannelInterruptFlags;                        // 33       m_channelInterruptFlags
	public int    procFlags;                                    // 34       m_procTypeMask
	public int    procChance;                                   // 35       m_procChance
	public int    procCharges;                                  // 36       m_procCharges
	public int    maxLevel;                                     // 37       m_maxLevel				
	public int    baseLevel;                                    // 38       m_baseLevel
	public int    spellLevel;                                   // 39       m_spellLevel
	public int    DurationIndex;                                // 40       m_durationIndex
	public int    powerType;                                    // 41       m_powerType
	public int    manaCost;                                     // 42       m_manaCost
	public int    manaCostPerlevel;                             // 43       m_manaCostPerLevel
	public int    manaPerSecond;                                // 44       m_manaPerSecond
	public int    manaPerSecondPerLevel;                        // 45       m_manaPerSecondPerLevel
	public int    rangeIndex;                                   // 46       m_rangeIndex
	public float     speed;                                        // 47       m_speed
	public int    modalNextSpell;                             // 48       m_modalNextSpell not used
	public int    StackAmount;                                  // 49       m_cumulativeAura
	public Int2    Totem;                      // 50-51    m_totem
	public Int8     Reagent;                  // 52-59    m_reagent
	public Int8    ReagentCount;             // 60-67    m_reagentCount
	public int     EquippedItemClass;                            // 68       m_equippedItemClass (value)
	public int     EquippedItemSubClassMask;                     // 69       m_equippedItemSubclass (mask)
	public int     EquippedItemInventoryTypeMask;                // 70       m_equippedItemInvTypes (mask)
	public Int3    Effect;                     // 71-73    m_effect
	public Int3     EffectDieSides;             // 74-76    m_effectDieSides
	public Int3     EffectRealPointsPerLeve;   // 77-79    m_effectRealPointsPerLevel
	public Int3     EffectBasePoints;           // 80-82    m_effectBasePoints (don't must be used in spell/auras explicitly, must be used cached Spell::m_currentBasePoints)
	public Int3    EffectMechanic;             // 83-85    m_effectMechanic
	public Int3    EffectImplicitTargetA;      // 86-88    m_implicitTargetA
	public Int3    EffectImplicitTargetB;      // 89-91    m_implicitTargetB
	public Int3    EffectRadiusIndex;          // 92-94    m_effectRadiusIndex - spellradius.dbc
	public Int3    EffectApplyAuraName;        // 95-97    m_effectAura
	public Int3    EffectAmplitude;            // 98-100   m_effectAuraPeriod
	public Float3     EffectMultipleValue;        // 101-103  m_effectAmplitude
	public Int3    EffectChainTarget;          // 104-106  m_effectChainTargets
	public Int3    EffectItemType;             // 107-109  m_effectItemType
	public Int3     EffectMiscValue ;            // 110-112  m_effectMiscValue
	public Int3     EffectMiscValueB;           // 113-115  m_effectMiscValueB
	public Int3    EffectTriggerSpell;         // 116-118  m_effectTriggerSpell
	public Float3     EffectPointsPerComboPoint;  // 119-121  m_effectPointsPerCombo
	public Int3    EffectSpellClassMaskA;                     // 122-124  m_effectSpellClassMaskA, effect 0
	public Int3    EffectSpellClassMaskB;                     // 125-127  m_effectSpellClassMaskB, effect 1
	public Int3    EffectSpellClassMaskC;                     // 128-130  m_effectSpellClassMaskC, effect 2
	public Int2    SpellVisual;                               // 131-132  m_spellVisualID
	public int    SpellIconID;                                  // 133      m_spellIconID
	public int    activeIconID;                                 // 134      m_activeIconID
	public int    spellPriority;                              // 135      m_spellPriority not used
	public LocalizedString     SpellName;                                // 136-151  m_name_lang
	//  public int    SpellNameFlag;                              // 152      m_name_flag not used
	public LocalizedString     Rank;                                     // 153-168  m_nameSubtext_lang
	//   public int    RankFlags;                                  // 169      m_nameSubtext_flag not used
	public LocalizedString    Description;                            // 170-185  m_description_lang not used
	//  public int    DescriptionFlags;                           // 186      m_description_flag not used
	public LocalizedString    ToolTip;                                // 187-202  m_auraDescription_lang not used
	//  public int    ToolTipFlags;                               // 203      m_auraDescription_flag not used
	public int    ManaCostPercentage;                           // 204      m_manaCostPct
	public int    StartRecoveryCategory;                        // 205      m_startRecoveryCategory
	public int    StartRecoveryTime;                            // 206      m_startRecoveryTime
	public int    MaxTargetLevel;                               // 207      m_maxTargetLevel
	public int    SpellFamilyName;                              // 208      m_spellClassSet
	public Int2    SpellFamilyFlags;                             // 209-210  m_spellClassMask NOTE: size is 12 bytes!!!
	public int    SpellFamilyFlags2;                            // 211      addition to m_spellClassMask
	public int    MaxAffectedTargets;                           // 212      m_maxTargets
	public int    DmgClass;                                     // 213      m_defenseType
	public int    PreventionType;                               // 214      m_preventionType
	public int    StanceBarOrder;                             // 215      m_stanceBarOrder not used
	public Float3     DmgMultiplier;              // 216-218  m_effectChainAmplitude
	public int    MinFactionId;                               // 219      m_minFactionID not used
	public int    MinReputation;                              // 220      m_minReputation not used
	public int    RequiredAuraVision;                         // 221      m_requiredAuraVision not used
	public Int2    TotemCategory;    // 222-223  m_requiredTotemCategoryID
	public int     AreaGroupId;                                  // 224      m_requiredAreaGroupId
	public int    SchoolMask;                                   // 225      m_schoolMask
	public int    runeCostID;                                   // 226      m_runeCostID
	public int    spellMissileID;                             // 227      m_spellMissileID not used
	public int  PowerDisplayId;                               // 228      m_powerDisplayID - id from PowerDisplay.dbc, new in 3.1
	public Float3   unk_320_4;                                 // 229-231  3.2.0
	public int  spellDescriptionVariableID;                   // 232      m_spellDescriptionVariableID, 3.2.0
	public int  SpellDifficultyId;                              // 233      m_spellDifficultyID - id from SpellDifficulty.dbc
	/* 
    private SpellEntry( )
    {
 	DmgMultiplier = new float[3];              // 216-218  m_effectChainAmplitude
 	TotemCategory = new int[3];    // 222-223  m_requiredTotemCategoryID
       unk_320_4 = new float[3];                                 // 229-231  3.2.0

      Totem = new  int[2];                      // 50-51    m_totem
      Reagent = new  int[8];                  // 52-59    m_reagent
      ReagentCount = new  int[8];             // 60-67    m_reagentCount

      Effect = new int[3];                     // 71-73    m_effect
      EffectDieSides = new int[3];             // 74-76    m_effectDieSides
      EffectRealPointsPerLeve = new float[3];   // 77-79    m_effectRealPointsPerLevel
      EffectBasePoints = new int[3];           // 80-82    m_effectBasePoints (don't must be used in spell/auras explicitly, must be used cached Spell::m_currentBasePoints)
      EffectMechanic = new int[3];             // 83-85    m_effectMechanic
      EffectImplicitTargetA = new int[3];      // 86-88    m_implicitTargetA
      EffectImplicitTargetB = new int[3];      // 89-91    m_implicitTargetB
      EffectRadiusIndex = new int[3];          // 92-94    m_effectRadiusIndex - spellradius.dbc
      EffectApplyAuraName = new int[3];        // 95-97    m_effectAura
      EffectAmplitude = new int[3];            // 98-100   m_effectAuraPeriod
      EffectMultipleValue = new float[3];        // 101-103  m_effectAmplitude
      EffectChainTarget = new int[3];          // 104-106  m_effectChainTargets
      EffectItemType = new int[3];             // 107-109  m_effectItemType
      EffectMiscValue = new int[3];            // 110-112  m_effectMiscValue
      EffectMiscValueB = new int[3];           // 113-115  m_effectMiscValueB
      EffectTriggerSpell = new int[3];         // 116-118  m_effectTriggerSpell
      EffectPointsPerComboPoint = new float[3];  // 119-121  m_effectPointsPerCombo
      EffectSpellClassMaskA = new int[3];                     // 122-124  m_effectSpellClassMaskA, effect 0
      EffectSpellClassMaskB = new int[3];                     // 125-127  m_effectSpellClassMaskB, effect 1
      EffectSpellClassMaskC = new int[3];                     // 128-130  m_effectSpellClassMaskC, effect 2
      SpellVisual = new int[2];                               // 131-132  m_spellVisualID

    }*/
}

[StructLayout(LayoutKind.Sequential)]
public class FactionEntry
{
	public int ID;											// 0
	public int reputationIndex;								// 1		Each faction that has gainable rep has a unique number. All factions that you can not gain rep with have -1.
	public int reputationBase;								// 2		Based on 0 = Neutral
	public int reputationModifier_1;						// 3		
	public int reputationModifier_2;						// 4		
	public int reputationModifier_3;						// 5		
	public int reputationFlag;								// 6
	public LocalizedString Name;							// 7-23		Display name of the faction
}
  
[StructLayout(LayoutKind.Sequential)]
public class CustomFaceEntry
{
	public int ID;
	public string ModelMesh;
	public string Tattos;
}
 
[StructLayout(LayoutKind.Sequential)]
public class CustomHairStyleEntry
{
	public int ID;
	public string ModelMesh;
}

[StructLayout(LayoutKind.Sequential)]
public class CustomFacialHairEntry
{
	public int ID;
	public string ModelMesh;
}

[StructLayout(LayoutKind.Sequential)]
public class CustomHairColorEntry
{
	public int ID;
	public float r, g, b, a;
}

[StructLayout(LayoutKind.Sequential)]
public class CustomSkinColorEntry
{
	public int ID;
	public float r, g, b, a;
}
  
[StructLayout(LayoutKind.Sequential)]
public class WorldMapAreaEntry
{
	public uint ID;						 							// 0        m_ID
	public uint map_id;												// 1        m_mapID
	public uint area_id;											// 2        m_areaID index (continent 0 areas ignored)
	public String internal_name;									// 3        m_areaName
	public float   y1;												// 4        m_locLeft
	public float   y2;												// 5        m_locRight
	public float   x1;                                              // 6        m_locTop
	public float   x2;                                              // 7        m_locBottom
	public int   virtual_map_id;                         		// 8        m_displayMapID -1 (map_id have correct map) other: virtual map where zone show (map_id - where zone in fact internally)
	public int   dungeonMap_id;                           		// 9        m_defaultDungeonFloor (DungeonMap.dbc)
	public uint  someMapID;
}

public class SpellItemEnchantmentEntry
{
	public int ID;							// 0        m_ID
	public int charges;					// 1        m_charges
	public Int3 type;						// 2-4      m_effect[3]
	public Int3 amount;						// 5-7      m_effectPointsMin[3]
	public Int3 amount2;					// 8-10     m_effectPointsMax[3]
	public Int3 spellid;					// 11-13    m_effectArg[3]
	public LocalizedString description;		// 14-29    m_name_lang[16]
//	public uint descriptionFlags;			// 30		string flags
	public int aura_id;					// 31       m_itemVisual
	public int slot;						// 32       m_flags
	public int GemID;						// 33       m_src_itemID
	public int  EnchantmentCondition;		// 34       m_condition_id
	public int requiredSkill;				// 35       m_requiredSkillID
	public int requiredSkillValue;			// 36       m_requiredSkillRank
	public int m_minLevel;
};

public class LockEntry
{
	public int	ID;						// 0        m_ID
	public Int8	Type;					// 1-8      m_Type
	public Int8	Index;					// 9-16     m_Index
	public Int8	Skill;					// 17-24    m_Skill
	public Int8	Action;					// 25-32    m_Action	Not Used
};

public class SkillLineAbilityEntry
{
	public int m_ID;
	public int m_skillLine;
	public int m_spell;
	public int m_supercededBySpell;
	public int m_trivialSkillLineRankHigh;
	public int m_trivialSkillLineRankLow;
};

public class GlyphPropertiesEntry
{
	public int ID;
	public uint spellID;
	public int glyphType;
	public int glyphIconID;
}

public class GemPropertiesEntry
{
	public int	ID;						//	m_id
	public int	spellitemenchantement;	//	m_enchant_id
	public bool	m_maxcount_inv;			//	Maybe Not Used
	public bool	m_maxcount_item;		//	Maybe Not Used
	public int	color;					//	m_type
}


