using UnityEngine;
using System.Collections;

public class GemSlotManager
{
	int firstSlot = (int)EnchantmentSlot.SOCK_ENCHANTMENT_SLOT;
	Item item;

	public GemSlotManager(Item _item)
	{
		item = _item;
	}

	public SpellItemEnchantmentEntry GetGemEnchantmentBySlot(int slot)
	{
		uint enchantmentID = 0;
		if(slot > -1 && slot < 3)
		{
			enchantmentID = item.GetEnchantmentId(firstSlot+slot);
		}
		SpellItemEnchantmentEntry ret = dbs.sSpellItemEnchantment.GetRecord(enchantmentID);
		return ret;
	}

	public void InsertGem(Item[] gems)
	{
		ulong gemGUID = 0;
		ulong[] chekedGemGUIDs = new ulong[3];
		for(int inx = 0; inx < 3; inx++)
		{
			gemGUID = 0;
			if(gems.Length <= inx)
			{
				Item gem = gems[inx];
				if(gem.classType != ItemClass.ITEM_CLASS_GEM || (gem.flags & (int)ItemPrototypeFlags.ITEM_FLAG_UNIQUE_EQUIPPED) == 0)
				{
					Debug.Log ("The wrong item. You can not insert item "+gem.name+" into the slot.");
				}
				else if(gem.guid != null && gem.guid > 0)
				{
					gemGUID = gem.guid;
				}
			}
			chekedGemGUIDs[inx] = gemGUID;
		}
		int newItemsCount = 0;
		foreach(ulong guid in chekedGemGUIDs) //Check if have changes.
		{
			if(guid > 0)
			{
				newItemsCount++;
			}
		}
		if(newItemsCount > 0)
		{
			WorldPacket pkt = new WorldPacket();
			pkt.SetOpcode((ushort)OpCodes.CMSG_SOCKET_GEMS);
			pkt.AppendPackGUID(item.guid);
			foreach(ulong guid in chekedGemGUIDs)
			{
				pkt.AppendPackGUID(guid);
			}
			RealmSocket.outQueue.Add(pkt);
		}
	}
}
