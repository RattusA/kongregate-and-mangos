﻿using UnityEngine;
using System.Collections;

public class GUITouchableTexture
{
	private Texture texture;
	private Rect positionRect;
	private uint spellId;
	private bool enabled;
	private bool doAction;

	private float cooldownRatio;
	private Texture cooldownTexture;

	public GUITouchableTexture()
	{
		texture = null;
		positionRect = new Rect(0, 0, 0, 0);
		spellId = 0;
		enabled = true;
		doAction = false;

		cooldownTexture = Resources.Load<Texture> ("Icons/CooldownIcon");
	}

	// Create new Toucheble Texture.
	public void CreateButton(Rect textureRect, string textureName, uint spellID)
	{
		texture = Resources.Load<Texture> (textureName);
		positionRect = new Rect(textureRect);
		spellId = spellID;
		enabled = true;
		doAction = false;

		cooldownRatio = 0;
	}

	// Set cooldown value.
	public void SetSpellCooldown(float ratio)
	{
		cooldownRatio = ratio;
	}

	// Draw textures on screen.
	public void DrawTouchebleTexture()
	{
		if(enabled)
		{
			bool touchIsOver = false;
#if(UNITY_ANDROID || UNITY_IPHONE)
			foreach(Touch touch in Input.touches)
			{
				if(ContaineTouch(touch))
				{
					switch(touch.phase)
					{
						case TouchPhase.Began:
						case TouchPhase.Moved:
						case TouchPhase.Stationary:
							touchIsOver = true;
							break;
						case TouchPhase.Ended:
							doAction = true;
							break;
					}
				}
			}
#endif

			Rect resizedRect = new Rect(positionRect);
#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
			// check mouse only on PC, Mac and Linux platforms
			if((Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1)) && 
			   positionRect.Contains(Event.current.mousePosition))
			{
				doAction = true;
			}

			if(touchIsOver || ((Input.GetMouseButton(0) || Input.GetMouseButton(1)) && 
			                   positionRect.Contains(Event.current.mousePosition)))
#else
			if(touchIsOver)
#endif
			{
				resizedRect = new Rect((positionRect.x - (positionRect.width * 0.2f)), 
				                       (positionRect.y - (positionRect.height * 0.2f)), 
				                       (positionRect.width * 1.4f), 
				                       (positionRect.height * 1.4f));
			}
			GUI.DrawTexture(resizedRect, texture);
		}
		else
		{
			GUI.DrawTexture(positionRect, texture);
		}
		DrawCoodownTexture();
	}
	
	private bool ContaineTouch(Touch touch)
	{
		bool retValue = false;
		Vector2 touchZone = new Vector2(touch.position.x, (Screen.height - touch.position.y));

		if(positionRect.Contains(touchZone))
		{
			retValue = true;
		}
		return retValue;
	}

	// Draw cooldown texture and block button.
	private void DrawCoodownTexture()
	{
		if(cooldownRatio > 0)
		{
			float cooldownRectHeight = positionRect.height * cooldownRatio;
			Rect cooldownRect = new Rect(positionRect.x, positionRect.y, positionRect.width, cooldownRectHeight);
			
			GUI.DrawTexture(cooldownRect, cooldownTexture);
			enabled = false;
		}
		else
		{
			enabled = true;
		}
	}

	public bool GetAction()
	{
		bool retValue = false;
		if(doAction)
		{

#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR 
			if(!(Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1)))
			{
				doAction = false;
				retValue = true;
			}

#elif UNITY_ANDROID || UNITY_IPHONE
			bool isTouch = false;
			foreach(Touch touch in Input.touches)
			{
				if(ContaineTouch(touch))
				{
					isTouch = true;
					break;
				}
			}

			if(!isTouch)
			{
				doAction = false;
				retValue = true;
			}
#endif
		}
		return retValue;
	}

	// Destry class and unload all resources.
	public void DestroyTexture()
	{
		texture = null;
		if(cooldownTexture)
		{
			cooldownTexture = null;
		}
	}
}
