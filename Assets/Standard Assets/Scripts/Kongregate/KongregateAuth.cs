﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KongregateAuth : MonoBehaviour
{
	private bool isAuthRequeted = false;
    public string mUserID = "2374646";
    public string mGameAuthToken = "0b0767bdc74319565e79b90aae57ddc4a0aca7fb141a9a86f529ef8bbc22b30d";
    [SerializeField]
    InputField userID;
    [SerializeField]
    InputField gameAuthToken;
    [SerializeField]
    private InputField logField;

    void Awake()
    {
        userID.text = mUserID;
        gameAuthToken.text = mGameAuthToken;
    }

    void OnEnable()
	{
		KongregateManager.logCallback += DrawLog;
		KongregateManager.Log("Enabled KongregateAuth");
	}

	void OnDisable()
	{
		KongregateManager.logCallback -= DrawLog;
	}
	
	void Update()
	{
	}

	public void AuthKongregateCallback()
	{
		KongregateManager.Log ("Start CMD_AUTH_KONGREGATE");

		ByteBuffer packet = new ByteBuffer();
		uint userId = uint.Parse(userID.text);
		string token = gameAuthToken.text;
		byte tokenLength = (byte)token.Length;
		ushort packetSize = (ushort)(sizeof(uint) + sizeof(byte) + tokenLength);
		byte aux = (byte)AuthCmd.CMD_AUTH_KONGREGATE;
		packet.Append(aux);
		packet.Append(packetSize);
		packet.Append(userId);
		packet.Append(tokenLength);
		packet.AppendSTR(token);
		AuthorizationSocket.Instance.SendData(packet);
	}

    public void BindKongregateCallback()
    {
        KongregateManager.Log("Start CMD_AUTH_KONGREGATE");

        ByteBuffer packet = new ByteBuffer();
        uint userId = uint.Parse(userID.text);
        string token = gameAuthToken.text;
        byte tokenLength = (byte)token.Length;
        ushort packetSize = (ushort)(sizeof(uint) + sizeof(byte) + tokenLength);
        byte aux = (byte)AuthCmd.CMD_BIND_KONGREGATE;
        packet.Append(aux);
        packet.Append(packetSize);
        packet.Append(userId);
        packet.Append(tokenLength);
        packet.AppendSTR(token);
        AuthorizationSocket.Instance.SendData(packet);
    }

    public void DrawLog(string log)
	{
		if(logField != null)
		{
			logField.text = log;
		}
	}
}
