﻿using UnityEngine;
using System.Collections;

public class KongregateManager : MonoBehaviour
{
	static private string log = "";
	public delegate void LogCallback(string log);
	static public LogCallback logCallback;

	// Public API (Static Members)
	static public bool isConnected
	{
		get { return Instance.jsAPILoaded; }
	}
	
	static bool _isGuest = true;
	static public bool isGuest
	{
		get 
		{
			Instance.CallJSFunction("isGuest()", "SetIsGuest"); 
			return _isGuest; 
		}
	}
	
	static string _username = "Guest";
	static public string username
	{
		get
		{ 
			Instance.CallJSFunction("getUsername()", "SetUsername");
			return _username;
		}
	}
	
	static string _userId = "0";
	static public string userId
	{
		get 
		{ 
			Instance.CallJSFunction("getUserId()", "SetUserId"); 
			return _userId;
		}
	}
	
	static string _items = "";
	static public string[] items
	{
		get
		{
			Instance.CallJSFunction("getUserItems()");
			if (string.IsNullOrEmpty(_items))
				return new string[0];
			else
				return _items.Split(',');
		}
	}

	static string _gameAuthToken = "";
	static public string gameAuthToken
	{
		get 
		{ 
			return _gameAuthToken;
		}
	}

	static public void ShowSignIn()
	{
		Instance.CallJSFunction("showSignInBox()");
	}
	
	static public void SubmitStat(string statistic, int value)
	{
		Instance.CallJSFunction(string.Format("submitStat('{0}', {1})", statistic, value));
	}
	
	static public void PurchaseItem(string item)
	{
		Debug.Log("Attempting purchase of " + item);
		Instance.CallJSFunction(string.Format("purchaseItem('{0}')", item));
	}
	
	static private KongregateManager _instance = null;
	static public KongregateManager Instance
	{
		get
		{
//			if (!_instance)
//			{
//				new GameObject("KongregateManager", typeof(KongregateManager));
//			}
			return _instance;
		}
	}
	
	public bool isBindedAccount = true;
	
	// Instance Members	
	bool jsAPILoaded;
	string jsReturnValue;
	
	void Awake()
	{
		// Only allow one instance of the API bridge
		if (_instance)
		{
			Debug.Log("Destroying GO");
			Destroy(gameObject);
		}
		
		Debug.Log("Awake kongregate");
		_instance = this;
	}
	

	void Start() 
	{
		Debug.Log("Trying to load Kongregate API");
		Log("Trying to load Kongregate API");
		Application.ExternalEval(/*string.Format(*/@"
			// Extern the JS Kongregate API

			function isGuest()
			{
				return kongregate.services.isGuest();
			}

			function getUsername()
			{
				var name = kongregate.services.getUsername();
				return name;
			}

			function getUserId()
			{
				return kongregate.services.getUserId();
			}

			function showSignInBox()
			{
				if(kongregate.services.isGuest())
  					kongregate.services.showSignInBox();
			}
		
			function submitStat(statName, value)
			{
				kongregate.stats.submit(statName, value);
			}

			function getUserItems()
			{
				kongregate.mtx.requestUserItemList(null, onUserItems);
			}

			function onUserItems(result)
			{
				if (result.success)
				{
					var items = '';
					for (var i = 0; i < result.data.length; i++)
					{
						items += result.data[i].identifier;
						if (i < result.data.length - 1)
							items += ',';
					}
					SendUnityMessage('SetUserItems', items);
				}
			}

			function purchaseItem(item)
			{
				kongregate.mtx.purchaseItems([item], onPurchaseResult);
				SendUnityMessage('LogMessage', 'purchase sent....');				
			}

			function onPurchaseResult(result)
			{
				if (result.success)
				{
					SendUnityMessage('LogMessage', 'purchase complete...');
					getUserItems();
				}
			}

			// Utility function to send data back to Unity
			function SendUnityMessage(functionName, message)
			{
				Log('Calling to: ' + functionName);
				var unity = kongregateUnitySupport.getUnityObject();
				unity.SendMessage('KongregateManager', functionName, message);
			}

			function Log(message)
			{
				var unity = kongregateUnitySupport.getUnityObject();
				unity.SendMessage('KongregateManager', 'LogMessage', message);
			}

			if(typeof(kongregateUnitySupport) != 'undefined')
			{
  				kongregateUnitySupport.initAPI('KongregateManager', 'OnKongregateAPILoaded');
  			};"/*, gameObject.name)*/);
		
		//		Application.ExternalCall("SendUnityMessage", "OnLoaded", "Message to myself");
		//		Application.ExternalCall("loadAPI");
		
		DontDestroyOnLoad(gameObject);
	}
	
	public void LogMessage(string message)
	{
		Debug.Log(message);
	}
	
	void OnLoaded(string error)
	{
		if (string.IsNullOrEmpty(error))
		{
			jsAPILoaded = true;
			Debug.Log("Connected");
		}
		else
			Debug.LogError(error);
	}
	
	public void SetIsGuest(object returnValue)
	{
		if (bool.TryParse(returnValue.ToString(), out _isGuest))
		{
			// Request username again if guest
			if (_isGuest)
				_username = "";
		}
	}
	
	void SetUsername(object returnValue)
	{
		_username = returnValue.ToString();
	}
	
	void SetUserId(object returnValue)
	{
		_userId = returnValue.ToString();
	}
	
	void SetUserItems(object returnValue)
	{
		_items = returnValue.ToString();
	}
	
	void CallJSFunction(string functionCall)
	{
		CallJSFunction(functionCall, null);
	}
	
	void CallJSFunction(string functionCall, string callback)
	{
		if (jsAPILoaded)
		{
			if (string.IsNullOrEmpty(callback))
			{
				Application.ExternalEval(functionCall);
			}
			else
			{
				Application.ExternalEval(string.Format(@"
					var value = {0};
					SendUnityMessage('{1}', String(value));
				", functionCall, callback));
			}
		}
	}
	
	void OnKongregateAPILoaded(string userInfo)
	{
		Debug.Log("API Loaded");
		Log("API Loaded");
		jsAPILoaded = true;

		string[] userParams = userInfo.Split('|');
		int userID = int.Parse(userParams[0]);
		string userName = userParams[1];
		_gameAuthToken = userParams[2];
		
		RegisterLoginEventListener();
		if (userID == 0)
		{
			SetIsGuest(true);
			Log ("Show Kongregate SignIn");
			ShowSignIn();
		}
		else
		{
			SetUserId(userID.ToString());
			SetUsername(userName);
			SetIsGuest(false);
		}
	}

	void OnKongregateUserSignedIn(string userInfo)
	{
		Log("OnKongregateUserSignedIn");
		string[] userParams = userInfo.Split('|');
		int userID = int.Parse(userParams[0]);
		string userName = userParams[1];
		_gameAuthToken = userParams[2];
		
		if (userID == 0)
		{
			SetIsGuest(true);
			Log ("Again show Kongregate SignIn");
			ShowSignIn();
		}
		else
		{
			SetUserId(userID.ToString());
			SetUsername(userName);
			SetIsGuest(false);
		}
	}

	void RegisterLoginEventListener()
	{
		Application.ExternalEval
		(
			"kongregate.services.addEventListener('login', function(){" +
			"	var services = kongregate.services;" +
			"	var params=[services.getUserId(), services.getUsername(), services.getGameAuthToken()].join('|');" +
			"	kongregate_api.getUnityObject().SendMessage('"+gameObject.name+"', 'OnKongregateUserSignedIn', params);" + 
			"});"
		);

	}

	static public void Log(string message)
	{
		log = log + (message + "\n");
		if(logCallback != null)
		{
			logCallback(log);
		}
	}
}
