﻿
public class MobRequest : IBundleRequest
{
	public string mUrl;
	public string assetBundleName;
	public AssetBundleManager.OnEndDownloadMobBundle mCallBack;
	public AssetBundleManager.MobCallBackStruct mArguments;
	
	public MobRequest()
	{}
	
	public MobRequest(string url, AssetBundleManager.OnEndDownloadMobBundle callBack,
	                  AssetBundleManager.MobCallBackStruct arguments)
	{
		mUrl = url;
		mCallBack = callBack;
		mArguments = arguments;
	}
	
	public string getUrl()
	{
		return mUrl;
	}
	
	public string getAssetBundleName()
	{
		return assetBundleName;
	}
	
	public void execCallBack()
	{
		mCallBack(mArguments);
	}
}
