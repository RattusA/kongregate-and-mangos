using UnityEngine;
using System.Collections;

public class BundleLoader : MonoBehaviour
{
	void Awake()
	{
		Caching.CleanCache();
	}

	public void LoadMapBundle ( AssetBundleManager.OnEndDownloadMapBundle callBackFunction, string assetPath )
	{
		MapRequest request = new MapRequest(assetPath, callBackFunction, assetPath);
		AssetBundleManager.instance.AddRequest(request as IBundleRequest);
	}

	public void LoadWearBundle (AssetBundleManager.OnEndDownloadWearBundle callBackFunction,
	                            AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		WearMeshRequest request = new WearMeshRequest(wearMeshCallBackStruct.assetPath[0], callBackFunction, wearMeshCallBackStruct);
		AssetBundleManager.instance.AddRequest(request as IBundleRequest);
	}
	
	public void LoadNPCBundle (AssetBundleManager.OnEndDownloadMobBundle callBackFunction,
	                           AssetBundleManager.MobCallBackStruct mobCallBackStruct)
	{
		MobRequest request = new MobRequest(mobCallBackStruct.assetPath, callBackFunction, mobCallBackStruct);
		AssetBundleManager.instance.AddRequest(request as IBundleRequest);
	}
	
	public void LoadSpellEffectBundle (AssetBundleManager.OnEndDownloadSpellEffectBundle callBackFunction,
	                                   AssetBundleManager.SpellEffectCallBackStruct spellEffectCallBackStruct)
	{
		SpellEffectRequest request = new SpellEffectRequest(spellEffectCallBackStruct.assetPath, callBackFunction, spellEffectCallBackStruct);
		AssetBundleManager.instance.AddRequest(request as IBundleRequest);
	}
	
	void OnDestroy ()
	{
		Caching.CleanCache();
	}
}