﻿
public class WearMeshRequest : IBundleRequest
{
	public string mUrl;
	public string assetBundleName;
	public AssetBundleManager.OnEndDownloadWearBundle mCallBack;
	public AssetBundleManager.WearMeshCallBackStruct mArguments;
	
	public WearMeshRequest()
	{}
	
	public WearMeshRequest(string url, AssetBundleManager.OnEndDownloadWearBundle callBack,
	                       AssetBundleManager.WearMeshCallBackStruct arguments)
	{
		mUrl = url;
		mCallBack = callBack;
		mArguments = arguments;
	}
	
	public string getUrl()
	{
		return mUrl;
	}

	public string getAssetBundleName()
	{
		return assetBundleName;
	}

	public void execCallBack()
	{
		mCallBack(mArguments);
	}
}
