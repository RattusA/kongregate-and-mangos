﻿using UnityEngine;
using System.Collections;

public interface IBundleRequest
{
	string getUrl();
	string getAssetBundleName();
	void execCallBack();
}
