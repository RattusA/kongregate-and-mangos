using UnityEngine;
using System.Collections;

public class MapRequest : IBundleRequest
{
	public string mUrl;
	public string assetBundleName;
	public AssetBundleManager.OnEndDownloadMapBundle mCallBack;
	public string mArguments;
	
	public MapRequest()
	{}
	
	public MapRequest(string url, AssetBundleManager.OnEndDownloadMapBundle callBack, string arguments)
	{
		mUrl = url;
		mCallBack = callBack;
		mArguments = arguments;
	}
	
	public string getUrl()
	{
		return mUrl;
	}
	
	public string getAssetBundleName()
	{
		return assetBundleName;
	}
	
	public void execCallBack()
	{
		mCallBack(mArguments);
	}
}
