﻿
public class SpellEffectRequest : IBundleRequest
{
	public string mUrl;
	public string assetBundleName;
	public AssetBundleManager.OnEndDownloadSpellEffectBundle mCallBack;
	public AssetBundleManager.SpellEffectCallBackStruct mArguments;
	
	public SpellEffectRequest()
	{}
	
	public SpellEffectRequest(string url,
	                          AssetBundleManager.OnEndDownloadSpellEffectBundle callBack,
	                          AssetBundleManager.SpellEffectCallBackStruct arguments)
	{
		mUrl = url;
		mCallBack = callBack;
		mArguments = arguments;
	}
	
	public string getUrl()
	{
		return mUrl;
	}
	
	public string getAssetBundleName()
	{
		return assetBundleName;
	}
	
	public void execCallBack()
	{
		mCallBack(mArguments);
	}
}