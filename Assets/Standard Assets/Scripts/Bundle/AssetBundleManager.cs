﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR	
using UnityEditor;
#endif

public class AssetBundleManager : MonoBehaviour
{
	public struct WearMeshCallBackStruct
	{
		public string[] assetPath;
		public EquipmentSlots slot;
		public GameObject mModel;
		public int LoDDetail;
		public Color skinColor;
		public int displayID;
	}
	public delegate void OnEndDownloadWearBundle(WearMeshCallBackStruct wearMeshCallBackStruct);
	
	public struct MobCallBackStruct
	{
		public string assetPath;
		public float scale;
		public ulong guid;
		public GameObject dummy;
	}
	public delegate void OnEndDownloadMobBundle(MobCallBackStruct mobCallBackStruct);
	
	public struct SpellEffectCallBackStruct
	{
		public string assetPath;
		public int effectNameId;
		public BaseObject linkedObject;
		public BaseObject targetObject;
		public Vector3 goalPosition;
		public float speed;
		public float delayTime;
		public Gender gender;
		public Object resultObject;
	}
	public delegate void OnEndDownloadSpellEffectBundle(SpellEffectCallBackStruct spellEffectCallBackStruct);

	public delegate void OnEndDownloadMapBundle(string assetPath);
	
	public static AssetBundleManager instance;
	const string kAssetBundlesPath = "/";
	// A dictionary to hold the AssetBundle references
	public Dictionary<string, Object> dictAssetBundleRefs;
	public Dictionary<string, Object> currentLoadedBundles;
	private Dictionary<string, List<IBundleRequest>> requestTable;
	public static bool isStarted = false;
	private string m_BaseDownloadingURL = "";
	private AssetBundleManifest bundleManifest;
	public static string message = "";

	// The base downloading url which is used to generate the full downloading url with the assetBundle names.
	public string BaseDownloadingURL
	{
		get { return m_BaseDownloadingURL; }
		set { m_BaseDownloadingURL = value; }
	}

	// Use this for initialization.
	IEnumerator Start ()
	{
		instance = this;
		dictAssetBundleRefs = new Dictionary<string, Object>();
		currentLoadedBundles = new Dictionary<string, Object>();
		requestTable = new Dictionary<string, List<IBundleRequest>>();
		yield return StartCoroutine(Initialize() );
	}

	// Initialize the downloading url and AssetBundleManifest object.
	protected IEnumerator Initialize()
	{
		// Don't destroy the game object as we base on it to run the loading script.
		DontDestroyOnLoad(gameObject);
		
		string platformFolderForAssetBundles = 
#if UNITY_EDITOR
			GetPlatformFolderForAssetBundles(EditorUserBuildSettings.activeBuildTarget);
#else
			GetPlatformFolderForAssetBundles(Application.platform);
#endif

		// Set base downloading url.
		string relativePath = GetRelativePath();
		BaseDownloadingURL = WWW.UnEscapeURL(relativePath + kAssetBundlesPath + platformFolderForAssetBundles + "/");

		yield return StartCoroutine(LoadManifest(BaseDownloadingURL + platformFolderForAssetBundles));
	}

	public string GetRelativePath()
	{
		if (Application.isEditor)
			return "file://" + Application.streamingAssetsPath.Replace("\\", "/"); // Use the build output folder directly.
		else if (Application.isWebPlayer)
			return System.IO.Path.GetDirectoryName(Application.absoluteURL).Replace("\\", "/")+ "/StreamingAssets";
		else if (Application.isMobilePlatform || Application.isConsolePlatform)
			return Application.streamingAssetsPath;
		else // For standalone player.
			return "file://" +  Application.streamingAssetsPath;
	}

#if UNITY_EDITOR
	public static string GetPlatformFolderForAssetBundles(BuildTarget target)
	{
		switch(target)
		{
		case BuildTarget.Android:
			return "Android";
		case BuildTarget.iOS:
			return "iOS";
		case BuildTarget.WebPlayer:
		case BuildTarget.WebPlayerStreamed:
			return "WebPlayer";
		case BuildTarget.StandaloneWindows:
		case BuildTarget.StandaloneWindows64:
			return "Windows";
		case BuildTarget.StandaloneOSXIntel:
		case BuildTarget.StandaloneOSXIntel64:
		case BuildTarget.StandaloneOSXUniversal:
			return "OSX";
			// Add more build targets for your own.
			// If you add more targets, don't forget to add the same platforms to GetPlatformFolderForAssetBundles(RuntimePlatform) function.
		default:
			return null;
		}
	}
#endif
	
	public static string GetPlatformFolderForAssetBundles(RuntimePlatform platform)
	{
		switch(platform)
		{
		case RuntimePlatform.Android:
			return "Android";
		case RuntimePlatform.IPhonePlayer:
			return "iOS";
		case RuntimePlatform.WindowsWebPlayer:
		case RuntimePlatform.OSXWebPlayer:
			return "WebPlayer";
		case RuntimePlatform.WindowsPlayer:
			return "Windows";
		case RuntimePlatform.OSXPlayer:
			return "OSX";
			// Add more build platform for your own.
			// If you add more platforms, don't forget to add the same targets to GetPlatformFolderForAssetBundles(BuildTarget) function.
		default:
			return null;
		}
	}

	public void AddRequest(IBundleRequest request)
	{
		string assetBundleName = string.Format ("Resources/{0}_prefab.unity3d",request.getUrl());
		assetBundleName = assetBundleName.ToLower();
		List<IBundleRequest> requestList;
		if(!requestTable.TryGetValue(assetBundleName, out requestList))
		{
			requestList = new List<IBundleRequest>();
		}
		requestList.Add (request);
		requestTable[assetBundleName] = requestList;
	}

	protected void Update()
	{
		if(requestTable != null && requestTable.Count > 0 && !isStarted)
		{
			StartCoroutine(StartLoading());
		}
	}

	protected IEnumerator StartLoading()
	{
		isStarted = true;
		Dictionary<string, List<IBundleRequest>> requestTableCopy = new Dictionary<string, List<IBundleRequest>>(requestTable);
		foreach(var keyValue in requestTableCopy)
		{
			if( bundleManifest != null)
			{
				List<string> stackOfBundleName = new List<string>();
				FillStackOfBundleName(keyValue.Key, ref stackOfBundleName);
				yield return StartCoroutine(DownloadAssetBundle(stackOfBundleName));
				stackOfBundleName.Clear ();
			}
			currentLoadedBundles.Remove(keyValue.Key);
			AssetBundle mainAssetBundle = null;
			if(dictAssetBundleRefs.ContainsKey(keyValue.Key))
			{
				mainAssetBundle = (AssetBundle)dictAssetBundleRefs[keyValue.Key];
				string assetName = mainAssetBundle.GetAllAssetNames()[0];
				AssetBundleRequest request = mainAssetBundle.LoadAssetAsync(assetName);
				yield return request;
				if(request.isDone)
				{
					currentLoadedBundles[keyValue.Key] = request.asset;
				}
			}
			List<IBundleRequest> requestList = new List<IBundleRequest>(requestTable[keyValue.Key]);
			foreach(IBundleRequest request in requestList)
			{
				request.execCallBack();
			}
			foreach(var assetBundle in dictAssetBundleRefs.Values)
			{
				((AssetBundle)assetBundle).Unload(false);
			}
			dictAssetBundleRefs.Clear();
			currentLoadedBundles.Remove(keyValue.Key);
			requestList.Clear();
			requestTable.Remove(keyValue.Key);
		}
		requestTableCopy.Clear ();
		isStarted = false;
	}

	protected void FillStackOfBundleName (string assetBundleName, ref List<string> stackOfBundleName)
	{
		if(!stackOfBundleName.Contains (assetBundleName))
		{
			stackOfBundleName.Insert(0, assetBundleName);
			List<string> dependencies = LoadDependencies(assetBundleName);
			if(dependencies.Count > 0)
			{
				foreach(string dependencieName in dependencies)
				{
					FillStackOfBundleName(dependencieName, ref stackOfBundleName);
				}
			}
		}
	}

	// Download an AssetBundle
	protected IEnumerator DownloadAssetBundle (List<string> stackOfBundleName)
	{
		foreach(string assetBundleName in stackOfBundleName)
		{
			bool containsKey = dictAssetBundleRefs.ContainsKey(assetBundleName);
			if(!containsKey || (containsKey && dictAssetBundleRefs[assetBundleName] == null))
			{
				string url = BaseDownloadingURL + assetBundleName;
				
				// For manifest assetbundle, always download it as we don't have hash for it.
				using(WWW download = WWW.LoadFromCacheOrDownload(url, bundleManifest.GetAssetBundleHash(assetBundleName), 0))
				{
					while(!download.isDone)
					{
						yield return download;
					}
					if (!string.IsNullOrEmpty(download.error))
					{
#if UNITY_EDITOR
						Debug.Log("Bundle loading has Error: url = " + url + "\n" + download.error);
#endif
					}
					else
					{
//						string assetName = download.assetBundle.GetAllAssetNames()[0];
//						AssetBundleRequest request = download.assetBundle.LoadAssetAsync(assetName);
//						yield return request;
//						if(request.isDone)
//						{
//							dictAssetBundleRefs[assetBundleName] = request.asset;
//						}
//						else
//						{
//							message = "Asset loading has Error: url = " + url;
//						}
//						download.assetBundle.Unload(false);
						dictAssetBundleRefs[assetBundleName] = download.assetBundle;
						download.Dispose();
//						request = null;
					}
				}
			}
		}
	}
	
	protected List<string> LoadDependencies(string assetBundleName)
	{
		List<string> dependencies = new List<string>();
		if (bundleManifest == null)
		{
			Debug.LogError("Please initialize AssetBundleManifest by calling Initialize()");
		}
		else
		{
			// Get dependecies from the AssetBundleManifest object..
			dependencies = new List<string>(bundleManifest.GetAllDependencies(assetBundleName));
		}
		return dependencies;
	}

	protected IEnumerator LoadManifest(string path)
	{
		using(WWW www = new WWW(path))
		{
			yield return www;
			if(!string.IsNullOrEmpty(www.error))
			{
				Debug.Log(www.error);
				yield break;
			}
			
			bundleManifest = (AssetBundleManifest)www.assetBundle.LoadAsset("AssetBundleManifest");
			yield return null;
			www.assetBundle.Unload(false);
		}
	}
}
