using UnityEngine;
using System.Collections;

public class PortraitCam : MonoBehaviour {
	private Transform target;
	private Transform updateTarget;
	private float multiplier;
	
	private GameObject duplicateTargetClone;
	private Transform obj;
	private byte timer;
	
	private Rect DrawingRect;
	
	//true if is Self Portrait
	private bool isSeflPortrait = false;
	
	
	//correction list
	public string[] Names;
	public float[] Corrections;
		
	
	
	
#region Setters + Getters
	
	public bool IsSelfPortrait{
		set { isSeflPortrait = value; }
		get { return isSeflPortrait; }
	}
	
	public Transform Target{
		set { target = value; }
		get { return target; }
	}
	
#endregion

	
#region Methods
	
	public void SetLayer(int layer){
		foreach(SkinnedMeshRenderer go in target.GetComponentsInChildren(typeof(SkinnedMeshRenderer))){
			go.gameObject.layer = layer;
			go.updateWhenOffscreen = true;
		}
	}
	
	private Transform FindHead(Transform root){
		Transform temp = null;
		
		foreach(Transform go in root.GetComponentsInChildren(typeof(Transform))){
			if(temp){
				continue;
			}
			
			if(go.name == "Bip01 Head"){
				temp = go;
			}
		}
		return temp;
	}
	
	public void MakeSnapShot(){
		duplicateTargetClone = (GameObject)Instantiate(target.gameObject, new Vector3(0, -1000, 0), Quaternion.identity);
		SetLayer(31);
		duplicateTargetClone.GetComponent<Rigidbody>().isKinematic = true;
		obj = duplicateTargetClone.transform.Find("obj");
		Debug.Log("Play animation - idle");
		obj.GetComponent<Animation>().Play("idle");
		
		print("DOLPHINS!!! WHALES!!!");
	}
	
#endregion

	
	void Start () {
		if(IsSelfPortrait || !IsSelfPortrait){
			DrawingRect = new Rect(Screen.width/140, Screen.height/140, Screen.width/12, Screen.width/12);
			
			
			
			
			//duplicateTargetClone.GetComponentInChildren(
		}
		//}
		//else{
		//	DrawingRect = new Rect(Screen.width * 0.45f, Screen.height/140, Screen.width/12, Screen.width/12);
		//}
		
		
		
		
		
		
		timer = 0;
	}
	
	void Update(){
		
		//transform.LookAt(updateTarget.position);
		if(timer < 5){
			timer ++;
		}
		
		
		print(target.name);
		
		
		if(timer > 2){
			obj.GetComponent<Animation>().Stop();
		}
		if(timer > 3){
			Destroy(duplicateTargetClone, 20);
		}
	}
}
