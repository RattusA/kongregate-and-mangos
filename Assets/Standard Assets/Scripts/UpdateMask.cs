﻿
public class UpdateMask
{
	private int mCount = 0;
	private int mBlocks = 0;
	private uint[] mUpdateMask = null;// la ei era un array de uint32

	public UpdateMask()
	{}

	public byte GetBit ( uint index  )
	{
//		byte shift1 = 3;
		byte shift2 = 1;
		byte si = 0x7;
		//MonoBehaviour.print("citire bit de pe pozitia " + index + "  " +(index>>shift1) +">="+mCount);
		if((index>>3) >= mCount)
		{
			return 0;
		}
		byte bit = (byte)((mUpdateMask[index>>3] & (shift2 << (byte)(index & si)) ));
		
		//MonoBehaviour.print(bit);
		return bit;//((mUpdateMask[index>>shift1]& (shift2<<index & si) ));
	}
	void  UnsetBit ( uint index )
	{
		// mUpdateMask[ index >> 3 ] &= (0xff ^ (1 <<  ( index & 0x7 ) ) );
	}
	void  SetBit ( uint index )
	{
		byte shift1 = 3;
		byte shift2 = 1;
		byte si = 0x7;
		mUpdateMask[ index >> shift1 ] |= (uint)(shift2 << (byte)( index & si ));
	}
	int GetBlockCount ()
	{
		return mBlocks;
	}
	uint GetLength ()
	{
		byte shift = 2;
		return (uint)(mBlocks << shift);
	}
	int GetCount ()
	{
		return mCount;
	}
	uint[] GetMask ()
	{
		return mUpdateMask;
	}
	public void  SetMask ( uint[] updateMask )
	{ 
		mUpdateMask = updateMask; 
	}
	public void  SetCount ( int valuesCount )
	{
		mCount = valuesCount;
		mBlocks = (valuesCount + 31) / 32;
		mUpdateMask = new uint[mBlocks];
		for(int i = 0; i < mBlocks; i++)
		{
			mUpdateMask[i] = 0;
		}
	}
	
	void  Clear ()
	{
		if (mUpdateMask != null)
		{
			for(int i = 0;i<mCount;i++)
			{
				mUpdateMask[i]=0;
			}
		}
	}
}