using UnityEngine;
using System.Collections.Generic;

public class InventoryManagerOLD
{
	public int curBag = 0;
	public int lastBag = 0;
	MainPlayer mainPlayer;
	UIText textPrototype;
	UIText textFontinyDark;
	UIText textFontinySimple;
	public UIAbsoluteLayout inventoryContainer;
	public UIAbsoluteLayout inventoryBagContainer;
	UIButton[] bagButton;
	UIButton[] bagSlot;
	UIButton[] equipSlot;
	UIButton statsBackGround;
	UIButton inventoryBagBackGround;
	UIButton inventoryRightSmallBackGround;
	UIButton inventoryRightBackGround;
	UIButton inventoryLeftBackGround;
	UIButton characterBackGround;
	UIButton xpProgressBar;
	UIButton xpIcon;
	UIButton xpProgress;
	UIButton switchStatsMod; 
	UIButton spellIconDrag = null;
	UIButton ammoSlot = null;
	UIButton unequipButton;
	UIButton destroyButton;
	UIButton applyButton;
	UIButton feedButton;
	public int countSlotsInBag;
	int bagsCount;
	string slotImage;
	Item itemToDestroy = null;
	public int inventoryStatsMode;
	
	public EnchantingItems enchantingItems;
	public PetFeeding petFeeding;
	
	UITextInstance destroyItemQuestion;
	UITextInstance destroyOneText;
	UITextInstance destroyAllText;
	UITextInstance cancelDestroyText;
	UISprite destroyItemPopup; 
	UISprite decorationTexture6;
	UISprite decorationTexture7;
	UIButton destroyOneButton;
	UIButton destroyAllButton;
	UIButton cancelDestroyButton;
	UIAbsoluteLayout destroyItemContainer;
	
	private UITextInstance xpText;
	private UITextInstance statsText;
	
	float doubleClick;
	bool  clickStart;
	int bagClick;
	int slotClick;
	
	public InventoryInfoWindow itemInfoWindow;
	float touchStartTime;
	
	Item[] itemInCurrentBag;
	Item[] equipedItem;
	uint equipedAmmoEntry;
	private ScrollZone TextZone = new ScrollZone();

	public InventoryManagerOLD ()
	{
		bagSlot = new UIButton[16];
		itemInCurrentBag = new Item[16];
		equipSlot = new UIButton[19];
		equipedItem = new Item[19];
		bagButton = new UIButton[5];
	}
	
	public void  Init ()
	{
		mainPlayer = WorldSession.player;
		inventoryStatsMode = 0;
		
		bagsCount = 1 + mainPlayer.itemManager.BaseBags.Count;
		countSlotsInBag = 16;
		doubleClick=0;
		clickStart =false;
		bagClick = -1;
		slotClick = -1;
		
		InitUI();
		itemInfoWindow = new InventoryInfoWindow(this);
	}
	
	public void  RefreshInventory ()
	{
		ButtonOLD.isNeedRefreshInventory = false;
		WorldSession.RequestMithrilCoinsCount();
		
		ClearItemButtonsInInventory();
		switchInventoryInfo();
		UIToolkit tempUIToolkit;
		for(byte i = 0; i < 16; i++)
		{
			string imageName = null;
			Item item = null;
			if(i < countSlotsInBag)
			{
				if (curBag == 0)
				{
					Item itemInBag;
					mainPlayer.itemManager.BaseBagsSlots.TryGetValue(i, out itemInBag);
					if(itemInBag != null && itemInBag.entry > 0)
					{
						item  = mainPlayer.itemManager.BaseBagsSlots[i];
					}
				}
				else
				{
					if((mainPlayer.itemManager.BaseBags[curBag]).GetItemFromSlot(i) != null)
					{
						item = (mainPlayer.itemManager.BaseBags[curBag]).GetItemFromSlot(i);						 	
					}
				}
			}
			if( (((itemInCurrentBag[i] == null) && (item != null) && (item.entry > 0)) || (bagSlot[i] == null))
			   || ((itemInCurrentBag[i] != null) && (item != null) && (itemInCurrentBag[i].guid != item.guid))
			   || ((itemInCurrentBag[i] != null) && ((item == null) || (item.entry <= 0)))
			   || (i >= countSlotsInBag))
			{
				if(bagSlot != null && bagSlot[i] != null)
				{
					tempUIToolkit = bagSlot[i].manager;
					inventoryBagContainer.removeChild(bagSlot[i], false);
					bagSlot[i].destroy();
					bagSlot[i] = null;			
					if(tempUIToolkit)
					{
						GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
					}
				}
				if(item != null && item.entry > 0)
				{
					imageName = DefaultIcons.GetItemIcon(item);
					itemInCurrentBag[i] = item;
				}
				else
				{
					itemInCurrentBag[i] = null;
				}
				//AICI se seteaza pozitia inventarului cu adevarat
				UIToolkit bagSlotUIToolkit;
				if (string.IsNullOrEmpty (imageName))
				{
					bagSlotUIToolkit = SinglePictureUtils.CreateUIToolkit ("Icons/tab_solo.png");
					bagSlot [i] = UIButton.create (bagSlotUIToolkit, "tab_solo.png", "tab_solo.png", 0, 0);
					bagSlot [i].color = new Color (0.0f, 0.0f, 0.0f, 0.0f);
				}
				else
				{
					bagSlotUIToolkit = SinglePictureUtils.CreateUIToolkit ("Icons/" + imageName);
					bagSlot [i] = UIButton.create (bagSlotUIToolkit, imageName, imageName, 0, 0);
				}
				bagSlot[i].info=i;
				bagSlot[i].setSize(	inventoryRightSmallBackGround.width/4.35f,
				                   inventoryRightSmallBackGround.height/4.35f);
				bagSlot[i].position = new Vector3(	((Screen.width*0.6f)+((i%4)*bagSlot[i].width))+(i%2), 
				                              ((((-Screen.height/9.3f)*1.7f)-((Screen.width/12)*0.8f))-((i/4)*bagSlot[i].height))+(i/2),
				                              0);
				bagSlot[i].onTouchDown += itemTouchDownDelegate;
				bagSlot[i].onTouchUpInside += itemTouchUpDelegate;
				
				inventoryBagContainer.addChild(bagSlot[i]);
			}
		}	
	}
	
	public void  RefreshEquip ()
	{
		ButtonOLD.SetRefreshEquip(false);
		ClearEquipedItemButtonsInInventory();
		
		Item item;
		UIToolkit equipSlotUIToolkit;
		for(byte i = 0; i < 19;i++)
		{
			string imageName = null;
			mainPlayer.itemManager.EquippedSlots.TryGetValue(i, out item);
			if( (((equipedItem[i] == null) && (item != null) && (item.entry > 0)) || (equipSlot[i] == null)) || 
			   ((equipedItem[i] != null) && (item != null) && (equipedItem[i].guid != item.guid)))
			{
				if(equipSlot != null && equipSlot[i] != null)
				{
					UIToolkit tempUIToolkit = equipSlot[i].manager;
					inventoryContainer.removeChild(equipSlot[i], false);
					equipSlot[i].destroy();
					equipSlot[i] = null;			
					if(tempUIToolkit != null)
					{
						GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
					}
				}
				if(item != null && item.entry > 0)
				{
					imageName = DefaultIcons.GetItemIcon(item);
					equipedItem[i] = item;
				}
				else
				{
					equipedItem[i] = null;
				}
				if (string.IsNullOrEmpty (imageName))
				{
					equipSlotUIToolkit = SinglePictureUtils.CreateUIToolkit ("Icons/tab_solo.png");
					equipSlot [i] = UIButton.create (equipSlotUIToolkit, "tab_solo.png", "tab_solo.png", 0, 0);
					//equipSlot[i].color = Color(0.5f,0.5f,0.5f,1);
				}
				else
				{
					equipSlotUIToolkit = SinglePictureUtils.CreateUIToolkit ("Icons/" + imageName);
					equipSlot [i] = UIButton.create (equipSlotUIToolkit, imageName, imageName, 0, 0);
				}
				//equipSlotSizePosition
				equipSlot[i].setSize(inventoryLeftBackGround.width/9, inventoryLeftBackGround.height/10.2f);
				if(i < 10)
				{
					equipSlot[i].position = new Vector3(inventoryLeftBackGround.position.x+inventoryLeftBackGround.width/14.21f,
					                                inventoryLeftBackGround.position.y-inventoryLeftBackGround.height/140.4f-i*equipSlot[i].height,
					                                0);
				}
				else
				{
					equipSlot[i].position = new Vector3(inventoryLeftBackGround.position.x+inventoryLeftBackGround.width/1.22f,
					                                inventoryLeftBackGround.position.y-inventoryLeftBackGround.height/140.4f-(i-9)*equipSlot[i].height,
					                                0);
				}		
				equipSlot[i].info = i;
				equipSlot[i].onTouchDown += equipSelect;
				equipSlot[i].onTouchUpInside += equipTouchUpDelegate;
				
				inventoryContainer.addChild(equipSlot[i]);
			}
		}
		CreateAmmoButton();
	}
	
	public void  RefreshBags ()
	{
		UIToolkit bagButtonUIToolkit;
		int mainBagIndex = 0;
		
		float bagButtonWidth = inventoryBagBackGround.width / 5;
		float bagButtonHeight = inventoryBagBackGround.height;
		
		ButtonOLD.isNeedRefreshBags = false;
		ButtonOLD.isNeedRefreshInventory = true;
		ClearBagButtonsInInventory();
		
		bagsCount = 1 + mainPlayer.itemManager.BaseBags.Count;
		
		Debug.Log("Update Main Bag");
		bagButtonUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/Default_bag.png");
		bagButton[mainBagIndex] = UIButton.create(bagButtonUIToolkit, "Default_bag.png", "Default_bag.png", 0, 0);
		bagButton[mainBagIndex].setSize(bagButtonWidth, bagButtonHeight);
		bagButton[mainBagIndex].position = new Vector3(Screen.width * 0.55f + mainBagIndex * bagButtonWidth, -Screen.height * 0.12f, 0);
		bagButton[mainBagIndex].onTouchUpInside += bagEvent;
		bagButton[mainBagIndex].info = mainBagIndex;
		inventoryBagContainer.addChild(bagButton[mainBagIndex]);
		
		foreach(KeyValuePair<int, Bag> bag in mainPlayer.itemManager.BaseBags)
		{
			ItemDisplayInfoEntry BagIcon = dbs.sItemDisplayInfo.GetRecord(bag.Value.displayInfoID);
			string bagicon = BagIcon.InventoryIcon;
			
			if (string.IsNullOrEmpty (bagicon))
			{	
				Debug.Log ("No Icon" + bag.Key);
				bagButtonUIToolkit = SinglePictureUtils.CreateUIToolkit ("Icons/Default_bag.png");
				bagButton [bag.Key] = UIButton.create (bagButtonUIToolkit, "Default_bag.png", "Default_bag.png", 0, 0);
			}
			else
			{
				Debug.Log ("Load Icon" + bag.Key);
				bagButtonUIToolkit = SinglePictureUtils.CreateUIToolkit ("Icons/" + bagicon);
				bagButton [bag.Key] = UIButton.create (bagButtonUIToolkit, bagicon, bagicon, 0, 0);
			}
			
			bagButton[bag.Key].setSize(bagButtonWidth, bagButtonHeight);
			bagButton[bag.Key].position = new Vector3(Screen.width * 0.55f + bag.Key * bagButtonWidth, -Screen.height * 0.12f, 0);
			bagButton[bag.Key].onTouchUpInside += bagEvent;
			bagButton[bag.Key].info = bag.Key;
			inventoryBagContainer.addChild(bagButton[bag.Key]);
		}
		
		for(uint index = 1; index < 5; index++)
		{
			if(bagButton[index] == null)
			{
				bagButtonUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/tab_solo.png");
				bagButton[index] = UIButton.create(bagButtonUIToolkit, "tab_solo.png", "tab_solo.png", 0, 0);
				bagButton[index].setSize(bagButtonWidth, bagButtonHeight);
				bagButton[index].position = new Vector3(Screen.width * 0.55f + index * bagButtonWidth, -Screen.height * 0.12f, 0);
				inventoryBagContainer.addChild(bagButton[index]);
			}
		}
	}
	
	void  ClearItemButtonsInInventory ()
	{
		for(int i = 0; i < 16; i++)
		{
			if(bagSlot != null && bagSlot[i] != null)
			{
				UIToolkit tempUIToolkit = bagSlot[i].manager;
				inventoryBagContainer.removeChild(bagSlot[i], false);
				bagSlot[i].destroy();
				bagSlot[i] = null;			
				if(tempUIToolkit != null)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
				itemInCurrentBag[i] = null;
			}
		}
	}
	
	void  ClearBagButtonsInInventory ()
	{
		for(byte i = 0; i < 5; i++)
		{
			if(bagButton != null && bagButton[i] != null)
			{
				UIToolkit tempUIToolkit = bagButton[i].manager;
				inventoryBagContainer.removeChild(bagButton[i], false);
				bagButton[i].destroy();
				bagButton[i] = null;
				if(tempUIToolkit != null)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
		}
	}
	
	void  ClearEquipedItemButtonsInInventory ()
	{
		UIToolkit tempUIToolkit;
		for(byte i = 0; i < 19;i++)
		{
			if(equipSlot != null && equipSlot[i] != null)
			{
				tempUIToolkit = equipSlot[i].manager;
				inventoryContainer.removeChild(equipSlot[i], false);
				equipSlot[i].destroy();
				equipSlot[i] = null;
				if(tempUIToolkit != null)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
				equipedItem[i] = null;
			}
		}
		if(ammoSlot != null)
		{
			tempUIToolkit = ammoSlot.manager;
			inventoryContainer.removeChild(ammoSlot,false);
			ammoSlot.destroy();
			ammoSlot = null;
			if(tempUIToolkit != null)
			{
				GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
			}
		}
	}
	
	//--------------------------------Anton Strul
	//--------------------------------Create ammo button
	void  CreateAmmoButton ()
	{
		uint ammoEntry = mainPlayer.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_AMMO_ID);
		if(equipedAmmoEntry != ammoEntry)
		{
			if(ammoSlot != null)
			{
				UIToolkit tempUIToolkit = ammoSlot.manager;
				inventoryContainer.removeChild(ammoSlot, false);
				ammoSlot.destroy();
				ammoSlot = null;			
				if(tempUIToolkit)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
			equipedAmmoEntry = ammoEntry;
			if(GetItemCount(ammoEntry) == 0)
			{
				ammoEntry = 0;
				Item.SendEquipAmmo(0);
			}
			string ammoImage = null;
			if(ammoEntry == 0)
			{
				ammoImage = "tab_solo.png";
			}
			else
			{
				ammoImage = DefaultIcons.GetItemIcon(ammoEntry);
			}
			UIToolkit ammoSlotUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+ammoImage);
			ammoSlot = UIButton.create( ammoSlotUIToolkit, ammoImage, ammoImage, 0, 0);
			ammoSlot.setSize(inventoryLeftBackGround.width/9, inventoryLeftBackGround.height/10.2f);
			ammoSlot.position = new Vector3((inventoryLeftBackGround.position.x+(inventoryLeftBackGround.width/1.22f)),
			                            (inventoryLeftBackGround.position.y-(inventoryLeftBackGround.height/140.4f)),
			                            0);
			//ammoSlot.info=9.5f;
			ammoSlot.onTouchDown += AmmoSelect;
			inventoryContainer.addChild(ammoSlot);
		}
	}
	
	void  AmmoSelect ( UIButton sender  )
	{
		uint ammoEntry = mainPlayer.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_AMMO_ID);
		uint ammoCount = GetItemCount(ammoEntry);
		mainPlayer.itemSelected = null;
		if(AppCache.sItems.GetItemEntry(ammoEntry) != null && ammoEntry != 0 && ammoCount > 0)
		{
			Item ammoItem = AppCache.sItems.GetItem(ammoEntry);
			ammoItem.SetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT, ammoCount);
			ammoItem.stats = ammoItem.GetItemBasicStats();
			mainPlayer.showCharacterStats = false;
			mainPlayer.stateItem = true;
			mainPlayer.itemSelected = ammoItem;
			TextZone.SetNewText(mainPlayer.getInventoryStatsInfo());
		}
		else
		{
			mainPlayer.showCharacterStats = true;
			TextZone.SetNewText(mainPlayer.getInventoryStatsInfo());
		}
	}
	
	uint GetItemCount ( uint entry  )
	{
		uint counter = 0;
		for(int i= 0; i < mainPlayer.itemManager.Items.Count; i++)
		{
			if (mainPlayer.itemManager.Items[i].entry == entry)
			{
				counter += mainPlayer.itemManager.Items[i].GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT);
			}
		}
		return counter;
	}
	//--------------------------------------
	
	//============ for the DESTROY item dialog
	public void  DestroyOne (UIButton sender)
	{
		mainPlayer.itemManager.DestroyItem(itemToDestroy, 1);
		mainPlayer.itemSelected = null;
		destroyItemContainer.hidden = true; //"dismisses" destroy dialog 
		destroyItemQuestion.hidden = true;
		destroyOneText.hidden = true;
		destroyAllText.hidden = true;
		cancelDestroyText.hidden = true;
		itemToDestroy = null;
		inventoryContainer.disable(false);
		inventoryBagContainer.disable(false);
		mainPlayer.interf.DisableMenuButtons(false);
		//mainPlayer.InspectMgr.itemContainer.disable(false);
	}	
	
	public void  DestroyAll (UIButton sender)
	{
		mainPlayer.itemManager.DestroyItem(itemToDestroy, 0);
		mainPlayer.itemSelected = null;
		destroyItemContainer.hidden = true; //"dismisses" destroy dialog 
		destroyItemQuestion.hidden = true;
		destroyOneText.hidden = true;
		destroyAllText.hidden = true;
		cancelDestroyText.hidden = true;
		itemToDestroy = null;
		inventoryContainer.disable(false);
		inventoryBagContainer.disable(false);
		mainPlayer.interf.DisableMenuButtons(false);
		//mainPlayer.InspectMgr.itemContainer.disable(false);
	}	
	
	public void  CancelDestroy (UIButton sender)
	{
		destroyItemContainer.hidden = true; //"dismisses" destroy dialog 
		destroyItemQuestion.hidden = true;
		destroyOneText.hidden = true;
		destroyAllText.hidden = true;
		cancelDestroyText.hidden = true;
		itemToDestroy = null; // clears destroy
		inventoryContainer.disable(false);
		inventoryBagContainer.disable(false);
		mainPlayer.interf.DisableMenuButtons(false);
		//mainPlayer.InspectMgr.itemContainer.disable(false);
	}
	//============
	
	public void  bagEvent ( UIButton sender  )
	{
		mainPlayer.itemSelected = null;
		mainPlayer.showCharacterStats = true;
		if(sender.info != 0)
		{
			mainPlayer.bagSelected = mainPlayer.itemManager.BaseBags[sender.info];
			countSlotsInBag = mainPlayer.itemManager.BaseBags[sender.info].GetCapacity();
			mainPlayer.bagSelected.PopulateSlots(mainPlayer.itemManager);
		}
		else
		{
			Bag bag = new Bag();
			bag.slot = 255;
			mainPlayer.bagSelected = bag;
			countSlotsInBag = 16;
		}
		lastBag = curBag;
		curBag = sender.info;
		ButtonOLD.isNeedRefreshInventory = true;
	}
	
	public void  InitUI ()
	{
		textPrototype = new UIText( UIPrime31.myToolkit1, "prototype", "prototype.png" );
		textPrototype.alignMode = UITextAlignMode.Center;
		textFontinyDark = new UIText (UIPrime31.myToolkit1, "fontiny dark","fontiny dark.png");
		textFontinySimple = new UIText (UIPrime31.myToolkit1, "fontiny simple","fontiny simple.png");
		
		inventoryContainer = new UIAbsoluteLayout();
		inventoryBagContainer = new UIAbsoluteLayout();
		mainPlayer.interf.setInventoryState(false);
		
		//character background  
		characterBackGround = UIButton.create( UIPrime31.myToolkit3, "character_background.png", "character_background.png", 0, 0);
		characterBackGround.setSize(Screen.width/2-Screen.width/12*2.2f, 4*Screen.height/6);
		characterBackGround.position = new Vector3(Screen.width/12*1.1f, -Screen.height*0.11f,4);
		
		xpProgressBar = UIButton.create (UIPrime31.myToolkit3, "slider.png", "slider.png", 0, 0);
		xpProgressBar.setSize(characterBackGround.width*0.9f, Screen.height*0.1f);
		xpProgressBar.position = new Vector3(Screen.width/12*1.25f, -Screen.height*0.65f, 3);
		
		xpText = textPrototype.addTextInstance("", xpProgressBar.position.x+xpProgressBar.width/2, -xpProgressBar.position.y+xpProgressBar.height/1.9f, 1f, -1, Color.blue, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		xpText.hidden=true;
		
		xpIcon = UIButton.create (UIPrime31.myToolkit3, "xp.png", "xp.png", 0, 0);
		xpIcon.setSize(Screen.width*0.1f, Screen.height*0.1f);
		xpIcon.position = new Vector3(xpProgressBar.position.x, xpProgressBar.position.y+xpIcon.height, 0); 
		
		xpProgress = UIButton.create (UIPrime31.myToolkit3, "progress_bar.png", "progress_bar.png", 0, 0);
		xpProgress.position = new Vector3(xpProgressBar.position.x+xpProgressBar.width*0.055f, xpProgressBar.position.y-xpProgressBar.height/9, 0);
		xpProgressUpdate();
		
		//character sheet background
		inventoryLeftBackGround = UIButton.create( UIPrime31.myToolkit3, "main_tab.png", "main_tab.png", 0, 0);
		inventoryLeftBackGround.setSize(Screen.width/2, Screen.height*0.89f);
		inventoryLeftBackGround.position = new Vector3(0, -Screen.height/10*1.1f, 5);
		
		//stats background
		statsBackGround = UIButton.create (UIPrime31.myToolkit3, "stats_background.png", "stats_background.png", 0, 0);
		statsBackGround.setSize(characterBackGround.width, (inventoryLeftBackGround.height-characterBackGround.height)*0.96f);
		statsBackGround.position = new Vector3(characterBackGround.position.x, -(characterBackGround.position.y+characterBackGround.height+statsBackGround.height), 4);
		
		//background for full inventory (do not leave the layer to 1, it's only for debuging)
		inventoryRightBackGround = UIButton.create( UIPrime31.myToolkit3, "inventory_slots.png", "inventory_slots.png", 0, 0);
		inventoryRightBackGround.setSize(Screen.width*0.39f,Screen.height*0.55f);
		inventoryRightBackGround.position = new Vector3(Screen.width*0.57f, -Screen.height/10*1.4f-Screen.width/12*0.8f, 5);
		
		// background for scaling of inventory icons, Position you set elsewhere
		inventoryRightSmallBackGround = UIButton.create( UIPrime31.myToolkit3, "tab_solo.png", "tab_solo.png", 0, 0);
		inventoryRightSmallBackGround.color = new Color(0.5f, 0.5f, 0.5f, 0);
		inventoryRightSmallBackGround.setSize(inventoryRightBackGround.width*0.92f, inventoryRightBackGround.height*0.92f);
		
		//background for bag slots
		inventoryBagBackGround = UIButton.create( UIPrime31.myToolkit3, "tab_solo.png", "tab_solo.png", 0, 0);
		inventoryBagBackGround.color = new Color(0.5f, 0.5f, 0.5f, 0);
		inventoryBagBackGround.setSize(Screen.width*0.43f, Screen.width/12*0.8f);
		inventoryBagBackGround.position = new Vector3(Screen.width*0.55f, -Screen.height/10*1.2f, 5);
		
		statsText = textPrototype.addTextInstance("Stats", characterBackGround.position.x+characterBackGround.width/2, Screen.height*0.8f, 0.8f*dbs._sizeMod, -1, Color.red, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		statsText.hidden = true;
		
		RefreshBags();
		RefreshInventory();
		RefreshEquip();
		
		switchStatsMod = UIButton.create( UIPrime31.myToolkit3, "switch_button.png", "switch_button.png", 0, 0);
		//switchStatsMod.color = Color(0.5f,0.5f,0.5f,1.0f);
		switchStatsMod.setSize(0.87f*Screen.height*0.13f, 0.87f*Screen.height/10);
		switchStatsMod.position = new Vector3(Screen.width/2-Screen.width/48 -2.5f*0.87f*Screen.height/10*1.15f, -Screen.height/10*1.1f-Screen.height*0.01f, -1);
		switchStatsMod.onTouchUpInside += switchInventoryStatsMode;
		
		//inventoryRightBackGround
		
		unequipButton = UIButton.create (UIPrime31.myToolkit3, "shield2.png", "shield2.png", 0, 0);
		unequipButton.setSize(Screen.width*0.1f, Screen.height*0.1f);
		unequipButton.position = new Vector3((bagSlot[15].position.x),
		                                 (inventoryRightBackGround.position.y-(inventoryRightBackGround.height*0.97f)),
		                                 0);
		unequipButton.onTouchDown += UnequipButtonDelegate;
		
		destroyButton = UIButton.create (UIPrime31.myToolkit3, "shield1.png", "shield1.png", 0, 0);
		destroyButton.setSize(Screen.width*0.1f, Screen.height*0.1f);
		destroyButton.position = new Vector3((bagSlot[13].position.x-destroyButton.width),
		                                 (inventoryRightBackGround.position.y-(inventoryRightBackGround.height*0.97f)),
		                                 0);
		destroyButton.onTouchDown += DestroyButtonDelegate;
		
		applyButton = UIButton.create (UIPrime31.myToolkit3, "Apply_button.png", "Apply_button.png", 0, 0);
		applyButton.setSize(Screen.width*0.18f, Screen.height*0.1f);
		applyButton.position = new Vector3((bagSlot[14].position.x-(applyButton.width*0.5f)),
		                               (inventoryRightBackGround.position.y-(inventoryRightBackGround.height*0.97f)),
		                               0);
		applyButton.onTouchDown += UseItemForEnchanteWeapon;
		
		if(mainPlayer.playerClass == Classes.CLASS_HUNTER)
		{
			feedButton = UIButton.create (UIPrime31.myToolkit3, "Feed_button.png", "Feed_button.png", 0, 0);
			feedButton.setSize(Screen.width*0.18f, Screen.height*0.1f);
			feedButton.position = new Vector3((bagSlot[14].position.x-(feedButton.width*0.5f)),
			                              ((inventoryRightBackGround.position.y-(inventoryRightBackGround.height*0.97f))-(Screen.height*0.085f)),
			                              1);
			feedButton.onTouchDown += FeedRangerPet;
		}
		
		
		//		if(BuildConfig.hasFreeMithrilButton)
		//		{
		//			tapFreeMithril = UIButton.create (UIPrime31.myToolkit3, "tap_free_mithril.png","tap_free_mithril.png",0,0);
		//			//tapFreeMithril.color=Color (0,0,0,0); //comment this row when the button functionality is made
		//			tapFreeMithril.setSize(Screen.width*0.45f,Screen.width*0.05f);
		//			tapFreeMithril.position = Vector3 (Screen.width/2+Screen.width*0.02f, -Screen.height*0.98f+tapFreeMithril.height,0);
		//			tapFreeMithril.onTouchDown += FreeMithrilDelegate;
		//			tapFreeMithril.hidden=true;
		//			inventoryBagContainer.addChild(tapFreeMithril);
		//		}
		inventoryContainer.addChild(characterBackGround, inventoryLeftBackGround, switchStatsMod, statsBackGround, xpProgressBar, xpProgress, xpIcon);
		inventoryBagContainer.addChild(inventoryRightBackGround, unequipButton, destroyButton, applyButton, inventoryRightSmallBackGround, inventoryBagBackGround);
		
		if(mainPlayer.playerClass == Classes.CLASS_HUNTER)
		{
			inventoryBagContainer.addChild(feedButton);
		}
		mainPlayer.interf.setInventoryState(false);
		inventoryContainer.hidden = !ButtonOLD.inventoryState;
		inventoryBagContainer.hidden = !ButtonOLD.inventoryState;
		
		enchantingItems = new EnchantingItems(mainPlayer);
		petFeeding = new PetFeeding(mainPlayer);
		
		destroyItemContainer = new UIAbsoluteLayout();
		destroyItemContainer.removeAllChild(true);
		
		destroyItemPopup = UIPrime31.myToolkit4.addSprite( "popup_background.png", Screen.width/3, Screen.height/4 , -3 );
		destroyItemPopup.setSize(Screen.width/1.7f , Screen.height/3);
		//destroyItemPopup.hidden = true;
		
		decorationTexture6 = UIPrime31.myToolkit3.addSprite( "decoration_02.png", destroyItemPopup.position.x + destroyItemPopup.width/30 , -destroyItemPopup.position.y + destroyItemPopup.height*0.2f , -4 );
		decorationTexture6.setSize(destroyItemPopup.height * 0.4f ,destroyItemPopup.height * 0.4f );
		//decorationTexture6.hidden = true;
		
		decorationTexture7 = UIPrime31.myToolkit3.addSprite( "decoration_01.png", decorationTexture6.position.x + decorationTexture6.width*0.8f , -decorationTexture6.position.y + decorationTexture6.height*0.8f , -4 );
		decorationTexture7.setSize(destroyItemPopup.width*0.8f ,destroyItemPopup.height * 0.1f );
		//decorationTexture7.hidden = true;
		
		destroyOneButton = UIButton.create( UIPrime31.myToolkit3 , "3_selected.png", "3_selected.png", destroyItemPopup.position.x + destroyItemPopup.width*0.12f , -destroyItemPopup.position.y + destroyItemPopup.height*0.65f , -4 );
		destroyOneButton.setSize( destroyItemPopup.width/4 , destroyItemPopup.height/5); 
		destroyOneButton.centerize();
		destroyOneButton.onTouchDown += DestroyOne;
		//yesDestroyButton.hidden = true;
		
		destroyAllButton = UIButton.create( UIPrime31.myToolkit3 , "3_selected.png", "3_selected.png", destroyItemPopup.position.x + destroyItemPopup.width*0.39f , -destroyItemPopup.position.y + destroyItemPopup.height*0.65f , -4 );
		destroyAllButton.setSize( destroyItemPopup.width/4 , destroyItemPopup.height/5); 
		destroyAllButton.centerize();
		destroyAllButton.onTouchDown += DestroyAll;
		//yesDestroyButton.hidden = true;
		
		cancelDestroyButton = UIButton.create( UIPrime31.myToolkit3 , "3_selected.png", "3_selected.png", destroyItemPopup.position.x + destroyItemPopup.width*0.66f , -destroyItemPopup.position.y + destroyItemPopup.height*0.65f , -4 );
		cancelDestroyButton.setSize( destroyItemPopup.width/4 , destroyItemPopup.height/5); 
		cancelDestroyButton.centerize();
		cancelDestroyButton.onTouchDown += CancelDestroy;
		//noDestroyButton.hidden = true;
		
		destroyOneText = textFontinyDark.addTextInstance("One", destroyOneButton.position.x , -destroyOneButton.position.y , 0.4f , -6 , Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);	// the destroy item dialog box
		destroyOneText.hidden = true;
		
		destroyAllText = textFontinyDark.addTextInstance("All", destroyAllButton.position.x , -destroyAllButton.position.y , 0.4f , -6 , Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);	// the destroy item dialog box
		destroyAllText.hidden = true;
		
		cancelDestroyText = textFontinyDark.addTextInstance("Cancel", cancelDestroyButton.position.x , -cancelDestroyButton.position.y , 0.4f ,-6 ,Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);	// the destroy item dialog box
		cancelDestroyText.hidden = true;
		
		destroyItemQuestion = textFontinySimple.addTextInstance("Do you want to destroy item(s)?", decorationTexture6.position.x + decorationTexture6.width*1.2f , -decorationTexture6.position.y + decorationTexture6.height/6 , 0.5f ,-6,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Top);	// the destroy item dialog box
		destroyItemQuestion.hidden = true;
		
		destroyItemContainer.addChild(destroyItemPopup, decorationTexture6, decorationTexture7, destroyOneButton, destroyAllButton, cancelDestroyButton); 
		destroyItemContainer.hidden = true;
	}
	
	public void  UnequipButtonDelegate ( UIButton sender  )
	{
		//Debug.Log("Unequip button pushed");
		
		if(mainPlayer.bagSelected != null && mainPlayer.bagSelected.slot != 255)
		{
			if(mainPlayer.bagSelected.IsEmpty())
			{
				if(mainPlayer.itemManager.MoveBagToFirstEmptySlot(mainPlayer.bagSelected as Item))
				{
					mainPlayer.bagSelected=null;
					//	 				mainPlayer.interf.isNeedRefreshBags=true;
				}
				else
				{
					mainPlayer.bagSelected=null;
					//	 				mainPlayer.inventoryMessage="No free space in inventory!";
					InterfaceManager.Instance.SetInventoryMessage("No free space in inventory!");
				}
			}
			else
			{
				//	 			mainPlayer.inventoryMessage="Bag must be empty!";
				InterfaceManager.Instance.SetInventoryMessage("Bag must be empty!");
			}
			
		}
		
		if(mainPlayer.itemSelected != null)
		{
			if(!mainPlayer.itemSelected.IsEquiped())
			{	//Debug.Log("equiping");
				mainPlayer.itemSelected.EquipItem();
				if(mainPlayer.itemSelected.IsEquiped())
					mainPlayer.stateItem = true;
			}
			else if(mainPlayer.itemSelected.IsEquiped())
			{
				if(mainPlayer.itemSelected.classType == ItemClass.ITEM_CLASS_PROJECTILE)
				{
					switch(mainPlayer.itemSelected.subClass)
					{
					case (int)ItemSubclassProjectile.ITEM_SUBCLASS_BOLT:
					case (int)ItemSubclassProjectile.ITEM_SUBCLASS_ARROW:
					case (int)ItemSubclassProjectile.ITEM_SUBCLASS_BULLET:
						Item.SendEquipAmmo(0);
						break;
					default:
						break;
					}
				}
				if(mainPlayer.itemManager.MoveToFirstEmptySlot(mainPlayer.itemSelected))
				{ //Debug.Log("unequiping");
					mainPlayer.stateItem = false;
					ButtonOLD.isNeedRefreshInventory = true;
				}
			}
		}
	}
	
	public void  DestroyButtonDelegate (  UIButton sender  )
	{
		//Debug.Log("Destroy button pushed");
		
		itemToDestroy = mainPlayer.itemSelected; // saves item, part of the destroy dialog functionality implemented in Button.js
		if(itemToDestroy == null)
		{
			return;
		}		
		
		destroyItemContainer.hidden = false; // makes use of the destroy dialog, showing it 
		destroyItemQuestion.hidden = false;
		destroyOneText.hidden = false;
		
		if(itemToDestroy.count > 1)
		{
			destroyAllText.hidden = false;
		}
		else
		{	
			destroyItemContainer._children[4].hidden = true;
		}
		
		cancelDestroyText.hidden = false;
		inventoryContainer.disable(true);
		inventoryBagContainer.disable(true);
		mainPlayer.interf.DisableMenuButtons(true);
		//mainPlayer.InspectMgr.itemContainer.disable(true);
	}
	
	public void  UseItemForEnchanteWeapon (UIButton sender)
	{
		enchantingItems.Init();
	}
	
	public void  FeedRangerPet (UIButton sender)
	{
		mainPlayer.interf.resetInventoryStatsMode(0);
		switchInventoryStatsMode(null);
		inventoryContainer.hidden = true;
		
		petFeeding.Init();
	}
	
	public void  switchInventoryInfo ()
	{
		TextZone.RemoveScrollZone();
		if(ButtonOLD.inventoryState)
		{
			Vector3 tmp = Vector3.zero;
			switch(inventoryStatsMode)
			{
			case 0:
				mainPlayer.zoom = false;
				mainPlayer.inventoryCameraReff.gameObject.SetActive(true);
				mainPlayer.UpdateCurrentCharacterRect();
				
				characterBackGround.setSize(Screen.width * 0.31667f, Screen.height * 0.66667f);
				characterBackGround.position = new Vector3(Screen.width * 0.09167f, -Screen.height * 0.11f, 4);
				statsBackGround.setSize(characterBackGround.width, (inventoryLeftBackGround.height - characterBackGround.height) * 0.96f);
				statsBackGround.position = new Vector3(characterBackGround.position.x, -(characterBackGround.position.y + characterBackGround.height + statsBackGround.height));
				
				statsText.hidden = false;
				xpText.hidden = false;
				xpProgressBar.hidden = false;
				xpProgress.hidden = false;
				xpIcon.hidden = false;
				tmp = statsText.position;
				tmp.y = Screen.height * 0.8f;
				statsText.position = tmp;
				xpProgressUpdate();
				xpText.text=(mainPlayer.currentXp + "/" + mainPlayer.maxXp);
				
				Rect smallStatRect = new Rect(Screen.width/12*1.5f,4.9f*Screen.height/6,Screen.width/2-Screen.width/12*3.0f,1.0f*Screen.height/6);
				TextZone.SetFontSizeByRatio(1.0f);
				TextZone.InitSkinInfo(mainPlayer.newskin.GetStyle("DescriptionAndStats"));
				TextZone.CreateScrollZoneWithText(smallStatRect, mainPlayer.getInventoryStatsInfo(), mainPlayer.mainTextColor);
				break;
				
			case 1:
				mainPlayer.zoom = true;
				mainPlayer.inventoryCameraReff.gameObject.SetActive(true);
				mainPlayer.UpdateCurrentCharacterRect();
				
				characterBackGround.setSize(Screen.width * 0.31667f, (inventoryLeftBackGround.height-Screen.height * 0.66667f) * 0.96f + Screen.height * 0.66667f);
				characterBackGround.position = new Vector3(Screen.width * 0.09167f, -Screen.height * 0.11f, 4);				
				statsBackGround.setSize(0,0);
				
				statsText.hidden = true;
				xpText.hidden = true;
				xpProgressBar.hidden = true;
				xpProgress.hidden = true;
				xpIcon.hidden = true;
				break;
				
			case 2: 
				mainPlayer.zoom = true;
				mainPlayer.inventoryCameraReff.gameObject.SetActive(false);
				mainPlayer.UpdateCurrentCharacterRect();
				
				characterBackGround.setSize(0, 0);
				statsBackGround.setSize(Screen.width * 0.31667f, (inventoryLeftBackGround.height-Screen.height * 0.66667f) * 0.96f + Screen.height * 0.66667f);
				statsBackGround.position = new Vector3(Screen.width * 0.09167f, -Screen.height * 0.11f, 4);
				
				statsText.hidden = false;
				tmp = statsText.position;
				tmp.y = Screen.height * 1.45f;
				statsText.position = tmp;
				xpText.hidden = true;
				xpProgressBar.hidden = true;
				xpProgress.hidden = true;
				xpIcon.hidden = true;
				
				Rect bigStatRect = new Rect(statsBackGround.position.x*1.1f,-statsBackGround.position.y*1.9f,statsBackGround.width*0.9f,statsBackGround.height*0.8f);
				TextZone.SetFontSizeByRatio(1.0f);
				TextZone.InitSkinInfo(mainPlayer.newskin.GetStyle("DescriptionAndStats"));
				TextZone.CreateScrollZoneWithText(bigStatRect, mainPlayer.getInventoryStatsInfo(), mainPlayer.mainTextColor);
				break;
				
			default: 
				Debug.Log("zoomEvent delegate problem");
				break;
			}
		}
	}
	
	void  xpProgressUpdate ()
	{
		if(mainPlayer.currentXp > 0)
		{
			xpProgress.setSize(xpProgressBar.width*0.88f*mainPlayer.currentXp/mainPlayer.maxXp ,xpProgressBar.height*0.72f);
		}
		else
		{
			xpProgress.setSize(0,0);
		}
	}
	
	public void  HideClassStats ()
	{
		TextZone.RemoveScrollZone();
		statsText.hidden=true;
		xpText.hidden=true;
	}
	
	public void  switchInventoryStatsMode ( UIButton sender  )
	{
		inventoryStatsMode++;
		if(inventoryStatsMode > 2) 
		{
			inventoryStatsMode = 0;
		}
		ButtonOLD.isNeedRefreshInventory = true;
	}
	
	public void  itemTouchDownDelegate ( UIButton sender  )
	{
		if(itemToDestroy != null) // this way we don't allow selecting beneath the destroy dialog, though icons still bump
		{
			return;
		}
		mainPlayer.bagSelected=null;
		if(!clickStart || (bagClick!=curBag) || (slotClick!=sender.info))
		{
			clickStart = true;
			doubleClick = 0;
			bagClick = curBag;
			slotClick = sender.info;
		}
		else if(mainPlayer.itemSelected != null)
		{
			mainPlayer.itemSelected.EquipItem();
		}
		
		mainPlayer.itemSelected=null;
		if(curBag==0)
		{
			if(mainPlayer.itemManager.BaseBagsSlots.ContainsKey(sender.info) &&
			   mainPlayer.itemManager.BaseBagsSlots[sender.info]!=null && 
			   mainPlayer.itemManager.BaseBagsSlots[sender.info].entry!=0)
			{
				mainPlayer.stateItem=false;
				mainPlayer.showCharacterStats=false;
				mainPlayer.itemSelected=mainPlayer.itemManager.BaseBagsSlots[sender.info];
				TextZone.SetNewText(mainPlayer.getInventoryStatsInfo());
				clickStart=true;
			}
			else
			{
				mainPlayer.showCharacterStats=true;
				TextZone.SetNewText(mainPlayer.getInventoryStatsInfo());
			}
		}
		else if((mainPlayer.itemManager.BaseBags[curBag] as Bag).GetItemFromSlot(sender.info) != null && (mainPlayer.itemManager.BaseBags[curBag] as Bag).GetItemFromSlot(sender.info).entry!=0 )
		{
			mainPlayer.stateItem = false;
			mainPlayer.itemSelected = ((mainPlayer.itemManager.BaseBags[curBag]) as Bag).GetItemFromSlot(sender.info);
			mainPlayer.showCharacterStats = false;
			TextZone.SetNewText(mainPlayer.getInventoryStatsInfo());
			clickStart=true;
		}
		else
		{
			mainPlayer.showCharacterStats=true;
			TextZone.SetNewText(mainPlayer.getInventoryStatsInfo());
		}
		
		touchStartTime = Time.time;
	}
	
	public void  itemTouchUpDelegate ( UIButton sender  )
	{
		if((Time.time - touchStartTime) < 2)
		{
			if(!mainPlayer.showCharacterStats)
			{
				itemInfoWindow.itemDelegate(sender, ItemSource.INVENTORY_SLOT);
			}
		}
	}
	
	public void  equipSelect ( UIButton sender  ){
		mainPlayer.itemSelected=null;
		if(mainPlayer.slots[sender.info]!=null && mainPlayer.slots[sender.info].entry!=0 )
		{
			mainPlayer.showCharacterStats=false;
			mainPlayer.stateItem=true;
			mainPlayer.itemSelected=mainPlayer.slots[sender.info];
			TextZone.SetNewText(mainPlayer.getInventoryStatsInfo());
		}
		else
		{
			mainPlayer.showCharacterStats=true;
			TextZone.SetNewText(mainPlayer.getInventoryStatsInfo());
		}
		
		touchStartTime = Time.time;
	}
	
	public void  equipTouchUpDelegate ( UIButton sender  )
	{
		if((Time.time - touchStartTime) < 2)
		{
			if(!mainPlayer.showCharacterStats)
			{
				itemInfoWindow.itemDelegate(sender, ItemSource.EQUIP_SLOT);
			}
		}
	}
	
	public void  UpdateItemToDestroy ()
	{
		if (itemToDestroy != null)
		{ // in which case destroy dialog would still be open
			destroyItemContainer.hidden = true; //"dismisses" destroy dialog 
			destroyItemQuestion.hidden = true;
			destroyOneText.hidden = true;
			destroyAllText.hidden = true;
			cancelDestroyText.hidden = true;
			itemToDestroy = null; // clears destroy
		}
	}
	
	public void  UpdateClickStatus ()
	{
		if(clickStart)
		{
			doubleClick+=Time.deltaTime;
			if(doubleClick>0.3f)
			{
				clickStart=false;
				doubleClick=0.0f;
			}
		}
	}
	
	void  ClearItemInfoWindow ()
	{
		if(itemInfoWindow != null && itemInfoWindow.isEnable)
		{
			itemInfoWindow.CloseItemDelegate(null);
		}
	}
	
	public void  ClearInventory ()
	{
		ClearItemInfoWindow();
		ClearItemButtonsInInventory();
		ClearBagButtonsInInventory();
		ClearEquipedItemButtonsInInventory();
	}
}