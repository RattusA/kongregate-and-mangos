﻿using UnityEngine;
using System.Collections;

public class SpellVisual : MonoBehaviour
{
//*********************************************
// SpellVisual.dbc
// Visuals used at each part of a spell cast, the missile of the spell, etc.
//*********************************************
//1 	m_ID				uinteger 	
//2 	m_precastKit			uinteger 	The visual effect used for the casting   
//3 	m_castKit			uinteger 	The visual effect used for the cast where the spell occurs 
//4 	m_impactKit			uinteger 	The visual effect used for the target 
//5 	m_stateKit			uinteger 	The visual effect that can be seen while this buff/debuff remains on the target
//6 	m_stateDoneKit			uinteger 	
//7 	m_channelKit			uinteger 	The visual effect used while channeling a spell 
//8 	m_hasMissile 		 	Boolean
//9 	m_missileModel			uinteger	The visual effect used for the spell missile 
//10 	m_missilePathType   		uinteger 
//11 	m_missileDestinationAttachment	uinteger 	
//12 	m_missileSound			uinteger 	
//13 	m_animEventSoundID		uinteger 	
//14 	m_flags 			integer 	The visual effect used at the center of an AOE spell probably used for other things as well 
//15 	m_casterImpactKit 		uinteger 	
//16 	m_targetImpactKit 		uinteger 	Previous documentation had swapped this with m_flags.
//17 	m_missileAttachment 		uinteger
//18 	m_missileFollowGroundHeight	uinteger 	
//19 	m_missileFollowGroundDropSpeed	uinteger 	
//20 	m_missileFollowGroundApproach	uinteger 	
//21 	m_missileFollowGroundFlags	uinteger 	
//22 	m_missileMotion 		uinteger 	
//23	m_missileTargetingKit 		uinteger 	
//24	m_instantAreaKit 		uinteger 	
//25	m_impactAreaKit 		uinteger 		
//26	m_persistentAreaKit 		uinteger 	The visual effect for AOE spells
//27	m_missileCastOffset 		float[3] 
//30	m_missileImpactOffset 		float[3] 
//**********************************************

	public int[] effect = new int[3];
	public int persistentAreaKit;
	
	void Awake ()
	{
		
	}
	
	void Update ()
	{
		
	}
}
