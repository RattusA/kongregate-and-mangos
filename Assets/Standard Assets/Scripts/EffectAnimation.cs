using UnityEngine;

public class EffectAnimation : MonoBehaviour 
{
	public float AnimationSpeed = 0;
	public string SoundName = "";
	public string MaleSoundName = "";
	public string FemaleSoundName = "";
	public AudioClip audioSoundName;
	public AudioClip audioMaleSoundName;
	public AudioClip audioFemaleSoundName;
	public GameObject Projectile;
	
	public bool LoopAnimation = false;
	public float ProjectileSpeed = 0;
	private bool CastOnce = true;
	
	private Transform target;
	
	private GameObject _instantiatedProjectile;
	private float _speed = 0;
	private Transform _startPosition;
	private Projectile _projectileScript;
	private AudioSource _source;
	private float timer = 1;
	const float FrequencyAnimation = 0.5f;
	
	public void SetTarget(Transform projectileTarget) 
	{
		target = projectileTarget;
	}
	
	public void SetSpeed(float projectileSpeed) 
	{
		ProjectileSpeed = projectileSpeed;
	}
	
	public void SetStartPosition(Transform position)
	{
		_startPosition = position;
	}
	
	public void StartPlayingAnimation()
	{
		//setting the speed of the animation right before playing it
		if(gameObject.GetComponent<Animation>() != null)
		{
			foreach(AnimationState _state in this.GetComponent<Animation>())
			{
				if(_speed != 0)
				{
					_state.speed = _speed;
				}
				else
				{
					_state.speed = AnimationSpeed;
				}
			}
			gameObject.GetComponent<Animation>().Play();
		}
		else
		{
			print("no Animation Component...");
		}
	}
	
	void Awake()
	{
		if(AnimationSpeed == 0)
		{
			AnimationSpeed = 1;
		}
		AudioClip defaultAudioClip = Resources.Load<AudioClip> ("Sounds/SoundFX/default");
		if(SoundName == "")
		{
			SoundName = "default";
		}
		if(MaleSoundName == "")
		{
			MaleSoundName = SoundName;
		}
		if(FemaleSoundName == "")
		{
			FemaleSoundName = SoundName;
		}
		if(audioSoundName == null)
		{
			audioSoundName = defaultAudioClip;
		}
		if(audioMaleSoundName == null)
		{
			audioMaleSoundName = audioSoundName;
		}
		if(audioFemaleSoundName == null)
		{
			audioFemaleSoundName = audioSoundName;
		}

		if(LoopAnimation == true)
		{
			CastOnce = false;
		}
	}
	
	private void CreateProjectile(Transform caster, Transform projectileTarget)
	{
		try{
			Vector3 _positionToInstantiate = caster.GetComponent<Collider>().bounds.center + caster.forward * caster.GetComponent<Collider>().bounds.size.x/2;
			_instantiatedProjectile = Object.Instantiate (Projectile, _positionToInstantiate, Quaternion.identity) as GameObject;
			_projectileScript = _instantiatedProjectile.GetComponent<Projectile>();
			_projectileScript.Target = projectileTarget;
			_projectileScript.ProjectileSpeed = ProjectileSpeed;
			_projectileScript.EnableRenderer(false);
		}
		catch(System.Exception exception)
		{
			Debug.LogError("Bug with creating projectile");
			DestroyEffect();
		}
	}
	
	void LaunchSound(Gender type)
	{
		if(MusicManager.EffectsEnabled)
		{
			_source = gameObject.AddComponent<AudioSource> ();

			switch (type)
			{
			case Gender.GENDER_MALE:
				_source.clip = audioMaleSoundName;
				break;
			case Gender.GENDER_FEMALE:
				_source.clip = audioFemaleSoundName;
				break;
			case Gender.GENDER_NONE:
				_source.clip = audioSoundName;
				break;
			}
			_source.volume = MusicManager.EffectVolume;
			_source.Play();
		}
	}
	
	void Update()
	{
		if(LoopAnimation == false && CastOnce == true)
		{
			CastOnce = false;
			if(Projectile != null)
			{
				CreateProjectile(_startPosition, target);
			}
		}
		
		if(LoopAnimation == true)
		{
			timer += Time.deltaTime;
			
			if(timer >= FrequencyAnimation)
			{
				timer = 0;
				CreateProjectile(target, _startPosition);
			}
		}
		
//		CheckMusicEffect();
	}
	
	private void CheckMusicEffect()
	{
		if(MusicManager.EffectsEnabled)
		{
			if(!_source.isPlaying)
			{
				_source.clip = null;
			}
			if(gameObject.GetComponent<Animation>() != null)
			{
				if(_source)
				{
					if(!gameObject.GetComponent<Animation>().isPlaying && !_source.isPlaying)
					{
						DestroyEffect();
					}
				}
				else
				{
					if(!gameObject.GetComponent<Animation>().isPlaying)
					{
						DestroyEffect();
					}
				}
			}
			else
			{
				if(_source)
				{
					if(!_source.isPlaying)
					{
						DestroyEffect();
					}
				}
			}
		}
		else
		{
			if(gameObject.GetComponent<Animation>() != null)
			{
				if(!gameObject.GetComponent<Animation>().isPlaying)
				{
					DestroyEffect();
				}				
			}
			else
			{
				DestroyEffect();
			}
		}
	}
	
	void DestroyEffect()
	{
		GameObject.Destroy(gameObject);
	}
}
