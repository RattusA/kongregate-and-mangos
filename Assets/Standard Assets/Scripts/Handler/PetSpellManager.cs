﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ActionBarIndex
{
	ACTION_BAR_INDEX_START 				= 0,
	ACTION_BAR_INDEX_PET_SPELL_START 	= 3,
	ACTION_BAR_INDEX_PET_SPELL_END 		= 7,
	ACTION_BAR_INDEX_END 				= 10,
};

public enum PetActionFlag
{
	
	ACTION_PET_COMMAND					= 0x06,
	ACTION_PET_REACT					= 0x07,
	ACTION_PET_ACTIVE_SPELL				= 0x81,
	ACTION_PET_PASSIVE_SPELL			= 0x01,
};

public class PetActionSpell
{
	uint actionId;
	bool isActive;
	string iconName;
	byte flag;
	
	public PetActionSpell(uint newActionId, byte newFlag, string newIconName)
	{
		actionId = newActionId;
		isActive = false;
		iconName = newIconName;
		flag = newFlag;
	}
}

public class PetSpellManager {

	Player _player;
	
	Dictionary<uint, Spell> _petSpellBook = new Dictionary<uint, Spell>();
	List<uint> _petSpellBookKey = new List<uint>();
	
	Dictionary<uint, PetActionSpell> _petCommandBook = new Dictionary<uint, PetActionSpell>();
	Dictionary<uint, PetActionSpell> _petReactBook = new Dictionary<uint, PetActionSpell>();
	
	/**
	 *	Read packet with pet spells.
	 **/

	public PetSpellManager(Player plr)
	{
		_player = plr;
	}
	
	public void HandlePetSpellsOpcode(WorldPacket pkt)
	{
		_petSpellBook.Clear();
		_petSpellBookKey.Clear();
		
		_petCommandBook.Clear();
		_petReactBook.Clear();
		
		ulong guid = pkt.ReadReversed64bit();		// Get pet Guid
		
		if((_player.petGuid == guid || guid == 0) && _player.pet != null && pkt.Size() == 8)
		{
			_player.petGuid = 0;
			_player.pet = null;
			_player.BroadcastMessage("UpdateActionBarSlots");
			return;
		}
		
		_player.petGuid = guid;			
		_player.petFamily = (int)pkt.ReadReversed16bit();		// Pet family
		pkt.ReadReversed32bit();								// Default 0 value
		
		byte petReactState = pkt.Read();		// Get pet react state
		byte petCommandState = pkt.Read();	// Get pet command state
		pkt.ReadReversed16bit();								// Default 0 value
		
		_player.BroadcastMessage("ClearPetActionButtons");
		BuildActionPanel(pkt);									// Get spells on action bar
		
		byte spellCount = pkt.Read();		// Get total count of spells
		
		
		for(byte count = 0; count < spellCount; count++)
		{
			uint spellInfo = (uint)pkt.ReadReversed32bit();
			uint spellID = spellInfo & 0x00FFFFFF;
			byte flag = (byte)(spellInfo >> 24);
			
			if(flag == (byte)PetActionFlag.ACTION_PET_PASSIVE_SPELL)
			{
				continue;
			}

			AddPetSpell(spellID);
		}
		
		_player.BroadcastMessage("UpdateActionBarSlots");
		
		byte cooldownCount = pkt.Read();
		
		for(byte index = 0; index < cooldownCount; index++)
		{
			uint spellId = (uint)pkt.ReadReversed32bit();
			uint spellCathegory = (uint)pkt.ReadReversed16bit();
			uint spellCooldownn = (uint)pkt.ReadReversed32bit();
			uint spellCathegoryCooldown = (uint)pkt.ReadReversed32bit();
			
			SetPetCooldown(spellId, spellCathegory, spellCooldownn, spellCathegoryCooldown);
		}
	}
	
	void SetPetCooldown(uint spellId, uint spellCathegory, uint recoveryTime, uint cathegoryRecoveryTime)
	{
		Cooldown cooldown = new Cooldown();
		Spell spell = SpellManager.CreateOrRetrive(spellId);
		
		if(cathegoryRecoveryTime > 0)
		{
			if(cathegoryRecoveryTime < spell.CategoryRecoveryTime)
			{
				cooldown = new Cooldown(cathegoryRecoveryTime / 1000.0f, spell.CategoryRecoveryTime / 1000.0f);
			}
			else if(cathegoryRecoveryTime < spell.RecoveryTime)
			{
				cooldown = new Cooldown(cathegoryRecoveryTime / 1000.0f, spell.RecoveryTime / 1000.0f);
			}
		}
		else if(recoveryTime > 0)
		{
			cooldown = new Cooldown(recoveryTime / 1000.0f, spell.RecoveryTime / 1000.0f);
		}
		else
		{
			return;
		}
		
		_player.cooldownManager.petCooldownList.SetCooldown(spell, cooldown);
	}
	
	void BuildActionPanel(WorldPacket pkt)
	{
		for(int index = 0; index < (int)ActionBarIndex.ACTION_BAR_INDEX_END; index++)
		{
			uint spellInfo = (uint)pkt.ReadReversed32bit();
			uint spellID = spellInfo & 0x00FFFFFF;
			byte flag = (byte)(spellInfo >> 24);
			
			switch(flag)
			{
			case (byte)PetActionFlag.ACTION_PET_COMMAND:
				AddCommandSpell(spellID);
				break;
			case (byte)PetActionFlag.ACTION_PET_REACT:
				AddReactSpell(spellID);
				break;
			default:
				int newIndex = index - 3;			// ActionBarIndex.ACTION_BAR_INDEX_PET_SPELL_START
				List<uint> parametrs = new List<uint>();
				parametrs.Add((uint)newIndex);
				parametrs.Add(spellID);
				_player.BroadcastMessage("SetPetSpellToActionBar", parametrs);
				break;
			}
		}
	}
	
	void AddCommandSpell(uint spellInfo)
	{
		PetActionSpell newCommandSpell;
		switch(spellInfo)
		{
		case 0:
			newCommandSpell = new PetActionSpell(spellInfo, (byte)PetActionFlag.ACTION_PET_COMMAND, "Halt.png");
			break;
		case 1:
			newCommandSpell = new PetActionSpell(spellInfo, (byte)PetActionFlag.ACTION_PET_COMMAND, "Hostile.png");
			break;
		default:
			newCommandSpell = new PetActionSpell(spellInfo, (byte)PetActionFlag.ACTION_PET_COMMAND, "Inactive.png");
			break;
		}
		_petCommandBook[spellInfo] = newCommandSpell;
	}
	
	void AddReactSpell(uint spellInfo)
	{
		PetActionSpell newCommandSpell;
		switch(spellInfo)
		{
		case 0:
			newCommandSpell = new PetActionSpell(spellInfo, (byte)PetActionFlag.ACTION_PET_REACT, "Assault.png");
			break;
		case 1:
			newCommandSpell = new PetActionSpell(spellInfo, (byte)PetActionFlag.ACTION_PET_REACT, "Escort.png");
			break;
		default:
			newCommandSpell = new PetActionSpell(spellInfo, (byte)PetActionFlag.ACTION_PET_REACT, "Follow.png");
			break;
		}
		_petReactBook[spellInfo] = newCommandSpell;
	}

	void AddPetSpell(uint spellInfo)
	{
		Spell spell = SpellManager.CreateOrRetrive(spellInfo);
		_petSpellBook[spellInfo] = spell;
		_petSpellBookKey.Add(spellInfo);
	}
	
	
	/**
	 *	Working with Dictionary
	 **/
	
	public bool IsPetSpell(uint spellID)
	{
		return _petSpellBook.ContainsKey(spellID);
	}
	
	public int GetPetSpellCount()
	{
		return _petSpellBook.Count;
	}
	
	public Spell GetPetSpellByID(uint spellID)
	{
		Spell ret;
		_petSpellBook.TryGetValue(spellID, out ret);
		
		return ret;
	}
	
	public Spell GetPetSpellByIndex(int index)
	{
		uint key = _petSpellBookKey[index];
		return GetPetSpellByID(key);
	}
	
	public Dictionary<uint, Spell> GetSpellList()
	{
		return _petSpellBook;
	}
	
	public void DoPetCommand(uint spellid, uint flag, ulong petTargetGuid)
	{
		flag = (flag << 24);
		
		uint data = spellid | flag;
		
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_PET_ACTION); //, 8+4+8
		pkt.Add(_player.petGuid);
		pkt.Add(data);
		pkt.Add(petTargetGuid);
		
		RealmSocket.outQueue.Add(pkt);
	}
}