﻿public class GameObjectTemplate
{
	public uint entry;
	public GameobjectTypes type;
	public uint displayID;
	public string name;
	public string iconName;
	public string castBarCaption;
	public string unk1;
	public int[] data = new int[24];
	public float size;
	public uint[] questItems = new uint[6]; //quest drop
}
