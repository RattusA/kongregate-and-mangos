﻿public class MobTemplate
{
	public uint entry;
	public string name;
	public string name1;
	public string name2;
	public string name3;
	public string subName;
	public string iconName;
	public uint type_flags;
	public uint type;
	public uint family;
	public uint rank;
	public uint[] killCredit = new uint[2];
	public uint[] modelId = new uint[4];
	public float unk16;
	public float unk17;
	public byte racialLeader;
	public uint[] questItems = new uint[6];
	public uint movementId;
	//not needed	
	public uint trainer_type;
	public uint trainer_class;
	public uint trainer_race;
	public uint trainer_spell;
}