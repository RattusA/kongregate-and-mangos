using UnityEngine;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Math;
using System;
using Org.BouncyCastle.Crypto.Digests;
using System.Threading;

public class WorldSession:MonoBehaviour // and obj manager
{
	public class questTemplate
	{
		public uint questEntry;
		public string status;
		public string name;
		public string Objective;
		public string description;
		public string progress;
		public string[] objectives = new string[6];
		public string completion;
		public string[] rewards = new string[6];
		public Texture2D[] rewardsIcon = new Texture2D[6];		
	};

	public static WorldSession istance;
	ulong CasterGUID = 0;
	public static RealmSocket parent;
	List<WhoListEntry> whoList = new List<WhoListEntry>();// WhoList entry objects
	public static Dictionary<ulong, BaseObject> realChars = new Dictionary<ulong, BaseObject>();/// real chars (other player Script)
	Dictionary<ulong, SpawnChar> objectsToSpawn = new Dictionary<ulong, SpawnChar>();// we have the opcode but not the gender and race
	List<ulong> waitingList = new List<ulong>();
	List<MobTemplate> templates = new List<MobTemplate>();
	static Dictionary<uint, GameObjectTemplate> gameObjectTemplates = new Dictionary<uint, GameObjectTemplate>();
	static int[] instancesArray = new int[]{33, 34, 36, 43, 47, 48, 90, 109, 129, 560};//when a new instance is added put it's id here
	public static MainPlayer player = null;
	public static InterfaceManager interfaceManager = null;
	//	Array lootItems = new Array();
	//	//Array needItems = new Array();
	bool  sessionInQueue = false;
	WaitingMessagesManager waitingMessages = new WaitingMessagesManager();
	GuildHandler guildhandler = new GuildHandler();
	static public WorldPacket charEnumPkt;
	static public StableMaster stableMaster;
	uint k = 0;
	static AuctionManager auctionManager;
	uint rewId ;
	byte QUEST_REPUTATIONS_COUNT = 5;

	void Awake()
	{
		istance = this;
	}

	public static LoadZone GetLoadZoneManager ()
	{
		LoadZone loadZoneManager = null;
		GameObject go = GameObject.Find("LoadZoneManager");
		if(go)
		{
			loadZoneManager = go.GetComponent<LoadZone>();
		}
		return loadZoneManager;
	}
	
	public static BundleLoader GetBundleLoader ()
	{
		BundleLoader bundleLoader = null;
		GameObject go = GameObject.Find("LoadZoneManager");
		if(go)
		{
			bundleLoader = go.GetComponent<BundleLoader>();
		}
		return bundleLoader;
	}
	
	public static bool isInstance (uint instId)
	{
		bool ret = false;
		for(int i = 0; i < instancesArray.Length; i++)	
		{
			if(instancesArray[i] == instId)
			{
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	BaseObject spawn (ulong guid, Races race, Gender gender)
	{
		SpawnChar script = (SpawnChar)getObjToSpawn(guid);
		GameObject obj = null;
		
		if(script == null)
		{
			//			Debug.Log("Object guid not found "+guid);
			return null;
		}
		
		switch(script.getTypeID())
		{
		case TYPEID.TYPEID_PLAYER:
			obj = Player.LoadPlayerModel(race, gender);
			break;
		case TYPEID.TYPEID_UNIT:
			try
			{
				MobTemplate mt = getMobTemplate(script.GetEntry());
				if(mt.modelId[0] == 0)
				{
					throw new Exception();
				}
				
				CreatureDisplayInfoEntry mobDisplay = dbs.sCreatureDisplayInfo.GetRecord(mt.modelId[0]);
				if(mobDisplay.ID == 0)
				{
					throw new Exception();
				}
				obj = Instantiate<GameObject>(Resources.Load<GameObject>(mobDisplay.ModelMesh));		
				
			}
			catch(Exception e)
			{
				obj = Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/mobs/1"));
			}
			break;
		case TYPEID.TYPEID_GAMEOBJECT:
			try
			{
				GameObjectTemplate go = getGameObjectTemplate(script.GetEntry());
				if(go == null)
				{
					throw new Exception();
				}
				obj = Instantiate<GameObject>(Resources.Load<GameObject>(dbs.sGameObjectDisplayInfo.GetRecord(go.displayID).ModelMesh));
			}
			catch(Exception e)
			{
				Debug.Log (string.Format ("Cant find gameobject: {0}. Loading default!", script.GetEntry ()));
				obj = Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/objects/cub"));
			}
			break;
		case TYPEID.TYPEID_CORPSE:
			obj = Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/cub1"));							
			Debug.Log("Loading Default corpse!");
			break;
		case TYPEID.TYPEID_CONTAINER:
			try
			{
				// WARNING perhaps nothing loads. Always throws an exception.
				obj = Instantiate<GameObject>(Resources.Load<GameObject> (
					string.Format ("prefabs/objects/{0}", getGameObjectTemplate (script.GetEntry ()).name)));
			}
			catch(Exception e)
			{
				obj = Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/objects/cub"));
				Debug.Log (string.Format ("Cant find gameobject{0}. Loading default gameobject!", script.GetEntry ()));
			}
			break;
		}
		BaseObject scr = obj.transform.Find("obj").gameObject.GetComponent<BaseObject>();
		scr.guid = guid;
		
		try
		{
			if(script != null)
			{
				//				Debug.Log("we have a script, copy values: type: "+script.getTypeID());
				scr.SetType(script.getTypeID());
				scr.InitValues();//obsolute. setType use this
				scr.CopyValues(script.getValues());
				
				scr.Start();
				scr.SetPosition(script.pos,script.o);
				scr.moveSpeed = script.moveSpeed;
				
				scr.auraManager = new AuraManager(scr);
				scr.auraManager.auras.Clear();
				foreach(Aura ar in script.auras)
				{
					ar.unit = scr as Unit;
					scr.auraManager.auras.Add(ar);
				}
				
				if(script.getTypeID() == TYPEID.TYPEID_GAMEOBJECT)
				{
					GameObjectTemplate go2 = getGameObjectTemplate(script.GetEntry());
					(scr as _GameObject).gameobjectType = go2.type;
					(scr as IBaseObject).CheckLootedFlag();
				}
				
				if(script.getTypeID() == TYPEID.TYPEID_UNIT)
				{
					(scr as IBaseObject).CheckLootedFlag();
					if((scr as NPC).isPortalMasterTriggered())//if Unit is a gossip and is not selectable then we have a special npc type and we set  him
					{
						//this npc is a special case because he is gossip and we select gossip menu options using Triggers not by buttons like a normal gossip
						try
						{
							Transform _triggerTransform = scr.TransformParent.Find("trigger_collider");
							if(_triggerTransform)
							{
								GameObject _trigger = _triggerTransform.gameObject;
								//we add portal script and set npc refference
								_trigger.AddComponent<portal>();
								_trigger.GetComponent<portal>().npc = (scr as NPC);
								scr.TransformParent.localScale = new Vector3(scr.GetScale(), scr.scale, scr.scale);
							}
						}
						catch(Exception exception)
						{
							MonoBehaviour.print("we could not find that child... " + exception.ToString());
						}
					}
					//instantiating minimap BLIP for units (npc and mobs alike)
					if(!(scr as NPC).isCreature()) // for NPCs
					{
						GameObject _minimapBlipM =Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/MiniMap/NPC"));
						_minimapBlipM.transform.parent = scr.TransformParent;
						_minimapBlipM.transform.localPosition = Vector3.zero;//(0, -500, 0);
					}
					else // for MOBs
					{	/*
						GameObject _minimapBlipN = Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/MiniMap/MOB"));
						_minimapBlipN.transform.parent = scr.transformParent;
						_minimapBlipN.transform.localPosition = Vector3.zero;//(0, -500, 0);
						*/
					}
				}
				
				if(script.getTypeID() == TYPEID.TYPEID_CORPSE)
				{
					(scr as Corpse).AssebleCorpse();	
				}
				
				removeObjectToSpawn(guid);//remove object from spawn object list for not beeing spawned a second time
			}
			else
			{
				Debug.Log("Model has no script attach");
			}	
			AddOrReplaceRealChar(scr);
			ignoreCollisions(guid);
			
			return scr;
		}
		catch(Exception exception)
		{
			Debug.Log(guid+" Object was not instantiated! Unknown TYPEID\n\n " + exception);
			return null;
		}
	}
	
	void  ignoreCollisions (ulong uguid){
		try{
			BaseObject ops = GetRealChar(uguid);
			Collider opsCollider = ops.transform.parent.GetComponent<Collider>();
			if(!ops || !opsCollider)
				return;
			
			if(!ops.gameObject)
				return;
			
			foreach(BaseObject char1 in realChars.Values)//ignore collision with the rest
			{
				if(ops != char1)
				{
					if(char1.gameObject)
					{
						try
						{
							Transform transformForWork = (char1.transform.parent == null) ? char1.transform: char1.transform.parent;
							Collider charCollider = transformForWork.GetComponent<Collider>();
							if(charCollider != null)
							{
								Physics.IgnoreCollision(opsCollider, charCollider, true);
							}
						}
						catch(Exception e)
						{
							Debug.LogException(e);
							//MonoBehaviour.print(e.ToString());}// self collision mabe
						}
					}
				}
			}
			
			GameObject player = GameObject.Find("player");//ignore collisions with main player
			if(player)
			{
				try
				{
					Physics.IgnoreCollision(opsCollider, player.gameObject.GetComponent<Collider>(),true);
				}
				catch(Exception e)
				{
					Debug.LogException(e);
					//MonoBehaviour.print(e.ToString());}// self collision mabe
				}
				
			}
		}catch(Exception e){Debug.Log(e);}
	}
	
	float updateTime = 5f;
	float timeLeft = 1f;
	public float _firstVisit;

	public void Update()
	{
		if(parent && parent.IsLoseConnect())
		{
			timeLeft -= Time.deltaTime;
			if(timeLeft <= 0)
			{
				parent.CloseSocket();
				Logout();
			}
		}
		else
		{
			timeLeft = updateTime;
		}
		
		if(Defines.showInterf)
		{
			Defines.showInterf = false;
			interfaceManager.hideInterface = false;
			player.interf.stateManager();
		}
		
		if(RealmSocket.IsRaidState)
		{
			RealmSocket.IsRaidState = false;
			
			RaidWindowManager.show();
		}
		
		if(RealmSocket.IsGuildState)
		{
			RealmSocket.IsGuildState = false;
			WorldSession.player.menuSelected = 4;
			WorldSession.player.guildUI.InitGuildAtlas();
			WorldSession.player.guildUI.boolShowingGuild = true;
		}
		
		if(RealmSocket.IsFriendState)
		{
			RealmSocket.IsFriendState = false;
			WorldSession.player.interf.SocialDelegate(null);
		}
		
		if(objectsToSpawn.Count == 0)
		{
			return;
		}
		
		ulong[] keys = new ulong[objectsToSpawn.Count];
		objectsToSpawn.Keys.CopyTo(keys, 0);
		foreach(ulong key in keys)
		{
			SpawnChar sc = null;
			if(objectsToSpawn.TryGetValue(key, out sc))
			{
				try
				{
					QueryObjectInfo(sc.guid);
				}
				catch(Exception e)// mabe it was allready deleted or unity bug
				{}
			}
		}
	}
	
	void  SendLootMethod (ulong guid)
	{
		//Debug.Log("+++++++++++++++++++++++++++++++++++++++++++++++++Se trimite modul de groop loot!!");
		WorldPacket pkt = new WorldPacket();
		uint lootMethod=4;
		uint threshold=2;
		pkt.Append(ByteBuffer.Reverse(lootMethod));
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append(ByteBuffer.Reverse(threshold));
		pkt.SetOpcode(OpCodes.CMSG_LOOT_METHOD);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendLootRoll (ulong guid, uint slot, byte rollType)
	{
		WorldPacket pkt = new WorldPacket();
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append(ByteBuffer.Reverse(slot));
		pkt.Append(rollType);
		pkt.SetOpcode(OpCodes.CMSG_LOOT_ROLL);
		RealmSocket.outQueue.Add(pkt);
	}
	
	//	void SendLootRelease(ulong guid)
	//	{
	//		WorldPacket pkt = new WorldPacket();
	//		pkt.Append(ByteBuffer.Reverse(guid));
	//		pkt.SetOpcode(OpCodes.CMSG_LOOT_RELEASE);
	//		RealmSocket.outQueue.Add(pkt);
	//	}
	
	
	public void  SendLootRequest (ulong guid)
	{
		WorldPacket pkt = new WorldPacket();
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.SetOpcode(OpCodes.CMSG_LOOT);
		RealmSocket.outQueue.Add(pkt);
	}
	
	void  SendQueryCreatureName (ulong guid, uint entry)
	{
		WorldPacket pkt = new WorldPacket();
		pkt.Append(ByteBuffer.Reverse(entry));
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.SetOpcode(OpCodes.CMSG_CREATURE_QUERY);
		RealmSocket.outQueue.Add(pkt);
	}
	
	void  SendQueryGameobject (uint entry,  ulong guid)
	{
		//Debug.Log("WORLD : CMSG_GAMEOBJECT_QUERY - Entry:"+entry+" Guid: "+guid);
		WorldPacket wp = new WorldPacket(OpCodes.CMSG_GAMEOBJECT_QUERY,0);
		wp.Append(ByteBuffer.Reverse(entry));
		wp.Append(ByteBuffer.Reverse(guid));
		RealmSocket.outQueue.Add(wp);
	}
	
	void HandleGameobjectQueryResponse(WorldPacket pkt)// incompleate handler
	{
		try{
			GameObjectTemplate gameObjTemp = new GameObjectTemplate();
			uint entryID = pkt.ReadReversed32bit();
			//Debug.Log("World: SMSG_GAMEOBJECT_QUERY_RESPONSE- Entry: "+entryID);
			if((entryID & 0x80000000) > 0)
			{
				//Debug.Log("World: SMSG_GAMEOBJECT_QUERY_RESPONSE- Entry: "+entryID+" missing gameobject info!");
				gameObjTemp.entry = entryID;
				gameObjTemp.name = "Unknown object";
			}
			else
			{
				gameObjTemp.entry = entryID;
				gameObjTemp.type = (GameobjectTypes)pkt.ReadReversed32bit();
				gameObjTemp.displayID = pkt.ReadReversed32bit();
				gameObjTemp.name = pkt.ReadString();
				pkt.Read();pkt.Read();pkt.Read(); //name2, name3, name4; null values
				gameObjTemp.iconName = pkt.ReadString();
				gameObjTemp.castBarCaption = pkt.ReadString();
				gameObjTemp.unk1 = pkt.ReadString();
				for(byte j = 0;j<24;j++)
				{
					gameObjTemp.data[j] = pkt.ReadInt();
				}
				gameObjTemp.size = pkt.ReadFloat();
				for(byte i = 0;i<6;i++)
				{
					gameObjTemp.questItems[i] = pkt.ReadReversed32bit();
				}
				//Debug.Log("World: New Object Template recived- Entry: "+gameObjTemp.entry+ ", display:"+gameObjTemp.displayID+", Name:"+gameObjTemp.name );
				
//				gameObjectTemplates.Add(gameObjTemp);
			}
			gameObjectTemplates[entryID] = gameObjTemp;
		}
		catch(Exception e)
		{
			Debug.Log(e);
		}
		
	}
	
	void  HandleNameQueryResponseOpcode (WorldPacket pkt)
	{
		ulong guid = pkt.GetPacketGuid();
		byte unk = pkt.Read();
		string pname = pkt.ReadString();
		Letter tmpLetter = player.mailHandler.FindLetterBySenderGuid((uint)guid);
		if(tmpLetter != null)
		{
			player.mailHandler.SetNameForGuid(pname, (uint)guid);
		}
		FriendInfo tmpInfo = player.SocialMgr.FindFriendByGuid(guid);
		if(tmpInfo != null)
		{
			tmpInfo.name = pname;
		}
		else
		{
			tmpInfo = player.SocialMgr.FindIgnoreByGuid(guid);
			if(tmpInfo != null)
			{
				tmpInfo.name = pname;
			}
		}
		
		pkt.ReadString();/// BG name
		Races race = (Races)pkt.Read();// race
		Gender gender = (Gender)pkt.Read();// gender
		byte classtype = pkt.Read();// class //Here be class info workaround for party members
		
		/*if (partyMemberNeed && player.group.partyMembers.length > 0)
		{	
			var foundOne= false;
			for(var i=0; i< player.group.partyMembers.length;i++)
			{
				PartyPlayer plr =  player.group.partyMembers[i];
				if(plr.guid == guid)
				{
					plr.playerClass = classtype;
					foundOne = true;
//					Debug.Log("player "+plr.name+" has class byte "+plr.playerClass);
				}
			}
			if(!foundOne)
				partyMemberNeed = false;
		}*/
		
		//Debug.Log("decs - player name " + pname);
		//Debug.Log("HandleNameQueryResponseOpcode - race"+race+"gender"+gender);
		OtherPlayer scr = GetRealChar(guid) as OtherPlayer;
		
		NextMessage message = waitingMessages.PopMessageFromWaitingList(guid );
		if (message != null )
		{
			SendMessageFromChatMessagePacket(message.pkt, message.ms, message.type, message.lang, pname);
		}
		
		if(scr != null)
		{
			if(scr.gameObject != null)
			{
				scr.InitName(pname);
				scr.race = race;
				scr.gender = gender;
				//				scr.classtype = classtype;
				//scr.initMeshs(scr.gameObject.transform,race,gender);
			}
		}
		else
		{
			scr = spawn(guid, race, gender) as OtherPlayer;
			if(scr == null)
			{
				return;
			} 
			scr.InitName(pname);
			scr.race = race;
			scr.gender = gender;
			//			scr.classtype = classtype;
			scr.armory = new AssambleCharacter(scr as Player);
			scr.armory.InitArmory(0);
			scr.armory.UpdateArmory(0);
			scr.itemManager.DeactivateRange();
			//			scr.itemManager.RefreshHands();
		}
		
	}
	
	void  HandleCreatureQueryResponseOpcede (WorldPacket pkt)
	{
		uint entry;
		uint aux = 0x80000000;
		entry = pkt.ReadReversed32bit();
		
		if((entry & aux) > 0)// there is no data for this entry on the server
		{
			return;
		}
		if(getMobTemplate(entry) != null)// we allready have this template... mebe we spamed the server:D
		{
			return;
		}
		MobTemplate template = new MobTemplate();
		template.entry = entry;
		template.name = pkt.ReadString();
		template.name1 = ""+pkt.Read();
		template.name2 = ""+pkt.Read();
		template.name3 = ""+pkt.Read();
		template.subName = pkt.ReadString();
		template.iconName = pkt.ReadString();
		template.type_flags = pkt.ReadReversed32bit();
		template.type = pkt.ReadReversed32bit();
		template.family = pkt.ReadReversed32bit();
		template.rank = pkt.ReadReversed32bit();
		template.killCredit[0] = pkt.ReadReversed32bit();
		template.killCredit[1] = pkt.ReadReversed32bit();
		for(int i = 0; i < 4; i++)
		{
			template.modelId[i] = pkt.ReadReversed32bit();
		}
		template.unk16 = pkt.ReadFloat(); //health modifier
		template.unk17 = pkt.ReadFloat(); //power modifier
		//MonoBehaviour.print("hp "+template.unk16+ " mp "+template.unk17);
		template.racialLeader = pkt.Read();
		for(int i = 0; i < 6; i++)
		{
			template.questItems[i] = pkt.ReadReversed32bit();
		}
		template.movementId = pkt.ReadReversed32bit();
		/*template.trainer_type  = pkt.ReadReversed32bit();
		template.trainer_class = pkt.ReadReversed32bit();
		template.trainer_race  = pkt.ReadReversed32bit();
		template.trainer_spell = pkt.ReadReversed32bit();*/
		templates.Add(template);
	}

	public static void  setParent (RealmSocket p)
	{
		parent = p;
	}
	
	void HandleAreaTriggerMessage(WorldPacket pkt)// TODO Not finishetd yet
	{
		pkt.ReadType(4);
		string str = pkt.ReadString();
		MonoBehaviour.print("77777777777777777777777777777777777		"+str);
	}

	public void RequestCharactersList()
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_CHAR_ENUM, 0);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  Logout ()
	{
		realChars.Clear();
		if(player != null)
		{
			UnityEngine.Object.Destroy(player.transform.parent.gameObject);
		}
		LoadSceneManager.LevelType = LoadSceneManager.LEVELTYPE.LEVELTYPE_LOGIN;
		LoadSceneManager.LoginState = LoadSceneManager.LOGINSTATE.LOGINSTATE_SELECT;
		if(GameManager.Instance != null)
		{
			GameManager.Instance.LoadScene("LoadingScreen");
		}
		else
		{
			Application.LoadLevel("LoadingScreen");
		}
		//Application.LoadLevel("login");
	}
	
	
	void  HandleNewWorldOpcode (WorldPacket pkt)
	{
		//Debug.Log("Generating new zone");
		objectsToSpawn.Clear();/// no more spawning in this world
		//MonoBehaviour.print("new World");
		uint mapId;
		Vector3 pos;
		float or;
		mapId = pkt.ReadReversed32bit();
		//MonoBehaviour.print(mapId);
		pos.x = pkt.ReadFloat();
		pos.y = pkt.ReadFloat();
		pos.z = pkt.ReadFloat();
		//MonoBehaviour.print(pos);
		or = pkt.ReadFloat();
		//MainPlayer.Pos = pos;
		//MainPlayer.or = or;
		player.SetPosition(pos,or);
		player.MoveStop();
		player.isMoving = false;
		//if(mapId!=0)
		GetLoadZoneManager().zone = ""+mapId;
		if(isInstance(mapId))
		{
			LoadSceneManager.LevelType = LoadSceneManager.LEVELTYPE.LEVELTYPE_INDOOR;
			//Application.LoadLevel("InDoorScene");
		}
		else
		{ 
			LoadSceneManager.LevelType = LoadSceneManager.LEVELTYPE.LEVELTYPE_OUTDOOR;
			
			//	Application.LoadLevel("OutDoorScene");
		}	
		if(GameManager.Instance != null)
		{
			GameManager.Instance.LoadScene("LoadingScreen");
		}
		else
		{
			Application.LoadLevel("LoadingScreen");
		}
		ButtonOLD.chatState = false;
		player.chatOpend = false;
		////
		// when getting teleported, the client sends CMSG_CANCEL_TRADE 2 times.. dont ask me why.
		WorldPacket wp = new WorldPacket(OpCodes.CMSG_CANCEL_TRADE,8);
		RealmSocket.outQueue.Add(wp);
		RealmSocket.outQueue.Add(wp);
		
		wp.SetOpcode(OpCodes.MSG_MOVE_WORLDPORT_ACK);
		RealmSocket.outQueue.Add(wp);
		
		wp.SetOpcode(OpCodes.CMSG_SET_ACTIVE_MOVER);
		wp.Append(ByteBuffer.Reverse(player.guid));// GetGuid();
		RealmSocket.outQueue.Add(wp);
		
		// TODO: put this into a capsule class later, that autodetects movement flags etc.
		uint aux32 = 0;
		ushort aux16 = 0;
		wp.SetOpcode(OpCodes.MSG_MOVE_FALL_LAND);
		//WorldPacket response(MSG_MOVE_FALL_LAND,4+2+4+4+4+4+4+4);
		wp.Append(aux32);
		wp.Append(aux16);//response << uint32(0) << (uint16)0; // no flags; unk
		wp.Append(aux32); // time correct?
		Transform tr = player.transform;
		wp.Append(tr.position.x);
		wp.Append(tr.position.y);
		wp.Append(tr.position.z);
		wp.Append(tr.rotation.eulerAngles.y);
		aux32 = 100;
		wp.Append(aux32);// << x << y << z << o << uint32(100); // simulate 100 msec fall time
		RealmSocket.outQueue.Add(wp);
	}
	
	void  HandleAuthChallengeOpcode (WorldPacket pkt)
	{
		LoginWindow.SetInventoryMessage("HandleAuthChallenge");
		
		uint unk = 0;
		byte[] unk_bt = BitConverter.GetBytes(unk);
		
		uint clientBuild32 = Convert.ToUInt32(Defines.clientBuild);
		
		byte[] clientBuild32_bt = BitConverter.GetBytes(clientBuild32);
		
		byte[] sp_bt = new byte[4];
		byte[] serverseed_bt = new byte[4];
		
		pkt.Read(sp_bt, 4);
		pkt.Read(serverseed_bt, 4);
		
//		uint sp = BitConverter.ToUInt32(sp_bt, 0);
//		uint serverseed = BitConverter.ToUInt32(serverseed_bt, 0);
		
		string username = LoginWindow.accountLogin.ToUpper();
		byte[] I = Encoding.UTF8.GetBytes(username);
		
		IDigest digest = new Sha1Digest();
		byte[] digest_output = new byte[digest.GetDigestSize()];
		
		SecureRandom random = new SecureRandom();
		byte[] sr = random.GenerateSeed(4);
		BigInteger clientseed = new BigInteger(1, sr);
		
		uint clientseed_uint32 = clientseed.AsDword();
		byte[] clientseed_uint32_bt = BitConverter.GetBytes(clientseed_uint32);
		
		digest.BlockUpdate(I);
		digest.BlockUpdate(unk_bt, 0, 4);
		digest.BlockUpdate(clientseed_uint32_bt, 0, 4);
		digest.BlockUpdate(serverseed_bt, 0, 4);
		digest.BlockUpdate(Global.sessionKey.getBytes());
		digest.DoFinal(digest_output, 0);
		
		WorldPacket auth = new WorldPacket();
		for(long idx = 0; idx<clientBuild32_bt.Length; idx++)
		{
			auth.Append(clientBuild32_bt[idx]);
		}
		
		auth.Append(unk);
		auth.Append(username);
		auth.Append(unk);
		
		Global.InvertBytes(clientseed_uint32_bt);
		
		for(long idx = clientseed_uint32_bt.Length - 1; idx >= 0; idx--)
		{
			auth.Append(clientseed_uint32_bt[idx]);
		}
		
		auth.Append(unk);
		auth.Append(unk);
		auth.Append(unk);
		auth.Append(unk);
		auth.Append(unk);
		auth.Append(digest_output, 20);
		auth.Append(unk);
		auth.SetOpcode(OpCodes.CMSG_AUTH_SESSION);				
		
		RealmSocket.outQueue.Add(auth);
		
		RealmSocket.cryptKey = Global.sessionKey;
		RealmSocket.initCrypt = true;
	}
	
	void  HandleAuthResponseOpcode (WorldPacket pkt)
	{
		ResponseCodes errcode = (ResponseCodes)pkt.Read();		
		if (!sessionInQueue)
		{
			byte[] BillingTimeRemaining_bt = new byte[4];
			byte[] BillingTimeRested_bt = new byte[4];
			
			pkt.Read(BillingTimeRemaining_bt, 4);
			byte BillingPlanFlags = pkt.Read();
			pkt.Read(BillingTimeRested_bt, 4);
			byte expansion = pkt.Read();
			
//			uint BillingTimeRemaining = BitConverter.ToUInt32(BillingTimeRemaining_bt, 0);
//			uint BillingTimeRested = BitConverter.ToUInt32(BillingTimeRested_bt, 0);
		}
		
		if (errcode == ResponseCodes.AUTH_OK)
		{
			sessionInQueue = false;
			
			if(GameManager.Instance != null)
			{
				GameManager.Instance.LoadScene("SelectCharacterWindow");
			}
			else
			{
				Application.LoadLevel("SelectCharacterWindow");
			}
		}
		else if (errcode == ResponseCodes.AUTH_WAIT_QUEUE)
		{
			byte[] PositionInQueue_bt = new byte[4];
			pkt.Read(PositionInQueue_bt, 4);
			byte unk = pkt.Read();
			
//			uint PositionInQueue = BitConverter.ToUInt32(PositionInQueue_bt, 0);
			
			if (sessionInQueue) // need to update the position in the queue
			{
			}
			else // notify a busy server, and display the position in the queue
			{
			}
			sessionInQueue = true;
		}
		else
		{
			Debug.LogError("World Authentication failed, errcode=" + errcode);
		}
	}
	
	private void  HandleCharEnumOpcode (WorldPacket pkt)
	{
		charEnumPkt = pkt;
		AccountCharactersList.instance.ReadPacketData();
	}
	
	public static void  EnterWorldWithCharacter (string nume, ulong guid,  uint mapId)
	{
		//logIn.LogInState = "Entering World";
		LoadSceneManager.LoadingStatus = LoadSceneManager.LOADING.LOADING_INPROGRESS;
		Instantiate<GameObject>(Resources.Load<GameObject>("GUI/LoadingDummy1"));
		
		WorldPacket wp = new WorldPacket(OpCodes.CMSG_PLAYER_LOGIN,8);
		wp.Append(guid);
		RealmSocket.outQueue.Add(wp);
		MainPlayer.GUID = ByteBuffer.Reverse(guid );
		Defines.loading = true;
		
		//if(mapId!=0)
		GetLoadZoneManager().zone = ""+mapId;
		if(isInstance(mapId))
		{
			LoadSceneManager.LevelType = LoadSceneManager.LEVELTYPE.LEVELTYPE_INDOOR;
			//Application.LoadLevel("InDoorScene");
		}
		else
		{ 
			LoadSceneManager.LevelType = LoadSceneManager.LEVELTYPE.LEVELTYPE_OUTDOOR;
			//Application.LoadLevel("OutDoorScene");
		}	
		
		LoginWindow.SetInventoryMessage("", 0.1f);
		if(GameManager.Instance != null)
		{
			GameManager.Instance.LoadScene("LoadingScreen");
		}
		else
		{
			Application.LoadLevel("LoadingScreen");
		}
	}
	
	void  HandleWhoOpcode (WorldPacket pkt)
	{
		uint count = (uint)pkt.ReadType(4);
		uint unk = (uint)pkt.ReadType(4);
		if(count>1)
		{
			whoList.Clear();
		}
		for(int i = 0; i < count; i++)
		{
			WhoListEntry wle = new WhoListEntry();
			wle.Copy(pkt);
			whoList.Add(wle);
		}
	}
	
	
	
	void  HandleCompressedUpdateObjectOpcode (WorldPacket pkt){
		//MonoBehaviour.print("recieved compressed package...");
		if(pkt.Decompress() )
		{
			
			HandleUpdateObjectOpcode(pkt);
			
		}
		else
		{
			MonoBehaviour.print("Pkt decompresion failed");
		}
	}	
	
	void  HandleUpdateObjectOpcode (WorldPacket pkt){
		//MonoBehaviour.print("updating pkt...");
		OBJECT_UPDATE_TYPE utype;
		uint usize;
		uint ublocks;
		uint readblocks = 0;
		ulong uguid;
		ublocks = pkt.ReadReversed32bit();
		
		while((pkt.RPos() < pkt.Size()) && (readblocks < ublocks))
		{
			utype = (OBJECT_UPDATE_TYPE)pkt.Read();
			//	MonoBehaviour.print("Ublock: "+readblocks+ " Updatetype:  " +tp);
			switch(utype)
			{
			case OBJECT_UPDATE_TYPE.UPDATETYPE_VALUES :
				uguid = pkt.GetPacketGuid();
				ValuesUpdate(uguid,pkt);//update fields
				break;
			case OBJECT_UPDATE_TYPE.UPDATETYPE_MOVEMENT :
				uguid = pkt.GetPacketGuid();
				TYPEID typeID;
				BaseObject obj = GetRealChar(uguid);
				if(obj != null)
				{
					typeID = obj.GetTypeID();
					MovementUpdate(typeID, uguid, pkt);
				}
				else
				{
					typeID = ObjectInfo.GetTypeIdByGuid(uguid);
				}
				break;
			case OBJECT_UPDATE_TYPE.UPDATETYPE_CREATE_OBJECT2 :
			case OBJECT_UPDATE_TYPE.UPDATETYPE_CREATE_OBJECT :
				uguid = pkt.GetPacketGuid();
				TYPEID objTypeId = (TYPEID)pkt.Read();
				//MonoBehaviour.print("  GUID: "+uguid);
				//if(getObj(uguid))//dont create objects if already present in memory. recreate every object except ourself
				//{
				remove(uguid);	//this remove all king on server objects items, units, ect	
				//}
				//getObj() function dont handle item and container objects, only objects derived from BaseObjet
				if(!GetRealChar(uguid))
				{
					switch(objTypeId)
					{
					case TYPEID.TYPEID_OBJECT : // do nothing
						MonoBehaviour.print("tralalla OBJECT "+uguid);
						break;
						
					case TYPEID.TYPEID_ITEM : 
						//	MonoBehaviour.print("Create Item" + uguid);
						objectsToSpawn[uguid] = new SpawnChar(uguid, objTypeId);
						break;
					case TYPEID.TYPEID_CONTAINER :
						//	MonoBehaviour.print("Create Container" + uguid);
						objectsToSpawn[uguid] = new SpawnChar(uguid, objTypeId);   
						break;
					case TYPEID.TYPEID_UNIT :
						//MonoBehaviour.print("Create Unit");
						objectsToSpawn[uguid] = new SpawnChar(uguid, objTypeId);
						break;
					case TYPEID.TYPEID_PLAYER :
						if(uguid != MainPlayer.GUID) /// diferent from our player
						{
							//	MonoBehaviour.print("Create Player");
							objectsToSpawn[uguid] = new SpawnChar(uguid, objTypeId);
						}
						break;
					case TYPEID.TYPEID_GAMEOBJECT :
						//MonoBehaviour.print("Create GO");
						objectsToSpawn[uguid] = new SpawnChar(uguid, objTypeId);
						break;
					case TYPEID.TYPEID_CORPSE :
						//MonoBehaviour.print("Create Corpse");
						objectsToSpawn[uguid] = new SpawnChar(uguid, objTypeId);
						break;
					case TYPEID.TYPEID_DYNAMICOBJECT :
						//	MonoBehaviour.print("Create Dinamic");
						//	objectsToSpawn.Add(new SpawnChar(uguid, objTypeId));
						break;
					case TYPEID.TYPEID_AREATRIGGER :
						MonoBehaviour.print("Create Area trigger");
						objectsToSpawn[uguid] = new SpawnChar(uguid, objTypeId);
						break;		
					default: Debug.Log("Create unknown");
						break;
					}
				}
				else
				{
					Debug.Log("Object already exists "+uguid);
				}
				
				try
				{
					MovementUpdate(objTypeId, uguid, pkt);
					ValuesUpdate(uguid,pkt);
					QueryObjectInfo(uguid);
				}
				catch(Exception exception)
				{
					MonoBehaviour.print(exception.ToString());
				}
				break;
				
			case OBJECT_UPDATE_TYPE.UPDATETYPE_OUT_OF_RANGE_OBJECTS :
				//	Debug.Log(OBJECT_UPDATE_TYPE.UPDATETYPE_OUT_OF_RANGE_OBJECTS+"");
				usize = pkt.ReadReversed32bit();
				for(int i = 0;i<usize;i++)
				{
					uguid = pkt.GetPacketGuid();
					remove(uguid);
				}
				break;
				
			case OBJECT_UPDATE_TYPE.UPDATETYPE_NEAR_OBJECTS:
				Debug.Log(OBJECT_UPDATE_TYPE.UPDATETYPE_NEAR_OBJECTS+"");
				break;
			default :
				MonoBehaviour.print("Dump block: " + readblocks + " bufer position :" + (pkt.RPos()-1) + " byte ");
				break;
				
			}
			readblocks++;
		}//endwhile
	}// func
	
	
	
	void  MovementUpdate (TYPEID objectTypeId, ulong uguid, WorldPacket pkt)
	{
		MovementInfo mi = new MovementInfo();
		OBJECT_UPDATE_FLAGS flags;
		float speedWalk;
		float speedRun;
		float speedSwimBack;
		float speedSwim;
		float speedWalkBack;
		float speedTurn;
		float speedFly;
		float speedFlyBack;
		float speedPitchRate;
		BaseObject obj = GetRealChar(uguid);
		SpawnChar aux = getObjToSpawn(uguid);
		
		flags = (OBJECT_UPDATE_FLAGS)pkt.ReadReversed16bit();
		
		OBJECT_UPDATE_FLAGS flag = OBJECT_UPDATE_FLAGS.UPDATEFLAG_LIVING;
		if((flags & flag) > 0)
		{
			
			mi.flags = (MovementFlags)pkt.ReadReversed32bit();
			mi.unkFlags = (MovementFlags)pkt.ReadType(2);
			mi.time = (uint)pkt.ReadType(4);
			mi.pos.x = pkt.ReadFloat();
			mi.pos.y = pkt.ReadFloat();
			mi.pos.z = pkt.ReadFloat();
			mi.o = pkt.ReadFloat();
			//	MonoBehaviour.print("start reading living for guid " + uguid+ "	"+mi.pos);
			
			if(obj)
			{
				MonoBehaviour.print("********************setting position");
				obj.SetPosition(mi.pos,mi.o);
			}
			else if(uguid == MainPlayer.GUID)
			{
				//				Debug.Log("Position MainPLayer "+mi.pos);
				// find our player if is spawned
				player.SetPosition(mi.pos,mi.o);
			}
			else
			{
				if(aux != null)
					aux.SetPosition(mi.pos,mi.o);
				
				if(objectTypeId == TYPEID.TYPEID_AREATRIGGER)
					MonoBehaviour.print("a depasit limita de tipuri");
				//MonoBehaviour.print("MovementUpdate - new ObjectType " + objectTypeId +" and guid " + uguid );
			}
			
			
			MovementFlags flag32 = MovementFlags.MOVEMENTFLAG_ONTRANSPORT;
			if((mi.flags & flag32) > 0)
			{
				MonoBehaviour.print(mi.flags & flag32);
				mi.t_guid = pkt.GetPacketGuid();
				mi.tpos.x = pkt.ReadFloat();
				mi.tpos.y = pkt.ReadFloat();
				mi.tpos.z = pkt.ReadFloat();
				mi.t_o = pkt.ReadFloat();
				mi.t_time = (uint)pkt.ReadType(4);
				mi.t_seat = pkt.Read();
				
				
				if((mi.unkFlags & MovementFlags.MOVEMENTFLAG_LEVITATING) > 0)
				{
					pkt.ReadType(4);//time
				}
				
				MonoBehaviour.print(" MovementUpdate - MOVEMENTFLAG_ONTRANSPORT ");
			}
			
			flag32 = MovementFlags.MOVEMENTFLAG_SWIMMING;// | MovementFlags.MOVEMENTFLAG_FLYING;
			MovementFlags flag322 = MovementFlags.MOVEMENTFLAG_FLYING;
			flag32 = flag32 | flag322;
			if((mi.flags & flag32) > 0 || (mi.unkFlags & MovementFlags.MOVEMENTFLAG_TURN_RIGHT) > 0)
			{
				mi.s_angle = pkt.ReadFloat();
			}
			
			mi.fallTime = (uint)pkt.ReadType(4);
			
			flag32 = MovementFlags.MOVEMENTFLAG_FALLING;
			if((mi.flags & flag32) > 0)
			{
				mi.j_unk = pkt.ReadFloat();
				mi.j_sinAngle = pkt.ReadFloat();
				mi.j_cosAngle = pkt.ReadFloat();
				mi.j_xyspeed = pkt.ReadFloat();
				MonoBehaviour.print(" MovementUpdate - MOVEMENTFLAG_FALLING ");
			}
			
			flag32 = MovementFlags.MOVEMENTFLAG_SPLINE_ELEVATION;
			if((mi.flags & flag32) > 0)
			{
				mi.u_unk1 = pkt.ReadFloat();
				MonoBehaviour.print(" MovementUpdate - MOVEMENTFLAG_SPLINE_ELEVATION ");
			}

			speedWalk = pkt.ReadFloat();
			speedRun = pkt.ReadFloat();
			speedSwimBack = pkt.ReadFloat();
			speedSwim = pkt.ReadFloat();
			speedWalkBack = pkt.ReadFloat();
			speedFly = pkt.ReadFloat();
			speedFlyBack = pkt.ReadFloat();
			speedTurn = pkt.ReadFloat();
			speedPitchRate = pkt.ReadFloat();

			if(aux != null)
			{
				//Debug.Log("Movement Update: RunSpeed "+speedRun);	
				aux.moveSpeed = speedRun;
			}
			else if(uguid == MainPlayer.GUID)
			{
				//	MonoBehaviour.print(" MovementUpdate - SPEEDRUN "+speedRun);
				getPlayer();
				if(player)
					player.SetSpeed(0,speedRun);
			}

			flag32 = MovementFlags.MOVEMENTFLAG_SPLINE_ENABLED;
			if((mi.flags & flag32) > 0)
			{
				//Debug.Log("+=================== DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA :>:> ====================");
				uint splineFlag = pkt.ReadReversed32bit();
				
				pkt.ReadType(4);//timepass
				pkt.ReadType(4);//duration
				pkt.ReadType(4);//id
				
				pkt.ReadFloat();//1.0ff
				pkt.ReadFloat();//1.0ff
				
				pkt.ReadFloat();//vertical accelariation
				pkt.ReadFloat();//start time
				
				uint nodes = pkt.ReadReversed32bit();//nodes
				for(int j = 0; j<nodes; j++)
				{
					/*pkt.readnew Vector3();*/ pkt.ReadFloat();pkt.ReadFloat();pkt.ReadFloat();
				}
				
				pkt.Read();//spline mode
				/*pkt.readnew Vector3();*/ pkt.ReadFloat();pkt.ReadFloat();pkt.ReadFloat();		
			}
		}
		else
		{
			Vector3 pos = new Vector3();
			float o = 0.0f;
			flag = OBJECT_UPDATE_FLAGS.UPDATEFLAG_POSITION;
			if((flags & flag) > 0)
			{
				//MonoBehaviour.print(" MovementUpdate - OBJECT_UPDATE_FLAGS.UPDATEFLAG_POSITION ");
				ulong pguid = pkt.GetPacketGuid();
				Vector3 spos = new Vector3();
				float so;
				
				pos.x = pkt.ReadFloat();
				pos.y = pkt.ReadFloat();
				pos.z = pkt.ReadFloat();
				spos.x = pkt.ReadFloat();
				spos.y = pkt.ReadFloat();
				spos.z = pkt.ReadFloat();
				o = pkt.ReadFloat();
				so = pkt.ReadFloat();//corpse orientation
				//	Debug.Log("Movement Update: so="+so);
				
				if(obj)
				{
					obj.SetPosition(pos, o);
				}
				else if(uguid == MainPlayer.GUID)
				{
					pos = new Vector3(pos.x, pos.z, pos.y);
					MonoBehaviour.print("OBJECT_UPDATE_FLAGS.UPDATEFLAG_POSITION  setting position "+pos);
					//MainPlayer.setPos(pos, o);
					player.SetPosition(pos, o);
				}
				else if(aux != null)
				{
					aux.SetPosition(pos,o);
				}
			}
			else
			{
				flag = OBJECT_UPDATE_FLAGS.UPDATEFLAG_HAS_POSITION;
				if((flags & flag) > 0)
				{
					flag = OBJECT_UPDATE_FLAGS.UPDATEFLAG_TRANSPORT;
					if((flags & flag) > 0)
					{
						pos.x = pkt.ReadFloat();
						pos.x = pkt.ReadFloat();
						pos.x = pkt.ReadFloat();
						o = pkt.ReadFloat();
					}
					else
					{
						pos.x = pkt.ReadFloat();
						pos.y = pkt.ReadFloat();
						pos.z = pkt.ReadFloat();
						o = pkt.ReadFloat();
					}
					
					if(obj)
					{
						obj.SetPosition(pos,o);
					}
					else if(uguid == MainPlayer.GUID)
					{
						pos = new Vector3(pos.x,pos.z,pos.y);
						player.SetPosition(pos, o);
						//MainPlayer.setPos(pos,o);
						
					}
					else if(aux != null){
						aux.SetPosition(pos,o);
					}
					
				}
			}
		}
		flag = OBJECT_UPDATE_FLAGS.UPDATEFLAG_LOWGUID;
		if((flags & flag) > 0)
		{
			//MonoBehaviour.print(" MovementUpdate - OBJECT_UPDATE_FLAGS.UPDATEFLAG_LOWGUID "+
			pkt.ReadType(4);
		}
		flag = OBJECT_UPDATE_FLAGS.UPDATEFLAG_HIGHGUID;
		//MonoBehaviour.print("enum flag " + flag);
		if((flags & flag) > 0)
		{
			//MonoBehaviour.print(" MovementUpdate - OBJECT_UPDATE_FLAGS.UPDATEFLAG_HIGHGUID "+
			pkt.ReadType(4);
		}
		flag = OBJECT_UPDATE_FLAGS.UPDATEFLAG_HAS_TARGET;
		if((flags & flag) > 0)
		{
			//MonoBehaviour.print(" MovementUpdate - OBJECT_UPDATE_FLAGS.UPDATEFLAG_HAS_TARGET "+
			pkt.GetPacketGuid();
		}
		flag = OBJECT_UPDATE_FLAGS.UPDATEFLAG_TRANSPORT;
		if((flags & flag) > 0)
		{
			//	MonoBehaviour.print(" MovementUpdate - OBJECT_UPDATE_FLAGS.UPDATEFLAG_TRANSPORT "+
			pkt.ReadType(4);
		}
		flag = OBJECT_UPDATE_FLAGS.UPDATEFLAG_VEHICLE;
		if((flags & flag) > 0)
		{
			//	MonoBehaviour.print(" MovementUpdate - OBJECT_UPDATE_FLAGS.UPDATEFLAG_VEHICLE "+pkt.ReadType(4)+ "  ceva: "+
			pkt.ReadType(4);
			pkt.ReadFloat();
		}
		flag = OBJECT_UPDATE_FLAGS.UPDATEFLAG_ROTATION;
		if((flags & flag) > 0)
		{
			//	MonoBehaviour.print(" MovementUpdate - OBJECT_UPDATE_FLAGS.UPDATEFLAG_ROTATION "+
			pkt.ReadType(8);				
			//pkt.ReadFloat();
		}
	}

	void  QueryObjectInfo (ulong guid){
		//Debug.Log("we query some info about smt...");
		if(guid == MainPlayer.GUID || waitingList.Contains(guid))
		{ // no need to get our infos... we ask when we need
			//MonoBehaviour.print("updating item slots and shit about main player...");
			return;
		}
		SpawnChar spawnChar = getObjToSpawn(guid); // just for objects to be spawned;
		if(spawnChar == null)/// no object
		{
			return;
		}
		//	Debug.Log("QueryObjectInfo: "+(k++)+"  times   Guid:"+guid);
		TYPEID type = spawnChar.getTypeID();
		switch(type)
		{
		case TYPEID.TYPEID_GAMEOBJECT:
			
			//Debug.Log("World: QueryObjectInfo- Entry: "+ob.GetEntry()+" get info!"+"  type:"+type);
			GameObjectTemplate objTemplate = getGameObjectTemplate(spawnChar.GetEntry());
			if(objTemplate != null)
			{
//				SendQueryGameobject(ob.GetEntry(),guid);
//				_GameObject gameObj = spawn(guid, Races.RACE_NONE, Gender.GENDER_MALE) as _GameObject;
//				if(gameObj)
//				{
//					MonoBehaviour.print("---------------------------------recive Gameobject info for entry "+ob.GetEntry());
//					removeGameObjectTemplate(obj.entry);
//					gameObj.InitName(obj.name);
//					return;
//				}
				SpawnGameObject(guid, spawnChar, objTemplate);
			}
			else
			{
				SendQueryGameobject(spawnChar.GetEntry(),guid);
			}
			break;
			
		case TYPEID.TYPEID_PLAYER :
			//MonoBehaviour.print("---------------------------------get Player info for guid "+ob.GetEntry());
			SendQueryPlayerName(guid);
			break;
			
		case TYPEID.TYPEID_UNIT:
			uint entry = spawnChar.GetEntry();
			MobTemplate mt = getMobTemplate(entry);
			if(mt != null)
			{
				/// spawn the Unit
				SpawnNPC(guid, spawnChar, mt);
				return;
			}
			//MonoBehaviour.print("---------------------------------get Unit info for entry "+ob.GetEntry());
			SendQueryCreatureName(guid, entry);
			break;
			
			//case TYPEID.TYPEID_OBJECT :
		case TYPEID.TYPEID_ITEM :
		case TYPEID.TYPEID_CONTAINER:
			//Debug.Log("se cere info: " + ob.guid);
			//	MonoBehaviour.print("---------------------------------get item info for entry "+ob.GetEntry()+"   "+guid);
			if(spawnChar.GetEntry() == 0)
			{
				removeObjectToSpawn(guid);						
			}
			else
			{
				AppCache.sItems.SendQueryItem(spawnChar.GetEntry());
			}					
			//MonoBehaviour.print(ob.GetUInt64Value(UpdateFields.EItemFields.ITEM_FIELD_OWNER) + " e m i l ~ ownership");
			break;
			
		case TYPEID. TYPEID_AREATRIGGER  :
			Debug.Log("%%%%%%%%%%%%%%%%%%%%  Area triggered get info "+guid);
			break;
			
		case TYPEID.TYPEID_CORPSE  :
			/* TYPEID_CORPSE		= 7,
				TYPEID_AIGROUP	   = 8,
				TYPEID_AREATRIGGER   = 9*/
//			ulong auxGUID  = guid & 0xFFFFFF;
//			ulong auxGUID2 = guid & 0xFFFFFFFF;
//			int auxHigh = (int)((guid>>52) & 0xFFF);
//			int auxEntry = (int)((guid>>24) &  0xFFFFFF);
//			MonoBehaviour.print(player.GetEntry()+"   "+MainPlayer.GUID+" Get Corpse info for guid "+ auxGUID+"   "+auxGUID2+"   "+auxHigh + "  "+auxEntry);
			//removeObjectToSpawn(guid);
			CorpseTemplate corpse = GetCorpseTemplate(guid);
			if(corpse != null)
			{
				spawn(guid, Races.RACE_NONE, Gender.GENDER_MALE);
			}
			else
			{
				SendCorpseQuery();
			}
			//Debug.Log("---------------------get corpse info blabla " + guid);   
			break;
			
		default:
			uint entry1 = spawnChar.GetEntry();
			entry1 = ByteBuffer.Reverse(entry1);
			MonoBehaviour.print("unhandled object type : " + type + " and guid " + guid + " with entry " + entry1);
			break;
		}
	}
	
	void  SpawnNPC (ulong npcGuid, SpawnChar script, MobTemplate mt)
	{
		waitingList.Add(npcGuid);
		AssetBundleManager.MobCallBackStruct mobCallBackStruct = new AssetBundleManager.MobCallBackStruct();
		mobCallBackStruct.guid = npcGuid;
		mobCallBackStruct.scale = 1;
		try
		{
			if(mt.modelId[0] == 0)
			{
				throw new Exception();
			}
			
			CreatureDisplayInfoEntry mobDisplay = dbs.sCreatureDisplayInfo.GetRecord(mt.modelId[0]);
			if(mobDisplay.ID == 0)
			{
				throw new Exception();
			}
			mobCallBackStruct.assetPath = "mobs/1";
			mobCallBackStruct.dummy = ApplyNPCModel(mobCallBackStruct);
			
			mobCallBackStruct.scale = mobDisplay.Scale;
			string modelPath = mobDisplay.ModelMesh.Replace("prefabs/", "");
			modelPath = modelPath.Replace("NPC", "Npc");
			modelPath = modelPath.Replace("npc", "Npc");
			mobCallBackStruct.assetPath = "prefabs/"+modelPath;
			GetBundleLoader().LoadNPCBundle(ApplyNPCBundle, mobCallBackStruct);		
		}
		catch(Exception e)
		{
			mobCallBackStruct.assetPath = "mobs/1";
			ApplyNPCModel(mobCallBackStruct);
		}
	}
	
	void  ApplyNPCBundle ( AssetBundleManager.MobCallBackStruct mobCallBackStruct )
	{
		string bundleName = mobCallBackStruct.assetPath;
		GameObject dummy = mobCallBackStruct.dummy;
		if(!dummy)
		{
			Debug.LogError("No have Mob dummy");
			return;
		}
		NPC dummyScript = (dummy.transform).gameObject.GetComponentInChildren<BaseObject>() as NPC;
		string assetBundleName = String.Format ("Resources/{0}_prefab.unity3d", bundleName);
		assetBundleName = assetBundleName.ToLower();
		UnityEngine.Object bundle = null;
		if(AssetBundleManager.instance.currentLoadedBundles.ContainsKey(assetBundleName))
		{
			bundle = AssetBundleManager.instance.currentLoadedBundles[assetBundleName];
		}
		GameObject resourceToInstantiate = null;
		if(bundle == null)
		{
			#if UNITY_EDITOR
			Debug.LogError("cannot load "+assetBundleName);
			#endif
		}
		else
		{
			resourceToInstantiate = bundle as GameObject;
		}
		if(!resourceToInstantiate)
		{
			resourceToInstantiate = GameResources.Load<GameObject>(bundleName);
		}
		GameObject obj;
		if(resourceToInstantiate)
		{
			obj = Instantiate<GameObject>(resourceToInstantiate);
			obj.transform.localScale *= mobCallBackStruct.scale;
		}
		else
		{
			obj = Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/mobs/1"));
		}
		
		BaseObject bo = (obj.transform.Find("obj")).gameObject.GetComponent<BaseObject>();
		if(bo == null)
		{
			bo = (obj.transform.Find("obj")).gameObject.AddComponent<NPC>();
		}
		bo.guid = mobCallBackStruct.guid;
		Quaternion tmp = obj.transform.rotation;
		tmp.eulerAngles = new Vector3(tmp.eulerAngles.x,
		                              dummy.transform.rotation.eulerAngles.y,
		                              tmp.eulerAngles.z);
		obj.transform.rotation = tmp;
		obj.transform.position = dummy.transform.position;
		
		try
		{
			if(bo)
			{
				bo.SetType(dummyScript.GetTypeID());
				bo.CopyValues(dummyScript.GetValues());
				bo.Start();
				bo.moveSpeed = dummyScript.moveSpeed;
				Rigidbody newRigidbody = bo.gameObject.GetComponentInParent<Rigidbody>();
				Rigidbody oldRigidbody = dummyScript.gameObject.GetComponentInParent<Rigidbody>();
				if(newRigidbody != null && oldRigidbody != null)
				{
					string message = newRigidbody.name+" = "+newRigidbody.isKinematic.ToString()+" "+oldRigidbody.name+" = "+oldRigidbody.isKinematic.ToString();
					Debug.Log(message);
					newRigidbody.isKinematic = oldRigidbody.isKinematic;
				}
				bo.auraManager.auras.Clear();
				foreach(Aura ar in dummyScript.auraManager.auras)
				{
					bo.auraManager.auras.Add(ar);
				}
				
				(bo as IBaseObject).CheckLootedFlag();
				if((bo as NPC).isPortalMasterTriggered())//if Unit is a gossip and is not selectable then we have a special npc type and we set  him
				{
					//this npc is a special case because he is gossip and we select gossip menu options using Triggers not by buttons like a normal gossip
					try
					{
						Transform _triggerTransform = bo.TransformParent.Find("trigger_collider");
						if(_triggerTransform)
						{
							GameObject _trigger = _triggerTransform.gameObject;
							//we add portal script and set npc refference
							portal portalComponent = _trigger.AddComponent<portal>();
							if(portalComponent != null)
							{
								portalComponent.npc = (bo as NPC);
							}
							bo.TransformParent.localScale = new Vector3(bo.GetScale(), bo.GetScale(), bo.GetScale());
						}
					}
					catch(Exception exception)
					{
						MonoBehaviour.print("we could not find that child... " + exception.ToString());
					}
				}
				//instantiating minimap BLIP for units (npc and mobs alike)
				if(!(bo as NPC).isCreature()) // for NPCs
				{
					GameObject _minimapBlipM =Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/MiniMap/NPC"));
					_minimapBlipM.transform.parent = bo.TransformParent;
					_minimapBlipM.transform.localPosition = Vector3.zero;//(0, -500, 0);
				}
				else // for MOBs
				{	
					// TODO
				}
				
			}
			else
			{
				Debug.Log("Model has no script attach");
			}	
			AddOrReplaceRealChar(bo);
		}
		catch(Exception exception)
		{
			Debug.Log(mobCallBackStruct.guid+" Object was not instantiated! Unknown TYPEID\n\n " + exception);
			bo = null;
		}
		
		NPC npc = bo as NPC;
		uint entry = bo.GetEntry();
		MobTemplate mt = getMobTemplate(entry);
		npc.InitName(mt.name);
		
		if(npc.isQuestGiver())//if unit is a questgiver we ask for his  status(has ex   new quest, can reward you, quests in progress,quest faild	)
		{
			WorldPacket pkt = new WorldPacket();
			pkt.SetOpcode(OpCodes.CMSG_QUESTGIVER_STATUS_MULTIPLE_QUERY);
			RealmSocket.outQueue.Add(pkt);	
		}
		//		waitingList.Remove(mobCallBackStruct.npcGuid);
		
		npc.movetime = dummyScript.movetime;
		npc.movetimeMax = dummyScript.movetimeMax;
		npc.path = dummyScript.path;
		npc.pathType = dummyScript.pathType;
		npc.targetWay = dummyScript.targetWay;
		npc.timeLeftFotTargetWaypoint = dummyScript.timeLeftFotTargetWaypoint;
		npc.timeFotTargetWaypoint = dummyScript.timeFotTargetWaypoint;
		npc.isSimpleMove = dummyScript.isSimpleMove;
		npc.stepSpeed = dummyScript.stepSpeed;
		npc.startWayPoint = dummyScript.startWayPoint;
		npc.endWayPoint = dummyScript.endWayPoint;
		GameObject.Destroy(dummy);
		ignoreCollisions(mobCallBackStruct.guid);
		npc.FixedUpdate();
	}
	
	GameObject ApplyNPCModel ( AssetBundleManager.MobCallBackStruct mobCallBackStruct )
	{
		string bundleName = mobCallBackStruct.assetPath;
		SpawnChar script = getObjToSpawn(mobCallBackStruct.guid);
		if(script == null)
		{
			return null;
		}
		GameObject resourceToInstantiate = GameResources.Load<GameObject> (string.Format ("prefabs/{0}", bundleName));
		GameObject obj;
		if(resourceToInstantiate)
		{
			obj = Instantiate<GameObject>(resourceToInstantiate);
			obj.transform.localScale *= mobCallBackStruct.scale;
		}
		else
		{
			obj = Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/mobs/1"));
		}
		
		BaseObject bo = (obj.transform.Find("obj")).gameObject.GetComponent<BaseObject>();
		if(bo == null)
		{
			bo = (obj.transform.Find("obj")).gameObject.AddComponent<NPC>();
		}
		bo.guid = mobCallBackStruct.guid;
		
		try
		{
			if(script != null)
			{
				#if UNITY_EDITOR
				Debug.Log("we have a script, copy values: type: "+script.getTypeID());
				#endif
				bo.SetType(script.getTypeID());
				bo.CopyValues(script.getValues());
				
				bo.Start();
				bo.SetPosition(script.pos, script.o);
				bo.moveSpeed = script.moveSpeed;
				Rigidbody newRigidbody = bo.gameObject.GetComponentInParent<Rigidbody>();
				if(newRigidbody != null)
				{
					newRigidbody.isKinematic = true;
				}
				
				bo.auraManager.auras.Clear();
				foreach(Aura ar in script.auras)
				{
					bo.auraManager.auras.Add(ar);
				}
				
				(bo as IBaseObject).CheckLootedFlag();
				if((bo as NPC).isPortalMasterTriggered())//if Unit is a gossip and is not selectable then we have a special npc type and we set  him
				{
					//this npc is a special case because he is gossip and we select gossip menu options using Triggers not by buttons like a normal gossip
					try
					{
						Transform _triggerTransform = bo.TransformParent.Find("trigger_collider");
						if(_triggerTransform)
						{
							GameObject _trigger = _triggerTransform.gameObject;
							//we add portal script and set npc refference
							_trigger.AddComponent<portal>();
							_trigger.GetComponent<portal>().npc = (bo as NPC);
							bo.TransformParent.localScale = new Vector3(bo.GetScale(), bo.GetScale(), bo.GetScale());
						}
					}
					catch(Exception exception)
					{
						MonoBehaviour.print("we could not find that child... " + exception.ToString());
					}
				}
				//instantiating minimap BLIP for units (npc and mobs alike)
				if(!(bo as NPC).isCreature()) // for NPCs
				{
					GameObject _minimapBlipM = Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/MiniMap/NPC"));
					_minimapBlipM.transform.parent = bo.TransformParent;
					_minimapBlipM.transform.localPosition = Vector3.zero;//(0, -500, 0);
				}
				else // for MOBs
				{	
					// TODO
				}
				
				removeObjectToSpawn(mobCallBackStruct.guid);//remove object from spawn object list for not beeing spawned a second time
			}
			else
			{
				Debug.Log("Model has no script attach");
			}	
			AddOrReplaceRealChar(bo);
			ignoreCollisions(mobCallBackStruct.guid);
		}
		catch(Exception exception)
		{
			Debug.Log(mobCallBackStruct.guid+" Object was not instantiated! Unknown TYPEID\n\n " + exception);
			bo = null;
		}
		
		NPC npc = bo as NPC;
		uint entry = script.GetEntry();
		MobTemplate mt = getMobTemplate(entry);
		npc.InitName(mt.name);
		
		if(npc.isQuestGiver())//if unit is a questgiver we ask for his  status(has ex   new quest, can reward you, quests in progress,quest faild	)
		{
			WorldPacket pkt = new WorldPacket();
			pkt.SetOpcode(OpCodes.CMSG_QUESTGIVER_STATUS_MULTIPLE_QUERY);
			RealmSocket.outQueue.Add(pkt);	
		}
		waitingList.Remove(mobCallBackStruct.guid);
		return obj;
	}
	
	public List<CorpseTemplate> corpseList = new List<CorpseTemplate>();	
	public CorpseTemplate GetCorpseTemplate (ulong guid)
	{
		//	Debug.Log("search corpse guid "+guid);
		if(corpseList.Count > 0)
		{
			var corpse= corpseList[0];
			corpseList.RemoveAt(0);
			return corpse;
		}
		
		return null;
		
	}
	
	void  SendCorpseQuery ()
	{
		//Debug.Log("Send MSG_CORPSE_QUERY");
		WorldPacket pkt = new WorldPacket(OpCodes.MSG_CORPSE_QUERY,2);
		RealmSocket.outQueue.Add(pkt);
	}
	
	void  ValuesUpdate (ulong uguid, WorldPacket pkt){
		
		BaseObject bo = null;
		SpawnChar sc = null;
		Item itm = null;
		object obj = null;
		
		TYPE type;
		int valuesCount;
		//if (object.GetTypeIdByGuid(uguid) == TYPEID.TYPEID_ITEM)
		//	Debug.Log("PPPPPPPPPPPPPPPPPPPPPPPPPWLA");
		if(!player)
		{
			getPlayer();
		}
		if(player && (uguid == player.guid))
		{	
			bo = player;
			type = (bo).GetTypeMask();
			valuesCount = 0x52E;
			obj = bo;
			//Debug.Log(valuesCount+" ValuesUpdate - MainPlayer "+(bo).guid+"   type: "+tp);
		}
		else
		{
			bo=GetRealChar(uguid);
			if(bo)
			{
				type = (bo).GetTypeMask();
				valuesCount = bo.GetValuesCount() ;
				obj = bo;
				//Debug.Log(valuesCount+" ValuesUpdate - found an existent mono object "+(bo).guid+"   type: "+tp);
			}
			else
			{
				sc = getObjToSpawn(uguid);
				if(sc != null)
				{
					type = (sc).getTypeMask();
					valuesCount = sc.GetValuesCount() ;
					obj = sc;
					//Debug.Log(valuesCount+" ValuesUpdate - found an unspawn object "+(sc).guid+"   type: "+tp);
				}
				else
				{
					itm = player.itemManager.GetItemByGuid(uguid);
					if(itm != null)
					{
						type = itm.GetTypeMask();
						valuesCount = itm.GetValuesCount() ;
						obj = itm;
						//Debug.Log(valuesCount+" ValuesUpdate - found an item object "+(itm).guid+"   type: "+tp);
					}
					else
					{
						SpawnChar aux = new SpawnChar(uguid, ObjectInfo.GetTypeIdByGuid(uguid));//we create a SpawnObject only to get valuescount base on guid
						valuesCount = aux.GetValuesCount();
						obj = null;
						type  = aux.getTypeMask();
						//Debug.Log(valuesCount+" ValuesUpdate - found an unheadle object "+uguid+"   type: "+tp);
					}
				}
			}
		}	
		
		byte blockCount;
		uint value;
		int maskSize;
		float fvalue;
		ulong value64;
		
		blockCount = pkt.Read();
		if(blockCount == 0)
		{
			MonoBehaviour.print("ValuesUpdate exit");
		}
		
		maskSize = blockCount << 2;
		UpdateMask umask = new UpdateMask();
		uint[] updateMask = new uint[maskSize];
		umask.SetCount(maskSize);	
		
		pkt.Read(updateMask,maskSize);
		
		umask.SetMask(updateMask);
		
		//Debug.Log("valuesCount: "  + valuesCount);
		int weaponField = 0;
		uint _weapon_ranged = 0;
		uint _weapon_main = 0;
		uint _weapon_offhand = 0;
		
		if(bo && (ObjectInfo.GetTypeIdByGuid(uguid) == TYPEID.TYPEID_PLAYER))
		{
			weaponField = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_18_ENTRYID;
			_weapon_ranged = bo.GetUInt32Value(weaponField);
			
			weaponField = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_17_ENTRYID;
			_weapon_offhand = bo.GetUInt32Value(weaponField);

			weaponField = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_16_ENTRYID;
			_weapon_main = bo.GetUInt32Value(weaponField);
		}
		for(uint i = 0; i < valuesCount; i++)
		{
			byte bit = umask.GetBit(i);
			if(bit > 0)
			{
				if(obj != null)
				{
					if(isFloatField(type,i))
					{
						fvalue = pkt.ReadFloat();
						if(bo != null)
						{
							bo.SetFloatValue(i,fvalue);
						}
						else if(sc != null)
						{
							sc.setFloatValue(i,fvalue);
						}
						else if(itm != null)
						{
							itm.SetFloatValue(i,fvalue);
						}
					}
					else if(IsUInt64FieldF(type,i) && umask.GetBit(i+1) > 0)
					{
						value64 = pkt.ReadReversed64bit();//8
						if(bo != null)
						{
							bo.SetUInt64Value(i,value64);
						}
						else if(sc != null)
						{
							sc.setUInt64Value(i, value64);
						}
						else if(itm != null)
						{
							itm.SetUInt64Value(i,value64);
						}
						i++;
					}
					else
					{
						value = pkt.ReadReversed32bit();
						
						if(bo != null)
						{
							bo.SetUInt32Value(i,value);
						}
						else if(sc != null)
						{
							sc.SetUInt32Value(i,value);
						}
						else if(itm != null)
						{
							itm.SetUInt32Value(i,value);
						}
					}
				}
				else
				{
					Debug.Log("Drop pkt");
					pkt.ReadType(4);//drop the value, since the object doesn't exist (always 4 bytes)
				}
			}
			
		}
		if(bo != null || itm != null)
		{
			UpdateObject(uguid, obj); 
		}
		bool  isWeaponChanged = false;
		if(bo != null && (ObjectInfo.GetTypeIdByGuid(uguid) == TYPEID.TYPEID_PLAYER))
		{
			weaponField = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_18_ENTRYID;
			isWeaponChanged |= (_weapon_ranged != bo.GetUInt32Value(weaponField));
			
			weaponField = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_17_ENTRYID;
			isWeaponChanged |= (_weapon_offhand != bo.GetUInt32Value(weaponField));
			
			weaponField = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_16_ENTRYID;
			isWeaponChanged |= (_weapon_main != bo.GetUInt32Value(weaponField));
		}
		if(isWeaponChanged)
		{
			Player playerScript;
			if(uguid == player.guid)
			{
				playerScript = player;
			}
			else
			{
				playerScript = bo.gameObject.GetComponent<OtherPlayer>();
			}
			if(playerScript)
			{
				playerScript.itemManager.DeactivateRange();
				//				playerScript.itemManager.RefreshHands();
			}
		}
		QuestPOIRequest(); //Call for update quest helpers from server
	}
	//============================================= End Updates/OTHER stuff Handlers  =======================================//
	
	void  UpdateObject (ulong guid, object obj)
	{
		TYPEID type = ObjectInfo.GetTypeIdByGuid(guid);
		if (type == TYPEID.TYPEID_OBJECT )
			type = ObjectInfo.GetTypeIdByGuid(ByteBuffer.Reverse(guid) );
		//Debug.Log("ValuesUpdate  "+guid+"  "+type);
		
		switch(type)
		{
		case TYPEID.TYPEID_OBJECT : // do nothing
			if(guid == player.petGuid)
			{
				(obj as NPC).UpdateStats();
			}				
			
			MonoBehaviour.print("Update Object "+guid+" name "+(obj as NPC).transform.parent.name);
			break; 
			
		case TYPEID.TYPEID_ITEM : 
			if(player != null && player.itemManager != null)
			{
				List<Item> items = player.itemManager.Items;
				for(int i = 0; i < items.Count; i++)
				{
					if(items[i].guid == guid)
					{
						if(!AppCache.sItems.HaveRecord(items[i].entry))
						{
							AppCache.sItems.SendQueryItem(items[i].entry);
						}
//							if(items[i].slot >= 20 && items[i].slot <= 23)
//							{
//								Bag newBag = items[i];
//								newBag.initializeBag();
//								newBag.PopulateSlots(player.itemManager);
//								player.itemManager.BaseBags[(newBag.slot-20)] = newBag;
						if (player.gVault != null && player.gVault.showVault)
						{
							player.gVault.refreshI = true;
							player.gVault.UpdateAll();
						}
//								player.itemManager.UpdateVisibleItems();
//							}
//							player.itemManager.UpdateItems();
						items[i].GetItemBasicStats();
						if(items[i].slot >= 0 && items[i].slot <= 19)
						{
							MainPlayer.updateSlots=true;
						}
						if(items[i].slot >= 20 && items[i].slot <= 23)
						{
							//								Button.isNeedRefreshBags=true;//refresh bag and inventory slots
						}
						else
						{
							ButtonOLD.isNeedRefreshInventory=true;//refresh inventory equip slots
						}
						//   Debug.Log("ValuesUpdate "+ "item update stats "+items[i].guid+items[i].name);
						break;
					}
				}
			}
			
			//Debug.Log("ValuesUpdate "+ "item - Done!");
			break;
			
		case TYPEID.TYPEID_CONTAINER :
			Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!! ValuesUpdate "+ "container");
			break;
			
		case TYPEID.TYPEID_UNIT :
			//	Debug.Log("ValuesUpdate "+ "Unit");
			(obj as IBaseObject).CheckLootedFlag();
			(obj as NPC).UpdateStats();
			break;
			
		case TYPEID.TYPEID_PLAYER : // create a player
			
			if(guid == MainPlayer.GUID) 
			{
				//		Debug.Log("ValuesUpdate "+ "Main player");
				MainPlayer.updateSlots=true;
				(obj as MainPlayer).UpdateStats();
				if (ButtonOLD.menuState || ((obj as MainPlayer).gVault != null && (obj as MainPlayer).gVault.showVault))					{
					(obj as MainPlayer).itemManager.UpdateBaseBagItems();
					if(player.gVault != null && player.gVault.showVault)
					{
						player.gVault.UpdateAll();
					}
				}
				// (obj as MainPlayer).itemManager.updateEquippedItemsPosition();
				//					Button.isNeedRefreshBags=true;//refresh bag and inventory slots
				if(player.banker != null && player.banker.isActive)
				{
					player.banker.ClearBankerBags();
					player.banker.ShowBankerBags();
					player.banker.ShowBankerItems();
					player.banker.showType = 0;
					player.banker.Show();
				}
				if(player.inv != null && player.inv.isActive)
				{
					//						player.inv.ClearBags();
					//						player.inv.ShowBags();
					player.inv.ShowItems();
				}
				
				if (player.gVault != null && player.gVault.showVault)
				{
					player.gVault.refreshI = true;
					player.gVault.UpdateAll();
				}
			}
			else /// diferent from our player
			{
				(obj as Player).UpdateStats();
				(obj as OtherPlayer).armory.UpdateArmory(0);
				//	Debug.Log("ValuesUpdate "+ "otherplayer");
				
			}
			break;
			
		case TYPEID.TYPEID_GAMEOBJECT :
			Debug.Log("ValuesUpdate "+ "gameobject "+(obj as _GameObject).name);
			(obj as IBaseObject).CheckLootedFlag();
			break;
			
		case TYPEID.TYPEID_CORPSE :
			Debug.Log("ValuesUpdate "+ "corpse");
			break;
			
		case TYPEID.TYPEID_DYNAMICOBJECT :
			Debug.Log("ValuesUpdate "+ "dynamicobject");
			break;
		}
		
	}
	
	
	//==========================================================================================================//
	//																											//	
	//												Tools Handlers				 								//
	//																											//
	//==========================================================================================================//	
	
	bool isFloatField (TYPE ty, uint f)
	{
		int i;
		TYPE type = TYPE.TYPE_OBJECT;
		if((ty & type) > 0 || ty == 0)
		{
			for(i = 0; IsFloatField.floats_object[i] != IsUInt64Field.minus1UI32; i++)
				if(IsFloatField.floats_object[i] == f)
					return true;
		}
		type = TYPE.TYPE_UNIT;
		if((ty & type) > 0 || ty == 0)
		{
			for(i = 0; IsFloatField.floats_unit[i] != IsUInt64Field.minus1UI32; i++)
				if(IsFloatField.floats_unit[i] == f)
					return true;
		}
		type = TYPE.TYPE_PLAYER;
		if((ty & type) > 0 || ty == 0)
		{
			for(i = 0; IsFloatField.floats_player[i] != IsUInt64Field.minus1UI32; i++)
				if(IsFloatField.floats_player[i] == f)
					return true;
		}
		type = TYPE.TYPE_GAMEOBJECT;
		if((ty & type) > 0 || ty == 0)
		{
			for(i = 0; IsFloatField.floats_gameobject[i] != IsUInt64Field.minus1UI32; i++)
				if(IsFloatField.floats_gameobject[i] == f)
					return true;
		}
		type = TYPE.TYPE_DYNAMICOBJECT;
		if((ty & type) > 0 || ty == 0)
		{
			for(i = 0; IsFloatField.floats_dynobject[i] != IsUInt64Field.minus1UI32; i++)
				if(IsFloatField.floats_dynobject[i] == f)
					return true;
		}
		/*type = TYPE.TYPE_CORPSE;
		if((ty & type) > 0)
		{
			for(i = 0; IsFloatField.floats_dynobject[i] != IsUInt64Field.minus1UI32; i++)
				if(IsFloatField.floats_dynobject[i] == f)
					return true;
		}
		type = TYPE.TYPE_CONTAINER;
		if((ty & type) > 0)
		{
			for(i = 0; IsFloatField.floats_dynobject[i] != IsUInt64Field.minus1UI32; i++)
				if(IsFloatField.floats_dynobject[i] == f)
					return true;
		 }*/					
		return false;
	}
	
	bool IsUInt64FieldF (TYPE ty, uint f)
	{
		TYPE type = TYPE.TYPE_OBJECT;
		int i;
		if((ty & type) > 0)
		{
			for(i = 0; IsUInt64Field.u64_object[i] != IsUInt64Field.minus1UI32; i++)
				if(IsUInt64Field.u64_object[i] == f)
					return true;
		}
		type = TYPE.TYPE_ITEM;
		if((ty & type) > 0)
		{
			for(i = 0; IsUInt64Field.u64_item[i] != IsUInt64Field.minus1UI32; i++)
				if(IsUInt64Field.u64_item[i] == f)
					return true;
		}
		/*type = TYPE.TYPE_CONTAINER;
		if((ty & type) > 0)
		{
			for(i = 0; IsUInt64Field.u64_container[i] != IsUInt64Field.minus1UI32; i++)
				if(IsUInt64Field.u64_container[i] == f)
					return true;
		}
		type = TYPE.TYPE_UNIT;*/
		if((ty & type) > 0)
		{
			for(i = 0; IsUInt64Field.u64_unit[i] != IsUInt64Field.minus1UI32; i++)
				if(IsUInt64Field.u64_unit[i] == f)
					return true;
		}
		type = TYPE.TYPE_PLAYER;
		if((ty & type) > 0)
		{
			for(i = 0; IsUInt64Field.u64_player[i] != IsUInt64Field.minus1UI32; i++)
				if(IsUInt64Field.u64_player[i] == f)
					return true;
		}
		type = TYPE.TYPE_GAMEOBJECT;
		if((ty & type) > 0)
		{
			for(i = 0; IsUInt64Field.u64_gameobject[i] != IsUInt64Field.minus1UI32; i++)
				if(IsUInt64Field.u64_gameobject[i] == f)
					return true;
		}
		type = TYPE.TYPE_DYNAMICOBJECT;
		if((ty & type) > 0)
		{
			for(i = 0; IsUInt64Field.u64_dynobject[i] != IsUInt64Field.minus1UI32; i++)
				if(IsUInt64Field.u64_dynobject[i] == f)
					return true;
		}
		type = TYPE.TYPE_CORPSE;
		if((ty & type) > 0)
		{
			for(i = 0; IsUInt64Field.u64_corpse[i] != IsUInt64Field.minus1UI32; i++)
				if(IsUInt64Field.u64_corpse[i] == f)
					return true;
		}
		return false;
	}
	
	void  getPlayer ()
	{
		//=============================
		// MainPlayer ref its beening set in swipe script-> SpawnMainChar
		//=============================
		
		/*
		GameObject go = GameObject.Find("player");
		if(go)
			player = go.GetComponentInChildren<MainPlayer>();
		*/
		if(player)
		{
			player.session = this;
		}	
		/// make a request of data
		/*WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_REQUEST_PARTY_MEMBER_STATS);
		pkt.Append(ByteBuffer.Reverse(player.guid));
		RealmSocket.outQueue.Add(pkt);*/
	}
	
	/// <summary>
	/// Gets the real char.
	/// </summary>
	/// <returns>The real char.</returns>
	/// <param name="guid">GUID.</param>
	static public BaseObject GetRealChar (ulong guid)
	{
		BaseObject plr = null;
		if(player.guid == guid)
		{
			plr = player;
		}
		else
		{
			realChars.TryGetValue(guid, out plr);
		}
		return plr;
	}
	
	SpawnChar getObjToSpawn (ulong guid)
	{
		SpawnChar ret = null;
		objectsToSpawn.TryGetValue(guid, out ret);
		return ret;
	}
	
	public BaseObject GetPlayerByName (string objName)
	{
		//Debug.Log("GetObjByName: " + objName);
		BaseObject[] chars = new BaseObject[realChars.Count];
		realChars.Values.CopyTo(chars, 0);
		BaseObject ret = null;
		foreach(BaseObject plr in chars)
		{
			if(plr.IsPlayer())
			{
				if((plr as Unit).Name.ToLower() == objName.ToLower())
				{
					//Debug.Log("Found player!");
					ret = plr;
				}
			}
		}
		//Debug.Log("Player not found!");
		return ret;
	}
	
	public MobTemplate getMobTemplate (uint entry)
	{
		uint i=0;
		foreach(MobTemplate mob in templates)
		{
			if(mob.entry==entry)
			{	//Templates.RemoveAt(i);
				return mob;
			}
			i++;	
		}
		return null;
	}
	
	static public GameObjectTemplate getGameObjectTemplate (uint entry)
	{
		GameObjectTemplate gameObj = null;
		gameObjectTemplates.TryGetValue(entry, out gameObj);
		return gameObj;
	}
	
	void  AddOrReplaceRealChar (BaseObject mb)
	{
		realChars[mb.guid] = mb;
	}
	
	public bool  removeObjectToSpawn (TYPEID _type)
	{
		List<ulong> keysForRemove = new List<ulong>();
		foreach(SpawnChar plr in objectsToSpawn.Values)
		{
			if(plr._typeID == _type)
			{
				keysForRemove.Add (plr.guid);
			}
		}
		foreach(ulong key in keysForRemove)
		{
			objectsToSpawn.Remove(key);
		}
		return false;		
	}
	
	bool removeObjectToSpawn (ulong guid)
	{
		bool ret = false;
		if(objectsToSpawn.ContainsKey(guid))
		{
			objectsToSpawn.Remove(guid);
			ret = true;
		}
		return ret;
	}
	
	bool remove (ulong guid)
	{
		TYPEID type = ObjectInfo.GetTypeIdByGuid(guid);
		switch(type)
		{
		case TYPEID.TYPEID_ITEM:	
		case TYPEID.TYPEID_CONTAINER:	
			//Debug.Log("Trying to remove item  with guid "+guid);			
			//handles all items the server says to destroy; 
			if (player.itemManager.RemoveItem(guid))
			{
				return true;
			}	
			break;
		case TYPEID.TYPEID_OBJECT:
		case TYPEID.TYPEID_PLAYER:
		case TYPEID.TYPEID_CORPSE:
		case TYPEID.TYPEID_GAMEOBJECT:
		case TYPEID.TYPEID_UNIT:
			if(realChars.ContainsKey(guid))
			{
				BaseObject plr = null;
				realChars.TryGetValue(guid, out plr);
				if(plr != null && plr.gameObject != null)
				{
					UnityEngine.Object.Destroy(plr.TransformParent.gameObject);
				}
				realChars.Remove(guid);
			}
			break;
		default:
			Debug.Log("Destroy unknown object" + type);
			break;
		}
		return true;
	}
	
	void  HandleEmoteOpcode (WorldPacket pkt)
	{
		uint emoteId = pkt.ReadReversed32bit();
		ulong guid = pkt.ReadReversed64bit();
		//player.print("play emote " + emoteId + " for mob/player with guid " + guid);
	}	
	//============================================= End Tools Handlers  =======================================//

	//==========================================================================================================//
	//																											//	
	//												CHAT Handlers				 								//
	//																											//
	//==========================================================================================================//	
	void  HandleNotificationOpcode (WorldPacket pkt)
	{
		//string notification;
		//notification = pkt.ReadString();
		Message ms = new Message();
		ms.message = pkt.ReadString();
		player.AddChatInput(ms);
	}
	
	bool  DEBUGBOOL = false;
	
	void  HandleChatMessageOpcode (WorldPacket pkt)
	{
		ChatMsg type;
//		byte chatTag;
		uint lang;
		ulong source_guid = 0;
		ulong source_guid2 = 0;
		ulong listener_guid = 0;
//		uint msglen;
		uint unk32;
		uint source_name_len = 0;
		uint listener_name_len = 0;
		string source_name = string.Empty;
//		string listener_name;
//		string msg;
		string channel = "";
		Unit MPlayer;
		
		type = (ChatMsg)pkt.Read();
		//source_name = Global.getMessageTypeAsString(type );
		lang = (uint)pkt.ReadType(4);   // ?? pkt.ReadReversed32bit()  - TO TEST
		
		if(lang == 0xFFFFFFFF)//LANG_ADDON)// && GetInstance()->GetConf()->skipaddonchat)
			return;
		Message ms = new Message();
		source_guid = pkt.ReadReversed64bit();
		unk32 = (uint)pkt.ReadType(4);
		//string chatType;
		
		switch(type)
		{
		case ChatMsg.CHAT_MSG_MONSTER_SAY:
		case ChatMsg.CHAT_MSG_MONSTER_PARTY:
		case ChatMsg.CHAT_MSG_MONSTER_YELL:
		case ChatMsg.CHAT_MSG_MONSTER_WHISPER:
		case ChatMsg.CHAT_MSG_MONSTER_EMOTE:
		case ChatMsg.CHAT_MSG_RAID_BOSS_WHISPER:
		case ChatMsg.CHAT_MSG_RAID_BOSS_EMOTE:
		case ChatMsg.CHAT_MSG_BN:
			source_name_len = (uint)pkt.ReadType(4);
			source_name = pkt.ReadString();
			// MaNGOS sends nothing for these, not used
			listener_guid = pkt.ReadType(8);//recvPacket >> listener_guid; // always 0
			if(listener_guid > 0)// && !IS_PLAYER_GUID(listener_guid))
			{
				listener_name_len = (uint)pkt.ReadType(4); // always 1 (\0)
				pkt.Read();//recvPacket >> listener_name; // always \0
			}
			Debug.Log("+++++++**********&&&&&^^^^^ celalalt Canalul este :" + ms.channel);
			break;
			
		default:
			if(type == ChatMsg.CHAT_MSG_CHANNEL)
			{
				channel = pkt.ReadString();
				//source_guid2 = pkt.ReadType(8); // no idea why it is sent twice
				ms.channel = channel;
				//Debug.Log("+++++++**********&&&&&^^^^^Canalul este :" + ms.channel);
			}
			else
				ms.channel = Global.GetMessageTypeAsString(type );
			//else 
			//source_name = pkt.ReadString();
			source_guid2 = pkt.ReadType(8);
			DEBUGBOOL = false;
			MPlayer = GetRealChar(source_guid) as Unit;
			
			//Debug.Log("NUME: " + MPlayer.name);
			if (MPlayer != null )
			{
				source_name = MPlayer.name;
			}
			//else
			//if (MainPlayer.GUID == source_guid )
			//{
			//	source_name = player.name;
			//}
			else
			{
				if (ObjectInfo.GetTypeIdByGuid(source_guid) == TYPEID.TYPEID_PLAYER )  // if guid is player
				{
					waitingMessages.PushMessageToWaitingList(pkt,source_guid, ms, type, lang, source_name);
					SendQueryPlayerName(source_guid );
					return;
				}
				
			}
			//else
			//	source_name = "Me";
			break;
		}
		SendMessageFromChatMessagePacket(pkt, ms, type, lang, source_name);
//		switch(type)
//		{
//			case ChatMsg.CHAT_MSG_YELL :
//				ms.messageColor = Color(0,1,0);
//				//chatType = "Y ";
//			break;
//			case ChatMsg.CHAT_MSG_WHISPER :
//				ms.messageColor = Color(1,0,1);
//				ms.type = ChatMsg.CHAT_MSG_WHISPER;
//				//chatType = "W ";
//			break;
//		}
//		msglen = pkt.ReadType(4);
//		msg = pkt.ReadString();
// 		//MonoBehaviour.print(msg);
//		chatTag = pkt.Read();
//		if(player)
//		{
//			ms.sender = source_name;
//			ms.message = msg;
//			player.AddChatInput(ms);
//			//if(source_name!="")
//			//	player.AddChatInput(source_name+":"+chatType+msg);
//			//else player.AddChatInput(chatType+msg);
//		}
	}
	
	void  SendMessageFromChatMessagePacket ( WorldPacket pkt, Message ms, ChatMsg type, uint lang, string source_name )
	{
		byte chatTag;
		switch(type)
		{
		case ChatMsg.CHAT_MSG_YELL :
			ms.messageColor = new Color(0,1,0);
			//chatType = "Y ";
			break;
		case ChatMsg.CHAT_MSG_WHISPER :
			ms.messageColor = new Color(1,0,1);
			ms.type = ChatMsg.CHAT_MSG_WHISPER;
			//chatType = "W ";
			break;
		}
		uint msglen = pkt.ReadReversed32bit();
		string msg = pkt.ReadChatString(msglen);
		//MonoBehaviour.print(msg);
		chatTag = pkt.Read();
		if(player)
		{
			ms.sender = source_name;
			ms.message = msg;
			player.AddChatInput(ms);
			/*if(source_name!="")
				player.AddChatInput(source_name+":"+chatType+msg);
			else player.AddChatInput(chatType+msg);*/
		}
	}
	
	void  HandleChannelNotify (WorldPacket pkt){
		ChatNotify notifyType = (ChatNotify)pkt.Read();
		string ChannelName = pkt.ReadString();
		ulong guid;// not used yet
		//if(notifyType == ChatNotify.)
		Message ms = new Message();
		uint channelId;
		string name;
		//if (!player.ChannelM.CheckIfChannelIsLocal(ChannelName ) )   // ignore all channel notifications that aren't in your list
		//	return;
		switch(notifyType)
		{
		case ChatNotify.CHAT_INVITE_WRONG_FACTION_NOTICE:
			ms.message = "you invited a player from another faction";
			break;
		case ChatNotify.CHAT_WRONG_FACTION_NOTICE: //semes to never been sent
			//ms.message = "you invited a player from another faction";
			break;
		case ChatNotify.CHAT_INVALID_NAME_NOTICE: // same
			break;
		case ChatNotify.CHAT_NOT_MEMBER_NOTICE:
			ms.message = "you are no longer a member of the channel: "+ChannelName;
			player.ChannelM.DeleteChannel(ChannelName );
			player.ChannelM.SaveChannelsToPlayerPrefs(player.name);
			break;
		case ChatNotify.CHAT_NOT_MODERATED_NOTICE: //not sent
			break;
		case ChatNotify.CHAT_NOT_IN_LFG_NOTICE:
			ms.message = ChannelName + " is not a lfg channel";
			break;
		case ChatNotify.CHAT_NOT_IN_AREA_NOTICE: //not sent
			break;
		case ChatNotify.CHAT_THROTTLED_NOTICE: //not sent
			break;
		case ChatNotify.CHAT_BANNED_NOTICE:
			ms.message = "you are banned on channel: "+ ChannelName;
			break;
		case ChatNotify.CHAT_MUTED_NOTICE:
			ms.message = "you can not write on channel: " + ChannelName;
			break;
		case ChatNotify.CHAT_NOT_OWNER_NOTICE:
			ms.message = "you are not the owner of the channel: " + ChannelName;
			break;
			//
		case ChatNotify.CHAT_JOINED_NOTICE:
			BaseObject newPlayer = GetRealChar(pkt.ReadReversed64bit());
			if(newPlayer)
				ms.message = (newPlayer as Player).Name + " has joined the channel: " + ChannelName;
			else
				ms.message = "A player has joined the channel: " + ChannelName;
			break;
		case ChatNotify.CHAT_LEFT_NOTICE:
			//ms.message = "a player has left the channel: " + ChannelName;
			//guid = pkt.ReadType(8);/////////// not used yet
			break;
		case ChatNotify.CHAT_YOU_JOINED_NOTICE:
			byte flags = pkt.Read();
			channelId = pkt.ReadReversed32bit();
			channel ch = player.ChannelM.getChannel(ChannelName);
			ch.id = channelId;
			ch.SetActiveRecursively();
			pkt.ReadType(4);//// just a 0 value;
			ms.message = "you have joined the channel: " + ChannelName + " id " + channelId;
			MainPlayer.LastChannel = "/" + channelId;	// channel joined saved to last channel
			player.ChannelM.SaveChannelsToPlayerPrefs(player.name);  // save channels localy
			break;
		case ChatNotify.CHAT_YOU_LEFT_NOTICE:
			ms.message = "you have left the channel: " + ChannelName;
			channelId = pkt.ReadReversed32bit();/////////////////// use this when u need to remove the channel from the channel list
			pkt.Read();/// just a 0 value;
			player.ChannelM.DeleteChannel(ChannelName );
			player.ChannelM.SaveChannelsToPlayerPrefs(player.name);  // save channels localy
			break;
		case ChatNotify.CHAT_PASSWORD_CHANGED_NOTICE:
			ms.message = "the passowrd for channel " + ChannelName + " has been changed";
			guid = pkt.ReadReversed64bit();
			break;
		case ChatNotify.CHAT_OWNER_CHANGED_NOTICE:
			ms.message = ChannelName + " channel has a new leader";
			guid = pkt.ReadReversed64bit();///////// use this to identify the new leader 
			break;
		case ChatNotify.CHAT_PLAYER_NOT_FOUND_NOTICE:
			name = pkt.ReadString();
			ms.message = "player " + name + " has not been found";
			break;
		case ChatNotify.CHAT_CHANNEL_OWNER_NOTICE:
			name = pkt.ReadString();
			ms.message = "the owner of the channel " + ChannelName + " is " + name;
			break;
		case ChatNotify.CHAT_MODE_CHANGE_NOTICE: ////////////////////// not interested yet
			break;
		case ChatNotify.CHAT_ANNOUNCEMENTS_ON_NOTICE: ////////////////////// not interested yet
			break;
		case ChatNotify.CHAT_ANNOUNCEMENTS_OFF_NOTICE: ////////////////////// not interested yet
			break;
		case ChatNotify.CHAT_MODERATION_ON_NOTICE: ////////////////////// not interested yet
			break;
		case ChatNotify.CHAT_MODERATION_OFF_NOTICE: ////////////////////// not interested yet
			break;
			///
		case ChatNotify.CHAT_PLAYER_KICKED_NOTICE: // a player have benn kicked from this channel
			break;
		case ChatNotify.CHAT_PLAYER_BANNED_NOTICE: // a player have benn banned from this channel
			break;
		case ChatNotify.CHAT_PLAYER_UNBANNED_NOTICE: // a player have been unbanned from this channel
			break;
		case ChatNotify.CHAT_PLAYER_NOT_BANNED_NOTICE: // not sent
			break;
		case ChatNotify.CHAT_PLAYER_ALREADY_MEMBER_NOTICE:
			// the player is allrady on this channel 
			break;
		case ChatNotify.CHAT_INVITE_NOTICE: ///
			ms.message = "you have been invited to join " + ChannelName + " channel";
			break;
		case ChatNotify.CHAT_PLAYER_INVITED_NOTICE: // you have invited "name" to join the channel
			break;
		case ChatNotify.CHAT_PLAYER_INVITE_BANNED_NOTICE: // not sent
			break;
		}
		//	MonoBehaviour.print(ms.message);
		if(ms.message!="")
			player.AddChatInput(ms);
	}
	
	//============================================= End Chat Handlers  =======================================//

	
	//==========================================================================================================//
	//																											//	
	//												Items/Inventory/Equipe Handlers								//
	//																											//
	//==========================================================================================================//	
	
	void  HandleItemLocationOpcode (WorldPacket pkt){
		ulong guid = pkt.ReadReversed64bit();
		//player.print("new item position for item "+guid);
		
		bool  found = false;
		byte bag = pkt.Read();
		byte slot = pkt.Read();
		slot++;
		foreach(Item item in player.itemManager.Items)
		{
			if(item.guid == guid)
			{
				if(bag == 255)
				{
					bag = 0;// default bag
				}
				//else
				//	Debug.Log("GUIDUL: "+guid+"Baggggggg :"+ bag+" Slot : " + slot);
				
				item.SetSlot(bag,slot);
				//Debug.Log("Baggggggg :"+ item.getBag()+" Slot : " + item.getSlot());
				player.itemManager.SetPositionForItem(item);
				
				if(slot>0 && slot<18)
				{
					//			  		player.inventoryMessage="Item equip succesfull!";
					InterfaceManager.Instance.SetInventoryMessage("Item equip succesfull!");
				}
				
				found = true;
				break;
			}
			
		}
		if(!found)
			Debug.Log("Baggggggg :"+ bag+" Slot : " + slot+" Dar nu a fost gasit");
	}
	
	void  HandleItemPushResult (WorldPacket pkt){   
		//this gives us extra informations	about 
		Debug.Log("********************* HandleItemPushResult");
		
		ulong ownerGuid = pkt.ReadReversed64bit();//
		uint recived = pkt.ReadReversed32bit();
		uint created = pkt.ReadReversed32bit();
		uint nomeroUno = pkt.ReadReversed32bit();
		byte bag = pkt.Read();
		uint slot = pkt.ReadReversed32bit();
		uint entry = pkt.ReadReversed32bit();
		uint itemSuffix = pkt.ReadReversed32bit();
		uint itemRandomProp = pkt.ReadReversed32bit();
		uint count = pkt.ReadReversed32bit();
		uint inventoryCount = pkt.ReadReversed32bit();
		
		if(bag==255)
			bag=0;
		
		Debug.Log("HandleItemPushResult - guid: "+ ownerGuid +" Item entry "+entry+" bag "+bag+" slot "+slot);
		Debug.Log("HandleItemPushResult - recived: "+ recived +" created "+ created +" nomeroUno "+ nomeroUno);
		Debug.Log("HandleItemPushResult - itemSuffix: "+ itemSuffix +" itemRandomProp "+ itemRandomProp +" count "+ 
		          count+" inventoryCount "+inventoryCount);
		if(!AppCache.sItems.HaveRecord(entry))
		{
			AppCache.sItems.SendQueryItem(entry);
		}
	}
	
	void  HandleItemQuerySingleResponseOpcode (WorldPacket pkt)
	{
		//Debug.Log("se face item query info plm...");
		uint itemEntryID = pkt.ReadReversed32bit();
		if((itemEntryID & 0x80000000) > 0)
		{
			//MonoBehaviour.print("World: SMSG_ITEM__QUERY_SINGLE_RESPONSE- Entry: "+itemEntryID+" Missing gameobject info!");
			return;
		}
		byte i;

		//Item item = new Item();
		ulong guidItem ;
		Item item = null;
		
		try
		{
			ulong keyForRemove = 0;
			foreach(SpawnChar sc in objectsToSpawn.Values)
			{
				if(sc.GetEntry() == itemEntryID)
				{
					guidItem = sc.guid;
					if(sc._typeID == TYPEID.TYPEID_ITEM)
					{
						item = new Item();
						item.guid = guidItem;
						item.SetType(TYPEID.TYPEID_ITEM); // what's the point ?
					}
					else if(sc._typeID == TYPEID.TYPEID_CONTAINER)
					{
						item = new Bag();
						item.guid = guidItem;
						item.SetType(TYPEID.TYPEID_CONTAINER); // is this correct ?
					}
					item.CopyValues(sc.getValues());
					keyForRemove = guidItem;		
					break;
				}
			}
			if(keyForRemove != 0)
			{
				objectsToSpawn.Remove(keyForRemove);
			}
		}
		catch(Exception e)
		{
			Debug.Log(e);
		}
		//Debug.Log("Primim informatii despre item"+item.bag+" "+item.slot);
		// find the player
		
		try
		{
			if(item == null)
			{
				item = new Item();
			}
			item.entry = itemEntryID;
			
			item.classType = (ItemClass)pkt.ReadReversed32bit();
			item.subClass = (int)pkt.ReadReversed32bit();
			item.unk32 = pkt.ReadInt();
			item.name = pkt.ReadString();
			//MonoBehaviour.print("World: SMSG_ITEM_QUERY_SINGLE_RESPONSE- entry: "+itemEntryID+" We found the item! Name: "+item.name);
			/// 3 subnames with value ""
			pkt.ReadString();
			pkt.ReadString();
			pkt.ReadString(); // 3 empty strings
			
			item.displayInfoID = pkt.ReadReversed32bit();
			item.quality = (ItemQualities)pkt.ReadReversed32bit();
			item.flags = pkt.ReadReversed32bit();
			item.flags2 = pkt.ReadReversed32bit();
			item.buyPrice = pkt.ReadReversed32bit();
			item.sellPrice = pkt.ReadReversed32bit();
			item.inventoryType = (InventoryType)pkt.ReadReversed32bit();
			item.allowableClass = pkt.ReadReversed32bit();
			item.allowableRace = pkt.ReadReversed32bit();
			item.itemLvl = pkt.ReadReversed32bit();
			item.reqLvl = pkt.ReadReversed32bit();
			item.reqSkill = pkt.ReadReversed32bit();
			item.reqSkillRank = pkt.ReadReversed32bit();
			item.reqSpell = pkt.ReadReversed32bit();
			item.reqHonorRank = pkt.ReadReversed32bit();
			item.reqCityRank = pkt.ReadReversed32bit();
			item.reqRepFaction = pkt.ReadReversed32bit();
			item.reqRepRank = pkt.ReadReversed32bit();
			item.maxCount = pkt.ReadInt();
			item.stackable = pkt.ReadInt();
			item.containerSlots = pkt.ReadReversed32bit();
			item.statsCount = pkt.ReadReversed32bit();
			//	Debug.Log("ITEMS ITEMS  ++++++++++++++++++++++++++++++++++	  "+ item.statsCount);
			for (i = 0;i<item.statsCount;i++)
			{
				item.itemStats[i] = new ItemStat();
				item.itemStats[i].itemStatType = (ItemModType)pkt.ReadReversed32bit();
				item.itemStats[i].itemStatValue = pkt.ReadInt();
				
			}
			
			item.scallingStatDistribution = pkt.ReadReversed32bit();
			item.scalingStatValue = pkt.ReadReversed32bit();
			for(i = 0;i<2;i++)// 2 = MAX_ITEM_PROTO_DAMAGS
			{
				item.damage[i] = new ItemDamage();
				item.damage[i].damageMin = pkt.ReadFloat();
				item.damage[i].damageMax = pkt.ReadFloat();
				item.damage[i].damageType = pkt.ReadReversed32bit();
			}
			
			item.armour = pkt.ReadReversed32bit();
			item.holyRes = pkt.ReadReversed32bit();
			item.fireRes = pkt.ReadReversed32bit();
			item.natureRes = pkt.ReadReversed32bit();
			item.frostRes = pkt.ReadReversed32bit();
			item.shadowRes = pkt.ReadReversed32bit();
			item.arcaneRes = pkt.ReadReversed32bit();
			item.delay = pkt.ReadReversed32bit();
			item.ammoType = pkt.ReadReversed32bit();
			item.rangedModRange = pkt.ReadFloat();
			int enchantEffect = (int)__SpellEffects.SPELL_EFFECT_ENCHANT_ITEM;
			int enchantTemporaryEffect = (int)__SpellEffects.SPELL_EFFECT_ENCHANT_ITEM_TEMPORARY;
			int enchantPrismaticSlotEffect = (int)__SpellEffects.SPELL_EFFECT_ADD_SOCKET;
			for(i = 0;i<5;i++)
			{
				item.spells[i] = new ItemSpell();
				item.spells[i].ID = pkt.ReadReversed32bit();
				if(item.spells[i].ID > 0)
				{
					try
					{
						SpellManager.CreateOrRetrive(item.spells[i].ID);
					}
					catch(Exception crashMsg)
					{
						Debug.LogError("Unknown spell = "+item.spells[i].ID);
					}
				}
				item.spells[i].trigger = pkt.ReadReversed32bit();
				item.spells[i].charges = pkt.ReadReversed32bit();
				item.spells[i].cooldown = pkt.ReadReversed32bit();
				item.spells[i].category = pkt.ReadInt();
				item.spells[i].categoryCooldown = pkt.ReadInt();
				
				Spell spell = SpellManager.GetSpell(item.spells[i].ID);
				if(spell != null && spell.Category == 0)
				{
					Cooldown cooldown = player.cooldownManager.GetCooldown(spell);
					if(cooldown != null)
					{
						Cooldown newCooldown;
						if(item.spells[i].category > 0)
						{
							newCooldown = new Cooldown(cooldown.currentTime, item.spells[i].categoryCooldown / 1000.0f);
						}
						else
						{
							newCooldown = new Cooldown(cooldown.currentTime, item.spells[i].cooldown / 1000.0f);
						}
						player.cooldownManager.playerCooldownList.SetCooldown(spell, newCooldown);
					}
					
					if(spell.Effect[0] == enchantEffect || spell.Effect[1] == enchantEffect || spell.Effect[2] == enchantEffect)
					{
						item.isPermanentEnchant = true;
					}
					if(spell.Effect[0] == enchantTemporaryEffect
					   || spell.Effect[1] == enchantTemporaryEffect
					   || spell.Effect[2] == enchantTemporaryEffect)
					{
						item.isTemporaryEnchant = true;
					}
					if(spell.Effect[0] == enchantPrismaticSlotEffect
					   || spell.Effect[1] == enchantPrismaticSlotEffect
					   || spell.Effect[2] == enchantPrismaticSlotEffect)
					{
						item.isPrismaticSlotEnchant = true;
					}
				}
			}
			
			item.bonding = pkt.ReadReversed32bit();
			item.description = pkt.ReadString();
			item.pageText = pkt.ReadReversed32bit();
			item.languageID = pkt.ReadReversed32bit();
			item.pageMaterial = pkt.ReadReversed32bit();
			item.startQuest = pkt.ReadReversed32bit();
			item.lockID = pkt.ReadReversed32bit();
			item.material = pkt.ReadInt();
			item.sheath = pkt.ReadReversed32bit();
			item.randomProperty = pkt.ReadReversed32bit();
			item.randomSuffix = pkt.ReadReversed32bit();
			item.block = pkt.ReadReversed32bit();
			item.itemSet = pkt.ReadReversed32bit();
			item.maxDurability = pkt.ReadReversed32bit();
			item.area = pkt.ReadReversed32bit();
			item.map = pkt.ReadReversed32bit();
			item.bagFamily = pkt.ReadReversed32bit();
			item.totemCategory = pkt.ReadReversed32bit();
			for (i = 0;i<3;i++)
			{
				item.socket[i] = new ItemSocket();
				item.socket[i].color = pkt.ReadReversed32bit();
				item.socket[i].content = pkt.ReadReversed32bit();
			}
			item.socketBonus = pkt.ReadReversed32bit();
			item.gemProperties = pkt.ReadReversed32bit();
			item.reqDisenchantSkill = pkt.ReadInt();
			item.armourDamageModifier = pkt.ReadFloat();
			item.duration = pkt.ReadReversed32bit();
			item.itemLimitCategory = pkt.ReadReversed32bit();
			item.holidayId = pkt.ReadReversed32bit();
			
		}
		catch(Exception exception)
		{
			Debug.Log("exceptie in citire item: " + exception);
		}
		
		ulong itemOwnerGUID = 0;
		try
		{
			itemOwnerGUID = item.GetUInt64Value((int)UpdateFields.EItemFields.ITEM_FIELD_OWNER);//6
		}
		catch(Exception exception)
		{
			Debug.Log("Error reading information above itemOwnerGUID: " + exception);
		}
		
		if(!AppCache.sItems.HaveRecord(itemEntryID))
		{
			AppCache.sItems.AddRecord(item);
		}
		
		/**
		 *	Asnwer from server about request loot
		 */
		if(player.lootManager.listRequestedLootItems.Contains(item.entry))
		{
			player.lootManager.listRequestedLootItems.Remove(item.entry);
			if(player.lootManager.needState)
			{
				player.lootManager.RefreshRollLoot();
			}
			else
			{
				player.lootManager.RefreshLoot();
			}
		}
		
		if(itemOwnerGUID != 0)
		{
			if(itemOwnerGUID == player.guid) // add it if it's my item // usualy it is mine
			{
				Item oldItem = player.itemManager.GetItemByGuid(item.guid);
				if(oldItem != null && oldItem.entry != 0)
				{
					player.itemManager.Items.Remove(oldItem);
				}
				player.itemManager.AddItem(item);
				
				player.itemManager.SetPositionForItem(item);
				
				if(item.bag==0 && item.slot>0 && item.slot<19)
				{	
					if (_firstVisit == 1)
					{
						//						player.inventoryMessage="Item equip succesfull!";
						InterfaceManager.Instance.SetInventoryMessage("Item equip succesfull!");
					}
					
					ButtonOLD.isNeedRefreshInventory = true;
				}
				
				if(player.gVault != null && player.gVault.showVault)
				{
					player.gVault.refreshI = true;
					player.gVault.UpdateAll();
				}
			}
			else
			{
				OtherPlayer otherPlayer = null;
				BaseObject baseOtherPlayer = GetRealChar(itemOwnerGUID);
				if(baseOtherPlayer != null)
				{
					otherPlayer = baseOtherPlayer.gameObject.GetComponent<OtherPlayer>();
				}
				if(otherPlayer != null)
				{
					Item otherPlayerItemOld = otherPlayer.itemManager.GetItemByGuid(item.guid);
					if(otherPlayerItemOld.entry != 0)
					{
						otherPlayer.itemManager.Items.Remove(otherPlayerItemOld);
					}
					otherPlayer.itemManager.AddItem(item);
					
					//					ushort otherPlayerBagSlot = player.itemManager.getBagAndSlot(item.guid);
					//					item.bag = otherPlayerBagSlot >> 8;
					//					item.slot = otherPlayerBagSlot & 255;
					//Debug.Log("SMSG_ITEM_QUERY_SINGLE_RESPONSE  ITEM: " + item.entry + " bag: " + item.bag + " slot: " + item.slot);
					otherPlayer.itemManager.SetPositionForItem(item);
					
					//					if(item.bag==0 && item.slot>0 && item.slot<18)
					//					{	
					//						if (_firstVisit == 1)
					//						{
					//							player.inventoryMessage="Item equip succesfull!";
					//							InterfaceManager.Instance.SetInventoryMessage("Item equip succesfull!");
					//						}
					//				  		Button.isNeedRefreshInventory = true;
					//					}
				}
			}
		}
		
		//		if(player.trade.trading)
		//			player.trade.tradeUI.ReplaceItem(item);
		if(player.mailHandler.mailList.Count > 0)
			player.mailHandler.ReplaceItem(item);
		if(player.InspectMgr.active)
			player.InspectMgr.ReplaceItem(item);
		if(player.target != null && (player.target as NPC).vendor != null)
			(player.target as NPC).vendor.ReplaceItem(item);
		if(player.trade.trading)
			player.trade.tradeUI.showTargetTradeSlots();
	}
	
	void  HandleInventoryListOpceode (WorldPacket pkt){
		ulong guid = pkt.ReadReversed64bit();
		byte count = pkt.Read();
		if(count == 0)
		{
			MonoBehaviour.print("no inventory items");
			//return;
		}
		MonoBehaviour.print("nr of inventory items " + count);
	}
	
	
	void  HandleEquipmentSetListOpcode (WorldPacket pkt)
	{
		try{
			uint count = pkt.ReadReversed32bit();
			if(count == 0)// 0 reversed is still 0
			{
				MonoBehaviour.print("***********************no items equiped");
				return;
			}
			uint i;
			for(i = 0;i<count;i++)
			{
				ulong guid = pkt.GetPacketGuid();
				uint unk32 = pkt.ReadReversed32bit();
				string name = pkt.ReadString();
				string iconName = pkt.ReadString();
				MonoBehaviour.print("item with guid " + guid+" and unk value "+ unk32+" has name "+name+" and icon name " + iconName);
				uint j;
				for(j = 0; j < 19; ++j)//EQUIPMENT_SLOT_END = 19/// on server
				{
					ulong unk64 = pkt.GetPacketGuid();
					MonoBehaviour.print("unk value ulong :" + unk64);
				}
			}
		}catch(Exception e){MonoBehaviour.print(e.ToString());}
	}
	
	void  HandleInventoryChangeFailureOpcode (WorldPacket pkt)
	{
		InventoryChangeFailure msg = (InventoryChangeFailure)pkt.Read();
		Debug.Log("World: SMSG_INVENTORY_CHANGE_FAILURE - msg: "+msg);
		MessageWnd wnd = new MessageWnd();
		
		if(player.target != null && ((player.target is NPC) && (player.target as NPC).banker != null))
		{
			if(msg == InventoryChangeFailure.EQUIP_ERR_MUST_PURCHASE_THAT_BAG_SLOT)
			{
				(player.target as NPC).banker.AlertWindow("You don't have free bag slots!");
			}
			else if(msg == InventoryChangeFailure.EQUIP_ERR_BANK_FULL)
			{
				(player.target as NPC).banker.AlertWindow("Bank is full!");
			}
		}
		
		switch(msg)
		{
		case InventoryChangeFailure.EQUIP_ERR_CANT_EQUIP_LEVEL_I: wnd.text = "Insufficient Level"; break;
		case InventoryChangeFailure.EQUIP_ERR_CANT_EQUIP_SKILL: wnd.text = "Insufficient Skill"; break;
		case InventoryChangeFailure.EQUIP_ERR_ITEM_DOESNT_GO_TO_SLOT: wnd.text = "Doesn't go to slot"; break;
		case InventoryChangeFailure.EQUIP_ERR_BAG_FULL: wnd.text = "Bag full"; break;
		case InventoryChangeFailure.EQUIP_ERR_NONEMPTY_BAG_OVER_OTHER_BAG: wnd.text = "Bag over other bag"; break;
		case InventoryChangeFailure.EQUIP_ERR_CANT_TRADE_EQUIP_BAGS: wnd.text = "Can't trade equiped bags"; break;
		case InventoryChangeFailure.EQUIP_ERR_ONLY_AMMO_CAN_GO_HERE: wnd.text = "Only ammo can  go here"; break;
		case InventoryChangeFailure.EQUIP_ERR_NO_REQUIRED_PROFICIENCY: wnd.text = "Required Proficiency"; break;
		case InventoryChangeFailure.EQUIP_ERR_NO_EQUIPMENT_SLOT_AVAILABLE: wnd.text = "No free equipment slot"; break;
		case InventoryChangeFailure.EQUIP_ERR_YOU_CAN_NEVER_USE_THAT_ITEM: wnd.text = "You cannot use that item"; break;
		case InventoryChangeFailure.EQUIP_ERR_YOU_CAN_NEVER_USE_THAT_ITEM2: wnd.text = "You cannot use that item2"; break;
		case InventoryChangeFailure.EQUIP_ERR_NO_EQUIPMENT_SLOT_AVAILABLE2: wnd.text = "No equipment slot available"; break;
		case InventoryChangeFailure.EQUIP_ERR_CANT_EQUIP_WITH_TWOHANDED: wnd.text = "Can't equip 1H with 2H on"; break;
		case InventoryChangeFailure.EQUIP_ERR_CANT_DUAL_WIELD: wnd.text = "Can't dual wield"; break;
		case InventoryChangeFailure.EQUIP_ERR_ITEM_DOESNT_GO_INTO_BAG: wnd.text = "This bag is full"; break;
		case InventoryChangeFailure.EQUIP_ERR_ITEM_DOESNT_GO_INTO_BAG2: wnd.text = "This bag is full"; break;
		case InventoryChangeFailure.EQUIP_ERR_CANT_CARRY_MORE_OF_THIS: wnd.text = "Can't carry more of this"; break;
		case InventoryChangeFailure.EQUIP_ERR_NO_EQUIPMENT_SLOT_AVAILABLE3: wnd.text = "No equipment slot available"; break;
		case InventoryChangeFailure.EQUIP_ERR_ITEM_CANT_STACK : wnd.text = "Item can't be stacked"; break;
		case InventoryChangeFailure.EQUIP_ERR_ITEM_CANT_BE_EQUIPPED: wnd.text = "Item can't be equipped"; break;
		case InventoryChangeFailure.EQUIP_ERR_ITEMS_CANT_BE_SWAPPED: wnd.text = "Items can't be swapped"; break;
		case InventoryChangeFailure.EQUIP_ERR_SLOT_IS_EMPTY: wnd.text = "Slot is empty"; break;
		case InventoryChangeFailure.EQUIP_ERR_ITEM_NOT_FOUND: wnd.text = "Item not found"; break;
		case InventoryChangeFailure.EQUIP_ERR_CANT_DROP_SOULBOUND: wnd.text = "Can't drop bound"; break;
		case InventoryChangeFailure.EQUIP_ERR_OUT_OF_RANGE : wnd.text = "Out of range"; break;
		case InventoryChangeFailure.EQUIP_ERR_TRIED_TO_SPLIT_MORE_THAN_COUNT: wnd.text = "Can't split more than have"; break;
		case InventoryChangeFailure.EQUIP_ERR_COULDNT_SPLIT_ITEMS: wnd.text = "Can't split items"; break;
		case InventoryChangeFailure.EQUIP_ERR_MISSING_REAGENT: wnd.text = "Missing reagent"; break;
		case InventoryChangeFailure.EQUIP_ERR_NOT_ENOUGH_MONEY: wnd.text = "Not enough gold"; break;
		case InventoryChangeFailure.EQUIP_ERR_NOT_A_BAG: wnd.text = "Not a bag"; break;
		case InventoryChangeFailure.EQUIP_ERR_NOT_IN_COMBAT: wnd.text = "Not in combat"; break;
		case InventoryChangeFailure.EQUIP_ERR_CANT_EQUIP_NEED_TALENT: wnd.text = "Can't equip need trait"; break;
		case InventoryChangeFailure.EQUIP_ERR_INVENTORY_FULL: wnd.text = "Your inventory is full"; break;
		case InventoryChangeFailure.EQUIP_ERR_YOU_ARE_DEAD: wnd.text = "You are dead!"; break;
			
		case InventoryChangeFailure.EQUIP_ERR_SHAPESHIFT_FORM_CANNOT_EQUIP: wnd.text = "There are no dungeons bound"; break;
		case InventoryChangeFailure.EQUIP_ERR_ITEM_INVENTORY_FULL_SATCHEL: wnd.text = "Char inside other dungeon"; break;
			
		case InventoryChangeFailure.EQUIP_ERR_SCALING_STAT_ITEM_LEVEL_TOO_LOW: wnd.text = "Not in a group"; break;
		case InventoryChangeFailure.EQUIP_ERR_CANT_BUY_QUANTITY: wnd.text = "Nobody bound to any dungeon"; break;
		case InventoryChangeFailure.EQUIP_ERR_ITEM_IS_BATTLE_PAY_LOCKED: wnd.text = "Char inside other dungeon"; break;
			
		default:
			//				player.inventoryMessage = "Error: "+msg;
			InterfaceManager.Instance.SetInventoryMessage("Error: "+msg);
			break;
			//...............................................
		}	
		//		player.inventoryMessage = wnd.text;
		InterfaceManager.Instance.SetInventoryMessage(wnd.text);
		
		if(ButtonOLD.needState || ButtonOLD.lootState)
			player.interf.TemplateAlertWindow(wnd.text);
		
		if(msg == InventoryChangeFailure.EQUIP_ERR_SHAPESHIFT_FORM_CANNOT_EQUIP
		   || msg == InventoryChangeFailure.EQUIP_ERR_ITEM_INVENTORY_FULL_SATCHEL
		   || msg == InventoryChangeFailure.EQUIP_ERR_SCALING_STAT_ITEM_LEVEL_TOO_LOW
		   || msg == InventoryChangeFailure.EQUIP_ERR_CANT_BUY_QUANTITY
		   || msg == InventoryChangeFailure.EQUIP_ERR_ITEM_IS_BATTLE_PAY_LOCKED)
		{
			wnd.rect = new Rect(Screen.width*0.35f,Screen.height*0.25f,Screen.width*0.4f,Screen.height*0.4f);
			wnd.text = "Instance reset failed:\n\n"+ player.inventoryMessage;	
			wnd.OnGUI = InstanceResponse;		
			player.windows.Add(wnd);   
		}  
	}
	
	void  HandleLevelUpOpcode (WorldPacket pkt)
	{
		uint level = pkt.ReadReversed32bit();
		Debug.Log("the level "+level);
		player.aHandler.PlayActionAnimation("levelup");
	}
	//============================================= End Items/Inventory/Equipe Handlers  ==================================//

	
	//==========================================================================================================//
	//																											//	
	//												Spells Handlers				 								//
	//																											//
	//==========================================================================================================//	
	void  HandleCastFailedOpcode (WorldPacket pkt)
	{
		byte castCount = pkt.Read();
		uint spellID = pkt.ReadReversed32bit();
		Debug.Log("SMSG_CAST_FAILED for spellID = "+spellID);
		if(player.getIsCasting() == spellID)
		{
			player.SetIsCasting(0);
			interfaceManager.castBarMng.flag = false;
			if(interfaceManager.playerMenuMng.extraMenuWindow.craftInterfaceController.gameObject.activeInHierarchy)
			{
				player.craftManager.countCraft = 0;
			}
		}
		SpellCastResult result = (SpellCastResult)pkt.Read();
		switch(result)
		{
		case SpellCastResult.SPELL_FAILED_SUCCESS:
			#if UNITY_EDITOR
			Debug.Log("Cast is SUCCESS");
			#endif
			break;
		case SpellCastResult.SPELL_FAILED_NO_DUELING:
			player.AddChatInput("You can't battle here.");
			break;
		case SpellCastResult.SPELL_FAILED_REQUIRES_SPELL_FOCUS: 
			uint requiresSpellFocus = pkt.ReadReversed32bit();
			interfaceManager.DrawMessageWindow("You must be close to the forge to do this.");
			Debug.LogWarning("TODO!!!");
			break;
		case SpellCastResult.SPELL_FAILED_LOW_CASTLEVEL:
//			int requiresSkillLvl = GetSkillLvl((player.target as BaseObject).GetUInt32Value((int)UpdateFields.EObjectFields.OBJECT_FIELD_ENTRY));
//			interfaceManager.DrawMessageWindow("You need min " + requiresSkillLvl + " in Prospecting to dig in this node.");
			interfaceManager.DrawMessageWindow("Your Craft skill is too low to craft it.");
			break;
		case SpellCastResult.SPELL_FAILED_REAGENTS:
			interfaceManager.DrawMessageWindow("Not enough ingredients");
			break;
		case SpellCastResult.SPELL_FAILED_UNIT_NOT_INFRONT:
			player.ShowErrorMessage("You must face target");
			break;
		case SpellCastResult.SPELL_FAILED_OUT_OF_RANGE:
			player.ShowErrorMessage("Out of range");
			break;
		case SpellCastResult.SPELL_FAILED_NO_POWER:
			switch(player.playerClass)
			{
			case Classes.CLASS_WARRIOR:
				player.ShowErrorMessage("Not enough energy.");
				break;
			case Classes.CLASS_PRIEST:
				player.ShowErrorMessage("Not enough mana.");
				break;
			case Classes.CLASS_MAGE:
				player.ShowErrorMessage("Not enough mana.");
				break;
			case Classes.CLASS_ROGUE:
				player.ShowErrorMessage("Not enough energy.");
				break;
			case Classes.CLASS_PALADIN:
				player.ShowErrorMessage("Not enough mana.");
				break;
			case Classes.CLASS_WARLOCK:
				player.ShowErrorMessage("Not enough mana.");
				break;
			case Classes.CLASS_HUNTER:
				player.ShowErrorMessage("Not enough mana.");
				break;
			}
			break;
		case SpellCastResult.SPELL_FAILED_WRONG_PET_FOOD:
			//				ErrorMessage.ShowError("Your pet does not\neat this food.");
			InterfaceManager.Instance.SetInventoryMessage("Your pet does not\neat this food.");
			break;
		case SpellCastResult.SPELL_FAILED_FOOD_LOWLEVEL:
			//				ErrorMessage.ShowError("Food level to low\nfor this pet.");
			InterfaceManager.Instance.SetInventoryMessage("Food level to low\nfor this pet.");
			break;
		case SpellCastResult.SPELL_FAILED_LOWLEVEL:
			//				if(player.interf.inventoryState)
			//				{
			//					ErrorMessage.ShowError("Item you want to re-forge\nis to low lvl for this\nre-forge ingredient.");
			//				}
			if(InterfaceManager.Instance.hideInterface || ButtonOLD.menuState)
			{
				InterfaceManager.Instance.SetInventoryMessage("Item you want to re-forge\nis to low lvl for this\nre-forge ingredient.");
			}
			break;
		case SpellCastResult.SPELL_FAILED_ALREADY_AT_FULL_HEALTH:
			if(InterfaceManager.Instance.hideInterface || ButtonOLD.menuState)
			{
				InterfaceManager.Instance.SetInventoryMessage("You are absolutely healthy.");
			}
			break;
		case SpellCastResult.SPELL_FAILED_ALREADY_AT_FULL_MANA:
		case SpellCastResult.SPELL_FAILED_ALREADY_AT_FULL_POWER:
			if(InterfaceManager.Instance.hideInterface || ButtonOLD.menuState)
			{
				InterfaceManager.Instance.SetInventoryMessage("You are full of energy.");
			}
			break;
		case SpellCastResult.SPELL_FAILED_NO_AMMO:
			if(InterfaceManager.Instance.hideInterface || ButtonOLD.menuState)
			{
				InterfaceManager.Instance.SetInventoryMessage("Out of ammo.");
			}
			break;
		case SpellCastResult.SPELL_FAILED_NOT_READY:
			if(InterfaceManager.Instance.hideInterface || ButtonOLD.menuState)
			{
				InterfaceManager.Instance.SetInventoryMessage("Spell not ready");
			}
			break;
			
			#if UNITY_EDITOR
		default:
			Debug.Log("Spell "+spellID+" failed on cast - "+result);
//			if(player.interf.inventoryState)
//			{
//				ErrorMessage.ShowError("Incorrect slot for chosen\nre-forge ingredient!");
//			}
			if(InterfaceManager.Instance.hideInterface || ButtonOLD.menuState)
			{
				InterfaceManager.Instance.SetInventoryMessage("Incorrect slot for chosen\nre-forge ingredient!");
			}
			break;
			#endif
		}
	}
	
	int GetSkillLvl (uint id)
	{
		int ret = 0;
		switch(id)
		{
		case 19903:	//(25)
			ret = 300;
			break;
		case 1731:	//(38)
			ret = 1;
			break;
		case 1610:	//(39)
		case 1732:	//(39)
			ret = 130;
			break;
		case 1733:	//(40)
		case 2653:	//(40)
		case 73940:	//(40)
			ret = 150;
			break;
		case 1735:	//(41)
			ret = 250;
			break;
		case 1734:	//(42)
		case 73941:	//(42)
			ret = 310;
			break;
		case 2040:	//(379)
		case 123310://(379)
			ret = 350;
			break;
		case 2047:	//(380)
		case 123309://(380)
			ret = 410;
			break;
		case 324:	//(400)
		case 123848://(400)
			ret = 460;
			break;
		case 165658://(719)
			ret = 460;
			break;
		case 175404://(939)
		case 177388://(939)
			ret = 510;
			break;
		case 181068://(1632)
		case 181069://(1632)
			ret = 610;
			break;
		case 181555://(1649)
		case 185877://(1649)
			ret = 550;
			break;
		case 181556://(1650)
			ret = 650;
			break;
		case 181557://(1651)
		case 185557://(1651)
			ret = 750;
			break;
		case 181569://(1652)
			ret = 700;
			break;
		case 189979://(1782)
			ret = 750;
			break;
		case 189980://(1783)
			ret = 800;
			break;
		case 189981://(1784)
			ret = 850;
			break;
		case 191133://(1785)
		case 195036://(1785)
			ret = 900;
			break;
		case 189978://(1800)
			ret = 700;
			break;
		}
		
		return ret;
	}
	
	void  HandleUpdateComboPointsOpCode (WorldPacket pkt)
	{
		ulong targetGuid = pkt.GetPacketGuid();
		byte comboPoints = pkt.Read();
		
		//	Debug.Log("World - SMSG_UPDATE_COMBO_POINTS -targetGUID: "+targetGuid+" ComboPoints: "+comboPoints);
		
		Unit target = GetRealChar(targetGuid) as Unit;
		
		if(!target)
		{
			if(target.guid == MainPlayer.GUID)
			{
				target = player;
			}
		}
		
		player.PortraitReff.comboPoints = comboPoints;
		/*
		if(comboPoints)
			player.PortraitReff.comboPoints = comboPoints;
			
			//player.showDMG(target, ""+comboPoints+" Combo Points", 1);
		*/
		//	ShowSpellEffectVisual(spellID, targetGuid, false);
		
	}
	
	void  HandleSpellHealLogOpcode (WorldPacket pkt){
		int effectId;
		SpellVisual scriptSpellVisual;
		GameObject effectObject;
		SpellEntry spellEntry;
		
		ulong targetGuid = pkt.GetPacketGuid();
		ulong casterGuid = pkt.GetPacketGuid(); 
		uint spellID = pkt.ReadReversed32bit();
		uint dmg = pkt.ReadReversed32bit();
		uint overHeal = pkt.ReadReversed32bit();
		uint absorb = pkt.ReadReversed32bit();
		byte critical = pkt.Read();
		pkt.Read(); //o 
		
		//Debug.Log("World - SMSG_SPELLHEALLOG - Victim: "+targetGuid+" spellID:"+spellID+ " healed: "+dmg);
		
		Unit target = GetRealChar(targetGuid) as Unit;
		
		if((target == null) || (target != null && target.guid == MainPlayer.GUID))
		{
			target = player as Unit;
		}
		
		BaseObject targetObj;
		if(absorb != 0 && dmg == 0)
		{
			player.showDMG(target, "Absorb", 1, 2, critical > 0, casterGuid);
			
			if(CheckFxOptions(casterGuid))
			{
				return;
			}
			
			scriptSpellVisual = FxEffectManager.GetSpellVisual(spellID);
			if(scriptSpellVisual)
			{
				effectId = scriptSpellVisual.effect[1];
				if(effectId > 0)
				{
					targetObj = GetRealChar(targetGuid);
					spellEntry = dbs.sSpells.GetRecord(spellID);
					FxEffectManager.CastEffect(spellEntry.speed, 2f, GetGender(targetGuid), effectId, targetObj, targetObj);
				}
			}
		}
		else if(dmg != 0)
		{
			player.showDMG(target, "" + dmg, 1, 2, critical > 0, casterGuid);
			
			if(CheckFxOptions(casterGuid))
			{
				return;
			}
			
			scriptSpellVisual = FxEffectManager.GetSpellVisual(spellID);
			if(scriptSpellVisual != null)
			{
				effectId = scriptSpellVisual.effect[1];
				if(effectId > 0)
				{
					targetObj = GetRealChar(targetGuid);
					spellEntry = dbs.sSpells.GetRecord(spellID);
					FxEffectManager.CastEffect(spellEntry.speed, 2.0f, GetGender(targetGuid), effectId, targetObj, targetObj);
				}
			}
		}
		else
		{
			player.showDMG(target, "Miss", 1, 2, critical > 0, casterGuid);
		}
		
		//ShowSpellEffectVisual(spellID, targetGuid, false);
	}
	
	void  HandleSpellNoMeleeDamageLogOpCode (WorldPacket pkt)
	{
		int effectId;
		SpellVisual scriptSpellVisual;
		GameObject effectObject;
		SpellEntry spellEntry;
		
		ulong targetGuid = pkt.GetPacketGuid();
		ulong attacker = pkt.GetPacketGuid();
		if(player == null)
		{
			getPlayer();
		}
		uint spellID = pkt.ReadReversed32bit();
		uint dmg = pkt.ReadReversed32bit();
		uint overkill = pkt.ReadReversed32bit();
		if(overkill != 0)
		{
			if(targetGuid == MainPlayer.GUID)
			{
				player.HP=0;
			}
		}
		byte schoolMask = pkt.Read();
		uint absorb = pkt.ReadReversed32bit();
		uint resist = pkt.ReadReversed32bit();
		byte physicalLog = pkt.Read();
		byte unused = pkt.Read();
		uint blocked = pkt.ReadReversed32bit();
		uint hitInfo = pkt.ReadReversed32bit();
		uint type = (uint)HitInfo.HITINFO_CRITICALHIT;
		uint critical = hitInfo & type;
		pkt.Read();
		
		Unit target = GetRealChar(targetGuid) as Unit;
		
		if(target == null)
		{
			if(target.guid == MainPlayer.GUID)
			{
				target = player;
			}
		}
		if(blocked != 0 && dmg == 0)
		{
			player.showDMG(target, "Blocked", schoolMask, 1, critical > 0, attacker);
		}
		else if(dmg != 0)
		{
			player.showDMG(target, ""+dmg, schoolMask, 1, critical > 0, attacker);
			if(CheckFxOptions(attacker))
			{
				return;
			}
			
			scriptSpellVisual = FxEffectManager.GetSpellVisual(spellID);
			if(scriptSpellVisual)
			{
				effectId = scriptSpellVisual.effect[2];
				if(effectId > 0)
				{
					spellEntry = dbs.sSpells.GetRecord(spellID);
					FxEffectManager.CastEffect(spellEntry.speed, 2f, GetGender(targetGuid),
					                           effectId, GetRealChar(targetGuid), GetRealChar(targetGuid));
				}
			}
		}
		else
		{
			player.showDMG(target, "Miss", schoolMask, 1, critical > 0, attacker);
		}
		
		//ShowSpellEffectVisual(spellID, targetGuid, false);
	}
	
	void  HandleSpellDelayedOpCode (WorldPacket pkt){
		ulong uguid = pkt.ReadReversed32bit();
		uint delayTime = pkt.ReadReversed32bit();
		
		Player caster = GetRealChar(uguid) as Player;
		
		if(!caster)
		{
			if(uguid == MainPlayer.GUID)
				caster = player;
		}
		
		if(caster)
		{
			caster.playerSpellManager.spellsDelayed = delayTime;
			//Debug.Log("delayed: "+delayTime);
		}
		
		//Debug.Log(uguid+"	Delayed: "+delayTime);
		
		interfaceManager.castBarMng.castTimeTotal += delayTime / 1000.0f;
		//		player.castTimeTotal += delayTime / 1000.0f;
	}
	
	//recive player spells at login
	void HandleInitSpellsOpcode(WorldPacket pkt) // TODO not all the data ar readed  /===================/Spells Handlers/==============/			  	
	{
		//MonoBehaviour.print("init list of spells...");
		try
		{
			if(!player) // the player has not been linked yet
			{
				getPlayer();
			}
			
			if(player)
			{					
				pkt.Read();					// just a 0 value
				ushort spellCount = pkt.ReadReversed16bit();
				for(uint i = 0; i < spellCount; i++)
				{
					parsePlayerSpell(pkt);
					pkt.ReadType(2);		// just a 0 value
				}
				
				ReadCooldownsForSpells(pkt);
			}
			
			
		}
		catch(Exception e)
		{
			MonoBehaviour.print(e.ToString());
		}
	}
	
	void  ReadCooldownsForSpells (WorldPacket pkt)
	{
		try
		{
			if(player)
			{
				player.cooldownManager.playerCooldownList.ClearCooldownList();
				
				ushort spellCooldownCount = pkt.ReadReversed16bit();				
				for(uint i = 0; i < spellCooldownCount; i++)
				{
					Cooldown cooldown = null;
					
					uint spellId = pkt.ReadReversed32bit();
					uint itemId = (uint)pkt.ReadReversed16bit();
					uint spellCategory = pkt.ReadReversed16bit();
					
					uint recoveryTime;
					uint categoryRecoveryTime;
					
					if(spellCategory != 0)
					{
						recoveryTime = pkt.ReadReversed32bit();
						categoryRecoveryTime = pkt.ReadReversed32bit();
					}
					else
					{
						recoveryTime = pkt.ReadReversed32bit();
						categoryRecoveryTime = pkt.ReadReversed32bit();
					}
					
					if(!verifyIfSpellIsTalent(spellId) || isCanBeLearnedTalentSpell(spellId))
					{
						try
						{
							Spell spell = SpellManager.CreateOrRetrive(spellId);
							
							int spellRecoveryTime = spell.RecoveryTime;
							int spellCategoryRecoveryTime = spell.CategoryRecoveryTime;
							
							if(spellRecoveryTime == 0 && spellCategoryRecoveryTime == 0)
							{
								spellRecoveryTime = 86400000;				// 24 hours;
								spellCategoryRecoveryTime = 86400000;		// 24 hours;
							}
							
							if(spellCategory != 0 && categoryRecoveryTime > 0)
							{
								if(categoryRecoveryTime < spellCategoryRecoveryTime)
								{
									cooldown = new Cooldown(categoryRecoveryTime / 1000.0f, 
									                        spellCategoryRecoveryTime / 1000.0f);
								}
								else if(categoryRecoveryTime < spellRecoveryTime)
								{
									cooldown = new Cooldown(categoryRecoveryTime / 1000.0f, 
									                        spellRecoveryTime / 1000.0f);
								}
								else
								{
									continue;
								}
							}
							else if(recoveryTime > 0)
							{
								cooldown = new Cooldown(recoveryTime / 1000.0f, 
								                        spellRecoveryTime / 1000.0f);
							}
							else
							{
								continue;
							}
							
							player.cooldownManager.playerCooldownList.SetCooldown(spell, cooldown);
						}
						catch(Exception e)
						{
							Debug.LogException(e);
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			MonoBehaviour.print(e.ToString());
		}
	}
	
	void  HandlerSetSpellModModifierOpCode (WorldPacket pkt)
	{
		//Debug.Log("World: SMSG_SET_MOD_SPELL_MODIFIER- Effect: "+pkt.Read()+"  Mod: "+pkt.Read()+"  val: "+pkt.ReadReversed32bit());
	}
	
	Gender GetGender (ulong guid)
	{
		Gender ret = Gender.GENDER_NONE;
		if(guid == player.guid)
		{
			ret = player.gender;
		}
		else
		{
			BaseObject obj = GetRealChar(guid);
			OtherPlayer otherPlayer = obj.gameObject.GetComponent<OtherPlayer>();
			if(otherPlayer != null)
			{
				ret = otherPlayer.gender;
			}
		}
		return ret;
	}
	
	// Пакет приходит при позитовном касте заклинания
	//
	void  HandleSpellGoOpCode (WorldPacket pkt)
	{
		ulong casterItemGuid = pkt.GetPacketGuid(); 	//itemGuid or casterGuid
		ulong casterGuid = pkt.GetPacketGuid(); 		//casterGuid
		byte castCount = pkt.Read();							//pending spell cast?
		if(castCount > 1)
		{
			Debug.Log("castCount = " + castCount);
		}
		uint spellID = pkt.ReadReversed32bit();				//spell ID
		Debug.Log("Go Spell "+spellID);
		uint casterFlags = pkt.ReadReversed32bit();
		uint timeStamp = pkt.ReadReversed32bit();				//server time hh:mm:ss:mmmm
		
		if(casterGuid != player.guid)
		{
			BaseObject obj = GetRealChar(casterGuid);
			if(!obj)
			{
				Debug.Log("Not found caster " + casterGuid.ToString());
				return;
			}
		}
		
		if (spellID == 8690 || spellID == 34619)
		{
			if(WorldSession.player)
			{
				WorldSession.player.transform.parent.GetComponent<Rigidbody>().velocity = Vector3.zero;
			}
		}
		
		SpellEntry spellEntry = dbs.sSpells.GetRecord(spellID);
		if(spellEntry.Effect.array == null)
		{
			Debug.Log("Not found spell with ID " + spellID);
			return;
		}		
		
		byte numberOfHits = pkt.Read();
		ulong targetGuid = 0;
		int effectId;
		SpellVisual scriptSpellVisual;
		GameObject effectObject;
		long spellAttrRanged = (long)SpellAttributes.SPELL_ATTR_RANGED;
		float spellCastTime = 0;
		while(numberOfHits > 0)
		{
			targetGuid = pkt.ReadReversed64bit();
			if(isNeedStopAutoCast(spellID, casterGuid, targetGuid))
				return;
			
			// old romanian code
			Player caster = GetRealChar(casterGuid) as Player;

			//			if(casterGuid == MainPlayer.GUID)
			//			{
			//				spellCastTime = player.castTime;
			//			}
			
			//if we have a GUID and the spell has effect on the target
			if((caster != null) && (spellEntry.Attributes & spellAttrRanged) > 0)
			{
				caster.itemManager.ActivateRange();
				caster.StartCoroutineAnimation("");
			}
			
			if((casterGuid != 0) && (targetGuid != 0) && (!CheckFxOptions(casterGuid) || spellID == 75))
			{
				scriptSpellVisual = FxEffectManager.GetSpellVisual(spellID);
				if(scriptSpellVisual)
				{
					effectId = scriptSpellVisual.effect[2];
					if(effectId > 0)
					{
						FxEffectManager.CastEffect(spellEntry.speed, spellCastTime, GetGender(casterItemGuid),
						                           effectId, GetRealChar(casterItemGuid), GetRealChar(targetGuid));
					}
				}
			}			
			// End romanian code
			numberOfHits--;
		}
		
		Item item = player.itemManager.GetItemByGuid(casterItemGuid);
		Spell spell = SpellManager.GetSpell(spellID);
		bool  stopCast = false;
		
		if(casterGuid == player.guid)
		{
			if(casterItemGuid != casterGuid && item != null)
			{
				stopCast = item.IsPotion();
				player.cooldownManager.CreateCooldown(spell, item, stopCast);
				player.itemManager.SetCooldownForItems(item, !stopCast);
			}
			else
			{
				stopCast = SpellManager.CheckStopCooldownFlag(spell);
				player.cooldownManager.CreateCooldown(spell, null, !stopCast);
			}
			
			if(spellID == 1462)
			{
				ShowBeastLore();
			}
		}
		else if(casterGuid == player.petGuid)
		{
			player.cooldownManager.SetPetCategoryCooldown(spell);
		}
		
		byte numberOfMiss = pkt.Read();
		SpellMissInfo missCondition = SpellMissInfo.SPELL_MISS_NONE;
		byte reflectResult = 0;
		SpellMissInfo reflectFlag = SpellMissInfo.SPELL_MISS_REFLECT;
		while(numberOfMiss > 0)
		{
			targetGuid = pkt.ReadReversed64bit();
			if(isNeedStopAutoCast(spellID, casterGuid, targetGuid))
				return;
			missCondition = (SpellMissInfo)pkt.Read();
			if(missCondition == reflectFlag)
				reflectResult = pkt.Read();
			numberOfMiss--;
		}
		
		SpellCastTargets spellTargets = new SpellCastTargets();
		spellTargets.Read(pkt);
		scriptSpellVisual = FxEffectManager.GetSpellVisual(spellID);
		if(scriptSpellVisual)
		{
			effectId = scriptSpellVisual.persistentAreaKit;
			if(effectId > 0)
			{
				uint srcFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_SOURCE_LOCATION; 	// pguid + 3 float
				uint dstFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_DEST_LOCATION; 	// pguid + 3 float
				if((spellTargets.targetMask & srcFlag) > 0)
				{
					FxEffectManager.CastEffect(spellEntry.speed, spellCastTime, GetGender(casterGuid),
					                           effectId, GetRealChar(casterItemGuid), null, spellTargets.srcCoordinats);
				}
				else if((spellTargets.targetMask & dstFlag) > 0)
				{
					FxEffectManager.CastEffect(spellEntry.speed, spellCastTime, GetGender(casterGuid),
					                           effectId, GetRealChar(casterItemGuid), null, spellTargets.dstCoordinats);
				}
				else
				{
					FxEffectManager.CastEffect(spellEntry.speed, spellCastTime, GetGender(casterGuid),
					                           effectId, GetRealChar(casterItemGuid), GetRealChar(targetGuid));
				}
			}
		}	
	}
	
	// Пакет приходит при начале каста заклинания
	//
	void HandleSpellStartOpCode(WorldPacket pkt)// incomplete handler
	{
		//we get this package if the cast parameters has been validate by the server. after delay, we recive the next pkt(SMSG_SPELL_GO).
		ulong castItemGuid = pkt.GetPacketGuid(); //itemGuid or casterGuid
		ulong casterGuid = pkt.GetPacketGuid();   //casterGuid
		
		byte castCount = pkt.Read();		//pending spell cast?
		uint spellID = pkt.ReadReversed32bit();		//spell ID
		Debug.Log("Start Spell "+spellID);
		uint castFlag = pkt.ReadReversed32bit();		//CAST_FLAG_UNKNOWN1, CAST_FLAG_AMMO, CAST_FLAG_UNKNOWN10
		uint spellDelay = pkt.ReadReversed32bit();	//dealy in miliseconds
		
		Unit obj = null;
		if(casterGuid != player.guid)
		{
			obj = GetRealChar(casterGuid) as Unit;
			if(obj == null)
			{
				Debug.Log("Not found caster " + casterGuid.ToString());
				return;
			}
		}
		else if(player.getIsCasting() == spellID)
		{
			player.SetIsCasting(0);
		}
		
		SpellCastTargets spellTargets = new SpellCastTargets();
		spellTargets.Read(pkt);
		
		SpellEntry sp = dbs.sSpells.GetRecord(spellID);
		if(sp == null)
		{
			Debug.Log("There is no spellID: " + spellID + " in client databace");
			return;
		}
		
		//if is Main Player, set the cooldowns and such
		Spell spell = SpellManager.GetSpell(spellID);
		if(casterGuid == player.guid)
		{
			if(spell != null)
			{
				player.cooldownManager.SetGlobalCooldown(spell);
			}
			
			//Debug.Log("init of cast bar, spell delay, bla bla");
			obj = player as Unit;
			if(spellDelay > 0)
			{
				DrawSpellProgressBar();
			}
		}
		else if(casterGuid == player.petGuid)
		{
			player.cooldownManager.SetPetGlobalCooldown(spell);
		}
		
		obj.castSpellID = spellID;
		if(spellDelay > 0)
		{
			interfaceManager.castBarMng.castTime = 0;
			interfaceManager.castBarMng.castTimeTotal = spellDelay / 1000f;
			interfaceManager.castBarMng.isConductedSpell = true;
		}
		//		object.castSpellID = spellID;
		//		object.castTime = 0;
		//		object.castTimeTotal = spellDelay / 1000f;
		//		object.isConductedSpell = true;
		
		if(casterGuid == player.guid || (casterGuid != player.guid && casterGuid != 0))
		{
			DisplaySpellVisualOnCaster(spellID, casterGuid, spellTargets, spellDelay / 1000f);
		}
		else
		{
			return;
		}
		
		if(player.playerSpellManager.IsNeedCastAttackAfterSpell(SpellManager.GetSpell(spellID)))
		{
			if(player.target)
				player.playerSpellManager.CastSpell(6603, 2, player.target.gameObject.transform.position);
			else
				player.playerSpellManager.CastSpell(6603, 2, player.gameObject.transform.position);
		}
	}	
	
	void  DisplaySpellVisualOnCaster (uint spellID, ulong casterGUID, SpellCastTargets spellTargets, float spellDelay)
	{
		//Debug.Log("entering display visual on caster!");
		SpellEntry _spell = dbs.sSpells.GetRecord(spellID);
		NPC _npc = null;
		Player _player;
		
		//Debug.Log("SPEEEEEEEEEEEEEEEED of spell with the spellID: " + _spell.ID + " is: " + _spell.speed);
		
		if(casterGUID == MainPlayer.GUID)
		{
			_player = player as Player;
		}
		else
		{
			_player = GetRealChar(casterGUID) as Player;
		}
		
		if(_player == null)
		{
			_npc = GetRealChar(casterGUID) as NPC;
		}
		
		
		if(_spell == null || (_player == null && _npc == null))
		{
			Debug.Log("No Spell " + spellID.ToString() + " or no Player " + casterGUID.ToString() + "!!!" );
			return;
		}
		else
		{
			bool  needCreateFxEffect = false;
			
			if(spellDelay == 0)
			{
				spellDelay = 1.5f;
			}
			
			if(_player != null)
			{
				long spellAttrRanged = (long)SpellAttributes.SPELL_ATTR_RANGED;
				if(!CheckFxOptions(casterGUID) || (_spell.Attributes & spellAttrRanged) > 0)
				{
					//					FxEffect.SpellCastFxEffect(_spell, casterGUID, spellDelay);
					needCreateFxEffect = true;
				}
				
				if((_spell.Attributes & spellAttrRanged) > 0)
				{
					if(_player.itemManager.IsEquipedRangeWeapon())
					{
						_player.itemManager.ActivateRange();
					}
					else
					{
						return;
					}
				} 
				else
				{
					_player.itemManager.DeactivateRange();
				}
				switch(_spell.DmgClass)
				{
				case 1:
					_player.StartCoroutineAnimation("magic_atk_1");
					break;
				case 0:
					if(	_player.craftManager.IsSpellRelevantCraft(_spell, CraftSkill.SKILL_FISHING) )
					{
						_player.StartCoroutineAnimation("Fishing_throw_line");
					}
					break;
				default:
					//						_player.StartCoroutineAnimation("");
					break;
				}
			}
			else
			{
				if(!CheckFxOptions(casterGUID))
				{
					//					FxEffect.SpellCastFxEffect(_spell, casterGUID, spellDelay);			
					needCreateFxEffect = true;
				}		
				
				switch(_spell.DmgClass)
				{
				case 1:
					_npc.aHandler.PlayActionAnimation("magic_atk_1");
					break;
				default:
					_npc.aHandler.PlayActionAnimation("");
					break;
				}
			}
			
			if(needCreateFxEffect)
			{
				SpellVisual scriptSpellVisual;
				scriptSpellVisual = FxEffectManager.GetSpellVisual(spellID);
				if(scriptSpellVisual)
				{
					int effectId = scriptSpellVisual.effect[0];
					if(effectId > 0)
					{
						GameObject effectObject;
						uint srcFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_SOURCE_LOCATION;	// pguid + 3 float
						uint dstFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_DEST_LOCATION;	// pguid + 3 float
						if((spellTargets.targetMask & srcFlag) > 0)
						{
							FxEffectManager.CastEffect(_spell.speed, spellDelay, GetGender(casterGUID),
							                           effectId, GetRealChar(casterGUID), null, spellTargets.srcCoordinats);
						}
						else if((spellTargets.targetMask & dstFlag) > 0)
						{
							FxEffectManager.CastEffect(_spell.speed, spellDelay, GetGender(casterGUID),
							                           effectId, GetRealChar(casterGUID), null, spellTargets.dstCoordinats);
						}
						else
						{
							FxEffectManager.CastEffect(_spell.speed, spellDelay, GetGender(casterGUID),
							                           effectId, GetRealChar(casterGUID), GetRealChar(casterGUID));
						}
					}
				}
			}
		}
		
	}
	
	bool CheckFxOptions (ulong guid)
	{
		bool  result = false;
		if(guid == MainPlayer.GUID && PlayerPrefs.GetInt("ShowPlayerFx", 1) == 0)
		{
			result = true;
		}
		else if(guid != MainPlayer.GUID && PlayerPrefs.GetInt("ShowOtherPlayerFx", 1) == 0)
		{
			result = true;
		}
		
		return result;
	}

	void  HandleAuraUpdateAllOpcode (WorldPacket pkt)
	{
		try
		{
			byte auraSlot;
			uint spellID;
			ulong uguid = pkt.GetPacketGuid();
			getPlayer();
			SpawnChar obj2 = getObjToSpawn(uguid);
			Unit obj = GetRealChar(uguid) as Unit;
			if(obj2 != null)
			{
				obj2.auras.Clear();
			}
			
			List<Aura> listOfAura = new List<Aura>();
			while(pkt.remainingData() > 0)
			{
				auraSlot = pkt.Read();
				spellID = pkt.ReadReversed32bit();
				var aura= new Aura(spellID, GetRealChar(uguid) as Unit);
				//				aura.spellID = spellID;
				aura.casterGuid = uguid;
				aura.auraSlot = auraSlot;
				aura.auraFlags = (AuraFlags)pkt.Read();
				aura.auraLevel = pkt.Read();
				aura.auraCharges = pkt.Read();
				if(aura.spellID > 0)
				{
					AuraFlags flag = AuraFlags.AFLAG_NOT_CASTER;
					if((aura.auraFlags & flag) == 0)
					{
						aura.casterGuid = pkt.ReadType(8);
					}
					flag = AuraFlags.AFLAG_DURATION;
					if((aura.auraFlags & flag) > 0)
					{
						aura.auraMaxDuration = pkt.ReadReversed32bit();
						aura.auraDuration = pkt.ReadReversed32bit()*0.001f;
					}
				}
				
				if(uguid == player.guid) //the set of auras belongs to a main player
				{
					if(player.auraManager.mainPlayerGO != player.gameObject)
					{
						player.auraManager.mainPlayerGO =  player.gameObject;
					}
					player.auraManager.UpdateAura(aura);
					player.interf.PortraitsFrame.UpdateUserAuras();
				}	
				else if(obj != null) //the set of auras belongs to npc or other player
				{
					if(obj.auraManager.mainPlayerGO != player.gameObject)
					{
						obj.auraManager.mainPlayerGO =  player.gameObject;
					}
					obj.auraManager.UpdateAura(aura);
				}
				else if(obj2 != null) // the creature hasnt be spawn yet. so we save the aura set for later use
				{
					obj2.auras.Add(aura);
				}
			}
		}
		catch(Exception e)
		{
			//			Debug.Log(e);
		}
	}
	
	void  HandlePeriodicAuraLogOpcode (WorldPacket pkt)
	{
		ulong targetGuid = pkt.GetPacketGuid();
		ulong casterGuid = pkt.GetPacketGuid();
		uint auraId = pkt.ReadReversed32bit();
		uint count  = pkt.ReadReversed32bit();
		AuraType auraName = (AuraType)pkt.ReadReversed32bit();
		//		MonoBehaviour.print("periodic aura update : " + auraId + " " + auraName);
		uint dmg = 0;
		uint overDmg;
		uint schoolMask = 0;
		uint absorb;
		uint resist;
		byte critical = 0;
		uint miscValue;
		
		switch(auraName)
		{
		case AuraType.SPELL_AURA_PERIODIC_DAMAGE:
		case AuraType.SPELL_AURA_PERIODIC_DAMAGE_PERCENT:
			dmg =  pkt.ReadReversed32bit();
			overDmg = pkt.ReadReversed32bit();;
			schoolMask = pkt.ReadReversed32bit();
			absorb = pkt.ReadReversed32bit();
			resist = pkt.ReadReversed32bit();
			critical = pkt.Read();
			break;
		case AuraType.SPELL_AURA_PERIODIC_HEAL:
		case AuraType.SPELL_AURA_OBS_MOD_HEALTH:
			dmg =  pkt.ReadReversed32bit();
			overDmg = pkt.ReadReversed32bit();
			absorb = pkt.ReadReversed32bit();
			critical = pkt.Read();
			break;
		case AuraType.SPELL_AURA_OBS_MOD_MANA:
		case AuraType.SPELL_AURA_PERIODIC_ENERGIZE:
			miscValue = pkt.ReadReversed32bit(); //power type
			dmg =  pkt.ReadReversed32bit();
			break;
		case AuraType.SPELL_AURA_PERIODIC_MANA_LEECH:
			miscValue = pkt.ReadReversed32bit(); //power type
			dmg =  pkt.ReadReversed32bit();
			float multiplier = pkt.ReadFloat();
			break;
		}
		
		Unit target = GetRealChar(targetGuid) as Unit;
		
		if(target == null)
		{
			if(targetGuid == MainPlayer.GUID)
			{
				target = player;
			}
		}
		
		if(target != null)
		{
			byte damageType;
			switch(auraName)
			{
			case AuraType.SPELL_AURA_PERIODIC_HEAL:
			case AuraType.SPELL_AURA_OBS_MOD_HEALTH:
				damageType = 2;
				if(dmg != 0)
				{
					player.showDMG(target, ""+dmg, schoolMask, damageType, critical > 2, casterGuid);
				}
				else
				{
					player.showDMG(target, "Absorb", schoolMask, damageType, critical > 2, casterGuid);
				}
				break;
			case AuraType.SPELL_AURA_PERIODIC_DAMAGE:
			case AuraType.SPELL_AURA_PERIODIC_DAMAGE_PERCENT:
			case AuraType.SPELL_AURA_OBS_MOD_MANA:
			case AuraType.SPELL_AURA_PERIODIC_ENERGIZE:
			case AuraType.SPELL_AURA_PERIODIC_MANA_LEECH:
				damageType = 1;
				if(dmg != 0)
				{
					player.showDMG(target, ""+dmg, schoolMask, damageType, critical > 0, casterGuid);
				}
				else
				{
					player.showDMG(target, "Absorb", schoolMask, damageType, critical > 0, casterGuid);
				}
				break;
			}
		}
	}

	void  HandleAuraUpdateOpcode (WorldPacket pkt)
	{
		//print("intra in aura update...");
		
		ulong uguid = pkt.GetPacketGuid();		
		var auraSlot= pkt.Read();
		uint spellID = pkt.ReadReversed32bit();
		
		/*
		SpellEntry checkSpell = dbs.sSpells.GetRecord(spellID);
		MonoBehaviour.print("spell id : " + spellID + " ~ " + checkSpell.InterruptFlags);
		if(checkSpell.InterruptFlags == SpellInterruptFlags.SPELL_INTERRUPT_FLAG_MOVEMENT){
			MonoBehaviour.print("~~~~ it is fucked up !!!!!");
		}
		*/
		//	if(checkSpell == null)//to not display unknown auras
		//	return;
		
		pkt.remainingData();

		Aura aura = new Aura(spellID, GetRealChar(uguid) as Unit);
		//		aura.spellID = spellID;
		aura.casterGuid = uguid;
		aura.auraSlot = auraSlot;
		
		//		Debug.Log("World: SMSG_AURA_UPDATE - guid: " + uguid + " slot: " + aura.auraSlot + " spellID: " + spellID);
		
		if(spellID>0)
		{
			aura.auraFlags = (AuraFlags)pkt.Read();
			aura.auraLevel = pkt.Read();
			aura.auraCharges = pkt.Read();
			
			AuraFlags flag = AuraFlags.AFLAG_NOT_CASTER;
			if((aura.auraFlags & flag) == 0)
			{
				ulong go = pkt.GetPacketGuid();
//				Debug.Log("CASTER IS NOT INFLUENCED: AURA SPELL CASTER GUID: " + go);
			}
			
			flag = 	AuraFlags.AFLAG_DURATION;
			if((aura.auraFlags & flag) > 0)
			{
				aura.auraMaxDuration = pkt.ReadReversed32bit();
				aura.auraDuration = pkt.ReadReversed32bit();
				aura.auraDuration *= 0.001f;
			}
		}
		
		if(player == null)
		{
			getPlayer();
		}
		Unit unit = GetRealChar(uguid) as Unit;
		if(unit != null)
		{
			if(unit.auraManager.mainPlayerGO != player.gameObject)
			{
				unit.auraManager.mainPlayerGO =  player.gameObject;
			}
			unit.auraManager.UpdateAura(aura, unit);
		}
	}
	
	void  HandleLearnedSpellOpcode (WorldPacket pkt)
	{
		getPlayer();
		if(player != null)
		{
			parsePlayerSpell(pkt);
			pkt.ReadType(2);		//a null value - 3.3f.3 unk
		}
	}
	//========================================================================TALENTS
	void  HandleTalentUpdateOpCode (WorldPacket pkt)
	{
		Debug.Log("RECIEVE PACKET: " + Time.time);
		try
		{
			//TalentHandler talenHandler = player.transform.GetComponentInChildren<TalentHandler>();
			TraitsManager.readPacketInfo(pkt);
			Debug.Log("SentTALENTUPDATE PACKET:");
		}
		catch(Exception err)
		{
			Debug.LogException(err);
		}
	}
	
	
	void  HandleLearnTalentGroupOpCode (WorldPacket pkt)
	{
		Debug.Log("learn talentgroup...");
	}
	
	
	//========================================================================Resurrect Spells
	void  HandleResurrectRequest (WorldPacket pkt)
	{
		ulong _casterGUID = pkt.ReadReversed64bit();
		uint _stringSize = pkt.ReadReversed32bit();
		string _casterName = "";
		Player _player;
		//checking to see if we were resurrected by a NPC
		_player = (_casterGUID == MainPlayer.GUID)? player : (GetRealChar(_casterGUID) as Player);
		_casterName = (_stringSize > 1)? pkt.ReadString() : _player.name;
		
		//reading the 0 that was sent after the string has ended
		pkt.Read();
		//byte _temp = pkt.Read();
		byte _casterType = pkt.Read(); // 0 - player, 1 - NPC
		
		if(_casterType == 1 || _stringSize > 1){
			WorldPacket newpkt = new WorldPacket();
			newpkt.SetOpcode(OpCodes.CMSG_RESURRECT_RESPONSE);
			newpkt.Append(ByteBuffer.Reverse(player.CasterGUID));
			byte _value = 1;
			newpkt.Append(_value);
			RealmSocket.outQueue.Add(newpkt);
		}
		else{
			player.CasterGUID = _casterGUID;
			player.ShowReviveDialogue = true;
			player.interf.reviveWindow.hidden = true;
		}
		Debug.Log("guid : " + _casterGUID + " stringSize: " + _stringSize + " casterName: " + (_casterName + "_|_") + " casterType: " + _casterType);
	}	
	
	
	public void  AcceptRevive ()
	{
		WorldPacket newpkt = new WorldPacket();
		newpkt.SetOpcode(OpCodes.CMSG_RESURRECT_RESPONSE);
		newpkt.Append(ByteBuffer.Reverse(player.CasterGUID));
		byte _value = 1;
		newpkt.Append(_value);
		RealmSocket.outQueue.Add(newpkt);
	}
	
	public void  DeclineRevive ()
	{
		WorldPacket newpkt = new WorldPacket();
		newpkt.SetOpcode(OpCodes.CMSG_RESURRECT_RESPONSE);
		newpkt.Append(ByteBuffer.Reverse(player.CasterGUID));
		byte _value = 0;
		newpkt.Append(_value);
		RealmSocket.outQueue.Add(newpkt);
		player.interf.reviveWindow.hidden = false;
	}
	//========================================================================Resurrect Spells END
	
	void  HandlePowerUpdate (WorldPacket pkt)
	{
		ulong guid = pkt.GetPacketGuid();
		Powers pw = (Powers)pkt.Read();// powerType
		float val = (float)pkt.ReadReversed32bit();
		
		if(pw == Powers.POWER_HAPPINESS && guid == player.petGuid)
		{
			if(val < 333000)
			{
				player.petHappines = "Hungry";
			}
			else if(val > 666000)
			{
				player.petHappines = "Overjoyed";
			}
			else
			{
				player.petHappines = "Satisfied";
			}
		}
		
		if(pw == Powers.POWER_RAGE)
		{
			val*=0.1f;
		}
		BaseObject ots = GetRealChar(guid);
		if(ots == null)
		{
			if(guid == player.guid)
			{
				ots = player;
			}
		}
	}	

	void  HandleSupercededSpellOpCode (WorldPacket pkt)
	{
		uint oldSpell = pkt.ReadReversed32bit();
		uint newSpell = pkt.ReadReversed32bit();
		getPlayer();
		if(player != null)
		{
			if (!verifyIfSpellIsTalent(newSpell) || isCanBeLearnedTalentSpell(newSpell))
			{
				player.playerSpellManager.RemoveSpell(oldSpell);
				player.playerSpellManager.AddSpell(SpellManager.CreateOrRetrive(newSpell));
			}
			player.actionBarManager.Replace(oldSpell, newSpell);
		}
		Debug.Log("SMSG_SUPERCEDEDSPELL "+ oldSpell+" new "+ newSpell);
//		player.actionBarPanel.RefreshSlots();
	}
	
	void  HandleLogoutResponse (WorldPacket pkt)
	{
		uint error;
		error = pkt.ReadReversed32bit();
		pkt.Read(); // could be some bool - always 0
		if(error > 0)
		{
			player.LoggingOut = false;
			player.interf.stateManager();
			player.AddChatInput("You can't log out right now!");
			Debug.Log("logout response error!");			
		}
		else
		{
			//================================
			player.interf.setInventoryState(false);
			ButtonOLD.questState =false;
			player.interf.SpellState(false);
			ButtonOLD.optionsState = false;
			ButtonOLD.partyState = false;
			ButtonOLD.wip1State = false;
			ButtonOLD.wip2State = false;
			ButtonOLD.mithrilShopState = false;
			MainPlayer.waitingToEquip = false;
			player.menuSelected=6;
			//================================
			player.LoggingOut = true;
			player.interf.stateManager();
			player.logOutTimer = 20;
			player.StartCoroutine(player.LogOutTimer());
			Debug.Log("logout response ok!");
		}
	}
	
	void  HandleLogoutComplete ()
	{
		LoadSceneManager.LoadingStatus = LoadSceneManager.LOADING.LOADING_INPROGRESS;
		Instantiate<GameObject>(Resources.Load<GameObject>("GUI/LoadingDummy1"));
		Debug.Log("logout complete!");
		Logout();
	}
	
	void  HandleLogoutCancelAck ()
	{
		Debug.Log("logout canceles!");
		player.LoggingOut = false;
		player.interf.stateManager();
	}
	
	void  HandleCancelChanneling (WorldPacket pkt, int temp){
		switch(temp)
		{
		case 1:
			MonoBehaviour.print("spell cancel channeling packet");// + pkt.ReadReversed32bit() + pkt.ReadReversed32bit() + pkt.ReadReversed32bit() + pkt.ReadReversed32bit());
			break;
		case 2:
			MonoBehaviour.print("spell break packet");
			break;
		case 3:
			MonoBehaviour.print("spell failed other packet");
			break;
		case 4:
			MonoBehaviour.print("spell failure");
			break;
		default:
			MonoBehaviour.print("entered default...");
			break;
		}
		
		ulong casterGUID = pkt.GetPacketGuid();
		byte castCount = pkt.Read();
		uint spellID = pkt.ReadReversed32bit();
		SpellCastResult spellCancelResult = (SpellCastResult)pkt.Read();
		switch(spellCancelResult)
		{
		case SpellCastResult.SPELL_FAILED_SUCCESS:
			#if UNITY_EDITOR
			Debug.Log("Cast "+spellID+" is SUCCESS");
			#endif
			break;
			#if UNITY_EDITOR
		default:
			Debug.Log("Spell "+spellID+" failed on cast - "+spellCancelResult);
			player.ShowErrorMessage("CastFailure");
			break;
			#endif
		}
		
		BaseObject obj;
		
		if(casterGUID == player.guid)
		{
			obj = player as BaseObject;
			
			//for main player only, cast bar related (stop it from showing and reset it's cooldowns and other variables)
			player.goCast = false;
			player.craftManager.countCraft = 0;
			interfaceManager.castBarMng.gameObject.SetActive(false);
			//			player.interf.progressBar.hidden = !player.goCast;
			
			
			if(player.getIsCasting() == spellID)
			{
				player.SetIsCasting(0);
			}
		}
		else
		{
			obj = GetRealChar(casterGUID);
		}
		
		interfaceManager.castBarMng.gameObject.SetActive(false);
		//		object.castTime = 0;
		//		object.castTimeTotal = 0;
		
		SpellVisual scriptSpellVisual = FxEffectManager.GetSpellVisual(spellID);
		if(scriptSpellVisual)
		{
			int effectId = scriptSpellVisual.effect[0];
			FxEffectManager.DestroyAndRemoveEffect(effectId, obj.guid);
			effectId = scriptSpellVisual.effect[2];
			FxEffectManager.DestroyAndRemoveEffect(effectId, obj.guid);
			effectId = scriptSpellVisual.persistentAreaKit;
			FxEffectManager.DestroyAndRemoveEffect(effectId, obj.guid);
		}
	}
	
	//============================================= End Spells Handlers  ==================================//
	
	
	
	
	//==========================================================================================================//
	//																											//	
	//												Combat Handlers				 								//
	//																											//
	//==========================================================================================================//	
	void HandleAttackStartOpcode(WorldPacket pkt)//===================/Attack Handlers/==============/  
	{
		//Debug.Log("HandleAttackStartOpcode");
		ulong actionerGUID = pkt.ReadReversed64bit();
		
		Unit attacker = GetRealChar(actionerGUID) as Unit;
		if(!attacker)
		{
			if(actionerGUID == MainPlayer.GUID)
			{
				attacker = player;
			}
			if(!attacker) // no attacker
			{
				return;
			}
		}
		if(actionerGUID == MainPlayer.GUID)
		{
			player.itemManager.DeactivateRange();
		}
		ulong targetGUID = pkt.ReadReversed64bit();
		Unit target = GetRealChar(targetGUID) as Unit;
		if(!target)
			if(targetGUID == MainPlayer.GUID)
		{
			target = player;
		}
		attacker.target = target;
		attacker.isAttacking = true;
	}
	
	void  HandleAttackStopOpcode (WorldPacket pkt){
		ulong actionerGUID = pkt.GetPacketGuid();
		
		Unit attacker = GetRealChar(actionerGUID) as Unit;
		if(!attacker)
		{
			if(actionerGUID == MainPlayer.GUID)
			{
				attacker = player;
			}
			if(!attacker) // no attacker
			{
				//MonoBehaviour.print("no attacker with guid : " + actionerGUID);
				return; 
			}
		}
		ulong targetGUID = pkt.ReadReversed64bit();
		//attacker.target = getObj(targetGUID); ///// why change te current targget????
		attacker.isAttacking = false;
		/// read another uint with value 0???
	}

	void HandleAttackStateUpdateOpcode(WorldPacket pkt) /// rewrite some of this code when we will have the animation handler
	{
		//		Debug.Log("wd");
		HitInfo hitInfo = (HitInfo)pkt.ReadReversed32bit();
		HitInfo type = HitInfo.HITINFO_CRITICALHIT;
		HitInfo critical = hitInfo & type;
		ulong attackerGUID = pkt.GetPacketGuid();
		ulong targetGUID = pkt.GetPacketGuid();
		uint dmg = pkt.ReadReversed32bit();
		
		if(!player)
		{
			getPlayer();
		}
		//if(targetGUID != MainPlayer.GUID)
		//	MonoBehaviour.print("dmg : " + dmg + " to mob/player with guid : " + targetGUID + " self guid : " + MainPlayer.GUID);
		uint overkill = pkt.ReadReversed32bit();
		if(overkill!=0)
		{
			if(targetGUID == player.guid)
			{
				player.HP=0;
			}
		}
		byte count = pkt.Read();/// allways 1 no need for a "for"
		/*byte i;
		for(int i = 0;i<count;i++)
		{
		}	*/
		uint schoolDmg = pkt.ReadReversed32bit();
		float damagef = pkt.ReadFloat();
		uint damageUI = pkt.ReadReversed32bit(); /// same as dmg  donno why is here too
		type = HitInfo.HITINFO_ABSORB | HitInfo.HITINFO_ABSORB2;
		uint absorb;
		if((hitInfo & type) > 0)
		{
			// same for as before
			absorb = pkt.ReadReversed32bit();
		}
		type = HitInfo.HITINFO_RESIST | HitInfo.HITINFO_RESIST2;
		uint rezist;
		if((hitInfo & type) > 0)
		{
			//same
			rezist = pkt.ReadReversed32bit();
		}
		byte targetState = pkt.Read();
		pkt.ReadType(4);
		pkt.ReadType(4); //just some 0s
		uint blocked;
		type = HitInfo.HITINFO_BLOCK;
		
		
		/// just some 0s left
		Unit target = GetRealChar(targetGUID) as Unit;
		if(target == null)
		{
			if(targetGUID == player.guid)
			{
				target = player;
			}
		}
		
		bool  isNeedEffect = false;
		if(( hitInfo & type) > 0)
		{
			blocked = pkt.ReadReversed32bit();
			//Unit obj = getObj(targetGUID) as Unit;
			if(target != null)
			{
				player.showDMG(target, "Blocked", schoolDmg, 0, critical > 0, attackerGUID);
				isNeedEffect = true;
			}
		}
		else if(target != null)
		{
			if(dmg != 0)
			{
				//tutorial stage code
				if (player.tutorialInstance != null)
				{
					if ((player.tutorialInstance.tutorialStep == TutorialStages.TUTORIAL_ATTACK_DUMMY) && player.target == target && target.Name == "Battle Dummy")
					{
						player.tutorialInstance.CompleteTutorialStage();
					}
				}
				//end of tutorial stage code
				
				player.showDMG(target, ""+dmg, schoolDmg, 0, critical > 0, attackerGUID);
				isNeedEffect = true;
			}
			else
			{
				player.showDMG(target, "Miss", schoolDmg, 0, critical > 0, attackerGUID);
				isNeedEffect = false;
			}
		}
		
		Unit attacker ;
		if(attackerGUID == player.guid)
		{
			attacker = player;
		}
		else
		{
			attacker = GetRealChar(attackerGUID) as Unit;
		}
		
		if(isNeedEffect)
		{
			SpellVisual scriptSpellVisual = FxEffectManager.GetSpellVisual(6603);
			if(scriptSpellVisual != null)
			{
				SpellEntry spellEntry = dbs.sSpells.GetRecord(6603);
				int effectId;
				GameObject effectObject;
				if(!CheckFxOptions(targetGUID))
				{
					effectId = scriptSpellVisual.effect[1];
					if(effectId > 0)
					{
						FxEffectManager.CastEffect(spellEntry.speed, 0f, GetGender(attackerGUID),
						                           effectId, GetRealChar(attackerGUID), GetRealChar(targetGUID));
					}
				}
				if(!CheckFxOptions(attackerGUID))
				{
					effectId = scriptSpellVisual.effect[0];
					if(effectId > 0)
					{
						FxEffectManager.CastEffect(spellEntry.speed, 0f, GetGender(attackerGUID),
						                           effectId, GetRealChar(attackerGUID), GetRealChar(attackerGUID));
					}
				}
			}
		}
		
		attacker.Attack(target);//// it will use animation handler.... 
		
		if(target != null)
		{
			target.aHandler.PlayActionAnimation("damage");
		}
	}
	//============================================= End Combat Handlers  ======================================//

	//==========================================================================================================//
	//																											//	
	//												Player Handlers				 								//
	//																											//
	//==========================================================================================================//	
	public void  SendQueryPlayerName (ulong guid)
	{
		WorldPacket pkt = new WorldPacket();
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.SetOpcode(OpCodes.CMSG_NAME_QUERY);
		RealmSocket.outQueue.Add(pkt);
	}
	
	void  HandleCriteriaUpdateOpcode (WorldPacket pkt)
	{
		uint id = pkt.ReadReversed32bit();
		ulong counter = pkt.GetPacketGuid();
		ulong guid = pkt.GetPacketGuid();
		pkt.ReadType(4);///  failed or not... donno why is uint... could be byte
		pkt.ReadType(4);// timer in seconds
		pkt.ReadType(4);// timer 1
		pkt.ReadType(4);// timer 2
		
		//player.print("id : " +id+ " counter : "+counter +" for mob with guid "+ guid + " where my guid = " + MainPlayer.GUID);
	}
	
	void HandleCharCreateResponseOpcode(WorldPacket pkt)//===================/Player Handlers/==============/
	{
		ResponseCodes createCharResponse = (ResponseCodes)pkt.Read();
		string showResponse = "";
		bool  createSuccess = false;
		
		switch(createCharResponse)
		{
		case ResponseCodes.CHAR_CREATE_DISABLED: 
			showResponse="Character Create disabled"; 
			break;
		case ResponseCodes.CHAR_CREATE_FAILED: 
			showResponse="Character Create failed";
			break;
		case ResponseCodes.CHAR_CREATE_EXPANSION: //prevent character creating Expansion race without Expansion acount
			showResponse="Character Create invalid. No expansion account"; 
			break;
		case ResponseCodes.CHAR_CREATE_EXPANSION_CLASS: //prevent character creating Expansion race without Expansion acount
			showResponse="Character Create invalid. No expansion account"; 
			break;
		case ResponseCodes.CHAR_NAME_NO_NAME: 
			showResponse="Character Create invalid name"; 
			break;
		case ResponseCodes.CHAR_NAME_RESERVED: 
			showResponse="This name is reserved and cannot be used."; 
			break;
		case ResponseCodes.CHAR_CREATE_NAME_IN_USE: 
			showResponse="This name is already in use. Please type-in another name."; 
			break;
		case ResponseCodes.CHAR_CREATE_ACCOUNT_LIMIT: 
			showResponse="Character Create account limit"; 
			break;
		case ResponseCodes.CHAR_CREATE_SERVER_LIMIT: 
			showResponse="Character Create server limit"; 
			break;
		case ResponseCodes.CHAR_CREATE_UNIQUE_CLASS_LIMIT: 
			showResponse="Character Create unique class limit"; 
			break;
		case ResponseCodes.CHAR_CREATE_LEVEL_REQUIREMENT: 
			showResponse="Character Create level requirement"; 
			break;
		case ResponseCodes.CHAR_CREATE_PVP_TEAMS_VIOLATION: 
			showResponse="Character Create pvp teams violation"; 
			break;
		case ResponseCodes.CHAR_CREATE_ERROR: 
			showResponse="Character Create error"; 
			break;
		case ResponseCodes.CHAR_CREATE_SUCCESS: 
			createSuccess = true;
			showResponse="Character was successfully created."; 
			break;
		}	
		
		if(showResponse == null)
		{
			if(createCharResponse != ResponseCodes.CHAR_NAME_SUCCESS)
			{
				showResponse = "Character Create name limitation";
			}
		}

		AccountCharactersList.instance.ShowMessage(showResponse);
		//SelectCharacterWindow.DestroyInstantiatedShieldAnimation("char_created");
		
		if(createSuccess)
		{
			//refresh char list
			RequestCharactersList();
		}
	}
	
	void  HandleStopMirrorTimerOpcode (WorldPacket pkt)
	{
		uint type = pkt.ReadReversed32bit();
		//player.print("timer stoped : " + type);
	}
	
	
	void  HandleTransferAborted (WorldPacket pkt)
	{
		MessageWnd wnd = new MessageWnd();
		wnd.text = "map ID: "+ pkt.ReadType(4);
		byte mesage = pkt.Read();
		switch(mesage)
		{
		case 7: wnd.text += "  Transfer aborted, Insufficient level."+ pkt.Read(); break; 
		case 8: wnd.text += "  Transfer aborted, Difficulty" + pkt.Read(); break;
		case 9: wnd.text += "  Transfer aborted, Unique mesaj" + pkt.Read(); break;
		default: Debug.Log("Transfer aborded, Unknown message " + pkt.Read()); break;
		}
		
		player.windows.Add(wnd);
		
		Debug.Log("SMSG_TRANFERABORDED - "+wnd.text+"   "+ mesage);
	}
	//============================================= End Player Handlers  =======================================//

	//==========================================================================================================//
	//																											//	
	//												Movement Handlers											//
	//																											//
	//==========================================================================================================//	
	bool isSpeedOpcode (OpCodes opc)
	{
		bool isSpeedOpcodeValue = false;
		switch(opc)
		{
		case OpCodes.MSG_MOVE_SET_WALK_SPEED:
		case OpCodes.MSG_MOVE_SET_RUN_SPEED:
		case OpCodes.MSG_MOVE_SET_RUN_BACK_SPEED:
		case OpCodes.MSG_MOVE_SET_SWIM_SPEED:
		case OpCodes.MSG_MOVE_SET_SWIM_BACK_SPEED:
		case OpCodes.MSG_MOVE_SET_TURN_RATE:
		case OpCodes.MSG_MOVE_SET_FLIGHT_SPEED:
		case OpCodes.MSG_MOVE_SET_FLIGHT_BACK_SPEED:
		case OpCodes.MSG_MOVE_SET_PITCH_RATE:
			isSpeedOpcodeValue = true;
			break;
		}
		return isSpeedOpcodeValue;
	}
	
	bool isMovementOpcode (OpCodes opc)
	{
		bool isMovementOpcodeValue = false;
		switch(opc)
		{
		case OpCodes.MSG_MOVE_SET_FACING:
		case OpCodes.MSG_MOVE_START_FORWARD:
		case OpCodes.MSG_MOVE_START_BACKWARD:
		case OpCodes.MSG_MOVE_STOP:
		case OpCodes.MSG_MOVE_START_STRAFE_LEFT:
		case OpCodes.MSG_MOVE_START_STRAFE_RIGHT:
		case OpCodes.MSG_MOVE_STOP_STRAFE:
		case OpCodes.MSG_MOVE_JUMP:
		case OpCodes.MSG_MOVE_START_TURN_LEFT:
		case OpCodes.MSG_MOVE_START_TURN_RIGHT:
		case OpCodes.MSG_MOVE_STOP_TURN:
		case OpCodes.MSG_MOVE_START_SWIM:
		case OpCodes.MSG_MOVE_STOP_SWIM:
		case OpCodes.MSG_MOVE_HEARTBEAT:
		case OpCodes.MSG_MOVE_FALL_LAND:
		case OpCodes.MSG_MOVE_TELEPORT_ACK:
			isMovementOpcodeValue = true;
			break;
		}
		return isMovementOpcodeValue;
	}
	
	void  HandleBotMove (WorldPacket pkt, BaseObject obj)
	{		
		uint movetime;
		uint waypoints;
		Vector3 pos;
		float time;
		byte pathType;
		
		pkt.Read();
		pos.x = pkt.ReadFloat();
		pos.z = pkt.ReadFloat();
		pos.y = pkt.ReadFloat();
		time = pkt.ReadFloat();
		pathType = pkt.Read();
		
		OtherPlayer bot = obj as OtherPlayer;
		
		bot.botFlag = true;
		
		bot.movetime = 0;
		
		switch(pathType) 
		{
		case 0: break; // normal packet
		case 1:
			bot.SetPosition(pos,0);
			return; // stop packet
		case 2: 
			//float unkf;
			//recvPacket >> unkf >> unkf >> unkf;
			pkt.ReadFloat();
			pkt.ReadFloat();
			pkt.ReadFloat();
			break;
		case 3: 
			ulong targetGuid = pkt.ReadReversed64bit();
			MonoBehaviour.print(targetGuid);
			if(targetGuid == player.guid)
				bot.target = player;
			else bot.target = GetRealChar(targetGuid);
			break;
		case 4: 
			pkt.ReadFloat();
			break;
		}
		
		bot.moveFlag = (MovementFlags)pkt.ReadReversed32bit();
		movetime = pkt.ReadReversed32bit();
		//MonoBehaviour.print("movrTime " + movetime);
		waypoints = pkt.ReadReversed32bit();
		
		Vector3 finish_point = new Vector3(0, 0, 0);
		finish_point.x = pkt.ReadFloat();
		finish_point.z = pkt.ReadFloat();
		finish_point.y = pkt.ReadFloat();
		
		bot.path = new Vector3[waypoints];
		uint offset;
		Vector3 middle = (pos + finish_point) / 2.0f;
		uint i = 0;
		
		//Debug.Log("waypoints count = " + waypoints );
		
		if(waypoints > 1 )
		{
			//uint i;
			for (i = 0; i < (waypoints - 1); i++)
			{
				offset = pkt.ReadReversed32bit();
				//obj.path[i] = new Vector3();
				uint x_offset = offset & 0x7ff;
				uint z_offset = ((offset >> 11) & 0x7ff);
				uint y_offset = ((offset >> 22) & 0x3ff);
				bot.path[i].x = middle.x - ((x_offset+1 >= 0x3ff) ? (x_offset | 0xfffff800) : (x_offset)) / 4;
				bot.path[i].y = middle.y - ((y_offset+1 >= 0x1ff) ? (y_offset | 0xfffffc00) : (y_offset)) / 4;
				bot.path[i].z = middle.z - ((z_offset+1 >= 0x3ff) ? (z_offset | 0xfffff800) : (z_offset)) / 4;
				bot.way=bot.path[i];
			}
		}
		
		bot.path[i] = finish_point;
		bot.way=finish_point;
		
		bot.movetimeMax=movetime / 1000.0f; //miliseconds -> seconds
		bot.movetime=movetime / 1000.0f;
		
		bot.InitMove();
	}

	void HandleMonsterMove(WorldPacket pkt)// TODO ... Not all data are used
	{	
		ulong guid = pkt.GetPacketGuid();
		
		if(guid == player.guid)
		{
			HandlePlayerMove(pkt, guid);
			return;
		}
		
		BaseObject obj = GetRealChar(guid);

		NPC npcObj = null;
		if(obj is OtherPlayer)
		{
			HandleBotMove(pkt, obj);
			return;
		}
		else if(obj is NPC)
		{
			npcObj = obj as NPC;
		}
		else
		{
			return;
		}
		uint movetime;
		uint waypoints;
		Vector3 pos;
		float time;
		byte pathType;
		
		pkt.Read();
		pos.x = pkt.ReadFloat();
		pos.z = pkt.ReadFloat();
		pos.y = pkt.ReadFloat();
		time = pkt.ReadFloat();
		pathType = pkt.Read();
		
		npcObj.movetime = 0;
		
		switch(pathType) 
		{
		case 0: break; // normal packet
		case 1:
			npcObj.SetPosition(pos,0);
			return; // stop packet
		case 2: 
			//float unkf;
			//recvPacket >> unkf >> unkf >> unkf;
			pkt.ReadFloat();
			pkt.ReadFloat();
			pkt.ReadFloat();
			break;
		case 3: 
			ulong targetGuid = pkt.ReadReversed64bit();
			MonoBehaviour.print(targetGuid);
			if(targetGuid == player.guid)
				npcObj.target = player;
			else npcObj.target = GetRealChar(targetGuid);
			break;
		case 4: 
			pkt.ReadFloat();
			break;
		}
		uint moveFlag = pkt.ReadReversed32bit();
		npcObj.moveFlag = (MovementFlags) moveFlag;
		movetime = pkt.ReadReversed32bit();
		
		npcObj.SetTime(movetime);
		waypoints = pkt.ReadReversed32bit();
		
		Vector3 finish_point = new Vector3(0, 0, 0);
		finish_point.x = pkt.ReadFloat();
		finish_point.z = pkt.ReadFloat();
		finish_point.y = pkt.ReadFloat();
		
		npcObj.path = new Vector3[waypoints];
		uint offset;
		Vector3 middle = (pos + finish_point) / 2.0f;
		uint i = 0;
		
		//Debug.Log("waypoints count = " + waypoints );
		
		if(waypoints > 1 )
		{
			//uint i;
			for (i = 0; i < (waypoints - 1); i++)
			{
				offset = pkt.ReadReversed32bit();
				//npcObj.path[i] = new Vector3();
				uint x_offset = offset & 0x7ff;
				uint z_offset = ((offset >> 11) & 0x7ff);
				uint y_offset = ((offset >> 22) & 0x3ff);
				npcObj.path[i].x = middle.x - ((x_offset+1 >= 0x3ff) ? (x_offset | 0xfffff800) : (x_offset)) / 4;
				npcObj.path[i].y = middle.y - ((y_offset+1 >= 0x1ff) ? (y_offset | 0xfffffc00) : (y_offset)) / 4;
				npcObj.path[i].z = middle.z - ((z_offset+1 >= 0x3ff) ? (z_offset | 0xfffff800) : (z_offset)) / 4;
				npcObj.way = (obj as NPC).path[i];
			}
		}
		
		npcObj.path[i] = finish_point;
		npcObj.way=finish_point;
		
		npcObj.movetimeMax=movetime / 1000.0f; //miliseconds -> seconds
		npcObj.movetime=movetime / 1000.0f;
		
		npcObj.initMove();
	}
	
	void  HandlePlayerMove (WorldPacket pkt, ulong guid)
	{
		uint movetime;
		uint moveFlag;
		uint waypoints;
		Vector3 pos;
		float time;
		byte pathType;
		
		player.botFlag = true;
		player.movetime = 0;
		
		pkt.Read();
		pos.x = pkt.ReadFloat();
		pos.z = pkt.ReadFloat();
		pos.y = pkt.ReadFloat();
		time = pkt.ReadFloat();
		pathType = pkt.Read();
		switch(pathType) 
		{
		case 0: break; // normal packet
		case 1:
			player.SetPosition(pos,0);
			return; // stop packet
		case 2:
			//float unkf;
			//recvPacket >> unkf >> unkf >> unkf;
			pkt.ReadFloat();
			pkt.ReadFloat();
			pkt.ReadFloat();
			break;
		case 3:
			ulong targetGuid = pkt.ReadReversed64bit();
			MonoBehaviour.print(targetGuid);
			if(targetGuid == player.guid)
				player.target = player;
			else player.target = GetRealChar(targetGuid);
			break;
		case 4:
			pkt.ReadFloat();
			break;
		}
		
		moveFlag = pkt.ReadReversed32bit();
		
		movetime = pkt.ReadReversed32bit();
		
		waypoints = pkt.ReadReversed32bit();
		
		Vector3 finish_point = new Vector3(0, 0, 0);
		finish_point.x = pkt.ReadFloat();
		finish_point.z = pkt.ReadFloat();
		finish_point.y = pkt.ReadFloat();
		
		player.path = new Vector3[waypoints];
		uint offset;
		Vector3 middle = (pos + finish_point) / 2.0f;
		uint i = 0;
		
		Debug.Log("Player waypoints count = " + waypoints);
		
		Debug.Log("Player start point " + i + ": x - " + pos.x + ", y - " + pos.y + ", z - " + pos.z);
		if(waypoints > 1 )
		{
			//uint i;
			for (i = 0; i < (waypoints - 1); i++)
			{
				offset = pkt.ReadReversed32bit();
				//obj.path[i] = new Vector3();
				uint x_offset = offset & 0x7ff;
				uint z_offset = ((offset >> 11) & 0x7ff);
				uint y_offset = ((offset >> 22) & 0x3ff);
				player.path[i].x = middle.x - ((x_offset+1 >= 0x3ff) ? (x_offset | 0xfffff800) : (x_offset)) / 4;
				player.path[i].y = middle.y - ((y_offset+1 >= 0x1ff) ? (y_offset | 0xfffffc00) : (y_offset)) / 4;
				player.path[i].z = middle.z - ((z_offset+1 >= 0x3ff) ? (z_offset | 0xfffff800) : (z_offset)) / 4;
				player.way = player.path[i];
				Debug.Log("Player waypoint " + i + ": x - " + player.path[i].x + ", y - " + player.path[i].y + ", z - " + player.path[i].z);
			}
		}
		
		Debug.Log("Player finish point " + i + ": x - " + finish_point.x + ", y - " + finish_point.y + ", z - " + finish_point.z);
		player.path[i] = finish_point;
		player.way=finish_point;
		
		player.movetimeMax=movetime / 1000.0f; //miliseconds -> seconds
		player.movetime=movetime / 1000.0f;
		
		player.InitMove();
	}

	void  HandleMovementOpcode (WorldPacket pkt)
	{
		//worldSession.log+="\nhendling movement";
		MovementFlags flags;
		ushort flags2;
		uint time;
		uint unk32;
		Vector3 pos = new Vector3();
		float o;
		ulong guid;
		
		guid = pkt.GetPacketGuid();
		OpCodes opcode = (OpCodes)pkt.GetOpcode();
		if(opcode == OpCodes.MSG_MOVE_TELEPORT_ACK)
		{
			pkt.ReadType(4);//0
			
			Debug.Log("OpCodes.MSG_MOVE_TELEPORT_ACK  "+guid);
			
			WorldPacket pkt2 = new WorldPacket();
			pkt2.SetOpcode(OpCodes.MSG_MOVE_TELEPORT_ACK);
			pkt2.AppendPackGUID(guid);
			flags = MovementFlags.MOVEMENTFLAG_FORWARD;
			pkt2.Append(ByteBuffer.Reverse((uint)flags));
			uint count = 1;
			pkt2.Append(ByteBuffer.Reverse(count));
			RealmSocket.outQueue.Add(pkt2);	
		}
		
		flags =  (MovementFlags)pkt.ReadReversed32bit();
		flags2 = (ushort)pkt.ReadType(2);
		time = (uint)pkt.ReadType(4);
		pos.x = pkt.ReadFloat();
		pos.y = pkt.ReadFloat();
		pos.z = pkt.ReadFloat();
		o = pkt.ReadFloat();
		unk32 = (uint)pkt.ReadType(4);
		Player ots = GetRealChar(guid) as Player;
		//MonoBehaviour.print(pos);
		
		if(ots)
		{
			ots.SetPosition(pos,o);
			
			//ots.SetOrientation(o);
			//ots.SetNextPosition(pos);
			ots.moveFlag = flags;//ByteBuffer.Reverse(flags);
//			MovementFlags flag = MovementFlags.MOVEMENTFLAG_FORWARD;
//			MovementFlags f = ots.moveFlag & flag;
			//Debug.Log("we are moving: " + f+"++++++++++++++++++++++++++");
			switch(opcode)
			{
			case OpCodes.MSG_MOVE_STOP :
				//	MonoBehaviour.print("OpCodes.MSG_MOVE_STOP " +pkt.GetOpcode());
				//ots.MoveStop();
				break;
			case OpCodes.MSG_MOVE_START_FORWARD:
				//MonoBehaviour.print("OpCodes.MSG_MOVE_START_FORWARD " +pkt.GetOpcode());
				//ots.MoveStartForward();
				break;
			case OpCodes.MSG_MOVE_START_TURN_LEFT:
				//MonoBehaviour.print("OpCodes.MSG_MOVE_START_TURN_LEFT " +pkt.GetOpcode());
				//ots.startRotateLeft();
				break;
			case OpCodes.MSG_MOVE_START_TURN_RIGHT:
				//MonoBehaviour.print("OpCodes.MSG_MOVE_START_TURN_RIGHT " +pkt.GetOpcode());
				//ots.startRotateRight();
				break;
			case OpCodes.MSG_MOVE_STOP_TURN:
				//MonoBehaviour.print("OpCodes.MSG_MOVE_STOP_TURN " +pkt.GetOpcode());
				//ots.stopRotate();
				break;
			case OpCodes.MSG_MOVE_START_STRAFE_LEFT :
				//MonoBehaviour.print("OpCodes.MSG_MOVE_START_STRAFE_LEFT " +pkt.GetOpcode());
				//ots.startMoveStrafeLeft();
				break;
			case OpCodes.MSG_MOVE_START_STRAFE_RIGHT :
				//MonoBehaviour.print("" +pkt.GetOpcode());
				//ots.startMoveStrafeRight();
				break;
			case OpCodes.MSG_MOVE_START_BACKWARD :
				//MonoBehaviour.print("OpCodes.MSG_MOVE_START_BACKWARD " +pkt.GetOpcode());
				//ots.MoveStartBackward();
				break;
			case OpCodes.MSG_MOVE_STOP_STRAFE :
				//MonoBehaviour.print("OpCodes.MSG_MOVE_STOP_STRAFE " +pkt.GetOpcode());
				//ots.stopStrafe();
				break;
			}
		}
		else if(player.guid == guid)
		{
			player.SetPosition(pos,o);
			//player.Falling();
			//Debug.Log("Player position: " + pos);
		}
		//else MonoBehaviour.print("no script for GUID :" + guid + "caractere in lista : "+realChars.Count);
	}
	
	void  HandleForceRunSpeedChange (WorldPacket pkt)
	{
		uint aux32;
		byte aux8;
		ulong uguid;
		
		uguid = pkt.GetPacketGuid();
		
		aux32 = (uint)pkt.ReadType(4);
		aux8 = pkt.Read();
		
		float speedRun = pkt.ReadFloat();
		
		getPlayer();
		if(player.guid == uguid)
		{
			player.SetSpeed(0, speedRun);
			return;
		}
		
		Unit obj = GetRealChar(uguid) as Unit;
		if(obj)
		{
			obj.SetSpeed(0, speedRun);
			return;
		}
		//	Debug.Log("New Speed : "+speedRun);
	}
	
	void  HandleForceMoveRoot (WorldPacket pkt)
	{
		//MonoBehaviour.print("Player is rooted....");
		ulong uguid = pkt.GetPacketGuid();
		
		getPlayer();
		if(uguid == player.guid){
			player.isMoving = false;
			player.isRooted = true;
			player.MoveStop();
		}
		else
		{
			Unit obj = GetRealChar(uguid) as Unit;
			if(obj)
				obj.isMoving = false;
		}   
	}
	
	void  HandleForceMoveUnRoot (WorldPacket pkt)
	{
		//MonoBehaviour.print("Player is no longer rooted....");
		ulong uguid = pkt.GetPacketGuid();
		
		getPlayer();
		if(uguid == player.guid)
		{
			player.isMoving = true;
			player.isRooted = false;
		}
		else
		{
			Unit obj = GetRealChar(uguid) as Unit;
			if(obj)
				obj.isMoving = true;
		}   
	}
	
	void  HandleSetSpeedOpcode (WorldPacket pkt)
	{
		ulong guid;
		Vector3 pos;
		float o;
		float speed;
		uint unk32;
		UnitMoveType movetype;
		ushort unk16;
		
		OpCodes opcode = (OpCodes)pkt.GetOpcode();
		switch(opcode)
		{
		case OpCodes.MSG_MOVE_SET_WALK_SPEED:
			movetype = UnitMoveType.MOVE_WALK;
			break;
			
		case OpCodes.MSG_MOVE_SET_RUN_SPEED:
			movetype = UnitMoveType.MOVE_RUN;
			break;
			
		case OpCodes.MSG_MOVE_SET_RUN_BACK_SPEED:
			movetype = UnitMoveType.MOVE_WALKBACK;
			break;
			
		case OpCodes.MSG_MOVE_SET_SWIM_SPEED:
			movetype = UnitMoveType.MOVE_SWIM;
			break;
			
		case OpCodes.MSG_MOVE_SET_SWIM_BACK_SPEED:
			movetype = UnitMoveType.MOVE_SWIMBACK;
			break;
			
		case OpCodes.MSG_MOVE_SET_TURN_RATE:
			movetype = UnitMoveType.MOVE_TURN;
			break;
			
		case OpCodes.MSG_MOVE_SET_FLIGHT_SPEED:
			movetype = UnitMoveType.MOVE_FLY;
			break;
			
		case OpCodes.MSG_MOVE_SET_FLIGHT_BACK_SPEED:
			movetype = UnitMoveType.MOVE_FLYBACK;
			break;
			
		case OpCodes.MSG_MOVE_SET_PITCH_RATE:
			movetype = UnitMoveType.MOVE_PITCH_RATE;
			break;
			
		default:
			return;
		}
		guid = pkt.GetPacketGuid();
		unk32 = (uint)pkt.ReadType(4);
		unk16 = (ushort)pkt.ReadType(2);
		unk32 = (uint)pkt.ReadType(4);/* getMSTime()*/
		pos.x = pkt.ReadFloat();
		pos.y = pkt.ReadFloat();
		pos.z = pkt.ReadFloat();
		o = pkt.ReadFloat();
		unk32 = (uint)pkt.ReadType(4);// falltime
		speed = pkt.ReadFloat();
		
		OtherPlayer obj = GetRealChar(guid) as OtherPlayer;
		MonoBehaviour.print("HandleSetSpeedOpcode - guid"+obj.guid+"   speed:"+speed);
		obj.SetSpeed(movetype, speed);
		obj.SetPosition(pos, o);
	}
	
	
	//============================================= End Movememnt Handlers  =======================================//
	
	
	//==========================================================================================================//
	//																											//	
	//												Party/Group Handlers										//
	//																											//
	//==========================================================================================================//	
	//Here be class info workaround for party members
	bool  partyMemberNeed = false;
	
	void HandlePartyMemberStatsOpcode(WorldPacket pkt)	///TODO:doesn´t have pet handler
	{
		if(!player)
			getPlayer();
		
		try
		{
			pkt.Read();											// const value - 0
			ulong guid = pkt.GetPacketGuid();
			PartyPlayer plr = player.group.GetPartyPlayer(guid);
			if(plr == null)
				return;
			
			uint hz = pkt.ReadReversed32bit();				// flag  TODO!!!!
			plr.status = (GroupMemberFlags)pkt.ReadReversed16bit();				// status
			
			if(plr.status == GroupMemberFlags.MEMBER_STATUS_OFFLINE)
			{
				player.group.updatePlayer(plr);
				return;
			}
			
			plr.HP = pkt.ReadReversed32bit();					// Health
			plr.maxHP = pkt.ReadReversed32bit();				// Max Health
			plr.powerType = (Powers)pkt.Read();			// powerType;
			plr.MP = (float)pkt.ReadReversed16bit();
			plr.maxMP = (float)pkt.ReadReversed16bit();
			if(plr.powerType == Powers.POWER_RAGE)
			{
				plr.MP *= 0.1f;
				plr.maxMP *= 0.1f;
			}
			
			plr.level = (uint)pkt.ReadReversed16bit();
			
			plr.zoneID = pkt.ReadReversed16bit();
			plr.positionX = pkt.ReadReversed16bit();
			plr.positionY = pkt.ReadReversed16bit();
			
			plr.auramask = pkt.ReadType(8);
			
			player.group.updatePlayer(plr);
			
			//Here be class info workaround for party members
			partyMemberNeed = true;
			SendQueryPlayerName(plr.guid);
			
		}
		catch(Exception e)
		{
			Debug.Log(e);
		}
	}
	
	void  HandleStartRaidReadyCheck (WorldPacket pkt)
	{
		ulong requestPlayerGuid = pkt.ReadReversed64bit();
		if(requestPlayerGuid != player.guid)
			player.group.prepareConfirmWindow();
	}
	
	void  HandleGroupInviteOpcode (WorldPacket pkt)
	{
		try
		{
			string name;
			pkt.Read();// value = 1
			name = pkt.ReadString();
			player.interf.InviteToParty(name);
		}
		catch(Exception e)
		{
			Debug.Log(e);
		}
	}
	
	void  HandlePartyCommandResultOpcode (WorldPacket pkt)
	{
		PartyOperation operation;
		string nume;
		PartyResult res;//PartyResult
		operation = (PartyOperation)pkt.ReadType(4);
		nume = pkt.ReadString();
		res = (PartyResult)pkt.ReadReversed32bit();
		// anther uint equal to 0
		if(operation == PartyOperation.PARTY_OP_INVITE)
		{
			MessageWnd window = new MessageWnd();
			window.NumberOfButtons = 1;
			switch(res)
			{
			case PartyResult.ERR_PARTY_RESULT_OK:
				return;
			case PartyResult.ERR_BAD_PLAYER_NAME_S:
				window.text = "The Player does not exist or is offline!";
				break;
			case PartyResult.ERR_GROUP_FULL:
				window.text = "The group is full!";
				break;
			case PartyResult.ERR_NOT_LEADER:
				window.text = "You are not the leader if the group!";
				break;
			case PartyResult.ERR_PLAYER_WRONG_FACTION:
				window.text = "WRONG FACTION!";
				break;
			case PartyResult.ERR_IGNORING_YOU_S:
				window.text = "The player is ignoring you!";
				break;
			default :
				return;
			}
			player.windows.Add(window);
			//MonoBehaviour.print(player.windows.length);
			//MonoBehaviour.print(window.rect);
		}
		else if(operation == PartyOperation.PARTY_OP_LEAVE)
		{
			player.group.clearGroupMember();
		}
	}
	
	
	void  HandleGroupListOpcode (WorldPacket pkt)
	{
		try
		{
			//MonoBehaviour.print("Group List");
			// disband and replace
			player.group.clearGroupMember();
			
			GroupManager updateGroup = player.group;
			
			updateGroup.groupType = (GroupType)pkt.Read();// group Type
			updateGroup.groupID = pkt.Read();// group id
			updateGroup.groupFlags = pkt.Read();// group flags
			updateGroup.isBGGroup = pkt.Read();// is battle ground group?
			
			updateGroup.addMainPlayerToGroup();
			
			if(updateGroup.groupType == GroupType.GROUPTYPE_LFD)
			{
				pkt.Read();// 0;
				pkt.ReadType(4);//0
			}
			else if(updateGroup.groupType == GroupType.GROUPTYPE_CHANGE_GROUP)
			{
				updateGroup.clearGroupMember();
				return;/// we are out of the group.... kill  the party and return
			}
			
			updateGroup.voiceChat = pkt.ReadType(8);//0x50000000FFFFFFFELL mabe voice chat
			pkt.ReadType(4);// 0
			updateGroup.membersCount = pkt.ReadReversed32bit(); // minus mainPlayer
			//MonoBehaviour.print(updateGroup.membersCount);
			
			for(byte i = 0;i<updateGroup.membersCount;i++)
			{
				PartyPlayer plr = new PartyPlayer();
				
				plr.name = pkt.ReadString();
				plr.guid = pkt.ReadReversed64bit();
				plr.status = (GroupMemberFlags)pkt.Read();
				plr.groupID = pkt.Read();
				plr.flags = pkt.Read();
				pkt.Read();// 0
				updateGroup.addPlayer(plr);
			}
			
			updateGroup.leaderGUID = pkt.ReadReversed64bit();
			updateGroup.lootMethod = (LootMethod)pkt.Read();
			updateGroup.looterGUID = pkt.ReadReversed64bit();
			updateGroup.lootThreshold = (ItemQualities)pkt.Read();// lootThreshold
			updateGroup.dungeonDifficulty = pkt.Read();//dungeonDifficulty
			updateGroup.raidDifficulty = (Difficulty)pkt.Read();//raid Difficulty
			pkt.Read();//0
			
			SendRaidRequest();
		}
		catch(Exception e)
		{
			Debug.Log(e);
		}
	}
	
	public void  SendRaidRequest ()
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_REQUEST_RAID_INFO, 0);
		RealmSocket.outQueue.Add(pkt);
	}
	//============================================= End party/Group Handlers  ==================================//

	//==========================================================================================================//
	//																											//	
	//										Instances Handlers											//
	//																											//
	//==========================================================================================================//	
	void HandleUpdateInstanceOwnershipOpcode(WorldPacket pkt)//======================Instances Handlers==================//
	{
		uint hasBeenSaved = (uint)pkt.ReadType(4);
		Debug.Log("Instances saved: "+hasBeenSaved);		
	}
	
	void  InstanceResponse (MessageWnd mW)
	{
		if(mW.text != null)
		{
			GUI.Label(new Rect(mW.rect.width*0.01f,mW.rect.height*0.1f,mW.rect.width * 0.99f, mW.rect.height*0.88f),mW.text, player.newskin.customStyles[8]);//,skin.customStyles[2]);
		}
		
		if(GUI.Button(new Rect(mW.rect.width*0.38f, mW.rect.height*0.75f, mW.rect.width*0.33f, mW.rect.height*0.2f),"Close", player.newskin.customStyles[9]))
		{
			mW.Yes();
		}
	}
	
	void  HandleInstanceResetOpcode (WorldPacket pkt)
	{
		//player.bind.homeBindAreaId
		Debug.Log("SMSG_INSTANCE_RESET "+pkt.ReadReversed32bit());
		
		MessageWnd wnd = new MessageWnd();
		wnd.rect = new Rect(Screen.width*0.35f,Screen.height*0.25f,Screen.width*0.4f,Screen.height*0.4f);
		wnd.text = "Instance reset successful";	
		wnd.OnGUI = InstanceResponse;
		player.windows.Add(wnd);
		
	}
	void  HandleInstanceResetFailedOpcode (WorldPacket pkt)
	{
		Debug.Log("SMSG_INSTANCE_RESET_FAILED ");
		
	}
	
	void  HandleInstanceDifficultyOpcode (WorldPacket pkt)
	{
		uint difficulty = (uint)pkt.ReadType(4);
		pkt.ReadType(4);//null val
		
		//	Debug.Log("Instances difficulty: "+difficulty);		
	}
	
	void  HandleRaidGroupOnlyOpcode (WorldPacket pkt)
	{
		uint homeBindeTimer = pkt.ReadReversed32bit();
		uint error = (uint)pkt.ReadType(4);
		
		//Debug.Log("Instances homebindeTimer: "+homeBindeTimer);		
		MessageWnd wnd = new MessageWnd();
		wnd.text = "Instance HomeBideTimer: in "+homeBindeTimer+" secondes";
		//wnd.time = homeBindeTimer;
		
		player.windows.Add(wnd);	
	}
	
	void  HandleRaidInstanceMessageOpcode (WorldPacket pkt)
	{
		InstanceResetWarningType type = (InstanceResetWarningType)pkt.ReadReversed32bit();
		uint mapID = pkt.ReadReversed32bit();
		uint difficulty = pkt.ReadReversed32bit();
		uint time = pkt.ReadReversed32bit();
		
		if(type == InstanceResetWarningType.RAID_INSTANCE_WELCOME)
		{
			pkt.Read();//null value
			pkt.Read();//null value
			Debug.Log("Instance reset warnings - Welcome");
		}
		
		//if(type == InstanceResetWarningType.RAID_INSTANCE_WELCOME)
		Debug.LogError("Instance reset warnings - time: "+ Global.GetTimeAsString(time, false));
		
	}
	
	void  confirmRaidReady (MessageWnd mW)
	{
		GUI.Label(new Rect(0,20,mW.rect.width,mW.rect.height),mW.text);
		
		if(GUI.Button(new Rect(0,mW.rect.height-20,mW.rect.width*0.4f,20),"Confirm"))
		{
			mW.Yes();
			
			WorldPacket pkt = new WorldPacket(OpCodes.MSG_RAID_READY_CHECK,8);
			RealmSocket.outQueue.Add(pkt);
		}
		
		if(GUI.Button(new Rect(mW.rect.width-30,mW.rect.height-20,mW.rect.width*0.4f,20),"Leave"))
		{
			mW.No();
			handleOpcode(mW.opcodes[0],null);
		}
	}
	
	void  HandleRaidReadyCheckOpcode (WorldPacket pkt)
	{
		if(!player)
			getPlayer();
		
		//byte i = player.group.GetPartyPlayerIndex(pkt.GetPacketGuid());
		//Debug.Log("Player  "+((PartyPlayer)(player.group.partyMembers[i])).name+"  confirm the raid");
	}
	
	void  HandleRaidReadyCheckConfirmOpcode (WorldPacket pkt)
	{
		if(!player)
			getPlayer();
		
		player.group.prepareConfirmWindow();
		
		PartyPlayer pp = player.group.GetPartyPlayer(pkt.GetPacketGuid());
		pp.status = (GroupMemberFlags)pkt.Read();
		
		MessageWnd wnd = new MessageWnd();
		wnd.text = "Comfirm Raid START";
		wnd.opcodes[0] = OpCodes.CMSG_GROUP_DECLINE;
		wnd.OnGUI = confirmRaidReady;
		player.windows.Add(wnd);
	}
	
	void  HandleRaidInstanceInfoOpcode (WorldPacket pkt)
	{
		RaidWindowManager.instanceCooldownList.Clear();
		uint counter = pkt.ReadReversed32bit();
		for(int index = 0; index < counter; index++)
		{
			uint instanceID = pkt.ReadReversed32bit();
			uint instanceDifficultID = pkt.ReadReversed32bit();
			ulong instanceGUID = pkt.ReadReversed64bit();
			pkt.Read();			// default = 1
			pkt.Read();			// default = 0
			uint resetTime = pkt.ReadReversed32bit();

			IDictionary instanseInfo = DefaultTabel.mapTemplate[instanceID.ToString()] as IDictionary;
			string instanceDifficultName = GetInstanceDifficult(instanceDifficultID, (uint)instanseInfo["AreaType"]);
			string instanceName = (string)instanseInfo["MapName"];
			
			string cooldownTime = Global.GetTimeAsString(resetTime, false);
			InstanceCooldown instanceCooldown = new InstanceCooldown(instanceID, instanceName, instanceDifficultName, cooldownTime);
			
			RaidWindowManager.instanceCooldownList.Add(instanceCooldown);
		}
	}
	
	private string GetInstanceDifficult (uint id, uint areaType)
	{
		string ret = "Normal";
		switch(areaType)
		{
		case 1:
			switch(id)
			{
			case 0:
				ret = "Normal";
				break;
			case 1:
				ret = "Legendary";
				break;
			}
			break;
		case 2:
			switch(id)
			{
			case 0:
				ret = "10-man";
				break;
			case 1:
				ret = "25-man";
				break;
			case 2:
				ret = "10-man Legendary";
				break;
			case 3:
				ret = "25-man Legendary";
				break;
			}
			break;
		}
		return ret;
	}
	
	void  HandlerRaidTargetUpdate (WorldPacket pkt)
	{
		if(pkt.Read() == 1)// update  target icon
		{	
			for(int i = 0;i<8/*TARGET_ICON_COUNT*/;i++)
			{
				
			}
		}
		else//recive target icon
		{
			pkt.ReadType(8);
			pkt.Read();
			pkt.ReadType(8);
		}
	}
	//============================================= End Instances Handlers  =======================================//
	
	void  HandleTimeSyncReqOpcode (WorldPacket pkt)
	{
		uint timeSyncCounter = pkt.ReadReversed32bit();
//		Debug.Log("timeSyncCounter:"+timeSyncCounter);
//		uint aux32 =  1;
//		pkt = new WorldPacket(OpCodes.CMSG_TIME_SYNC_RESP, 4+4);
//		pkt.Append(ByteBuffer.Reverse(timeSyncCounter));
//		pkt.Append(ByteBuffer.Reverse(aux32));
//		RealmSocket.outQueue.Add(pkt);
	}

	//==========================================================================================================//
	//																											//	
	//												Quests Handlers				 								//
	//																											//
	//==========================================================================================================//	
	void  HandleGossipComplete (WorldPacket pkt)
	{
		MonoBehaviour.print("Wolrd: SMSG_GOSSIP_COMPLETE ");
	}
	
	/**
	 *	Recive answer with list of Gossip menu items
	 */
	
	void  HandleGossipMessageOpcode (WorldPacket pkt){
		ulong guid = pkt.ReadReversed64bit();
		NPC npc = GetRealChar(guid) as NPC;
		
		if(!npc)
			return;
		
		byte j;
		npc.gossip.SetMenuId(pkt.ReadReversed32bit());
		npc.gossip.SetTitleId(pkt.ReadReversed32bit());
		uint menuCount = pkt.ReadReversed32bit();
		
		List<GossipMenuItem> menuItems = new List<GossipMenuItem>();
		GossipMenuItem gmi;
		for(j =0; j<menuCount; j++)
		{
			gmi = new GossipMenuItem();
			
			pkt.ReadReversed32bit();
			gmi.m_Icon = pkt.Read();
			gmi.m_Coded = pkt.Read();
			gmi.m_BoxMoney = pkt.ReadReversed32bit();
			gmi.m_Message = pkt.ReadString();
			gmi.m_BoxMessage = pkt.ReadString();
			
			menuItems.Add(gmi);
		}
		
		npc.gossip.AddMenuItems(menuItems);
		
		uint questMenuCount = pkt.ReadReversed32bit();
		if(questMenuCount>0)
		{
			player.showQuest = true;
		}
		else
		{
			Debug.Log("No questMenuCount!");
		}	
		
		for(j =0;j<questMenuCount;j++)
		{
			quest qst = new quest();
			qst.questId =  pkt.ReadReversed32bit();
			pkt.ReadReversed32bit();
			qst.level = pkt.ReadReversed32bit();
			pkt.ReadReversed32bit();
			pkt.Read();//3.3f.3 changes icon: blue question or yellow exclamation				
			qst.title = pkt.ReadString();
			
			if(qst.questId > 0)
			{
				npc.questGiver.addQuest(qst); 
			}
		}
		
		if(menuCount > 0 && !npc.isPortalMasterTriggered())
		{
			interfaceManager.SetWindowState(InterfaceWindowState.NPC_WINDOW_STATE);
			interfaceManager.npcWindowMng.InitGossipWindow(npc.gossip);
			interfaceManager.hideInterface = true;
			player.interf.stateManager();
		}
	}
	
	
	void  HandleQuestUpdateAddKill (WorldPacket pkt)
	{
		uint questEntry = pkt.ReadReversed32bit();
		uint creatureOrGoldEntry = pkt.ReadReversed32bit();
		uint currentCreatureOrGoldCount = pkt.ReadReversed32bit();
		var creatureOrGoldCount= pkt.ReadReversed32bit();
		ulong npcGuid = pkt.ReadType(8);
		
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_QUESTGIVER_STATUS_MULTIPLE_QUERY);
		RealmSocket.outQueue.Add(pkt);	
	}
	
	void  HandleQuestqiverRequestStatus (WorldPacket pkt)
	{
		ulong guid = pkt.ReadReversed64bit();
		byte stat = pkt.Read();
		NPC npc = GetRealChar(guid) as NPC;
		
		//	MonoBehaviour.print("World: SMSG_QUESTGIVER_STATUS- "+npc.guid+"  stat "+stat);
		uint flag= (uint)NPCFlags.UNIT_NPC_FLAG_QUESTGIVER;	
		if(npc != null && (npc.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0)
		{
			npc.changeStatus(stat);
		}
	}
	
	void  HandleQuestqiverRequestStatusMultiple (WorldPacket pkt)
	{
		uint n = pkt.ReadReversed32bit();
		NPC npc;
		byte status;
		for(byte i=0; i<n;i++)
		{
			npc = GetRealChar(pkt.ReadReversed64bit()) as NPC;
			status = pkt.Read();
			uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_QUESTGIVER;	
			if(npc != null && (npc.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0)
			{
				npc.changeStatus(status);
			}
		}
	}

	void  HandleQuestqiverRequestItems (WorldPacket pkt)
	{
		try
		{
			//Debug.Log("Server Questgiver request items ");
			ulong npcGuid = pkt.ReadReversed64bit();
			uint questID = pkt.ReadReversed32bit();
			NPC npc = GetRealChar(npcGuid) as NPC;
			if(npc == null)
				return;
			//Debug.Log("Server Questgiver request items 22222");
			//npc.player = player;	
			int index = npc.questGiver.getQuestIndex(questID);
			quest qst;
			if(index < 0)
			{
				//		Debug.Log("Server Questgiver request items ");
				qst = new quest();
			}
			else
			{
				qst = npc.questGiver.quests[index];
			}
			
			qst.questId = questID;
			qst.title = pkt.ReadString();
			qst.progress = pkt.ReadString();
			
			//string rewTxt =
			pkt.ReadReversed32bit();//ceva null
			pkt.ReadReversed32bit();//complete/incomplete emote
			pkt.ReadReversed32bit(); // Close Window after cancel
			pkt.ReadReversed32bit();//quest flag
			pkt.ReadReversed32bit();//suggestedGroupNum
			pkt.ReadReversed32bit();//rew/req money?
			
			qst.getReqItemsCount = (int)pkt.ReadReversed32bit();
			for(int i = 0;i<qst.getReqItemsCount ;i++)
			{
				qst.reqItemId[i] = pkt.ReadReversed32bit();
				
				qst.reqItemCount[i] = pkt.ReadReversed32bit();//reqItemCount
				pkt.ReadReversed32bit();//displayInfoID
				Debug.Log(" neeeeeeeeeeeeeeeeeeeeeeeed  "+qst.reqItemId[i]+ "  "+qst.reqItemCount[i]);
			}
			
			// Completable = flags1 && flags2 && flags3 && flags4		
			pkt.ReadReversed32bit();// flags1
			pkt.ReadReversed32bit();// flags2
			pkt.ReadReversed32bit();// flags3
			pkt.ReadReversed32bit();// flags4	
			
			if(index<0)	
				npc.questGiver.quests.Add(qst);
			
			npc.questGiver.prepareReqQuestItems(qst);
			npc.questGiver.completeQuestStages = 0;	
			
			//MonoBehaviour.print("npc: !"+ npcGuid+ "  questId: "+ questID + " title: "+qst.title + "Numar de iteme necesare: " + reqItems +" Entryul itemului: "+ entry );
			
			
		}
		catch(Exception e)
		{
			MonoBehaviour.print(e);
		}
	}		

	void HandleQuestgiverOfferReward(WorldPacket pkt)//not all data used
	{
		Debug.Log("World: SMSG_QUESTGIVER_OFFER_REWARD - ");
		ulong npcGuid = pkt.ReadReversed64bit();
		NPC npc = GetRealChar(npcGuid) as NPC;
		
		if(npc == null)
		{
			return;
		}
		//	npc.player = player;
		
		uint qstId = pkt.ReadReversed32bit();
		int index = npc.questGiver.getQuestIndex(qstId);
		quest qst;
		//Debug.Log("sssssssss  "+index);
		if(index < 0)
		{	
			Debug.Log("World: SMSG_QUESTGIVER_OFFER_REWARD - No quest Found!!!");
			qst = new quest();
		}
		else	
			qst = npc.questGiver.quests[index];
		
		qst.questId = qstId;
		qst.title = pkt.ReadString();//title
		qst.offerRewText = pkt.ReadString();//rew text		
		qst.autoFinish = pkt.Read();//auto finish
		
		pkt.ReadReversed32bit();//quest flag
		
		pkt.ReadReversed32bit();//suggestedGroupNum
		
		uint emo = pkt.ReadReversed32bit();//emote Count;
		
		for(int i = 0;i<emo;i++)
		{
			pkt.ReadReversed32bit();// Delay Emote
			pkt.ReadReversed32bit();//OfferRewardEmote
		}
		
		uint rewChoiseItems = pkt.ReadReversed32bit();
		for(int i = 0;i<rewChoiseItems ;i++)
		{
			uint entry = pkt.ReadReversed32bit();
			qst.rewChoiceItemId[i] = (entry);//reqItemId
			if(qst.rewChoiceItemId[i] > 0)
			{
				Item itm = new Item();
				itm.entry = entry;
				npc.questGiver.addItem(itm);
			}
			pkt.ReadReversed32bit();//reqItemCount
			pkt.ReadReversed32bit();//displayInfoID
		}
		
		uint rewItemsCount = pkt.ReadReversed32bit();
		for(int i = 0;i< rewItemsCount;i++)
		{
			uint entry2 = pkt.ReadReversed32bit();
			qst.rewItemId[i] = (entry2);//reqItemId
			if(qst.rewItemId[i] > 0)
			{
				Item itm2 = new Item();
				itm2.entry = entry2;
				npc.questGiver.addItem(itm2);
			}
			pkt.ReadReversed32bit();//rew item count
			pkt.ReadReversed32bit();//displayinfoID
			
		}
		
		qst.moneyAmaunt = pkt.ReadReversed32bit();
		qst.XPAmount = pkt.ReadReversed32bit();
		
		npc.questGiver.completeQuestStages = 2;
		
		if(index < 0)
		{
			npc.questGiver.quests.Add(qst);
		}
		//others.................................
	}
	
	
	
	void  HandleQuestqiverQuestComplete (WorldPacket pkt)
	{
		uint questID = pkt.ReadReversed32bit();
		var str= "";
		str += "Received " + pkt.ReadReversed32bit() + " xp.";
		pkt.ReadReversed32bit();
		pkt.ReadReversed32bit();
		pkt.ReadReversed32bit();
		pkt.ReadReversed32bit();
		player.AddChatInput(str);
		player.showQuest = false;
		player.target  =null;
		WorldPacket pkt3 = new WorldPacket();
		pkt3.SetOpcode(OpCodes.CMSG_QUESTGIVER_STATUS_MULTIPLE_QUERY);
		RealmSocket.outQueue.Add(pkt3);	
	}
	
	void  HandleQuestqiverQuestDetailsOpcode (WorldPacket pkt)
	{
		//MonoBehaviour.print("World: SMSG_QUESTGIVER_QUEST_DETAILS! ");
		ulong uguid = pkt.ReadReversed64bit();
		NPC npc = GetRealChar(uguid) as NPC;
		if(npc == null)
		{
			MonoBehaviour.print("World: SMSG_QUESTGIVER_QUEST_DETAILS- NPC not found!");
			return;
		}
		//npc.player = player;
		// wotlk, something todo with quest sharing?
		pkt.ReadReversed64bit();
		uint questId = pkt.ReadReversed32bit();
		int index = npc.questGiver.getQuestIndex(questId);
		
		quest qst;
		if(index < 0)
			qst = new quest();
		else	
			qst = npc.questGiver.quests[index];
		
		qst.questId = questId;
		
		qst.title = pkt.ReadString();//name
		qst.details = pkt.ReadString();//detalii
		qst.objectives = pkt.ReadString();//objectives
		
		qst.autoFinish = pkt.Read();//auto finish
		qst.getQuestFlags = pkt.ReadReversed32bit();
		qst.getSuggestedPlayers = pkt.ReadReversed32bit();
		qst.isFinished = pkt.Read(); // IsFinished? value is sent back to server in quest accept packet
		// QUEST_FLAGS_HIDDEN_REWARDS = 0x00000200 == 512	// Items and money rewarded only sent in SMSG_QUESTGIVER_OFFER_REWARD (not in SMSG_QUESTGIVER_QUEST_DETAILS or in client quest log(SMSG_QUEST_QUERY_RESPONSE))
		//if(index<0)
		//{
		
		//	return;
		
		//}
		byte i;
		uint flagHiddenRewards = (uint)QuestFlags.QUEST_FLAGS_HIDDEN_REWARDS;
		if((qst.getQuestFlags & flagHiddenRewards) > 0)
		{
			pkt.ReadReversed32bit(); // Rewarded chosen items hidden
			pkt.ReadReversed32bit(); // Rewarded items hidden
			pkt.ReadReversed32bit();// Rewarded money hidden
			pkt.ReadReversed32bit(); // Rewarded XP hidden
		}
		else
		{
			qst.getRewChoiceItemCount = pkt.ReadReversed32bit();	
			for(i = 0;i<qst.getRewChoiceItemCount;i++)
			{
				uint entry = pkt.ReadReversed32bit();
				qst.rewChoiceItemId[i] = entry;
				qst.rewChoiceItemCount[i] = pkt.ReadReversed32bit();
				qst.displayChoiceInfoId[i] = pkt.ReadReversed32bit();
				AppCache.sItems.SendQueryItem(entry);
			}
			
			qst.getItemsCount = pkt.ReadReversed32bit();
			for(i = 0;i<qst.getItemsCount;i++)
			{
				uint entry2 = pkt.ReadReversed32bit();
				qst.rewItemId[i] = entry2;
				qst.rewItemCount[i] = pkt.ReadReversed32bit();
				qst.displayInfoId[i] = pkt.ReadReversed32bit();
				AppCache.sItems.SendQueryItem(entry2);
			}	
			// send rewMoneyMaxLevel explicit for max player level, else send RewOrReqMoney
//			if(2==1/*daca playerLevel == Max*/)
//				qst.getRewMoneyMaxLevel = pkt.ReadReversed32bit();
//			else
				qst.getRewOrReqMoney = pkt.ReadReversed32bit();
			
			qst.xpValue = pkt.ReadReversed32bit(); 
		}
		
		qst.rewHonorAddition = pkt.ReadReversed32bit();
		qst.rewHonorMultiplier = pkt.ReadFloat();
		qst.rewSpell = pkt.ReadReversed32bit();
		qst.rewCastSpell = pkt.ReadReversed32bit();
		qst.charTitleId = pkt.ReadReversed32bit();
		qst.bonusTalents = pkt.ReadReversed32bit();
		pkt.ReadReversed32bit();// bonus arena points
		pkt.ReadReversed32bit();// rep reward show mask?
		
		for(i = 0;i<QUEST_REPUTATIONS_COUNT;i++)
			qst.rewRepFaction[i] = pkt.ReadReversed32bit();	
		for(i = 0;i<QUEST_REPUTATIONS_COUNT;i++)
			qst.rewRepValueId[i] = pkt.ReadReversed32bit();
		for(i = 0;i<QUEST_REPUTATIONS_COUNT;i++)
			qst.rewRepValue[i] = pkt.ReadReversed32bit();	
		
		qst.questEmoteCount = pkt.ReadReversed32bit(); // QUEST_EMOTE_COUNT = 4
		
		for(i = 0;i<qst.questEmoteCount;i++)
		{
			qst.detailsEmote[i] = pkt.ReadReversed32bit();
			qst.detailsEmoteDelay[i] = pkt.ReadReversed32bit();
		}
		
		if(index<0)//in case npc has just one quest available.Server dosent send the quest list anymore, just this opcode
			npc.questGiver.addQuest(qst);
		
		npc.questGiver.prepareRewQuestItems(qst);//gets quest items info (name, atributes) 
		npc.questGiver.goShowQuestDetails = true;
		npc.questGiver.completeQuestStages = 0;
		//MonoBehaviour.print("World: SMSG_QUESTGIVER_QUEST_DETAILS! "+questId);
		//npc.questGiver.completeQuest = false;
	}
	
	
	
	void HandleQuestQueryResponse(WorldPacket pkt)//not all data used   
	{
		try{
			var questId= pkt.ReadReversed32bit();//quest Id	
			//	MonoBehaviour.print("World: SMSG_QUEST_QUERY_RESPONSE- QuestEntry: "+questId);
			
			int index = player.questManager.getQuestIndex(questId);
			
			if(index<0)
			{
				MonoBehaviour.print("World: SMSG_QUEST_QUERY_RESPONSE- QuestEntry: "+questId+" .Quest not found!");
				return;
			}
			
			quest qst =  player.questManager.quests[index];
			
			qst.questId = questId;
			
			qst.questMethod = pkt.ReadReversed32bit();//
			
			qst.level = pkt.ReadReversed32bit();//level
			////////////
			qst.minLevel = pkt.ReadReversed32bit();//
			qst.zoneOrSort = pkt.ReadReversed32bit();
			
			qst.type = pkt.ReadReversed32bit();
			qst.suggestedPlayers = pkt.ReadReversed32bit();
			
			qst.repObjectiveFaction = pkt.ReadReversed32bit();
			qst.repObjectiveValue = pkt.ReadReversed32bit();
			
			pkt.ReadReversed32bit();
			pkt.ReadReversed32bit();
			
			qst.nextQuestInChain = pkt.ReadReversed32bit();
			qst.rewXPId = pkt.ReadReversed32bit();
			
			qst.rewOrRegMoney = pkt.ReadReversed32bit();
			
			qst.rewMoneyMaxLevel = pkt.ReadReversed32bit();
			qst.rewSpell = pkt.ReadReversed32bit();
			qst.rewCastSpell = pkt.ReadReversed32bit();
			//reward honor points
			qst.rewHonorAddition = pkt.ReadReversed32bit();
			qst.rewHonorMultiplier = pkt.ReadFloat();
			
			qst.scrItemId = pkt.ReadReversed32bit();
			qst.questFlags = pkt.ReadReversed32bit();
			qst.charTitleId = pkt.ReadReversed32bit();
			qst.playersSlain = pkt.ReadReversed32bit();
			qst.bonusTalents = pkt.ReadReversed32bit();
			pkt.ReadReversed32bit();
			pkt.ReadReversed32bit();
			
			byte i;
			for(i = 0;i<4;i++)
			{
				qst.rewItemId[i] = pkt.ReadReversed32bit();
				AppCache.sItems.SendQueryItem(qst.rewItemId[i]);
				qst.rewItemCount[i] = pkt.ReadReversed32bit();
			}
			
			for(i = 0;i<6;i++)
			{
				qst.rewChoiceItemId[i] = pkt.ReadReversed32bit();
				AppCache.sItems.SendQueryItem(qst.rewChoiceItemId[i]);
				qst.rewChoiceItemCount[i] = pkt.ReadReversed32bit();
			}
			
			for(i = 0;i<5;i++)
				qst.rewRepFaction[i] = pkt.ReadReversed32bit();	
			for(i = 0;i<5;i++)
				qst.rewRepValueId[i] = pkt.ReadReversed32bit();
			for(i = 0;i<5;i++)
				qst.rewRepValue[i] = pkt.ReadReversed32bit();	
			
			
			qst.pointMapId = pkt.ReadReversed32bit();
			qst.pointX = pkt.ReadFloat();
			qst.pointY = pkt.ReadFloat();
			qst.pointOpt = pkt.ReadReversed32bit();
			
			qst.title = pkt.ReadString();//name
			qst.objectives = pkt.ReadString();//objectives
			qst.details = pkt.ReadString();//detalii
			
			qst.details = qst.details.Replace("CHAR_CLASS", Global.GetClassAsString(WorldSession.player.playerClass ) );  // complete quest details with name and class
			qst.details = qst.details.Replace("CHAR_NAME", WorldSession.player.name );
			
			qst.endText = pkt.ReadString();
			qst.completedText = pkt.ReadString();
			
			for(i = 0;i<4;i++)
			{
				qst.reqCreatureOrGold[i] = pkt.ReadReversed32bit();
				qst.reqCreatureOrGoldCount[i] = pkt.ReadReversed32bit();
				qst.reqSourceId[i] = pkt.ReadReversed32bit();
				qst.reqSourceCount[i] = pkt.ReadReversed32bit();
			}		
			
			for(i = 0;i<6;i++)
			{
				qst.reqItemId[i] = pkt.ReadReversed32bit();
				qst.reqItemCount[i] = pkt.ReadReversed32bit();
			}
			
			for(i = 0;i<4;i++)
			{
				qst.objectiveText[i] = pkt.ReadString();
			}
			
			//	player.questManager.quests.Add(qst);
			
		}catch(Exception e){MonoBehaviour.print(e);}
		
	}
	
	void  HandleQuestConfirmAcceptOpcode (WorldPacket pkt)
	{
		Debug.Log("World: SMSG_QUEST_CONFIRM_ACCEPT- Chose!");
		MessageWnd wnd = new MessageWnd();
		wnd.text = "Accept quest?";
		wnd.opcodes[0]=OpCodes.CMSG_QUEST_CONFIRM_ACCEPT;
		wnd.opcodes[1]=OpCodes.CMSG_QUESTLOG_REMOVE_QUEST;
		player.windows.Add(wnd);
		
	}
	
	void  HandleQuestgiverQuestListOpcode (WorldPacket pkt)
	{
		ulong npcGuid = pkt.ReadReversed64bit();
		//Debug.Log("World: SMSG_QUESTGIVER_QUEST_LIST - npc: "+npcGuid);
		NPC npc = GetRealChar(npcGuid) as NPC;
		if(npc == null)
		{
			return;
		}
		string title = pkt.ReadString();
		pkt.ReadType(4);
		pkt.ReadType(4);
		uint meniuItemCount = pkt.Read();
		//	Debug.Log("World: SMSG_QUESTGIVER_QUEST_LIST - npc: "+npcGuid+ " questsCount: "+meniuItemCount);
		
		for(byte m=0;m<meniuItemCount;m++)
		{
			quest qst = new quest();
			
			qst.questId = pkt.ReadReversed32bit();
			//	Debug.Log("World: SMSG_QUESTGIVER_QUEST_LIST - npc: "+npcGuid+ " quest: "+qst.questId);
			pkt.ReadType(4);//icon
			qst.level = pkt.ReadReversed32bit();
			qst.questFlags = pkt.ReadReversed32bit();
			pkt.Read();
			qst.title = pkt.ReadString();
			
			WorldPacket pkt2 = new WorldPacket(OpCodes.CMSG_QUESTGIVER_REQUEST_REWARD,100);
			pkt2.Append(ByteBuffer.Reverse(npcGuid));
			pkt2.Append(ByteBuffer.Reverse(qst.questId));
			RealmSocket.outQueue.Add(pkt2);	
			
			npc.questGiver.addQuest(qst);
			
			//Debug.Log("World: SMSG_QUESTGIVER_QUEST_LIST - npc: "+npcGuid+ " quest: "+qst.questId+"  Added");
			//npc.questGiver.quests.Add(qst);
		}
		//	npc.goShowQuestDetails=false;
	}
	//============================================= End Quest Handlers  =======================================//
	
	void  HandleTrainerListOpCode (WorldPacket pkt)
	{
		ulong npcGuid = pkt.ReadReversed64bit();
		NPC obj = GetRealChar(npcGuid) as NPC;
		
		Debug.Log("World: SMSG_TRAINER_LIST- guid:"+npcGuid);
		if(obj == null || !obj.isTrainer())
		{
			return;
		}
		
		interfaceManager.SetWindowState(InterfaceWindowState.NPC_WINDOW_STATE);
		interfaceManager.npcWindowMng.InitTrainerWindow(npcGuid, pkt);
		
		//	obj.trainer = new TrainerManager(interfaceManager.trainerWnd, npcGuid);
		//	obj.trainer.InitTrainerSpellList(pkt);
		//	interfaceManager.trainerWnd.trainerManager = obj.trainer;
	}
	
	void  HandleTrainerBuySuccededOpcode (WorldPacket pkt)
	{
		NPC npc = GetRealChar(pkt.ReadReversed64bit()) as NPC;
		if(npc != null && npc.trainer != null)
		{
			uint spellID = pkt.ReadReversed32bit();
			npc.trainer.SpellBuySucceeded(spellID);
			if(spellID == 264)
			{
				uint shootEntry = dbs.staticWOWToWOM(3018);
				WorldPacket tempPacket = new WorldPacket();
				tempPacket.SetOpcode(OpCodes.CMSG_LEARN_SPELL);
				tempPacket.Append(shootEntry);
				RealmSocket.outQueue.Add(tempPacket);
			}
		}
		
		//Debug.Log("You learned the spell successfully.");
	}
	
	void  HandleTrainerBuyFailedOpcode (WorldPacket pkt)
	{
		Debug.LogWarning("You did not learn the spell. Please try again later.");
	}
	
	void  HandleTestPlayerCreatedOpcode (WorldPacket pkt)
	{
		if(AnimViewer.current_obj)
			Destroy(AnimViewer.current_obj);
		string namestr = pkt.ReadString();
		byte typeb = pkt.Read();
		if(typeb == 1)
			AnimViewer.current_obj = Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/chars/"+namestr));
		else
			AnimViewer.current_obj = Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/mobs/"+namestr));
		Debug.Log("Could not load "+ namestr+"  "+typeb);
		
		UnityEngine.Object ff = AnimViewer.current_obj.transform.Find("obj").gameObject.GetComponent<OtherPlayer>();
		if(ff != null)
		{
			Destroy(ff);
		}
		ff = AnimViewer.current_obj.transform.Find("obj").gameObject.GetComponent<NPC>();
		if(ff != null)
		{
			Destroy(ff);
		}
		TestPlayer tps = AnimViewer.current_obj.AddComponent<TestPlayer>();
		if(typeb == 1)
			tps.ischar = true;
		else
			tps.ischar = false;
		//Debug.Log(str);
		float x = pkt.ReadFloat();
		float y = pkt.ReadFloat();
		float z = pkt.ReadFloat();
		Transform tt = AnimViewer.current_obj.transform;
		tt.position=new Vector3(x, y, z);
		//if(tps.ischar)
		//tps.EquipArmor("Standard");
		Debug.Log(x+" "+y+" "+" "+z);
	}
	
	void  HandleTestPlayerUpdatedOpcode (WorldPacket pkt)
	{
		if(AnimViewer.current_obj)
		{
			if(AnimViewer.Weapons == null)
			{
				AnimViewer.Weapons = Resources.LoadAll<GameObject>("prefabs/weapons");
			}
			string anim_str;
			byte anim_mode;
			string weapon;
			string armor;
			
			anim_str = pkt.ReadString();
			weapon = pkt.ReadString();
			armor = pkt.ReadString();
			anim_mode = pkt.Read();
			Debug.Log("anim: "+anim_str+" mode: "+anim_mode+" weapon: "+weapon+" armor "+armor);
			
			TestPlayer tps = AnimViewer.current_obj.GetComponent<TestPlayer>();
			
			//if(!tps.ischar)
			//	return;
			
			if(tps.current_weapon != weapon)
			{
				for(byte i=0;i<AnimViewer.Weapons.Length;i++)
				{
					GameObject weapon_obj = AnimViewer.Weapons[i];
					if(weapon_obj.name == weapon)
						tps.EquipWeapon(null,tps.rightHand,weapon_obj);
				}
			}
			
			if(tps.current_armor_set != armor)
				tps.EquipArmor(armor);
			
			if(tps.current_animation != anim_str || tps.current_anim_mode != anim_mode)
				tps.ChangeAnimation(anim_str,anim_mode);
		}
	}
	
	//===============================vendor handlers
	void  HandleSellItem (WorldPacket pkt)
	{
		ulong npcGuid;
		uint itemGuid;
		byte msg;
		npcGuid = pkt.ReadReversed64bit();
		itemGuid = pkt.ReadReversed32bit();
		msg = pkt.Read();
		NPC npc;
		npc = GetRealChar(npcGuid) as NPC;
		if(npc != null && npc.vendor != null)
		{
			npc.vendor.vendorUI.showSellError((SellFailure)msg);
		}
	}
	
	void  HandleBuyError (WorldPacket pkt)
	{
		ulong npcGuid;
		uint itemId;
		byte msg;
		npcGuid = pkt.ReadReversed64bit();
		itemId = pkt.ReadReversed32bit();
		msg = pkt.Read();
		NPC npc;
		npc = GetRealChar(npcGuid) as NPC;
		if(npc != null && npc.vendor != null)
		{
			npc.vendor.vendorUI.showBuyError((BuyFailure)msg);
		}
	}
	
	void  HandleListInventory (WorldPacket pkt)
	{
		NPC npc;
		ulong npcGuid;
		byte numItems;
		npcGuid = pkt.ReadReversed64bit();
		numItems = pkt.Read();
		if(numItems == 0)
		{
			pkt.Read(); // error
		}
		else
		{
			var vendorItems= new List<Item>();
			var vendorItemsExtra= new List<VendorItem>();
			Item tmpItem = null;
			VendorItem tempItem;
			for(byte i = 0; i < numItems; i++)
			{
				tmpItem = new Item();
				tempItem = new VendorItem();
				tempItem.vendorSlot = pkt.ReadReversed32bit();
				tempItem.itemId = pkt.ReadReversed32bit();
				(tmpItem as Item).entry = tempItem.itemId;
				tempItem.displayInfoId = pkt.ReadReversed32bit();
				tempItem.maxCount = pkt.ReadReversed32bit();
				tempItem.price = pkt.ReadReversed32bit();
				tempItem.maxDurability = pkt.ReadReversed32bit();
				tempItem.buyCount = pkt.ReadReversed32bit();
				tempItem.extendedCost = pkt.ReadReversed32bit();
				tempItem.BuildStatsString();
				vendorItems.Add(tmpItem);
				vendorItemsExtra.Add(tempItem);
				AppCache.sItems.SendQueryItem(tempItem.itemId);
			}
			
			npc = GetRealChar(npcGuid) as NPC;
			if(npc != null && npc.vendor != null)
			{
				if(npc.vendor.vendorItems != null)
				{
					npc.vendor.vendorItems.Clear();
					npc.vendor.vendorItems = null;
					npc.vendor.vendorItemsExtra = null;
				}
				npc.vendor.vendorItems = vendorItems;
				npc.vendor.vendorItemsExtra = vendorItemsExtra;
				npc.vendor.vendorUI.SetMaxPage();
				//npc.vendor.vendorUI.ShowVendorSlots();
				npc.needUpdate = true;
				npc.startTime = Time.time;
			}
			npc = null;
			vendorItems = null;
			vendorItemsExtra = null;
		}
	}
	
	// banker
	void  HandleShowBank (WorldPacket pkt)
	{
		NPC npc;
		ulong npcGuid;
		npcGuid = pkt.ReadReversed64bit();
		npc = GetRealChar(npcGuid) as NPC;
		//		if(npc && npc.banker)
		//		{
		//			npc.banker.Enable();
		//		}
		if(npc != null && npc.isBanker())
		{
			interfaceManager.SetWindowState(InterfaceWindowState.NPC_WINDOW_STATE);
			interfaceManager.npcWindowMng.SetWindowState(NpcWindowState.BANKER_WINDOW);
			interfaceManager.npcWindowMng.bankerWnd.SetBankerGuid(npcGuid);
			
			WorldSession.player.blockTargetChange = true;
			WorldSession.player.interf.stateManager();
		}
		
	}
	
	/*
		ERR_BANKSLOT_FAILED_TOO_MANY	= 0,
		ERR_BANKSLOT_INSUFFICIENT_FUNDS = 1,
		ERR_BANKSLOT_NOTBANKER		  = 2,
		ERR_BANKSLOT_OK				 = 3
	*/
	void  HandleBuyBankSlotResult (WorldPacket pkt)
	{
		uint error;
		error = pkt.ReadReversed32bit();
		switch(error)
		{
		case 0:
			interfaceManager.DrawMessageWindow("There are no more slots to buy.");
			break;
		case 1:
			interfaceManager.DrawMessageWindow("You don't have enough mithril.");
			break;
		case 2:
			interfaceManager.DrawMessageWindow("I'm not a banker.");
			break;
		case 3:
			interfaceManager.DrawMessageWindow("Added new slot to your bank.");
			break;
		}
	}
	
	// mithril
	static public void  RequestMithrilCoinsCount ()
	{
		WorldPacket tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_MITHRIL_COUNT_INFO);
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
	
	public static void  RequestRefreshMithrilCoinsCount ()
	{
		WorldPacket tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_REFRESH_MITHRIL_COUNT_INFO);
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
	
	void  HandleMithrilCoinsCount (WorldPacket pkt)
	{
		uint mithrilCoins = 0;
		mithrilCoins = pkt.ReadReversed32bit();
		//Debug.Log("Mithril count: " + asdf);
		player.mithrilCoins = mithrilCoins;
	}
	
	bool verifyIfSpellIsTalent ( uint spell )
	{
		bool ret = false;
		for (var i= 0; i < dbs.sTalents.Records; i++)
		{
			TalentEntry talentEntry = dbs.sTalents.GetRecordFromIndex(i);
			if (spell == talentEntry.SpellID1
			    || spell == talentEntry.SpellID2
			    || spell == talentEntry.SpellID3
			    || spell == talentEntry.SpellID4
			    || spell == talentEntry.SpellID5)
			{
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	void  HandleLoginSetTimeSpeed ( WorldPacket pkt )
	{	
//		uint min_mask = 0x3f;
		uint hr_mask = 0x7C0;
//		uint wday_mask = 0x3800;
//		uint mday_mask = 0xFC000;
//		uint mon_mask = 0xF00000;
//		uint year_mask = 0xF000000;
		uint datetime = pkt.ReadReversed32bit();
		float speed = pkt.ReadFloat();
		uint bytes = pkt.ReadReversed32bit();
//		uint min = min_mask & datetime;
		uint hr = (hr_mask & datetime) >> 6;
//		uint wday = (wday_mask & datetime) >> 11;
//		uint mday_data = (mday_mask & datetime) >> 14;
//		uint mon_data = (mon_mask & datetime) >> 20;
//		uint year_data = (year_mask & datetime) >> 24;
//		uint mon = mon_data +1;
//		uint mday = mday_data + 1;
//		uint year = year_data + 100 + 1900;
		//Debug.Log("datetime:"+datetime+"\nmin:"+min+"\nhr:"+hr);
		var time=System.DateTime.Now;
		PlayerPrefs.SetInt("GMTdifference", (time.Hour>hr) ? time.Hour-(int)hr : (time.Hour+24)-(int)hr );
	}

	//==========================================================================================================//
	//																											//	
	//										OpCodes/Packets Handlers											//
	//																											//
	//==========================================================================================================//	
	void  HandleDestroyOpcode (WorldPacket pkt)
	{
		ulong guid;
		byte aux;
		guid = pkt.ReadReversed64bit();
		aux = pkt.Read();
		//	Debug.Log("Se face destroy");
		remove(guid); // handles items and units
	}

	public void  handlePkt (WorldPacket pkt)
	{
		ulong gameObjectGuid;
		uint animationId;
		BaseObject fishingBobberGO;
		Animator fishingBobberAnimator;
		OpCodes opcode = (OpCodes)pkt.GetOpcode();
		try // memory loss
		{
			//Debug.Log("HandlePkt: " + pkt.GetOpcode());
			if(isMovementOpcode(opcode))
			{
				//MonoBehaviour.print("MovementOpcode - "+ pkt.GetOpcode());
				HandleMovementOpcode(pkt);
				return;
			}
			
			if(isSpeedOpcode(opcode))//do nothing 
			{
				//MonoBehaviour.print("SpeedOpcode - "+ pkt.GetOpcode());
				HandleSetSpeedOpcode(pkt);
				return;
			}
			if(opcode != OpCodes.SMSG_MONSTER_MOVE && opcode != OpCodes.SMSG_TIME_SYNC_REQ)
			{
				if(opcode == OpCodes.SMSG_GOSSIP_MESSAGE)
				{
					Debug.Log ("Catch!");
				}
			}
			switch(opcode)
			{
			case OpCodes.SMSG_UPDATE_INSTANCE_OWNERSHIP:
				HandleUpdateInstanceOwnershipOpcode(pkt);	
				break;
			case OpCodes.SMSG_INSTANCE_DIFFICULTY:
				HandleInstanceDifficultyOpcode(pkt);	
				break;
			case OpCodes.SMSG_RAID_INSTANCE_MESSAGE:
				HandleRaidInstanceMessageOpcode(pkt);
				break;
			case OpCodes.SMSG_INSTANCE_RESET  :
				HandleInstanceResetOpcode(pkt);
				break;	
			case OpCodes.SMSG_INSTANCE_RESET_FAILED :
				HandleInstanceResetFailedOpcode(pkt);
				break;
			case OpCodes.SMSG_RAID_GROUP_ONLY:
				HandleRaidGroupOnlyOpcode(pkt);
				break;
//			case OpCodes.MSG_RAID_READY_CHECK_CONFIRM: 
//				HandleRaidReadyCheckConfirmOpcode(pkt);
//				Debug.Log("**************************OpCodes.MSG_RAiD_READY_CHECK_CONFIRM");
//				break;
			case OpCodes.SMSG_RAID_INSTANCE_INFO:
				Debug.Log("**************************OpCodes.SMSG_RAID_INSTANCE_INFO");
				HandleRaidInstanceInfoOpcode(pkt);
				break;					
				//??????????????????????		
			case OpCodes.MSG_RAID_READY_CHECK:
				HandleStartRaidReadyCheck(pkt);
				break;				
			case OpCodes.SMSG_PARTY_MEMBER_STATS_FULL :
				HandlePartyMemberStatsOpcode(pkt);
				break;
			case OpCodes.SMSG_GROUP_INVITE : 
				MonoBehaviour.print("invite info");
				HandleGroupInviteOpcode(pkt);
				break;
			case OpCodes.SMSG_PARTY_COMMAND_RESULT:
				MonoBehaviour.print("party result");
				HandlePartyCommandResultOpcode(pkt);
				break;
			case OpCodes.SMSG_GROUP_LIST :
				HandleGroupListOpcode(pkt);
				break;
				
			case OpCodes.SMSG_GAMEOBJECT_QUERY_RESPONSE:
				HandleGameobjectQueryResponse(pkt);
				break;
				
				//teleport opcode
			case OpCodes.SMSG_TRANSFER_ABORTED:
				HandleTransferAborted(pkt);
				break;
				//speed change OpCodes		
			case OpCodes.SMSG_FORCE_RUN_SPEED_CHANGE:
				HandleForceRunSpeedChange(pkt);
				break;
			case OpCodes.SMSG_FORCE_MOVE_ROOT:
				HandleForceMoveRoot(pkt);
				break;	
			case OpCodes.SMSG_FORCE_MOVE_UNROOT:
				HandleForceMoveUnRoot(pkt);
				break;				
				//==================================================================================TALENT OpCodes
			case OpCodes.SMSG_TALENT_UPDATE:
				HandleTalentUpdateOpCode(pkt);
				break;
				//this needs to be removed from here.... (:
			case OpCodes.CMSG_LEARN_TALENT_GROUP:
				HandleLearnTalentGroupOpCode(pkt);
				break;
			case OpCodes.CMSG_LEARN_TALENT:
				HandleLearnTalentGroupOpCode(pkt);
				break;
				//==================================================================================RESSURECT OpCodes
			case OpCodes.SMSG_RESURRECT_REQUEST:
				Debug.Log("ress request...");
				HandleResurrectRequest(pkt);
				break;
			case OpCodes.SMSG_RESURRECT_FAILED:
				Debug.Log("ress failed...");
				//HandleResurrectFailed(pkt);
				break;
			case OpCodes.SMSG_PRE_RESURRECT:
				Debug.Log("pre resurrect...");
				//HandllePreResurrect(pkt);
				break;
				//==================================================================================Spells/Auras OpCodes				
			case OpCodes.SMSG_AURA_UPDATE:
				HandleAuraUpdateOpcode(pkt);
				break;
			case OpCodes.SMSG_AURA_UPDATE_ALL:
				HandleAuraUpdateAllOpcode(pkt);
				break;
			case OpCodes.SMSG_SPELL_START:
				HandleSpellStartOpCode(pkt);
				break;
			case OpCodes.SMSG_SPELL_GO:
				HandleSpellGoOpCode(pkt);
				break;
			case OpCodes.SMSG_LEARNED_SPELL:
				HandleLearnedSpellOpcode(pkt);
				break;
				
				
			case OpCodes.SMSG_PET_GUIDS:
				HandlePetGuidslOpcode(pkt);
				break;
			case OpCodes.SMSG_PET_SPELLS:
				player.petSpellManager.HandlePetSpellsOpcode(pkt);
				break;
				
				//=== 
			case OpCodes.SMSG_SUPERCEDED_SPELL:
				HandleSupercededSpellOpCode(pkt);
				break;
				
			case OpCodes.CMSG_CANCEL_CHANNELLING:
				HandleCancelChanneling(pkt, 1);
				break;
				
			case OpCodes.SMSG_SPELLBREAKLOG:
				HandleCancelChanneling(pkt, 2);
				break;
				
			case OpCodes.SMSG_SPELL_FAILURE:
				HandleCancelChanneling(pkt, 4);
				break;
				
			case OpCodes.SMSG_SPELL_FAILED_OTHER:
				HandleCancelChanneling(pkt, 3);
				break;	
				
			case OpCodes.SMSG_CAST_FAILED:
				HandleCastFailedOpcode(pkt);
				break;
			case OpCodes.SMSG_SPELLNONMELEEDAMAGELOG:
				HandleSpellNoMeleeDamageLogOpCode(pkt);
				break;
			case OpCodes.SMSG_PERIODICAURALOG:
				HandlePeriodicAuraLogOpcode(pkt);
				break;
			case OpCodes.SMSG_SPELLHEALLOG:
				HandleSpellHealLogOpcode(pkt);
				break;
			case OpCodes.SMSG_UPDATE_COMBO_POINTS:
				HandleUpdateComboPointsOpCode(pkt);
				break;
				
			case OpCodes.SMSG_SET_PCT_SPELL_MODIFIER:
			case OpCodes.SMSG_SET_FLAT_SPELL_MODIFIER:
				HandlerSetSpellModModifierOpCode(pkt);
				break;
				
			case OpCodes.SMSG_REMOVED_SPELL:
				MonoBehaviour.print("unlearn spell...");
				if(!player) getPlayer();
				player.playerSpellManager.RemoveSpell(pkt.ReadReversed32bit());
				break;	
			case OpCodes.SMSG_SPELL_DELAYED:
				HandleSpellDelayedOpCode(pkt);
				break;	
				//==================================================================================Trainer OpCodes		
			case OpCodes.SMSG_TRAINER_LIST:
				HandleTrainerListOpCode(pkt);
				break;	
				
			case OpCodes.SMSG_TRAINER_BUY_SUCCEEDED:
				HandleTrainerBuySuccededOpcode(pkt);
				break;
				
			case OpCodes.SMSG_TRAINER_BUY_FAILED:
				HandleTrainerBuyFailedOpcode(pkt);
				break;								
				//==================================================================================END Trainer OpCodes				
			case OpCodes.SMSG_AUTH_CHALLENGE :
				HandleAuthChallengeOpcode(pkt);
				//byteBuffer.log+="\nsmsg challange : " + pkt.GetOpcode();
				break;
			case OpCodes.SMSG_AUTH_RESPONSE :
				//byteBuffer.log+="\nAuthresponse : " + pkt.GetOpcode();
				HandleAuthResponseOpcode(pkt);
				break;
			case OpCodes.SMSG_CHAR_ENUM :
				//byteBuffer.log+="\nchar enum : " + pkt.GetOpcode();
				HandleCharEnumOpcode(pkt);
				break;
			case OpCodes.SMSG_COMPRESSED_UPDATE_OBJECT :
				HandleCompressedUpdateObjectOpcode(pkt);
				break;
			case OpCodes.SMSG_UPDATE_OBJECT :
				HandleUpdateObjectOpcode(pkt);
				break; 
			case OpCodes.SMSG_CHAR_CREATE:
				HandleCharCreateResponseOpcode(pkt);
				break;
			case OpCodes.SMSG_CHAR_DELETE:
				RequestCharactersList();
				break;
			case OpCodes.SMSG_DESTROY_OBJECT :
				//	MonoBehaviour.print("delete object");
				HandleDestroyOpcode(pkt);
				break;
			case OpCodes.SMSG_LOGOUT_RESPONSE:
				HandleLogoutResponse(pkt);
				break;
			case OpCodes.SMSG_LOGOUT_CANCEL_ACK:
				HandleLogoutCancelAck();
				break;
			case OpCodes.SMSG_LOGOUT_COMPLETE :
				HandleLogoutComplete();
				break;
			case OpCodes.SMSG_NAME_QUERY_RESPONSE :
				//MonoBehaviour.print("char info");
				HandleNameQueryResponseOpcode(pkt);
				break;
			case OpCodes.SMSG_CREATURE_QUERY_RESPONSE :
				HandleCreatureQueryResponseOpcede(pkt);
				break;
				
			case OpCodes.SMSG_MESSAGECHAT :
				//MonoBehaviour.print("chat info");
				HandleChatMessageOpcode(pkt);
				break;
			case OpCodes.SMSG_CHANNEL_NOTIFY :
				HandleChannelNotify(pkt);
				break;
			case OpCodes.SMSG_NEW_WORLD:
				// new world
				//MonoBehaviour.print("new world");
				HandleNewWorldOpcode(pkt);
				break;
			case OpCodes.SMSG_AREA_TRIGGER_MESSAGE :
				HandleAreaTriggerMessage(pkt);
				break;
			case OpCodes.SMSG_POWER_UPDATE :
				HandlePowerUpdate(pkt);
				break;
			case OpCodes.SMSG_NOTIFICATION :
				HandleNotificationOpcode(pkt);
				break;
			case OpCodes.SMSG_MONSTER_MOVE:
				HandleMonsterMove(pkt);
				break;
				
			case OpCodes.SMSG_DURABILITY_DAMAGE_DEATH: //Main player is death
				Debug.Log("OpCodes.SMSG_DURABILITY_DAMAGE_DEATH");
				//	player.isMoving = false;
				break;					
			case OpCodes.SMSG_DEATH_RELEASE_LOC:
				Debug.Log("For MiniMap. Closest Graveyard	map: "+pkt.ReadReversed32bit()+" pos: "+ new Vector3(pkt.ReadFloat(),pkt.ReadFloat(),pkt.ReadFloat()) );
				break;		
			case OpCodes.SMSG_ATTACKSTART :
				//player.print("attack start");
				HandleAttackStartOpcode(pkt);
				break;
			case OpCodes.SMSG_ATTACKSTOP :
				//	player.print("attack stop");
				HandleAttackStopOpcode(pkt);
				break;
			case OpCodes.SMSG_ATTACKERSTATEUPDATE :
				//Debug.Log("World: SMSG_ATTACKERSTATEUPDATE *****************************  ");
				HandleAttackStateUpdateOpcode(pkt);
				break;
			case OpCodes.SMSG_ATTACKSWING_BADFACING : //same problem
				MonoBehaviour.print("bad facing");
				player.ShowErrorMessage("You must face target");
				break;
			case OpCodes.SMSG_ATTACKSWING_NOTINRANGE:
				player.isAttacking = false;// don't atack;
				player.ShowErrorMessage("Out of range");
				MonoBehaviour.print("out of range");
				break;
				
			case OpCodes.SMSG_INITIAL_SPELLS :
				HandleInitSpellsOpcode(pkt);
				break;
			case OpCodes.SMSG_EMOTE:
				HandleEmoteOpcode(pkt);
				break;
			case OpCodes.SMSG_CRITERIA_UPDATE :
				HandleCriteriaUpdateOpcode(pkt);
				break;
				//case OpCodes.SMSG_EQUIPMENT_SET_LIST :
				//HandleEquipmentSetListOpcode(pkt);
				//break;
			case OpCodes.SMSG_ITEM_QUERY_SINGLE_RESPONSE :
				//try{
				HandleItemQuerySingleResponseOpcode(pkt);
				//}catch(Exception e){MonoBehaviour.print(e.ToString());}
				break;// */;
				
			case OpCodes.SMSG_STOP_MIRROR_TIMER:
				HandleStopMirrorTimerOpcode(pkt);
				break;
			case OpCodes.SMSG_TIME_SYNC_REQ :
				HandleTimeSyncReqOpcode(pkt);
				break;
			case OpCodes.SMSG_LOGIN_SETTIMESPEED:
				HandleLoginSetTimeSpeed(pkt);
				break;
				
				/*	case OpCodes.SMSG_ADDON_INFO:
				break;
			case OpCodes.SMSG_TUTORIAL_FLAGS:
				break;
			case OpCodes.SMSG_CLIENTCACHE_VERSION:
				break;*/
				
				//handlere opcoduri questuri
			case OpCodes.SMSG_GOSSIP_MESSAGE: //obtaining the list of quests from npc
				HandleGossipMessageOpcode(pkt);
				break;
			case OpCodes.SMSG_QUESTGIVER_QUEST_LIST:
				HandleQuestgiverQuestListOpcode(pkt);
				break;					
				/*case OpCodes.SMSG_QUEST_CONFIRM_ACCEPT:
					HandleQuestConfirmAcceptOpcode(pkt);
				 break; 	*/
				/*case OpCodes.SMSG_QUESTGIVER_QUEST_INVALID: //an error had ocured when trying to obtain the quest
					HandleQuestGiverQuestInvalid(pkt);
				break;*/
				
			case OpCodes.SMSG_QUESTUPDATE_ADD_KILL:
				HandleQuestUpdateAddKill(pkt);
				break;
				
			case OpCodes.SMSG_GOSSIP_COMPLETE: 
				HandleGossipComplete(pkt);
				player.showQuest = false;				
				break;	
			case OpCodes.SMSG_NPC_TEXT_UPDATE:
				if(player.target != null && (player.target.IsCreature()==true) && (player.target as NPC).gossip != null)
				{
					(player.target as NPC).gossip.UpdateTextOptions(pkt);
				}
				break;
			case OpCodes.SMSG_QUESTLOG_FULL: 
				MonoBehaviour.print("Quest Log Full!");
				break;
				
			case OpCodes.SMSG_QUEST_QUERY_RESPONSE:
				HandleQuestQueryResponse(pkt);
				break;
				
			case OpCodes.SMSG_QUESTGIVER_QUEST_DETAILS:
				HandleQuestqiverQuestDetailsOpcode(pkt);
				break; 
				
			case OpCodes.SMSG_QUESTGIVER_REQUEST_ITEMS:   
				HandleQuestqiverRequestItems(pkt);
				break;
				
			case OpCodes.SMSG_QUESTGIVER_OFFER_REWARD:
				HandleQuestgiverOfferReward(pkt);
				break;
				
			case OpCodes.SMSG_QUESTGIVER_STATUS:
				HandleQuestqiverRequestStatus(pkt);
				break;
			case OpCodes.SMSG_QUESTGIVER_STATUS_MULTIPLE:
				HandleQuestqiverRequestStatusMultiple(pkt);
				break;		
				
			case OpCodes.SMSG_QUESTGIVER_QUEST_COMPLETE:
				//	MonoBehaviour.print("///////////Quest COMPLETE!");
				HandleQuestqiverQuestComplete(pkt);
				break;
				
			case OpCodes.SMSG_QUEST_POI_QUERY_RESPONSE:
				HandleQuestPOIResponse(pkt);
				break;		
				
			case OpCodes.SMSG_INVENTORY_CHANGE_FAILURE:
				HandleInventoryChangeFailureOpcode(pkt);
				break;
			case OpCodes.SMSG_LEVELUP_INFO:
				HandleLevelUpOpcode(pkt);
				//Debug.Log("levelup");
				break;
				
			case OpCodes.SMSG_LOOT_RELEASE_RESPONSE:
				//Debug.Log("Lootul a fost released");
				player.lootManager.HandleLootReleaseResponseOpcode(pkt);
				break;
			case OpCodes.SMSG_LOOT_RESPONSE:
				//Debug.Log("Lista cu lootul a fost primita");	
				player.lootManager.HandleLootResponseOpCode(pkt);
				break;
			case OpCodes.SMSG_LOOT_REMOVED:
				//Debug.Log("Un anumit lootItem a fost eliminat");
				player.lootManager.HandleLootRemovedOpCode(pkt);
				break;
				// ========= MAIL HANDLING =============================
			case OpCodes.SMSG_SEND_MAIL_RESULT:
				player.mailHandler.HandleSendMailResult(pkt);
				break;
			case OpCodes.SMSG_MAIL_LIST_RESULT:
				player.mailHandler.HandleMailListResult(pkt);
				player.mailHandler.HandleShowMailBox(pkt);
				break;
			case OpCodes.SMSG_SHOW_MAILBOX:
				player.mailHandler.HandleShowMailBox(pkt);
				break;
			case OpCodes.SMSG_RECEIVED_MAIL:
				player.mailHandler.HandleReceivedMail();
				break;
				// ========= MAIL HANDLING =============================
				//end decs
			case OpCodes.SMSG_LOOT_START_ROLL:
				//Debug.Log("Se face start roll pentru un item");
				player.lootManager.HandleLootStartRoll(pkt);
				break;
			case OpCodes.SMSG_LOOT_ROLL:
				//Debug.Log("Am primit raspuns de roll/greed(desire)");
				player.lootManager.HandleLootRollOpCode(pkt);
				break;
			case OpCodes.SMSG_LOOT_ROLL_WON:
				//Debug.Log("Stim cine a castigat rollul");
				player.lootManager.HandleLootRollWinOpCode(pkt);
				break;
			case OpCodes.SMSG_LOOT_ALL_PASSED:
				//Debug.Log("Toti au dat Pass");
				player.lootManager.HandleAllPassed(pkt);
				break;
				
			case OpCodes.SMSG_GAMEOBJECT_CUSTOM_ANIM:
				// TODO function
				gameObjectGuid = pkt.ReadReversed64bit();
				animationId = pkt.ReadReversed32bit();
				fishingBobberGO = GetRealChar(gameObjectGuid);
				if(fishingBobberGO != null)
				{
					fishingBobberAnimator = fishingBobberGO.transform.parent.GetComponentInChildren<Animator>();
					if(fishingBobberAnimator != null)
					{
						fishingBobberAnimator.SetBool("CatchFish", true);
					}
				}
				Debug.Log("Time "+Time.time+" SMSG_GAMEOBJECT_CUSTOM_ANIM "+gameObjectGuid+" animation = "+animationId);
				break;
			case OpCodes.SMSG_GAMEOBJECT_DESPAWN_ANIM:
				gameObjectGuid = pkt.ReadReversed64bit();
				fishingBobberGO = GetRealChar(gameObjectGuid);
				if(fishingBobberGO != null)
				{
					fishingBobberAnimator = fishingBobberGO.transform.parent.GetComponentInChildren<Animator>();
					if(fishingBobberAnimator != null)
					{
						fishingBobberAnimator.SetBool("CatchFish", false);
					}
				}
				Debug.Log("Time "+Time.time+" SMSG_GAMEOBJECT_DESPAWN_ANIM "+gameObjectGuid);
				break;
				
				
			case OpCodes.SMSG_PLAYERBINDERROR:
				Debug.Log(" smsg_playerbinder_error "+pkt.Size());
				break;
				
			case OpCodes.SMSG_PLAYERBOUND:
				ulong plrGUID= pkt.ReadReversed64bit();
				int areaID= (int)pkt.ReadReversed32bit();
				
				if(plrGUID == MainPlayer.GUID)//this function must be moved in Player.js
				{
					player.PlayerBounded(areaID);
				}
				break;			
			case OpCodes.SMSG_BINDPOINTUPDATE:
				player.BindPointUpdate(pkt);
				break;  
				
			case OpCodes.SMSG_LIST_INVENTORY:
				HandleListInventory(pkt);
				break;
			case OpCodes.SMSG_SHOW_BANK:
				HandleShowBank(pkt);
				break;
			case OpCodes.SMSG_BUY_BANK_SLOT_RESULT:
				HandleBuyBankSlotResult(pkt);
				break;
			case OpCodes.MSG_CORPSE_QUERY: //main player corpse info
				player.CorpseQuery(pkt);
				break;
			case OpCodes.SMSG_CORPSE_RECLAIM_DELAY: // time to reclaim corpse;
				player.CorpseReclaimDelay(pkt);
				break;
				
			case OpCodes.SMSG_SELL_ITEM:
				HandleSellItem(pkt);
				break;
			case OpCodes.SMSG_BUY_FAILED:
				HandleBuyError(pkt);
				break;
			case OpCodes.SMSG_BUY_ITEM:
				// this should be just a chat message about the item and quantity
				break;
			case OpCodes.SMSG_ITEM_PUSH_RESULT:
				//MonoBehaviour.print("push lalalalalala......");
				HandleItemPushResult(pkt);
				//pkt.ReadReversed64bit();
				//Debug.Log(player.startNPCInteraction+"SMSG_IEM_PUSH "+pkt.ReadReversed64bit());
				/*if(player.startNPCInteraction == true)
						{
							(player.target as NPC).UpdateNPC();
						}
						*/
				break;
				// GUILD OpCodes
			case OpCodes.SMSG_PETITION_SHOWLIST:
				guildhandler.HandlePetitionShowList(pkt, player);
				break;
				
			case OpCodes.MSG_QUERY_GUILD_BANK_TEXT:
				guildhandler.HandleGuildBankText(pkt, player);
				break;
			case OpCodes.MSG_GUILD_BANK_LOG_QUERY:
				guildhandler.HandleGuildBankQueryLog(pkt, player);
				break;
			case OpCodes.SMSG_GUILD_BANK_LIST:
				guildhandler.HandleGuildBankList(pkt, player);
				if(player.target && player.target.GetTypeID() ==TYPEID.TYPEID_GAMEOBJECT
				   && (player.target as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_GUILD_BANK)
					player.gVault.ShowAll();
				break;
			case OpCodes.MSG_GUILD_BANK_MONEY_WITHDRAWN:
				guildhandler.HandleBankMoneyWithdrawn(pkt, player);
				break;
				
			case OpCodes.SMSG_GUILD_EVENT:
				guildhandler.ManageEvent(pkt, player);
				break;
			case OpCodes.SMSG_GUILD_COMMAND_RESULT:
				guildhandler.GetCommandResult(pkt, player);
				break;	
			case OpCodes.SMSG_GUILD_INFO:
				guildhandler.HandleInfo(pkt, player);
				break;
			case OpCodes.SMSG_GUILD_ROSTER:
				guildhandler.HandleRoster(pkt, player);
				break;
			case OpCodes.SMSG_GUILD_INVITE:
				guildhandler.HandleInvite(pkt, player);
				//Debug.Log("INVITE");
				player.guildUI.showInvitationGuid = true;
				break;
			case OpCodes.SMSG_GUILD_QUERY_RESPONSE:
				guildhandler.UpdateRanks(pkt, player);
				break;
			case OpCodes.MSG_GUILD_EVENT_LOG_QUERY:
				guildhandler.HandleGuildEventLog(pkt, player);
				break;
				
				// WORLD INIT
			case OpCodes.SMSG_INIT_WORLD_STATES:
				player.InitWorldStates(pkt);
				break;
				// DUEL OpCodes
			case OpCodes.SMSG_DUEL_REQUESTED:
				player.duel.DuelRequested(pkt);
				break;
			case OpCodes.SMSG_DUEL_COUNTDOWN:
				player.duel.DuelCountdown(pkt, player);
				break;
			case OpCodes.SMSG_DUEL_COMPLETE:
				player.duel.DuelCompleted(pkt, player);
				break;
			case OpCodes.SMSG_DUEL_WINNER:
				player.duel.DuelWinner(pkt, player);
				break;
			case OpCodes.SMSG_DUEL_INBOUNDS:
				player.duel.InBounds(pkt, player);
				break;
			case OpCodes.SMSG_DUEL_OUTOFBOUNDS:
				player.duel.OutOfBounds(pkt, player);
				break;
				//-------------------------------------------------------------------------------
			case OpCodes.SMSG_TRADE_STATUS:
				player.trade.HandleTradeStatus(pkt);
				break;
			case OpCodes.SMSG_TRADE_STATUS_EXTENDED:
				player.trade.HandleTradeStatusExtended(pkt);
				break;
				//-------------------------------------------------------------------------------
			case OpCodes.SMSG_CONTACT_LIST:
				player.SocialMgr.HandleContactList(pkt);
				break;
			case OpCodes.SMSG_FRIEND_STATUS:
				player.SocialMgr.HandleFriendStatus(pkt);
				break;
			case OpCodes.SMSG_MITHRIL_COUNT_INFO:
				HandleMithrilCoinsCount(pkt);
				break;
			case OpCodes.SMSG_REFRESH_MITHRIL_COUNT_INFO:
				HandleMithrilCoinsCount(pkt);
				break;
			case OpCodes.SMSG_INSPECT_RESULTS:
				player.InspectMgr.HandleInspectResults(pkt);
				break;
			case OpCodes.SMSG_CONVERT_TEMPORARY_ACC:
				HandleAccountConvertResult(pkt);
				break;
				//==========================Tutorial==========================
			case OpCodes.SMSG_TUTORIAL_FLAGS:
				HandleTutorialPacketResult(pkt);
				break;
				
			case OpCodes.SMSG_QUESTUPDATE_COMPLETE:
				HandleQuestUpdateComplete(pkt);
				break;
				
			case OpCodes.MSG_CHANNEL_START:
				HandleDuration(pkt);
				break;
				
			case OpCodes.SMSG_COOLDOWN_EVENT:
				CooldownEvent(pkt);
				break;
				
			case OpCodes.SMSG_SPELL_COOLDOWN:
				Debug.LogWarning("SMSG_SPELL_COOLDOWN");
				break;
				
			case OpCodes.SMSG_MODIFY_COOLDOWN:
				Debug.LogWarning("SMSG_MODIFY_COOLDOWN");
				break;
				
			case OpCodes.SMSG_CLEAR_COOLDOWN:
				HandleClearCooldown(pkt);
				break;
				
			case OpCodes.SMSG_COOLDOWN_CHEAT:
				Debug.LogWarning("SMSG_COOLDOWN_CHEAT");
				break;
				
			case OpCodes.SMSG_ITEM_COOLDOWN:
				Debug.LogWarning("SMSG_ITEM_COOLDOWN");
				break;
				
			case OpCodes.SMSG_UPDATE_WORLD_STATE:
				HandlerUpdateWorldStates(pkt);
				Debug.LogWarning("SMSG_UPDATE_WORLD_STATE");
				break;
				
			case OpCodes.SMSG_BATTLEFIELD_LIST:
				Debug.LogWarning("SMSG_BATTLEFIELD_LIST");
				break;
				
			case OpCodes.SMSG_BATTLEFIELD_STATUS:
				interfaceManager.playerMenuMng.extraMenuWindow.battleZoneWindow.battleZoneManager.BattleFieldStatus(pkt);
				Debug.LogWarning("SMSG_BATTLEFIELD_STATUS");
				break;
				
			case OpCodes.MSG_PVP_LOG_DATA:
				HandlerPvpLogData(pkt);
				Debug.LogWarning("MSG_PVP_LOG_DATA");
				break;
			case OpCodes.SMSG_INITIALIZE_FACTIONS:
				Fractions.InitializationReputationData(pkt);
				break;
				
			case OpCodes.SMSG_SET_FACTION_STANDING:
				Debug.LogWarning("SMSG_SET_FACTION_STANDING");
				Fractions.SetFractionReputation(pkt);
				break;
				
			case OpCodes.SMSG_ACTION_BUTTONS:
				HandleInitialActionButtons(pkt);
				break;
				
				// Auction
			case OpCodes.SMSG_AUCTION_COMMAND_RESULT:
				AuctionCommandResult(pkt);
				break;
			case OpCodes.MSG_AUCTION_HELLO:
				AuctionHello(pkt);
				break;
			case OpCodes.SMSG_AUCTION_BIDDER_LIST_RESULT:
			case OpCodes.SMSG_AUCTION_OWNER_LIST_RESULT:
			case OpCodes.SMSG_AUCTION_LIST_RESULT:
				NPC npc = (player.target as NPC);
				if(npc != null && npc.auctioneer != null)
				{
					npc.auctioneer.auctionManager.AuctionListOfItemsResponse(pkt);
				}
				break;
			case OpCodes.SMSG_AUCTION_LIST_PENDING_SALES:
				auctionManager.ListOfPendingSalesResponse(pkt);
				break;
				// End auction
				
			case OpCodes.SMSG_GROUP_SET_LEADER:
				HandleChangeLeader(pkt);
				break;
				
			case OpCodes.MSG_RAID_READY_CHECK_CONFIRM:
				HandleReadyCheck(pkt);
				break;
				
			case OpCodes.SMSG_SUMMON_REQUEST:
				HandleSummonRequest(pkt);
				break;
			case OpCodes.SMSG_CHARACTER_BANK_NEXT_SLOT_COST:
				HandleBankSlotCost(pkt);
				break;
				
			case OpCodes.SMSG_USE_NPC_PORTAL_ERROR:
				PortalErrorsHandler(pkt);
				break;
				
			case OpCodes.SMSG_SEND_GEAR_RATING:
				GearRatingResponce(pkt);
				break;
			case OpCodes.MSG_LIST_STABLED_PETS:
				ShowStableMaster(pkt);
				break;
			case OpCodes.SMSG_STABLE_RESULT:
				ShowStableMessage(pkt);
				break;
			case OpCodes.SMSG_LOOT_MASTER_LIST:
				player.lootManager.HandleMasterLoot(pkt);
				Debug.LogWarning("SMSG_LOOT_MASTER_LIST");
				break;
			default:
				//OpCodes op = pkt.GetOpcode();
				//Debug.Log("opcode "+ op+" is not handled");
				break;
			}
		}
		catch(Exception e)
		{
			//MonoBehaviour.print(e);
		}
	}
	
	void  ShowStableMaster (WorldPacket pkt)
	{
		player.blockTargetChange = true;
		interfaceManager.hideInterface = true;
		player.interf.stateManager();
		
		GameObject stableGameObject = new GameObject();
		stableGameObject.name = "StableMaster";
		stableMaster = stableGameObject.AddComponent<StableMaster>();
		
		stableMaster.ReadSlotsInfo(pkt);
	}
	
	void  ShowStableMessage (WorldPacket pkt)
	{
		StableResultCode errorCode = (StableResultCode)pkt.Read();
		switch(errorCode)
		{
		case StableResultCode.STABLE_ERR_MONEY:
			ErrorMessage.ShowError("Your have not \n enough money.");
			break;
		case StableResultCode.STABLE_ERR_STABLE:
			ErrorMessage.ShowError("Operation\n failed.");
			break;
		case StableResultCode.STABLE_SUCCESS_STABLE:
			ErrorMessage.ShowError("Pet Stables.");
			break;
		case StableResultCode.STABLE_SUCCESS_UNSTABLE:
			ErrorMessage.ShowError("Pet Taked.");
			break;
		case StableResultCode.STABLE_SUCCESS_BUY_SLOT:
			ErrorMessage.ShowError("New slot was \n purchased.");
			break;
		case StableResultCode.STABLE_ERR_EXOTIC:
			ErrorMessage.ShowError("Your can not use \n exotic pet.");
			break;
		}
	}

	void  HandleReadyCheck (WorldPacket pkt)
	{
		GroupManager updateGroup = player.group;
		ulong plr = pkt.ReadReversed64bit();
		byte flag = pkt.Read();
		updateGroup.setReadyCheck(plr, flag);
	}
	
	void  HandleChangeLeader (WorldPacket pkt)
	{
		GroupManager updateGroup = player.group;
		string leaderName = pkt.ReadString();
		updateGroup.getNewLeader(leaderName);
	}
	
	public const int MAX_ACTION_BUTTONS= 144;
	
	void  HandleInitialActionButtons (WorldPacket pkt)
	{
		byte data = pkt.Read();
		uint resiveID = 0;
		byte type = 0;
		
		for (int i = 0; i < MAX_ACTION_BUTTONS; i++)
		{
			resiveID = pkt.ReadReversed32bit();
			
			if(resiveID != 0 && i < 62)
			{
				type = (byte)(resiveID >> 24);
				resiveID = resiveID & 0x00FFFFFF;
//				player.actionBarManager.SetObjectToSlot(i, (ActionButtonType)type, resiveID);
			}
		}
		
//		player.actionBarPanel.RefreshSlots();
	}
	
	void  HandleClearCooldown (WorldPacket pkt)
	{
		uint spellID = pkt.ReadReversed32bit();
		ulong guid = pkt.ReadReversed64bit();
		if(player.guid == guid)
		{
			Spell spell = SpellManager.GetSpell(spellID);
			if(spell == null)
			{
				
				throw new Exception("You don't know spell with id " + spellID);
			}
			player.cooldownManager.playerCooldownList.RemoveCooldown(spell);
//			player.actionBarPanel.RefreshSlots();
		}
	}
	
	void  HandlePetGuidslOpcode (WorldPacket pkt)
	{
		ulong guid = pkt.ReadReversed32bit();
		if(guid == 1)
		{
			player.petGuid = pkt.ReadReversed64bit();
		}
		else
		{
			Debug.Log("ERROR");
		}		
	}
	
	void  HandleBankSlotCost (WorldPacket pkt)
	{
		ulong guid = pkt.Read();//pkt.GetPacketGuid();
		if(guid == 8)
		{
			interfaceManager.messageWnd.InitOneButtonWindow("You have max bag slots!");
			interfaceManager.messageWnd.onOkButton += interfaceManager.npcWindowMng.bankerWnd.Mng.SendBuyBankSlot;
		}
		else
		{
			uint cost = pkt.ReadReversed32bit();
			interfaceManager.messageWnd.InitTwoButtonWindow("You are about to pay " + cost.ToString() + " Mithril for a bag slot!", "Buy", "Cancel");
			interfaceManager.messageWnd.onOkButton += interfaceManager.npcWindowMng.bankerWnd.Mng.SendBuyBankSlot;
		}
	}
	
	void  HandleSummonRequest (WorldPacket pkt)
	{
		ulong guid = pkt.ReadReversed64bit();//pkt.GetPacketGuid();
		uint zoneID = pkt.ReadReversed32bit();
		uint duration = pkt.ReadReversed32bit();
		UIButton tmp = null;
		PartyPlayer otherPlayer;
		WorldMapAreaEntry worldMapArea = dbs.sWorldMapArea.GetRecord(zoneID);
		otherPlayer = player.group.GetPartyPlayer(guid);
		if(player != null && otherPlayer != null && worldMapArea != null)
		{
			player.interf.sumonnerGUIDName = otherPlayer.name;
			player.interf.sumonningTime = duration / 1000.0f;
			player.interf.isTimerStart = true;
			player.interf.startTime = Time.time;
			player.interf.setSummonZoneName(worldMapArea.internal_name);
			player.interf.SummonPopUpWindow(tmp);
		}
	}	
	
	void  HandleQuestUpdateComplete (WorldPacket pkt)
	{	
		uint q_id = pkt.ReadReversed32bit();
		//Debug.Log("HandleQuestUpdateComplete for quest " + q_id );
		WorldPacket wp = new WorldPacket(OpCodes.CMSG_QUESTGIVER_STATUS_MULTIPLE_QUERY,0); RealmSocket.outQueue.Add(wp);  
	}
	
	void  HandleDuration (WorldPacket pkt)
	{
		ulong casterGuid = pkt.GetPacketGuid();   //casterGuid
		uint spellInfo = pkt.ReadReversed32bit();
		uint duration = pkt.ReadReversed32bit();
		Unit obj = null;
		
		if(duration == 0)
		{
			return;
		}
		
		if(casterGuid == MainPlayer.GUID)
		{
			obj = player as Unit;
			if(duration > 0)
			{
				DrawSpellProgressBar();
			}
		}
		obj.castSpellID = spellInfo;
		
		if(duration > 0)
		{
			interfaceManager.castBarMng.castTime = 0;
			interfaceManager.castBarMng.castTimeTotal = duration / 1000f;
			interfaceManager.castBarMng.isConductedSpell = true;
		}
		//		object.castTime = 0f;
		//		object.castTimeTotal = duration / 1000f;
		//		object.isConductedSpell = false;
		
		SpellVisual scriptSpellVisual = FxEffectManager.GetSpellVisual(spellInfo);
		if(scriptSpellVisual)
		{
			SetFxEffectDuration(scriptSpellVisual.effect[0], casterGuid, duration/1000f);
			SetFxEffectDuration(scriptSpellVisual.effect[1], casterGuid, duration/1000f);
			SetFxEffectDuration(scriptSpellVisual.effect[2], casterGuid, duration/1000f);
			SetFxEffectDuration(scriptSpellVisual.persistentAreaKit, casterGuid, duration/1000f);
		}
	}
	
	void  SetFxEffectDuration (int effectId, ulong casterGuid, float duration)
	{
		if(effectId > 0)
		{
			GameObject effectObject = FxEffectManager.GetEffect(effectId, casterGuid);
			effectObject.BroadcastMessage("SetDelayTime", duration, SendMessageOptions.DontRequireReceiver);
		}
		
	}
	
	void  CooldownEvent (WorldPacket pkt)
	{
		uint spellID = pkt.ReadReversed32bit();
		ulong casterGuid = pkt.ReadReversed64bit();
		Spell spell = SpellManager.GetSpell(spellID);
		if(casterGuid == player.guid && spell != null)
		{
			player.cooldownManager.CooldownEvent(spell);
		}
	}
	
	public void  handleOpcode (OpCodes oc, ulong[] parametters)
	{
		WorldPacket pkt;
		uint aux32 = 0;
		byte aux8 = 0;
		switch(oc)
		{
		case OpCodes.CMSG_GROUP_DECLINE:
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			RealmSocket.outQueue.Add(pkt);
			MonoBehaviour.print("party decline");
			break;
		case OpCodes.CMSG_GROUP_ACCEPT:
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			
			pkt.Append(aux32);
			RealmSocket.outQueue.Add(pkt);
			MonoBehaviour.print("party accept");
			break;
		case OpCodes.CMSG_GROUP_DISBAND:
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			RealmSocket.outQueue.Add(pkt);
			break;
		case OpCodes.CMSG_LOGOUT_CANCEL:
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			RealmSocket.outQueue.Add(pkt);
			break;
			
			//handlere questuri		
		case OpCodes.CMSG_GOSSIP_HELLO: 
		case	OpCodes.CMSG_QUESTGIVER_HELLO :
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			pkt.Append(ByteBuffer.Reverse(parametters[0]));
			RealmSocket.outQueue.Add(pkt);
			break;
		case OpCodes.CMSG_QUEST_CONFIRM_ACCEPT:
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			aux32 = (uint)parametters[0];
			pkt.Append(ByteBuffer.Reverse(aux32));
			RealmSocket.outQueue.Add(pkt);
			break;
			
		case OpCodes.CMSG_QUESTGIVER_ACCEPT_QUEST:
			//	MonoBehaviour.print("////////////////////////////////////////////accept quest: " + parametters[1]+"  "+parametters[2]+"   "+parametters[0]);
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			pkt.Append(ByteBuffer.Reverse(parametters[0]));
			aux32 = (uint)parametters[1];
			pkt.Append(ByteBuffer.Reverse(aux32));
			aux32 = 0;
			pkt.Append(aux32);
			RealmSocket.outQueue.Add(pkt);
			break;
			
		case OpCodes.CMSG_QUESTLOG_REMOVE_QUEST:
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			aux8 = (byte)parametters[0];
			pkt.Append(aux8);//quest slot
			//MonoBehaviour.print("////////////////////////////////////////////remove quest: " + aux8);
			RealmSocket.outQueue.Add(pkt);
			break;
			
		case OpCodes.CMSG_QUEST_QUERY: //detalii despre quest
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			aux32 = (uint)parametters[0];
			pkt.Append(ByteBuffer.Reverse(aux32));//quest id
			RealmSocket.outQueue.Add(pkt);
			break;
			
		case OpCodes.CMSG_QUESTGIVER_QUERY_QUEST: //detalii despre quest
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			pkt.Append(ByteBuffer.Reverse(parametters[0]));//npc guid
			aux32 = (uint)parametters[1];
			pkt.Append(ByteBuffer.Reverse(aux32));//quest id
			aux8 = 0;			
			pkt.Append(aux8);//unk1
			//MonoBehaviour.print("Get quest info query"+parametters[0]+" QuestId: "+aux32+" unk1"+aux8);
			RealmSocket.outQueue.Add(pkt);
			break;   
			
		case OpCodes.CMSG_QUESTGIVER_STATUS_QUERY:
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			pkt.Append(ByteBuffer.Reverse(parametters[0]));
			//MonoBehaviour.print("/////////////////////checkNPCstatus: NPCguid: " +parametters[0]);
			//pkt.Append(ByteBuffer.Reverse(aux32));
			RealmSocket.outQueue.Add(pkt);		
			break;
			
		case OpCodes.CMSG_QUESTGIVER_COMPLETE_QUEST:
			//	try{
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			pkt.Append(ByteBuffer.Reverse(parametters[0]));
			aux32 = (uint)parametters[1];
			pkt.Append(ByteBuffer.Reverse(aux32));
			RealmSocket.outQueue.Add(pkt);		
			//}catch(Exception e){player.print(e);}
			break;
		case OpCodes.CMSG_QUESTGIVER_REQUEST_REWARD:
			//	try{
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			pkt.Append(ByteBuffer.Reverse(parametters[0]));
			aux32 = (uint)parametters[1];
			pkt.Append(ByteBuffer.Reverse(aux32));
			RealmSocket.outQueue.Add(pkt);		
			//}catch(Exception e){player.print(e);}
			break;
		case OpCodes.CMSG_QUESTGIVER_CHOOSE_REWARD:
			pkt = new WorldPacket();
			pkt.SetOpcode(oc);
			pkt.Append(ByteBuffer.Reverse(parametters[0]));
			aux32 = (uint)parametters[1];
			pkt.Append(ByteBuffer.Reverse(aux32));
			aux32 = (uint)parametters[2];
			pkt.Append(ByteBuffer.Reverse(aux32));
			//MonoBehaviour.print("/////////////////////chooseReward: NPCguid: " +parametters[0] + "  questID " + parametters[1]+" choise"+aux32);
			RealmSocket.outQueue.Add(pkt);	
			
			break;
			//decs
			
		default:
			Debug.LogWarning("client opcode "+ oc+" is not handled");
			break;
		}
	}
	
	void  convertAccount (string login, string password)
	{
		login = login.ToUpper();
		password = password.ToUpper();
		
		string _authstr = login + ":" + password;
		string hash_str = LoginWindow.CalculateSHA1Hash(_authstr);
		
		
		WorldPacket pktTest5 = new WorldPacket();   
		pktTest5.SetOpcode(OpCodes.CMSG_CONVERT_TEMPORARY_ACC);
		pktTest5.Append(login);
		pktTest5.Append(hash_str);
		RealmSocket.outQueue.Add(pktTest5);
	}
	
	void  HandleAccountConvertResult (WorldPacket pkt)
	{
		AccountConvertResult result = (AccountConvertResult)pkt.Read();
		switch(result)
		{
		case AccountConvertResult.CONVERT_SUCCESS:
			break;
		}
	}
	
	private void  DrawSpellProgressBar ()
	{
		player.goCast = true;
		interfaceManager.castBarMng.gameObject.SetActive(true);
		//		player.interf.progressBar.hidden = !player.goCast;
		
	}
	
	private bool isCanBeLearnedTalentSpell (uint spellID)
	{
		bool  returnValue = false;
		switch(spellID)
		{	
			// Fighter
			// Power
		case 12328:
		case 12294:
		case 46924:
			// Ire
		case 12323: // Ear-Cracking Bellow
		case 12292:
		case 23881:
		case 60970:
			// Aegis
		case 12975: // Stand Against the Darkness
		case 12809:
		case 50720:
		case 20243:
		case 46968:
			
			// Confesor
			// Devotion
		case 14751: // Concentration of the Hermit
		case 10060:
		case 33206:
		case 47540:
			// Reverend
		case 19236:	// Spiritual Bow
		case 724:
		case 34861:
		case 47788:
			// Gloom
		case 15407: // Brain Scrambler
		case 15487:
		case 15286:
		case 15473:
		case 64044:
		case 34914:
		case 47585:
			
			// Templar
			// Righteous
		case 31821: // Ethercloud
		case 20473:
		case 31842:
		case 53563:
			// Warden
		case 64205: // Blood of the Righteous
		case 20911:
		case 20925:
		case 31935:
		case 53595:
			// Reprisal
		case 20375: // Authority
		case 20066:
		case 35395:
		case 53385:
			
			// Rough
			//
		case 14177:
		case 1329:
		case 51662:
			//
		case 14251: // Responding Shrik
		case 13877:
		case 13750:
		case 51690:
			//
		case 14278: // Subtle Melle
		case 14185:
		case 16511:
		case 36554:
			
			// Mage
			// Spirit
		case 54646: // Caster True-Direction
		case 12043:
		case 12042:
		case 31589:
		case 44425:
			// Inferno
		case 11366: // Infernal Burst 1
		case 11113:
		case 11129:
		case 31661:
		case 44457:
			// Ice
		case 12472: // Bodyshock Chill
		case 11958:
		case 11426:
		case 31687:
		case 44572:
			// 
			
			// Necromancer
			// Pestilent
		case 18223:
		case 30108:
		case 48181:
			// Abomination
		case 19028: // Inter-Corrupt
		case 47193:
		case 30146:
		case 59672:
			// Towerfall
		case 17877: // Bristling Dark
		case 17962:
		case 30283:
		case 50796:
			
			// Ranger
			// Tracker's Husbandry
		case 19577:
		case 19574:
			// Precision Craft
		case 19434: // Sigted Assail
		case 23989:
		case 19506:
		case 34490:
		case 53209:
			// Wilderness Schholary
		case 19503: // Killspray
		case 19306:
		case 19386:
		case 3674:
		case 53301:
			returnValue = true;
			break;
		}
		return returnValue;
	}
	
	//--------------------------Anton Strul
	//--------------------------Quest POI 
	private void  QuestPOIRequest ()
	{
		uint questID;
		uint questCount = player.questManager.getQuestCount();
		if(questCount > 0)
		{
			WorldPacket paketPOI = new WorldPacket();   
			paketPOI.SetOpcode(OpCodes.CMSG_QUEST_POI_QUERY);
			paketPOI.Append(ByteBuffer.Reverse(questCount));
			for(byte questSlot = 0; questSlot < questCount; questSlot++)
			{
				questID = player.questManager.getQuestBySlotId(questSlot);
				paketPOI.Append(ByteBuffer.Reverse(questID));
			}
			RealmSocket.outQueue.Add(paketPOI);
		}
	}
	
	void  HandleQuestPOIResponse (WorldPacket pkt)
	{
		uint questCount = pkt.ReadReversed32bit();
		QuestPoiManager.poiPointList.Clear();
		for(byte i = 0; i < questCount; i++)
		{
			PoiPointManager questPoi = new PoiPointManager();
			uint questID = pkt.ReadReversed32bit();
			uint questStagePoiCount = pkt.ReadReversed32bit();
			for(byte j = 0; j < questStagePoiCount; j++)
			{
				uint poiID = pkt.ReadReversed32bit();
				QuestStage questStagePoi = new QuestStage();
				questStagePoi.objectiveIndex= pkt.ReadInt();
				questStagePoi.mapID = pkt.ReadReversed32bit();
				questStagePoi.mapAreaID = pkt.ReadReversed32bit();
				questStagePoi.floorID = pkt.ReadReversed32bit();
				questStagePoi.unk3 = pkt.ReadReversed32bit();
				questStagePoi.unk4 = pkt.ReadReversed32bit();
				uint poiPositionCount = pkt.ReadReversed32bit();
				for(uint k = 0; k < poiPositionCount; k++)
				{
					questStagePoi.poiPositionsList.Add(new Vector3(pkt.ReadInt(), 0, pkt.ReadInt()));
				}
				if(player.mapId == questStagePoi.mapID || 
				   (questStagePoi.mapID == 0 && 
				 (player.mapId == 0 || player.mapId == 35 || player.mapId == 25 || player.mapId == 723 || player.mapId == 609 || player.mapId == 586)))
				{
					questPoi.poiStages.Add(poiID, questStagePoi);
				}
			}
			QuestPoiManager.poiPointList.Add(questID, questPoi);
		}
		player.questManager.setMarkedQuest(player.questManager.markedQuest);
	}
	//--------------------------Quest POI end
	
	string CheckIfTrap (uint entry)
	{	
		string trapName = string.Empty;
		switch(entry)
		{
		case 164839:
		case 164879:
		case 164880:
		case 181031:
		case 189321:
		case 189322:
			trapName = "CombustibleSnare";//3072
			break;
		case 2561:
		case 164876:
		case 164877:
			trapName = "ShardcrestSnare";//3071
			break;
		case 164638:
		case 164872:
		case 164873:
		case 164874:
		case 164875:
		case 181030:
		case 189318:
		case 189319:
			trapName = "SmolderSnare";//3074
			break;
		case 164639:
			trapName = "GustingSnare";//3073
			break;
		case 183957:
			trapName = "Spider Snare";//7205
			break;
		default:
			trapName = "NoTrap";
			break;
		}
		return trapName;
	}
	
	void  parsePlayerSpell (WorldPacket pkt)
	{
		uint spellId = pkt.ReadReversed32bit();
		if(!verifyIfSpellIsTalent(spellId) || isCanBeLearnedTalentSpell(spellId))
		{
			try
			{
				player.playerSpellManager.AddSpell(SpellManager.CreateOrRetrive(spellId));
			}
			catch(Exception e)
			{
				Debug.LogException(e);
			}
		}
	}
	
	bool isNeedStopAutoCast (uint spellId, ulong casterGuid, ulong targetGuid)
	{
		bool  ret = false;
		if(player && (player.guid == casterGuid) && ((spellId == 75) || (spellId == 5019)))
		{
			BaseObject targetObject = GetRealChar(targetGuid);
			if(targetObject == null
			   || ((targetObject is NPC || targetObject is OtherPlayer) && (targetObject as Unit).IsDead()))
			{
				CancelAutoRepeatSpell();
				ret = true;
			}
		}
		return ret;
	}
	
	void  CancelAutoRepeatSpell ()
	{
		WorldPacket packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_CANCEL_AUTO_REPEAT_SPELL);
		RealmSocket.outQueue.Add(packet);
	}
	
	void  PortalErrorsHandler (WorldPacket packet)
	{
		GameObject portalErrorsControllerObject = Instantiate<GameObject>(new GameObject());
		if(portalErrorsControllerObject)
		{
			portalErrorsControllerObject.name = "TeleportErrorsController";
			portalErrorsControllerObject.AddComponent<PortalErrors>();
		}
		PortalErrors portalErrorController = portalErrorsControllerObject.GetComponent<PortalErrors>();
		portalErrorController.ReadPacket(packet);
	}
	
	void  GearRatingResponce (WorldPacket packet)
	{
		if(player != null)
			player.gearRating = packet.ReadReversed32bit();
	}
	
	// Auction
	void  AuctionCommandResult (WorldPacket pkt)
	{
		uint auctionId = pkt.ReadReversed32bit();
		AuctionAction action = (AuctionAction)pkt.ReadReversed32bit();
		AuctionError errorCode = (AuctionError)pkt.ReadReversed32bit();
		uint auctionOutBid;
		string message = "";
		switch(errorCode)
		{
		case AuctionError.AUCTION_OK:
			if(action == AuctionAction.AUCTION_BID_PLACED)
			{
				auctionOutBid = pkt.ReadReversed32bit();
				Debug.Log("Auction - new AuctionOutBid = "+auctionOutBid);
			}
			else
				Debug.Log("Auction - Succes action");
			break;
		case AuctionError.AUCTION_ERR_INVENTORY:
			message = "Auction - error with inventory - "+ItemFlags.InventoryErrorMessage(pkt.ReadReversed32bit());
			break;
		case AuctionError.AUCTION_ERR_HIGHER_BID:
			ulong bidderGuid = pkt.GetPacketGuid();
			uint bid = pkt.ReadReversed32bit();
			auctionOutBid = pkt.ReadReversed32bit();
			message = "Auction - have higer bid";
			break;
		case AuctionError.AUCTION_ERR_DATABASE:
			message = "Auction - database error";
			break;
		case AuctionError.AUCTION_ERR_NOT_ENOUGH_MONEY:
			message = "Auction - not enough money";
			break;
		case AuctionError.AUCTION_ERR_ITEM_NOT_FOUND:
			message = "Auction - item not found";
			break;
		case AuctionError.AUCTION_ERR_BID_INCREMENT:
			message = "Auction - error bid increment";
			break;
		case AuctionError.AUCTION_ERR_BID_OWN:
			message = "Auction - error bid own";
			break;
		case AuctionError.AUCTION_ERR_RESTRICTED_ACCOUNT:
			message = "Auction - error restricted account";
			break;
		default:
			message = "Auction - unknown error";
			break;
		}
		if(errorCode != AuctionError.AUCTION_OK)
		{
			Debug.LogError(message);
		}
	}
	// End auction
	
	void  HandleTutorialPacketResult (WorldPacket packet)
	{
		SelectCharacterWindow.tutorialPacketFWs = packet;
		Swipe.tutorialPacketInSwipe = packet;
		TutorialHandler.Instance.isTutorialNoEnded = Swipe.setTutorial(packet);
	}
	
	void  AuctionHello (WorldPacket pkt)
	{
		NPC npc = (player.target as NPC);
		if(npc != null && npc.auctioneer != null)
		{
			npc.auctioneer.AuctioneerHelloResponse(pkt);
		}
	}
	
	void  HandlerPvpLogData (WorldPacket pkt)
	{
		byte isArena = pkt.Read(); // 0 if BZ, 1 is arene 
		byte battleZoneStatus = pkt.Read();	// 0 - bg not end, 1- bg is end
		if(battleZoneStatus == 1)
		{
			byte winnerTeam = pkt.Read();	// winner team
		}
		
		uint playersCount = pkt.ReadReversed32bit();
		
		List<BattleZoneLogData> playerList = new List<BattleZoneLogData>();
		for(uint index = 0; index < playersCount; index++)
		{
			BattleZoneLogData battleLogData = new BattleZoneLogData();
			
			battleLogData.guid = pkt.GetPacketGuid();
			Player player = GetRealChar(battleLogData.guid) as Player;
			if(player != null)
			{
				battleLogData.name = player.Name;
				battleLogData.faction = (byte)player.race;
			}
			
			battleLogData.killingBlows = pkt.ReadReversed32bit();
			
			battleLogData.honorKills = pkt.ReadReversed32bit();
			battleLogData.deaths = pkt.ReadReversed32bit();
			battleLogData.bonusHonor = pkt.ReadReversed32bit();
			
			battleLogData.damageDone = pkt.ReadReversed32bit();
			battleLogData.healingDone = pkt.ReadReversed32bit();
			
			pkt.ReadReversed32bit();
			battleLogData.flagCaptures = pkt.ReadReversed32bit();
			battleLogData.flagReturns = pkt.ReadReversed32bit();
			
			playerList.Add(battleLogData);
		}
		
		interfaceManager.playerMenuMng.extraMenuWindow.battleZoneWindow.battleZoneManager.playerList = playerList;
	}
	
	void  HandlerUpdateWorldStates (WorldPacket pkt)
	{
		uint code = pkt.ReadReversed32bit();
		switch(code)
		{
		case 4248:
			interfaceManager.playerMenuMng.extraMenuWindow.battleZoneWindow.battleZoneManager.timeLeft = pkt.ReadReversed32bit();
			break;
		}
	}
	
	void  ShowBeastLore ()
	{
		string beastInfo = "";
		
		beastInfo += "Name: " + (player.target as Unit).Name;
		beastInfo += "\nLevel: " + (player.target as Unit).GetLevel();
		
		MobTemplate mt = getMobTemplate(player.target.GetEntry());
		string petName = DefaultTabel.petFamilyName[mt.family.ToString()] as string;
		beastInfo += "\nFamily: " + petName;
		
		uint minDamage = player.target.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MINDAMAGE);
		uint maxDamage = player.target.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXDAMAGE);
		beastInfo += "\nDamage: " + minDamage + "-" + maxDamage;
		
		int offset = (int)SpellSchools.SPELL_SCHOOL_NORMAL;
		int posStat = (int)player.target.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		int negStat = (int)player.target.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		beastInfo += "\nArmor: " + player.target.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset) + "(" + (posStat + negStat) + ")";
		
		beastInfo += "\nFood: " ;
		IDictionary dictionary = DefaultTabel.petFoodName[petName] as IDictionary;
		string food = "";
		
		var dictkey = dictionary.Keys;
		
		foreach(string foodNumber in dictkey)
		{			
			food = dictionary[foodNumber] as string;			
			beastInfo += food;
		}
		
		interfaceManager.beastLoreWindow.ShowBeastInfoWindow(beastInfo);
	}

	void SpawnGameObject (ulong goGuid, SpawnChar script, GameObjectTemplate goTemplate)
	{
		waitingList.Add(goGuid);
		AssetBundleManager.MobCallBackStruct callBackStruct = new AssetBundleManager.MobCallBackStruct();
		callBackStruct.guid = goGuid;
		callBackStruct.scale = 1;
		callBackStruct.assetPath = "";
		callBackStruct.dummy = ApplyGameObjectModel(callBackStruct);
		try
		{
			if(goTemplate.displayID == 0)
			{
				throw new Exception();
			}
			
			GameObjectDisplayInfoEntry goDisplayInfo = dbs.sGameObjectDisplayInfo.GetRecord(goTemplate.displayID);
			if(goDisplayInfo.ID == 0)
			{
				throw new Exception();
			}

			callBackStruct.scale = goDisplayInfo.Scale;
			string modelPath = goDisplayInfo.ModelMesh.Replace("prefabs/", "");
			callBackStruct.assetPath = string.Format ("prefabs/{0}", modelPath);
			GetBundleLoader().LoadNPCBundle(ApplyGameObjectBundle, callBackStruct);		
		}
		catch(Exception e)
		{
		}
	}
	
	void ApplyGameObjectBundle ( AssetBundleManager.MobCallBackStruct callBackStruct )
	{
		waitingList.Remove(callBackStruct.guid);
		string bundleName = callBackStruct.assetPath;
		GameObject dummy = callBackStruct.dummy;
		if(!dummy)
		{
			Debug.LogError("No have Mob dummy");
			return;
		}
		string assetBundleName = string.Format ("Resources/{0}_prefab.unity3d", bundleName);
		assetBundleName = assetBundleName.ToLower();
		UnityEngine.Object bundle = null;
		AssetBundleManager.instance.currentLoadedBundles.TryGetValue(assetBundleName, out bundle);
		GameObject resourceToInstantiate = null;
		if(bundle == null)
		{
			#if UNITY_EDITOR
			Debug.LogError("cannot load "+assetBundleName);
			#endif
		}
		else
		{
			resourceToInstantiate = bundle as GameObject;
		}
		if(resourceToInstantiate == null)
		{
			resourceToInstantiate = GameResources.Load<GameObject>(bundleName);
		}
		if(resourceToInstantiate != null)
		{
			GameObject obj;
			obj = Instantiate<GameObject>(resourceToInstantiate);
			obj.transform.localScale *= callBackStruct.scale;
			_GameObject objScript = obj.GetComponentInChildren<BaseObject>() as _GameObject;
			if(objScript == null)
			{
				objScript = obj.AddComponent<_GameObject>();
			}
			if(objScript != null )
			{
				_GameObject dummyScript = dummy.gameObject.GetComponentInChildren<BaseObject> () as _GameObject;
				objScript.guid = dummyScript.guid;
				objScript.SetType(dummyScript.GetTypeID());
				objScript.CopyValues(dummyScript.GetValues());
				
				objScript.Start();
				Vector3 position = new Vector3(dummyScript.transform.position.x,
				                               dummyScript.transform.position.z,
				                               dummyScript.transform.position.y);
				objScript.SetPosition(position, dummyScript.GetOrientation());
				objScript.moveSpeed = dummyScript.moveSpeed;
				
				objScript.auraManager = new AuraManager(objScript);
				objScript.auraManager.auras.Clear();
				foreach(Aura ar in dummyScript.auraManager.auras)
				{
					ar.unit = objScript as BaseObject as Unit;
					objScript.auraManager.auras.Add(ar);
				}
				
				objScript.entryID = dummyScript.GetEntry();
				GameObjectTemplate objTemplate = getGameObjectTemplate(dummyScript.GetEntry());
				if(objTemplate != null)
				{
					objScript.gameobjectType = objTemplate.type;
				}
				else
				{
					objScript.gameobjectType = dummyScript.gameobjectType;
				}
				(objScript as IBaseObject).CheckLootedFlag();
				objScript.InitName(dummyScript.objName);
				UnityEngine.Object.Destroy(callBackStruct.dummy);
				AddOrReplaceRealChar(objScript);
			}
			else
			{
				Debug.Log("Model has no script attach");
			}
		}
	}
	
	GameObject ApplyGameObjectModel ( AssetBundleManager.MobCallBackStruct callBackStruct)
	{
		string bundleName = callBackStruct.assetPath;
		SpawnChar script = getObjToSpawn(callBackStruct.guid);
		if(script == null)
		{
			return null;
		}
		GameObject resourceToInstantiate = null;
		GameObject obj = null;
		try
		{
			resourceToInstantiate = GameResources.Load<GameObject> (string.Format ("prefabs/{0}", bundleName));
		}
		catch(Exception exception)
		{}
		if(resourceToInstantiate)
		{
			obj = Instantiate<GameObject>(resourceToInstantiate);
			obj.transform.localScale *= callBackStruct.scale;
		}
		else
		{
			obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
		}
		
		_GameObject objScript = obj.GetComponentInChildren<BaseObject>() as _GameObject;
		if(objScript == null)
		{
			objScript = obj.AddComponent<_GameObject>();
		}
		if(objScript != null )
		{
			objScript.guid = script.guid;
			objScript.SetType(script.getTypeID());
			objScript.CopyValues(script.getValues());
			
			objScript.Start();
			objScript.SetPosition(script.pos, script.o);
			objScript.moveSpeed = script.moveSpeed;
			
			objScript.auraManager = new AuraManager(objScript);
			objScript.auraManager.auras.Clear();
			foreach(Aura ar in script.auras)
			{
				ar.unit = objScript as BaseObject as Unit;
				objScript.auraManager.auras.Add(ar);
			}
			
			GameObjectTemplate objTemplate = getGameObjectTemplate(script.GetEntry());
			objScript.gameobjectType = objTemplate.type;
			objScript.entryID = script.GetEntry();
			(objScript as IBaseObject).CheckLootedFlag();
			AddOrReplaceRealChar(objScript);
			objScript.InitName(objTemplate.name);
			removeObjectToSpawn(callBackStruct.guid);
		}
		else
		{
			Debug.Log("Model has no script attach");
		}
		
		return obj;
	}
}