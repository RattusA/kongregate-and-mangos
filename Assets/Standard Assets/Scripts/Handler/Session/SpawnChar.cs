﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnChar //extends _BaseObject //for reducing redundancy in code. 
{
	public TYPEID _typeID; //was protected instead of public
	protected TYPE _typeMask;
	public ulong guid;
	public Vector3 pos;
	public float o;
	string _name;
	protected ushort _valuescount = 0x6;
	protected uint[] _uint32values;
	public float moveSpeed;
	
	public List<Aura> auras = new List<Aura>();
	
	public SpawnChar ( ulong uguid, TYPEID typeid )
	{
		guid = uguid;
		setType( typeid);
	}
	
	ulong GetUInt64Value ( ushort index )
	{
		ulong value;
		int shift = 32;
		value = _uint32values[index+1];
		value = value << shift;
		value = value | _uint32values[index];
		return value;
	}
	
	public uint GetEntry ()
	{
		return _uint32values[(int)UpdateFields.EObjectFields.OBJECT_FIELD_ENTRY];
	}
	
	public uint[] getValues ()
	{
		return _uint32values;
	}
	private void  InitValues ()
	{
		_uint32values = new uint[_valuescount];
	}
	
	public ushort GetValuesCount ()
	{
		return _valuescount;
	}
	public void  SetUInt32Value ( uint index ,   uint value )
	{
		_uint32values[index] = value;
	}
	public void  setFloatValue ( uint index ,   float value )
	{
//		ByteBuffer bb = new ByteBuffer();
//		bb.Append(value);
//		_uint32values[index] = (uint)bb.ReadType(4);
		_uint32values[index] = System.BitConverter.ToUInt32(System.BitConverter.GetBytes(value), 0);
	}
	public void  setUInt64Value ( uint index ,   ulong value )
	{
		ByteBuffer bb = new ByteBuffer();
		bb.Append(value);
		_uint32values[index] = (uint)bb.ReadType(4);
		_uint32values[index+1] = (uint)bb.ReadType(4);
	}
	
	public TYPEID getTypeID ()
	{
		return _typeID;
	}
	public TYPE getTypeMask ()
	{
		return _typeMask;
	}
	
	void  setType ( TYPEID type )
	{
		_typeID = type;
		switch(_typeID)
		{
			
		case TYPEID.TYPEID_ITEM : 
			_valuescount = 0x40;
			_typeMask = TYPE.TYPE_ITEM;
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_ITEM/TYPEID.TYPEID_OBJECT " + type);
			break;
		case TYPEID.TYPEID_CONTAINER :
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_CONTAINER " + type);
			_valuescount = 0x8A;
			_typeMask = TYPE.TYPE_CONTAINER;
			break;
		case TYPEID.TYPEID_UNIT :
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_UNIT " + type);
			_valuescount = 0x94;
			_typeMask = TYPE.TYPE_UNIT;
			break;
		case TYPEID.TYPEID_PLAYER :
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_PLAYER " + type);
			_valuescount = 0x52E;
			_typeMask = TYPE.TYPE_PLAYER;
			break;
		case TYPEID.TYPEID_GAMEOBJECT :
			//MonoBehaviour.print("setType - received a GAMEOBJECT " + type);
			_valuescount = 0x12;
			_typeMask = TYPE.TYPE_GAMEOBJECT;
			break;
		case TYPEID.TYPEID_CORPSE :
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_CORPSE " + type);
			_valuescount = 0x24;
			_typeMask = TYPE.TYPE_CORPSE;
			break;
		case TYPEID.TYPEID_DYNAMICOBJECT :
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_DYNAMICOBJECT " + type);
			_valuescount = 0xC;
			_typeMask = TYPE.TYPE_DYNAMICOBJECT;
			break;
		}
		InitValues();
	}
	
	public void  SetPosition ( Vector3 position ,   float orientation )
	{
		pos = position;
		o = orientation;
	}
	
	uint GetDisplayId ()
	{ return (_uint32values[(int)UpdateFields.EUnitFields.UNIT_FIELD_DISPLAYID]); } 
}
