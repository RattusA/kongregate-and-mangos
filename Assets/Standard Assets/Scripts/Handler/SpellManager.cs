﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SpellManager
{
	public static Dictionary<uint, Spell> spellList = new Dictionary<uint, Spell>();
	
	public static Spell CreateSpell(uint spellID)
	{
		SpellEntry spellEntry = dbs.sSpells.GetRecord(spellID);	
		if(spellEntry == null || spellEntry.ID == 0)
		{
			throw new Exception("Spell with spellID " + spellID + " not found");
		}
		
		Spell spell = new Spell(spellEntry);
		spellList[spellID] = spell;
		
		return spell;
	}
	
	public static Spell GetSpell(uint spellID)
	{
		Spell ret;
		spellList.TryGetValue(spellID, out ret);
		
		return ret;
	}
	
	public static Spell CreateOrRetrive(uint spellID)
	{
		Spell ret = GetSpell(spellID);
		return (ret != null ? ret : CreateSpell(spellID));
	}
	
	public static string GetVisualEffectName(uint spellID)
	{
		string ret;
		Spell spell = GetSpell(spellID);
		string spellName = spell.SpellName.Strings[0];
		spellName = spellName.Replace(":", "");
		ret = spell.SpellVisual[0].ToString() + "-" + spellName;
		return ret;
	}
	
	public static bool CheckStopCooldownFlag(Spell spell)
	{
		bool ret = false;
		if((spell.Attributes & 0x02000000) > 0)
		{
			ret = true;
		}
		return ret;
	}
}