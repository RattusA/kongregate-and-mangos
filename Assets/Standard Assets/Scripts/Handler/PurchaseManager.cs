using UnityEngine;
using System;
using System.Collections;

public class PurchaseManager : MonoBehaviour
{	
	public class IABPurchase
	{
		public string mInProgressString;
		public string mButtonString;
		public string mPrice;
		public string mIdentifier;
		public string mTitle;
		public string mDescription;
		
		public IABPurchase ( object title, object price, object identifier, object description )
		{
			mTitle = title.ToString();
			mPrice = price.ToString();
			mIdentifier = identifier.ToString();
			mDescription = description.ToString();
			
			mButtonString = "Buy " + title + " for $" + mPrice;
			mInProgressString = "Purchasing " + title;
		}
	}

	const string googleIABPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp8TCfp6da6sC6NWonj4teGGeBuwi8Deyz84a0ppuG+4TMXJfDX645GbSJGMA71q2PeeOjWCkFBvWhyd0FMmcquHJA5zoqdm8Mr/CETq81g5m7VazSHX9WGHWuHQOcow34K12zm+VMPLNcof4SBYl0JZnTlfRkTe8FTJ3IWGsVC6mf5u+3JtQbNsfsrhXLdHWOw+TYkyTzrymJaAAFUCrGoMEkRK8AjMShel2vGsm/FQ86RN5qX8lnaZ0s3Er1j/B+K36f9PIcAVSgHqK4SMHLDxQhO1dUBOH6TBwQ6UzzUwn6tXhDeb7h+xSQiE+O6MDADjVFROiNnYbH/LgpWkKmQIDAQAB";
	public static ArrayList mProducts;
	public static Vector2 ScrollViewVector;
	
	public static bool isIABSupported= false;
	public static string purchaseInProgress;
	
	public static string mAlertMessage = "";	
	
	public static bool  mProductsReceived;
	string buyedProductDesc = " ";
	string[] _products = new string[7];
	string[] _productsAmazon = new string[6];
	string[] _productsOS = new string[6];
	string[] _productsMac = new string[6];
	string[] _productsIPhone = new string[6];
	string[] _productsIPad = new string[6];
	public static int platID; //refers to Google Play (0) / Amazon (1) platforms.
	bool  isiOSSupported = false;
	
	UIText text0;
	UIText text1;
	UISprite backGround0;
	UISprite backGround2;
	UIButton xButton;
	
	private bool  firstOnEnable = true;
	void  OnEnable ()
	{
		#if UNITY_ANDROID
//		if(BuildConfig.platformName == "amazon")
//		{
//			if(firstOnEnable)
//				firstOnEnable = false;
//			else
//				AmazonIAP.initiateGetUserIdRequest(); 
//		}
		#endif
	}
	
	void  Start ()
	{
		_products[0] = "com.veraxon.c200";
		_products[1] = "com.veraxon.c730";
		_products[2] = "com.veraxon.c1600";
		_products[3] = "com.veraxon.c2700";
		_products[4] = "com.veraxon.c5500";
		_products[5] = "com.veraxon.c12000";  
		_products[6] = "com.veraxon.c12000";//sorry for this, it is logic to use productsID from RequestProducts() but it seems
		//that the AmazonIAP cannot initiate an Item Data Request. 
		_productsAmazon[0] = "com.worldofmidgard.a12000mcoins";
		_productsAmazon[1] = "com.worldofmidgard.a5500mcoins";
		_productsAmazon[2] = "com.worldofmidgard.a2700mcoins";
		_productsAmazon[3] = "com.worldofmidgard.a1600mcoins";
		_productsAmazon[4] = "com.worldofmidgard.a730mcoins";
		_productsAmazon[5] = "com.worldofmidgard.a200mcoins";  
		
		_productsOS[0] = "com.veraxon.12000mrcoins";
		_productsOS[1] = "com.veraxon.5500mrcoins";
		_productsOS[2] = "com.veraxon.2700mrcoins";
		_productsOS[3] = "com.veraxon.1600mrcoins";
		_productsOS[4] = "com.veraxon.730mrcoins";  
		_productsOS[5] = "com.veraxon.200mrcoins";
		
		_productsMac[0] = "com.veraxon.mc12000mcoins";
		_productsMac[1] = "com.veraxon.mc5500mcoins";
		_productsMac[2] = "com.veraxon.mc2700mcoins";
		_productsMac[3] = "com.veraxon.mc1600mcoins";
		_productsMac[4] = "com.veraxon.mc730mcoins";
		_productsMac[5] = "com.veraxon.mc200mcoins";
		
		_productsIPhone[0] = "com.veraxon.12000iphonemr2";
		_productsIPhone[1] = "com.veraxon.5500iphonemr2";
		_productsIPhone[2] = "com.veraxon.2700iphonemr2";
		_productsIPhone[3] = "com.veraxon.1600iphonemr2";
		_productsIPhone[4] = "com.veraxon.730iphonemr2";
		_productsIPhone[5] = "com.veraxon.200iphonemr2";
		
		_productsIPad[0] = "com.veraxon.12000ipadmr2";
		_productsIPad[1] = "com.veraxon.5500ipadmr2";
		_productsIPad[2] = "com.veraxon.2700ipadmr2";
		_productsIPad[3] = "com.veraxon.1600ipadmr2";
		_productsIPad[4] = "com.veraxon.730ipadmr2";
		_productsIPad[5] = "com.veraxon.200ipadmr2";
		//The first call your app should make is the initiateItemDataRequest method. 
		//It is invalid to call any other methods until this is called. Be sure to setup your event 
		//handlers before calling it as it will end up firing off some events as responses are returned 
		//from Amazon.
		try
		{
			#if UNITY_ANDROID
			if(BuildConfig.platformName == "amazon" || BuildConfig.platformName == "nomit")
			{
				AmazonIAP.initiateItemDataRequest(_productsAmazon); 
			}
			else
			{
				if(!LoginWindow.GplayManagerGO)//on logout dont create another IABAndroidManager
				{
					LoginWindow.GplayManagerGO = (GameObject)Instantiate(GameResources.Load("prefabs/GooglePlay/IABAndroidManager"),
					                                                     Vector3.zero,
					                                                     Quaternion.identity);
					DontDestroyOnLoad(LoginWindow.GplayManagerGO);
				}
				//Init In app billing
				GoogleIAB.init(googleIABPublicKey);
			}
			#endif
			#if UNITY_IPHONE
			isiOSSupported = Prime31.StoreKitBinding.canMakePayments();
			Debug.Log("IOSCOSMIN0: "+isiOSSupported);
			if( BuildConfig.platformName == "iphone" )
				Prime31.StoreKitBinding.requestProductData(_productsIPhone);
			else if( BuildConfig.platformName == "ipad" )
				Prime31.StoreKitBinding.requestProductData(_productsIPad);
			else
				Prime31.StoreKitBinding.requestProductData(_productsOS);//_productsOS[0]+","+_productsOS[1]+","+_productsOS[2]+","+_productsOS[3]+","+_productsOS[4]+","+_productsOS[5]);
			#endif
			#if UNITY_STANDALONE_OSX
			isiOSSupported = StoreKitBinding.canMakePayments();
			Debug.Log("Mac: "+isiOSSupported);
			StoreKitBinding.requestProductData(_productsMac);//[0]+","+_productsOS[1]+","+_productsOS[2]+","+_productsOS[3]+","+_productsOS[4]+","+_productsOS[5]);
			// Mac only methods
			Debug.Log( "is receipt valid: " + StoreKitBinding.validateMacAppStoreReceipt() );
			
			#endif
		}
		catch( Exception err )
		{
			Debug.LogException(err);
		}
	}
	
	public void Init ()
	{
		// IAPid takes 2 values: 0 and 1 ( 0 is for Google Play and 1 is for Amazon)
		
		//		mProducts = new ArrayList();
		//		mProducts.Add( new IABPurchase( "700 Mithril Coins", "$0.99f", "com.worldofmidgard.700mcoins" ) );
		//		mProducts.Add( new IABPurchase( "1,600 Mithril Coins", "$1.99f", "com.worldofmidgard.1600mcoins" ) );
		//		mProducts.Add( new IABPurchase( "4,000 Mithril Coins", "$4.99f", "com.worldofmidgard.4000mcoins" ) );
		//		mProducts.Add( new IABPurchase( "9,000 Mithril Coins", "$9.99f", "com.worldofmidgard.9000mcoins" ) );
		//		mProducts.Add( new IABPurchase( "19,000 Mithril Coins", "$19.99f", "com.worldofmidgard.19000mcoins" ) );
		//		mProducts.Add( new IABPurchase( "55,000 Mithril Coins", "$49.99f", "com.worldofmidgard.55000mcoins" ) );
		//		mProducts.Add( new IABPurchase( "Test purchase", "Free", "android.test.purchased" ) );
		
		mAlertMessage = "";
		purchaseInProgress = "";
		mProductsReceived = false;
		
		backGround0 = UIPrime31.myToolkit4.addSprite( "popup_background.png",0,0,-2);
		backGround0.setSize (Screen.width*0.9f, Screen.height*0.9f);
		backGround0.position = new Vector3 (Screen.width*0.05f, -Screen.height * 0.05f,-2);
		backGround0.hidden = true;
		
		backGround2 = UIPrime31.myToolkit4.addSprite("deco_buy_mithrill.png",0,0,-3);
		backGround2.setSize(Screen.width*0.86f, Screen.height*0.125f);
		backGround2.position = new Vector3(Screen.width*0.067f, -Screen.height*0.0975f,-3);
		backGround2.hidden = true;
		
		xButton = UIButton.create(UIPrime31.myToolkit4,"close_button.png","close_button.png",0,0);
		xButton.position = new Vector3(Screen.width*0.86f, -Screen.height*0.1f, -3);
		xButton.setSize(Screen.height*0.07f,Screen.height*0.07f);
		xButton.onTouchDown += XButtonDelegate;
		xButton.hidden=true;
		
		text0 = new UIText (UIPrime31.myToolkit1, "fontiny dark","fontiny dark.png");
		
		text1 = new UIText (UIPrime31.myToolkit1, "fontiny title","fontiny title.png");
		text1.alignMode = UITextAlignMode.Center;
		
		GameObject obj = GameObject.Find( "PurchaseGO" );
		if(obj != null)
		{	
			#if UNITY_ANDROID
			if(BuildConfig.platformName != "amazon")
			{
				GoogleIABManager.purchaseManager = obj;
			}
			#endif
		}
		else
		{
			Debug.Log("Init purchasing: player not found!");
		}
		RequestProducts();
		//Request the products from server.. rather than hardcode.
	}
	
	bool  alreadyDrawn = false;
	UIButton[] buyButtons; 
	UITextInstance[] textPrice;
	UITextInstance[] textProduct;
	UITextInstance textTitle;
	MainPlayer mainPlayer;
	public void DrawMithrilShop (  MainPlayer mplayer )
	{	
		backGround0.hidden=false;
		backGround2.hidden=false;
		xButton.hidden=false;
		if( !mProductsReceived )
		{
			GUI.Label( new Rect(Screen.width*0.3f,Screen.height*0.35f,Screen.width*0.4f,Screen.height*0.1f), "Retrieving shop products", mplayer.newskin.customStyles[4]);
			return;
		}
		if(!alreadyDrawn)
		{	
			mainPlayer = mplayer;
			mainPlayer.interf.optionsButton.onTouchDown -= mainPlayer.interf.optionsMode;
			mainPlayer.interf.socialButton.onTouchDown -= mainPlayer.interf.SocialDelegate;
			mainPlayer.interf.guildButton.onTouchDown -= mainPlayer.interf.AnimViewMode;
			mainPlayer.interf.spellButton.onTouchDown -= mainPlayer.interf.defaultSpell;
			mainPlayer.interf.spellButton.onTouchDown -= mainPlayer.interf.spellMode;
			mainPlayer.interf.questButton.onTouchDown -= mainPlayer.interf.questMode;
			mainPlayer.interf.inventoryButton.onTouchDown -= mainPlayer.interf.inventoryMode;
			mainPlayer.interf.backButton.onTouchDown -= mainPlayer.interf.backMode;
			if(BuildConfig.hasItemShopButton)
			{
				mainPlayer.interf.shopButtonNew.onTouchDown -= mainPlayer.interf.ItemShopDelegate;
			}
			else
			{
				#if UNITY_ANDROID
				if(BuildConfig.platformName == "amazon")
				{
					mainPlayer.interf.shopButtonNew.onTouchUpInside -= mainPlayer.interf.BuyMithrilIapAmazonDelegate;
				}
				else
				{
					mainPlayer.interf.shopButtonNew.onTouchUpInside -= mainPlayer.interf.BuyMithrilIapDelegate;
				}
				#elif UNITY_IPHONE
				//Do buy mithril ios
				mainPlayer.interf.shopButtonNew.onTouchUpInside -= mainPlayer.interf.BuyMithrilIapDelegate;
				#else
				//Do nothing
				//shopButtonNew.onTouchUpInside -= null;
				#endif
			}
			//mainPlayer.interf.shopButtonNew.onTouchDown -= mainPlayer.interf.ItemShopDelegate;
			mainPlayer.interf.logoutButton.onTouchDown -= mainPlayer.interf.LogoutDelegate;
			
			textTitle = text1.addTextInstance("Buy Mithril", Screen.width*0.5f, Screen.height*0.1f,0.5f * dbs._sizeMod, -4);
			buyButtons = new UIButton[mProducts.Count];
			textPrice = new UITextInstance[ mProducts.Count];
			textProduct = new UITextInstance[ mProducts.Count];
			
			for (int i = 0; i < mProducts.Count; i++)
			{
				IABPurchase product = mProducts[i] as IABPurchase;
				buyButtons[i] = UIButton.create ( UIPrime31.myToolkit4 , "2_selected.png","2_selected.png",0,0);
				buyButtons[i].setSize(Screen.width*0.86f, Screen.height*0.075f);
				buyButtons[i].position = new Vector3(Screen.width*0.07f, -Screen.height*0.25f - Screen.height*0.12f*i,-3);
				buyButtons[i].info=i;
				buyButtons[i].onTouchDown += BuyButtonDelegate;
				buyButtons[i].hidden = false;
				text0.alignMode = UITextAlignMode.Left;
				textProduct[i] = text0.addTextInstance(product.mTitle,Screen.width * 0.15f, Screen.height * 0.26f + Screen.height*0.12f*i , 0.4f * dbs._sizeMod, -4);
				textProduct[i].hidden = false;
				text0.alignMode = UITextAlignMode.Right;
				textPrice[i] = text0.addTextInstance("$ "+product.mPrice,Screen.width*0.87f, Screen.height * 0.26f + Screen.height*0.12f*i , 0.4f * dbs._sizeMod,-4);
				textPrice[i].hidden = false;
				//Debug.Log("title:"+product.mTitle);
				
			}
			
			alreadyDrawn=true;
		}
		//GUI.Label( new Rect(Screen.width*0.3f,Screen.height*0.1f,Screen.width*0.4f,Screen.height*0.1f), "Mithril Shop", mplayer.newskin.customStyles[4]);
		
		
		/*
		GUI.DrawTexture( new Rect(Screen.width*0.0f,Screen.height*0.05f,Screen.width*1.0f,Screen.height*0.9f), mplayer.Background);
		Rect rect = new Rect( Screen.width*0.12f,Screen.height*0.2f, Screen.width*0.8f,Screen.height*0.7f );
		Rect viewRect = new Rect( 0, 0, Screen.width*0.7f, mProducts.Count * Screen.height*0.10f );
		
		ScrollViewVector = GUI.BeginScrollView(rect, ScrollViewVector, viewRect );
		for( int i = 0; i < mProducts.Count; ++i)
		{
			IABPurchase product = mProducts[i] as IABPurchase;
			string btnString = product.mIdentifier == purchaseInProgress ? product.mInProgressString : product.mButtonString;
			if ( GUI.Button( new Rect(Screen.width*0.1f,Screen.height*0.00f + Screen.height*0.10f*i,Screen.width*0.6f,Screen.height*0.095f), btnString, mplayer.newskin.button) )
			{
				//if already purchasing.. don't begin another purchase
				if( purchaseInProgress == "" )
				{
					//Debug.Log( "purchasing item" );
					buyedProductDesc = product.mTitle;
					BuyProduct( product.mIdentifier );
					
				}
			}	
		}		
		GUI.EndScrollView();
		*/
		if( mAlertMessage != "" )
			GUI.Label( new Rect( Screen.width*0.13f, Screen.height*0.2f, Screen.width*0.86f, Screen.height*0.075f ), mAlertMessage );
	}
	
	void  BuyButtonDelegate ( UIButton sender )
	{
		IABPurchase product = mProducts[sender.info] as IABPurchase;
		buyedProductDesc = product.mTitle;
		BuyProduct( product.mIdentifier );	
	}
	
	void  XButtonDelegate ( UIButton sender )
	{	
		mainPlayer.menuSelected = -1;
		backGround0.hidden = true;
		backGround2.hidden = true;
		for (int i = 0; i < mProducts.Count; i++)
		{
			buyButtons[i].hidden=true;
			textProduct[i].hidden=true;
			textPrice[i].hidden=true;
		}
		xButton.hidden=true;
		textTitle.hidden=true;
		alreadyDrawn=false;
		mainPlayer.interf.mainMenuContainer.hidden = false;
		mainPlayer.interf.optionsButton.onTouchDown += mainPlayer.interf.optionsMode;
		mainPlayer.interf.socialButton.onTouchDown += mainPlayer.interf.SocialDelegate;
		mainPlayer.interf.guildButton.onTouchDown += mainPlayer.interf.AnimViewMode;
		mainPlayer.interf.spellButton.onTouchDown += mainPlayer.interf.defaultSpell;
		mainPlayer.interf.spellButton.onTouchDown += mainPlayer.interf.spellMode;
		mainPlayer.interf.questButton.onTouchDown += mainPlayer.interf.questMode;
		mainPlayer.interf.inventoryButton.onTouchDown += mainPlayer.interf.inventoryMode;
		mainPlayer.interf.backButton.onTouchDown += mainPlayer.interf.backMode;
		if(BuildConfig.hasItemShopButton)
		{
			mainPlayer.interf.shopButtonNew.onTouchDown += mainPlayer.interf.ItemShopDelegate;
		}
		else
		{
			#if UNITY_ANDROID
			if(BuildConfig.platformName == "amazon")
			{
				mainPlayer.interf.shopButtonNew.onTouchUpInside += mainPlayer.interf.BuyMithrilIapAmazonDelegate;
			}
			else
			{
				mainPlayer.interf.shopButtonNew.onTouchUpInside += mainPlayer.interf.BuyMithrilIapDelegate;
			}
			#elif UNITY_IPHONE
			//Do buy mithril ios
			mainPlayer.interf.shopButtonNew.onTouchUpInside += mainPlayer.interf.BuyMithrilIapDelegate;
			#else
			//Do nothing
			//shopButtonNew.onTouchUpInside += null;
			#endif
		}
		//mainPlayer.interf.shopButtonNew.onTouchDown += mainPlayer.interf.ItemShopDelegate;
		mainPlayer.interf.logoutButton.onTouchDown += mainPlayer.interf.LogoutDelegate;
		for (int j= 0; j < mainPlayer.interf.woodenBarBackGroundNumber; j++)
		{
			mainPlayer.interf.woodenBarBackGround[j].hidden=false;
		}
		mainPlayer.interf.rowMainMenu.hidden = false;
		ButtonOLD.mithrilShopState = false;
		WorldSession.RequestRefreshMithrilCoinsCount();
	}
	
	// ********************************************************************************
	// * Requests the list for products from the server so user can know what he can purchase.
	// * Calls url that returns: success - int, currency_list - array.
	//
	// * Ex. {"result":{"success":1,"currency_list":[{"id":"android.test.purchased","title":"700 Mithril Coins","desc":"Get 700 Mithril Coins to use in the item shop.","price":0.99f},{"id":"com.lordsatwar.1600coins","title":"1600 Mithril Coins","desc":"Get 1600 Mithril Coins to use in the item shop.","price":1.99f}]}}
	// ********************************************************************************
	IEnumerator RequestProducts ()
	{	
		
		string mUrl;
		string currency_type = "";
		#if UNITY_IPHONE
		Debug.Log( "Requesting products for IOS" );
		if( BuildConfig.platformName == "iphone" )
			currency_type = "iphone";
		else if( BuildConfig.platformName == "ipad" )
			currency_type = "ipad";
		else
			currency_type = "itunes3";
		#elif UNITY_STANDALONE_OSX
		Debug.Log( "Requesting products for MAC" );
		currency_type = "mac_app_store";
		#else
		if(BuildConfig.platformName == "amazon")
		{
			Debug.Log( "Requesting products for Amazon" );
			currency_type = "amazon2";
		}
		else
		{
			Debug.Log( "Requesting products for GPlay" );
			currency_type = "iab";
		}
		#endif
		mUrl = "https://mithril.worldofmidgard.com/worldofmidgard/functions/get_currency_list.php?type=" + currency_type;
		WWW w= new WWW( mUrl );
		while(!w.isDone)
		{
			yield return w;
		}
		while(!String.IsNullOrEmpty(w.error) && w.error.ToString().Contains("Resolving host timed out"))
		{
			w = new WWW(mUrl);
			while(!w.isDone)
			{
				yield return w;
			}
		}
		print("Product responce = "+w.text);
		if(!String.IsNullOrEmpty(w.error))
		{
			Debug.Log( w.error );
			mAlertMessage = "Requesting products has failed";
		}
		else
		{
			Debug.Log( "Products received: " + w.text );
			string json = w.text;
			Hashtable response = MiniJSON.jsonDecode( json ) as Hashtable;
			if( response == null )
			{
				mAlertMessage = "Product retrieval has failed, Please try again";
				return false;
			}
			Hashtable result = null;
			if( response.ContainsKey( "result" ) )
			{
				result = response["result"] as Hashtable;
				if( result == null )
				{	
					mAlertMessage = "Product retrieval has failed, Please try again";
					return false;
				}
			}
			else
			{
				mAlertMessage = "Product retrieval has failed, Please try again";
				return false;
			}
			if( !result.ContainsKey("success") || (int)result["success"] != 1 )
			{
				mAlertMessage = "Product retrieval has failed, Please try again";
				return false;
			}
			
			Hashtable products = (Hashtable)result["currency_list"];
			if( products == null || products.Count == 0 )
			{
				mAlertMessage = "Product retrieval has failed, Please try again";
				return false;
			}
			mProducts = new ArrayList();
			
			string[] productsID = new string[products.Count];
			
			for( int i = 0; i < products.Count; ++i )
			{
				Hashtable product = (Hashtable)products[i]; 
				mProducts.Add( new IABPurchase( product["title"], product["price"], product["id"], product["desc"] ) );
				Debug.Log(""+product["title"]+", "+product["price"]+", "+product["id"]+", "+product["desc"]);
				productsID[i] = (string)product["id"];
			}
			mProductsReceived = true;				
		}		
		
	}
	
	
	void  PurchaseFailed (object err )
	{
		//print( "Purchase has failed with error: " + err );
		Debug.Log("IOSCOSMIN4: "+ err);
		purchaseInProgress = "";
		mAlertMessage = "Error: " + err;
	}
	
	void  PurchaseFailed ()
	{
		purchaseInProgress = "";
	}
	
	IEnumerator  PurchaseVerifySignature (object parts )
	{
		Debug.Log( "php verify signature: " + parts );		
		string mUrl = "https://mithril.worldofmidgard.com/worldofmidgard/functions/verify_iab_purchase.php";
		WWWForm form= new WWWForm();
		form.AddField( "account_email", LoginWindow.accountLogin );
		form.AddField( "signed_data", ( parts as string[] )[0] );
		form.AddField( "signature", ( parts as string[] )[1] );
		
		
		WWW w = new WWW( mUrl, form );
		while(!w.isDone)
		{
			yield return w;
		}
		while(!String.IsNullOrEmpty(w.error) && w.error.ToString().Contains("Resolving host timed out"))
		{
			w = new WWW(mUrl, form);
			while(!w.isDone)
			{
				yield return w;
			}
		}
		if(!String.IsNullOrEmpty(w.error))
		{
			Debug.Log( w.error );
			mAlertMessage = "Purchase verification failed";
		}
		else
		{
			string json = w.text;
			Hashtable response = MiniJSON.jsonDecode( json ) as Hashtable;
			if( response == null )
			{
				mAlertMessage = "Receipt verification has failed";
				purchaseInProgress = "";
				return false;
			}
			Hashtable result = null;
			if( response.ContainsKey( "result" ) )
			{
				result = response["result"] as Hashtable;
				if( result == null )
				{	
					mAlertMessage = "Receipt verification has failed";
					purchaseInProgress = "";
					return false;
				}
			}				
			if( (int)result["success"] == 1 )
				mAlertMessage = "You have succesfully purchased: " + result["currency_amount"] + " Mithril Coins";
			else if( (int)result["success"] == 0 )
				mAlertMessage = "Receipt verification has failed because: "+" "+ result["reason"]+ " : " + result["desc"];			
			
		}
		purchaseInProgress = "";	
	}
	
	IEnumerator  VerifyiTunesReceipt ( string recipe )
	{
		Debug.Log( "IOSCOSMIN1 ITUNESVFTR: " + recipe );
		string mUrl = "";
		#if UNITY_IPHONE		
		if( BuildConfig.platformName == "iphone" )
			mUrl = "https://mithril.worldofmidgard.com/worldofmidgard/functions/itunes/verify_iphone_receipt.php";
		else if( BuildConfig.platformName == "ipad" )
			mUrl = "https://mithril.worldofmidgard.com/worldofmidgard/functions/itunes/verify_ipad_receipt.php";
		else
			mUrl = "https://mithril.worldofmidgard.com/worldofmidgard/functions/itunes/verify_itunes3_receipt.php";
		#endif
		#if UNITY_STANDALONE_OSX
		mUrl = "https://mithril.worldofmidgard.com/worldofmidgard/functions/itunes/verify_mac_app_store_receipt.php";
		#endif		
		
		WWWForm form= new WWWForm();	
		form.AddField( "account_email", LoginWindow.accountLogin );
		form.AddField( "receipt_data", recipe );
		form.AddField( "mode", 1 );
		//Debug.Log("IOSCOSMIN2: WAITING  "+mUrl+"    "+logIn.nume+"    "+recipe+"   mode 1");
		WWW w= new WWW( mUrl, form );
		while(!w.isDone)
		{
			yield return w;
		}
		while(!String.IsNullOrEmpty(w.error) && w.error.ToString().Contains("Resolving host timed out"))
		{
			w = new WWW(mUrl, form);
			while(!w.isDone)
			{
				yield return w;
			}
		}
		if(!String.IsNullOrEmpty(w.error))
		{
			Debug.Log( w.error );
			mAlertMessage = "Purchase verification failed";
		}
		else
		{
			string json = w.text;
			Hashtable response = MiniJSON.jsonDecode( json ) as Hashtable;
			if( response == null )
			{
				mAlertMessage = "Receipt verification has failed";
				purchaseInProgress = "";
				return false;
			}
			Hashtable result = null;
			if( response.ContainsKey( "result" ) )
			{
				result = response["result"] as Hashtable;
				if( result == null )
				{	
					mAlertMessage = "Receipt verification has failed";
					purchaseInProgress = "";
					return false;
				}
			}				
			if( (int)result["success"] == 1 )
				mAlertMessage = "You have succesfully purchased: " + result["currency_amount"] + " Mithril Coins";
			else if( (int)result["success"] == 0 )
				mAlertMessage = "Receipt verification has failed because: "+" "+ result["reason"]+ " : " + result["desc"];			
			
		}
		purchaseInProgress = "";	
		
	}
	IEnumerator  PurchaseVerifySignatureForAmazon (  string parts )
	{
		#if UNITY_ANDROID
		string mUrl = "https://mithril.worldofmidgard.com/worldofmidgard/functions/amazon_receipt_verification_service.php?type=amazon2";
		//string mUrl = "https://mithril.worldofmidgard.com/wom-dev-5627275/functions/amazon_receipt_verification_service.php?type=amazon2";
		WWWForm form= new WWWForm();
		form.AddField( "user_id", AmazonIAPEventListener.UserId );
		form.AddField( "account_login", LoginWindow.accountLogin );
		form.AddField( "purchase_token", parts); //the token received from amazon
		Debug.Log("COSMIN2: "+parts);
		
		WWW w= new WWW( mUrl, form );
		while(!w.isDone)
		{
			yield return w;
		}
		while(!String.IsNullOrEmpty(w.error) && w.error.ToString().Contains("Resolving host timed out"))
		{
			w = new WWW(mUrl, form);
			while(!w.isDone)
			{
				yield return w;
			}
		}
		if(!String.IsNullOrEmpty(w.error))
		{
			Debug.Log( w.error );
			mAlertMessage = "Purchase verification failed";
		}
		else
		{
			string json = w.text;
			Hashtable response = MiniJSON.jsonDecode( json ) as Hashtable;
			Hashtable result = null;
			if( response == null )
			{
				mAlertMessage = "Receipt verification has failed";
				purchaseInProgress = "";
				return false;
			}
			
			if( response.ContainsKey( "result" ) )
			{
				result = response["result"] as Hashtable;
				if( result == null )
				{	
					mAlertMessage = "Receipt verification has failed";
					purchaseInProgress = "";
					return false;
				}
			}				
			if( (int)result["success"] == 1 )
				mAlertMessage = "You have succesfully purchased: " + buyedProductDesc;
			else if( (int)result["success"] == 0 )
				mAlertMessage = "Receipt verification has failed because: "+" "+ result["reason"]+ " : " + result["desc"];			
		}
		purchaseInProgress = "";
		#else
		yield return true;
		#endif	
	}
	
	void  BuyProduct (object identifier )
	{
		string productId = identifier.ToString();
		//#if UNITY_IPHONE 
		Debug.Log(identifier);
		#if UNITY_IPHONE
		if(isiOSSupported == true)
		{
			Debug.Log("COSMIN6 PURCHAISING");
			purchaseInProgress = productId;
			Prime31.StoreKitBinding.purchaseProduct( productId, 1 );
		}
		else
			mAlertMessage = "iTunes  unavailable";
		#elif UNITY_STANDALONE_OSX
		if(isiOSSupported == true)
		{
			Debug.Log("COSMIN6 PURCHAISING");
			purchaseInProgress = productId;
			StoreKitBinding.purchaseProduct( productId, 1 );
		}
		else
			mAlertMessage = "iTunes  unavailable";
		#endif
		
		#if UNITY_ANDROID
		if(platID == 1)
		{
			AmazonIAP.initiatePurchaseRequest( productId );
			Debug.Log("COSMIN3: "+productId);
			mAlertMessage = "AmazonIAP.initiatePurchaseRequest ("+productId+");";
		}
		else
		{
			if( isIABSupported )
			{
				purchaseInProgress = productId;
				GoogleIAB.purchaseProduct( productId );
				mAlertMessage = "GoogleIAB.purchaseProduct("+productId+");";
			}
			else
				mAlertMessage = "Google Marketplace unavailable";
		}
		#endif
		
		//#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN
		mAlertMessage = "Google Marketplace, Amazon and iTunes unavailable(by platform)";
		#endif
	}
	
	void  IABSupported (  bool supported )
	{
		isIABSupported = supported;
	}
	
	void  VerifySignatureFail (object err )
	{
		mAlertMessage = "Local signature verification has failed: " + err;
		purchaseInProgress = "";
	}
}