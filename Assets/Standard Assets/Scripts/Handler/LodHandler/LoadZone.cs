using UnityEngine;
using System;

public class LoadZone : MonoBehaviour
{
	//this static variable is initializated with the map id ,each time we teleport to a new map/dungeon
	//the server send to client the id of the map
	public string zone = /*"Dungen1"*/"603"; //a default id for the doungen
	public static int zoneID = 603;
	//static bool  loaded = false;

	void  Start ()
	{
		DontDestroyOnLoad(this);

		GameObject interfacePrefab = GameObject.Instantiate(UnityEngine.Resources.Load<GameObject>("Interface Prefabs/Interface"));
		WorldSession.interfaceManager = interfacePrefab.GetComponent<InterfaceManager>();
		
		Debug.Log("Test");
	}
	
	void  FixedUpdate ()
	{
		if((MapLoadDebug.instanceMapEnd == 0) && (GameObject.Find("world/mesh")))
		{
			MapLoadDebug.instanceMapEnd = Time.realtimeSinceStartup;
		}
	}
	
	//loading the map after his id is obtained from server
	public void  load ()
	{
		string zoneName = "";
		//print( "switching to " + dungeon);
		switch(zone)
		{  
		case "33"  : zoneName = "mine";  break;
		case "36"  : zoneName = "mine";break; //wherewolf
		case "129" : zoneName = "Helenic";break;
		case "34"  : zoneName = "gem_cave";break;
		case "43"  : zoneName = "catacombes";break;
		case "47"  : zoneName = "gothic_tower";break;
		case "48"  : zoneName = "lava";break;
		case "90"  : zoneName = "spiral_tower";break;
		case "109" : zoneName = "tomb";break;
		case "560" : zoneName = "lava";break; //lava 2
		case "289" : zoneName = "Ghotic_castle";break;
		case "349" : zoneName = "Ghotic_castle_dungeon";break;
		case "389" : zoneName = "Ghotic_castle_dungeon";break;
		case "553" : zoneName = "lava";break;
		case "554" : zoneName = "spiral_tower";break;
		case "555" : zoneName = "gothic_tower";break;
		case "556" : zoneName = "tomb"; break;
		case "557" : zoneName = "gem_cave"; break;
		case "558" : zoneName = "Helenic";break;
			
		case "564" : zoneName = "catacombes";break;
		case "590" : zoneName = "coliseum2";break;
			
			
		case "584" : zoneName = "yaxche"; break;
		case "587" : zoneName = "yaxche"; break;
		case "552" : zoneName = "Ghotic_castle";break;
		case "588" : zoneName = "Ghotic_castle";break;
		case "543" : zoneName = "battleground"; break;
		case "545" : zoneName = "strongburg_village"; break;
		case "546" : zoneName = "strongburg_city"; break;

		case "571" : zoneName = "Nordic_Country_Zone"; break;
			
		case "576" : zoneName = "desert";break;
		case "578" : zoneName = "Molten_chasm"; break;
		case "585" : zoneName = "passage";break;
		case "547" : zoneName = "cathedral";break;
			
		case "589" : zoneName = "Glevel9"; break;
		case "596" : zoneName = "GLevelLP"; break;
		case "595" : zoneName = "Medieval_Castle"; break;
		case "597" : zoneName = "Canyoun"; break;
		case "598" : zoneName = "coliseum"; break;
		case "599" : zoneName = "Ghotic_castle_normal"; break;
			
		case "600" : zoneName = "Ghotic_castle_blood"; break;
		case "601" : zoneName = "Ghotic_castle_dark"; break;
		case "602" : zoneName = "Ghotic_castle_old"; break;
		case "604" : zoneName = "Ghotic_castle_sand"; break;
			
		case "1"   : zoneName = "desert";break;
		case "13"  : zoneName = "sand_city";break;
		case "37"  : zoneName = "battleground"; break;
		case "209" : zoneName = "yaxche"; break;
		case "42"  : zoneName = "jungle";  break;
		case "573" : zoneName = "strongburg_village"; break;
		case "369" : zoneName = "strongburg_city"; break;
			
		case "0"   : zoneName = "spawn_area";break; //human
		case "35"  : zoneName = "spawn_area";break; //orc
			//case "530" : zoneName = "spawn_area";break; //dwarf old area (instanced area)
		case "25"  : zoneName = "spawn_area";break; //dwarf new area
		case "723" : zoneName = "spawn_area";break;	//blood drak
		case "608" : zoneName = "battleground608"; break;
		case "609" : zoneName = "spawn_area";break; // elf
		case "586" : zoneName = "spawn_area";break; //dark elf
		case "623" : zoneName = "TEMP/Tundra_new";break; // tundra test
			
			
		case "632" : zoneName = "strongburg_village"; break;
		case "650" : zoneName = "strongburg_city"; break;
		case "658" : zoneName = "Cave"; break;
		case "668" : zoneName = "Scene_06"; break;
			
		case "672" : zoneName = "Scene_05_2"; break;
		case "673" : zoneName = "Snow_Scene_2"; break;
		case "712" : zoneName = "MEP_Desert_2"; break;
		case "713" : zoneName = "Molten_chasm"; break;
			
		case "718" : zoneName = "Ghotic_castle";break;
		case "450" : zoneName = "Ghotic_castle_dungeon";break;
		case "575" : zoneName = "jungle_dungeon";break;
		case "449" : zoneName = "Cave"; break;
			
		case "582"  : zoneName = "battleground"; break;
		case "489"  : zoneName = "battleground"; break;
			
		case "605" : zoneName = "Snow_Scene"; break;
		case "606" : zoneName = "Scene_01"; break;
		case "610" : zoneName = "Scene_05"; break;
		case "612" : zoneName = "Scene_03"; break;
		case "613" : zoneName = "MEP_Desert"; break;
		case "614" : zoneName = "Dungeon_1a"; break;
			//			case "620" : zoneName = "DeadTower"; break;
		case "620" : zoneName = "Ghotic_castle_dark"; break;
		case "621" : zoneName = "SkyVillage"; break;
		case "622" : zoneName = "DungeonCS"; break;
		case "641" : zoneName = "Desert_castle_map"; break;
		case "642" : zoneName = "Palace_of_Orinthalian"; break;
			
		case "44"  : zoneName = "cathedral";break; // aliance spawn area
		case "70"  : zoneName = "cathedral";break; // fury spawn area
			
		case "189" : zoneName = "NewSpawnArea"; break;
		case "429" : zoneName = "NewSpawnArea"; break;
		case "534" : zoneName = "NewSpawnArea"; break;
		case "540" : zoneName = "NewSpawnArea"; break;
		case "542" : zoneName = "NewSpawnArea"; break;
			
		case "451" : zoneName = "Ghotic_castle";break;
			
		case "269" : zoneName ="dev_island";break; //dev map
			
		default : zoneName = "spawn_area";break;
		}
		//print( "switching to " + dungeonName);
		//we instantiate a map 
		//print("Entering [ "+dungeonName+" ] Dungeon");
		LoadMapBundle(zoneName);
	}
	
	void  LoadMapBundle ( string zoneName )
	{
		GameObject world  = GameObject.Find("world");
		if(world)
			Destroy(world);
		
		MapLoadDebug.mapBundleLoadingStartTime = Time.realtimeSinceStartup;
		WorldSession.GetBundleLoader().LoadMapBundle(LoadMap, "prefabs/Maps/" + zoneName);
	}
	
	void  LoadMap ( string bundleName )
	{
		MapLoadDebug.mapBundleLoadingEndTime = Time.realtimeSinceStartup;
		MapLoadDebug.worldInstanceEndTime = 0;
		MapLoadDebug.instanceMapEnd = 0;
		string assetBundleName = string.Format ("Resources/{0}_prefab.unity3d", bundleName);
		assetBundleName = assetBundleName.ToLower();
		UnityEngine.Object bundle = null;
		if(AssetBundleManager.instance.currentLoadedBundles.ContainsKey(assetBundleName))
		{
			bundle = AssetBundleManager.instance.currentLoadedBundles[assetBundleName];
		}
		GameObject world = null;
		if(bundle == null)
		{
			#if UNITY_EDITOR
			Debug.LogError("cannot load "+assetBundleName);
			#endif
		}
		else
		{
			world = bundle as GameObject;
		}
		if(!world)
		{
			world = GameResources.Load<GameObject>(bundleName);
		}
		if(world)
		{
			world = Instantiate(world);
			world.name = "world";//change the name of the instatiated map to "world"
		}
		else
		{
			throw new Exception("World " + bundleName + " not found!");
		}
		//		WorldSession.GetBundleLoader().UnloadBundle(bundleName);
		zoneID = int.Parse(zone);
		GameObject areaTriggers = SetAreaTriggers();
		areaTriggers.transform.parent = world.transform;
		
//		GameResources.Characters.Clear();
//		GameResources.Mobs.Clear();
//		GameResources.Weapons.Clear();
		MapLoadDebug.assableMainPlayerBegin = Time.realtimeSinceStartup;
		MapLoadDebug.assableMainPlayerEnd = 0;
		GetMainCamSwipe().StartSpawnMainChar();
	}
	
	static GameObject SetAreaTriggers ()
	{
		try
		{
			GameObject areaTriggerHolder = new GameObject("AreaTriggerHolder");
			areaTriggerHolder.transform.position = Vector3.zero;
			areaTriggerHolder.transform.rotation = Quaternion.identity;
			
			GameObject resource = GameResources.Load<GameObject>("prefabs/objects/AreaTrigger");
			if(!resource)
			{
				return new GameObject("Error");;
			}
			
			AreaTriggerEntry areaTrigger;
			for(int i = 0; i<dbs.sAreaTrigger.Records; i++)
			{
				areaTrigger = dbs.sAreaTrigger.GetRecordFromIndex(i);
				if(areaTrigger.ID != 0 && areaTrigger.MapID == zoneID)
				{
					//Debug.Log(new Vector3(areaTrigger.x, areaTrigger.y, areaTrigger.z)+ "  radius: "+YardsToMeters(areaTrigger.Radius));
					GameObject go = Instantiate(resource, new Vector3(areaTrigger.x, areaTrigger.z, areaTrigger.y), Quaternion.identity) as GameObject;
					
					(go.GetComponent<AreaTrigger>() as AreaTrigger).areaTrigger = areaTrigger;
					
//					float _radius = YardsToMeters(areaTrigger.Radius);
					var box= (go.GetComponent<BoxCollider>() as BoxCollider);
					box.size = new Vector3((areaTrigger.BoxX),(areaTrigger.BoxY),(areaTrigger.BoxZ));
					box.center = new Vector3(box.center.x, (areaTrigger.BoxY)*0.5f, box.center.z);
					
					go.transform.parent = areaTriggerHolder.transform;
					Quaternion tmpRotation = go.transform.rotation;
					tmpRotation.eulerAngles = new Vector3(go.transform.rotation.eulerAngles.x,
					                                      areaTrigger.BoxO * 57.324f,
					                                      go.transform.rotation.eulerAngles.z);
					go.transform.rotation = tmpRotation;
					go.layer = 19;
				}
			}	
			
			return areaTriggerHolder;
		}catch( Exception exception ){Debug.Log(exception); return new GameObject("Error");}
	}
	
	static float ONE_YARD_IN_METERS = 0.9144f;
	static float YardsToMeters ( float yards )
	{
		return (yards* ONE_YARD_IN_METERS);
	}
	
	Swipe GetMainCamSwipe ()
	{
		Swipe mainCamSwipe = null;
		GameObject go = GameObject.Find("Main Camera");
		if(go)
		{
			mainCamSwipe = go.GetComponent<Swipe>();
		}
		return mainCamSwipe;
	}
	
}