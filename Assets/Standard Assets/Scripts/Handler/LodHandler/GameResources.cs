﻿using System;
using UnityEngine;

public class GameResources
{
	public static GameObject InstantiateChar(string name)
	{
		return UnityEngine.Object.Instantiate<GameObject> (Resources.Load<GameObject> (string.Format ("prefabs/chars/{0}", name)));
	}
	
	public static T Load<T>(string path) where T : UnityEngine.Object
	{
		T obj = Resources.Load<T> (path);
		if(obj != null)
		{
			return obj;
		}
		throw new Exception (string.Format ("The resource can't be found at path: {0} of type: {1}", path, typeof(T)));
	}
}
