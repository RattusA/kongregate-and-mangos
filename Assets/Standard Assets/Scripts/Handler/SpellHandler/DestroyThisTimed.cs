﻿using UnityEngine;
using System.Collections;

public class DestroyThisTimed : MonoBehaviour
{
	public float destroyTime = 5;

	// Use this for initialization
	void Start ()
	{
		Destroy (gameObject, destroyTime);
	}
}
