﻿using UnityEngine;
using System.Collections.Generic;

public class TraitsManager
{
	private static Dictionary<int, TalentButton> firstTraitsBuild = new Dictionary<int, TalentButton>();
	private static Dictionary<int, TalentButton> secondTraitsBuild = new Dictionary<int, TalentButton>();
	private static Dictionary<int, TalentButton> petTraitsBuild = new Dictionary<int, TalentButton>();
	private static int[] unspendTraits = new int[3];
	
	private static Dictionary<int, TalentButton> activeTraitList = new Dictionary<int, TalentButton>();
	private static byte activeList;
	private static byte activeTraits;
	public static bool  needUpdateTraitWindow = false;
	
	public static TalentButton[] traitsButtonInfo = new TalentButton[16];
	public static string[] traitsButtonIcon = new string[16];
	public static int unspendTraitsInTab = 0;
	public static int spendTraitsInTab = 0;
	
	public static string investSpellIcon;
	public static string traitsRank;
	public static string spellNameInfo;
	private static string spellDescriptionInfo;
	private static string nextRankNameInfo;
	private static string nextRankDescriptionInfo;
	public static string canLearnTalent;
	public static string descriptionText;
	
	private const uint TALENTS_PER_TEAR = 5;
	private const uint PET_TALENTS_PER_TEAR = 3;
	
	static public void  readPacketInfo ( WorldPacket pkt )
	{
		byte traitsType= pkt.Read();
		if(traitsType == 0)
		{
			readPlayerTraitsInfo(pkt);
		}
		else
		{
			readPetTraitsInfo(pkt);
		}
		
		if(needUpdateTraitWindow)
		{
			setActiveTraitList(activeList);
			if(traitsType == 0)
			{
				WorldSession.player.interf.traitsWindow.initTraitsWindow();
			}
			else
			{
				WorldSession.player.interf.traitsWindow.initPetTraitsWindow();
			}
			needUpdateTraitWindow = false;
		}
	}
	
	private static void  readPetTraitsInfo ( WorldPacket pkt )
	{
		petTraitsBuild.Clear();
		
		int unspentPetTalents = pkt.ReadInt();
		unspendTraits[2] = unspentPetTalents;
		
		byte talentPetCount = pkt.Read();
		for(byte i = 0; i < talentPetCount; i++)
		{
			TalentButton talentButton = new TalentButton();
			talentButton.talentID = pkt.ReadInt();
			talentButton.rank = pkt.Read() + 1;
			
			TalentEntry talentEntry = dbs.sTalents.GetRecord(talentButton.talentID);
			switch (talentButton.rank)
			{
			case 2:
				talentButton.spellID = talentEntry.SpellID2;
				break;
			case 3:
				talentButton.spellID = talentEntry.SpellID3;
				break;
			default: 
				talentButton.spellID = talentEntry.SpellID1;
				break;
			}
			
			talentButton.tabID = dbs.sTalents.GetRecord(talentButton.talentID).TabID;
			talentButton.column = dbs.sTalents.GetRecord(talentButton.talentID).Column;
			talentButton.row = dbs.sTalents.GetRecord(talentButton.talentID).Row;
			talentButton.maxRank = getMaxRank(talentEntry.ID);
			
			int iconID = dbs.sSpells.GetRecord(talentButton.spellID).SpellIconID;
			talentButton.icon = dbs.sSpellIcons.GetRecord(iconID).Icon;
			
			if (string.IsNullOrEmpty (talentButton.icon))
			{
				talentButton.icon = "icon.png";
			}
			
			petTraitsBuild[talentButton.talentID] = talentButton;
		}
	}
	
	private static void  readPlayerTraitsInfo ( WorldPacket pkt )
	{
		firstTraitsBuild.Clear();
		secondTraitsBuild.Clear();
		
		int unspentTalents = pkt.ReadInt();
		byte specsCount = pkt.Read();
		activeTraits = pkt.Read();
		unspendTraits[activeTraits] = unspentTalents;
		
		for(byte index = 0; index < specsCount; index++)
		{
			byte talentPlayerCount = pkt.Read();
			
			for(byte i = 0; i < talentPlayerCount; i++)
			{
				TalentButton talentButton = new TalentButton();
				talentButton.talentID = pkt.ReadInt();
				talentButton.rank = pkt.Read() + 1;
				
				TalentEntry talentEntry = dbs.sTalents.GetRecord(talentButton.talentID);
				switch (talentButton.rank)
				{
				case 2:
					talentButton.spellID = talentEntry.SpellID2;
					break;
				case 3:
					talentButton.spellID = talentEntry.SpellID3;
					break;
				case 4:
					talentButton.spellID = talentEntry.SpellID4;
					break;
				case 5:
					talentButton.spellID = talentEntry.SpellID5;
					break;
				default: 
					talentButton.spellID = talentEntry.SpellID1;
					break;
				}
				
				talentButton.tabID = dbs.sTalents.GetRecord(talentButton.talentID).TabID;
				talentButton.column = dbs.sTalents.GetRecord(talentButton.talentID).Column;
				talentButton.row = dbs.sTalents.GetRecord(talentButton.talentID).Row;
				talentButton.maxRank = getMaxRank(talentEntry.ID);
				
				int iconID = dbs.sSpells.GetRecord(talentButton.spellID).SpellIconID;
				talentButton.icon = dbs.sSpellIcons.GetRecord(iconID).Icon;
				
				if (string.IsNullOrEmpty (talentButton.icon))
				{
					talentButton.icon = "icon.png";
				}			
				
				switch(index)
				{
				case 0:
					firstTraitsBuild[talentButton.talentID] = talentButton;
					break;
				case 1:
					secondTraitsBuild[talentButton.talentID] = talentButton;
					break;
				}
			}
			
			// Символы
			WorldSession.interfaceManager.playerMenuMng.SpellBookMng.GlyphsWnd.GlyphsMng.SetGlyphsSlots(pkt);			
		}		
	}
	
	static public void  initTraitsInfoByTab ( int selectedTab, uint page )
	{
		int selectedTabID = GetTabID(selectedTab);
		
		initTraitsIcons(selectedTabID, page);
		unspendTraitsInTab = GetUnspendPoints(activeList);
		spendTraitsInTab = getSpentTalentsInTab(selectedTabID);
		
		clearTraitsButtonInfo();
		
		for(int index = 0; index < dbs.sTalents.Records; index++)
		{
			TalentEntry talentEntry = dbs.sTalents.GetRecordFromIndex(index);
			
			if(talentEntry.TabID != selectedTabID || talentEntry.Row > 3)
			{
				continue;
			}
			else
			{
				int buttonNumber;
				if(page == 1 && talentEntry.Column < 4)
				{
					buttonNumber = talentEntry.Row * 4 + talentEntry.Column;
				}
				else if(page == 2 && talentEntry.Column < 7 && talentEntry.Column > 3)
				{
					buttonNumber = talentEntry.Row * 4 + talentEntry.Column - 4;
				}
				else
				{
					continue;
				}
				
				traitsButtonInfo[buttonNumber] = new TalentButton();
				traitsButtonInfo[buttonNumber].talentID = talentEntry.ID;
				traitsButtonInfo[buttonNumber].spellID = talentEntry.SpellID1;
				
				TalentButton checkValue;
				activeTraitList.TryGetValue(talentEntry.ID, out checkValue);
				
				if(checkValue != null)
				{
					traitsButtonInfo[buttonNumber].rank = activeTraitList[talentEntry.ID].rank;
					traitsButtonInfo[buttonNumber].spellID = activeTraitList[talentEntry.ID].spellID;
					traitsButtonInfo[buttonNumber].maxRank = activeTraitList[talentEntry.ID].maxRank;
				}
				else
				{
					traitsButtonInfo[buttonNumber].rank = 0;
					traitsButtonInfo[buttonNumber].maxRank = getMaxRank(talentEntry.ID);
					traitsButtonInfo[buttonNumber].column = talentEntry.Column;
					traitsButtonInfo[buttonNumber].row = talentEntry.Row;
				}
			}
		}
	}
	
	private static void  clearTraitsButtonInfo ()
	{
		for(byte index = 0; index < 16; index++)
		{
			traitsButtonInfo[index] = null;			
		}
	}
	
	private static uint getMaxRank ( int talentId  )
	{	
		uint ret = 0;
		if(dbs.sTalents.GetRecord(talentId).SpellID5 != 0)
			ret = 5;
		else if(dbs.sTalents.GetRecord(talentId).SpellID4 != 0)
			ret = 4;
		else if(dbs.sTalents.GetRecord(talentId).SpellID3 != 0)
			ret = 3;
		else if(dbs.sTalents.GetRecord(talentId).SpellID2 != 0)
			ret = 2;
		else if(dbs.sTalents.GetRecord(talentId).SpellID1 != 0)
			ret = 1;
		return ret;
	}
	
	private static void  initTraitsIcons ( int selectedTab ,   uint page  )
	{
		for(byte iconsCount = 0; iconsCount < 16; iconsCount++)
		{
			traitsButtonIcon[iconsCount] = "lock_button.png";
		}
		
		foreach(KeyValuePair<int, TalentButton> item in activeTraitList)
		{
			if(item.Value.tabID == selectedTab)
			{
				int position;
				
				if(page == 1 && item.Value.column < 4)
				{
					position = item.Value.row * 4 + item.Value.column;
					traitsButtonIcon[position] = item.Value.icon;
				}
				else if(page == 2 && item.Value.column < 7 && item.Value.column > 3)
				{
					position = item.Value.row * 4 + item.Value.column - 4;
					traitsButtonIcon[position] = item.Value.icon;
				}
			}
		}
	}
	
	private static int getSpentTalentsInTab ( int selectedTab )
	{
		int count = 0;
		
		foreach(KeyValuePair<int, TalentButton> item in activeTraitList)
		{
			if(item.Value.tabID == selectedTab)
			{
				count += item.Value.rank/* + 1*/;
			}
		}
		return count;
	}
	
	static public void  setActiveTraitList ( byte index )
	{
		// 0 - pet trait list
		// 1 - player trait list
		
		activeList = index;
		switch(activeList)
		{
		case 0:
			activeTraitList = new Dictionary<int, TalentButton>(petTraitsBuild);
			break;
		case 1:
			switch(activeTraits)
			{
			case 0:
				activeTraitList = new Dictionary<int, TalentButton>(firstTraitsBuild);
				break;
			case 1:
				activeTraitList = new Dictionary<int, TalentButton>(secondTraitsBuild);
				break;
			}
			break;
		}
		
	}
	
	public static int GetUnspendPoints ( byte index )
	{
		int ret = 0;
		switch(index)
		{
		case 0:
			ret = unspendTraits[2];
			break;
		case 1:
			ret = unspendTraits[activeTraits];
			break;
		}
		return ret;
	}
	
	static public int  getActionTraitIndex ()
	{
		if(activeList == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	
	static public int GetTabID ( int tab )
	{
		int ret = 0;
		int cls = 0;
		if(activeList != 0)
		{
			int playerClass = (int)WorldSession.player.playerClass;
			cls = (int)Mathf.Pow(2.0f, (float)(playerClass-1));
		}
		
		for (int index = 0; index < dbs.sTalentTab.Records; index++)
		{
			if (dbs.sTalentTab.GetRecordFromIndex(index).Classes == cls && dbs.sTalentTab.GetRecordFromIndex(index).TabNumber == tab)
			{
				ret = dbs.sTalentTab.GetRecordFromIndex(index).ID;
			}
		}
		return ret;
	}
	
	static public string GetTabName ( int tab  )
	{
		float classnr = 0;
		if(activeList != 0)
		{
			int playerClass = (int)WorldSession.player.playerClass;
			classnr = Mathf.Pow(2.0f, (float)(playerClass-1));
		}
		
		for (int i= 0; i < dbs.sTalentTab.Records; i++)
		{
			if (dbs.sTalentTab.GetRecordFromIndex(i).TabNumber == tab && dbs.sTalentTab.GetRecordFromIndex(i).Classes == classnr)
			{
				return dbs.sTalentTab.GetRecordFromIndex(i).Name.Strings[0];
			}
		}
		return "Error!";
	}
	
	static public void  prepareTallentInfo ( int index )
	{	
		investSpellIcon = "";
		traitsRank = "";
		spellNameInfo = "";
		spellDescriptionInfo = "";
		nextRankNameInfo = "";
		nextRankDescriptionInfo = "";
		canLearnTalent = "";
		descriptionText = "";
		
		TalentButton selectedTalent = traitsButtonInfo[index];
		
		int iconNumber = dbs.sSpells.GetRecord(selectedTalent.spellID).SpellIconID;
		investSpellIcon = dbs.sSpellIcons.GetRecord(iconNumber).Icon;
		
		if (string.IsNullOrEmpty (investSpellIcon))
		{
			investSpellIcon = "icon.png";
		}
		
		traitsRank = (selectedTalent.rank).ToString() + "/" + (selectedTalent.maxRank).ToString();
		
		if(dbs.sSpells.GetRecord(selectedTalent.spellID).SpellName.Strings[0] != null)
		{
			spellNameInfo = dbs.sSpells.GetRecord(selectedTalent.spellID).SpellName.Strings[0];
		}
		else
		{
			spellNameInfo = "Not Found in database";
		}
		
		if (string.IsNullOrEmpty (spellNameInfo))
		{
			spellNameInfo = "Not added in spellsDBC";
		}
		
		spellDescriptionInfo = getDescription(selectedTalent);
		canLearnTalent = checkLearnTalentConditions(selectedTalent);	
		
		if(selectedTalent.rank != 0)
		{
			descriptionText += spellDescriptionInfo + "\n";
		}
		
		if(selectedTalent.rank != selectedTalent.maxRank)
		{
			descriptionText += "\nNext Lvl\n" + nextRankDescriptionInfo;
		}
	}
	
	private static string getDescription ( TalentButton sender )
	{	
		string desc = "";	
		if(sender.rank == 0)
		{
			desc ="\n";
			nextRankDescriptionInfo = "\n" + dbs.sSpells.GetRecord(sender.spellID).Description.Strings[1];
		}
		else if (sender.rank < sender.maxRank && sender.rank > 0)
		{	
			desc = "\n" + dbs.sSpells.GetRecord(sender.spellID).Description.Strings[1];
			nextRankDescriptionInfo = "\n" + dbs.sSpells.GetRecord(getNextRankSpellID(sender)).Description.Strings[1];
		}	
		else if(sender.rank == sender.maxRank)
		{
			desc = "\n" + dbs.sSpells.GetRecord(sender.spellID).Description.Strings[1];
			nextRankDescriptionInfo = "";
		}
		return desc;
	}
	
	private static string checkLearnTalentConditions ( TalentButton sender )
	{
		string ret = string.Empty;
		
		if(sender.rank == sender.maxRank)
		{
			ret = "You can't invest more\nin this trait";
		}
		else if(unspendTraitsInTab == 0)
		{
			ret = "You do not have\ntrait points left";
		}
		else
		{
			long pointRequired;
			if(activeList == 0)
			{
				pointRequired = sender.column * PET_TALENTS_PER_TEAR;
			}
			else
			{
				pointRequired = sender.column * TALENTS_PER_TEAR;
			}
			
			if(pointRequired > spendTraitsInTab)
			{
				ret = "You need at least " + pointRequired + " points\nin this path to invest\n in this trait.";
			}
		}
		return ret;	
	}
	
	private static int getNextRankSpellID ( TalentButton sender )
	{
		int ret = 0;
		switch (sender.rank)
		{
		case 1:
			ret = dbs.sTalents.GetRecord(sender.talentID).SpellID2;
			break;
		case 2:
			ret = dbs.sTalents.GetRecord(sender.talentID).SpellID3;
			break;
		case 3:
			ret = dbs.sTalents.GetRecord(sender.talentID).SpellID4;
			break;
		case 4:
			ret = dbs.sTalents.GetRecord(sender.talentID).SpellID5;
			break;
		}
		return ret;
	}
}