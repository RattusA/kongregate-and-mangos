﻿
public class TalentButton
{
	public int talentID;
	public int tabID;
	public int rank;
	public uint maxRank;
	public string icon;
	public int spellID;
	public string description;
	public int column;
	public int row;
	
	public TalentButton ()
	{
		talentID = 0;
		tabID = 0;
		rank = 0;
		maxRank = 0;
		icon = "";
		spellID = 0;
		description = "";
		column = 0;
		row = 0;
	}
}

