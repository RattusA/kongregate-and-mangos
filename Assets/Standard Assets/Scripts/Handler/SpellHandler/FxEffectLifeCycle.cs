﻿using UnityEngine;

public class FxEffectLifeCycle:MonoBehaviour
{
	public float lifeTime = 0;
	public float startTime;
	public byte spellCastCount = 0;

	public void DestroyEffect()
	{
		spellCastCount--;
		if(spellCastCount < 1)
			GameObject.DestroyImmediate(gameObject);
	}

	public void SetDelayTime(float time)
	{
		float currentTime = Time.time;
		float destroyTime = 0;
		DestroyThisTimed destroyByTimerScript = gameObject.GetComponentInChildren<DestroyThisTimed>();
		if(destroyByTimerScript)
		{
			destroyTime = destroyByTimerScript.destroyTime;
		}
		time = (time < destroyTime) ? destroyTime : time;
		if((startTime + lifeTime - currentTime) < time)
		{
			lifeTime = currentTime + time - startTime;
		}
	}	

	public void IncrementSpellCastCount()
	{
		spellCastCount++;
	}

	void Awake ()
	{
		startTime = Time.time;
	}

	void Update ()
	{
		var currentTime = Time.time;
		if((startTime + lifeTime - currentTime) <= 0)
		{
			DestroyEffect();
		}
	}
}