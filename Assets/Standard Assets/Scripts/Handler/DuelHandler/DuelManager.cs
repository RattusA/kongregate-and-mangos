using UnityEngine;

public class DuelManager
{
	enum eDuelCompleteType
	{
		DUEL_INTERUPTED = 0,
		DUEL_WON        = 1,
		DUEL_FLED       = 2
	};
	
	public bool  duelRequested;
	public bool  displayTimer;
	public uint secondsToDuel;
	
	UIAbsoluteLayout duelContainer;
	UISprite duelBackground;
	UIButton duelAccept;
	UIButton duelDecline;
	UIButton duelCloseX;
	
	public DuelManager ()
	{
		displayTimer = false;
		duelRequested = false;
	}
	
	public void  DuelRequested ( WorldPacket pkt )
	{
		//Debug.Log("DUEL REQUESTED");
		ulong targetGUID = pkt.ReadReversed64bit();
		ulong myGUID = pkt.ReadReversed64bit();
		if(myGUID == MainPlayer.GUID)
		{
			//Debug.Log("duel requested by me");
		}
		else
		{
			// display duel accept/decline menu
			duelRequested = true;
		}
	}
	
	public void  DuelCountdown ( WorldPacket pkt ,   MainPlayer thePlayer )
	{
		uint ms;
		ms = pkt.ReadReversed32bit();
		//Debug.Log("Duel countdown: " + ms);
		secondsToDuel = ms / 1000;
		displayTimer = true;
		thePlayer.StartCoroutine(thePlayer.DuelTimer());
	}
	
	public void  DuelCompleted ( WorldPacket pkt ,   MainPlayer thePlayer )
	{
		byte interupted = pkt.Read();
		duelRequested = false;

//		string flagDuel = "unknown";
//		dueling = false;
//		switch(interupted)
//		{
//			case eDuelCompleteType.DUEL_INTERUPTED:
//				//if(!duelRequested)
//					//thePlayer.AddChatInput("Your opponent declined the duel.");
//				flagDuel = "Interupted";
//				break;
//			case eDuelCompleteType.DUEL_WON:
//				flagDuel = "Won";
//				break;
//			case eDuelCompleteType.DUEL_FLED:
//				flagDuel = "Fled";
//				break;
//		}
//		Debug.Log("Got SMSG_DUEL_COMPLETED with flag: " + flagDuel);
	}
	
	public void  DuelWinner ( WorldPacket pkt ,   MainPlayer thePlayer )
	{
		byte duelType;
		string opponentName;
		string winnerName;
		duelType = pkt.Read();
		winnerName = pkt.ReadString();
		opponentName = pkt.ReadString();
		if(duelType == 0)
		{
			//Debug.Log(winnerName + " has just defeated " + opponentName + " in a duel!");
			thePlayer.AddChatInput(winnerName + " has just defeated " + opponentName + " in a battle!");
		}
		else
		{
			//Debug.Log(opponentName + " has fled from a duel with " + winnerName + "!");
			thePlayer.AddChatInput(opponentName + " has run away from a battle with " + winnerName + "!");
		}
	}
	
	public void  OutOfBounds ( WorldPacket pkt ,   MainPlayer thePlayer )
	{
		//Debug.Log("You are leaving the duel area and going to lose the duel!");
		thePlayer.AddChatInput("You are leaving the battle area and going to lose the duel!");
	}
	
	public void  InBounds ( WorldPacket pkt ,   MainPlayer thePlayer )
	{
		//Debug.Log("Stay within the duel area to not lose the duel.");
		thePlayer.AddChatInput("Stay within the battle area to not lose the duel.");
	}
	
	public void  DisplayDialog ()
	{	
		duelRequested = false;
		
		duelContainer = new UIAbsoluteLayout();
		
		duelBackground = UIPrime31.questMix.addSprite("battle_background.png" , Screen.width*0.2f , Screen.height*0.3f , 5);
		duelBackground.setSize(Screen.width*0.6f , Screen.height*0.5f);
		
		duelAccept = UIButton.create(UIPrime31.questMix , "accept_button.png" , "accept_button.png" , duelBackground.position.x + duelBackground.width*0.225f , -duelBackground.position.y + duelBackground.height*0.42f , 4);
		duelAccept.setSize(duelBackground.width*0.55f , duelBackground.height*0.2f);
		duelAccept.onTouchDown += Accept;
		
		duelDecline = UIButton.create(UIPrime31.questMix , "decline_button.png" , "decline_button.png" , duelBackground.position.x + duelBackground.width*0.225f , -duelBackground.position.y + duelBackground.height*0.7f , 4);
		duelDecline.setSize(duelBackground.width*0.55f , duelBackground.height*0.2f);
		duelDecline.onTouchDown += Cancel;
		
		duelCloseX = UIButton.create( UIPrime31.myToolkit3 , "close_button.png", "close_button.png", 0 , 0 , 0);
		duelCloseX.setSize(duelBackground.height*0.18f , duelBackground.height*0.18f);
		duelCloseX.position = new Vector3( duelBackground.position.x + duelBackground.width - duelCloseX.width/1.5f , duelBackground.position.y + duelCloseX.height/3 , 4);
		duelCloseX.onTouchDown += Cancel;
		
		duelContainer.addChild(duelBackground , duelAccept , duelDecline , duelCloseX);
		
		/*		GUI.Box( new Rect(Screen.width * 0.3f, Screen.height * 0.3f, Screen.width * 0.4f, Screen.height * 0.4f), "Battle Requested");
		if(GUI.Button( new Rect(Screen.width * 0.35f, Screen.height * 0.4f, Screen.width * 0.3f, Screen.height * 0.1f), "Accept"))
		{
			Accept()
		}
		if(GUI.Button( new Rect(Screen.width * 0.35f, Screen.height * 0.55f, Screen.width * 0.3f, Screen.height * 0.1f), "Decline"))
		{
			Cancel();
		}
*/	}
	
	public void  Request ( MainPlayer me ,   Player target )
	{
		if(me != target)
		{
			Spell sp = dbs.sSpells.GetRecord(7266) as Spell;
			me.playerSpellManager.CastSpell(sp,target.guid,Vector3.zero);
		}
	}
	
	void  Accept ( UIButton sender )
	{	
		Debug.Log("Accept");
		WorldPacket temp_pkt = new WorldPacket();
		temp_pkt.SetOpcode(OpCodes.CMSG_DUEL_ACCEPTED);
		temp_pkt.Append(ByteBuffer.Reverse(MainPlayer.GUID));
		RealmSocket.outQueue.Add(temp_pkt);
		duelContainer.removeAllChild(true);
	}
	
	public void  Cancel ( UIButton sender )
	{
		Debug.Log("Cancel");
		WorldPacket temp_pkt2 = new WorldPacket();
		temp_pkt2.SetOpcode(OpCodes.CMSG_DUEL_CANCELLED);
		temp_pkt2.Append(ByteBuffer.Reverse(MainPlayer.GUID));
		RealmSocket.outQueue.Add(temp_pkt2);
		duelContainer.removeAllChild(true);
	}
}