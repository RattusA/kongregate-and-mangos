﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cooldown {

	public float currentTime;
	public float maxTime;
	public Item item;
	public bool enable;
	
	public Cooldown(float currentTime, float maxTime)
	{
		this.currentTime = currentTime;
		this.maxTime = maxTime;
		this.item = null;
		this.enable = true;
	}

	public Cooldown(float currentTime, float maxTime, Item item, bool enable)
	{
		this.currentTime = currentTime;
		this.maxTime = maxTime;
		this.item = item;
		this.enable = enable;
	}
	
	public Cooldown()
	{
		this.currentTime = 0;
		this.maxTime = 0;
		this.item = null;
		this.enable = true;
	}
}

public class CooldownList {

	protected Dictionary<Spell, Cooldown> cooldownList = new Dictionary<Spell, Cooldown>();
	
	public void SetCooldown(Spell spell, Cooldown cooldown)
	{
		cooldownList[spell] = cooldown;
	}
	
	public void RemoveCooldown(Spell spell)
	{
		cooldownList.Remove(spell);
	}
	
	public void ClearCooldownList()
	{
		cooldownList.Clear();
	}
	
	public Cooldown GetCooldown(Spell spell)
	{
		Cooldown ret = new Cooldown();
		cooldownList.TryGetValue(spell, out ret);
		return ret;
	}
	
	public void UpdateCooldowns()
	{
		float time = Time.deltaTime;
		List<Spell> spellsToRemove = new List<Spell>();
		
		foreach(KeyValuePair<Spell, Cooldown> cooldown in cooldownList)
		{
			if(cooldown.Value.enable)
			{
				cooldown.Value.currentTime -= time;
			}
			
			if(cooldown.Value.currentTime <= 0.0f)
			{
				spellsToRemove.Add(cooldown.Key);
			}
		}
		
		foreach(Spell spell in spellsToRemove)
		{
			cooldownList.Remove(spell);
		}
	}
	
	public void EnableCooldownByItem(Item item)
	{
		foreach(KeyValuePair<Spell, Cooldown> cooldown in cooldownList)
		{
			if(cooldown.Value.item != null && cooldown.Value.item == item)
			{
				cooldown.Value.enable = true;
			}
		}
	}
	
	public void EnableCooldownBySpell(Spell spell)
	{
		foreach(KeyValuePair<Spell, Cooldown> cooldown in cooldownList)
		{
			if(spell != null && (cooldown.Key == spell || cooldown.Key.Category == spell.Category))
			{
				cooldown.Value.enable = true;
			}
		}
	}
}

public class CooldownManager
{
	public CooldownList playerCooldownList = new CooldownList();
	public CooldownList petCooldownList = new CooldownList();

	protected Player player;

	public CooldownManager(Player player)
	{
		this.player = player;
	}
	
	public void SetGlobalCooldown(Spell activeSpell)
	{
		Dictionary<uint, Spell> spellBook = player.playerSpellManager.GetSpellList();
		
		if(activeSpell.StartRecoveryTime > 0)
		{
			foreach(KeyValuePair<uint, Spell> spell in spellBook)
			{
				if(spell.Value.StartRecoveryTime == 0)
				{
					continue;
				}
				
				Cooldown cooldown = playerCooldownList.GetCooldown(spell.Value);
				float recoveryTime = activeSpell.StartRecoveryTime / 1000.0f;
				
				if(cooldown == null || cooldown.currentTime < recoveryTime)
				{
					cooldown = new Cooldown(recoveryTime, recoveryTime);
					playerCooldownList.SetCooldown(spell.Value, cooldown);					
				}
			}
		}
	}
	
	public void CreateCooldown(Spell activeSpell, Item item, bool infinityCooldown)
	{
		int category = 0;
		float cooldown = -1;
		float categoryCooldown = -1;
		
		if(item != null)
		{	
			ItemSpell itemSpell = item.GetOnActiveSpell();
			if(itemSpell != null)
			{
				category = itemSpell.category;
				cooldown = itemSpell.cooldown / 1000.0f;
				categoryCooldown = itemSpell.categoryCooldown / 1000.0f;
			}
		}
		
		if(cooldown < 0 && categoryCooldown < 0)
		{
			category = activeSpell.Category;
			cooldown = activeSpell.RecoveryTime / 1000.0f;
			categoryCooldown = activeSpell.CategoryRecoveryTime / 1000.0f;
		}
		
		if(cooldown < 0 && categoryCooldown < 0)
		{	
			return;
		}
		
		if(cooldown > 0)
		{
			SetCooldown(activeSpell, cooldown, item, infinityCooldown);
		}
		else if(categoryCooldown > 0)
		{
			SetCooldown(activeSpell, categoryCooldown, item, infinityCooldown);
		}
		
		if(category > 0 && categoryCooldown > 0)
		{
			foreach(KeyValuePair<uint, Spell> spell in SpellManager.spellList)
			{
				if(spell.Value.Category == category && activeSpell.ID != spell.Value.ID)
				{
					SetCooldown(spell.Value, categoryCooldown, item, infinityCooldown);
				}
			}
		}
	}
	
	private void SetCooldown(Spell spell, float cooldownTime, Item item, bool enable)
	{
		Cooldown cooldown = new Cooldown(cooldownTime, cooldownTime, item, enable);
		playerCooldownList.SetCooldown(spell, cooldown);
	}
	
	public Cooldown GetCooldown(Spell spell)
	{
		Cooldown cooldown;
		
		cooldown = playerCooldownList.GetCooldown(spell);
		if(cooldown == null)
		{
			cooldown = petCooldownList.GetCooldown(spell);
		}
		
		return cooldown;
	}
	
	public void SetCooldownState(Spell spell, bool state)
	{
		Cooldown cooldown = playerCooldownList.GetCooldown(spell);
		if(cooldown != null)
		{
			cooldown.enable = state;
			playerCooldownList.SetCooldown(spell, cooldown);
		}
	}
	
	public void CooldownEvent(Spell spell)
	{
		Cooldown cooldown = GetCooldown(spell);
		if(cooldown != null)
		{
			if(cooldown.item != null)
			{
				playerCooldownList.EnableCooldownByItem(cooldown.item);
			}
			else
			{
				playerCooldownList.EnableCooldownBySpell(spell);
			}
		}
	}
	
	/**
	 *	PETS
	 */
	
	public void SetPetGlobalCooldown(Spell activeSpell)
	{
		Dictionary<uint, Spell> spellBook = player.petSpellManager.GetSpellList();
		
		if(activeSpell.StartRecoveryTime > 0)
		{
			foreach(KeyValuePair<uint, Spell> spell in spellBook)
			{
				if(spell.Value.StartRecoveryTime == 0)
				{
					continue;
				}
				
				Cooldown cooldown = petCooldownList.GetCooldown(spell.Value);
				float recoveryTime = activeSpell.StartRecoveryTime / 1000.0f;
				
				if(cooldown == null || cooldown.currentTime < recoveryTime)
				{
					cooldown = new Cooldown(recoveryTime, recoveryTime);
					petCooldownList.SetCooldown(spell.Value, cooldown);
				}
			}
		}
	}
	
	public void SetPetCategoryCooldown(Spell activeSpell)
	{
		Dictionary<uint, Spell> spellBook = player.playerSpellManager.GetSpellList();
		float cooldownTime;
		
		if(activeSpell.CategoryRecoveryTime > 0)
		{
			foreach(KeyValuePair<uint, Spell> spell in spellBook)
			{
				if(spell.Value.Category == activeSpell.Category)
				{
					cooldownTime = activeSpell.CategoryRecoveryTime / 1000.0f;
					Cooldown categoryCooldown = new Cooldown(cooldownTime, cooldownTime);
					
					petCooldownList.SetCooldown(spell.Value, categoryCooldown);
				}
			}
		}
		
		if(activeSpell.RecoveryTime > 0)
		{
			if(activeSpell.RecoveryTime > activeSpell.StartRecoveryTime)
			{
				cooldownTime = activeSpell.RecoveryTime / 1000.0f;
				Cooldown freeCooldown = new Cooldown(cooldownTime, cooldownTime);
				
				petCooldownList.SetCooldown(activeSpell, freeCooldown);
			}
		}
	}
}
