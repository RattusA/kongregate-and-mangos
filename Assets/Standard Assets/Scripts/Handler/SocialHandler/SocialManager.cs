using UnityEngine;
using System.Collections.Generic;

public enum FriendStatus
{
	FRIEND_STATUS_OFFLINE   = 0,
	FRIEND_STATUS_ONLINE    = 1,
	FRIEND_STATUS_AFK       = 2,
	FRIEND_STATUS_UNK3      = 3,
	FRIEND_STATUS_DND       = 4
};

public enum SocialFlag
{
	SOCIAL_FLAG_ZERO		= 0x00,
	SOCIAL_FLAG_FRIEND      = 0x01,
	SOCIAL_FLAG_IGNORED     = 0x02,
	//paul>>>
	SOCIAL_FLAG_FRIEND_IGNORED = 0x03,
	//<<<paul	
	SOCIAL_FLAG_MUTED       = 0x04,
	SOCIAL_FLAG_RAF         = 0x08
};

public enum FriendsResult
{
	FRIEND_DB_ERROR         = 0x00,
	FRIEND_LIST_FULL        = 0x01,
	FRIEND_ONLINE           = 0x02,
	FRIEND_OFFLINE          = 0x03,
	FRIEND_NOT_FOUND        = 0x04,
	FRIEND_REMOVED          = 0x05,
	FRIEND_ADDED_ONLINE     = 0x06,
	FRIEND_ADDED_OFFLINE    = 0x07,
	FRIEND_ALREADY          = 0x08,
	FRIEND_SELF             = 0x09,
	FRIEND_ENEMY            = 0x0A,
	FRIEND_IGNORE_FULL      = 0x0B,
	FRIEND_IGNORE_SELF      = 0x0C,
	FRIEND_IGNORE_NOT_FOUND = 0x0D,
	FRIEND_IGNORE_ALREADY   = 0x0E,
	FRIEND_IGNORE_ADDED     = 0x0F,
	FRIEND_IGNORE_REMOVED   = 0x10,
	FRIEND_IGNORE_AMBIGUOUS = 0x11,
	FRIEND_MUTE_FULL        = 0x12,
	FRIEND_MUTE_SELF        = 0x13,
	FRIEND_MUTE_NOT_FOUND   = 0x14,
	FRIEND_MUTE_ALREADY     = 0x15,
	FRIEND_MUTE_ADDED       = 0x16,
	FRIEND_MUTE_REMOVED     = 0x17,
	FRIEND_MUTE_AMBIGUOUS   = 0x18,
	FRIEND_UNK7             = 0x19,
	FRIEND_UNKNOWN          = 0x1A
};

public class FriendInfo
{
	public string name;
	public ulong guid;
	public SocialFlag Flags;
	public string Status;
	public uint Area;
	public uint Level;
	public uint Class;
	public string Note;
	public float LastOnline;
	
	public FriendInfo ()
	{
		Status = "Off";
		Flags = SocialFlag.SOCIAL_FLAG_ZERO;
		Area = 0;
		Level = 0;
		Class = 0;
		Note = "";
		name  = "";
		LastOnline = 0;
	}
}

public class SocialManager
{
	public List<FriendInfo> friendList = new List<FriendInfo>();
	public List<FriendInfo> ignoreList = new List<FriendInfo>();
	MainPlayer mainPlayer;
	/*
		max friend limit: 50
		max ignore limit: 50
	*/
	
	public SocialManager ( MainPlayer thePlayer )
	{
		mainPlayer = thePlayer;
	}
	
	public FriendInfo FindFriendByGuid ( ulong guid )
	{
		for(int i= 0; i < friendList.Count; i++)
			if(friendList[i].guid == guid)
				return friendList[i];
		return null;
	}
	
	public FriendInfo FindIgnoreByGuid ( ulong guid )
	{
		for(int i= 0; i < ignoreList.Count; i++)
			if(ignoreList[i].guid == guid)
				return ignoreList[i];
		return null;
	}
	
	public void  HandleContactList ( WorldPacket pkt )
	{
		friendList.Clear();
		ignoreList.Clear();
		uint numFriends;
		pkt.ReadReversed32bit(); // is always 7 so it just doesn't matter :D
		numFriends = pkt.ReadReversed32bit();
		//Debug.Log("SMSG_CONTACT_LIST with numFriends: " + numFriends);
		FriendInfo tmpInfo;
		while(numFriends > 0)
		{
			tmpInfo = new FriendInfo();
			tmpInfo.guid = pkt.ReadReversed64bit();
			tmpInfo.Flags = (SocialFlag)pkt.ReadReversed32bit();
			tmpInfo.Note = pkt.ReadString();
			if((tmpInfo.Flags & SocialFlag.SOCIAL_FLAG_FRIEND) > 0)
			{
				FriendStatus tmpStatus = (FriendStatus)pkt.Read();
				switch(tmpStatus)
				{
				case FriendStatus.FRIEND_STATUS_ONLINE:
					tmpInfo.Status = "On";
					break;
				case FriendStatus.FRIEND_STATUS_AFK:
					tmpInfo.Status = "Afk";
					break;
				case FriendStatus.FRIEND_STATUS_DND:
					tmpInfo.Status = "DND";
					break;
				default:
					tmpInfo.Status = "Off";
					break;
				}
				if(tmpStatus != FriendStatus.FRIEND_STATUS_OFFLINE)
				{
					tmpInfo.Area = pkt.ReadReversed32bit();
					tmpInfo.Level = pkt.ReadReversed32bit();
					tmpInfo.Class = pkt.ReadReversed32bit();
				}
			}
			if(tmpInfo.Flags == SocialFlag.SOCIAL_FLAG_FRIEND)
			{
				friendList.Add(tmpInfo);
			}
			else if(tmpInfo.Flags == SocialFlag.SOCIAL_FLAG_IGNORED || tmpInfo.Flags == SocialFlag.SOCIAL_FLAG_FRIEND_IGNORED)
			{
				ignoreList.Add(tmpInfo);
			}
			mainPlayer.session.SendQueryPlayerName(tmpInfo.guid);
			numFriends--;
		}
	}
	
	public void  HandleFriendStatus ( WorldPacket pkt )
	{
		ulong friendGuid;
		FriendInfo tmpInfo;
		FriendStatus tmpStatus = FriendStatus.FRIEND_STATUS_OFFLINE;
		FriendsResult result = (FriendsResult)pkt.Read();
		Debug.Log("GOT FRIEND STATUS! with result: "+ result);
		friendGuid = pkt.ReadReversed64bit();
		switch(result)
		{
		case FriendsResult.FRIEND_ONLINE:
			tmpInfo = FindFriendByGuid(friendGuid);
			tmpStatus = (FriendStatus)pkt.Read();
			switch(tmpStatus)
			{
			case FriendStatus.FRIEND_STATUS_ONLINE:
				tmpInfo.Status = "On";
				break;
			case FriendStatus.FRIEND_STATUS_AFK:
				tmpInfo.Status = "Afk";
				break;
			case FriendStatus.FRIEND_STATUS_DND:
				tmpInfo.Status = "DND";
				break;
			default:
				tmpInfo.Status = "Off";
				break;
			}
			
			tmpInfo.Area = pkt.ReadReversed32bit();
			tmpInfo.Level = pkt.ReadReversed32bit();
			tmpInfo.Class = pkt.ReadReversed32bit();
			break;
		case FriendsResult.FRIEND_OFFLINE:
			tmpInfo = FindFriendByGuid(friendGuid);
			tmpInfo.Status = "Off";
			break;
		case FriendsResult.FRIEND_REMOVED:
			tmpInfo = FindFriendByGuid(friendGuid);
			friendList.Remove(tmpInfo);
			break;
		case FriendsResult.FRIEND_ADDED_ONLINE:
			tmpInfo = new FriendInfo();
			tmpInfo.guid = friendGuid;
			tmpInfo.Note = pkt.ReadString();
			tmpStatus = (FriendStatus)pkt.Read();
			switch(tmpStatus)
			{
			case FriendStatus.FRIEND_STATUS_ONLINE:
				tmpInfo.Status = "On";
				break;
			case FriendStatus.FRIEND_STATUS_AFK:
				tmpInfo.Status = "Afk";
				break;
			case FriendStatus.FRIEND_STATUS_DND:
				tmpInfo.Status = "DND";
				break;
			default:
				tmpInfo.Status = "Off";
				break;
			}
			
			tmpInfo.Area = pkt.ReadReversed32bit();
			tmpInfo.Level = pkt.ReadReversed32bit();
			tmpInfo.Class = pkt.ReadReversed32bit();
			friendList.Add(tmpInfo);
			mainPlayer.session.SendQueryPlayerName(tmpInfo.guid);
			break;
		case FriendsResult.FRIEND_ADDED_OFFLINE:
			tmpInfo = new FriendInfo();
			tmpInfo.guid = friendGuid;
			tmpInfo.Status = "Off";
			tmpInfo.Note = pkt.ReadString();
			friendList.Add(tmpInfo);
			mainPlayer.session.SendQueryPlayerName(tmpInfo.guid);
			break;
		case FriendsResult.FRIEND_IGNORE_ADDED:
			tmpInfo = new FriendInfo();
			tmpInfo.guid = friendGuid;
			ignoreList.Add(tmpInfo);
			mainPlayer.session.SendQueryPlayerName(tmpInfo.guid);
			break;
		case FriendsResult.FRIEND_IGNORE_REMOVED:
			tmpInfo = FindIgnoreByGuid(friendGuid);
			ignoreList.Remove(tmpInfo);
			mainPlayer.session.SendQueryPlayerName(tmpInfo.guid);
			break;
		}
		//mainPlayer.gSocialUI.RefreshFriendsOrIgnoreList();
		/*
		tmpInfo = FindFriendByGuid(friendGuid);
		if(tmpInfo)
		{
			if(result == FriendsResult.FRIEND_OFFLINE)
			{
				tmpInfo.Status = "Off";
			}
			else
			{
				if(result == FriendsResult.FRIEND_ADDED_ONLINE || result == FriendsResult.FRIEND_ADDED_OFFLINE)
					tmpInfo.Note = pkt.ReadString();
				if(result == FriendsResult.FRIEND_ADDED_ONLINE || result == FriendsResult.FRIEND_ONLINE)
				{
					FIXME_VAR_TYPE tmpStatus= pkt.Read();
					switch(tmpStatus)
					{
					case FriendStatus.FRIEND_STATUS_ONLINE:
						tmpInfo.Status = "On";
						break;
					case FriendStatus.FRIEND_STATUS_AFK:
						tmpInfo.Status = "Afk";
						break;
					case FriendStatus.FRIEND_STATUS_DND:
						tmpInfo.Status = "DND";
						break;
					default:
						tmpInfo.Status = "Off";
					}
	
					tmpInfo.Area = pkt.ReadReversed32bit();
					tmpInfo.Level = pkt.ReadReversed32bit();
					tmpInfo.Class = pkt.ReadReversed32bit();
					Debug.Log("WorldSession: SMSG_FRIEND_STATUS "+tmpStatus+" friendGuid = "+friendGuid);
				}
			}
		}
		else
		{
			tmpInfo = new FriendInfo();
			if(result == FriendsResult.FRIEND_ADDED_ONLINE || result == FriendsResult.FRIEND_ADDED_OFFLINE)
				tmpInfo.Note = pkt.ReadString();
			if(result == FriendsResult.FRIEND_ADDED_ONLINE || result == FriendsResult.FRIEND_ONLINE)
			{
				FIXME_VAR_TYPE tmpStatus2= pkt.Read();
				switch(tmpStatus2)
				{
				case FriendStatus.FRIEND_STATUS_ONLINE:
					tmpInfo.Status = "On";
					break;
				case FriendStatus.FRIEND_STATUS_AFK:
					tmpInfo.Status = "Afk";
					break;
				case FriendStatus.FRIEND_STATUS_DND:
					tmpInfo.Status = "DND";
					break;
				default:
					tmpInfo.Status = "Off";
				}

				tmpInfo.Area = pkt.ReadReversed32bit();
				tmpInfo.Level = pkt.ReadReversed32bit();
				tmpInfo.Class = pkt.ReadReversed32bit();
				Debug.Log("WorldSession: SMSG_FRIEND_STATUS adding new "+tmpStatus+" friendGuid = "+friendGuid);
			}
			if(tmpInfo.Flags == SocialFlag.SOCIAL_FLAG_FRIEND)
			{
				Debug.Log("Adding friend with GUID: " + tmpInfo.guid);
				friendList.Add(tmpInfo);
			}
			else if(tmpInfo.Flags == SocialFlag.SOCIAL_FLAG_IGNORED)
			{
				Debug.Log("Adding ignore with GUID: " + tmpInfo.guid);
				ignoreList.Add(tmpInfo);
			}
			mainPlayer.session.SendQueryPlayerName(tmpInfo.guid);
		}*/
	}
	public void  SendContactList ()
	{
		Debug.Log("SENDING CONTACT LIST");
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_CONTACT_LIST);
		uint howIAm = 1337;
		pkt.Append(ByteBuffer.Reverse(howIAm));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendAddFriend ( string friendName ,   string friendNote )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_ADD_FRIEND);
		pkt.Append(friendName);
		pkt.Append(friendNote);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendDeleteFriend ( ulong friendGuid )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_DEL_FRIEND);
		pkt.Append(ByteBuffer.Reverse(friendGuid));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendAddIgnore ( string ignoreName )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_ADD_IGNORE);
		pkt.Append(ignoreName);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendDeleteIgnore ( ulong ignoreGuid )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_DEL_IGNORE);
		pkt.Append(ByteBuffer.Reverse(ignoreGuid));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendSetContactNotes ( ulong contactGuid ,   string note )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_SET_CONTACT_NOTES);
		pkt.Append(ByteBuffer.Reverse(contactGuid));
		pkt.Append(note);
		RealmSocket.outQueue.Add(pkt);
	}
}