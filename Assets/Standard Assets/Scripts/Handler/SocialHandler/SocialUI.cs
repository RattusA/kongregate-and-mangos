using UnityEngine;
using System.Collections.Generic;

public class SocialUI
{
	private MainPlayer mainPlayer;
	byte btState;
	public bool  isHidden = true;
	
	//uiClass////////////////
	public UIPrime31 uiClass;
	private UIAbsoluteLayout mainContainer;
	private UIAbsoluteLayout popupContainer;
	private UIScrollableVerticalLayout scrollableNames;
	private UISprite mainBackground;
	private UISprite decorationTexture;
	private UISprite namesCase;
	private UISprite noteCase;
	private UISprite upArrow;
	private UISprite downArrow;
	private UIButton addButton;
	private UIButton deleteButton;
	private UIButton friendsButton;
	private UIButton ignoreButton;
	private UIButton whisperButton;
	private UIButton noteButton;
	private UIButton inviteButton;
	private UISprite selectionSprite;
	
	UIText text1;
//	UIText text2;
//	UIText text3;
	
	private UITextInstance nameTextInstance; 
	private UITextInstance lvlTextInstance;
	private UITextInstance statusTextInstance;
	
	private UITextInstance noteTextInstance;
	private bool  noteIsClean = true;
	private bool  ignoreState = false;
	
	private List<UISprite> classIconsArray;
	private List<UITextInstance> friendsNamesArray;
	private List<UITextInstance> popupTextsArray;
	private List<UITextInstance> lvlTextsArray;
	private List<UITextInstance> statusTextsArray;
	
//	private List<FriendInfo> friendsFromManager;
//	private List<FriendInfo> ignoreFromManager;
	
	private FriendInfo selectedPlayer;

	////////////////////
	
	public int menuState;
	
	private string TextField = "";
	private string CharName = "";
	private MessageWnd messageWnd = new MessageWnd();
	private bool  WindowActive = false;
	private Vector2 friendScrollVector;
	private Vector2 ignoreScrollVector;
	private FriendInfo selected;
	
	public SocialUI ( MainPlayer thePlayer )
	{
		uiClass = UIPrime31.instance;
		menuState = 0;
		mainPlayer = thePlayer;
		btState = 0;
		selected = null;

		text1 = new UIText (UIPrime31.myToolkit1, "fontiny dark","fontiny dark.png");
//		text2 = new UIText (UIPrime31.myToolkit1, "fontiny title","fontiny title.png");
//		text3 = new UIText (UIPrime31.myToolkit1, "fontiny simple","fontiny simple.png");
		
		mainContainer = new UIAbsoluteLayout();
		popupContainer = new UIAbsoluteLayout();
		classIconsArray = new List<UISprite>();		
		friendsNamesArray = new List<UITextInstance>();
		popupTextsArray = new List<UITextInstance>();
		lvlTextsArray = new List<UITextInstance>();
		statusTextsArray = new List<UITextInstance>();
	}
	
	public void  ShowSocial ()
	{
		isHidden = false;
		RefreshFriendNote();
		if(selectedPlayer == null)
		{
			selectionSprite.hidden = true;
		}

		switch(menuState)
		{
		case 0: //main social menu
			if(mainContainer.hidden)
			{
				mainContainer.hidden = false;
				nameTextInstance.hidden = false; 
				lvlTextInstance.hidden = false;
				statusTextInstance.hidden = false;
			}
			if((!ignoreState && mainPlayer.SocialMgr.friendList.Count > 0) || (ignoreState && mainPlayer.SocialMgr.ignoreList.Count > 0))
				RefreshFriendsOrIgnoreList();
			break;
			
		case -1://scrollable items
			if((!ignoreState && scrollableNames._children.Count != mainPlayer.SocialMgr.friendList.Count) || (ignoreState && scrollableNames._children.Count != mainPlayer.SocialMgr.ignoreList.Count))
			{
				ForceRefresh();
				return;
			}
			
			scrollableNames.hidden = false;
			
			byte i = 0;
			foreach(UISprite scrollableButton in scrollableNames._children)
			{
				Vector3 tmp = (classIconsArray[i] as UISprite).position;
				tmp.y = scrollableButton.position.y;
				(classIconsArray[i] as UISprite).position = tmp;
				(classIconsArray[i] as UISprite).hidden = scrollableButton.hidden;
				tmp = (friendsNamesArray[i] as UITextInstance).position;
				tmp.y = scrollableButton.position.y - scrollableButton.height*0.6f;
				(friendsNamesArray[i] as UITextInstance).position = tmp;
				(friendsNamesArray[i] as UITextInstance).hidden = scrollableButton.hidden;
				tmp = (lvlTextsArray[i] as UITextInstance).position;
				tmp.y = scrollableButton.position.y - scrollableButton.height*0.6f;
				(lvlTextsArray[i] as UITextInstance).position = tmp;
				(lvlTextsArray[i] as UITextInstance).hidden = scrollableButton.hidden;
				tmp = (statusTextsArray[i] as UITextInstance).position;
				tmp.y = scrollableButton.position.y - scrollableButton.height*0.6f;
				(statusTextsArray[i] as UITextInstance).position = tmp;
				(statusTextsArray[i] as UITextInstance).hidden = scrollableButton.hidden;
				
				if(selectedPlayer != null && (ignoreState)? selectedPlayer == mainPlayer.SocialMgr.ignoreList[i] : selectedPlayer == mainPlayer.SocialMgr.friendList[i])
				{
					selectionSprite.hidden = scrollableButton.hidden;
					tmp = selectionSprite.position;
					tmp.y = scrollableButton.position.y;
					selectionSprite.position = tmp;
				}
				i++;
			}
			break;
			
		case -2://draw addPlayer textField
			TextField = GUI.TextField( new Rect(Screen.width * 0.3f , Screen.height * 0.4f , Screen.width * 0.4f  , Screen.height * 0.1f ), TextField , 30 , mainPlayer.guildskin.textField);
			break;
			
		case -3://draw set note textfield
			TextField = GUI.TextField( new Rect(Screen.width * 0.225f , Screen.height * 0.42f , Screen.width * 0.552f  , Screen.height * 0.31f ), TextField  , 50 , mainPlayer.guildskin.textField);
			break;
		}
		/*		GUI.DrawTexture( new Rect(0, Screen.height*0.05f, Screen.width, Screen.height*0.9f), mainPlayer.Background);
		GUI.Label( new Rect(Screen.width*0.3f,Screen.height*0.1f,Screen.width*0.4f,Screen.height*0.1f), "Social List", mainPlayer.newskin.customStyles[4]);
		if(!btState)
			FriendWindow();
		else
			IgnoreWindow();
*/
	}
	
	public void  DrawSocialWindow ()
	{
		//TEXTURES//
		mainBackground = UIPrime31.myToolkit3.addSprite("basic_background.png" , 0 , Screen.height * 0.08f , 5);
		mainBackground.setSize(Screen.width * 1.002f , Screen.height * 0.92f);
		
		decorationTexture = UIPrime31.myToolkit3.addSprite( "decoration_texture2.png", 0 , Screen.height * 0.112f , 4);
		decorationTexture.setSize(Screen.width ,Screen.height * 0.03f );
		
		namesCase = UIPrime31.questMix.addSprite("background_case.png" , Screen.width * 0.01f , Screen.height * 0.23f , 4);
		namesCase.setSize(Screen.width * 0.6f , Screen.height * 0.76f);
		
		upArrow = UIPrime31.interfMix.addSprite("double_arrows_up.png" , namesCase.position.x + namesCase.width * 0.92f , -namesCase.position.y + namesCase.height * 0.05f , 1);
		upArrow.setSize(namesCase.width * 0.06f , namesCase.height * 0.07f);
		
		downArrow = UIPrime31.interfMix.addSprite("double_arrows_down.png" , namesCase.position.x + namesCase.width * 0.92f , -namesCase.position.y + namesCase.height * 0.88f , 1);
		downArrow.setSize(namesCase.width * 0.06f , namesCase.height * 0.07f);
		
		noteCase = UIPrime31.interfMix.addSprite("note_pannel.png" , Screen.width * 0.62f , Screen.height * 0.61f , 4);
		noteCase.setSize(Screen.width * 0.37f , Screen.height * 0.37f);
		
		addButton = UIButton.create(UIPrime31.interfMix , "add_button.png" , "add_button.png" , Screen.width * 0.03f , Screen.height * 0.14f , 4);
		addButton.setSize(Screen.width*0.2f , Screen.height*0.08f);
		addButton.onTouchUpInside += AddFriendOrIgnoreDelegate;
		
		deleteButton = UIButton.create(UIPrime31.interfMix , "delete_button.png" , "delete_button.png" , Screen.width * 0.77f , Screen.height * 0.14f , 4);
		deleteButton.setSize(addButton.width , addButton.height);
		deleteButton.onTouchUpInside += DeleteFriendOrIgnoredDelegate;
		
		friendsButton = UIButton.create(UIPrime31.interfMix , "friends_button.png" , "friends_button.png" , Screen.width * 0.26f , Screen.height * 0.13f , 4);
		friendsButton.setSize(Screen.width*0.22f , Screen.height*0.09f);
		friendsButton.onTouchUpInside += ShowIgnoreOrFriendsList;
		
		ignoreButton = UIButton.create(UIPrime31.interfMix , "ignore_button.png" , "ignore_button.png" , Screen.width * 0.52f , Screen.height * 0.13f , 4);
		ignoreButton.setSize(Screen.width*0.22f , Screen.height*0.09f);
		ignoreButton.onTouchUpInside += ShowIgnoreOrFriendsList;
		
		whisperButton = UIButton.create(UIPrime31.interfMix , "whisper_button.png" , "whisper_button.png" , Screen.width * 0.65f , Screen.height * 0.25f , 4);
		whisperButton.setSize(Screen.width*0.33f , Screen.height*0.09f);
		whisperButton.onTouchUpInside += NoteOrWhisperDelegate;
		
		noteButton = UIButton.create(UIPrime31.interfMix , "note_button.png" , "note_button.png" , Screen.width * 0.65f , Screen.height * 0.37f , 4);
		noteButton.setSize(Screen.width*0.33f , Screen.height*0.09f);
		noteButton.onTouchUpInside += NoteOrWhisperDelegate;
		
		inviteButton = UIButton.create(UIPrime31.interfMix , "invite_button.png" , "invite_button.png" , Screen.width * 0.65f , Screen.height * 0.49f , 4);
		inviteButton.setSize(Screen.width*0.33f , Screen.height*0.09f);
		inviteButton.onTouchUpInside += InviteToPartyDelegate;
		
		selectionSprite = UIPrime31.interfMix.addSprite("selected_mail_frame.png" , Screen.width * 0.02f , 0 , 3);
		selectionSprite.setSize(namesCase.width*0.97f , namesCase.height*0.08f);
		
		mainContainer.addChild(mainBackground , decorationTexture , namesCase , upArrow , downArrow , noteCase , addButton , deleteButton , friendsButton , ignoreButton , whisperButton , noteButton , inviteButton , selectionSprite);
		mainContainer.hidden = true;
		
		nameTextInstance = text1.addTextInstance("Name" , namesCase.position.x + namesCase.width * 0.3f , -namesCase.position.y + namesCase.height * 0.07f , UID.TEXT_SIZE*0.7f , 2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		lvlTextInstance = text1.addTextInstance("Lvl." , namesCase.position.x + namesCase.width * 0.7f , -namesCase.position.y + namesCase.height * 0.07f , UID.TEXT_SIZE*0.7f , 2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		statusTextInstance = text1.addTextInstance("Status" , namesCase.position.x + namesCase.width * 0.83f , -namesCase.position.y + namesCase.height * 0.07f , UID.TEXT_SIZE*0.7f , 2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		nameTextInstance.hidden = true; 
		lvlTextInstance.hidden = true;
		statusTextInstance.hidden = true;
	}
	
	void  ShowIgnoreOrFriendsList ( UIButton sender )
	{
		
		if(sender == friendsButton && ignoreState)
		{
			Debug.Log("WE HAVE A LIST OF" + mainPlayer.SocialMgr.friendList.Count + "FRIENDS");
			ClearScrollable();
			ignoreState = false;
			selectedPlayer = null;
			menuState = 0;
		}
		else if(sender == ignoreButton && !ignoreState)
		{
			Debug.Log("WE HAVE A LIST OF" + mainPlayer.SocialMgr.ignoreList.Count + "IGNORED");
			ClearScrollable();
			ignoreState = true;
			selectedPlayer = null;
			menuState = 0;
		}
	}
	
	void  ClearScrollable ()
	{
		//scrollableNames.hidden = true;
		
		if(scrollableNames != null)
		{
			scrollableNames.removeAllChild(true);
			scrollableNames.setSize(0 , 0);
			scrollableNames = null;
		}
		foreach(UISprite child in classIconsArray)
		{
			child.destroy();
			child.manager.removeElement(child);
		}
		foreach(UITextInstance name in friendsNamesArray)
		{
			name.clear();
		}
		foreach(UITextInstance lvl in lvlTextsArray)
		{
			lvl.clear();
		}
		foreach(UITextInstance status in statusTextsArray)
		{
			status.clear();
		}
		lvlTextsArray.Clear();
		statusTextsArray.Clear();
		classIconsArray.Clear();
		friendsNamesArray.Clear();
	}
	
	void  RefreshFriendsOrIgnoreList ()
	{
		
		selectedPlayer = null;
		foreach(var friend in (ignoreState)? mainPlayer.SocialMgr.ignoreList : mainPlayer.SocialMgr.friendList)
		{
			if(string.IsNullOrEmpty(friend.name))
			{
				menuState = 0;
				mainPlayer.SocialMgr.SendContactList();
				return;
			}
		}
		
		Debug.Log("refresh NAMES");
		
		if(mainPlayer.menuSelected == 101)
		{
			menuState = -1;
		}
		
		
		UITextInstance tempTextInstance;
		
		ClearScrollable();
		
		scrollableNames = new UIScrollableVerticalLayout(Screen.height * 0.02f);
		scrollableNames.position = new Vector3( Screen.width * 0.02f , -Screen.height * 0.23f - namesCase.height * 0.17f , 0 );
		scrollableNames.setSize(namesCase.width , namesCase.height * 0.7f);
		scrollableNames.marginModify = true;
		string iconName;
		
		if(mainPlayer.menuSelected != 101)
		{
			scrollableNames.hidden = true;
		}
		else
		{
			scrollableNames.hidden = false;
		}
		
		foreach(var friend in (ignoreState)? mainPlayer.SocialMgr.ignoreList : mainPlayer.SocialMgr.friendList)
		{
			switch ((Classes)friend.Class)
			{
			case Classes.CLASS_WARRIOR:
				iconName = "0-fighter.png";
				break;
			case Classes.CLASS_PALADIN:
				iconName = "4-templar.png";
				break;
			case Classes.CLASS_MAGE:
				iconName = "2-mage.png";
				break;
			case Classes.CLASS_ROGUE:
				iconName = "3-rogue.png";
				break;
			case Classes.CLASS_PRIEST:
				iconName = "1-confessor.png";
				break;
			case Classes.CLASS_WARLOCK:
				iconName = "7-necromancer.png";
				break;
			case Classes.CLASS_HUNTER:
				iconName = "8-ranger.png";
				break;
			default:
				iconName = "classMark_off.png";
				break;
			}
			
			tempTextInstance = mainPlayer.interf.text2.addTextInstance(friend.name , namesCase.width * 0.11f , 0 , UID.TEXT_SIZE*0.8f , 1 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Middle);
			friendsNamesArray.Add(tempTextInstance);
			
			tempTextInstance = mainPlayer.interf.text2.addTextInstance(friend.Level.ToString() , namesCase.position.x + namesCase.width * 0.7f , 0 , UID.TEXT_SIZE*0.8f , 1 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
			lvlTextsArray.Add(tempTextInstance);
			
			tempTextInstance = mainPlayer.interf.text2.addTextInstance(friend.Status , namesCase.position.x + namesCase.width * 0.83f , 0 , UID.TEXT_SIZE*0.8f , 1 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
			statusTextsArray.Add(tempTextInstance);
			
			UISprite classIcon = UIPrime31.myToolkit3.addSprite(iconName , scrollableNames.position.x + Screen.width * 0.01f , 0 , 2);
			classIcon.setSize(namesCase.height * 0.08f , namesCase.height * 0.08f);
			classIconsArray.Add(classIcon);
			
			UIButton invisibleButton = UIButton.create(UIPrime31.interfMix , "black10px.png" , "black10px.png" , 0 , 0 , 9);
			invisibleButton.setSize(namesCase.width*0.97f , namesCase.height*0.08f);
			invisibleButton.onTouchDown += SelectPlayer;
			
			scrollableNames.addChild(invisibleButton);
		}
	}
	
	void  RefreshFriendNote ()
	{
		if(selectedPlayer == null && noteIsClean)
			return;
		else if(selectedPlayer != null && !noteIsClean)
			return;
		else if(selectedPlayer == null && noteTextInstance != null)
		{
			noteTextInstance.clear();
			noteTextInstance = null;
			noteIsClean = true;
			return;
		}
		else
		{
			string tempText;
			string tempText2;
			int tempNum;
			
			tempText = "";
			tempText2 = selectedPlayer.Note;
			
			while(tempText2.Length > 0)
			{
				if(tempText2.Length > 20 && (tempText2.IndexOf("\n" , 0) > 22 || tempText2.IndexOf("\n" , 0) == -1))
				{
					tempNum = tempText2.IndexOf(" " , 18);
					if(tempNum != -1)
					{	
						tempText += tempText2.Substring(0 , tempNum) + "\n";
						tempText2 = tempText2.Substring(tempNum+1 , tempText2.Length-tempNum-1);
					}
					else
					{
						tempText = tempText + tempText2;
						tempText2 = "";
					}
				}
				else
				{
					tempText = tempText + tempText2;
					tempText2 = "";
				}
			}
			noteTextInstance = mainPlayer.interf.text1.addTextInstance(tempText, Screen.width * 0.63f , Screen.height * 0.63f , UID.TEXT_SIZE*0.6f , 0 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
			noteIsClean = false;
		}
	}
	
	void  NoteOrWhisperDelegate ( UIButton sender )
	{
		if(selectedPlayer == null)
			return;
		
		bool  whisperCase = true;
		if(sender == noteButton)
			whisperCase = false;
		
		
		menuState = -3;
		DisableMainWindow(true);
		
		UISprite popupBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.25f , 0);
		popupBackground.setSize(Screen.width*0.6f , Screen.height*0.6f);
		
		UISprite popupTextBox = UIPrime31.questMix.addSprite("big_textbox.png" , popupBackground.position.x + popupBackground.width*0.03f , -popupBackground.position.y + popupBackground.height*0.26f , -1);
		popupTextBox.setSize(popupBackground.width*0.94f , popupBackground.height*0.56f);
		
		UISprite decoration = UIPrime31.interfMix.addSprite((whisperCase)? "whisper_decoration.png" : "note_decoration.png" , popupBackground.position.x + popupBackground.width*0.05f , -popupBackground.position.y + popupBackground.height*0.04f , -1);
		decoration.setSize(popupBackground.width*0.9f , popupBackground.height*0.21f);
		
		UIButton okPopupButton = UIButton.create(UIPrime31.myToolkit3 , "button_OK.png" , "button_OK.png" , popupBackground.position.x + popupBackground.width*0.1f , -popupBackground.position.y + popupBackground.height*0.84f , -1);
		okPopupButton.setSize(popupBackground.width * 0.35f , popupBackground.height * 0.12f);
		if(whisperCase)
			okPopupButton.onTouchUpInside += ConfirmSendWhisper;
		else
			okPopupButton.onTouchUpInside += ConfirmSetNote;
		
		UIButton cancelPopupButton = UIButton.create(UIPrime31.myToolkit5 , "close.png" , "close.png" , popupBackground.position.x + popupBackground.width*0.6f , -popupBackground.position.y + popupBackground.height*0.84f , -1);
		cancelPopupButton.setSize(popupBackground.width * 0.35f , popupBackground.height * 0.12f); 
		cancelPopupButton.onTouchUpInside += ClosePopupDelegate;
		
		UIButton closePopup = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closePopup.setSize(popupBackground.width*0.08f , popupBackground.width*0.08f);
		closePopup.position = new Vector3( popupBackground.position.x + popupBackground.width - closePopup.width/1.5f , popupBackground.position.y + closePopup.height/5 , -1);
		closePopup.onTouchUpInside += ClosePopupDelegate;
		
		UITextInstance tempTextInstance;
		
		if(whisperCase)
		{
			tempTextInstance = mainPlayer.interf.text2.addTextInstance(selectedPlayer.name , popupBackground.position.x + popupBackground.width*0.21f , -popupBackground.position.y + popupBackground.height * 0.17f , UID.TEXT_SIZE*0.95f , -2 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Middle);
			popupTextsArray.Add(tempTextInstance);
			
			tempTextInstance = mainPlayer.interf.text3.addTextInstance("WHISPER" , popupBackground.position.x + popupBackground.width*0.21f , -popupBackground.position.y + popupBackground.height * 0.08f , UID.TEXT_SIZE*0.7f , -2 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Middle);
			popupTextsArray.Add(tempTextInstance);
		}
		else
		{
			tempTextInstance = mainPlayer.interf.text2.addTextInstance(selectedPlayer.name , popupBackground.position.x + popupBackground.width*0.19f , -popupBackground.position.y + popupBackground.height * 0.09f , UID.TEXT_SIZE*0.95f , -2 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Middle);
			popupTextsArray.Add(tempTextInstance);
			
			tempTextInstance = mainPlayer.interf.text3.addTextInstance("NOTE" , popupBackground.position.x + popupBackground.width*0.19f , -popupBackground.position.y + popupBackground.height * 0.18f , UID.TEXT_SIZE*0.7f , -2 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Middle);
			popupTextsArray.Add(tempTextInstance);
		}
		popupContainer.addChild(popupBackground , okPopupButton , cancelPopupButton , closePopup , popupTextBox , decoration);
	}
	
	void  ConfirmSendWhisper ( UIButton sender )
	{
		mainPlayer.SendMessage2("/w " + selectedPlayer.name + " " + TextField);
		ClosePopupDelegate(sender);
	}
	
	void  ConfirmSetNote ( UIButton sender )
	{
		mainPlayer.SocialMgr.SendSetContactNotes(selectedPlayer.guid, TextField);
		selectedPlayer.Note = TextField;
		ClosePopupDelegate(sender);
	}
	
	void  InviteToPartyDelegate ( UIButton sender )
	{
		if(selectedPlayer == null)
			return;
		menuState = -10;
		DisableMainWindow(true);
		
		UISprite popupBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.25f , 0);
		popupBackground.setSize(Screen.width*0.6f , Screen.height*0.4f);
		
		UIButton okPopupButton = UIButton.create(UIPrime31.myToolkit3 , "button_OK.png" , "button_OK.png" , popupBackground.position.x + popupBackground.width*0.1f , -popupBackground.position.y + popupBackground.height*0.7f , -1);
		okPopupButton.setSize(popupBackground.width * 0.35f , popupBackground.height * 0.2f);
		okPopupButton.onTouchUpInside += ConfirmInviteToParty;
		
		UIButton cancelPopupButton = UIButton.create(UIPrime31.myToolkit5 , "close.png" , "close.png" , popupBackground.position.x + popupBackground.width*0.6f , -popupBackground.position.y + popupBackground.height*0.7f , -1);
		cancelPopupButton.setSize(popupBackground.width * 0.35f , popupBackground.height * 0.2f); 
		cancelPopupButton.onTouchUpInside += ClosePopupDelegate;
		
		UIButton closePopup = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closePopup.setSize(popupBackground.width*0.08f , popupBackground.width*0.08f);
		closePopup.position = new Vector3( popupBackground.position.x + popupBackground.width - closePopup.width/1.5f , popupBackground.position.y + closePopup.height/5 , -1);
		closePopup.onTouchUpInside += ClosePopupDelegate;
		
		UITextInstance tempTextInstance = mainPlayer.interf.text3.addTextInstance("Do you want to invite\n" + selectedPlayer.name + "\nto group?" , popupBackground.position.x + popupBackground.width/2 , -popupBackground.position.y + popupBackground.height * 0.4f , UID.TEXT_SIZE*0.9f , -2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		popupTextsArray.Add(tempTextInstance);
		
		popupContainer.addChild(popupBackground , okPopupButton , cancelPopupButton , closePopup);
	}
	
	void  ConfirmInviteToParty ( UIButton sender )
	{
		if (!(mainPlayer.HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_IN_COMBAT)))
		{
			mainPlayer.SendMessage2("/invite "+selectedPlayer.name);
		}
		ClosePopupDelegate(sender);
	}
	
	void  DeleteFriendOrIgnoredDelegate ( UIButton sender )
	{
		if(selectedPlayer == null)
			return;
		menuState = -10;
		DisableMainWindow(true);
		
		UISprite popupBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.25f , 0);
		popupBackground.setSize(Screen.width*0.6f , Screen.height*0.4f);
		
		UIButton okPopupButton = UIButton.create(UIPrime31.myToolkit3 , "button_OK.png" , "button_OK.png" , popupBackground.position.x + popupBackground.width*0.1f , -popupBackground.position.y + popupBackground.height*0.7f , -1);
		okPopupButton.setSize(popupBackground.width * 0.35f , popupBackground.height * 0.2f);
		okPopupButton.onTouchUpInside += ConfirmDeleteDelegate;
		
		UIButton cancelPopupButton = UIButton.create(UIPrime31.myToolkit5 , "close.png" , "close.png" , popupBackground.position.x + popupBackground.width*0.6f , -popupBackground.position.y + popupBackground.height*0.7f , -1);
		cancelPopupButton.setSize(popupBackground.width * 0.35f , popupBackground.height * 0.2f); 
		cancelPopupButton.onTouchUpInside += ClosePopupDelegate;
		
		UIButton closePopup = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closePopup.setSize(popupBackground.width*0.08f , popupBackground.width*0.08f);
		closePopup.position = new Vector3( popupBackground.position.x + popupBackground.width - closePopup.width/1.5f , popupBackground.position.y + closePopup.height/5 , -1);
		closePopup.onTouchUpInside += ClosePopupDelegate;
		
		UITextInstance tempTextInstance = mainPlayer.interf.text3.addTextInstance("Are you sure you want\nto delete\n" + selectedPlayer.name + "?" , popupBackground.position.x + popupBackground.width/2 , -popupBackground.position.y + popupBackground.height * 0.4f , UID.TEXT_SIZE*0.9f , -2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		popupTextsArray.Add(tempTextInstance);
		
		popupContainer.addChild(popupBackground , okPopupButton , cancelPopupButton , closePopup);
	}
	
	void  SelectPlayer ( UIButton sender )
	{
		byte i = 0;
		foreach(UIButton child in scrollableNames._children)
		{
			if(child == sender)
			{
				if(ignoreState)
					selectedPlayer = mainPlayer.SocialMgr.ignoreList[i];
				else
					selectedPlayer = mainPlayer.SocialMgr.friendList[i];
				if(noteTextInstance != null)
				{
					noteTextInstance.clear();
					noteTextInstance = null;
					noteIsClean = true;
				}
			}
			i++;
		}
	}
	
	void  ConfirmDeleteDelegate ( UIButton sender )
	{
		if(ignoreState)
			mainPlayer.SocialMgr.SendDeleteIgnore(selectedPlayer.guid);
		else
			mainPlayer.SocialMgr.SendDeleteFriend(selectedPlayer.guid);
		
		ClosePopupDelegate(sender);
	}
	
	void  AddFriendOrIgnoreDelegate ( UIButton sender )
	{
		menuState = -2;
		DisableMainWindow(true);
		
		UISprite popupBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.25f , 0);
		popupBackground.setSize(Screen.width*0.6f , Screen.height*0.4f);
		
		UIButton okPopupButton = UIButton.create(UIPrime31.myToolkit3 , "button_OK.png" , "button_OK.png" , popupBackground.position.x + popupBackground.width*0.1f , -popupBackground.position.y + popupBackground.height*0.7f , -1);
		okPopupButton.setSize(popupBackground.width * 0.35f , popupBackground.height * 0.2f);
		okPopupButton.onTouchUpInside += ConfirmAddFriend;
		
		UIButton cancelPopupButton = UIButton.create(UIPrime31.myToolkit5 , "close.png" , "close.png" , popupBackground.position.x + popupBackground.width*0.6f , -popupBackground.position.y + popupBackground.height*0.7f , -1);
		cancelPopupButton.setSize(popupBackground.width * 0.35f , popupBackground.height * 0.2f); 
		cancelPopupButton.onTouchUpInside += ClosePopupDelegate;
		
		UIButton closePopup = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closePopup.setSize(popupBackground.width*0.08f , popupBackground.width*0.08f);
		closePopup.position = new Vector3( popupBackground.position.x + popupBackground.width - closePopup.width/1.5f , popupBackground.position.y + closePopup.height/5 , -1);
		closePopup.onTouchUpInside += ClosePopupDelegate;
		
		UITextInstance tempTextInstance;
		if(ignoreState)
		{
			tempTextInstance = mainPlayer.interf.text3.addTextInstance("Type the name you want to Ignore" , popupBackground.position.x + popupBackground.width/2 , -popupBackground.position.y + popupBackground.height * 0.2f , UID.TEXT_SIZE*0.9f , -2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
			popupTextsArray.Add(tempTextInstance);
		}
		else
		{
			tempTextInstance = mainPlayer.interf.text3.addTextInstance("Type the name of your Friend" , popupBackground.position.x + popupBackground.width/2 , -popupBackground.position.y + popupBackground.height * 0.2f , UID.TEXT_SIZE*0.9f , -2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
			popupTextsArray.Add(tempTextInstance);
		}
		popupContainer.addChild(popupBackground , okPopupButton , cancelPopupButton , closePopup);
	}
	
	void  ConfirmAddFriend ( UIButton sender )
	{
		if(ignoreState)
		{
			foreach(FriendInfo possibleFriend in mainPlayer.SocialMgr.friendList)
			{
				if(possibleFriend.name.ToLower() == TextField.ToLower())
				{
					Debug.Log("FRIEND OF YOURS");
					mainPlayer.SocialMgr.SendDeleteFriend(possibleFriend.guid);
				}
			}
			mainPlayer.SocialMgr.SendAddIgnore(TextField);
		}
		else
		{
			foreach(FriendInfo possibleIgnored in mainPlayer.SocialMgr.ignoreList)
			{
				if(possibleIgnored.name.ToLower() == TextField.ToLower())
				{
					Debug.Log("IGNORED OF YOURS");
					mainPlayer.SocialMgr.SendDeleteIgnore(possibleIgnored.guid);
				}
			}
			mainPlayer.SocialMgr.SendAddFriend(TextField, "");
		}
		selectedPlayer = null;
		ClosePopupDelegate(sender);
	}
	
	void  ClosePopupDelegate ( UIButton sender )
	{
		DisableMainWindow(false);
		menuState = 0;
		DestroyPopupWindow();
		if(TextField == null)
			TextField = "";
	}
	
	void  DestroyPopupWindow ()
	{
		popupContainer.removeAllChild(true);
		foreach(UITextInstance anyText in popupTextsArray)
		{
			anyText.clear();
		}
		popupTextsArray.Clear();
	}
	
	void  DisableMainWindow ( bool val )
	{
		foreach(UISprite anySprite in mainContainer._children)
		{
			if(anySprite is UIButton)
			{
				(anySprite as UIButton).disabled = val;
			}
		}
		if(scrollableNames != null)
		{
			foreach(UIButton anyButton in scrollableNames._children)
			{
				anyButton.disabled = val;
			}
			scrollableNames.scrollStop = val;
		}
	}
	
	void  ForceRefresh ()
	{
		ClearScrollable();
		selectedPlayer = null;
		if(noteTextInstance != null)
		{
			noteTextInstance.clear();
			noteTextInstance = null;
			noteIsClean = true;
		}
		menuState = 0;
	}
	
	public void  HideSocialUI ()
	{
		isHidden = true;
		ignoreState = false;
		menuState = 0;
		mainContainer.hidden = true;
		nameTextInstance.hidden = true; 
		lvlTextInstance.hidden = true;
		statusTextInstance.hidden = true;
		ClearScrollable();
		selectedPlayer = null;
		if(noteTextInstance != null)
		{
			noteTextInstance.clear();
			noteTextInstance = null;
			noteIsClean = true;
		}
		
		//scrollableNames.hidden = true;
	}
}