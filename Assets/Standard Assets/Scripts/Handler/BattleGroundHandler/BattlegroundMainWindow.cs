﻿using UnityEngine;
using System.Collections;

public class BattlegroundMainWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private GUIStyle blackButtonTextStyle;
	private GUIStyle greenButtonTextStyle;
	private GUIStyle greenLabelTextStyle;
	
	private Texture2D selectTexture;
	
	private string joinText = "Enlist";
	private string joinGroupText = "Premade";
	
	
	public BattlegroundMainWindow ()
	{
		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		
		blackButtonTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, Screen.height * 0.05f, 
		                                               Color.black, true, FontStyle.Bold);
		greenButtonTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, Screen.height * 0.065f, 
		                                               Color.green, true, FontStyle.Bold);
		greenLabelTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, Screen.height * 0.065f, 
		                                              new Color(0.0f, 80.0f / 255.0f, 0.0f, 1.0f), true, FontStyle.Bold);
		selectTexture = Resources.Load<Texture2D>("GUI/InGame/select_green");
	}
	
	public void  DrawWindow ( TexturePacker atlas ,   Texture2D texture )
	{
		GUI.DrawTexture(new Rect(0.005f * sw, 0.15f * sh, 0.8f * sw, 0.82f * sh), texture);
		
		//		// PVP Button
		//		atlas.drawTexture(new Rect(0.81f * sw, 0.19f * sh, 0.185f * sw, 0.09f * sh), "3_selected.png");
		//		GUI.Button(new Rect(0.83f * sw, 0.19f * sh, 0.145f * sw, 0.09f * sh), "PvP", blackButtonTextStyle);
		
		//		// Battles Button
		//		atlas.drawTexture(new Rect(0.81f * sw, 0.35f * sh, 0.185f * sw, 0.09f * sh), "3_selected.png");
		//		GUI.Button(new Rect(0.83f * sw, 0.35f * sh, 0.145f * sw, 0.09f * sh), "Batlles", blackButtonTextStyle);
		
		// JOIN Button
		if(joinGroupText != "Leave")
		{
			atlas.DrawTexture(new Rect(0.81f * sw, 0.19f * sh, 0.185f * sw, 0.09f * sh), "3_selected.png");
			if(GUI.Button(new Rect(0.83f * sw, 0.19f * sh, 0.145f * sw, 0.09f * sh), joinText, blackButtonTextStyle))
			{
				if(joinText == "Leave")
				{
					joinText = "Enlist";
					WorldSession.player.battlegroundManager.LeaveBattleField();
				}
				else
				{
					joinText = "Leave";
					WorldSession.player.battlegroundManager.AddToWaitList(0, 2, 0, 0);
				}
			}
		}
		
		// JOIN GROUP Button
		if(WorldSession.player.group.getPlayersCount() == 0 && joinText != "Leave")
		{
			
			atlas.DrawTexture(new Rect(0.81f * sw, 0.35f * sh, 0.185f * sw, 0.09f * sh), "3_selected.png");
			GUI.Button(new Rect(0.83f * sw, 0.35f * sh, 0.145f * sw, 0.09f * sh), joinGroupText, blackButtonTextStyle);
			{
				if(joinGroupText == "Leave")
				{
					joinGroupText = "Premade";
					WorldSession.player.battlegroundManager.LeaveBattleField();
				}
				else
				{
					joinGroupText = "Leave";
					WorldSession.player.battlegroundManager.AddToWaitList(0, 2, 0, 1);
				}
			}
		}
		
		// Quit Button
		atlas.DrawTexture(new Rect(0.81f * sw, 0.89f * sh, 0.185f * sw, 0.09f * sh), "3_selected.png");
		if(GUI.Button(new Rect(0.83f * sw, 0.89f * sh, 0.145f * sw, 0.09f * sh), "Quit", blackButtonTextStyle))
		{
			RaidWindowManager.closeRaidWindow();		
			WorldSession.player.interf.backMode(null);
		}
		
		atlas.DrawTexture(new Rect(0.03f * sw, 0.18f * sh, 0.75f * sw, 0.12f * sh), "wooden_button.png");
		GUI.Label(new Rect(0.03f * sw, 0.175f * sh, 0.75f * sw, 0.1f * sh), "Battles", greenButtonTextStyle);
		
		atlas.DrawTexture(new Rect(0.74f * sw, 0.28f * sh, 0.05f * sw, 0.05f * sw), "ScrollUp.png");
		atlas.DrawTexture(new Rect(0.74f * sw, 0.52f * sh, 0.05f * sw, 0.05f * sw), "ScrollDown.png");
		
		GUI.DrawTexture(new Rect(0.05f * sw, 0.28f * sh, 0.7f * sw, 0.1f * sh), selectTexture);
		GUI.Label(new Rect(0.05f * sw, 0.28f * sh, 0.7f * sw, 0.1f * sh), "Castle Battleground 10 vs 10", greenLabelTextStyle);
		//		GUI.Button(new Rect(0.03f * sw, 0.28f * sh, 0.7f * sw, 0.31f * sh), "");
		
		
		atlas.DrawTexture(new Rect(0.03f * sw, 0.61f * sh, 0.75f * sw, 0.12f * sh), "wooden_button.png");
	}
}