using UnityEngine;

public class BattlegroundManager
{
	protected MainPlayer mainPlayer;
	private BattlegroundMainWindow battlegroundMainWindow;
	
	public TexturePacker battlegroundAtlas;
	public Texture2D infoDecoration;
	
	public bool  isActive = false;
	public uint windowState = 0;
	
	public BattlegroundManager ( MainPlayer mainPlayer )
	{
		this.mainPlayer = mainPlayer;
		
		battlegroundMainWindow = new BattlegroundMainWindow();
	}
	
	public void  DrawBattlegroundWindow ()
	{
		switch(windowState)
		{
		case 0:
			//				battlegroundMainWindow.DrawWindow(battlegroundAtlas, infoDecoration);
			break;
		case 1:
			break;
		}
	}
	
	public void  AddToWaitList ( System.UInt64 guid ,   uint bgType ,   uint bgID ,   byte isGroup )
	{
		//		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_BATTLEMASTER_JOIN, 17);
		//		pkt.Add(guid);
		//		pkt.Add(bgType);
		//		pkt.Add(bgID);
		//		pkt.Append(isGroup);
		//		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  LeaveBattleField ()
	{
		//		byte unk1 = 0;
		//		System.UInt16 unk2 = 0;
		//		uint unk3 = 0;
		//		
		//		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_BATTLEMASTER_JOIN, 8);
		//		pkt.Append(unk1);
		//		pkt.Append(unk1);
		//		pkt.Add(unk3);
		//		pkt.Add(unk2);
		//		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  BattleFieldStatus ( WorldPacket pkt )
	{
		uint queueSlot = pkt.ReadReversed32bit();
		
		System.UInt64 battlegroundID = pkt.ReadReversed64bit();
		pkt.Read();		// Const value = 0
		pkt.Read();		// Const value = 0
		uint clientInstanceID = pkt.ReadReversed32bit();
		byte isRaited = pkt.Read();
		uint status = pkt.ReadReversed32bit();
		switch(status)
		{	
		case 1:
			uint waitTime = pkt.ReadReversed32bit();
			uint queueTime = pkt.ReadReversed32bit();
			break;
		case 2:
			uint mapID = pkt.ReadReversed32bit();
			pkt.Read(0);		// Const value = 0
			uint timeToJoin = pkt.ReadReversed32bit();
			break;
			//			case 3:
			//				uint mapID = pkt.ReadReversed32bit();
			//				pkt.Read(0);		// Const value = 0
			//				break;
		}
	}
	
	public void  JoinToBattleField ()
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_BATTLEMASTER_JOIN, 17);
		RealmSocket.outQueue.Add(pkt);
	}
}