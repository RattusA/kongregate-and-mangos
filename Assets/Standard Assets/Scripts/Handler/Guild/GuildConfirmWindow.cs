﻿using UnityEngine;

public class GuildConfirmWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Rect windowDecorationRect;
	private Rect textLabelRect;
	
	private Rect yesButtonRect;
	private Rect noButtonRect;
	
	private GUIStyle testStyle = new GUIStyle();
	private GuildUI guild;
	
	public GuildConfirmWindow ()
	{
		windowDecorationRect = new Rect(sw * 0.21875f, sh * 0.33333f, sw * 0.5625f, sh * 0.33333f);
		textLabelRect = new Rect(sw * 0.23f, sh * 0.35f, sw * 0.54f, sh * 0.15f);
		yesButtonRect = new Rect(sw * 0.28125f, sh * 0.54167f, sw * 0.1875f, sh * 0.06667f);
		noButtonRect = new Rect(sw * 0.53125f, sh * 0.54167f, sw * 0.1875f, sh * 0.06667f);

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		testStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.05f, Color.white, true, FontStyle.Normal);
	}
	
	public void  Show ()
	{
		if(guild == null)
		{
			guild = WorldSession.player.guildUI;
			return;
		}
		
		GUI.DrawTexture(windowDecorationRect, guild.mainDecoration);
		
		switch(guild.confirmWindowPage)
		{
		case GuildConfirmState.KICK_PLAYER:
			GUI.Label(textLabelRect, "You realy want to kick " + guild.member.Name + " from guild?", testStyle);
			break;
		case GuildConfirmState.LEAVE_GUILD:
			GUI.Label(textLabelRect, "You realy want to leave guild?", testStyle);
			break;
		case GuildConfirmState.REMOVE_RANK:
			GUI.Label(textLabelRect, "You realy want to remove lowest rank?", testStyle);
			break;
		case GuildConfirmState.CHANGE_GUILD_LD:
			GUI.Label(textLabelRect, "You realy want change Guild Leadet", testStyle);
			break;
		}
		
		guild.guildAtlasToolkit.DrawTexture(yesButtonRect, "YesButton.png");
		if(GUI.Button(yesButtonRect, "", GUIStyle.none))
			Yes();
		
		guild.guildAtlasToolkit.DrawTexture(noButtonRect, "NoButton.png");
		if(GUI.Button(noButtonRect, "", GUIStyle.none))
			No();
	}
	
	private void  Yes ()
	{
		switch(guild.confirmWindowPage)
		{
		case GuildConfirmState.KICK_PLAYER:
			WorldSession.player.guildmgr.RemoveMember(guild.member.Name);
			guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
			break;
		case GuildConfirmState.LEAVE_GUILD:
			WorldSession.player.guildmgr.LeaveGuild();
			guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
			break;
		case GuildConfirmState.REMOVE_RANK:
			WorldSession.player.guildmgr.DeleteLowestRank();
			guild.ChangeGuildWindowState(GuildWindowState.MANAGE_WINDOW);
			break;
		case GuildConfirmState.CHANGE_GUILD_LD:
			guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
			break;
		}		
	}
	
	private void  No ()
	{
		switch(guild.confirmWindowPage)
		{
		case GuildConfirmState.KICK_PLAYER:
			guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
			break;
		case GuildConfirmState.LEAVE_GUILD:
			guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
			break;
		case GuildConfirmState.REMOVE_RANK:
			guild.ChangeGuildWindowState(GuildWindowState.MANAGE_WINDOW);
			break;
		case GuildConfirmState.CHANGE_GUILD_LD:
			guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
			break;
		}		
	}
}