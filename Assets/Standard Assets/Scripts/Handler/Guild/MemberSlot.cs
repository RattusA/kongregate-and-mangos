﻿
public class MemberSlot
{
	public ulong guid;
	uint accountId;
	public string Name;
	public uint RankId;
	public byte Level;
	public Classes Class;
	public uint ZoneId;
	//uint LogoutTime;
	public float LogoutTime;
	public string Pnote;
	public string OFFnote;
	uint BankResetTimeMoney;
	uint BankRemMoney;
	uint[] BankResetTimeTab = new uint[ SharedDefines.GUILD_BANK_MAX_TABS ];
	uint[] BankRemSlotsTab = new uint[ SharedDefines.GUILD_BANK_MAX_TABS ];
	public byte online;
	private string strLogoutTime= null;

	public string GetLogoutTime ()
	{
		if(strLogoutTime == null)
		{
			if(LogoutTime == 0)
			{
				strLogoutTime = "Online";
			}
			else
			{
				uint secs = (uint)(LogoutTime * TimeConstants.DAY);
				strLogoutTime = "" + Global.GetFormatTimeAsString(secs);
			}
		}
		return strLogoutTime;	
	}
}