using UnityEngine;
using System;
using System.Collections.Generic;

public enum ClickEvent{ BANK, INVENTORY, CONTAINER };

public class GuildVault
{
	//GUILD VAULT UI
	private uint i;
	private UISprite invBackground;
	private UISprite vaultBackground;
	private UIButton anyButton;
	private UISprite buttonsBackground;
	private UISprite windowBackground;
	private UISprite windowDecoration;
	private UISprite decorCase;
	private UIButton windowButton;
	private UISprite[] logDecorations;
	//	private UIButton prevButton;
	//	private UIButton nextButton;
	private UITextInstance guildVaultText;
	private UITextInstance UIalert;
	private UITextInstance[] UITexts;
	private UITextInstance[] logText;
	private NonUIElements.TextBox tabInfoText;
	private UIAbsoluteLayout alertContainer;
	private UIAbsoluteLayout popupContainer;
	private UIScrollableVerticalLayout logScrollable;
	private bool  displayMoneyField;
	private bool  showLog;
	private bool  emptyField;
	/////////////////////////////////////
	
	private MainPlayer thePlayer;
	private Item selectedItem;
	private UIButton itemSelected;
	private Vector3 tempItmPos;
	private string moneyField;
	
	bool  hideMinimap;
	public bool  active = false;
	public bool  showVault;
	bool  needRefresh;
	_GameObject vault;
	UIScrollableVerticalLayout tabGrid;
	bool  showInfoTab;
	private Vector2 _logScrollVector2;
	
	bool  _drag;
	bool  goDrag;
	Item dragItem;
	string image;
	UIAbsoluteLayout inventoryContainer;
	UIAbsoluteLayout tabsContainer;
	UIAbsoluteLayout tabsIconContainer;
	UIButton[] tabs = new UIButton[6];
	UIButton[] slots = new UIButton[20];
	UIAbsoluteLayout slotsContainer;
	
	ClickEvent sourceDrag;
	//ClickEvent destDrag;
	
	UIButton dragItemIm;
	UIScrollableVerticalLayout b_grid;
	bool  workingRefreshI;
	public bool  refreshI;
	bool  refreshTab;
	
	/**************************************************/
	UIButton[] bagButton;
	UISprite inventoryBagBackGround;
	float inventoryBagSize;
	UIButton[] bagSlot; //inventory slots
	
	public bool  startDrag = false;
	
	float dragX;
	float dragY;
	Vector2 touchPosition;
	
	BaseObject targetvault ;
	/**************************************************/
	
	public bool  WindowActive = false;
	private MessageWnd messageWnd = new MessageWnd();
	
	public GuildVault ( MainPlayer plr, BaseObject targetVault)
	{
		if(!plr.IsInGuild())
			return;
		
		hideMinimap = true;
		displayMoneyField = false;
		showLog = false;
		thePlayer = plr;
		thePlayer.blockTargetChange = true;
		WorldSession.interfaceManager.hideInterface = true;
		thePlayer.interf.stateManager();
		workingRefreshI = false;
		refreshI = false;
		inventoryContainer = new UIAbsoluteLayout();
		dragItem = null;
		selectedItem = null;
		itemSelected = null;
		goDrag = false;
		showVault = false;
		_drag = false;
		b_grid = null;
		plr.guildmgr.ActivateGuildBanker(targetVault.guid);
		plr.guildmgr.GuildRoster();
		plr.guild.selectedTab = 0;
		plr.guildmgr.QueryGuildTab( targetVault.guid, plr.guild.selectedTab );
		plr.guild.initTabs();
		tabsContainer = new UIAbsoluteLayout();
		tabsIconContainer = new UIAbsoluteLayout();
		slotsContainer = new UIAbsoluteLayout();
		alertContainer = new UIAbsoluteLayout();
		popupContainer = new UIAbsoluteLayout();
		
		UITexts = new UITextInstance[4];
		targetvault = targetVault;
	}
	
	public void DisplayVault()   // OnGUI 
	{
		if ( WindowActive )
		{
			if( displayMoneyField )
			{
				moneyField = GUI.TextField( new Rect(windowBackground.position.x + windowBackground.width*0.06f ,
				                                     -windowBackground.position.y + windowBackground.height*0.55f ,
				                                     windowBackground.width*0.88f ,
				                                     windowBackground.height*0.12f),
				                           moneyField,
				                           thePlayer.guildskin.textField);
			}
			
			if( showLog )
			{
				if(logDecorations != null && logText != null)
				{
					for(i = 0 ; i < thePlayer.guild.bankEvents.Count ; i++)
					{
						if((logText[i] != null) && (logDecorations[i] != null))
						{
							logText[i].hidden = ((-Screen.height*0.75f < logText[i].position.y) && (logText[i].position.y < -Screen.height*0.3f))? false : true;
							logText[i].position = new Vector3(logText[i].position.x,
							                                  logDecorations[i].position.y - Screen.height*0.045f,
							                                  logText[i].position.z);
						}
					}
				}
			}
			
			if ( showInfoTab )
				thePlayer.guild.tabText = GUI.TextField( new Rect(decorCase.position.x + decorCase.width*0.012f , -decorCase.position.y + decorCase.height*0.02f , decorCase.width*0.976f , decorCase.height*0.96f),thePlayer.guild.tabText, 350 ,thePlayer.guildskin.textField);
		}
	}
	
	private void  initWindow ()
	{
		ClearAll();
		WindowActive = true;
		thePlayer.meniuOpend = true;
		showInfoTab = false;
	}
	
	private void  initShow ()
	{
		WindowActive = false;
		showInfoTab = false;
	}
	
	private void  ShowCurrentTabNumber ()
	{
		if(guildVaultText != null)
		{
			guildVaultText.clear();
			guildVaultText = null;
		}
		
		if(thePlayer.guild != null && thePlayer.guild.purchasedTabs > 0)
			guildVaultText = thePlayer.interf.text1.addTextInstance("Tab "+ (thePlayer.guild.selectedTab+1).ToString(),
			                                                        Screen.width*0.48f,
			                                                        Screen.height*0.3f,
			                                                        UID.TEXT_SIZE*0.9f,
			                                                        -2,
			                                                        Color.white,
			                                                        UITextAlignMode.Center,
			                                                        UITextVerticalAlignMode.Top);
	}
	
	
	public void  ShowAll ()
	{
		if(!thePlayer.IsInGuild())
			return;
		
		ClearAll();
		Debug.Log("Show ALL!");
		showVault = true;
		ShowInvSlots();
		if(thePlayer.guild.purchasedTabs > 0)
		{
			ShowGuildBank();
		}
		thePlayer.meniuOpend = true;
		active = true;
		thePlayer.canShowMenu = false;
		
		if(hideMinimap)
		{
			if(thePlayer.ShowMinimap)
				thePlayer.ShowMinimap = false;
			if(thePlayer.compassMiniMap)
				thePlayer.compassMiniMap.SetActive(false);
		}
	}
	
	public void  ClearAll ()
	{
		Debug.Log("Clear ALL!");
		showVault = false;
		ClearInventory();
		if(thePlayer.guild.purchasedTabs > 0)
		{
			showVault = false;
			ClearInventory();
			if(thePlayer.guild.purchasedTabs > 0)
			{
				ClearTabs();
			}
			
			ClearTabs();
			ClearTabSlots();
			
		}
		thePlayer.meniuOpend = false;
		active = false;
		thePlayer.canShowMenu = true;
		if(!hideMinimap)
		{
			UIButton tempButton = null;
			thePlayer.interf.backMode(tempButton);
			thePlayer.blockTargetChange = false;
			WorldSession.interfaceManager.hideInterface = false;
			thePlayer.interf.stateManager();
			
			if(thePlayer.compassMiniMap)
				thePlayer.compassMiniMap.SetActive(true);
			
			if(guildVaultText != null)
			{
				guildVaultText.clear();
				guildVaultText = null;
			}
		}
	}
	
	public void  UpdateAll ()
	{
		
		if(refreshI && !workingRefreshI && !_drag)
		{
			Debug.Log("Doing inventory refresh in vault!");
			ClearInventory();
			if(thePlayer.guild.purchasedTabs > 0)
			{
				ClearTabs();
				ClearTabSlots();
				ShowGuildBank();
			}
			ShowInvSlots();
		}
		
		if(refreshTab)
		{
			RefreshTabSlots();
		}
		
		if(displayMoneyField)
		{
			RefreshGoldAmounts();
		}
	}
	
	private void  DestroyPopup ()
	{
		popupContainer.removeAllChild(true);
		if(tabInfoText != null) tabInfoText.destroy();
		
		for(int i= 0 ; i < 4 ; i++)
		{
			if(UITexts[i] != null)
			{
				UITexts[i].clear();
				UITexts[i] = null;
			}
		}
	}
	
	private void  DestroyLog ()
	{
		popupContainer.removeAllChild(true);
		
		
		if(logScrollable != null)
		{
			logScrollable.removeAllChild(true);
			logScrollable.setSize(0 , 0);
			logScrollable = null;
		}
		
		if(logText != null && logDecorations != null)
		{
			for(int i = thePlayer.guild.bankEvents.Count-1 ; i >= 0 ; i--)
			{
				if(logText[i] != null)
				{
					logText[i].clear();
					logText[i] = null;
				}
			}
			for(int i = thePlayer.guild.bankEvents.Count ; i >= 0 ; i--)
				if(logDecorations[i] != null)
					logDecorations[i] = null;
		}
	}
	
	private void  ShowButtons ()
	{
		/*		if(thePlayer.guild.purchasedTabs > 0)
		{
			buttonsBackground = UIPrime31.interfMix.addSprite("medium_frame.png" , Screen.width*0.05f , Screen.height*0.895f , -8);
			buttonsBackground.setSize(Screen.width*0.3f , Screen.height*0.1f);
			tabsContainer.addChild(buttonsBackground);
			
			if(thePlayer.guild.selectedTab > 0)
			{
				prevButton = UIButton.create(UIPrime31.interfMix , "previous_button.png" , "previous_button.png" , buttonsBackground.position.x + buttonsBackground.width*0.05f , -buttonsBackground.position.y + buttonsBackground.height*0.1f , -9);
				prevButton.setSize(buttonsBackground.width*0.4f , buttonsBackground.height*0.8f);
				prevButton.onTouchUpInside += PrevButtonDelegate;
				tabsContainer.addChild(prevButton);
			}
			if(thePlayer.guild.selectedTab < (thePlayer.guild.purchasedTabs - 1))
			{
				nextButton= UIButton.create(UIPrime31.interfMix , "next_button.png" , "next_button.png" , buttonsBackground.position.x + buttonsBackground.width*0.55f , -buttonsBackground.position.y + buttonsBackground.height*0.1f , -9);
				nextButton.setSize(buttonsBackground.width*0.4f , buttonsBackground.height*0.8f);
				nextButton.onTouchUpInside += NextButtonDelegate;
				tabsContainer.addChild(nextButton);
			}
		}
*/		
		buttonsBackground = UIPrime31.interfMix.addSprite("decoration4.png" , Screen.width*0.4f , Screen.height*0.36f , -1);
		buttonsBackground.setSize(Screen.width*0.16f , Screen.height*0.03f);
		tabsContainer.addChild(buttonsBackground);
		
		buttonsBackground = UIPrime31.interfMix.addSprite("big_frame.png" , Screen.width*0.39f , Screen.height*0.28f , 0);
		buttonsBackground.setSize(Screen.width*0.18f , Screen.height*0.6f);
		tabsContainer.addChild(buttonsBackground);
		
		anyButton = UIButton.create(UIPrime31.interfMix , "tabInfo_button.png" , "tabInfo_button.png" , buttonsBackground.position.x + buttonsBackground.width*0.05f , -buttonsBackground.position.y + buttonsBackground.height*0.2f , -1);
		anyButton.setSize(buttonsBackground.width*0.9f , buttonsBackground.height*0.11f);
		anyButton.onTouchUpInside += TabInfoButtonDelegate;
		tabsContainer.addChild(anyButton);
		
		anyButton = UIButton.create(UIPrime31.interfMix , "log_button.png" , "log_button.png" , buttonsBackground.position.x + buttonsBackground.width*0.05f , -buttonsBackground.position.y + buttonsBackground.height*0.36f , -1);
		anyButton.setSize(buttonsBackground.width*0.9f , buttonsBackground.height*0.11f);
		anyButton.onTouchUpInside += LogButtonDelegate;
		tabsContainer.addChild(anyButton);
		
		anyButton = UIButton.create(UIPrime31.interfMix , "gold_button.png" , "gold_button.png" , buttonsBackground.position.x + buttonsBackground.width*0.05f , -buttonsBackground.position.y + buttonsBackground.height*0.52f , -1);
		anyButton.setSize(buttonsBackground.width*0.9f , buttonsBackground.height*0.11f);
		anyButton.onTouchUpInside += GoldButtonDelegate;
		tabsContainer.addChild(anyButton);
		
		anyButton = UIButton.create(UIPrime31.interfMix , "buyTab_button.png" , "buyTab_button.png" , buttonsBackground.position.x + buttonsBackground.width*0.05f , -buttonsBackground.position.y + buttonsBackground.height*0.68f , -1);
		anyButton.setSize(buttonsBackground.width*0.9f , buttonsBackground.height*0.11f);
		anyButton.onTouchUpInside += BuyTabButtonDelegate;
		tabsContainer.addChild(anyButton);
		
		anyButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , buttonsBackground.position.x + buttonsBackground.width*0.05f , -buttonsBackground.position.y + buttonsBackground.height*0.84f , -1);
		anyButton.setSize(buttonsBackground.width*0.9f , buttonsBackground.height*0.11f);
		anyButton.onTouchUpInside += CloseButtonDelegate;
		tabsContainer.addChild(anyButton);
		
		ShowCurrentTabNumber();
	}
	
	public void  ShowTabInfo ( string tabInfo )
	{
		if(emptyField)
		{
			return;
		}	
		initWindow();
		DestroyPopup();
		
		windowBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.1f , Screen.height*0.17f , -3);
		windowBackground.setSize(Screen.width*0.7f , Screen.height*0.8f);
		
		windowDecoration = UIPrime31.interfMix.addSprite("vendor_decoration.png" , windowBackground.position.x + windowBackground.width*0.025f , -windowBackground.position.y + windowBackground.height*0.05f , -4);
		windowDecoration.setSize(windowBackground.width*0.95f , windowBackground.height*0.1f);
		
		decorCase = UIPrime31.questMix.addSprite("background_case.png" , windowBackground.position.x + windowBackground.width*0.05f , -windowBackground.position.y + windowBackground.height*0.16f , -4);
		decorCase.setSize(windowBackground.width*0.9f , windowBackground.height*0.57f);
		
		windowButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		windowButton.setSize(windowBackground.width*0.08f , windowBackground.width*0.08f);
		windowButton.position = new Vector3( windowBackground.position.x + windowBackground.width - windowButton.width/1.5f , windowBackground.position.y + windowButton.height/5 , -6);
		windowButton.onTouchUpInside += CloseTabInfo;
		popupContainer.addChild(windowButton);
		
		if(tabInfo != "")
		{
			windowButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , windowBackground.position.x + windowBackground.width*0.1f , -windowBackground.position.y + windowBackground.height*0.86f , -4);
			windowButton.setSize(windowBackground.width*0.3f , windowBackground.height*0.1f);
			windowButton.onTouchUpInside += CloseTabInfo;
			popupContainer.addChild(windowButton);
			
			windowButton = UIButton.create(UIPrime31.questMix , "compose_button.png" , "compose_button.png" , windowBackground.position.x + windowBackground.width*0.6f , -windowBackground.position.y + windowBackground.height*0.86f , -4);
			windowButton.setSize(windowBackground.width*0.3f , windowBackground.height*0.1f);
			windowButton.onTouchUpInside += ChangeTabInfoDelegate;
			popupContainer.addChild(windowButton);
			
			UITexts[0] = thePlayer.interf.text3.addTextInstance("Tab Info" , windowBackground.position.x + windowBackground.width * 0.12f , -windowBackground.position.y + windowBackground.height*0.04f , UID.TEXT_SIZE*0.9f , -5 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
			UITexts[1] = thePlayer.interf.text3.addTextInstance("Remaining slots for today: " + GuildManager.getIntAsString( thePlayer.guild.remainingSlotsForToday ) , windowBackground.position.x + windowBackground.width * 0.07f , -windowBackground.position.y + windowBackground.height*0.76f , UID.TEXT_SIZE*0.8f , -4 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
			
			tabInfoText = new NonUIElements.TextBox("" , new Vector3(decorCase.position.x + decorCase.width*0.03f, -decorCase.position.y - decorCase.height*0.05f , 0), new Vector2(0 , 0) , 5 ,Color.white, UID.TEXT_SIZE*0.7f ,"fontiny dark");
			tabInfoText.size = new Vector2(Screen.width * 0.0182f , Screen.height * 0.5f);
			tabInfoText.text = ("\n"+tabInfo);
			tabInfoText.m_textInstances.position = new Vector3(tabInfoText.m_textInstances.position.x,
			                                                   tabInfoText.m_textInstances.position.y,
			                                                   -5);
		}
		else
		{
			showInfoTab = true;
			
			windowButton = UIButton.create(UIPrime31.myToolkit3 , "button_OK.png" , "button_OK.png" , windowBackground.position.x + windowBackground.width*0.35f , -windowBackground.position.y + windowBackground.height*0.86f , -4);
			windowButton.setSize(windowBackground.width*0.3f , windowBackground.height*0.1f);
			windowButton.onTouchUpInside += AcceptTabInfo;
			popupContainer.addChild(windowButton);
			
			decorCase.setSize(windowBackground.width*0.9f , windowBackground.height*0.68f);
			
			UITexts[0] = thePlayer.interf.text3.addTextInstance("Change Tab Info" , windowBackground.position.x + windowBackground.width * 0.12f , -windowBackground.position.y + windowBackground.height*0.04f , UID.TEXT_SIZE*0.9f , -4 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		}
		
		popupContainer.addChild(windowBackground , windowDecoration , decorCase);
	}
	
	public void  ShowTabLog ()
	{
		DestroyLog();
		
		windowBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.1f , Screen.height*0.17f , -3);
		windowBackground.setSize(Screen.width*0.7f , Screen.height*0.8f);
		
		windowDecoration = UIPrime31.interfMix.addSprite("vendor_decoration.png" , windowBackground.position.x + windowBackground.width*0.025f , -windowBackground.position.y + windowBackground.height*0.05f , -4);
		windowDecoration.setSize(windowBackground.width*0.95f , windowBackground.height*0.1f);
		
		decorCase = UIPrime31.questMix.addSprite("background_case.png" , windowBackground.position.x + windowBackground.width*0.05f , -windowBackground.position.y + windowBackground.height*0.16f , -4);
		decorCase.setSize(windowBackground.width*0.9f , windowBackground.height*0.68f);
		
		windowButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , windowBackground.position.x + windowBackground.width*0.35f , -windowBackground.position.y + windowBackground.height*0.86f , -4);
		windowButton.setSize(windowBackground.width*0.3f , windowBackground.height*0.1f);
		windowButton.onTouchUpInside += CloseTabLog;
		popupContainer.addChild(windowButton);
		
		windowButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		windowButton.setSize(windowBackground.width*0.07f , windowBackground.width*0.07f);
		windowButton.position = new Vector3( windowBackground.position.x + windowBackground.width - windowButton.width/1.5f , windowBackground.position.y + windowButton.height/5 , -4);
		windowButton.onTouchUpInside += CloseTabLog;
		
		popupContainer.addChild(windowBackground , windowDecoration , decorCase , windowButton);
		
		if(logText != null)
			logText = null;
		if(logDecorations != null)
			logDecorations = null;
		
		if(thePlayer.guild.bankEvents.Count > 0)
		{
			logScrollable = new UIScrollableVerticalLayout( (int)(Screen.height*0.085f) );
			logScrollable.setSize(Screen.width*0.63f, Screen.height*0.46f);
			logScrollable.position = new Vector3( Screen.width*0.135f , -Screen.height*0.335f , 0 );
			logScrollable.marginModify = true;
			logScrollable.edgeInsets = new UIEdgeInsets(0, (int)(Screen.width*0.02f), 0, 0);
			
			logText = new UITextInstance[thePlayer.guild.bankEvents.Count];
			logDecorations = new UISprite[thePlayer.guild.bankEvents.Count + 1];
			
			logDecorations[thePlayer.guild.bankEvents.Count] = UIPrime31.interfMix.addSprite("decoration2.png" , 0 , 0 , -5);
			logDecorations[thePlayer.guild.bankEvents.Count].setSize(decorCase.width*0.9f , decorCase.height*0.09f);
		}
		
		for(int i= thePlayer.guild.bankEvents.Count-1 ; i >= 0 ; i--)
		{
			logText[i] = thePlayer.interf.text1.addTextInstance(thePlayer.guild.bankEvents[i].text , decorCase.position.x + decorCase.width*0.05f , 0 , UID.TEXT_SIZE*0.7f , -5 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
			logDecorations[i] = UIPrime31.interfMix.addSprite("decoration2.png" , 0 , 0 , -5);
			logDecorations[i].setSize(decorCase.width*0.9f , decorCase.height*0.09f);
			logScrollable.addChild(logDecorations[i]);
		}
		
		if(thePlayer.guild.bankEvents.Count > 0)
		{
			logScrollable.addChild(logDecorations[thePlayer.guild.bankEvents.Count]);		
			showLog = true;
		}
	}
	
	private void  ShowMoneyWindow ()
	{
		DestroyPopup();
		
		windowBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.1f , Screen.height*0.25f , -3);
		windowBackground.setSize(Screen.width*0.7f , Screen.height*0.7f);
		
		windowDecoration = UIPrime31.interfMix.addSprite("vendor_decoration.png" , windowBackground.position.x + windowBackground.width*0.025f , -windowBackground.position.y + windowBackground.height*0.05f , -4);
		windowDecoration.setSize(windowBackground.width*0.95f , windowBackground.height*0.1f);
		
		decorCase = UIPrime31.questMix.addSprite("big_textbox.png" , windowBackground.position.x + windowBackground.width*0.05f , -windowBackground.position.y + windowBackground.height*0.17f , -4);
		decorCase.setSize(windowBackground.width*0.9f , windowBackground.height*0.35f);
		popupContainer.addChild(decorCase);
		
		decorCase = UIPrime31.questMix.addSprite("long_text_case.png" , windowBackground.position.x + windowBackground.width*0.05f , -windowBackground.position.y + windowBackground.height*0.53f , -4);
		decorCase.setSize(windowBackground.width*0.9f , windowBackground.height*0.15f);
		popupContainer.addChild(decorCase);
		
		windowButton = UIButton.create(UIPrime31.interfMix , "deposit_button.png" , "deposit_button.png" , windowBackground.position.x + windowBackground.width*0.05f , -windowBackground.position.y + windowBackground.height*0.7f , -4);
		windowButton.setSize(windowBackground.width*0.4f , windowBackground.height*0.11f);
		windowButton.onTouchUpInside += DepositMoney;
		popupContainer.addChild(windowButton);
		
		windowButton = UIButton.create(UIPrime31.interfMix , "withdraw_button.png" , "withdraw_button.png" , windowBackground.position.x + windowBackground.width*0.55f , -windowBackground.position.y + windowBackground.height*0.7f , -4);
		windowButton.setSize(windowBackground.width*0.4f , windowBackground.height*0.11f);
		windowButton.onTouchUpInside += WithdrawMoney;
		popupContainer.addChild(windowButton);
		
		windowButton = UIButton.create(UIPrime31.questMix , "close_button2.png" , "close_button2.png" , windowBackground.position.x + windowBackground.width*0.34f , -windowBackground.position.y + windowBackground.height*0.84f , -4);
		windowButton.setSize(windowBackground.width*0.32f , windowBackground.height*0.11f);
		windowButton.onTouchUpInside += CloseMoneyWindow;
		popupContainer.addChild(windowButton);
		
		windowButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		windowButton.setSize(windowBackground.width*0.07f , windowBackground.width*0.07f);
		windowButton.position = new Vector3( windowBackground.position.x + windowBackground.width - windowButton.width/1.5f , windowBackground.position.y + windowButton.height/5 , -4);
		windowButton.onTouchUpInside += CloseMoneyWindow;
		
		popupContainer.addChild(windowBackground , windowDecoration , windowButton);
		
		UITexts[0] = thePlayer.interf.text3.addTextInstance("Gold" , windowBackground.position.x + windowBackground.width * 0.12f , -windowBackground.position.y + windowBackground.height*0.04f , UID.TEXT_SIZE*0.9f , -4 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		RefreshGoldAmounts();
	}
	
	void  RefreshGoldAmounts ()
	{
		for(int i= 1 ; i < 4 ; i++)
		{
			if(UITexts[i] != null)
			{
				UITexts[i].clear();
				UITexts[i] = null;
			}
		}
		
		UITexts[1] = thePlayer.interf.text1.addTextInstance("Your Gold: " + thePlayer.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COINAGE), windowBackground.position.x + windowBackground.width * 0.07f , -windowBackground.position.y + windowBackground.height*0.2f , UID.TEXT_SIZE*0.8f , -5 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		UITexts[2] = thePlayer.interf.text1.addTextInstance("Chest Gold: " + thePlayer.guild.m_GuildBankMoney , windowBackground.position.x + windowBackground.width * 0.07f , -windowBackground.position.y + windowBackground.height*0.3f , UID.TEXT_SIZE*0.8f , -5 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		UITexts[3] = thePlayer.interf.text1.addTextInstance("Remaining gold for today: " + GuildManager.getIntAsString(thePlayer.guild.remainingMoneyForToday) , windowBackground.position.x + windowBackground.width * 0.07f , -windowBackground.position.y + windowBackground.height*0.4f , UID.TEXT_SIZE*0.8f , -5 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
	}
	
	private void  ItemInfoWindow ()
	{
		DestroyPopup();
		
		windowBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.1f , Screen.height*0.25f , -3);
		windowBackground.setSize(Screen.width*0.7f , Screen.height*0.7f);
		popupContainer.addChild(windowBackground);
		
		itemSelected.position = new Vector3( windowBackground.position.x + windowBackground.width*0.03f , windowBackground.position.y - windowBackground.height*0.17f , -4);
		windowDecoration = UIPrime31.questMix.addSprite("decoration_case.png" , itemSelected.position.x - itemSelected.width*0.03f , -itemSelected.position.y - itemSelected.height*0.03f , -5);
		windowDecoration.setSize(itemSelected.width*1.2f , itemSelected.height*1.2f);
		popupContainer.addChild(windowDecoration);
		
		windowDecoration = UIPrime31.interfMix.addSprite("vendor_decoration.png" , windowBackground.position.x + windowBackground.width*0.025f , -windowBackground.position.y + windowBackground.height*0.05f , -4);
		windowDecoration.setSize(windowBackground.width*0.95f , windowBackground.height*0.1f);
		popupContainer.addChild(windowBackground , windowDecoration);
		
		windowDecoration = UIPrime31.interfMix.addSprite("vendor_decoration1.png" , windowBackground.position.x + windowBackground.width*0.015f , -windowBackground.position.y + windowBackground.height*0.25f , -4);
		windowDecoration.setSize(windowBackground.width*0.25f , windowBackground.height*0.45f);
		popupContainer.addChild(windowDecoration);
		
		windowDecoration = UIPrime31.questMix.addSprite("background_case.png" , windowBackground.position.x + windowBackground.width*0.27f , -windowBackground.position.y + windowBackground.height*0.17f , -4);
		windowDecoration.setSize(windowBackground.width*0.7f , windowBackground.height*0.8f);
		popupContainer.addChild(windowDecoration);
		
		windowButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , windowBackground.position.x + windowBackground.width*0.03f , -windowBackground.position.y + windowBackground.height*0.85f , -4);
		windowButton.setSize(windowBackground.width*0.22f , windowBackground.height*0.093f);
		windowButton.onTouchUpInside += CloseItemDelegate;
		popupContainer.addChild(windowButton);
		
		windowButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		windowButton.setSize(windowBackground.width*0.08f , windowBackground.width*0.08f);
		windowButton.position = new Vector3( windowBackground.position.x + windowBackground.width - windowButton.width/1.5f , windowBackground.position.y + windowButton.height/5 , -6);
		windowButton.onTouchUpInside += CloseItemDelegate;
		popupContainer.addChild(windowButton);
		
		UITexts[0] = thePlayer.interf.text3.addTextInstance("Item: " + selectedItem.name , windowBackground.position.x + windowBackground.width * 0.12f , -windowBackground.position.y + windowBackground.height*0.04f , UID.TEXT_SIZE*0.9f , -4 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		UITexts[1] = thePlayer.interf.text1.addTextInstance(selectedItem.GetClassName() + ": " + selectedItem.GetSubClassName() , windowDecoration.position.x + windowDecoration.width * 0.06f , -windowDecoration.position.y + windowDecoration.height*0.05f , UID.TEXT_SIZE*0.8f , -6 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
	}
	
	private void  DepositMoney ( UIButton sender )
	{
		try
		{
			int money = int.Parse( moneyField );
			thePlayer.guildmgr.GuildBankDepositMoney( targetvault.guid, money );
		}
		catch( Exception ex )
		{
			Debug.Log(ex.ToString());
		}
		
		moneyField = "";
		ShowMoneyWindow();
	}
	
	private void  WithdrawMoney ( UIButton sender )
	{
		try
		{
			int money = int.Parse( moneyField );
			thePlayer.guildmgr.GuildBankWithdrawMoney( targetvault.guid, money );
		}
		catch( Exception ex )
		{
			Debug.Log(ex.ToString());
		}
		
		moneyField = "";
		ShowMoneyWindow();
	}
	
	private void  CloseMoneyWindow ( UIButton sender )
	{
		displayMoneyField = false;
		DestroyPopup();
		initShow();
		ShowAll();
	}
	
	private void  CloseTabLog ( UIButton sender )
	{
		showLog = false;
		DestroyLog();
		initShow();
		ShowAll();
	}
	
	private void  CloseTabInfo ( UIButton sender )
	{
		DestroyPopup();
		initShow();
		ShowAll();
	}
	
	private void  ChangeTabInfoDelegate ( UIButton sender )
	{
		ShowTabInfo("");
	}
	
	private void  AcceptTabInfo ( UIButton sender )
	{
		DestroyPopup();
		initShow();
		ShowAll();
		Debug.Log("tab update: " + thePlayer.guild.selectedTab + " t: " + thePlayer.guild.tabText);
		if(thePlayer.guild.tabText == "")
			emptyField = true;
		thePlayer.guildmgr.SetGuildBankTabText( thePlayer.guild.selectedTab, thePlayer.guild.tabText );
	}
	
	private void  TabInfoButtonDelegate ( UIButton sender )
	{
		thePlayer.guildmgr.QueryGuildBankText( thePlayer.guild.selectedTab );
		Debug.Log("tab: " + thePlayer.guild.selectedTab);
		emptyField = false;
	}
	
	private void  LogButtonDelegate ( UIButton sender )
	{
		initWindow();
		thePlayer.guildmgr.QueryGuildBankLog( thePlayer.guild.selectedTab );
	}
	
	private void  GoldButtonDelegate ( UIButton sender )
	{
		moneyField = "";
		ShowMoneyWindow();
		displayMoneyField = true;
		initWindow();
		thePlayer.guildmgr.QueryGuildBankMoneyWithdrawn();
	}
	
	private void  BuyTabButtonDelegate ( UIButton sender )
	{
		AlertWindow("Do you want to buy\nguild tab for 499 Mithril?");
	}
	
	private void  CloseButtonDelegate ( UIButton sender )
	{
		hideMinimap = false;
		ClearAll();
	}
	
	private void  BuyTab ( UIButton sender )
	{
		thePlayer.guildmgr.SendGuildBankBuyTab( targetvault.guid, thePlayer.guild.purchasedTabs );
		DestroyAlert();
		Debug.Log("Send buy: " + thePlayer.guild.purchasedTabs );
	}
	
	void  ShowGuildBank ()
	{
		if ( WindowActive )
		{
			return;
		}
		int i;
		
		vaultBackground = UIPrime31.interfMix.addSprite("vendor_inventory_panel.png", Screen.width*0.02f, Screen.height*0.26f, 5);
		vaultBackground.setSize(Screen.width * 0.36f , Screen.height * 0.635f);
		
		UIToolkit tempUIToolkit;
		for(i=0;i<thePlayer.guild.purchasedTabs;i++)
		{
			tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/box_1.png");
			tabs[i] = UIButton.create( tempUIToolkit,"box_1.png", "box_1.png", 0, 0);
			tabs[i].setSize(Screen.width*0.07f , Screen.height*0.1f);
			tabs[i].position = new Vector3(Screen.width*0.02f + tabs[i].width*i , -Screen.height*0.15f , 0);
			tabs[i].onTouchUpInside+=tabEvent;
			tabs[i].info = i;
			tabsIconContainer.addChild(tabs[i]);
		}
		
		if ( thePlayer.guild.selectedTab != 254 )
		{
			Item item;
			string image = "fundal.png";
			for(i=0; i<20; i++)
			{
				item = thePlayer.guild.m_TabListMap[thePlayer.guild.selectedTab].Slots[i];
				
				if(item != null)
				{
					if(item.entry > 0)
					{	 
						image = DefaultIcons.GetItemIcon(item.entry);
					}
				}
				if(image == "" || item == null || item.entry <= 0)
				{
					image = "fundal.png";
				}
				
				tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
				slots[i] = UIButton.create( tempUIToolkit, image, image, 0, 0);
				slots[i].info = i;
				slots[i].setSize(Screen.width * 0.076f , Screen.height * 0.111f);
				slots[i].position = new Vector3(((vaultBackground.position.x+(vaultBackground.width*0.078f))+((i%4)*slots[i].width)),
				                            ((vaultBackground.position.y-(vaultBackground.height*0.062f))-((i/4)*slots[i].height)),
				                            -2);
				slots[i].onTouchDown += clickSlotEvent;
				slots[i].onTouchUpInside += ShowItemInfo;
				
				slotsContainer.addChild(slots[i]);
			}
		}
	}
	
	private void  CloseItemDelegate ( UIButton sender )
	{
		UIToolkit tempUIToolkit = itemSelected.manager;
		itemSelected.destroy();
		if(tempUIToolkit)
		{
			GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
		}
		
		DestroyPopup();
		initShow();
		ShowAll();
	}
	
	void  ShowInvSlots ()
	{
		if(workingRefreshI || WindowActive)
			return;
		
		ShowButtons();
		
		inventoryBagSize = Screen.width*0.07f;
		
		invBackground = UIPrime31.myToolkit3.addSprite("inventory_slots.png" , Screen.width * 0.58f, Screen.height * 0.26f , 5);
		invBackground.setSize(Screen.width * 0.36f , Screen.height * 0.525f);
		
		UIToolkit tempUIToolkit;
		if (thePlayer.itemManager != null && thePlayer.itemManager.BaseBags != null) //might be in process of being remade itself
		{
			workingRefreshI = true; // start by halting updates
			
			bagButton = new UIButton[5];
			
			tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/box_1.png");
			bagButton[0] = UIButton.create( tempUIToolkit, "box_1.png", "box_1.png", 0, 0);
			bagButton[0].setSize(Screen.width * 0.07f , Screen.height * 0.1f);
			bagButton[0].position = new Vector3(Screen.width * 0.585f , -Screen.height * 0.15f , 0);
			bagButton[0].info = 0;
			bagButton[0].onTouchUpInside += bagEvent;
			
			inventoryContainer.addChild(bagButton[0]);
			string bagicon;
			
			foreach(KeyValuePair<int, Bag> bag in thePlayer.itemManager.BaseBags)
			{
				ItemDisplayInfoEntry BagIcon = dbs.sItemDisplayInfo.GetRecord((bag.Value as Bag).displayInfoID);
				bagicon = BagIcon.InventoryIcon;
				if (string.IsNullOrEmpty (bagicon))
				{
					bagicon = "box_1.png";
				}
				tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/" + bagicon);
				bagButton[bag.Key] = UIButton.create( tempUIToolkit, bagicon, bagicon, 0, 0);
				bagButton[bag.Key].setSize(Screen.width * 0.07f , Screen.height * 0.1f);
				bagButton[bag.Key].position = new Vector3(Screen.width * 0.585f + bag.Key * bagButton[bag.Key].width , -Screen.height * 0.15f , 0);
				bagButton[bag.Key].info = bag.Key;
				bagButton[bag.Key].onTouchUpInside += bagEvent; // activeaza inventarul pentru alt bag
				inventoryContainer.addChild(bagButton[bag.Key]);
			}
			
			bagSlot = new UIButton[16];
			
			for(int i = 0; i < thePlayer.interf.inventoryManager.countSlotsInBag; i++) // parcurge itemele din bag, face inventarul
			{
				image="";
				Item item = null;
				ButtonOLD.isNeedRefreshInventory = true;
				
				if(thePlayer.interf.inventoryManager.curBag==0)
				{
					if(thePlayer.itemManager.BaseBagsSlots.ContainsKey(i))
					{
						item = thePlayer.itemManager.BaseBagsSlots[i];
					}
				}
				else if(((thePlayer.itemManager.BaseBags[thePlayer.interf.inventoryManager.curBag])).GetItemFromSlot(i) != null)
				{
					item = ((thePlayer.itemManager.BaseBags[thePlayer.interf.inventoryManager.curBag])).GetItemFromSlot(i);
				}
				
				if(item != null)
				{
					if(item.entry > 0)
					{	 
						image = DefaultIcons.GetItemIcon(item.entry);
					}
				}
				
				if(image == "")
				{
					image = "fundal.png";
				}
				
				tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/" + image);
				bagSlot[i] = UIButton.create( tempUIToolkit, image, image, 0, 3);
				bagSlot[i].info = i;
				bagSlot[i].setSize(Screen.width * 0.076f, Screen.height * 0.111f);
				bagSlot[i].position = new Vector3(((invBackground.position.x+(invBackground.width*0.078f))+((i%4)*bagSlot[i].width)),
				                              ((invBackground.position.y-(invBackground.height*0.07f))-((i/4)*bagSlot[i].height)),
				                              -2);
				if(image != "fundal.png")
				{
					bagSlot[i].onTouchDown += onClickInventory;
					bagSlot[i].onTouchUpInside += ShowItemInfo;
				}
				
				
				inventoryContainer.addChild(bagSlot[i]);
			}
			workingRefreshI = false;
			refreshI = false;
		}
	}
	
	//	function ShowTabSlots()
	//	{
	//		if(tabGrid == null)
	//		{
	//			Debug.Log("Showing tab with id: " + thePlayer.guild.selectedTab);
	//			tabGrid = new UIScrollableVerticalLayout(1);
	//			tabGrid.setGridLayout(5);
	//			tabGrid.position = new Vector3(Screen.width*0.07f,-Screen.height/10*1.3f-Screen.width/12*0.8f,0);
	//			tabGrid.cellSize = UID.CELL_SIZE;
	//			tabGrid.setSize(5*UID.CELL_SIZE, 4*UID.CELL_SIZE);
	//
	//			UICell btn;
	//			byte i;
	//			for(i = 0; i < SharedDefines.GUILD_BANK_MAX_SLOTS; i ++)
	//			{
	//				btn = UICell.create(Button.mixToolkit, null );
	//				btn.info = i;
	//				btn.onTouchUp += onClickTab;
	//				btn.setSize(UID.CELL_SIZE, UID.CELL_SIZE);
	//				
	//				tabGrid.addChild( btn );
	//			}
	//
	//			for(i = 0; i < SharedDefines.GUILD_BANK_MAX_SLOTS; i ++)
	//			{
	//				tabGrid.updateGrid(i, new OBJ(OBJ.CELLTYPE.CELLTYPE_ITEM, thePlayer.guild.m_TabListMap[thePlayer.guild.selectedTab].Slots[i].entry), i);
	//			}
	//		}
	//	}
	
	void  RefreshTabSlots ()
	{
		if(tabGrid == null)
		{
			return;
		}
		for(byte i = 0; i < SharedDefines.GUILD_BANK_MAX_SLOTS; i++)
		{
			tabGrid.updateGrid(i, new OBJ(OBJ.CELLTYPE.CELLTYPE_ITEM, thePlayer.guild.m_TabListMap[thePlayer.guild.selectedTab].Slots[i].entry), i);
		}
	}
	
	public void  VaultDrag ()
	{
		int j = 0;
		
		if (!(goDrag || _drag))
			return;
		
		if(goDrag && !Input.GetMouseButton(0))
		{
			goDrag = false; 
			dragItem = null; //releases "marked" item
			// all this does is make sure that, if player releases before "1 sec" passed, dragging wait is undone
			return;
		}
		if ( goDrag )
		{
			_drag = true; 
			Debug.Log("_drag");
			
			image = DefaultIcons.GetItemIcon(dragItem.entry);
			
			UIToolkit tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
			dragItemIm = UIButton.create(tempUIToolkit, image, image, 0, -3);
			//dragItemIm.setSize(Screen.width*0.39f/4,Screen.height*0.53f/4);
			dragItemIm.setSize(Screen.width * 0.12f, Screen.width * 0.12f);
			
			goDrag = false; // !!! goDrag just triggers dragging, next fixedupdate it false (and has to be)
		}
		
		//Debug.Log("D: " + _drag);
		if(_drag && Input.GetMouseButton(0))
		{
			dragItemIm.position = new Vector3((Input.mousePosition.x-(Screen.width*0.12f)),
			                              ((-Screen.height+Input.mousePosition.y)+(Screen.width*0.12f)),
			                              -3);
		}
		else if(_drag) // in combination, this means touch was released
		{
			if(thePlayer.itemManager != null)
			{
				for(int i = 0 ; i < thePlayer.itemManager.BaseBagsSlots.Count ; i++) // i like this adressing more
				{
					if(bagSlot != null
					   && bagSlot[i] != null
					   && bagSlot[i].touchFrame.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
					{
						// now we need to assign object move according to index, simple ! same as in onClickI()
						if (sourceDrag == ClickEvent.INVENTORY )
						{
							if ( thePlayer.interf.inventoryManager.curBag == 0 )
							{
								thePlayer.itemManager.MoveToSlot(dragItem, (byte)(i + 23), 0); // move item to inventory
							}
							else
							{
								thePlayer.itemManager.MoveToSlot(dragItem, (byte)i, (byte)thePlayer.interf.inventoryManager.curBag);
							}
							Debug.Log("Should have moved item " + dragItem.name + " to slot " + i);
							dragItem = null; //deallocate drag item
							
							DestroyDragItemImage();
							_drag = false;
							goDrag = false;
							return;
						}
						else  // ClickEvent.BANK
						{
							// TODO: make support for current bag > for now 255 ( first one )
							
							//							Debug.Log(" move: b " + thePlayer.guild.selectedTab + "  slot: " + selectedSlot + "  to: " + (i+23) + "  entry: " + dragItem.entry);
							//							for(int _j=0;_j<thePlayer.itemManager.Items.Count;_j++)
							//							{	
							//								if ( thePlayer.itemManager.Items[_j].entry == dragItem.entry
							//										&& thePlayer.itemManager.Items[_j].slot == 0
							//										&& thePlayer.guild.remainingSlotsForToday != 0 )   // hardcode shit - without this won't work correctly - server dosent send create item
							//								{
							//									bool  checkFreeSlot = true;
							//									for(int _k=0;_k<thePlayer.itemManager.Items.Count;_k++)
							//									{
							//										if ((thePlayer.itemManager.Items[_k].slot-1) == (i+23))
							//										{
							//											checkFreeSlot = false;
							//										}
							//									}
							//									if ( checkFreeSlot )
							//									{
							//										Debug.Log("GASIT");
							//										thePlayer.itemManager.Items[_j].slot = i + 24;
							if ( thePlayer.interf.inventoryManager.curBag == 0 )
							{
								thePlayer.guildmgr.SendGuildSwapItemsBankChar( targetvault.guid,
								                                              thePlayer.guild.selectedTab,
								                                              selectedSlot,
								                                              (byte)255,
								                                              (byte)(i+23),
								                                              dragItem.entry,
								                                              (uint)0,
								                                              (byte)1);
							}
							else
							{
								thePlayer.guildmgr.SendGuildSwapItemsBankChar( targetvault.guid,
								                                              thePlayer.guild.selectedTab,
								                                              selectedSlot,
								                                              (byte)(thePlayer.interf.inventoryManager.curBag + 18),
								                                              (byte)i,
								                                              dragItem.entry,
								                                              (uint)0,
								                                              (byte)1);
							}
							
							ButtonOLD.isNeedRefreshBags = true;
							//									}
							//								}
							//								else
							//								{
							//									bool  checkFreeSlot2 = true;
							//									for(int _k2=0;_k2<thePlayer.itemManager.Items.Count;_k2++)
							//									{
							//										if ( (thePlayer.itemManager.Items[_k2].slot-1) == (i+23) )
							//											checkFreeSlot2 = false;
							//									}
							//									if ( checkFreeSlot2 )
							//										if ( thePlayer.interf.inventoryManager.curBag == 0 )
							//											thePlayer.guildmgr.SendGuildSwapItemsBankChar( targetvault.guid, thePlayer.guild.selectedTab, selectedSlot, 255, i+23, dragItem.entry, 0, 1);
							//										else
							//											thePlayer.guildmgr.SendGuildSwapItemsBankChar( targetvault.guid, thePlayer.guild.selectedTab, selectedSlot, thePlayer.interf.inventoryManager.curBag, i, dragItem.entry, 0, 1);
							//								}
							//							}
						}
					}
				}
			}
			
			for(int i = 0; i < 20; i++)
			{
				if (slots[i] != null)
				{
					if(slots[i].touchFrame.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
					{
						Debug.Log("ITEM LA : " + i);
						Debug.Log("item:s " + dragItem.slot + "  entry: " + dragItem.entry);
						byte bag;
						if(sourceDrag == ClickEvent.INVENTORY)
						{
							bag = dragItem.bag;
							if(bag == 0)
								bag = 255;
							if((thePlayer.guild.m_TabListMap[ thePlayer.guild.selectedTab ].Slots[i] != null )
							   && (thePlayer.guild.m_TabListMap[ thePlayer.guild.selectedTab ].Slots[i].entry == 0))
							{
									thePlayer.guildmgr.SendGuildSwapItemsBankChar( targetvault.guid,
								                                              thePlayer.guild.selectedTab,
								                                              (byte)i,
								                                              bag,
								                                              dragItem.slot,
								                                              dragItem.entry,
								                                              (uint)0,
								                                              (byte)0);
							}
						}
						else if(sourceDrag == ClickEvent.CONTAINER)
						{
							Debug.Log("plm");
							bag = thePlayer.itemManager.GetBagSlotByContaineItem(dragItem);
							
							if ((thePlayer.guild.m_TabListMap[ thePlayer.guild.selectedTab ].Slots[i] != null)
							    && ( thePlayer.guild.m_TabListMap[ thePlayer.guild.selectedTab ].Slots[i].entry == 0 ))
							{
									thePlayer.guildmgr.SendGuildSwapItemsBankChar( targetvault.guid,
								                                              thePlayer.guild.selectedTab,
								                                              (byte)i,
								                                              bag,
								                                              dragItem.slot,
								                                              dragItem.entry,
								                                              (uint)0,
								                                              (byte)0);
							}
							Debug.Log("dest: tab: " + thePlayer.guild.selectedTab + " b: " + i + "  -- source: bag: " + bag + " slot: " + dragItem.slot);
						}
						else
						{
							thePlayer.guildmgr.SendGuildSwapItemsBankToBank(targetvault.guid,
							                                                thePlayer.guild.selectedTab,
							                                                selectedSlot,
							                                                thePlayer.guild.selectedTab,
							                                                (byte)i,
							                                                dragItem.entry,
							                                                (uint)0);
						}
					}
				}
			}	
			if (_drag)	// if there was no catch in inventory
			{
				if(thePlayer.itemManager != null)
				{
					if(b_grid != null && b_grid._children[j] != null) // make sure this exists
					{
						Rect cellRect = new Rect(b_grid._children[j].position.x, -b_grid._children[j].position.y, UID.CELL_SIZE , UID.CELL_SIZE);
						if ( cellRect.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)) )
						{
							// now we need to assign object move according to index, simple ! same as in onClickI()
							thePlayer.itemManager.MoveToSlot(dragItem, (byte)(j + 39), 0); // move item to inventory
							Debug.Log("Should have moved item " + dragItem.name + " to slot " + j);
							dragItem = null; //deallocate drag item
							
							DestroyDragItemImage();
							_drag = false;
							goDrag = false;
							return;
						}
					}
				}
			}
			if (_drag)
			{
				DestroyDragItemImage();
				_drag = false;
				goDrag = false;
				dragItem = null; //deallocate drag item
			}
		}
	}
	
	void  ShowItemInfo ( UIButton sender )
	{
		if(selectedItem == null)
			return;
		
		string image = DefaultIcons.GetItemIcon(selectedItem.entry);
		UIToolkit tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/" + image);
		
		itemSelected = UIButton.create(tempUIToolkit, image, image, 0, 3);
		itemSelected.setSize(Screen.width * 0.076f, Screen.height * 0.111f);
		
		tempItmPos = sender.position;
		initWindow();
		DestroyDragItemImage();
		ItemInfoWindow();
	}
	
	void  tabEvent ( UIButton sender )
	{
		Debug.Log("Tab: " + sender.info + "  guid: " + targetvault.guid);
		thePlayer.guildmgr.QueryGuildTab( targetvault.guid, (byte)sender.info );
	}
	
	public void  bagEvent ( UIButton sender )
	{
		thePlayer.itemSelected = null;
		if(sender.info!=0)
		{
			thePlayer.bagSelected = thePlayer.itemManager.BaseBags[sender.info];
		}
		thePlayer.showCharacterStats=true;
		if(sender.info!=0)
		{
			thePlayer.interf.inventoryManager.countSlotsInBag = thePlayer.itemManager.BaseBags[sender.info].GetCapacity();
		}
		else
		{
			thePlayer.interf.inventoryManager.countSlotsInBag =16;
		}
		thePlayer.interf.inventoryManager.lastBag = thePlayer.interf.inventoryManager.curBag;
		thePlayer.interf.inventoryManager.curBag = sender.info;
		
		if ( sender.info != 0 )
		{
			thePlayer.bagSelected.PopulateSlots( thePlayer.itemManager );
		}
		
		ClearInventory();
		ShowInvSlots();
	}
	
	public void  ClearInventory ()
	{
		if(inventoryContainer != null)
		{
			while(inventoryContainer._children.Count > 0)
			{
				var aux=inventoryContainer._children[0];
				UIToolkit tempUIToolkit = aux.manager;
				inventoryContainer.removeChild(aux, false);
				aux.destroy();
				if(tempUIToolkit)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
		}
		if(invBackground != null)
		{
			invBackground.destroy();
			invBackground = null;
		}
	}
	
	void  ClearTabs ()
	{
		UIToolkit tempUIToolkit;
		UISprite aux;
		while( tabsContainer._children.Count > 0 )
		{
			aux = tabsContainer._children[0];
			tabsContainer.removeChild(aux, false);
			aux.destroy();
		}
		
		while( tabsIconContainer._children.Count > 0 )
		{
			aux = tabsIconContainer._children[0];
			tempUIToolkit = aux.manager;
			tabsIconContainer.removeChild(aux, false);
			aux.destroy();
			if(tempUIToolkit)
			{
				GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
			}
		}
		
		while( slotsContainer._children.Count > 0 )
		{
			aux = slotsContainer._children[0];
			tempUIToolkit = aux.manager;
			slotsContainer.removeChild(aux, false);
			aux.destroy();
			if(tempUIToolkit)
			{
				GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
			}
		}
		if(vaultBackground != null)
		{
			vaultBackground.destroy();
			vaultBackground = null;
		}
		
		if(guildVaultText != null)
		{
			guildVaultText.clear();
			guildVaultText = null;
		}
	}
	
	private void  FreezeBackgroundButtons ( bool val )
	{
		foreach(var child in inventoryContainer._children)
		{
			if(child is UIButton)
				(child as UIButton).disabled = val;
		}
		
		foreach(var child in tabsContainer._children)
		{
			if(child is UIButton)
				(child as UIButton).disabled = val;
		}
		
		foreach(var child in tabsIconContainer._children)
		{
			if(child is UIButton)
				(child as UIButton).disabled = val;
		}
		
		foreach(var child in slotsContainer._children)
		{
			if(child is UIButton)
				(child as UIButton).disabled = val;
		}
	}
	
	void  ClearTabSlots ()
	{
		if(tabGrid == null)
			return;
		UISprite aux;
		while(tabGrid._children.Count > 0)
		{
			aux = tabGrid._children[0];
			tabGrid.removeChild(aux, false);
			aux.destroy();
		}
		tabGrid = null;
	}
	
	private byte selectedSlot;
	
	void  clickSlotEvent ( UIButton sender )
	{
		Debug.Log("slot: " + sender.info);
		if ( dragItem == null )
		{
			if (( thePlayer.guild.m_TabListMap[thePlayer.guild.selectedTab].Slots[sender.info] != null)
			    && ( thePlayer.guild.m_TabListMap[thePlayer.guild.selectedTab].Slots[sender.info].entry != 0 ))
			{
				dragItem = thePlayer.guild.m_TabListMap[thePlayer.guild.selectedTab].Slots[sender.info];
				selectedItem = dragItem;
				goDrag = true;
				Debug.Log("Item loaded in drag item "+ dragItem.name);
				sourceDrag = ClickEvent.BANK;
				selectedSlot = (byte)sender.info;
			}
			else
				selectedItem = null;
			
		}
		
	}
	
	public void  onClickInventory ( UIButton sender )
	{
		Debug.Log("Inventory, you clicked "+sender.info);
		
		if (dragItem == null) // if drag item is not loaded
		{
			if (thePlayer.interf.inventoryManager.curBag==0)
			{
				if (thePlayer.itemManager.BaseBagsSlots[sender.info].entry!=0)
				{
					//startDragCount = 8;//numericDragTime;
					dragItem = thePlayer.itemManager.BaseBagsSlots[sender.info];
					selectedItem = dragItem;
					goDrag = true;
					Debug.Log("Item loaded in drag item "+ dragItem.name);
					sourceDrag = ClickEvent.INVENTORY;
				}
				else
				{
					selectedItem = null;
				}
			}
			else 
			{
				Item itm = ((thePlayer.itemManager.BaseBags[thePlayer.interf.inventoryManager.curBag])).GetItemFromSlot(sender.info);
				
				if ( itm.entry != 0 )
				{
					dragItem = itm;
					goDrag = true;
					Debug.Log("Item loaded in drag item "+ dragItem.name);
					sourceDrag = ClickEvent.CONTAINER;
				}
				else
					selectedItem = null;
			}
		}
	}
	
	public void  onClickTab ( UIButton sender )
	{
		Debug.Log("Clicked in "+sender.info+" tab slot");
		
		if (dragItem == null && thePlayer.guild.m_TabListMap[thePlayer.guild.selectedTab].Slots[sender.info] != null)
		{
			dragItem = thePlayer.guild.m_TabListMap[thePlayer.guild.selectedTab].Slots[sender.info];
			goDrag = true;
			Debug.Log("Item loaded in drag item "+ dragItem.name);
		}
	}
	
	private void  AlertWindow ( string message )
	{
		DestroyAlert();
		FreezeBackgroundButtons(true);
		
		UISprite alertBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.3f , -8);
		alertBackground.setSize(Screen.width*0.55f , Screen.height*0.45f);
		
		UISprite alertDecoration = UIPrime31.questMix.addSprite("ornament4.png" , alertBackground.position.x + alertBackground.width*0.03f , -alertBackground.position.y + alertBackground.height*0.22f , -9);
		alertDecoration.setSize(alertBackground.width*0.9f , alertBackground.height*0.33f);
		
		UIButton alertButton = UIButton.create(UIPrime31.interfMix , "buy_button.png" , "buy_button.png" , alertBackground.position.x + alertBackground.width*0.1f , -alertBackground.position.y + alertBackground.height*0.72f , -9);
		alertButton.setSize(alertBackground.width*0.3f , alertBackground.height*0.13f);
		alertButton.onTouchUpInside += BuyTab;
		alertContainer.addChild(alertButton);
		
		alertButton = UIButton.create(UIPrime31.myToolkit3 , "canceltbutton.png" , "canceltbutton.png" , alertBackground.position.x + alertBackground.width*0.6f , -alertBackground.position.y + alertBackground.height*0.72f , -9);
		alertButton.setSize(alertBackground.width*0.3f , alertBackground.height*0.13f);
		alertButton.onTouchUpInside += CloseAlertDelegate;
		alertContainer.addChild(alertButton);
		
		alertButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		alertButton.setSize(alertBackground.width*0.1f , alertBackground.width*0.1f);
		alertButton.position = new Vector3( alertBackground.position.x + alertBackground.width - alertButton.width/1.5f , alertBackground.position.y + alertButton.height/5 , -9);
		alertButton.onTouchUpInside += CloseAlertDelegate;
		
		alertContainer.addChild(alertBackground , alertDecoration , alertButton);
		
		UIalert = thePlayer.interf.text3.addTextInstance(message , alertBackground.position.x + alertBackground.width * 0.6f , -alertBackground.position.y + alertBackground.height*0.25f , UID.TEXT_SIZE*0.7f , -9 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
	}
	
	private void  CloseAlertDelegate ( UIButton sender )
	{
		DestroyAlert();
	}
	
	private void  DestroyAlert ()
	{
		FreezeBackgroundButtons(false);
		alertContainer.removeAllChild(true);
		if(UIalert != null)
		{
			UIalert.clear();
			UIalert = null;
		}
	}
	
	private void  DestroyDragItemImage ()
	{
		if(dragItemIm != null)
		{
			UIToolkit tempUIToolkit = dragItemIm.manager;
			dragItemIm.destroy();
			dragItemIm = null;
			if(tempUIToolkit)
			{
				GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
			}
		}
	}
}