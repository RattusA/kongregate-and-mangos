using UnityEngine;
using System;

public class GuildHandler
{
	public void  HandlePetitionShowList (  WorldPacket pkt ,   Player mplayer )
	{
		ulong guid;
		byte count;
		uint index;
		uint charter;
		uint charterDisplayId;
		uint charterCost;
		uint unk;
		uint reqSigns;
		
		guid = pkt.ReadReversed64bit();
		count = pkt.Read();
		if ( count == 1 )
		{
			index = pkt.ReadReversed32bit();
			charter = pkt.ReadReversed32bit();
			charterDisplayId = pkt.ReadReversed32bit();
			charterCost = pkt.ReadReversed32bit();
			unk = pkt.ReadReversed32bit();
			reqSigns = pkt.ReadReversed32bit();
		}
	}
	
	public void  HandleGuildBankText (  WorldPacket pkt, MainPlayer mplayer )
	{
		byte tabSelected = pkt.Read();
		string tabText = pkt.ReadString();
		Debug.Log("recived texT: " + tabSelected + "  t: " + tabText + "  seltab: " +mplayer.guild.selectedTab);
		if ( mplayer.guild.selectedTab == tabSelected )   // update text only if it's the same selected tab
		{
			if ( tabText == null )
			{
				mplayer.guild.tabText = "";
			}
			else
			{
				if ( tabText[0] == 0 )
				{
					mplayer.guild.tabText = "";
				}
				else
				{
					mplayer.guild.tabText = tabText;
				}
			}
			
			(mplayer as MainPlayer).gVault.ShowTabInfo(mplayer.guild.tabText);
		}
	}
	
	public void  HandleBankMoneyWithdrawn (  WorldPacket pkt ,   MainPlayer mplayer )
	{
		mplayer.guild.remainingMoneyForToday = pkt.ReadReversed32bit();
	}
	
	public void  HandleGuildBankQueryLog (  WorldPacket pkt ,   MainPlayer mplayer )
	{
		// MSG_GUILD_BANK_LOG_QUERY
		// member = mplayer.guild.FindMember( mguid );
		GuildBankEventLogEntry giuldBankEvent;
		ulong mguid;
		byte sizeLog;
		
		mplayer.guild.bankEvents.Clear();
		if ( mplayer.guild.members.Count == 0 )
			throw new InvalidOperationException("Guild members are empty!");
		
		mplayer.guild.bankEventsTab = pkt.Read();
		sizeLog = pkt.Read();
		for(int i=0;i<sizeLog;i++)
		{
			giuldBankEvent = new GuildBankEventLogEntry();
			giuldBankEvent.eventType = (GuildBankEventLogTypes)pkt.Read();
			mguid = pkt.ReadReversed64bit();
			giuldBankEvent.member = mplayer.guild.FindMember( mguid );
			
			if ( giuldBankEvent.eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_DEPOSIT_MONEY 
			    || giuldBankEvent.eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_WITHDRAW_MONEY 
			    || giuldBankEvent.eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_REPAIR_MONEY 
			    || giuldBankEvent.eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_UNK1 
			    || giuldBankEvent.eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_UNK2 )
			{
				giuldBankEvent.itemOrMoney = pkt.ReadReversed32bit();
			}
			else
			{
				giuldBankEvent.itemOrMoney = pkt.ReadReversed32bit();
				giuldBankEvent.itemStackCount = (byte)pkt.ReadReversed32bit();
				if ( giuldBankEvent.eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_MOVE_ITEM || giuldBankEvent.eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_MOVE_ITEM2 )
					giuldBankEvent.destTabId = pkt.Read();
			}
			giuldBankEvent.timeStamp = pkt.ReadReversed32bit();
			giuldBankEvent.updateText();
			mplayer.guild.bankEvents.Add( giuldBankEvent );
		}
		Debug.Log("ENTER FUNCTION HandleGuildBankQueryLog()");
		MainPlayer mainplayer = mplayer;
		mainplayer.gVault.ShowTabLog();
	}
	
	public void  HandleGuildBankList (  WorldPacket pkt ,   MainPlayer mplayer )
	{
		Debug.Log("received guild bank list");
		//uint remainingSlotsForToday;
		byte maxItemSlots;
		byte tabinfo;
		
		// mplayer.guild.initTabs();
		
		mplayer.guild.m_GuildBankMoney = pkt.ReadReversed64bit();
		mplayer.guild.selectedTab = pkt.Read();
		
		mplayer.guild.remainingSlotsForToday = pkt.ReadReversed32bit();
		
		tabinfo = pkt.Read(); // TAB INFO
		if ( tabinfo > 0 )
		{
			mplayer.guild.purchasedTabs = pkt.Read();
			for(int _i=0;_i<mplayer.guild.purchasedTabs;_i++)
			{
				mplayer.guild.m_TabListMap[_i].Name = pkt.ReadString();
				mplayer.guild.m_TabListMap[_i].Icon = pkt.ReadString();
			}
		}
		// END TAB INFO
		
		// ITEMS DISPLAY
		maxItemSlots = pkt.Read();
		Debug.Log("tabId " + mplayer.guild.selectedTab + " max: " + maxItemSlots);
		
		byte slot;
		uint entry;
		uint sufix; // ??
		uint itempropertyid;
		uint count;
		
		byte enchCount;
		byte enchIndex;
		uint enchId;
		ItemEntry itmEntry = null;
		for(int i=0;i<maxItemSlots;i++)
		{
			slot = pkt.Read();
			if ( mplayer.guild.m_TabListMap[mplayer.guild.selectedTab].Slots[slot] == null)
			{
				mplayer.guild.m_TabListMap[mplayer.guild.selectedTab].Slots[slot] = new Item();
			}
			entry = pkt.ReadReversed32bit();
			if ( entry > 0 )
			{
				pkt.ReadReversed32bit(); // dunno
				itempropertyid = pkt.ReadReversed32bit();
				if ( itempropertyid > 0 )
				{
					sufix = pkt.ReadReversed32bit();
				}
				count = pkt.ReadReversed32bit();
				pkt.ReadReversed32bit(); //unk
				pkt.Read();
				
				// enchants:
				enchCount = pkt.Read();
				for(int j=0;j<enchCount;j++)
				{
					enchIndex = pkt.Read();
					enchId = pkt.ReadReversed32bit();
				}
				
				//				mplayer.guild.m_TabListMap[mplayer.guild.selectedTab].Slots[slot].stackCount = count;
				Debug.Log("Received: " + entry + " c: " + count );
				
				itmEntry = AppCache.sItems.GetItemEntry(entry);
				if ( itmEntry != null )
				{
					mplayer.guild.m_TabListMap[mplayer.guild.selectedTab].Slots[slot].name = itmEntry.Name;
					mplayer.guild.m_TabListMap[mplayer.guild.selectedTab].Slots[slot].classType = (ItemClass)itmEntry.Class;
					mplayer.guild.m_TabListMap[mplayer.guild.selectedTab].Slots[slot].subClass = itmEntry.SubClass;
				}
			}
			
			mplayer.guild.m_TabListMap[mplayer.guild.selectedTab].Slots[slot].entry = entry;
			//Debug.Log("Entry: " + entry );
			// set count to item slot :-? ---------------------------------------------!
		}
		// END ITEMS DISPLAY
		MainPlayer mainplayer = mplayer;
		mainplayer.gVault.UpdateAll();
	}
	
	public void  HandleGuildEventLog (  WorldPacket pkt ,   MainPlayer mplayer )
	{
		EventLog eventLog;
		ulong mguid;
		byte sizelog = pkt.Read();
		mplayer.guild.events.Clear();
		if ( mplayer.guild.members.Count == 0 )
			throw new InvalidOperationException("Guild members are empty!");
		
		for(int i=0;i<sizelog;i++)
		{
			eventLog = new EventLog();
			eventLog.eventType = (GuildEventLogTypes)pkt.Read();
			mguid = pkt.ReadReversed64bit();
			eventLog.member1 = mplayer.guild.FindMember( mguid );
			if ( eventLog.eventType != GuildEventLogTypes.GUILD_EVENT_LOG_JOIN_GUILD && eventLog.eventType != GuildEventLogTypes.GUILD_EVENT_LOG_LEAVE_GUILD )
			{
				mguid = pkt.ReadReversed64bit();
				eventLog.member2 = mplayer.guild.FindMember( mguid );
			}
			if ( eventLog.eventType == GuildEventLogTypes.GUILD_EVENT_LOG_PROMOTE_PLAYER || eventLog.eventType == GuildEventLogTypes.GUILD_EVENT_LOG_DEMOTE_PLAYER )
				eventLog.newrank = pkt.Read();
			//SRV: Event timestamp
			//SRV:       data << uint32(time(NULL)-itr->TimeStamp);
			eventLog.eventTime = pkt.ReadReversed32bit();
			eventLog.updateText( mplayer );
			mplayer.guild.events.Add( eventLog );
			//Debug.Log("Event: " + eventLog.eventType + " member1: " + eventLog.member1.Name + " time: " + eventLog.eventTime );
		}
	}
	
	public void  UpdateRanks (  WorldPacket pkt ,   MainPlayer mplayer )
	{
		uint m_Id;
		string m_Name;
		Guild pguild = mplayer.guild;
		string rankname;
		pguild.m_Ranks.Clear();
		
		m_Id = pkt.ReadReversed32bit();
		m_Name = pkt.ReadString();
		
		pguild.Name = m_Name;
		pguild.m_Id = m_Id;
		
		for(int i=0;i<SharedDefines.GUILD_RANKS_MAX_COUNT;i++)
		{
			rankname = pkt.ReadString();
			if ( string.IsNullOrEmpty(rankname) || rankname[0] == 0 )
			{
				break;
			}
			pguild.m_Ranks.Add( new RankInfo( rankname ) );
			//mplayer.AddChatInput(new message("Rank: ","",rankname));
		}
		// Here we recive emblem infos like style and colors - not used yet
		/*
		data << uint32(m_EmblemStyle);
	    data << uint32(m_EmblemColor);
	    data << uint32(m_BorderStyle);
	    data << uint32(m_BorderColor);
	    data << uint32(m_BackgroundColor);
	    data << uint32(0);                                      // probably real ranks count
		*/
	}
	
	public void  HandleInvite (  WorldPacket pkt ,   MainPlayer mplayer )
	{
		string player_inv;
		string guild_name;
		
		player_inv = pkt.ReadString();
		guild_name = pkt.ReadString();
		
		string str = "You have been invited by " + player_inv + " to join the guild " + guild_name;
		mplayer.AddChatInput(new Message("Guild","",str));
		mplayer.guildUI.guildNameInvited = guild_name;
	}
	
	public void HandleRoster(  WorldPacket pkt ,   MainPlayer mplayer )		// roster is used with UpdateRanks to know the name of your ranks
	{
		uint membersize;
		string MOTD;
		string GInfo;
		
		uint rank_size;

		GuildRankRights rank_rights;
		uint rank_moneypd;
		RankInfo rank_info;
		
		membersize = pkt.ReadReversed32bit();
		MOTD = pkt.ReadString();
		GInfo = pkt.ReadString();
		
		rank_size = pkt.ReadReversed32bit();
		for(int i=0;i<rank_size;i++)
		{
			rank_rights = (GuildRankRights)pkt.ReadReversed32bit();
			rank_moneypd = pkt.ReadReversed32bit();				// count of: withdraw gold(gold/day) Note: in game set gold, in packet set bronze
			//rank_info = new RankInfo( rank_rights, rank_moneypd );
			rank_info = mplayer.guild.getRankByIndex(i);
			if ( rank_info == null )
			{
				Debug.Log("Rank inexistent! : " + i);
				//break;
				rank_info = new RankInfo( rank_rights, rank_moneypd );
			}
			rank_info.UpdateInfo( rank_rights, rank_moneypd );
			
			for(int j=0;j<SharedDefines.GUILD_BANK_MAX_TABS;j++)
			{
				rank_info.TabRight[j] = pkt.ReadReversed32bit();		// for TAB_i rights: view tabs = 0x01, deposit items =0x02
				rank_info.TabSlotPerDay[j] = pkt.ReadReversed32bit();	// for TAB_i count of: withdraw items(stack/day)
			}
			//mplayer.guild.m_Ranks.UpdateRankInfo( i, rank_rights, rank_moneypd );
		}
		
		//Debug.Log(" Got like: " + mplayer.guild.m_Ranks.Count + " ranks");
		
		MemberSlot member;
		mplayer.guild.members.Clear();
		for(int i = 0; i < membersize; i++)
		{
			member = new MemberSlot();
			member.guid = pkt.ReadReversed64bit();
			member.online = pkt.Read();
			member.Name = pkt.ReadString();
			member.RankId = pkt.ReadReversed32bit();
			member.Level = pkt.Read();
			member.Class = (Classes)pkt.Read();
			pkt.Read();  // null
			member.ZoneId = pkt.ReadReversed32bit();
			member.LogoutTime = 0;
			if ( member.online == 0 )
				member.LogoutTime = pkt.ReadFloat();
			member.Pnote = pkt.ReadString();
			member.OFFnote = pkt.ReadString();
			mplayer.guild.members.Add( member );
			//mplayer.AddChatInput(new message("Guild","","Membru: " + member.Name + " lvl: " + member.Level + " rank: " + member.RankId + " logouttime: " + member.LogoutTime));
		}
		mplayer.guild.SortMembersByRank();
		mplayer.guild.setMotd( MOTD, GInfo );
		
		mplayer.guildUI.UpdateMember( mplayer.guild.members );   // GUILD UI CODE, update showing member
		
		//mplayer.guild.LogAllMembers();
		
	}
	
	public void  HandleInfo (  WorldPacket pkt ,   MainPlayer mplayer )
	{
		string name;
		uint secsToTimeBitFields;
		uint membersize;
		uint accountsnumber;
		
		name = pkt.ReadString();
		secsToTimeBitFields = pkt.ReadReversed32bit();
		membersize = pkt.ReadReversed32bit();
		accountsnumber = pkt.ReadReversed32bit();
		
		Debug.Log("Info guild: " + name + "  secs: " + secsToTimeBitFields + " m size: " + membersize + "  acc number: " + accountsnumber);
		
		string str = "Name: " + name + " size: " + membersize;
		mplayer.guild.setInfo(name, secsToTimeBitFields);
		mplayer.AddChatInput(new Message("Guild","",str));
		if(mplayer.target != null && (mplayer.target as NPC).guildMaster != null)
		{
			if((mplayer.target as NPC).guildMaster.waitingForConfirmation)
				(mplayer.target as NPC).guildMaster.checkIfGuildCreated();
		}	
		/*
		if ( mplayer.guildMaster.waitingForConfirmation )
			mplayer.guildMaster.checkIfGuildCreated();*/
	}
	
	public void  ManageEvent (  WorldPacket pkt ,   MainPlayer mplayer )
	{
		GuildEvents eventId;
		byte strCount;
		string str;
		string eventStr = "";
		string msgStr = "";
		
		eventId = (GuildEvents)pkt.Read();
		strCount = pkt.Read();
		
		eventStr = GuildManager.GetEventAsString( eventId );
		
		Debug.Log("Event: " + eventId.ToString() + "   count: " + strCount );
		
		mplayer.guildUI.boolShowingGuild = true;  // refresh guild show
		
		//		if ( !strCount )
		//			mplayer.AddChatInput(new message("Guild","",eventStr));
		//		else
		//		{
		int guildremoveaux =1;
		for(int i=0;i<strCount;i++)
		{	
			switch ( eventId )
			{
			case GuildEvents.GE_REMOVED:
				if(guildremoveaux==2)
					msgStr += "has been removed from guild by ";
				
				break;
			case GuildEvents.GE_JOINED:
				msgStr += eventStr + " ";
				break;
			case GuildEvents.GE_PROMOTION:
			case GuildEvents.GE_DEMOTION: 
				switch (i)
				{
				case 1: 
					msgStr += eventStr;
					break;
				case 2:
					msgStr += "to ";
					break;
				}
				break;
			case GuildEvents.GE_DISBANDED:
			case GuildEvents.GE_LEFT:
				mplayer.guildUI.notinGuild = true;
				mplayer.guild.members.Clear();
				msgStr += eventStr + " ";
				break;
			default:
				msgStr += eventStr + " ";
				break;
			}
			
			str = pkt.ReadString();
			msgStr += str + " ";
			guildremoveaux++;
		}
		mplayer.AddChatInput(new Message("Guild","",msgStr));
		//		}
	}
	
	public void  GetCommandResult (  WorldPacket pkt ,   MainPlayer mplayer )
	{
		uint typecmd;
		uint cmdresult;
		string str;
		string typecmd_str = "";
		string errcmd_str = "";
		
		typecmd = pkt.ReadReversed32bit();
		str = pkt.ReadString();
		cmdresult = pkt.ReadReversed32bit();
		
		//Debug.LogError("GET RESULT: " + typecmd + "   sir: " + str + "    res: " + cmdresult);
		
		switch ( typecmd )
		{
		case 0:
			mplayer.guildUI.notinGuild = true;
			mplayer.guild.members.Clear();
			break;
		default:
			mplayer.guildUI.notinGuild = false;	
			break;
		}
		
		typecmd_str = GuildManager.GetCommandTypeAsString( (Typecommand)cmdresult );
		
		errcmd_str = GuildManager.GetCommandErrorAsString( (CommandErrors)cmdresult );
		
		str = typecmd_str + str + errcmd_str;
		mplayer.guildUI.showWindow(str);
		
		//mplayer.AddChatInput(new message("Guild","",str));	//-not sure if right -Sebek
		if(mplayer.target != null && (mplayer.target as NPC).guildMaster != null)
		{	
			if((mplayer.target as NPC).guildMaster.waitingForConfirmation)
				(mplayer.target as NPC).guildMaster.sendFailedCreateGuildWindow();
		}
		/*
		if ( mplayer.guildMaster.waitingForConfirmation )
			mplayer.guildMaster.sendFailedCreateGuildWindow();*/
	}
}
