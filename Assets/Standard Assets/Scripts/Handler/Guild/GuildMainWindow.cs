﻿using UnityEngine;

public class GuildMainWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Vector2 scrollPosition = Vector2.zero;
	private Rect scrollZone;
	private Rect fullScrollZone;
	
	private Rect guildNameLabelRect;
	private Rect guildPlayerZoneRect;
	
	private Rect nameLabel;
	private Rect lvlLabel;
	private Rect lastLabel;
	
	private Rect messageButtonRect;
	private Rect infoButtonRect;
	private Rect logButtonRect;
	private Rect addButtonRect;
	private Rect kickButtonRect;
	private Rect manageButtonRect;
	private Rect leaveButtonRect;
	
	public Rect arrowUpButtonRect;
	public Rect arrowDownButtonRect;
	
	private GUIStyle normalMCStyle = new GUIStyle();
	private GUIStyle normalMLStyle = new GUIStyle();
	private GUIStyle boldMCStyle = new GUIStyle();
	
	private bool  loadData = true;
	private bool  inviteRights = false;
	private bool  removeRights = false;
	
	private GuildUI guild;
	
	/**
	 *	Guild Main Window
	 *	MenuState = 1
	 **/
	
	public GuildMainWindow ()
	{
		scrollZone = new Rect(sw * 0.03f, sh * 0.25f, sw * 0.58f, sh * 0.7f);
		fullScrollZone = new Rect(0, 0, sw * 0.56f, sh * 0.7f);
		guildNameLabelRect = new Rect(sw * 0.2f, sh * 0.125f, sw * 0.6f, sh * 0.04f);
		guildPlayerZoneRect = new Rect(sw * 0.00625f, sh * 0.16667f, sw * 0.65f, sh * 0.825f);
		nameLabel = new Rect(sw * 0.1f, sh * 0.2f, sw * 0.2f, sh * 0.04f);
		lvlLabel = new Rect(sw * 0.33f, sh * 0.2f, sw * 0.1f, sh * 0.04f);
		lastLabel = new Rect(sw * 0.46f, sh * 0.2f, sw * 0.1f, sh * 0.04f);
		messageButtonRect = new Rect(sw * 0.65625f, sh * 0.18333f, sw * 0.3375f, sh * 0.08333f);
		infoButtonRect = new Rect(sw * 0.65625f, sh * 0.3f, sw * 0.3375f, sh * 0.08333f);
		logButtonRect = new Rect(sw * 0.65625f, sh * 0.41667f, sw * 0.3375f, sh * 0.08333f);
		addButtonRect = new Rect(sw * 0.65625f, sh * 0.53333f, sw * 0.3375f, sh * 0.08333f);
		kickButtonRect = new Rect(sw * 0.65625f, sh * 0.65f, sw * 0.3375f, sh * 0.08333f);
		manageButtonRect = new Rect(sw * 0.65625f, sh * 0.76667f, sw * 0.3375f, sh * 0.08333f);
		leaveButtonRect = new Rect(sw * 0.65625f, sh * 0.88333f, sw * 0.3375f, sh * 0.08333f);
		arrowUpButtonRect = new Rect(sw * 0.59375f, sh * 0.20833f, sw * 0.05f, sw * 0.05f);
		arrowDownButtonRect = new Rect(sw * 0.59375f, sh * 0.88333f, sw * 0.05f, sw * 0.05f);

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		normalMCStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.04f, 
		                                        Color.black, true, FontStyle.Normal);
		normalMLStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.04f, 
		                                        Color.black, true, FontStyle.Normal);
		boldMCStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.04f, 
		                                      Color.black, true, FontStyle.Bold);
	}
	
	private void  LoadDate ()
	{
		inviteRights = WorldSession.player.guildmgr.CheckRights(WorldSession.player, GuildRankRights.GR_RIGHT_INVITE);
		removeRights = WorldSession.player.guildmgr.CheckRights(WorldSession.player, GuildRankRights.GR_RIGHT_REMOVE);
		
		guild = WorldSession.player.guildUI;
		
		loadData = false;
	}
	
	public void  Show ()
	{
		if(loadData)
			LoadDate();
		
		GUI.Label(guildNameLabelRect, WorldSession.player.guild.Name, guild.guildSkin.GetStyle("GuildName"));
		
		guild.guildAtlasToolkit.DrawTexture(messageButtonRect, "GuildMessageButton.png");
		if(GUI.Button(messageButtonRect, "", GUIStyle.none))
		{
			GuildInfo();
		}
		
		guild.guildAtlasToolkit.DrawTexture(infoButtonRect, "GuildInfoButton.png");
		if(GUI.Button(infoButtonRect, "", GUIStyle.none))
		{
			PlayerInfo();
		}
		
		guild.guildAtlasToolkit.DrawTexture(logButtonRect, "GuildLogButton.png");
		if(GUI.Button(logButtonRect, "", GUIStyle.none))
		{
			LogWindow();
		}
		
		if(inviteRights)
		{
			guild.guildAtlasToolkit.DrawTexture(addButtonRect, "GuildAddButton.png");
			if(GUI.Button(addButtonRect, "", GUIStyle.none))
			{
				AddPlayer();
			}
		}
		
		if(removeRights)
		{
			guild.guildAtlasToolkit.DrawTexture(kickButtonRect, "GuildKickButton.png");
			if(GUI.Button(kickButtonRect, "", GUIStyle.none))
			{
				RemoveFromGuild();
			}
		}
		
		if(WorldSession.player.guild.getMyRank(WorldSession.player) == 0)
		{
			guild.guildAtlasToolkit.DrawTexture(manageButtonRect, "GuildManageButton.png");
			if(GUI.Button(manageButtonRect, "", GUIStyle.none))
			{
				guild.ChangeGuildWindowState(GuildWindowState.MANAGE_WINDOW);
			}
		}
		
		guild.guildAtlasToolkit.DrawTexture(leaveButtonRect, "GuildLeaveButton.png");
		if(GUI.Button(leaveButtonRect, "", GUIStyle.none))
		{
			LeaveGuild();
		}
		
		GUI.DrawTexture(guildPlayerZoneRect, guild.textDecoration);
		
		GUI.Label(nameLabel, "Name", boldMCStyle);
		GUI.Label(lvlLabel, "Lvl.", boldMCStyle);
		GUI.Label(lastLabel, "Last", boldMCStyle);
		
		scrollPosition = GUI.BeginScrollView(scrollZone, scrollPosition, fullScrollZone, GUIStyle.none, GUIStyle.none);
		DrawGuildMembers();
		GUI.EndScrollView();
		
		
		guild.guildAtlasToolkit.DrawTexture(arrowUpButtonRect, "ScrollUp.png");
		if(GUI.RepeatButton(arrowUpButtonRect, "", GUIStyle.none))
		{
			ScrollUp();
		}
		
		guild.guildAtlasToolkit.DrawTexture(arrowDownButtonRect, "ScrollDown.png");
		if(GUI.RepeatButton(arrowDownButtonRect, "", GUIStyle.none))
		{
			ScrollDown();
		}
	}
	
	private void  GuildInfo ()
	{
		guild.ChangeGuildWindowState(GuildWindowState.MESSAGE_WINDOW);
		loadData = true;
	}
	
	private void  PlayerInfo ()
	{
		if(guild.memberID == 999)
		{
			return;
		}
		guild.ChangeGuildWindowState(GuildWindowState.PLAYER_INFO);
		loadData = true;
	}
	
	private void  LogWindow ()
	{
		guild.ChangeGuildWindowState(GuildWindowState.LOG_WINDOW);
		loadData = true;
	}
	
	private void  RemoveFromGuild ()
	{
		if(guild.memberID == 999)
		{
			return;
		}
		
		guild.SetConfirmWindowState(GuildConfirmState.KICK_PLAYER);
		guild.ChangeGuildWindowState(GuildWindowState.CONFIRM_WINDOW);
		loadData = true;
	}
	
	private void  LeaveGuild ()
	{
		guild.SetConfirmWindowState(GuildConfirmState.LEAVE_GUILD);
		guild.ChangeGuildWindowState(GuildWindowState.CONFIRM_WINDOW);
		loadData = true;
	}
	
	private void  ScrollUp ()
	{
		scrollPosition.y -= sh * 0.03333f;
	}
	
	private void  ScrollDown ()
	{
		scrollPosition.y += sh * 0.03333f;
	}
	
	private void  AddPlayer ()
	{
		guild.SetAddWindowState(AddWindowType.ADD_PLAYER);
		guild.ChangeGuildWindowState(GuildWindowState.ADD_WINDOW);
		loadData = true;
	}
	
	private void  DrawGuildMembers ()
	{
		int membersCount = WorldSession.player.guild.members.Count;
		
		for(int pos = 0; pos < membersCount; pos++)
		{
			float padding = sh * 0.08f * pos;
			MemberSlot player = WorldSession.player.guild.members[pos];
			string lvl = "" + player.Level;
			string last = player.GetLogoutTime();
			
			Rect buttonRect = new Rect(0, padding, sw * 0.56f, sh * 0.06f);
			Rect iconRect = new Rect(sw * 0.01f, padding, sw * 0.03f, sw * 0.03f);
			Rect nameTextRect = new Rect(sw * 0.05f, padding, sw * 0.2f, sh * 0.04f);
			Rect lvlTextRect = new Rect(sw * 0.31f, padding, sw * 0.08f, sh * 0.04f);
			Rect lastTextRect = new Rect(sw * 0.38f, padding, sw * 0.2f, sh * 0.04f);
			
			if(guild.memberID != pos)
			{
				DrawUnSelectedButton(buttonRect, player, pos);
			}
			else
			{
				DrawSelectedButton(buttonRect);
			}
			
			if(player.LogoutTime == 0)
			{
				guild.guildAtlasToolkit.DrawTexture(iconRect, GetClassIcon(player.Class));
			}
			else
			{
				guild.guildAtlasToolkit.DrawTexture(iconRect, "ClassIconEmpty.png");
			}
			
			GUI.Label(nameTextRect, player.Name, normalMLStyle);
			GUI.Label(lvlTextRect, lvl, normalMCStyle);
			GUI.Label(lastTextRect, last, normalMCStyle);
		}
		fullScrollZone = new Rect(0, 0, sw * 0.56f, sh * 0.08f * membersCount);
	}
	
	private string GetClassIcon ( Classes classID )
	{
		string iconName = "classMark_off.png";
		switch(classID)
		{
		case Classes.CLASS_WARRIOR:
			iconName = "ClassIconFighter.png";
			break;
		case Classes.CLASS_PALADIN:
			iconName = "ClassIconTemplar.png";
			break;
		case Classes.CLASS_MAGE:
			iconName = "ClassIconMage.png";
			break;
		case Classes.CLASS_ROGUE:
			iconName = "ClassIconRogue.png";
			break;
		case Classes.CLASS_PRIEST:
			iconName = "ClassIconConfessor.png";
			break;
		case Classes.CLASS_WARLOCK:
			iconName = "ClassIconNecromancer.png";
			break;
		case Classes.CLASS_HUNTER:
			iconName = "ClassIconRanger.png";
			break;
		}
		
		return iconName;
	}
	
	private void  DrawUnSelectedButton ( Rect positionRect ,   MemberSlot player ,   int pos )
	{
		if(GUI.Button(positionRect, "", GUIStyle.none))
		{
			guild.SetMember(player, pos);
		}
	}
	
	private void  DrawSelectedButton ( Rect positionRect )
	{
		if(GUI.Button(positionRect, "", guild.guildSkin.GetStyle("SelectedPlayer")))
		{
			guild.SetMember(null, 999);
		}
	}
}