﻿using UnityEngine;

public class GuildLogWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;

	private Vector2 scrollPosition = Vector2.zero;
	private Rect scrollZone;
	
	private Rect windowDecorationRect;
	private Rect topDecorationRect;
	
	private Rect guildMessageTextBoxRect;
	private Rect exitButtonRect;
	
	private bool  loadData = true;
	private GUIStyle logTextStyle = new GUIStyle();
	private GuildUI guild;
	
	public GuildLogWindow ()
	{
		scrollZone = new Rect(sw * 0.1f, sh * 0.21f, sw * 0.79f, sh * 0.58f);
		windowDecorationRect = new Rect(sw * 0.0625f, sh * 0.08333f, sw * 0.875f, sh * 0.83333f);
		topDecorationRect = new Rect(sw * 0.1125f, sh * 0.10833f, sw * 0.8f, sh * 0.08333f);
		guildMessageTextBoxRect = new Rect(sw * 0.0875f, sh * 0.19167f, sw * 0.825f, sh * 0.61667f);
		exitButtonRect = new Rect(sw * 0.35f, sh * 0.81667f, sw * 0.275f, sh * 0.08333f);

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		logTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.UpperLeft, sh * 0.04f, Color.black, true, FontStyle.Normal);
	}
	
	public void  Show ()
	{
		if(guild == null)
		{
			guild = WorldSession.player.guildUI;
			return;
		}
		
		if(loadData)
		{
			WorldSession.player.guildmgr.RequestGuildEventLog();
			loadData = false;
		}
		
		GUI.DrawTexture(windowDecorationRect, guild.mainDecoration);
		GUI.DrawTexture(topDecorationRect, guild.logDecoration);
		
		GUI.DrawTexture(guildMessageTextBoxRect, guild.textDecoration);
		
		guild.guildAtlasToolkit.DrawTexture(exitButtonRect, "ExitButton.png");
		if(GUI.Button(exitButtonRect, "", GUIStyle.none))
		{
			CloseWindow();
		}
		
		GUILayout.BeginArea(scrollZone);
		scrollPosition = GUILayout.BeginScrollView(scrollPosition);
		DrawLogInfo();
		GUILayout.EndScrollView();	
		GUILayout.EndArea();
	}
	
	private void  DrawLogInfo ()
	{
		int eventCount = WorldSession.player.guild.events.Count;
		
		if(eventCount >= 1)
		{
			for(int i = eventCount-1; i >= 0; i--)
			{
				GUILayout.Label(WorldSession.player.guild.events[i].text, logTextStyle);
			}
		}
	}
	
	private void  CloseWindow ()
	{	
		guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
		loadData = true;
	}
}