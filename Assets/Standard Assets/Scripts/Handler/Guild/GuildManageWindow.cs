﻿using UnityEngine;

public class GuildManageWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Vector2 scrollPosition = Vector2.zero;
	private Rect scrollZone;
	private Rect fullScrollZone;
	
	private Rect windowDecorationRect;
	private Rect guildNameLabelRect;
	
	private Rect decorationLineRect;
	private Rect decoretionShieldRect;
	
	private Rect addButtonRect;
	private Rect removeButtonRect;
	private Rect settingsButtonRect;
	private Rect exitButtonRect;
	
	private Rect arrowUpButtonRect;
	private Rect arrowDownButtonRect;
	
	private GUIStyle whiteTextStyle = new GUIStyle();
	private GUIStyle redTextStyle = new GUIStyle();
	private GUIStyle blackTextStyle = new GUIStyle();
	
	private GuildUI guild;
	/**
	 *	Guild manage window
	 *	MenuState = 7
	 **/
	
	public GuildManageWindow ()
	{
		scrollZone = new Rect(sw * 0.14375f, sh * 0.28333f, sw * 0.4f, sh * 0.6f);
		fullScrollZone = new Rect(0, 0, sw * 0.375f, sh * 0.6f);
		windowDecorationRect = new Rect(sw * 0.125f, sh * 0.16667f, sw * 0.8125f, sh * 0.75f);
		guildNameLabelRect = new Rect(sw * 0.15f, sh * 0.2f, sw * 0.45f, sh * 0.055f);
		decorationLineRect = new Rect(sw * 0.15f, sh * 0.25f, sw * 0.5375f, sh * 0.0333f);
		decoretionShieldRect = new Rect(sw * 0.6625f, sh * 0.18333f, sw * 0.225f, sh * 0.2f);
		addButtonRect = new Rect(sw * 0.6125f, sh * 0.41667f, sw * 0.3125f, sh * 0.09167f);
		removeButtonRect = new Rect(sw * 0.6125f, sh * 0.53333f, sw * 0.3125f, sh * 0.09167f);
		settingsButtonRect = new Rect(sw * 0.6125f, sh * 0.65f, sw * 0.3125f, sh * 0.09167f);
		exitButtonRect = new Rect(sw * 0.6125f, sh * 0.76667f, sw * 0.3125f, sh * 0.09167f);
		arrowUpButtonRect = new Rect(sw * 0.54375f, sh * 0.3f, sw * 0.0625f, sh * 0.08333f);
		arrowDownButtonRect = new Rect(sw * 0.54375f, sh * 0.8f, sw * 0.0625f, sh * 0.08333f);

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		whiteTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.065f, Color.white, true, FontStyle.Normal);
		redTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.05f, Color.red, true, FontStyle.Normal);
		blackTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.05f, Color.black, true, FontStyle.Normal);
	}
	
	public void  Show ()
	{
		if(guild == null)
		{
			guild = WorldSession.player.guildUI;
			return;
		}
		
		GUI.DrawTexture(windowDecorationRect, guild.mainDecoration);
		GUI.Label(guildNameLabelRect, WorldSession.player.guild.Name, whiteTextStyle);
		
		GUI.DrawTexture(decorationLineRect, guild.manageDecoration);
		GUIAtlas.interfMix3.DrawTexture(decoretionShieldRect, "guild_decor.png");
		
		GUIAtlas.interfMix3.DrawTexture(addButtonRect, "add_button2.png");
		if(GUI.Button(addButtonRect, "", GUIStyle.none))
			AddRank();
		
		GUIAtlas.interfMix3.DrawTexture(removeButtonRect, "remove_button.png");
		if(GUI.Button(removeButtonRect, "", GUIStyle.none))
			RemoveRank();
		
		GUIAtlas.interfMix3.DrawTexture(settingsButtonRect, "settings_button.png");
		if(GUI.Button(settingsButtonRect, "", GUIStyle.none))
			SettingRank();
		
		GUIAtlas.interfMix3.DrawTexture(exitButtonRect, "exit_button.png");
		if(GUI.Button(exitButtonRect, "", GUIStyle.none))
			CloseWindow();
		
		
		guild.guildAtlasToolkit.DrawTexture(arrowUpButtonRect, "ScrollUp.png");
		if(GUI.RepeatButton(arrowUpButtonRect, "", GUIStyle.none))
			ScrollUp();
		
		guild.guildAtlasToolkit.DrawTexture(arrowDownButtonRect, "ScrollDown.png");
		if(GUI.RepeatButton(arrowDownButtonRect, "", GUIStyle.none))
			ScrollDown();
		
		
		scrollPosition = GUI.BeginScrollView(scrollZone, scrollPosition, fullScrollZone, GUIStyle.none, GUIStyle.none);
		DrawGuildRanks();
		GUI.EndScrollView();
	}
	
	
	private void  DrawGuildRanks ()
	{
		int rankCounts = WorldSession.player.guild.m_Ranks.Count;
		fullScrollZone = new Rect(0, 0, sw * 0.375f, 10 + rankCounts * sh * 0.1083f);
		
		for(int index = 0; index < rankCounts; index++)
		{
			RankInfo rank = WorldSession.player.guild.m_Ranks[index];
			float padding = 10 + index * sh * 0.1083f;
			Rect positionRect = new Rect(10, padding, sw * 0.375f, sh * 0.09167f);
			
			GUIAtlas.interfMix3.DrawTexture(positionRect, "empty_button.png");
			if(GUI.Button(positionRect, "", GUIStyle.none))
			{
				guild.rankId = index;
			}
			
			if(guild.rankId == index)
			{
				GUI.Label(positionRect, rank.Name, redTextStyle);
			}
			else
			{
				GUI.Label(positionRect, rank.Name, blackTextStyle);
			}
		}
	}
	
	private void  AddRank ()
	{
		guild.SetAddWindowState(AddWindowType.ADD_RANK);
		guild.ChangeGuildWindowState(GuildWindowState.ADD_WINDOW);
	}
	
	private void  RemoveRank ()
	{
		guild.SetConfirmWindowState(GuildConfirmState.REMOVE_RANK);
		guild.ChangeGuildWindowState(GuildWindowState.CONFIRM_WINDOW);
	}
	
	private void  SettingRank ()
	{
		if(guild.rankId == 999)
			return;
		
		guild.manageWindowPage = ManageRankPage.FIRST_PAGE;
		guild.ChangeGuildWindowState(GuildWindowState.MANAGE_SETTING);
	}
	
	private void  CloseWindow ()
	{
		guild.rankId = 999;
		guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
	}
	
	private void  ScrollUp ()
	{
		scrollPosition.y -= sh * 0.03333f;
	}
	
	private void  ScrollDown ()
	{
		scrollPosition.y += sh * 0.03333f;
	}
}