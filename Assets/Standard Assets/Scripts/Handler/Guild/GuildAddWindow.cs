﻿using UnityEngine;

public class GuildAddWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Rect windowDecorationRect;
	
	private Rect labelRect;
	private Rect textBoxRect;
	private Rect textRect;
	
	private Rect addButtonRect;
	private Rect cancelButtonRect;
	
	private GUIStyle messageTextStyle = new GUIStyle();
	private GUIStyle imputeTextStyle = new GUIStyle();
	private GUIStyle buttonTextStyle = new GUIStyle();
	private string text = "";
	
	private GuildUI guild;
	
	/**
	 *	Universal Guild Add window
	 *	MenuState = 5
	 *		WindowState
	 *		1 - Add new player to guild
	 *		2 - Add new rank into guild
	 *		3 - Change guild leader
	 **/
	
	public GuildAddWindow ()
	{
		windowDecorationRect = new Rect(sw * 0.15625f, sh * 0.29167f, sw * 0.6875f, sh * 0.5f);
		labelRect = new Rect(sw * 0.1875f, sh * 0.35f, sw * 0.625f, sh * 0.125f);
		textBoxRect = new Rect(sw * 0.1875f, sh * 0.5f, sw * 0.625f, sh * 0.125f);
		textRect = new Rect(sw * 0.205f, sh * 0.51f, sw * 0.55f, sh * 0.11f);
		addButtonRect = new Rect(sw * 0.21875f, sh * 0.66667f, sw * 0.275f, sh * 0.08333f);
		cancelButtonRect = new Rect(sw * 0.53125f, sh * 0.66667f, sw * 0.275f, sh * 0.08333f);

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		messageTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.06f, Color.white, true, FontStyle.Normal);
		imputeTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.09f, Color.black, true, FontStyle.Normal);
		buttonTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.05f, Color.black, true, FontStyle.Normal);	
	}
	
	public void  Show ()
	{
		if(guild == null)
		{
			guild = WorldSession.player.guildUI;
			return;
		}
		
		GUI.DrawTexture(windowDecorationRect, guild.mainDecoration);
		
		switch(guild.addWindowState)
		{
		case AddWindowType.ADD_PLAYER:
			GUI.Label(labelRect, "Insert the name of the player to invite.", messageTextStyle);
			
			guild.guildAtlasToolkit.DrawTexture(textBoxRect, "BigTextBox.png");
			text = GUI.TextArea(textRect, text, imputeTextStyle);
			
			break;
		case AddWindowType.ADD_RANK:
			GUI.Label(labelRect, "Insert the name of the rank to add.",messageTextStyle);
			
			guild.guildAtlasToolkit.DrawTexture(textBoxRect, "BigTextBox.png");
			text = GUI.TextArea(textRect, text, imputeTextStyle);
			
			break;
		case AddWindowType.CHANGE_LEADER:
			GUI.Label(labelRect, "Are you sure you want to change the Guild Master?",messageTextStyle);
			break;
		}
		
		switch(guild.addWindowState)
		{
		case AddWindowType.ADD_PLAYER:
			guild.guildAtlasToolkit.DrawTexture(addButtonRect, "EmptyButtom.png");
			if(GUI.Button(addButtonRect, "Add", buttonTextStyle))
			{
				AddNewPlayer();
			}
			
			guild.guildAtlasToolkit.DrawTexture(cancelButtonRect, "EmptyButtom.png");
			if(GUI.Button(cancelButtonRect, "Cancel", buttonTextStyle))
			{
				guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
			}
			
			break;
		case AddWindowType.ADD_RANK:
			guild.guildAtlasToolkit.DrawTexture(addButtonRect, "EmptyButtom.png");
			if(GUI.Button(addButtonRect, "Add", buttonTextStyle))
			{
				AddNewRank();
			}
			
			guild.guildAtlasToolkit.DrawTexture(cancelButtonRect, "EmptyButtom.png");
			if(GUI.Button(cancelButtonRect, "Cancel", buttonTextStyle))
			{
				guild.ChangeGuildWindowState(GuildWindowState.MANAGE_WINDOW);
			}
			
			break;
		case AddWindowType.CHANGE_LEADER:
			guild.guildAtlasToolkit.DrawTexture(addButtonRect, "EmptyButtom.png");
			if(GUI.Button(addButtonRect, "Yes", buttonTextStyle))
			{
				ChangeGuildMaster();
			}
			
			guild.guildAtlasToolkit.DrawTexture(cancelButtonRect, "EmptyButtom.png");
			if(GUI.Button(cancelButtonRect, "No", buttonTextStyle))
			{
				guild.ChangeGuildWindowState(GuildWindowState.PLAYER_INFO);
			}
			
			break;
		}
	}
	
	private void  AddNewPlayer ()
	{
		if(WorldSession.player.guildmgr.CheckRights(WorldSession.player, GuildRankRights.GR_RIGHT_INVITE))
		{
			WorldSession.player.guildmgr.InvitePlayer(text);
		}
		else
		{
			Debug.LogError("TODO.");
		}
		
		text = "";
		guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
	}
	
	private void  AddNewRank ()
	{
		WorldSession.player.guildmgr.AddRank(text);
		text = "";
		guild.ChangeGuildWindowState(GuildWindowState.MANAGE_WINDOW);
	}
	
	private void  ChangeGuildMaster ()
	{
		WorldSession.player.guildmgr.ChangeLeader(guild.member.Name);
		guild.ChangeGuildWindowState(GuildWindowState.PLAYER_INFO);
	}
}