using UnityEngine;

public class GuildPlayerInfoWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	protected MainPlayer mainPlayer;
	
	public Rect windowDecorationRect;
	public Rect decorationRect;
	
	public Rect playerNameLabelRect;
	public Rect playerStatusLabelRect;
	public Rect rankLabelRect;
	public Rect noteLabelRect;
	
	public Rect upgradeButtonRect;
	public Rect degradeButtonRect;
	public Rect saveButtonRect;
	public Rect exitButtonRect;
	
	public Rect textRect;
	public Rect textAresRect;
	
	private GUIStyle style = new GUIStyle();
	private string noteText = "";
	private bool  loadData = true;
	
	private bool  upgradeRight = false;
	private bool  degradeRight = false;
	private bool  updateNoteRight = false;
	
	private GuildUI guild;
	
	
	/**
	 *	Guild manage window
	 *	MenuState = 3
	 **/
	
	public GuildPlayerInfoWindow ()
	{
		windowDecorationRect = new Rect(sw * 0.125f, sh * 0.16667f, sw * 0.8125f, sh * 0.75f);
		decorationRect = new Rect(sw * 0.15f, sh * 0.21667f, sw * 0.75f, sh * 0.11667f);
		playerNameLabelRect = new Rect(sw * 0.16f, sh * 0.195f, sw * 0.3f, sh * 0.04f);
		playerStatusLabelRect = new Rect(sw * 0.46f, sh * 0.195f, sw * 0.3f, sh * 0.04f);
		rankLabelRect = new Rect(sw * 0.16f, sh * 0.29f, sw * 0.3f, sh * 0.05f);
		noteLabelRect = new Rect(sw * 0.16f, sh * 0.35f, sw * 0.3f, sh * 0.05f);
		upgradeButtonRect = new Rect(sw * 0.6125f, sh * 0.41667f, sw * 0.3125f, sh * 0.09167f);
		degradeButtonRect = new Rect(sw * 0.6125f, sh * 0.53333f, sw * 0.3125f, sh * 0.09167f);
		saveButtonRect = new Rect(sw * 0.6125f, sh * 0.65f, sw * 0.3125f, sh * 0.09167f);
		exitButtonRect = new Rect(sw * 0.6125f, sh * 0.76667f, sw * 0.3125f, sh * 0.09167f);
		textRect = new Rect(sw * 0.15f, sh * 0.41667f, sw * 0.45f, sh * 0.44167f);
		textAresRect = new Rect(sw * 0.16f, sh * 0.425f, sw * 0.42f, sh * 0.41f);

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		style.font = font;
		style.alignment = TextAnchor.MiddleLeft;
		style.fontSize = (int)(sh * 0.06f);
		style.normal.textColor = Color.white;
	}
	
	public void  Show ()
	{
		if(guild == null)
		{
			guild = WorldSession.player.guildUI;
			return;
		}
		
		if(loadData)
		{
			mainPlayer = WorldSession.player;
			upgradeRight = mainPlayer.guildmgr.CheckRights(mainPlayer, GuildRankRights.GR_RIGHT_PROMOTE);
			degradeRight = mainPlayer.guildmgr.CheckRights(mainPlayer, GuildRankRights.GR_RIGHT_DEMOTE);
			updateNoteRight = mainPlayer.guildmgr.CheckRights(mainPlayer, GuildRankRights.GR_RIGHT_EPNOTE);
			
			loadData = false;
			noteText = mainPlayer.guildUI.member.Pnote;
			if(noteText == "\0")
			{
				noteText = string.Empty;
			}
		}
		
		GUI.DrawTexture(windowDecorationRect, guild.mainDecoration);
		GUI.Label(playerNameLabelRect, mainPlayer.guildUI.member.Name, style);
		GUI.Label(playerStatusLabelRect, "Status: " + mainPlayer.guildUI.member.GetLogoutTime(), style);
		GUIAtlas.interfMix3.DrawTexture(decorationRect, "decoration5.png");
		GUI.Label(rankLabelRect, "Rank: " + mainPlayer.guild.GetRankAsString(mainPlayer.guildUI.member.RankId), style);
		GUI.Label(noteLabelRect, "Note:", style);
		
		GUI.DrawTexture(textRect, guild.textDecoration);
		
		if(upgradeRight)
		{
			DrawUpgradeButton();
		}
		
		if(degradeRight)
		{
			DrawDegradeFunction();
		}
		
		if(updateNoteRight)
		{
			DrawSaveButton();
		}
		else
		{
			GUI.Label(textAresRect, noteText, mainPlayer.guildUI.guildSkin.GetStyle("ULBlackText"));
		}
		
		guild.guildAtlasToolkit.DrawTexture(exitButtonRect, "ExitButton.png");
		if(GUI.Button(exitButtonRect, "", style))
		{
			CloseWindow();
		}
	}
	
	private void  DrawUpgradeButton ()
	{
		GUIAtlas.interfMix3.DrawTexture(upgradeButtonRect, "upgrade_button.png");
		if(GUI.Button(upgradeButtonRect, "", style))
		{
			UpgradePlayer();
		}
	}
	
	private void  DrawDegradeFunction ()
	{
		GUIAtlas.interfMix3.DrawTexture(degradeButtonRect, "degrade_button.png");
		if(GUI.Button(degradeButtonRect, "", style))
		{
			DegradePlayer();
		}
	}
	
	private void  DrawSaveButton ()
	{
		noteText = GUI.TextArea(textAresRect, noteText, mainPlayer.guildUI.guildSkin.GetStyle("ULBlackText"));
		
		guild.guildAtlasToolkit.DrawTexture(saveButtonRect, "SaveButton.png");
		if(GUI.Button(saveButtonRect, "", style))
		{
			SavePlayerInfo();
		}
	}
	
	private void  UpgradePlayer ()
	{
		if(mainPlayer.guildUI.member.RankId == 1 && mainPlayer.guild.getMyRank(mainPlayer) == 0)
		{
			ChanheGuildLeader();
		}
		else
		{
			mainPlayer.guildmgr.SendPromotion(mainPlayer.guildUI.member.Name);
		}
		loadData = true;
	}
	
	private void  ChanheGuildLeader ()
	{
		mainPlayer.guildUI.SetAddWindowState(AddWindowType.CHANGE_LEADER);
		mainPlayer.guildUI.ChangeGuildWindowState(GuildWindowState.ADD_WINDOW);
	}
	
	private void  DegradePlayer ()
	{
		mainPlayer.guildmgr.SendDemote(mainPlayer.guildUI.member.Name);
		loadData = true;
	}
	
	private void  SavePlayerInfo ()
	{
		mainPlayer.guildmgr.SetPlayerNote(mainPlayer.guildUI.member.Name, noteText);
		CloseWindow();
	}
	
	private void  CloseWindow ()
	{
		loadData = true;
		mainPlayer.guildUI.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
	}
}