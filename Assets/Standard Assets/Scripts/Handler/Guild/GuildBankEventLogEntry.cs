﻿
public class GuildBankEventLogEntry
{
	public GuildBankEventLogTypes eventType;
	//uint playerGuid;
	public MemberSlot member;
	public uint itemOrMoney;
	public byte itemStackCount;
	public byte destTabId;
	public uint timeStamp;
	public string text;
	ItemEntry itm;
	
	public void  updateText ()
	{
		text = member.Name + " ";
		text += GuildManager.GetGuildBankEventAsString( eventType );
		if ( eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_DEPOSIT_MONEY 
		    || eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_WITHDRAW_MONEY 
		    || eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_REPAIR_MONEY 
		    || eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_UNK1 
		    || eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_UNK2 )
		{
			text += "amount:\n" + itemOrMoney + "\n";
		}
		else
		{
			itm = AppCache.sItems.GetItemEntry(itemOrMoney);//not needed for now
			text += "\n" + itm.Name;
			text += " " + itemStackCount;
			if ( eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_MOVE_ITEM || eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_MOVE_ITEM2 )
			{
				text += " destTab: " + destTabId;
			}
			
		}
		
		text += " - " + Global.GetTimeAsString( timeStamp, false ) + " ago";
	}
	
//	bool isMoneyEvent ()
//	{
//		return eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_DEPOSIT_MONEY || eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_WITHDRAW_MONEY || eventType == GuildBankEventLogTypes.GUILD_BANK_LOG_REPAIR_MONEY;
//	}
}
