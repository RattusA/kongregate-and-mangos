using UnityEngine;

public class GuildManageRankWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	protected MainPlayer mainPlayer;
	
	private Rect windowDecorationRect;
	private Rect rankLabelRect;
	private Rect rankTextBoxRect;
	private Rect rankTextBox;
	
	private Rect checkButton1Rect;
	private Rect checkButton2Rect;
	private Rect checkButton3Rect;
	private Rect checkButton4Rect;
	private Rect checkButton5Rect;

	private Rect checkButton6Rect;
	private Rect checkButton7Rect;
	private Rect checkButton8Rect;
	private Rect checkButton9Rect;
	private Rect checkButton0Rect;
	
	private Rect checkButton1Label;
	private Rect checkButton2Label;
	private Rect checkButton3Label;
	private Rect checkButton4Label;
	private Rect checkButton5Label;

	private Rect checkButton6Label;
	private Rect checkButton7Label;
	private Rect checkButton8Label;
	private Rect checkButton9Label;
	private Rect checkButton0Label;

	public Rect nextPageRect;
	
	private Rect moneyTextBoxRect;
	private Rect previousPageRect;
	
	private Rect[] bankSlotRect;
	private string[] bankSlotImageName;
	
	private Rect selectedSlotRect;
	private string selectedSlotName;
	
	private Rect checkButton11Rect;
	private Rect checkButton12Rect;
	private Rect checkButton13Rect;
	
	private Rect checkButton11Label;
	private Rect checkButton12Label;
	private Rect checkButton13Label;
	
	private Rect repairGoldLabel;
	private Rect goldTextFieldRect;
	private Rect goldTextField;
	
	private Rect saveButtonRect;
	private Rect exitButtonRect;
	
	private string rankName = "";
	private string goldPerDayText = "";
	private string itemsPerSlot = "";
	private bool  loadData = true;
	
	private RankInfo rank;
	private rankRightsBool rankrightsBooleans = new rankRightsBool();
	private rankBankRightsBool bankTabs = new rankBankRightsBool();
	private uint indexTab = 0;
	
	private GUIStyle whiteMLStyle = new GUIStyle();
	private GUIStyle whiteMCStyle = new GUIStyle();
	private GUIStyle blackMLStyle = new GUIStyle();
	private GUIStyle blackMCStyle = new GUIStyle();
	
	private GuildUI guild;
	
	/**
	 *	Guild manage rank window
	 *	MenuState = 9
	 **/
	
	public GuildManageRankWindow ()
	{
		windowDecorationRect = new Rect(sw * 0.05f, sh * 0.15f, sw * 0.8f, sh * 0.16667f);
		rankLabelRect = new Rect(sw * 0.125f, sh * 0.15f, sw * 0.3f, sh * 0.08333f);
		rankTextBoxRect = new Rect(sw * 0.4375f, sh * 0.15f, sw * 0.4375f, sh * 0.08333f);
		rankTextBox = new Rect(sw * 0.45f, sh * 0.15f, sw * 0.42f, sh * 0.08333f);
		checkButton1Rect = new Rect(sw * 0.05f, sh * 0.35f, sw * 0.05f, sw * 0.05f);
		checkButton2Rect = new Rect(sw * 0.05f, sh * 0.48333f, sw * 0.05f, sw * 0.05f);
		checkButton3Rect = new Rect(sw * 0.05f, sh * 0.61667f, sw * 0.05f, sw * 0.05f);
		checkButton4Rect = new Rect(sw * 0.05f, sh * 0.75f, sw * 0.05f, sw * 0.05f);
		checkButton5Rect = new Rect(sw * 0.05f, sh * 0.88333f, sw * 0.05f, sw * 0.05f);
		checkButton6Rect = new Rect(sw * 0.4375f, sh * 0.35f, sw * 0.05f, sw * 0.05f);
		checkButton7Rect = new Rect(sw * 0.4375f, sh * 0.48333f, sw * 0.05f, sw * 0.05f);
		checkButton8Rect = new Rect(sw * 0.4375f, sh * 0.61667f, sw * 0.05f, sw * 0.05f);
		checkButton9Rect = new Rect(sw * 0.4375f, sh * 0.75f, sw * 0.05f, sw * 0.05f);
		checkButton0Rect = new Rect(sw * 0.4375f, sh * 0.88333f, sw * 0.05f, sw * 0.05f);
		checkButton1Label = new Rect(sw * 0.12f, sh * 0.35f, sw * 0.3f, sw * 0.05f);
		checkButton2Label = new Rect(sw * 0.12f, sh * 0.48333f, sw * 0.3f, sw * 0.05f);
		checkButton3Label = new Rect(sw * 0.12f, sh * 0.61667f, sw * 0.3f, sw * 0.05f);
		checkButton4Label = new Rect(sw * 0.12f, sh * 0.75f, sw * 0.3f, sw * 0.05f);
		checkButton5Label = new Rect(sw * 0.12f, sh * 0.88333f, sw * 0.3f, sw * 0.05f);
		checkButton6Label = new Rect(sw * 0.5f, sh * 0.35f, sw * 0.45f, sw * 0.05f);
		checkButton7Label = new Rect(sw * 0.5f, sh * 0.48333f, sw * 0.45f, sw * 0.05f);
		checkButton8Label = new Rect(sw * 0.5f, sh * 0.61667f, sw * 0.45f, sw * 0.05f);
		checkButton9Label = new Rect(sw * 0.5f, sh * 0.75f, sw * 0.45f, sw * 0.05f);
		checkButton0Label = new Rect(sw * 0.5f, sh * 0.88333f, sw * 0.45f, sw * 0.05f);
		nextPageRect = new Rect(sw * 0.925f, sh * 0.9f, sw * 0.0625f, sh * 0.08333f);
		moneyTextBoxRect = new Rect(sw * 0.4375f, sh * 0.15f, sw * 0.4375f, sh * 0.08333f);
		previousPageRect = new Rect(sw * 0.925f, sh * 0.13667f, sw * 0.0625f, sh * 0.08333f);
		checkButton11Rect = new Rect(sw * 0.05f, sh * 0.58333f, sw * 0.05f, sw * 0.05f);
		checkButton12Rect = new Rect(sw * 0.05f, sh * 0.71667f, sw * 0.05f, sw * 0.05f);
		checkButton13Rect = new Rect(sw * 0.05f, sh * 0.85f, sw * 0.05f, sw * 0.05f);
		checkButton11Label = new Rect(sw * 0.12f, sh * 0.58333f, sw * 0.25f, sw * 0.05f);
		checkButton12Label = new Rect(sw * 0.12f, sh * 0.71667f, sw * 0.3f, sw * 0.05f);
		checkButton13Label = new Rect(sw * 0.12f, sh * 0.85f, sw * 0.3f, sw * 0.05f);
		repairGoldLabel = new Rect(sw * 0.4f, sh * 0.58333f, sw * 0.4f, sh * 0.08333f);
		goldTextFieldRect = new Rect(sw * 0.8125f, sh * 0.58333f, sw * 0.175f, sh * 0.08333f);
		goldTextField = new Rect(sw * 0.82f, sh * 0.58333f, sw * 0.165f, sh * 0.08333f);
		saveButtonRect = new Rect(sw * 0.5f, sh * 0.75f, sw * 0.275f, sh * 0.08333f);
		exitButtonRect = new Rect(sw * 0.5f, sh * 0.86667f, sw * 0.275f, sh * 0.08333f);
		selectedSlotRect = new Rect(sw * 0.4f, sh * 0.26f, sw * 0.4f, sh * 0.08333f);
		bankSlotRect = new Rect[6];
		bankSlotImageName = new string[6];
		selectedSlotName = "Current Bank slot - 1";

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		whiteMLStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.045f, Color.white, true, FontStyle.Normal);
		whiteMCStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.045f, Color.white, true, FontStyle.Normal);
		blackMLStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.045f, Color.black, true, FontStyle.Normal);
		blackMCStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.045f, Color.black, true, FontStyle.Normal);
		
		bankSlotRect[0] = new Rect(sw * 0.05f, sh * 0.35f, sw * 0.0875f, sh * 0.11667f);
		bankSlotRect[1] = new Rect(sw * 0.2125f, sh * 0.35f, sw * 0.0875f, sh * 0.11667f);
		bankSlotRect[2] = new Rect(sw * 0.375f, sh * 0.35f, sw * 0.0875f, sh * 0.11667f);
		bankSlotRect[3] = new Rect(sw * 0.5375f, sh * 0.35f, sw * 0.0875f, sh * 0.11667f);
		bankSlotRect[4] = new Rect(sw * 0.7f, sh * 0.35f, sw * 0.0875f, sh * 0.11667f);
		bankSlotRect[5] = new Rect(sw * 0.8625f, sh * 0.35f, sw * 0.0875f, sh * 0.11667f);
		
		bankSlotImageName[0] = "ButtonOne.png";
		bankSlotImageName[1] = "ButtonTwo.png";
		bankSlotImageName[2] = "ButtonThree.png";
		bankSlotImageName[3] = "ButtonFour.png";
		bankSlotImageName[4] = "ButtonFive.png";
		bankSlotImageName[5] = "ButtonSix.png";
	}
	
	
	public void  Show ()
	{
		if(guild == null)
		{
			guild = WorldSession.player.guildUI;
			return;
		}
		
		if(loadData)
		{
			mainPlayer = WorldSession.player;
			rank = mainPlayer.guild.m_Ranks[mainPlayer.guildUI.rankId];
			rankName = rank.Name;
			goldPerDayText = "" + rank.BankMoneyPerDay;
			
			
			rankrightsBooleans.UpdateRights(rank.Rights);
			bankTabs.UpdateRights(rank.TabRight, rank.TabSlotPerDay);
			indexTab = 0;
			itemsPerSlot = bankTabs.guildTabSlotPerDay[indexTab];
			
			selectedSlotName = "Current Bank slot - 1";
			loadData = false;
		}
		
		switch(mainPlayer.guildUI.manageWindowPage)
		{
		case ManageRankPage.FIRST_PAGE:
			ShowFirstWindow();
			break;
		case ManageRankPage.SECOND_PAGE:
			ShowSecondWindow();
			break;
		}
	}
	
	private void  ShowFirstWindow ()
	{
		GUIAtlas.interfMix3.DrawTexture(windowDecorationRect, "decoration6.png");
		guild.guildAtlasToolkit.DrawTexture(rankTextBoxRect, "ShortTextBox.png");
		GUI.Label(rankLabelRect, "RANK NAME:", whiteMCStyle);
		rankName = GUI.TextArea(rankTextBox, rankName, blackMCStyle);
		
		rankrightsBooleans.promote.active = DrawToggleButton(checkButton1Rect, rankrightsBooleans.promote.active);
		rankrightsBooleans.demote.active = DrawToggleButton(checkButton2Rect, rankrightsBooleans.demote.active);
		rankrightsBooleans.pinvite.active = DrawToggleButton(checkButton3Rect, rankrightsBooleans.pinvite.active);
		rankrightsBooleans.premove.active = DrawToggleButton(checkButton4Rect, rankrightsBooleans.premove.active);
		rankrightsBooleans.setnote.active = DrawToggleButton(checkButton5Rect, rankrightsBooleans.setnote.active);
		
		rankrightsBooleans.setmotd.active = DrawToggleButton(checkButton6Rect, rankrightsBooleans.setmotd.active);
		rankrightsBooleans.setinfo.active = DrawToggleButton(checkButton7Rect, rankrightsBooleans.setinfo.active);
		rankrightsBooleans.withdrawgold.active = DrawToggleButton(checkButton8Rect, rankrightsBooleans.withdrawgold.active);
		rankrightsBooleans.withdrawrepair.active = DrawToggleButton(checkButton9Rect, rankrightsBooleans.withdrawrepair.active);
		rankrightsBooleans.all.active = DrawToggleButton(checkButton0Rect, rankrightsBooleans.all.active);		
		
		GUI.Label(checkButton1Label, "UPGRADE", whiteMLStyle);
		GUI.Label(checkButton2Label, "DEGRADE", whiteMLStyle);
		GUI.Label(checkButton3Label, "INVITE", whiteMLStyle);
		GUI.Label(checkButton4Label, "KICK", whiteMLStyle);
		GUI.Label(checkButton5Label, "MAKE NOTE", whiteMLStyle);
		
		GUI.Label(checkButton6Label, "MAKE GUILD MESSAGE", whiteMLStyle);
		GUI.Label(checkButton7Label, "MAKE GUILD INFO", whiteMLStyle);
		GUI.Label(checkButton8Label, "WITHDRAW GOLD", whiteMLStyle);
		GUI.Label(checkButton9Label, "GOLD FOR REPAIR", whiteMLStyle);
		GUI.Label(checkButton0Label, "ALL PERMISSIONS", whiteMLStyle);
		
		guild.guildAtlasToolkit.DrawTexture(nextPageRect, "ArrowDown.png");
		if(GUI.Button(nextPageRect, "", GUIStyle.none))
		{
			guild.SetManageWindowPage(ManageRankPage.SECOND_PAGE);
		}
	}
	
	private void  ShowSecondWindow ()
	{
		GUIAtlas.interfMix3.DrawTexture(windowDecorationRect, "decoration6.png");
		guild.guildAtlasToolkit.DrawTexture(moneyTextBoxRect, "ShortTextBox.png");
		GUI.Label(rankLabelRect, "GOLD PER DAY:", whiteMCStyle);
		goldPerDayText = GUI.TextArea(rankTextBox, goldPerDayText, blackMLStyle);
		
		guild.guildAtlasToolkit.DrawTexture(previousPageRect, "ArrowUp.png");
		if(GUI.Button(previousPageRect, "", GUIStyle.none))
		{
			mainPlayer.guildUI.SetManageWindowPage(ManageRankPage.FIRST_PAGE);
		}
		
		for(byte index = 0; index < mainPlayer.guild.purchasedTabs; index++)
		{
			guild.guildAtlasToolkit.DrawTexture(bankSlotRect[index], bankSlotImageName[index]);
			if(GUI.Button(bankSlotRect[index], "", GUIStyle.none))
			{
				GetBankSlotInfo(index);
				selectedSlotName = "Current Bank slot - " + (index + 1);
			}
		}
		
		if(mainPlayer.guild.purchasedTabs > 0)
		{
			GUI.Label(selectedSlotRect, selectedSlotName, whiteMLStyle);
			
			bankTabs.guildTabRight[indexTab].view.active = DrawToggleButton(checkButton11Rect, bankTabs.guildTabRight[indexTab].view.active);
			bankTabs.guildTabRight[indexTab].deposit.active = DrawToggleButton(checkButton12Rect, bankTabs.guildTabRight[indexTab].deposit.active);
			bankTabs.guildTabRight[indexTab].updatetext.active = DrawToggleButton(checkButton13Rect, bankTabs.guildTabRight[indexTab].updatetext.active);
			
			GUI.Label(checkButton11Label, "TAB VIEW", whiteMLStyle);
			GUI.Label(checkButton12Label, "TAB DEPOSIT", whiteMLStyle);
			GUI.Label(checkButton13Label, "UPDATE TEXT", whiteMLStyle);
			
			guild.guildAtlasToolkit.DrawTexture(goldTextFieldRect, "ShortTextBox.png");
			GUI.Label(repairGoldLabel, "WITHDRAWAL PER DAY", whiteMLStyle);
			
			itemsPerSlot = GUI.TextArea(goldTextField, itemsPerSlot, blackMLStyle);
		}
		
		guild.guildAtlasToolkit.DrawTexture(saveButtonRect, "SaveButton.png");
		if(GUI.Button(saveButtonRect, "", GUIStyle.none))
		{
			SaveOptions();
		}
		
		guild.guildAtlasToolkit.DrawTexture(exitButtonRect, "ExitButton.png");
		if(GUI.Button(exitButtonRect, "", GUIStyle.none))
		{
			ExitOptions();
		}
	}
	
	private void  GetBankSlotInfo ( uint slotNumber )
	{
		bankTabs.guildTabSlotPerDay[indexTab] = itemsPerSlot;
		indexTab = slotNumber;
		itemsPerSlot = bankTabs.guildTabSlotPerDay[slotNumber];
	}
	
	
	private bool DrawToggleButton ( Rect positionRect ,   bool flag )
	{
		if(flag)
		{
			guild.guildAtlasToolkit.DrawTexture(positionRect, "CheckBoxChecked.png");
		}
		else
		{
			guild.guildAtlasToolkit.DrawTexture(positionRect, "CheckBoxUnchecked.png");
		}
		
		if(GUI.Button(positionRect, "", GUIStyle.none))
		{
			flag = !flag;
		}
		
		return flag;
	}
	
	private void  SaveOptions ()
	{
		bankTabs.guildTabSlotPerDay[indexTab] = itemsPerSlot;
		GuildRankRights rankRights = rankrightsBooleans.GetRightsTogether();
		rank.Name = rankName;
		rank.Rights = rankRights;
		
		if(rank.BankMoneyPerDay != 4294967295)
		{
			rank.BankMoneyPerDay = System.Convert.ToUInt32(goldPerDayText);
		}
		
		bankTabs.SaveRights(rank.TabRight, rank.TabSlotPerDay);
		
		mainPlayer.guild.SendRankUpdate(mainPlayer.guildUI.rankId, rank);
		selectedSlotName = "Current Bank slot - 1";
		
		ExitOptions();
	}
	
	private void  ExitOptions ()
	{
		loadData = true;
		selectedSlotName = "Current Bank slot - 1";
		mainPlayer.guildUI.ChangeGuildWindowState(GuildWindowState.MANAGE_WINDOW);
	}
}