using UnityEngine;
using System.Collections.Generic;

public enum AddWindowType
{
	ADD_PLAYER			=	1,
	ADD_RANK			=	2,
	CHANGE_LEADER		=	3
};

public enum ManageRankPage
{
	FIRST_PAGE			=	1,
	SECOND_PAGE			=	2
};


public enum GuildWindowState
{
	MAIN_WINDOW			=	1,
	MESSAGE_WINDOW		= 	2,
	PLAYER_INFO			=	3,
	LOG_WINDOW			=	4,
	ADD_WINDOW			=	5,
	KICK_WINDOW			=	6,
	MANAGE_WINDOW		=	7,
	CONFIRM_WINDOW		=	8,
	MANAGE_SETTING		=	9
};

public enum GuildConfirmState
{
	KICK_PLAYER			=	1,
	LEAVE_GUILD			=	2,
	REMOVE_RANK			=	3,
	CHANGE_GUILD_LD		=	4
};


public class GuildUI
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	public string guildNameInvited;
	private UITextInstance playerText;
	public bool  boolShowingGuild = false;
	private Vector2 _guildScrollVector2;
	private Vector2 _logScrollVector2;
	private Vector2 _rankScrollVector2;
	private MessageWnd messageWnd = new MessageWnd();
	private bool  WindowActive = false;
	public MemberSlot member;
	public int memberID = 999;
	//private GUIStyle invisibleStyle = newskin.customStyles[5];  InvisibleStyle
	public bool  notinGuild = false;
	private MainPlayer mainPlayer;
	private string CharName = "";
	private string RankName;
	private string strNote;
	private string strMotd;
	private string strInfo;
	private GUISkin newskin;
	private GUIStyle motdStyle;
	
	public RankInfo rank;
	private string strRankName;
	public int rankId = 999;
	private uint rankRights;
	private string rankMoneyPerDay;
	public bool  showInvitationGuid = false;
	
	private UISprite invitationBackground;
	private UISprite invitationDecoration;
	private UIButton invitationAccept;
	private UIButton invitationDecline;
	private UIButton invitationClose;
	private UITextInstance invitationText;
	private UIAbsoluteLayout invitationContainer;
	
	
	
	private rankBankRightsBool rankTabs = new rankBankRightsBool();
	private uint indexTabChose = 0;
	
	//GuildBankRights[] guildTabRight = new GuildBankRights[SharedDefines.GUILD_BANK_MAX_TABS];
	//uint[] guildTabSlotPerDay = new uint[SharedDefines.GUILD_BANK_MAX_TABS];
	
	
	private rankRightsBool rankrightsBooleans = new rankRightsBool();
	
	/*************************************************************************************************/
	
	private GuildMainWindow guildMainWindow = new GuildMainWindow();
	private GuildAddWindow guildAddWindow = new GuildAddWindow();
	private GuildMessageWindow guildMessageWindow = new GuildMessageWindow();
	private GuildPlayerInfoWindow guildPlayerInfoWindow = new GuildPlayerInfoWindow();
	private GuildManageWindow guildManageWindow = new GuildManageWindow();
	private GuildManageRankWindow guildManageRankWindow = new GuildManageRankWindow();
	private GuildConfirmWindow guildConfirmWindow = new GuildConfirmWindow();
	private GuildLogWindow guildLogWindow = new GuildLogWindow();
	
	private Rect backgroudRect = new Rect(0, Screen.height * 0.1f, Screen.width, Screen.height * 0.9f);
	public GuildWindowState windowState = GuildWindowState.MAIN_WINDOW;
	
	public AddWindowType addWindowState = AddWindowType.ADD_PLAYER;
	public ManageRankPage manageWindowPage = ManageRankPage.FIRST_PAGE;
	public GuildConfirmState confirmWindowPage = GuildConfirmState.KICK_PLAYER;
	
	public GUISkin guildSkin;
	
	public TexturePacker guildAtlasToolkit;
	private Texture2D backgroundTexture;
	
	public Texture2D mainDecoration;
	public Texture2D textDecoration;

	public Texture2D logDecoration;
	public Texture2D manageDecoration;
	
	public GuildUI ()
	{
		guildSkin = Resources.Load<GUISkin>("GUI/GuildSkin");
		//guildSkin.label.fontSize = Screen.height * 0.03f;
		
		newskin = (dbs._sizeMod == 2)? Resources.Load<GUISkin>("GUI/Skins_2x") : Resources.Load<GUISkin>("GUI/Skins");
		motdStyle = newskin.customStyles[4];
	}
	
	private void  setMotdStyle (  int length)
	{
		if ( length > 30 )
			motdStyle = newskin.customStyles[7];
		else
			motdStyle = newskin.customStyles[4];
	}
	
	public void  InitGuildAtlas ()
	{
		guildAtlasToolkit = new TexturePacker("GuildAtlas");
		backgroundTexture = Resources.Load<Texture2D>("GUI/InGame/basic_background");
		
		mainDecoration = Resources.Load<Texture2D>("GUI/BigTextures/DialogScreenBackground");
		textDecoration = Resources.Load<Texture2D>("GUI/BigTextures/InfoScreenBackground");
		
		logDecoration = Resources.Load<Texture2D>("GUI/BigTextures/LongDecorations/Decoration_1");
		manageDecoration = Resources.Load<Texture2D>("GUI/BigTextures/LongDecorations/Decoration_2");
	}
	
	public void  RemoveGuildAtlas ()
	{
		guildAtlasToolkit.Destroy();
		guildAtlasToolkit = null;
		
		mainDecoration = null;
		textDecoration = null;
		
		logDecoration = null;
		manageDecoration = null;
	}
	
	public void  showGuild ( MainPlayer mplayer )
	{
		if(boolShowingGuild)// only one initialization
		{
			windowState = GuildWindowState.MAIN_WINDOW;
			mainPlayer = mplayer;
			boolShowingGuild = false;
			mplayer.guildmgr.RequestRanksInfo(mplayer);
			mplayer.guildmgr.GuildRoster();
		}
		
		GUI.DrawTexture(backgroudRect, backgroundTexture);
		
		if(!mplayer.IsInGuild())
		{
			GUI.DrawTexture( new Rect(sw * 0.2f, sh * 0.2f, sw * 0.6f, sh * 0.6f), mainDecoration);
			GUI.Label( new Rect(sw * 0.2f, sh * 0.2f, sw * 0.6f, sh * 0.6f),"You are not in Guild.", guildSkin.GetStyle("GuildName"));
		}
		else if(mplayer.guild.FindMember(mplayer.guid) != null)
		{			
			switch(windowState)
			{
			case GuildWindowState.MAIN_WINDOW:		// Guild Main Window
				guildMainWindow.Show();
				break;
			case GuildWindowState.MESSAGE_WINDOW:	// Message Window
				guildMessageWindow.Show();
				break;
			case GuildWindowState.PLAYER_INFO:		// Info Window
				guildPlayerInfoWindow.Show();
				break;
			case GuildWindowState.LOG_WINDOW:		// Log Window
				guildLogWindow.Show();
				break;
			case GuildWindowState.ADD_WINDOW:		// Add Window
				guildAddWindow.Show();
				break;
			case GuildWindowState.KICK_WINDOW:		// Kick Window
				break;
			case GuildWindowState.MANAGE_WINDOW:	// Manage Window
				guildManageWindow.Show();
				break;
			case GuildWindowState.CONFIRM_WINDOW:	// Confirm Window
				guildConfirmWindow.Show();
				break;
			case GuildWindowState.MANAGE_SETTING:	//
				guildManageRankWindow.Show();
				break;
			}
		}
	}
	
	public void  ChangeGuildWindowState ( GuildWindowState newState )
	{	
		windowState = newState;
	}
	
	public void  SetAddWindowState ( AddWindowType newState )
	{
		addWindowState = newState;
	}
	
	public void  SetManageWindowPage ( ManageRankPage newState )
	{
		manageWindowPage = newState;
	}
	
	public void  SetConfirmWindowState ( GuildConfirmState newState )
	{
		confirmWindowPage = newState;
	}
	
	public void  SetMember ( MemberSlot newMember, int slot )
	{
		member = newMember;
		memberID = slot;
	}	
	
	//	private function changeGuildMaster(  messageWnd mW)
	//	{
	//		GUI.Label( new Rect(mW.rect.width*0.05f,mW.rect.height*0,mW.rect.width*0.9f,mW.rect.height*0.6f), mW.text, mainPlayer.newskin.customStyles[4] );
	//		if(GUI.Button( new Rect(mW.rect.width*0.65f,mW.rect.height*0.80f,mW.rect.width*0.33f-10,mW.rect.height*0.12f),"Ok", mainPlayer.guildskin.button))
	//		{
	//			//WindowActive = false;
	//			mainPlayer.guildmgr.ChangeLeader( member.Name );
	//			messageWnd.OnGUI = memberWindow;
	//		}
	//		if(GUI.Button( new Rect(mW.rect.width*0.05f,mW.rect.height*0.80f,mW.rect.width*0.33f-10,mW.rect.height*0.12f),"Cancel", mainPlayer.guildskin.button))
	//		{
	//			//WindowActive = false;
	//			messageWnd.OnGUI = memberWindow;
	//		}
	//	}
	
	public void  showWindow (  string text){
		messageWnd.resetValues();
		messageWnd.text = text;
		messageWnd.OnGUI = null;
		WindowActive = true;
	}
	
	public void UpdateMember( List<MemberSlot> guildMembers )
	{
		if ( member != null )
		{
			for(int i=0;i<guildMembers.Count;i++)
			{
				if ( guildMembers[i].Name == member.Name )
					member = guildMembers[i];
			}
		}
		if ( mainPlayer.guild.MOTD != null )
			setMotdStyle( mainPlayer.guild.MOTD.Length );
	}
	
	public void  ShowInviteWindow (  MainPlayer player)
	{
		if ( mainPlayer == null )
			mainPlayer = player;
		
		DestroyInvitationUI();
		invitationContainer = new UIAbsoluteLayout();
		
		invitationBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.22f , 5);
		invitationBackground.setSize(Screen.width*0.6f , Screen.height*0.45f);
		
		invitationDecoration = UIPrime31.questMix.addSprite("party_decoration.png" , invitationBackground.position.x + invitationBackground.width*0.05f , -invitationBackground.position.y + invitationBackground.height*0.15f , 4);
		invitationDecoration.setSize(invitationBackground.width*0.9f , invitationBackground.height*0.55f);
		
		invitationAccept = UIButton.create(UIPrime31.questMix , "accept_button.png" , "accept_button.png" , invitationBackground.position.x + invitationBackground.width*0.1f , -invitationBackground.position.y + invitationBackground.height*0.75f , 4);
		invitationAccept.setSize(invitationBackground.width*0.3f , invitationBackground.height*0.16f);
		invitationAccept.onTouchDown += InvitationAcceptDelegate;
		
		invitationDecline = UIButton.create(UIPrime31.questMix , "decline_button.png" , "decline_button.png" , invitationBackground.position.x + invitationBackground.width*0.6f , -invitationBackground.position.y + invitationBackground.height*0.75f , 4);
		invitationDecline.setSize(invitationBackground.width*0.3f , invitationBackground.height*0.16f);
		invitationDecline.onTouchDown += InvitationDeclineDelegate;
		
		invitationClose = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		invitationClose.setSize(invitationBackground.height*0.2f , invitationBackground.height*0.2f);
		invitationClose.position = new Vector3( invitationBackground.position.x + invitationBackground.width - invitationClose.width/1.5f , invitationBackground.position.y + invitationClose.height/3 , 4);
		invitationClose.onTouchDown += InvitationDeclineDelegate;
		
		invitationContainer.addChild(invitationBackground , invitationDecoration , invitationAccept , invitationDecline , invitationClose);
		
		invitationText = mainPlayer.interf.text3.addTextInstance("You have been invited\nto join " + guildNameInvited , invitationBackground.position.x + invitationBackground.width * 0.53f , -invitationBackground.position.y + invitationBackground.height*0.1f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
	}
	
	
	void  DestroyInvitationUI ()
	{
		if(invitationContainer != null)
		{
			invitationContainer.removeAllChild(true);
			invitationContainer = null;
		}
		if(invitationText != null)
		{
			invitationText.clear();
			invitationText = null;
		}
	}
	
	void  InvitationAcceptDelegate ( UIButton sender )
	{
		mainPlayer.guildmgr.AcceptInvite();
		DestroyInvitationUI();
		guildNameInvited = "";
	}
	
	void  InvitationDeclineDelegate ( UIButton sender )
	{
		mainPlayer.guildmgr.DeclineInvite();
		DestroyInvitationUI();
		guildNameInvited = "";
	}
}