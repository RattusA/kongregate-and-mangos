﻿
public class RankInfo
{
	public string Name;
	public GuildRankRights Rights; // uint
	public uint BankMoneyPerDay;
	public uint[] TabRight = new uint[ SharedDefines.GUILD_BANK_MAX_TABS ];
	public uint[] TabSlotPerDay = new uint[ SharedDefines.GUILD_BANK_MAX_TABS ];

	public RankInfo ( string _name )
	{
		Name = _name;
		Rights = GuildRankRights.GR_RIGHT_EMPTY;
		BankMoneyPerDay = 0;
		InitTabs();
	}
	
	public RankInfo ( GuildRankRights _rights, uint _money )
	{
		Name = "NS";
		Rights = _rights;
		BankMoneyPerDay = _money;
		InitTabs();
	}
	
	public void  UpdateInfo ( GuildRankRights _rights, uint _money )
	{
		Rights = _rights;
		BankMoneyPerDay = _money;
	}
	
	private void  InitTabs ()
	{
		for(int i= 0; i < SharedDefines.GUILD_BANK_MAX_TABS; ++i)
		{
			TabRight[i] = 0;
			TabSlotPerDay[i] = 0;
		}
	}
}