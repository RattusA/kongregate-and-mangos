﻿using UnityEngine;
using System.Collections.Generic;

public class Guild
{
	public class GuildBankTab
	{
		public Item[] Slots;
		public string Name;
		public string Icon;
		public string Text;
		public GuildBankTab ()
		{
			Slots = new Item[SharedDefines.GUILD_BANK_MAX_SLOTS];
			//for(int i=0;i<SharedDefines.GUILD_BANK_MAX_SLOTS;i++)
			//	Slots[i] = new Item();
		}
	}
	
	public uint m_Id;
	public string Name = "";
	ulong m_LeaderGuid;
	public string MOTD;
	public string GINFO;
	uint m_CreatedDate;
	
	
	public List<RankInfo> m_Ranks = new List<RankInfo>();  // RankList
	public List<MemberSlot> members = new List<MemberSlot>();  // MemberList
	public List<EventLog> events = new List<EventLog>();  // Events log
	
	public List<GuildBankEventLogEntry> bankEvents = new List<GuildBankEventLogEntry>(); // bank events
	public byte bankEventsTab;
	
	//byte tabSelected;
	public string tabText = "";
	
	// bank shit:
	
	public List<GuildBankTab> m_TabListMap = new List<GuildBankTab>();
	public byte purchasedTabs;
	public byte selectedTab = 254;
	public uint remainingSlotsForToday;
	
	uint m_GuildEventLogNextGuid;
	uint m_GuildBankEventLogNextGuid_Money;
	uint[] m_GuildBankEventLogNextGuid_Item = new uint[SharedDefines.GUILD_BANK_MAX_TABS];
	
	public ulong m_GuildBankMoney;
	public uint remainingMoneyForToday = 0;
	
	public uint getMyRank (  Player mplayer )
	{
		uint rankid = mplayer.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_GUILDRANK);
		return rankid;
	}
	
	public void  initTabs ()
	{
		m_TabListMap.Clear();
		for(int i=0;i<SharedDefines.GUILD_BANK_MAX_TABS;i++)
		{
			m_TabListMap.Add(new GuildBankTab());
		}
	}
	
	/*
	    typedef std::vector<GuildBankTab*> TabListMap;
        TabListMap m_TabListMap;

        /** These are actually ordered lists. The first element is the oldest entry.
        typedef std::list<GuildEventLogEntry> GuildEventLog;
        typedef std::list<GuildBankEventLogEntry> GuildBankEventLog;
        GuildEventLog m_GuildEventLog;
        GuildBankEventLog m_GuildBankEventLog_Money;
        GuildBankEventLog m_GuildBankEventLog_Item[GUILD_BANK_MAX_TABS];

        uint32 m_GuildEventLogNextGuid;
        uint32 m_GuildBankEventLogNextGuid_Money;
        uint32 m_GuildBankEventLogNextGuid_Item[GUILD_BANK_MAX_TABS];

        uint64 m_GuildBankMoney;
	*/
	
	public MemberSlot FindMember ( ulong guid )
	{
		for(int i=0;i<members.Count;i++)
		{
			if(members[i].guid == guid)
			{
				return members[i];
			}
		}
		return null;
	}
	
	public GuildRankRights GetRightsFromRankId ( uint rankId )
	{
		if ( m_Ranks.Count <= rankId )
		{
			return GuildRankRights.GR_RIGHT_EMPTY;
		}
		return m_Ranks[ (int)rankId ].Rights;
	}
	
	public MemberSlot GetMemberSlot ( string name )
	{
		for(int i=0;i<members.Count;i++)
		{
			if(members[i].Name == name)
				return members[i];
		}
		Debug.Log("Member " + name + " not found");
		return null;
	}
	
	public string GetRankAsString ( uint id )
	{
		if ( id >= m_Ranks.Count )
			return null;
		return m_Ranks[(int)id].Name;
	}
	
	void  LogAllMembers ()
	{
		for(int i=0;i<members.Count;i++)
			Debug.Log("Member: " + members[i].Name);
	}
	
	public RankInfo getRankByIndex ( int index )
	{
		if ( m_Ranks.Count > index )
			return m_Ranks[index];
		
		return null;
	}
	
	public void  setInfo ( string name, uint created )
	{
		this.Name = name;
		this.m_CreatedDate = created;
	}
	
	public void  setMotd ( string motd, string info )
	{
		this.MOTD = motd;
		this.GINFO = info;
	}
	
	void  EditRankByIndex ( int index, string newName )
	{
		RankInfo rank = getRankByIndex( index );
		if ( rank == null )
		{
			Debug.Log("Invalid index");
			return;
		}
		rank.Name = newName;
		//SendRankUpdate( index );
	}
	
	public void  SendRankUpdate (int index, RankInfo rank )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_RANK);
		pkt.Append( ByteBuffer.Reverse( index ) );  // recvPacket >> rankId;
		pkt.Append( ByteBuffer.Reverse( (uint)rank.Rights ) );
		pkt.Append( rank.Name );
		pkt.Append( ByteBuffer.Reverse( rank.BankMoneyPerDay ) );
		for(int i=0;i<SharedDefines.GUILD_BANK_MAX_TABS;i++)
		{
			//Debug.Log("R: " + rank.TabRight[i] + " - " + rank.TabSlotPerDay[i]);
			pkt.Append( ByteBuffer.Reverse( rank.TabRight[i] ) );
			pkt.Append( ByteBuffer.Reverse( rank.TabSlotPerDay[i] ) );
		}
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SortMembersByRank ()
	{
		members.Sort( HigherRank );
	}
	
	private int  HigherRank ( MemberSlot mem1, MemberSlot mem2 )
	{
		int ret = 1;
		if ( mem1.RankId > mem2.RankId )
		{
			ret = -1;
		}
		else
		{
			if ( mem1.RankId == mem2.RankId )
			{
				int comp = mem1.Name.CompareTo( mem2.Name );
				ret = -comp;
			}
		}
		return ret;
	}
	
	/*
		uint32 m_Id;
        std::string m_Name;
        ObjectGuid m_LeaderGuid;
        std::string MOTD;
        std::string GINFO;
        time_t m_CreatedDate;

        uint32 m_EmblemStyle;
        uint32 m_EmblemColor;
        uint32 m_BorderStyle;
        uint32 m_BorderColor;
        uint32 m_BackgroundColor;
        uint32 m_accountsNumber;                            // 0 used as marker for need lazy calculation at request

        RankList m_Ranks;

        MemberList members;

        typedef std::vector<GuildBankTab*> TabListMap;
        TabListMap m_TabListMap;

        ///** These are actually ordered lists. The first element is the oldest entry.
        typedef std::list<GuildEventLogEntry> GuildEventLog;
        typedef std::list<GuildBankEventLogEntry> GuildBankEventLog;
        GuildEventLog m_GuildEventLog;
        GuildBankEventLog m_GuildBankEventLog_Money;
        GuildBankEventLog m_GuildBankEventLog_Item[GUILD_BANK_MAX_TABS];

        uint32 m_GuildEventLogNextGuid;
        uint32 m_GuildBankEventLogNextGuid_Money;
        uint32 m_GuildBankEventLogNextGuid_Item[GUILD_BANK_MAX_TABS];

        uint64 m_GuildBankMoney;

	*/
}