using UnityEngine;
using System;

public class GuildManager : MonoBehaviour
{
	void  SendPetitionBuy (  ulong guid, string name )
	{
		byte unk8 = 0;
		uint unk32 = 0;
		ulong unk64 = 0;
		string unkStr = "";
		uint clientIndex = 1;
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_PETITION_BUY);
		pkt.Append( ByteBuffer.Reverse( guid ) );
		pkt.Append( ByteBuffer.Reverse( unk32 ) );
		pkt.Append( ByteBuffer.Reverse( unk64 ) );
		pkt.Append( name );
		pkt.Append( unkStr );
		for(int i = 0; i < 7; i++)
		{
			pkt.Append( ByteBuffer.Reverse( unk32 ) );
		}
		pkt.Append( unk8 );
		pkt.Append( unk8 );
		pkt.Append( ByteBuffer.Reverse( unk32 ) );
		pkt.Append( ByteBuffer.Reverse( unk32 ) );
		pkt.Append( ByteBuffer.Reverse( unk32 ) );
		
		RealmSocket.outQueue.Add(pkt);
		
		for(int i = 0; i < 10; i++)
		{
			pkt.Append( unkStr );
		}
		pkt.Append( ByteBuffer.Reverse( clientIndex ) );
		pkt.Append( ByteBuffer.Reverse( unk32 ) );
		
	}
	
	void  SendPetitionShowList (  ulong guid )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_PETITION_SHOWLIST);
		pkt.Append( ByteBuffer.Reverse( guid ) );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SetGuildBankTabText (  byte tabId ,   string text )
	{
		//CMSG_SET_GUILD_BANK_TEXT
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_SET_GUILD_BANK_TEXT);
		pkt.Append( tabId );
		pkt.Append( text );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  QueryGuildBankText (  byte tabId )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.MSG_QUERY_GUILD_BANK_TEXT);
		pkt.Append( tabId );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  QueryGuildBankMoneyWithdrawn ()
	{
		// MSG_GUILD_BANK_MONEY_WITHDRAWN
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.MSG_GUILD_BANK_MONEY_WITHDRAWN);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  QueryGuildBankLog (  byte tabId )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.MSG_GUILD_BANK_LOG_QUERY);
		pkt.Append( tabId );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  GuildBankWithdrawMoney (  ulong goGuid ,   int money )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_BANK_WITHDRAW_MONEY);
		pkt.Append( ByteBuffer.Reverse( goGuid ) );
		pkt.Append( ByteBuffer.Reverse( money ) );
		
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  GuildBankDepositMoney (  ulong goGuid ,   int money )
	{
		Debug.Log("SEND MONEY DEPOSIT :\n GUILDgUID : " + goGuid + "\nMONEY : " + money);
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_BANK_DEPOSIT_MONEY);
		pkt.Append( ByteBuffer.Reverse( goGuid ) );
		pkt.Append( ByteBuffer.Reverse( money ) );
		
		RealmSocket.outQueue.Add(pkt);
	}
	
	void  GuildBankUpdateTab (  ulong goGuid ,   byte tabId ,   string Name ,   string IconIndex )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_BANK_UPDATE_TAB);
		pkt.Append( ByteBuffer.Reverse( goGuid ) );
		pkt.Append( tabId );
		pkt.Append( Name );
		pkt.Append( IconIndex );
		
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendGuildBankBuyTab (  ulong goGuid ,   byte tabId )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_BANK_BUY_TAB);
		pkt.Append( ByteBuffer.Reverse( goGuid ) );
		pkt.Append( tabId );
		
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void SendGuildSwapItemsBankChar(  ulong goGuid ,   byte bankTab ,   byte bankTabSlot ,   byte playerBag ,   byte playerSlot ,   uint itemEntry ,   uint splitedAmount ,   byte toChar )// from bank to char - 1 or char to bank - 0
	{
		byte bankToBank = 0;
		byte autoStore = 0;
		//byte toChar = 1;

		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_BANK_SWAP_ITEMS);
		pkt.Append( ByteBuffer.Reverse( goGuid ) );
		pkt.Append( bankToBank );
		
		pkt.Append( bankTab );
		pkt.Append( bankTabSlot );
		pkt.Append( ByteBuffer.Reverse( itemEntry ) );
		pkt.Append( autoStore );
		
		pkt.Append( playerBag );
		pkt.Append( playerSlot );
		pkt.Append( toChar );
		pkt.Append( ByteBuffer.Reverse( splitedAmount ) );
		
		RealmSocket.outQueue.Add(pkt);
	}
	
	void SendGuildSwapItemsBankToCharAuto(  ulong goGuid ,   byte bankTab ,   byte bankTabSlot ,   uint itemEntry ,   uint autoStoreCount )  // from bank to char
	{
		byte bankToBank = 0;
		byte autoStore = 1;
		byte toChar = 1;
		uint unk = 0;
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_BANK_SWAP_ITEMS);
		pkt.Append( ByteBuffer.Reverse( goGuid ) );
		pkt.Append( bankToBank );
		
		pkt.Append( bankTab );
		pkt.Append( bankTabSlot );
		pkt.Append( ByteBuffer.Reverse( itemEntry ) );
		pkt.Append( autoStore );
		pkt.Append( ByteBuffer.Reverse( autoStoreCount ) );
		pkt.Append( toChar );
		pkt.Append( ByteBuffer.Reverse( unk ) );
		
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendGuildSwapItemsBankToBank (  ulong goGuid ,   byte bankTab ,   byte bankTabSlot  ,   byte bankTabDst ,   byte bankTabSlotDst ,   uint itemEntry ,   uint splitedAmount )
	{
		byte bankToBank = 1;
		uint unk1 = 0;
		byte unk2 = 0;
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_BANK_SWAP_ITEMS);
		pkt.Append( ByteBuffer.Reverse( goGuid ) );
		pkt.Append( bankToBank );
		
		pkt.Append( bankTabDst );
		pkt.Append( bankTabSlotDst );
		pkt.Append( ByteBuffer.Reverse( unk1 ) );
		pkt.Append( bankTab );
		pkt.Append( bankTabSlot );
		pkt.Append( ByteBuffer.Reverse( itemEntry ) );
		pkt.Append( unk2 );
		pkt.Append( ByteBuffer.Reverse( splitedAmount ) );
		
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  QueryGuildTab (  ulong objguid, byte tabid )
	{
		Debug.Log("guild tab");
		byte unk = 0;
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_BANK_QUERY_TAB);
		pkt.Append( ByteBuffer.Reverse( objguid ) );
		pkt.Append( tabid );
		pkt.Append( unk );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  ActivateGuildBanker (  ulong objguid )
	{
		Debug.Log("Sent CMSG_GUILD_BANKER_ACTIVATE");
		byte unk = 0;
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_BANKER_ACTIVATE);
		pkt.Append( ByteBuffer.Reverse( objguid ) );
		pkt.Append( unk );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  RequestGuildEventLog ()
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.MSG_GUILD_EVENT_LOG_QUERY);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public bool CheckRights (  MainPlayer mplayer ,   GuildRankRights right )
	{
		MemberSlot member = mplayer.guild.GetMemberSlot( mplayer.name );
		if ( member == null )
			throw new InvalidOperationException("Player not found in guild!");
		if ( ( mplayer.guild.GetRightsFromRankId( member.RankId ) & right ) == right)
			return true;
		else
			return false;
	}
	
	void  SetOfficerNote (  string playerName ,   string pNote )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_SET_OFFICER_NOTE);
		pkt.Append( playerName );
		pkt.Append( pNote );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SetPlayerNote (  string playerName ,   string pNote )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_SET_PUBLIC_NOTE);
		pkt.Append( playerName );
		pkt.Append( pNote );
		RealmSocket.outQueue.Add(pkt);
		//Debug.Log("Set pNote: " + playerName + " note: " + pNote );
	}
	
	public void  DeleteLowestRank ()
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_DEL_RANK);
		RealmSocket.outQueue.Add(pkt);
		//CMSG_GUILD_DEL_RANK
	}
	
	public void  AddRank (  string name )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_ADD_RANK);
		pkt.Append( name );
		RealmSocket.outQueue.Add(pkt);
		//CMSG_GUILD_ADD_RANK
	}
	
	public void RequestRanksInfo(  Player mplayer )   // this gives also name of the guild that he is in
	{
		uint guildId = mplayer.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_GUILDID);
		//Debug.Log("Id guilda: " + guildId);
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_QUERY);
		pkt.Append( ByteBuffer.Reverse(guildId) );
		RealmSocket.outQueue.Add(pkt);
		//CMSG_GUILD_QUERY
	}
	
	public void  SetInfo (  string info )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_INFO_TEXT);
		pkt.Append( info );
		RealmSocket.outQueue.Add(pkt);
		//CMSG_GUILD_INFO_TEXT
	}
	
	public void  SetMOTD (  string motd )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_MOTD);
		pkt.Append( motd );
		RealmSocket.outQueue.Add(pkt);
		//CMSG_GUILD_MOTD
	}
	
	public void  ChangeLeader (  string name )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_LEADER);
		pkt.Append( name );
		RealmSocket.outQueue.Add(pkt);
		//CMSG_GUILD_LEADER
	}
	
	public void  DisbandGuild ()
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_DISBAND);
		RealmSocket.outQueue.Add(pkt);
		//CMSG_GUILD_DISBAND
	}
	
	public void  RemoveMember (  string name )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_REMOVE);
		pkt.Append( name );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendPromotion (  string name )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_PROMOTE);
		pkt.Append( name );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendDemote (  string name )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_DEMOTE);
		pkt.Append( name );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  LeaveGuild ()
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_LEAVE);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  CreateGuild (  string name )
	{
		Debug.Log(" Create: " + name );
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_CREATE);
		pkt.Append( name );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  InvitePlayer (  string name )
	{
		Debug.Log(" Invite: " + name );
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_INVITE);
		pkt.Append( name );
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  AcceptInvite ()
	{
		Debug.Log(" Accept");
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_ACCEPT);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  DeclineInvite ()
	{
		Debug.Log("Decline");
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_DECLINE);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  RequestInfo ()
	{
		Debug.Log(" Req info");
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_INFO);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void GuildRoster()  // get members and more info
	{
		//Debug.Log(" guild roster");
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GUILD_ROSTER);
		RealmSocket.outQueue.Add(pkt);
	}

	static public string GetGuildBankEventAsString (  GuildBankEventLogTypes eventId )
	{
		string eventStr  = "";
		switch( eventId )
		{
		case GuildBankEventLogTypes.GUILD_BANK_LOG_DEPOSIT_ITEM:
			eventStr = "has placed item ";
			break;
		case GuildBankEventLogTypes.GUILD_BANK_LOG_WITHDRAW_ITEM:
			eventStr = "has withdrawn item ";
			break;
		case GuildBankEventLogTypes.GUILD_BANK_LOG_MOVE_ITEM:
			eventStr = "has moved item ";
			break;
		case GuildBankEventLogTypes.GUILD_BANK_LOG_DEPOSIT_MONEY:
			eventStr = "has placed gold ";
			break;
		case GuildBankEventLogTypes.GUILD_BANK_LOG_WITHDRAW_MONEY:
			eventStr = "has withdrawn gold ";
			break;
		case GuildBankEventLogTypes.GUILD_BANK_LOG_REPAIR_MONEY:
			eventStr = "has taken gold for repairs ";
			break;
		case GuildBankEventLogTypes.GUILD_BANK_LOG_MOVE_ITEM2:
			eventStr = "has moved item";
			break;
		}
		
		return eventStr;
	}
	
	static public string GetEventAsString (  GuildEvents eventId )
	{
		string eventStr  = "";
		switch ( eventId )
		{
		case GuildEvents.GE_PROMOTION:
			eventStr = "has promoted ";
			break;
		case GuildEvents.GE_DEMOTION:
			eventStr = "has demoted ";
			break;
		case GuildEvents.GE_MOTD:
			eventStr = " has changed motd";
			break;
		case GuildEvents.GE_JOINED:
			eventStr = " has joined";
			break;
		case GuildEvents.GE_LEFT:
			eventStr = " has left";
			break;
		case GuildEvents.GE_REMOVED:
			eventStr = " has been removed";
			break;
		case GuildEvents.GE_LEADER_IS:
			eventStr = " is leader";
			break;
		case GuildEvents.GE_LEADER_CHANGED:
			eventStr = " leader changed";
			break;
		case GuildEvents.GE_DISBANDED:
			eventStr = " guild disbanded";
			break;
		case GuildEvents.GE_TABARDCHANGE:
			eventStr = " tabard change";
			break;
		case GuildEvents.GE_UNK1:
			eventStr = " ";
			break;
		case GuildEvents.GE_UNK2:
			eventStr = " ";
			break;
		case GuildEvents.GE_SIGNED_ON:
			eventStr = " signed on";
			break;
		case GuildEvents.GE_SIGNED_OFF:
			eventStr = " signed off";
			break;
		case GuildEvents.GE_GUILDBANKBAGSLOTS_CHANGED:
			eventStr = " guild bank bag slot changed";
			break;
		case GuildEvents.GE_BANKTAB_PURCHASED:
			eventStr = " bank tab purchased";
			break;
		case GuildEvents.GE_UNK5:
			eventStr = " ";
			break;
		case GuildEvents.GE_GUILDBANK_UPDATE_MONEY:
			eventStr = " guild bank update gold";
			break;
		case GuildEvents.GE_GUILD_BANK_MONEY_WITHDRAWN:
			eventStr = " bank gold withdrawn";
			break;
		case GuildEvents.GE_GUILDBANK_TEXT_CHANGED:
			eventStr = " bank text has changed";
			break;
		}
		return eventStr;
	}
	
	static public string GetCommandTypeAsString (  Typecommand typecmd )
	{
		string typecmd_str = "";
		switch ( typecmd )
		{
		case Typecommand.GUILD_CREATE_S:
			typecmd_str = "Create ";
			break;
		case Typecommand.GUILD_INVITE_S:
			typecmd_str = "Invite ";
			break;
		case Typecommand.GUILD_QUIT_S:
			typecmd_str = "Quit ";
			break;
		case Typecommand.GUILD_FOUNDER_S:
			typecmd_str = "Founder ";
			break;
		default:
			typecmd_str = "";
			break;
		};
		return typecmd_str;
	}
	
	static public string GetCommandErrorAsString (  CommandErrors cmdresult )
	{
		string errcmd_str = "";
		switch ( cmdresult )
		{
		case CommandErrors.ERR_PLAYER_NO_MORE_IN_GUILD:
			errcmd_str = " player no more in guild.";
			break;
		case CommandErrors.ERR_GUILD_INTERNAL:
			errcmd_str = " guild internal";
			break;
		case CommandErrors.ERR_ALREADY_IN_GUILD:
			errcmd_str = " already in guild";
			break;
		case CommandErrors.ERR_ALREADY_IN_GUILD_S:
			errcmd_str = " already in guild";
			break;
		case CommandErrors.ERR_INVITED_TO_GUILD:
			errcmd_str = " invited to guild";
			break;
		case CommandErrors.ERR_ALREADY_INVITED_TO_GUILD_S:
			errcmd_str = " already invited to guild";
			break;
		case CommandErrors.ERR_GUILD_NAME_INVALID:
			errcmd_str = " guild name invalid";
			break;
		case CommandErrors.ERR_GUILD_NAME_EXISTS_S:
			errcmd_str = " guild name already exists";
			break;
		case CommandErrors.ERR_GUILD_PERMISSIONS:
			errcmd_str = " guild permissions";
			break;
		case CommandErrors.ERR_GUILD_PLAYER_NOT_IN_GUILD:
			errcmd_str = " player not in guild";
			break;
		case CommandErrors.ERR_GUILD_PLAYER_NOT_IN_GUILD_S:
			errcmd_str = " player not in guild";
			break;
		case CommandErrors.ERR_GUILD_PLAYER_NOT_FOUND_S:
			errcmd_str = " player not found";
			break;
		case CommandErrors.ERR_GUILD_NOT_ALLIED:
			errcmd_str = " not allied";
			break;
		case CommandErrors.ERR_GUILD_RANK_TOO_HIGH_S:
			errcmd_str = " rank too high";
			break;
		case CommandErrors.ERR_GUILD_RANK_TOO_LOW_S:
			errcmd_str = " rank too low";
			break;
		case CommandErrors.ERR_GUILD_RANKS_LOCKED:
			errcmd_str = " ranks locked";
			break;
		case CommandErrors.ERR_GUILD_RANK_IN_USE:
			errcmd_str = " rank in use";
			break;
		case CommandErrors.ERR_GUILD_IGNORING_YOU_S:
			errcmd_str = " is ignoring you";
			break;
		case CommandErrors.ERR_GUILD_UNK1:
			errcmd_str = " ";
			break;
		case CommandErrors.ERR_GUILD_WITHDRAW_LIMIT:
			errcmd_str = " withdraw limit";
			break;
		case CommandErrors.ERR_GUILD_NOT_ENOUGH_MONEY:
			errcmd_str = " not enough gold";
			break;
		case CommandErrors.ERR_GUILD_BANK_FULL:
			errcmd_str = " bank full";
			break;
		case CommandErrors.ERR_GUILD_ITEM_NOT_FOUND:
			errcmd_str = " item not found";
			break;
			
		};
		return errcmd_str;
	}
	
	static public string getIntAsString (  uint mint )
	{
		if ( mint == 4294967295 )
			return "INF";
		return "" + mint;
	}
}