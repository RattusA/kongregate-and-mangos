
public class EventLog
{
	//ulong guid1;
	//ulong guid2;
	public MemberSlot member1;
	public MemberSlot member2;
	public GuildEventLogTypes eventType;
	public byte newrank;
	public uint eventTime;  // ago
	public string text;
	public void  updateText ( MainPlayer player )
	{
		switch ( eventType )
		{
		case GuildEventLogTypes.GUILD_EVENT_LOG_INVITE_PLAYER:
			text = member1.Name + " has invited " + member2.Name;
			break;
		case GuildEventLogTypes.GUILD_EVENT_LOG_JOIN_GUILD:
			text = member1.Name + " has joined guild";
			break;
		case GuildEventLogTypes.GUILD_EVENT_LOG_PROMOTE_PLAYER:
			text = member1.Name + " has promoted " + member2.Name + " to " + player.guild.GetRankAsString( newrank );
			break;
		case GuildEventLogTypes.GUILD_EVENT_LOG_DEMOTE_PLAYER:
			text = member1.Name + " has demoted " + member2.Name + " to " + player.guild.GetRankAsString( newrank );
			break;
		case GuildEventLogTypes.GUILD_EVENT_LOG_UNINVITE_PLAYER:
			text = member1.Name + " has kicked " + member2.Name;
			break;
		case GuildEventLogTypes.GUILD_EVENT_LOG_LEAVE_GUILD:
			text = member1.Name + " has left guild";
			break;
		default:
			text = eventType + " " + member1.Name;
			break;
		}
		text += " " + Global.GetTimeAsString( eventTime, false ) + " ago";
		/*
		enum GuildEventLogTypes
		{
		    GUILD_EVENT_LOG_INVITE_PLAYER     = 1,
		    GUILD_EVENT_LOG_JOIN_GUILD        = 2,
		    GUILD_EVENT_LOG_PROMOTE_PLAYER    = 3,
		    GUILD_EVENT_LOG_DEMOTE_PLAYER     = 4,
		    GUILD_EVENT_LOG_UNINVITE_PLAYER   = 5,
		    GUILD_EVENT_LOG_LEAVE_GUILD       = 6,
		};
		*/
	}
}
