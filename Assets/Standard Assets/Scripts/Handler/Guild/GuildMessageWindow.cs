﻿using UnityEngine;

public class GuildMessageWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Rect windowDecorationRect;
	
	private Rect guildMessageDecorationRect;
	private Rect guildMessageLabel;
	private Rect guildMessageLabelRect;
	private Rect guildMessageTextBoxRect;
	
	private Rect guildInfoDecorationRect;
	private Rect guildInfoLabel;
	private Rect guildInfoLabelRect;
	private Rect guildInfoTextBoxRect;
	
	private Rect exitButtonRect;
	private Rect saveButtonRect;
	
	private GUIStyle messageTextStyle = new GUIStyle();
	private GUIStyle messageTitleStyle = new GUIStyle();
	
	private string guildMessageText = "";
	private string guildInfoText = "";
	
	private bool  loadData = true;
	private GuildUI guild;
	
	/**
	 *	Guild message window
	 *	MenuState = 2
	 *		Two text box
	 *		1 - guild message text
	 *		2 - guild info text
	 **/
	
	public GuildMessageWindow ()
	{
		windowDecorationRect = new Rect(sw * 0.0625f, sh * 0.08333f, sw * 0.875f, sh * 0.83333f);
		guildMessageDecorationRect = new Rect(sw * 0.1125f, sh * 0.10833f, sw * 0.0625f, sh * 0.08333f);
		guildMessageLabel = new Rect(sw * 0.2f, sh * 0.10833f, sw * 0.6f, sh * 0.08333f);
		guildMessageLabelRect = new Rect(sw * 0.13f, sh * 0.22f, sw * 0.73f, sh * 0.2f);
		guildMessageTextBoxRect = new Rect(sw * 0.1125f, sh * 0.19167f, sw * 0.75f, sh * 0.25f);
		guildInfoDecorationRect = new Rect(sw * 0.1125f, sh * 0.46667f, sw * 0.0625f, sh * 0.08333f);
		guildInfoLabel = new Rect(sw * 0.2f, sh * 0.46667f, sw * 0.6f, sh * 0.08333f);
		guildInfoLabelRect = new Rect(sw * 0.13f, sh * 0.57f, sw * 0.72f, sh * 0.2f);
		guildInfoTextBoxRect = new Rect(sw * 0.1125f, sh * 0.55f, sw * 0.75f, sh * 0.25f);
		exitButtonRect = new Rect(sw * 0.5875f, sh * 0.81667f, sw * 0.275f, sh * 0.08333f);
		saveButtonRect = new Rect(sw * 0.1125f, sh * 0.81667f, sw * 0.275f, sh * 0.08333f);

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		messageTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.UpperLeft, sh * 0.045f, Color.black, true, FontStyle.Normal);
		messageTitleStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.06f, Color.white, false, FontStyle.Normal);
	}
	
	public void  Show ()
	{
		if(guild == null)
		{
			guild = WorldSession.player.guildUI;
			return;
		}
		
		if(loadData)
		{
			loadData = false;
			guildMessageText = WorldSession.player.guild.MOTD;
			if(guildMessageText == "\0")
			{
				guildMessageText = string.Empty;
			}
			
			guildInfoText = WorldSession.player.guild.GINFO;
			if(guildInfoText == "\0")
			{
				guildInfoText = string.Empty;
			}
		}
		
		GUI.DrawTexture(windowDecorationRect, guild.mainDecoration);
		
		GUIAtlas.interfMix3.DrawTexture(guildMessageDecorationRect, "zoom_in_button.png");
		GUI.Label(guildMessageLabel, "GUILD MESSAGE:", messageTitleStyle);
		guild.guildAtlasToolkit.DrawTexture(guildMessageTextBoxRect, "BigTextBox.png");
		if(WorldSession.player.guildmgr.CheckRights(WorldSession.player, GuildRankRights.GR_RIGHT_SETMOTD))
		{
			guildMessageText = GUI.TextArea(guildMessageLabelRect, guildMessageText, messageTextStyle);
		}
		else
		{
			GUI.Label(guildMessageLabelRect, guildMessageText, messageTextStyle);
		}
		
		GUIAtlas.interfMix3.DrawTexture(guildInfoDecorationRect, "zoom_in_button.png");
		GUI.Label(guildInfoLabel, "GUILD INFO:", messageTitleStyle);
		guild.guildAtlasToolkit.DrawTexture(guildInfoTextBoxRect, "BigTextBox.png");
		if(WorldSession.player.guildmgr.CheckRights(WorldSession.player, GuildRankRights.GR_RIGHT_MODIFY_GUILD_INFO))
		{
			guildInfoText = GUI.TextArea(guildInfoLabelRect, guildInfoText, messageTextStyle);
		}
		else
		{
			GUI.Label(guildInfoLabelRect, guildInfoText, messageTextStyle);
		}
		
		guild.guildAtlasToolkit.DrawTexture(saveButtonRect, "SaveButton.png");
		if(GUI.Button(saveButtonRect, "", GUIStyle.none))
		{
			SaveGuildMessage();
		}
		
		guild.guildAtlasToolkit.DrawTexture(exitButtonRect, "ExitButton.png");
		if(GUI.Button(exitButtonRect, "", GUIStyle.none))
		{
			CloseWindow();
		}
	}
	
	private void  CloseWindow ()
	{
		loadData = true;
		guildMessageText = "";
		guildInfoText = "";
		
		guild.ChangeGuildWindowState(GuildWindowState.MAIN_WINDOW);
	}
	
	private void  SaveGuildMessage ()
	{
		bool  updateMotd = WorldSession.player.guildmgr.CheckRights(WorldSession.player, GuildRankRights.GR_RIGHT_MODIFY_GUILD_INFO);
		bool  updateInfo = WorldSession.player.guildmgr.CheckRights(WorldSession.player, GuildRankRights.GR_RIGHT_SETMOTD);
		
		if(updateMotd && updateInfo)
		{
			WorldSession.player.guildmgr.SetMOTD(guildMessageText);
			WorldSession.player.guildmgr.SetInfo(guildInfoText);
		}
		
		CloseWindow();
	}
}