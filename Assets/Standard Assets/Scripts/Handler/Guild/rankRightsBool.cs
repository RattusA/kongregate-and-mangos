﻿
public class rankRightsBool
{
	public class Right
	{
		public bool  active;
		public GuildRankRights guildRankRight;
		
		public Right (  bool act, GuildRankRights right)
		{
			active = act;
			guildRankRight = right;
		}

		public void  UpdateActive (  GuildRankRights right)
		{
			if ( ( right & guildRankRight ) == guildRankRight )
				active = true;
			else
				active = false;
		}
	}
	
	public Right promote = new rankRightsBool.Right(false, GuildRankRights.GR_RIGHT_PROMOTE);
	public Right demote = new rankRightsBool.Right(false, GuildRankRights.GR_RIGHT_DEMOTE);
	public Right pinvite = new rankRightsBool.Right(false, GuildRankRights.GR_RIGHT_INVITE);
	public Right premove = new rankRightsBool.Right(false, GuildRankRights.GR_RIGHT_REMOVE);
	public Right setmotd = new rankRightsBool.Right(false, GuildRankRights.GR_RIGHT_SETMOTD);
	public Right setinfo = new rankRightsBool.Right(false, GuildRankRights.GR_RIGHT_MODIFY_GUILD_INFO);
	public Right setnote = new rankRightsBool.Right(false, GuildRankRights.GR_RIGHT_EPNOTE);
	public Right withdrawgold = new rankRightsBool.Right(false, GuildRankRights.GR_RIGHT_WITHDRAW_GOLD);
	public Right withdrawrepair = new rankRightsBool.Right(false, GuildRankRights.GR_RIGHT_WITHDRAW_REPAIR);
	Right gchatlisten =new rankRightsBool.Right (false, GuildRankRights.GR_RIGHT_GCHATLISTEN);
	Right gchatspeak =new rankRightsBool.Right (false, GuildRankRights.GR_RIGHT_GCHATSPEAK);
	Right offchatlisten =new rankRightsBool.Right (false, GuildRankRights.GR_RIGHT_OFFCHATLISTEN);
	Right offchatspeak =new rankRightsBool.Right (false, GuildRankRights.GR_RIGHT_OFFCHATSPEAK);
	public Right all = new rankRightsBool.Right(false, GuildRankRights.GR_RIGHT_ALL);

	public void  UpdateRights (  GuildRankRights right)
	{
		promote.UpdateActive(right);
		demote.UpdateActive(right);
		pinvite.UpdateActive(right);
		premove.UpdateActive(right);
		setmotd.UpdateActive(right);
		setinfo.UpdateActive(right);
		setnote.UpdateActive(right);
		withdrawgold.UpdateActive(right);
		withdrawrepair.UpdateActive(right);
		all.UpdateActive(right);
		gchatlisten.UpdateActive(right);
		gchatspeak.UpdateActive(right);
		offchatlisten.UpdateActive(right);
		offchatspeak.UpdateActive(right);
	}
	
	/*gchatlisten.guildRankRight;
	gchatspeak.guildRankRight;
	offchatlisten.guildRankRight;
	offchatspeak.guildRankRight;*/
	public GuildRankRights GetRightsTogether ()
	{
		GuildRankRights rights = GuildRankRights.GR_RIGHT_EMPTY;
		if ( promote.active )
		{
			rights = rights | promote.guildRankRight;
			rights = rights | gchatlisten.guildRankRight;
			rights = rights | gchatspeak.guildRankRight;
			rights = rights | offchatlisten.guildRankRight;
			rights = rights | offchatspeak.guildRankRight;
		}
		if ( demote.active )
		{
			rights = rights | demote.guildRankRight;
			rights = rights | promote.guildRankRight;
			rights = rights | gchatlisten.guildRankRight;
			rights = rights | gchatspeak.guildRankRight;
			rights = rights | offchatlisten.guildRankRight;
			rights = rights | offchatspeak.guildRankRight;
		}
		if ( pinvite.active )
			rights = rights | pinvite.guildRankRight;
		rights = rights | promote.guildRankRight;	//-Sebek
		rights = rights | gchatlisten.guildRankRight;
		rights = rights | gchatspeak.guildRankRight;
		rights = rights | offchatlisten.guildRankRight;
		rights = rights | offchatspeak.guildRankRight;
		if ( premove.active )
			rights = rights | premove.guildRankRight;
		if ( setmotd.active )
			rights = rights | setmotd.guildRankRight;
		if ( setinfo.active )
			rights = rights | setinfo.guildRankRight;
		if ( setnote.active )
			rights = rights | setnote.guildRankRight;
		if ( withdrawgold.active )
			rights = rights | withdrawgold.guildRankRight;
		if ( withdrawrepair.active )
			rights = rights | withdrawrepair.guildRankRight;
		if ( all.active )
			rights = rights | all.guildRankRight;
		return rights;
	}
}