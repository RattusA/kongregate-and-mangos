﻿
public class rankBankRightsBool
{
	public class Right
	{
		public bool  active;
		public GuildBankRights guildBankRight;
		
		public Right ( bool act, GuildBankRights right )
		{
			active = act;
			guildBankRight = right;
		}

		public void  UpdateActive ( GuildBankRights right )
		{
			if ((right & guildBankRight) == guildBankRight)
				active = true;
			else
				active = false;
		}
	}
	
	public class TabRight
	{
		public Right view = new rankBankRightsBool.Right(false, GuildBankRights.GUILD_BANK_RIGHT_VIEW_TAB);
		public Right deposit = new rankBankRightsBool.Right(false, GuildBankRights.GUILD_BANK_RIGHT_DEPOSIT_ITEM);
		public Right updatetext = new rankBankRightsBool.Right(false, GuildBankRights.GUILD_BANK_RIGHT_UPDATE_TEXT);
		
		public void  UpdateTabRight (  uint right)
		{
			view.UpdateActive((GuildBankRights)right);
			deposit.UpdateActive((GuildBankRights)right);
			updatetext.UpdateActive((GuildBankRights)right);
		}
		
		public GuildBankRights getRightsTogether ()
		{
			GuildBankRights rights = 0;
			if ( view.active )
				rights = rights | view.guildBankRight;
			if ( deposit.active )
				rights = rights | deposit.guildBankRight;
			if ( updatetext.active )
				rights = rights | updatetext.guildBankRight;
			return rights;
		}
	}

	public TabRight[] guildTabRight = new TabRight[SharedDefines.GUILD_BANK_MAX_TABS];
	public string[] guildTabSlotPerDay = new string[SharedDefines.GUILD_BANK_MAX_TABS];
	
	public rankBankRightsBool ()
	{
		//for(int i=0;i<SharedDefines.GUILD_BANK_MAX_TABS;i++)
		//	guildTabSlotPerDay[i] = "";
		for(int i=0;i<SharedDefines.GUILD_BANK_MAX_TABS;i++)
			guildTabRight[i] = new TabRight();
	}
	
	public void  UpdateRights (  uint[] tabright ,uint[] tabslotperday)
	{
		for(int i=0;i<SharedDefines.GUILD_BANK_MAX_TABS;i++)
		{
			guildTabRight[i].UpdateTabRight( tabright[i] );
			guildTabSlotPerDay[i] = "" + tabslotperday[i];
		}
	}
	
	public void  SaveRights (  uint[] tabright ,uint[] tabslotperday)
	{
		for(int i=0;i<SharedDefines.GUILD_BANK_MAX_TABS;i++)
		{
			tabright[i] = (uint)guildTabRight[i].getRightsTogether();
			tabslotperday[i] = uint.Parse(guildTabSlotPerDay[i]);
		}
	}
}