using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

//structura de date cu descrierea completa a fiecarui item
static public class itemStatTypes
{
	static public int strg;
	static public int agil;
	static public int intl;
	static public string descr;
}

public class MainPlayer:Player
{
	const float refreshTime = 0.5f;

//	public static int messageSpace = -1;
	public static ulong GUID;

	private bool  mIsMeetingStoneMenu = false;
	private ulong mMeetingStoneGuid;	
	private float chatspam = Time.time; //anti-chatspam - Sebek
	private Quaternion startRot;

	public bool  canShowMenu = true;
	public List<MessageWnd> windows = new List<MessageWnd>();
	public int MinChatBoxSize = 3;
	public Transform mainCamTransform;
	float refreshTimeLeft = 0;
	MovementFlags movementFlags;
	
	//new portrait stuff
	public Portrait PortraitReff;
	bool  WeNeedToRefreshSnapShots;
	//================
	public Transform inventoryCameraReff;
	public bool  ShowReviveDialogue = false;
	public ulong CasterGUID = 0;
	public bool  ShowMinimap = true;
	//====== sotropa daniel :|

	public static bool  waitingToEquip = false; // :|
	GameObject targetIndicator;
	Renderer targetIndicatorRenderer;
	
	public WorldSession session;

	public GroupManager group;
	public needManager need;
	public QuestManager questManager;
	//GameObject inventoryCamera ;
	CapsuleCollider playerColider;
	
	MovementFlags sendMovementFlags;
	
	public ButtonOLD interf;
	bool  interfaceLoaded  = false;
	bool  interfaceLoadedOnce  = false;
	public bool  lootOnce = true;
	ulong OldGuid;
	public ulong OldLootGuid=0;
	int OldTargetHP;
	int OldTargetMaxHP;
	int OldTargetPower;
	int OldTargetMaxPower;
	public PurchaseManager purchaseManager = null;
	
	//Texture dragTexture = Resources.Load<Texture>("culori/blue");
	
	Rect enemyCastBarRect;
	Rect enemyCastBarProgress;
	
	//Camera portrait_cam;
	//byte portrait_cam_time = 0;
	//Texture portrait_tex; //char portrait texture
	//Texture target_portrait_tex;//target portrait texture

	static ulong targetGUID;
	Item itemLoot = null; 
	
	public ChannelManagement ChannelM = new ChannelManagement();
	public GuildManager guildmgr = new GuildManager();
	public GuildUI guildUI = new GuildUI();
	
	public SocialManager SocialMgr;
	public SocialUI gSocialUI;
	public DuelManager duel;
	public TradeManager trade;
	public GuildVault gVault;
	public MailHandler mailHandler;
	public Inventory inv;
	public InspectManager InspectMgr;
	public ActionBarManager actionBarManager;
	public LootManager lootManager;
	public BattlegroundManager battlegroundManager;
	public QuestRewardItemInfoWindow questRewardItemInfoWindow;
	
	bool  XPBoosted;
	public bool  blockTargetChange;
	public bool  LoggingOut = false;
	public int logOutTimer = 0;
	int usedDildau = 0;
	public int spellManagerStatus = 0; //this variables keeps track of ALL the skill bar using binary additions 0-no button shown, 1-jump button, 2- 6 bottom buttons, 4-3 buttons right, 8-5 buttons right, 16 - 3 buttons left of right, 32- need to refresh spell bar
	public GameObject compassMiniMap;
	public bool  raidFlag = false;

	//MapBind variable
	//===========================
	static public Bind bind = new Bind();
	
	public static CorpseTemplate corpseLocation = new CorpseTemplate();
	public uint enteringAnAreaTrigger = 0;
	
	//===========================
	//WORLD GAMEOBJECT REFFERENCE
	public GameObject WorldReference; 
	
	//----------------------MiniMap quest helpers
	public List<NPC> questGiverList = new List<NPC>();
	//----------------------Last Dead Position
	public Vector3 lastDeadPosition;
	
	public uint statsFontSize = 22; //Font size for stats window
	
	public string mainTextColor = "#442200ff"; //dark brown
	
	public Banker banker = null;
	public bool  isBankerItemInfo = false;
	
	public VendorUI vendor = null;
	public bool  isVendorItemInfo = false;
	
	public bool  isLootItemInfo = false;
	
	public Guild guild = new Guild();
	
	bool  isCasting = false;
	
	//Error message variables
	//string errorMessage;
	//bool  showErrorMessage=false;
	
	public GUISkin ItemSkin;
	
	public PortalErrors teleportErrorController;
	
	public ActionBarPanel actionBarPanel;
	public TutorialHandler tutorialInstance;
	
	private Texture closeXTexture;
	private Rect chatTextWindowRect;
	
	public GlyphManager glyphManager;
	
	//buttons sizes
	int buttonWidth;
	int buttonHeight;
	int buttonSpacing;
	
	//MainMeniu siezes
	int nrMainTabs;
	int tabMainWidth;
	int tabMainHeight;
	//tabs sizes
	int nrTabs;
	int tabWidth;
	int tabHeight;
	//subtabs sizes
	int nrSubTabs;
	int subTabWidth;
	int subTabHeight;
	//lists sizes
	int nrItemsVisible;
	int listWidth;
	int listHeight; 
	
	public int menuSelected; // no selection -1 ** Back 0 ** Inventory 1 ** Quests 2 ** Spells 3 ** Others 4, **Traits 11, ** 6, 7,... //
	int newtabHeight;
	int newtabWidth;
	
	//GUIStyle messageTextStyle = new GUIStyle();
	Rect chatRect;
	Vector2 _chatScrollVector2;
	GUISkin InvisibleStyle;
	
	//=== nicu ===\\
	string _s="";
	Message ms;
	GUIStyle skin;// = newskin.GetStyle("GuildChat");
	//============\\

	float equipSize;
	Rect StatsRect;
	Rect SmallerStatsRect;
	Rect SmallerStatsLootRect;
	Rect SmallerStatsNeedRect;
	
	Rect LeftInventoryRect;
	Rect CharacterZoomRect; 
	Rect CharacterRect;
	Rect currentCharacterRect;
	
	Rect BagSpotsRect;
	Rect BagRect;
	Rect InsideBagRect;
	
	Rect destroyButtonRect;
	Rect equipButtonRect;
	
	Rect InventoryMessageRect;
	
	float equipSlotsSize;
	
	bool  firstTimeCheck = true; 
	
	byte s;
	byte c;
	byte m;
	uint money;

	Rect zoomButtonRect;
	
	public bool  zoom = false;
	
	string nameSlot; 
	
	int boxesPerHeight = 4;
	int boxesPerWidth = 4;
	int nrofBoxes = 17;
	int noBoxes = 16; 
	
//	int itemHint = -1; //item number in inventory to show hint, -1 no hint, >100 equip slots
//	Rect itemHintRect;
//	Texture2D itemHintTexture;
	
	float doubleclickTime = 0.0f;
	
	int inventoryPosX;
	int inventoryPosY;
	
	int[] inventoryPosition = new int[32]; // -1 no item , 0..itemManager.items.length 
	
	int currentBag = 0; // 0 default bag 1-4 equip bags
	
	uint selectedQuest = 0;
	Vector2 questScrollPosition = Vector2.zero;
	Vector2 scrollQViewVector = Vector2.zero;
	Rect questsRect;
	Rect questsRightRect;
	Rect questsLeftRect;
	Rect optionsRect;
	
	Rect spellsLeftRect;
	Rect spellsRightRect;
	
	float spellsStartX;
	float spellsStartY;	//Alex - delete screen.width / 22 
	int spellsPerRow = 7;
	Rect spellSRightSmallerRect;
	float spellsSize;
	
	public ActionButtonType spellCategory = ActionButtonType.ACTION_BUTTON_MACRO; // 0 default spells // 1 current class spells // 2 items  (potions, scrolls, etc.) //3 mounts
	
	float _equipSize;
	float _equipStartX;
	float _equipStartY;
	int _equipBarNumber = 0;
	float _equipSizeBetween;
	int _equipButtonDoubleSizePoz = -1;
	float _equipButtonDoubleSizeTime = 0;
	
	public bool  isNPC = false;
	ulong[] param = new ulong[3];
	public bool  showQuest = false;
	
	public WorldPacket pkt;
	
	int healthBarWidth;
	int healthBarHeight;
	
	Rect playerIconRect;
	Rect targetIconRect ; 
	
	Rect playerNameRect;
	Rect targetNameRect; 
	
	float auraStartX;
	float auraStartY; 
	public int auraWidth;
	int auraHeight;
	
//	Texture2D barEmpty;
//	Texture2D barHealth;
//	Texture2D barMana; // Mage Templar
//	Texture2D barPower;// Rogue
//	Texture2D barWrath;// Warrior
//	Texture2D barCurrent;
	
	Rect textRect; 
	float rowSize ; // Screen.height percentage  -Sebek
	
	Rect finalTextRect;	// variable textbox -Sebek
	Rect textBigRect;
//	Rect textRect; 
	bool  _showChat = false;
	
	NPC npc2;
	
//	bool  firstTimeBar = true;
	float xC = 0;
	float yC = 0;
	float zC = 0;
	
	static string guildName;
	
	public bool  goCast = false;
	
	float opacitySpellBackGround = 1;
	public string localTime;
	public string serverTime;
	
	Vector2 scrollViewVector = Vector2.zero;
	
	//int primaryKey = 0;// optional, pt a genera 'guid-uri' pt iteme
	//int itemClass = 0;// items to desplay on the invetory
	System.Threading.ParameterizedThreadStart itemType = null;// items to be displayed ////////////////////////////////////
	int subMeniu = -1;//subcategorii iteme
	int Meniu = -1;//categorii ieme
	//Vector2 scrollViewVector = Vector2.zero;
//	Texture2D inventoryTexture;//textura de fundal din meniu
	
	public bool  meniuOpend = false; //deschide meniu 
	//bool  bankOpened = false; // deschide banca
	bool  inventory = false;//afiseaza inventar
	bool  quest = false;//afiseaza lista cu questuri
	bool  spells = false;//afiseaza lista cu spells
	public bool  chatOpend = false;
	bool  options = false;

	public bool  stateItem = false;//itemul este echipat sau nu
	public Item itemSelected;//itemul selectat
	public Bag bagSelected;
	string nameItemEquiped;//numele itemului echipat
	
	//bool  showCaracter = false; //
	Texture tex; // textura caracter
	
	string textToSend = "";
	public string macrosText = "";
	string traitsDescriptionText = "";
	List<Message> allText = new List<Message>();
	//Vector2 scrollPosition = new Vector2(0,50);
	float scrollPosition = Screen.height; 
	int lines = 0;
	static byte lineHeight = 25;   
	
	public bool  showCharacterStats = true;
	
	//UIText text;
	public Texture2D Background;
	
	public GUISkin newskin;
	public GUISkin guildskin;
	GUIStyle currentnewstyle;
	GUISkin sk;
	
	
	public joyStick moveStick;
	joyStick rotateStick;
	public Swipe swipeScript;
	[SerializeField]
	private Texture2D selectTargetF;
	[SerializeField]
	private Texture2D selectTargetA;
	[SerializeField]
	private Texture2D selectTargetN;
	uint[] mountsSpell = new uint[4];
	Camera cam;

	Vector2 touchPosition;
	Vector2 lastTouchPosition;
	Vector2 firstTouchPosition;
	float firstTouchTime;
	bool  characterRotationState = false;
	bool  moveToFirst = false;
	
	bool dragIsActive= false;
	Rect dragRect;
	float xFromTouch;
	float yFromTouch;
//	Item dragItem;
	uint dragPos;
	
	//decs
	public string itemImageDrag = null;
	public UIButton itemButtonDrag = null;

	public uint currentXp;
	public uint maxXp;
	float time = 0;
	private ScrollZone TextZone = new ScrollZone();

	private Rect _mapRect = new Rect(Screen.width - 0.0872f * Screen.height, 0, 0.0872f * Screen.height, 0.0872f * Screen.height);
	private Texture _map = Resources.Load<Texture>("GUI/Maps/map_circle_menu");
	private bool  _showMenuButton=false;
	uint lastSceneLoaded = 0;
	private Texture _sendTexture = Resources.Load<Texture>("GUI/InGame/w");
	
	void  Awake ()
	{  
		group = new GroupManager(this);
		need = new needManager(this);
		inv = new Inventory(this);
		buttonWidth = (int)((Screen.width/3.02f)*1.2f);
		buttonHeight = Screen.height/16;
		buttonSpacing = 1;
		nrMainTabs = 8;
		tabMainWidth = (int)(Screen.width/8.585f);
		tabMainHeight = Screen.height/10;
		nrTabs = 8;
		tabWidth = Screen.width/10;
		tabHeight = Screen.height/9;
		nrSubTabs = 4;
		subTabWidth = (int)(Screen.width/8.2f);
		subTabHeight = (int)(Screen.height/11.08f);
		nrItemsVisible= 6;
		listWidth = buttonWidth;
		listHeight = (buttonHeight+buttonSpacing)*nrItemsVisible; 
		menuSelected = -1; // no selection -1 ** Back 0 ** Inventory 1 ** Quests 2 ** Spells 3 ** Others 4, **Traits 11, ** 6, 7,... //
		newtabHeight = (int)(tabHeight * 0.8f);
		newtabWidth = Screen.width/8;
		chatRect = new Rect(0, tabMainHeight, Screen.width, Screen.height * 0.8f - tabMainHeight);
		InvisibleStyle = Resources.Load<GUISkin>("GUI/ChatSkin");
		equipSize = Screen.width/12;
		StatsRect = new Rect(equipSize,4.8f*Screen.height/6,Screen.width/2-equipSize*2,1.1f*Screen.height/6);
		SmallerStatsRect = new Rect(equipSize*1.5f,4.9f*Screen.height/6,Screen.width/2-equipSize*3.0f,1.0f*Screen.height/6);
		SmallerStatsLootRect = new Rect(Screen.width/2.5f+Screen.width/6,Screen.height/6,Screen.width/3.3f,Screen.height/2.0f);
		SmallerStatsNeedRect = new Rect(Screen.width/17,Screen.height/7+Screen.height/1.6f/5+Screen.height/1.6f/10,Screen.width/2.5f-Screen.width/10,Screen.height/1.6f);
		LeftInventoryRect = new Rect(0,tabMainHeight*1.1f, Screen.width/2, Screen.height-tabMainHeight*1.1f);
		CharacterZoomRect = new Rect(equipSize*0.1f, tabMainHeight*1.1f, LeftInventoryRect.width-equipSize*0.2f, 5.35f*Screen.height/6); 
		CharacterRect = new Rect(equipSize*1.1f, tabMainHeight*1.1f, Screen.width/2-equipSize*2.2f, 4*Screen.height/6);
		currentCharacterRect = new Rect(CharacterRect.x,CharacterRect.y,CharacterRect.width,CharacterRect.height - Screen.height*0.15f);
		BagSpotsRect = new Rect(Screen.width*0.55f,tabMainHeight*1.2f,Screen.width*0.43f,equipSize*0.8f);
		BagRect = new Rect(Screen.width*0.55f,tabMainHeight*1.2f+equipSize*0.8f, Screen.width*0.43f,Screen.height*0.55f);
		InsideBagRect = new Rect(Screen.width*0.57f,tabMainHeight*1.3f+equipSize*0.8f, Screen.width*0.39f,Screen.height*0.53f);
		destroyButtonRect = new Rect(Screen.width-tabWidth*4.2f,BagRect.y+BagRect.height*1.02f,tabWidth*1.8f,tabHeight*0.7f);
		equipButtonRect = new Rect(Screen.width-tabWidth*2.2f,BagRect.y+BagRect.height*1.02f,tabWidth*1.8f,tabHeight*0.7f);
		InventoryMessageRect = new Rect(0,0,Screen.width,Screen.height);
		equipSlotsSize = 0.87f*Screen.height/10;
		zoomButtonRect = new Rect(Screen.width/2 - equipSize/4 -equipSlotsSize - equipSlotsSize*0.15f, LeftInventoryRect.y+Screen.height*0.01f, equipSlotsSize*1.3f, equipSlotsSize);
//		itemHintTexture = Resources.Load<Texture2D>("culori/blue");
		questsRect = new Rect(Screen.width/20, tabMainHeight+Screen.height/20, 18*Screen.width/20, Screen.height-tabMainHeight-Screen.height/10);
		questsRightRect = new Rect(Screen.width/10+tabMainWidth, tabMainHeight+Screen.height/20, 17*Screen.width/20-tabMainWidth, Screen.height-tabMainHeight-Screen.height/10);
		questsLeftRect = new Rect(Screen.width/20, tabMainHeight+Screen.height/20, tabMainWidth, Screen.height-tabMainHeight-Screen.height/10);
		optionsRect = new Rect(tabWidth*3.0f, tabHeight+20, (subTabWidth*2) ,(subTabHeight+10)*nrSubTabs);
		spellsLeftRect = new Rect(0,tabMainHeight*1.1f,Screen.width*0.25f,Screen.height - tabMainHeight-Screen.height/6);
		spellsRightRect = new Rect(Screen.width*0.25f,tabMainHeight*1.1f,Screen.width*0.75f,Screen.height - tabMainHeight- Screen.height/6);
		spellsStartX = spellsRightRect.x + spellsRightRect.width*0.05f + Screen.width /25;
		spellsStartY = spellsRightRect.y + spellsRightRect.height*0.05f + Screen.width/20;	//Alex - delete screen.width / 22 
		spellSRightSmallerRect = new Rect(spellsStartX, spellsStartY, spellsRightRect.width*0.9f, spellsRightRect.height*0.9f);
		spellsSize = spellSRightSmallerRect.width / spellsPerRow;
		_equipSize = Screen.width / 12;
		_equipStartX = _equipSize * 5;
		_equipStartY = Screen.height - _equipSize;
		_equipSizeBetween = _equipSize/7;
		healthBarWidth = Screen.width/7;
		healthBarHeight = Screen.height/25;
		playerIconRect = new Rect(healthBarHeight*0.1f, healthBarHeight*0.1f, healthBarHeight*3, healthBarHeight*3);
		targetIconRect = new Rect(Screen.width*0.45f, healthBarHeight*0.1f, healthBarHeight*3, healthBarHeight*3); 
		playerNameRect = new Rect(healthBarHeight*0.1f+playerIconRect.width, healthBarHeight*0.1f, healthBarWidth, healthBarHeight);
		targetNameRect = new Rect(Screen.width*0.45f+targetIconRect.width, healthBarHeight*0.1f, healthBarWidth, healthBarHeight); 
		auraStartX = playerNameRect.x+healthBarWidth;
		auraStartY = playerNameRect.y; 
		auraWidth = Screen.width/15;
		auraHeight = Screen.height/25;
//		barEmpty = Resources.Load<Texture2D>("culori/barEmpty");
//		barHealth = Resources.Load<Texture2D>("culori/barHealth");
//		barMana = Resources.Load<Texture2D>("culori/barMana"); // Mage Templar
//		barPower = Resources.Load<Texture2D>("culori/barPower");// Rogue
//		barWrath = Resources.Load<Texture2D>("culori/barWrath");// Warrior
		textRect = new Rect(Screen.width/4, Screen.height*75/100 , _equipSize*7, Screen.height*6.66f/100 ); 
		finalTextRect = new Rect(Screen.width/4, (Screen.height*75/100)-rowSize , _equipSize*7, rowSize );	// variable textbox -Sebek
		textBigRect = new Rect(Screen.width/4, 0, _equipSize*7, Screen.height/2-Screen.height/15 );
//		textRect = new Rect(Screen.width/4, Screen.height*3/4 , _equipSize*7, Screen.height/15 ); 
//		textBigRect = new Rect(Screen.width/4, 0, _equipSize*7, Screen.height/2-Screen.height/15 );

		base.Awake();
		
		isMainPlayer = true;		
		AddChatInput("Debug","\n\n\n\n\n\n");				//Scrolls 1st lines of chat down
		IsInGuild();																// Checks if player belongs to a guild, used for illegal /g chat - Sebek
		WorldReference = GameObject.Find("world");
		duel = new DuelManager();
		SocialMgr = new SocialManager(this);
		XPBoosted = false;
		blockTargetChange = false;
		mailHandler = new MailHandler(this);
		InspectMgr = new InspectManager(this);
		actionBarManager = new ActionBarManager();
		cooldownManager = new CooldownManager(this);
		lootManager = new LootManager(this);
		battlegroundManager = new BattlegroundManager(this);
		questRewardItemInfoWindow = new QuestRewardItemInfoWindow();
		
		playerSpellManager = new PlayerSpellManager(this);
		petSpellManager = new PetSpellManager(this);
		cooldownManager = new CooldownManager(this);
		
		OptionsManager.Initialize(this.name);
		
		closeXTexture = Resources.Load<Texture>("GUI/Maps/close_button");
		
		if(!WorldSession.player || WorldSession.player.actionBarPanel == null)
		{
			actionBarPanel = new ActionBarPanel();
		}
		
		if(PlayerPrefs.HasKey("usedDildau"))
			usedDildau = PlayerPrefs.GetInt("usedDildau");
		
		CorpseTemplate corpseLocation= Prefs.Load<CorpseTemplate>("Corpse Location");
		
		glyphManager = new GlyphManager(this as Player);
	}
	
	static Vector2 Rotate ( float angle, Vector2 targget, Vector2 pivot )
	{
		angle=angle*Mathf.PI/180;
		Vector2 ret;
		targget.x-=pivot.x;
		targget.y-=pivot.y;
		ret.x=Mathf.Cos(angle)*targget.x - Mathf.Sin(angle)*targget.y;
		ret.y=Mathf.Cos(angle)*targget.y + Mathf.Sin(angle)*targget.x;
		ret.x+=pivot.x;
		ret.y+=pivot.y;
		return ret;
	}
	
	public MainPlayer ()
	{
		_valuescount = 0x52E;
		InitValues();
		_type = TYPEID.MAIN_PLAYER;	
		itemLoot = null;
		
		//InvisibleStyle.normal.background = UnityEngine.Resources.Load<Texture2D>("banners/alfa");
	}
	
	public void BuildPkt( OpCodes opcode )//ushort)
	{
//		byte aux = 0;
		ushort aux16 = 0;
		uint aux32 = 0;
		ulong aux64 = 0;
		float auxF = 0;
		float orient;
		OpCodes opc = opcode;
		WorldPacket wp = new WorldPacket(opc);//,4+2+4+16);
		
		wp.SetOpcode(opc);
		wp.AppendPackGUID(GUID);
		wp.Append(ByteBuffer.Reverse((uint)moveFlag));
		wp.Append(aux16);
		wp.Append(aux32);/// this shoud be a timer in ms
		Vector3 pos = GetPositionForServer();
		
		//Debug.Log("Move: " + opcode + " pos: " + pos);
		
		//print(pos);
		wp.Append(pos.x);
		wp.Append(pos.y);
		wp.Append(pos.z);
		orient = Mathf.PI*0.5f - TransformParent.rotation.eulerAngles.y * Mathf.PI/180; // readians value for server interpretation
		//print("rad orient : " + orient);
		wp.Append(orient);
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_ONTRANSPORT;
		if((moveFlag & flag) > 0)
		{
			wp.Append(aux64);
			wp.Append(auxF);
			wp.Append(auxF);
			wp.Append(auxF);
			wp.Append(auxF);/// wposition
			wp.Append(aux32);/// transport time;
		}
		flag = MovementFlags.MOVEMENTFLAG_SWIMMING;
		if((moveFlag & flag) > 0)
		{
			wp.Append(auxF);///aici trebuie sa fie unghiul de inot
		}
		wp.Append(aux32);
		flag = MovementFlags.MOVEMENTFLAG_FALLING;
		if((moveFlag & flag) > 0)
		{
			auxF = 0.5f;
			wp.Append(auxF);
			wp.Append(Mathf.Sin(orient + (Mathf.PI*0.5f)));
			wp.Append(Mathf.Cos(orient + (Mathf.PI*0.5f)));
			wp.Append(moveSpeed);
		}
		//while(wp.Size()<32)
		//	wp.Append(aux);
		RealmSocket.outQueue.Add(wp);
	}
	
	public void BuildPkt( OpCodes opcode, Quaternion rotation )//ushort)
	{
		if(rotation == null)
		{
			rotation = TransformParent.rotation;
		}
//		byte aux = 0;
		ushort aux16 = 0;
		uint aux32 = 0;
		ulong aux64 = 0;
		float auxF = 0;
		float orient;
		OpCodes opc = opcode;
		WorldPacket wp = new WorldPacket(opc);//,4+2+4+16);
		
		wp.SetOpcode(opc);
		wp.AppendPackGUID(GUID);
		wp.Append(ByteBuffer.Reverse((uint)moveFlag));
		wp.Append(aux16);
		wp.Append(aux32);/// this shoud be a timer in ms
		Vector3 pos = GetPositionForServer();
		
		//Debug.Log("Move: " + opcode + " pos: " + pos);
		
		//print(pos);
		wp.Append(pos.x);
		wp.Append(pos.y);
		wp.Append(pos.z);
		orient = Mathf.PI*0.5f - rotation.eulerAngles.y * Mathf.PI/180; // readians value for server interpretation
		//print("rad orient : " + orient);
		wp.Append(orient);
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_ONTRANSPORT;
		if((moveFlag & flag) > 0)
		{
			wp.Append(aux64);
			wp.Append(auxF);
			wp.Append(auxF);
			wp.Append(auxF);
			wp.Append(auxF);/// wposition
			wp.Append(aux32);/// transport time;
		}
		flag = MovementFlags.MOVEMENTFLAG_SWIMMING;
		if((moveFlag & flag) > 0)
		{
			wp.Append(auxF);///aici trebuie sa fie unghiul de inot
		}
		wp.Append(aux32);
		flag = MovementFlags.MOVEMENTFLAG_FALLING;
		if((moveFlag & flag) > 0)
		{
			auxF = 0.5f;
			wp.Append(auxF);
			wp.Append(Mathf.Sin(orient + (Mathf.PI*0.5f)));
			wp.Append(Mathf.Cos(orient + (Mathf.PI*0.5f)));
			wp.Append(moveSpeed);
		}
		//while(wp.Size()<32)
		//	wp.Append(aux);
		RealmSocket.outQueue.Add(wp);
	}
	
	void  MoveStop ()
	{
		if(0 != (sendMovementFlags & (MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT)))
		{
			/*uint flag = MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD;
	if(!(moveFlag & flag))
		return;
	flag = MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_WALK_MODE;
	moveFlag = moveFlag & ~flag;*/
			base.MoveStop();
			BuildPkt(OpCodes.MSG_MOVE_STOP);
			
			sendMovementFlags &= ~(MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT );
		}
	}
	
	public void  MoveStopStrafe ()
	{
		if(0 != (sendMovementFlags & (MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT)))
		{
			MovementFlags flag = (MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT);
			if((moveFlag & flag) == 0)
			{
				return;
			}
			moveFlag = moveFlag & ~flag;
			BuildPkt(OpCodes.MSG_MOVE_STOP_STRAFE);
			
			sendMovementFlags &= ~(MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT);
		}
	}
	
	void  StartSwimming ()
	{
		base.StartSwimming();
		BuildPkt(OpCodes.MSG_MOVE_START_SWIM);
	}
	
	void  MoveStartForward ()
	{
		
		if (isRooted)
			return;
		if ((sendMovementFlags & MovementFlags.MOVEMENTFLAG_FORWARD) > 0
		    && (sendMovementFlags & MovementFlags.MOVEMENTFLAG_STRAFE_LEFT) == 0
		    && (sendMovementFlags & MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT) == 0) 
			return;
		
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_FORWARD;
		moveFlag |= flag;
		flag = (MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT);
		moveFlag &= ~flag;
		BuildPkt(OpCodes.MSG_MOVE_START_FORWARD);
		sendMovementFlags = MovementFlags.MOVEMENTFLAG_FORWARD;
		/*if(0 == (sendMovementFlags & MovementFlags.MOVEMENTFLAG_FORWARD))
	{		
		if(isRooted) 
			return;
		/*uint flag = MovementFlags.MOVEMENTFLAG_FORWARD;
		if(moveFlag & flag)
			return;
		moveFlag = moveFlag | flag;
		//print("FW pkt");
		flag = MovementFlags.MOVEMENTFLAG_BACKWARD;
		moveFlag = moveFlag & ~flag;*/
		/*
		if(0 != (sendMovementFlags & MovementFlags.MOVEMENTFLAG_BACKWARD))
		{
			MoveStop();
		}			
		super();
		
		lastpkt = OpCodes.MSG_MOVE_START_FORWARD;
		BuildPkt(OpCodes.MSG_MOVE_START_FORWARD);
		sendMovementFlags |= MovementFlags.MOVEMENTFLAG_FORWARD;
	}*/
	}
	
	void  MoveStartBackward ()
	{
		
		if (isRooted || (sendMovementFlags & MovementFlags.MOVEMENTFLAG_BACKWARD) > 0) return;
		
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_BACKWARD;
		moveFlag |= flag;
		flag = (MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT);
		moveFlag &= ~flag;
		BuildPkt(OpCodes.MSG_MOVE_START_BACKWARD);
		sendMovementFlags = MovementFlags.MOVEMENTFLAG_BACKWARD;
		/*if(0 == (sendMovementFlags & MovementFlags.MOVEMENTFLAG_BACKWARD))
	{		
		if(isRooted) return;
		/*uint flag = MovementFlags.MOVEMENTFLAG_BACKWARD;
		if(moveFlag & flag)
			return;
		flag = (MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_WALK_MODE); // backward walk is always slow; flag must be set, otherwise causing weird movement in other client
		moveFlag = moveFlag | flag ;
		flag = MovementFlags.MOVEMENTFLAG_FORWARD;
		//flag = ~MovementFlags.MOVEMENTFLAG_FORWARD;
		moveFlag = moveFlag & ~flag; */
		
		/* if(0 != (sendMovementFlags & MovementFlags.MOVEMENTFLAG_FORWARD))
		{
			MoveStop();
		}
		super();
		
		lastpkt = OpCodes.MSG_MOVE_START_BACKWARD;
		BuildPkt(OpCodes.MSG_MOVE_START_BACKWARD);
		sendMovementFlags |= MovementFlags.MOVEMENTFLAG_BACKWARD;*/
		//}
		
		
		
	}
	
	void  MoveStartStrafeLeft ()
	{
		if(isRooted) 
		{
			return;
		}
		if ((sendMovementFlags & MovementFlags.MOVEMENTFLAG_STRAFE_LEFT) > 0
		    && (sendMovementFlags & MovementFlags.MOVEMENTFLAG_FORWARD) == 0
		    && (sendMovementFlags & MovementFlags.MOVEMENTFLAG_BACKWARD) == 0)
		{
			return;
		}
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
		moveFlag |= flag;
		flag = (MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT | MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD);
		moveFlag = moveFlag & ~flag;
		BuildPkt(OpCodes.MSG_MOVE_START_STRAFE_LEFT);
		sendMovementFlags = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
		
		/*		flag = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
	moveFlag = moveFlag | flag;
	
	uint flag = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
	if(moveFlag & flag)
		return;
	moveFlag = moveFlag | flag;
	//print("FW pkt");
	flag = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
	moveFlag = moveFlag & ~flag;
/*
	if(isRooted) return;
	uint flag = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
	if(moveFlag & flag)
		return;
	flag = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
	try
	{
		moveFlag = moveFlag | flag;
	}
	catch( System.Exception e )
	{
		moveFlag = moveFlag | flag;
	}
//    flag = moveFlag | flag ;
	//print("" + flag);
	//moveFlag = flag ;
	flag = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
	flag = ~flag;
	moveFlag = moveFlag & flag; 
	BuildPkt(OpCodes.MSG_MOVE_START_STRAFE_LEFT);
	*/
	}
	
	void  MoveStartStrafeRight ()
	{
		if (isRooted)
		{
			return;
		}
		if ((sendMovementFlags & MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT) > 0
		    && (sendMovementFlags & MovementFlags.MOVEMENTFLAG_FORWARD) == 0
		    && (sendMovementFlags & MovementFlags.MOVEMENTFLAG_BACKWARD) == 0)
		{
			return;
		}
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
		moveFlag |= flag;
		flag = (MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD);
		moveFlag = moveFlag & ~flag;
		BuildPkt(OpCodes.MSG_MOVE_START_STRAFE_RIGHT);
		sendMovementFlags = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
		
//		flag = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
//		moveFlag = moveFlag | flag;
//
//		uint flag = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
//		if(moveFlag & flag)
//			return;
//		moveFlag = moveFlag | flag;
//		flag = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
//		moveFlag = moveFlag & ~flag;
//
////		uint flag = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
////		if(moveFlag & flag)
////			return;
////		flag = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT; 
////		moveFlag = moveFlag | flag;
////		flag = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
////		flag = ~flag;
////		moveFlag = moveFlag & flag;
////		BuildPkt(OpCodes.MSG_MOVE_START_STRAFE_RIGHT);
	}
	
	void  MoveStartForwardLeft ()
	{
		if (isRooted)
		{
			return;
		}
		if ((sendMovementFlags & MovementFlags.MOVEMENTFLAG_STRAFE_LEFT) > 0
		    && (sendMovementFlags & MovementFlags.MOVEMENTFLAG_FORWARD) > 0)
		{
			return;
		}
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_FORWARD;
		MovementFlags flag2 = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
		OpCodes opcode = OpCodes.MSG_NULL_ACTION;
		if ((moveFlag & flag) == 0)
		{
			opcode = OpCodes.MSG_MOVE_START_FORWARD;
		}
		if ((moveFlag & flag2) == 0)
		{
			opcode = OpCodes.MSG_MOVE_START_STRAFE_LEFT;
		}
		moveFlag |= flag | flag2;
		flag = (MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT | MovementFlags.MOVEMENTFLAG_BACKWARD);
		moveFlag = moveFlag & ~flag;
		BuildPkt(opcode);
		sendMovementFlags = MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
		//Debug.Log("moveflag: " + moveFlag + " | sendflag: " + sendMovementFlags);
		
	}
	
	void  MoveStartForwardRight ()
	{
		if (isRooted)
		{
			return;
		}
		if ((sendMovementFlags & MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT) > 0
		    && (sendMovementFlags &  MovementFlags.MOVEMENTFLAG_FORWARD) > 0)
		{
			return;
		}
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_FORWARD;
		MovementFlags flag2 = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
		OpCodes opcode = OpCodes.MSG_NULL_ACTION;
		if ((moveFlag & flag) == 0)
		{
			opcode = OpCodes.MSG_MOVE_START_FORWARD;
		}
		if ((moveFlag & flag2) == 0)
		{
			opcode = OpCodes.MSG_MOVE_START_STRAFE_RIGHT;
		}
		moveFlag |= flag | flag2;
		flag = (MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_BACKWARD);
		moveFlag = moveFlag & ~flag;
		BuildPkt(opcode);
		sendMovementFlags = MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
		
	}
	
	void  MoveStartBackwardLeft ()
	{
		if (isRooted)
		{
			return;
		}
		if ((sendMovementFlags & MovementFlags.MOVEMENTFLAG_STRAFE_LEFT) > 0
		    && (sendMovementFlags & MovementFlags.MOVEMENTFLAG_BACKWARD) > 0)
		{
			return;
		}
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_BACKWARD; 
		MovementFlags flag2 = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
		OpCodes opcode = OpCodes.MSG_NULL_ACTION;
		if ((moveFlag & flag) == 0)
		{
			opcode = OpCodes.MSG_MOVE_START_BACKWARD;
		}
		if ((moveFlag & flag2) == 0)
		{
			opcode = OpCodes.MSG_MOVE_START_STRAFE_LEFT;
		}
		moveFlag |= flag | flag2;
		flag = (MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT | MovementFlags.MOVEMENTFLAG_FORWARD);
		moveFlag = moveFlag & ~flag;
		BuildPkt(opcode);
		sendMovementFlags = MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
		
	}
	
	void  MoveStartBackwardRight ()
	{
		if (isRooted)
		{
			return;
		}
		if ((sendMovementFlags & MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT) > 0
		    && (sendMovementFlags & MovementFlags.MOVEMENTFLAG_BACKWARD) > 0)
		{
			return;
		}
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_BACKWARD;
		MovementFlags flag2 = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
		OpCodes opcode = OpCodes.MSG_NULL_ACTION;
		if ((moveFlag & flag) == 0)
		{
			opcode = OpCodes.MSG_MOVE_START_BACKWARD;
		}
		if ((moveFlag & flag2) == 0)
		{
			opcode = OpCodes.MSG_MOVE_START_STRAFE_RIGHT;
		}
		moveFlag |= flag | flag2;
		flag = (MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_FORWARD);
		moveFlag = moveFlag & ~flag;
		BuildPkt(opcode);
		sendMovementFlags = MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
	}
	
	void  MoveStartTurnLeft ()
	{
		/*if(lastpkt == OpCodes.MSG_MOVE_START_TURN_LEFT)
		return;*/
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_TURN_LEFT;
		if((moveFlag & flag) > 0)
		{
			return;
		}
		moveFlag = moveFlag | flag;
		flag = MovementFlags.MOVEMENTFLAG_TURN_RIGHT;
		moveFlag = moveFlag & ~flag;
		lastpkt = OpCodes.MSG_MOVE_START_TURN_LEFT;
		BuildPkt(OpCodes.MSG_MOVE_START_TURN_LEFT);
	}
	
	void  MoveStartTurnRight ()
	{
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_TURN_RIGHT;
		if((moveFlag & flag) > 0)
		{
			return;
		}
		moveFlag = moveFlag | flag;
		flag = MovementFlags.MOVEMENTFLAG_TURN_LEFT;
		moveFlag = moveFlag & ~flag;
		lastpkt = OpCodes.MSG_MOVE_START_TURN_RIGHT;
		BuildPkt(OpCodes.MSG_MOVE_START_TURN_RIGHT);
	}
	
	public void  MoveStopTurn ()
	{
		if(0 != (sendMovementFlags & (MovementFlags.MOVEMENTFLAG_TURN_LEFT | MovementFlags.MOVEMENTFLAG_TURN_RIGHT)))
		{
			/*if(lastpkt == OpCodes.MSG_MOVE_STOP_TURN)
			return;*/
			MovementFlags flag = MovementFlags.MOVEMENTFLAG_TURN_LEFT;// | MovementFlags.MOVEMENTFLAG_TURN_RIGHT;
			MovementFlags flag2 = MovementFlags.MOVEMENTFLAG_TURN_RIGHT;
			flag = flag | flag2;
			if((moveFlag & flag) == 0)
			{
				return;
			}
			moveFlag = moveFlag & ~flag;
			lastpkt = OpCodes.MSG_MOVE_STOP_TURN;
			BuildPkt(OpCodes.MSG_MOVE_STOP_TURN);
			
			
			sendMovementFlags &= ~(MovementFlags.MOVEMENTFLAG_TURN_LEFT | MovementFlags.MOVEMENTFLAG_TURN_RIGHT);
		}
	}
	
	//int heartNumber =0;
	
	void  SendHeartBit ()
	{
		/*print("flags : " + moveFlag + "   "+heartNumber);
	heartNumber++;*/
		BuildPkt(OpCodes.MSG_MOVE_HEARTBEAT);
	}
	
	void  SetPosition ( Vector3 pos, float o )
	{
		Debug.Log("Set Position "+pos);
		base.SetPosition(pos,o); // for some reason it sets rotation to zero instead of "o"(orientation)
		/*transformParent.position = pos;
	//transformParent.rotation.eulerAngles.y = o * 180 / Mathf.PI; // degree value
	transformParent.rotation.eulerAngles.y = (Mathf.PI*0.5f - o) * 180.0f/Mathf.PI;*/
		//startRot = transformParent.rotation;
		
		//Debug.Log("Set main player position: " + pos);
		
		TransformParent.Rotate(0, o, 0);
		
		//holdRotation hR = transformParent.Find("Main Camera").GetComponent<holdRotation>();
		//hR.Start();
	}
	
	/*	static Vector3 Pos;// = new Vector3(-60.1113f,30,-61.7522f);
static float or;

static void  setPos ( Vector3 pos ,   float o )
	{
	if(Application.loadedLevelName == "scena")
	{
		MainPlayer to = WorldSession.player;
		to.SetPosition(pos,o);
		
		return;
	}
	Pos = pos;
	or = o;
}*/
	
	void Start() //init stuff
	{
		UpdateTime();
		ButtonOLD._UI = GameObject.Find("UI");
		base.Start();
//		tutorialInstance = transformParent.gameObject.GetComponentInChildren<TutorialHandler>();

		WorldSession.interfaceManager.SetMainPlayer(this);
		// 		WorldSession.interfaceManager.tradeWindow.TradeMng.SetMainPlayer(this);
		
		
		trade = new TradeManager(this);
		craftManager = new CraftManager(this);
		
		//=====
		inventoryCameraReff = GameObject.Find("inventoryCamera(Clone)").transform;
		inventoryCameraReff.gameObject.SetActive(false);
		playerColider = transform.parent.GetComponent<CapsuleCollider>();
		
		//keyboard = iPhoneKeyboard.Open(tempString, iPhoneKeyboardType.Default, false, false, false, false, "Enter Your Name Here");
		//if(keyboard)
		//{
		//	keyboard.hideInput=true;
		
		//}
//		RenderSettings.fog=Fog;
		
		//InitSlots();
		//PlayerPrefs.DeleteAll();
		
		
		transform.gameObject.layer = 11;
		transform.parent.gameObject.layer = 11;
		
		var allChildren= gameObject.GetComponentsInChildren<Transform>();
		foreach(Transform child in allChildren) 
			child.gameObject.layer = 11;
		
		_Unmount();
		
		if(GameObject.Find("friendly(Clone)"))
		{
			targetIndicator = GameObject.Find("friendly(Clone)");
		}
		else
		{
			targetIndicator = Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/objects/friendly"));
		}
		
		targetIndicatorRenderer = targetIndicator.GetComponent<Renderer>();
		
		targetIndicator.SetActive(false);
		Destroy(targetIndicator.GetComponent<Collider>());
		//System.GC.Collect();///eliberating some memory
		
		moveFlag = 0;
		moveStick = new joyStick( new Rect(Screen.width*0.025f, Screen.height*0.76f, Screen.height*0.17f, Screen.height*0.17f), null);
		moveStick.outerTexture = Resources.Load<Texture2D>("GUI/InGame/stick_1");
		moveStick.setInTexture(Resources.Load<Texture2D>("GUI/InGame/stick_2"));
		
		movementFlags = (MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT);
		
		refreshTimeLeft=Time.timeSinceLevelLoad;
		Defines.loading = false;
		
		//transformParent.position = Pos;
		//transformParent.rotation.eulerAngles.y = or;
		
		startRot = TransformParent.rotation;
		mainCamTransform = Camera.main.transform;//GameObject.Find("Main Camera").transform;
		swipeScript = mainCamTransform.GetComponent<Swipe>();

		guid = GUID;
		
		DontDestroyOnLoad(targetIndicator);
		DontDestroyOnLoad(TransformParent.gameObject);

		selectTargetF = Resources.Load<Texture2D>("textures/friend indicator/select plane friendly");
		selectTargetA = Resources.Load<Texture2D>("textures/friend indicator/select plane enemy");	
		selectTargetN = Resources.Load<Texture2D>("textures/friend indicator/select plane neutral");	
		
//		inventoryTexture = Resources.Load<Texture2D>("culori/blue_transp");
		GameObject gl = Resources.Load<GameObject>("prefabs/inventoryCamera");
		cam = gl.GetComponent<Camera>();
		tex = cam.targetTexture;
		
		//portrait_cam_time = 3;
		
		if(dbs._sizeMod == 2)
		{
			newskin = Resources.Load<GUISkin>("GUI/Skins_2x");
			guildskin = Resources.Load<GUISkin>("GUI/GuildSkins_2x");
			statsFontSize = 65;
			ItemSkin = UnityEngine.Resources.Load<GUISkin>("GUI/RegistrationSkin_2x");			
		}
		else
		{
			newskin = Resources.Load<GUISkin>("GUI/Skins");
			guildskin = Resources.Load<GUISkin>("GUI/GuildSkins");
			statsFontSize = 22;
			ItemSkin = UnityEngine.Resources.Load<GUISkin>("GUI/RegistrationSkin");
		}
		TextZone.InitSkinInfo(newskin.GetStyle("DescriptionAndStats"));
		
		questManager = new QuestManager(this); 
		if(questManager != null)
			questManager.getPlayerListQuests();
		
		Background = UnityEngine.Resources.Load<Texture2D>("others/transpback");		
		
		
		armory.InitArmory(0);
		
		//this is to check if value for see damage on other players in playerprefs exist and if not set it some default 0
		if (!PlayerPrefs.HasKey("ShowDMGonOtherPlayers")) {
			PlayerPrefs.SetInt("ShowDMGonOtherPlayers", 0);
		}
		if(PlayerPrefs.HasKey(name+"spellManagerStaus"))
		{
			spellManagerStatus = PlayerPrefs.GetInt(name+" spellManagerStatus");
		}
		else
		{
			spellManagerStatus = 1+2+32;
		}
		compassMiniMap = Camera.main.transform.Find("Compass").gameObject;
		compassMiniMap.SetActive(false);
		#if UNITY_IPHONE
		if(Screen.width > 1023 && Screen.height > 700)
			compassMiniMap.transform.position = new Vector3(compassMiniMap.transform.position.x /2, compassMiniMap.transform.position.y,compassMiniMap.transform.position.z);
		#endif
		//for reseting purposes
		//spellManagerStatus=32;
		//=== nicu ===\\
		skin = newskin.GetStyle("GuildChat");
		if(PlayerPrefs.GetInt(this.name+" ChatSize") > 0)
		{
			MinChatBoxSize = PlayerPrefs.GetInt(this.name+" ChatSize");
		}
		purchaseManager = GameObject.Find("PurchaseGO").GetComponent<PurchaseManager>();
	}	
	
	bool  firstTimeInitSlots = true;	
	
	int i;
	bool  fw = false;
	bool  left = false;
	bool  right = false; 
	bool  firstMove = false;
	bool  firstRotate = false;
	int ion = 0;
	
	
	bool  fpressed = false;
	bool  lpressed = false;
	bool  rpressed = false;
	
	byte lastDirection = 0;
	
	/// new movement
	JoystickReturn value = JoystickReturn.UNKNOWN;
	//byte dontIgnoreThisFrameVelocity = 0; // if equal to 5
	
	public float updateTime = 2.0f;
	public float timeLeftToUpdateGroup = 0.5f;
	
	public Item[] slots = new Item[20];
	static public bool updateSlots = true;
	
	// Update equiping slots
	void  UpdateSlots ()
	{
		updateSlots=false;
		for(int i = 0; i < 20; i++)
			slots[i] = null;
		for(int i = 0; i < itemManager.Items.Count; i++)
		{
			Item item = itemManager.Items[i];
			if (item.slot < 20 && item.slot >= 0 && item.bag == 255)
				slots[item.slot] = item;//george
		}
		ButtonOLD.SetRefreshEquip(true);
		UpdateStealth();
	}
	
	// Update character rect size when zooming
	public void  UpdateCurrentCharacterRect ()
	{
		if(zoom)
		{
			if(currentCharacterRect != CharacterZoomRect)
			{
				currentCharacterRect  = new Rect(CharacterRect.x,CharacterRect.y,CharacterRect.width+Screen.width*0.05f,CharacterRect.height+Screen.height*0.15f);
			}
		}
		else
		{
			if(currentCharacterRect != CharacterRect)
			{
				currentCharacterRect = new Rect(CharacterRect.x,CharacterRect.y,CharacterRect.width,CharacterRect.height - Screen.height*0.15f);
			}
		}
	}
	
	static float startDragCount; 
	static bool  goDrag = false;
	static bool  _drag= false;
	
	//=========LOD SYSTEM VARIABLES===========	
	private float _distance;
	private Vector3 _lastPosition;
	private float _lodDistanceResolution = 2;
	//========================================
	void  FixedUpdate ()
	{
		if((MapLoadDebug.assableMainPlayerEnd == 0) && (armory.IsAllLoaded()))
		{
			MapLoadDebug.assableMainPlayerEnd = Time.realtimeSinceStartup;
		}
		if((LoadSceneManager.LoadingStatus == LoadSceneManager.LOADING.LOADING_INPROGRESS) 
		   && (armory.IsAllLoaded())
		   && GameObject.Find("world/mesh"))
		{
			armory.EnableAllParts();
			LoadSceneManager.LoadingStatus = LoadSceneManager.LOADING.LOADING_IDLE;
			MapLoadDebug.loadingScreenEndTime = Time.realtimeSinceStartup;
		}
		
		if((LoadSceneManager.LoadingStatus != LoadSceneManager.LOADING.LOADING_INPROGRESS) && GameObject.Find("world/mesh"))
		{
			transform.parent.GetComponent<Rigidbody>().useGravity = true;
			if(MapLoadDebug.worldInstanceEndTime == 0)
			{
				MapLoadDebug.worldInstanceEndTime = Time.realtimeSinceStartup;
			}
		}
		else
		{
			transform.parent.GetComponent<Rigidbody>().useGravity = false;
		}
		
		base.FixedUpdate();
		
		UpdateTime();
		UpdateServerMovement();
		
		//===========LOD SYSTEM===========
		_distance = Vector3.Distance(_lastPosition, TransformParent.position);
		if(_distance > _lodDistanceResolution && WorldReference && (LoadSceneManager.LoadingStatus != LoadSceneManager.LOADING.LOADING_INPROGRESS))
	{
			//Debug.Log("updating... " + Time.time);
			_lastPosition = TransformParent.position;
			WorldReference.BroadcastMessage("UpdateLodMesh", SendMessageOptions.DontRequireReceiver);
		}
		//================================
		
		if(goDrag)
		{
			startDragCount -= Time.deltaTime;
			if(startDragCount < 0.0f)
			{
				if(Input.GetMouseButton(0))
				{
					_drag = true;
					goDrag = false;
				}
				else
				{
					_drag = false;
					goDrag = false;
				}
			}
		}
		
		if(gVault != null && gVault.showVault)
		{
			gVault.VaultDrag();
			gVault.UpdateAll();
		}
		
		
		moveStick.fixedUpdate();
		
		timeLeftToUpdateGroup -= Time.deltaTime;
		if(timeLeftToUpdateGroup > 0)
		{
			return;
		}
		else 
		{
			group.updatePlayersStats();
		}

		if( craftManager != null &&
		   craftManager.spellToCraft != 0 && 
		   !WorldSession.interfaceManager.castBarMng.gameObject.activeInHierarchy &&
		   !WorldSession.interfaceManager.castBarMng.flag)
		{
			if(craftManager.countCraft > 0)
			{
				WorldSession.interfaceManager.castBarMng.flag = true;
				playerSpellManager.CastSpell(craftManager.spellToCraft, this.guid, Vector3.zero);
				craftManager.countCraft--;
			}
			else
			{
				craftManager.spellToCraft = 0;
			}
		}
		
		GetWeaponSkillStats();
	}
	
	void  UpdateTouchInventory ()
	{
		touchPosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y); // mouse only for testing;
		if (currentCharacterRect.Contains(touchPosition)&&(Input.GetMouseButtonDown(0)||(characterRotationState==true&&Input.GetMouseButton(0)))) 
		{
			showCharacterStats = true;
			
			UpdateCharacterRotation();
			lastTouchPosition = touchPosition;
			characterRotationState = true;
		}
		else
		{
			characterRotationState = false;
		}
	}
	
	UIButton spellIconDrag = null;
	bool  classSpellDrag = false;
	bool  defaultSpellDrag = false;
	bool  usableItemDrag = false;
	Item entryDrag =null;
	
	void  UpdateCharacterRotation ()
	{
		
		Vector2 ret;
		float angle;
		
		if (characterRotationState == true)
		{
			//Transform cam = inventoryCamera.transform;
			
			if (lastTouchPosition.x < touchPosition.x)
			{
				angle=-3*Mathf.PI/180*(touchPosition.x-lastTouchPosition.x);
				
				ret.x=Mathf.Cos(angle)*inventoryCameraReff.localPosition.x - Mathf.Sin(angle)*inventoryCameraReff.localPosition.z;
				ret.y=Mathf.Cos(angle)*inventoryCameraReff.localPosition.z + Mathf.Sin(angle)*inventoryCameraReff.localPosition.x;
				
				inventoryCameraReff.localPosition = new Vector3(ret.x,inventoryCameraReff.localPosition.y,ret.y);
				inventoryCameraReff.LookAt(inventoryCameraReff.transform.parent.position+new Vector3(0,playerColider.height*0.5f,0));			
			}
			else if (lastTouchPosition.x > touchPosition.x)
			{
				angle=3*Mathf.PI/180*(lastTouchPosition.x-touchPosition.x);
				
				ret.x=Mathf.Cos(angle)*inventoryCameraReff.localPosition.x - Mathf.Sin(angle)*inventoryCameraReff.localPosition.z;
				ret.y=Mathf.Cos(angle)*inventoryCameraReff.localPosition.z + Mathf.Sin(angle)*inventoryCameraReff.localPosition.x;
				
				inventoryCameraReff.localPosition = new Vector3(ret.x,inventoryCameraReff.localPosition.y,ret.y);
				inventoryCameraReff.LookAt(inventoryCameraReff.transform.parent.position+new Vector3(0,playerColider.height*0.5f,0));			
			}
		}
	}
	
	
	void  LateUpdate ()
	{
		if(menuSelected == 1 )
		{
			if(!(interf.inventoryManager.itemInfoWindow != null && interf.inventoryManager.itemInfoWindow.isEnable))
			{
				UpdateTouchInventory();
			}
		}
		else if(menuSelected == 3)
		{
			//UpdateTouchSpells();
		}
		
		if((GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_BYTES_2) >> 24) == 1)
		{
			XPBoosted = true;
		}
		else
		{
			XPBoosted = false;
		}
		if(InspectMgr.active)
		{
			InspectMgr.UpdateRotation();
		}
	}
	
	//run one time vibration signal
	void  Vibrate ()
	{
		#if UNITY_IPHONE || UNITY_ANDROID || UNITY_EDITOR
		Handheld.Vibrate();
		#endif
	}
	
	MovementFlags flag = MovementFlags.MOVEMENTFLAG_BACKWARD;
	float lastOr;
	
	void  Update ()
	{
		base.Update();
		lootManager.Update();
		
		if((gSocialUI == null || gSocialUI.uiClass == null) && UIPrime31.instance != null)
		{
			gSocialUI = new SocialUI(this);
			gSocialUI.DrawSocialWindow();
		}
		mainCamTransform = Camera.main.transform;
		swipeScript = mainCamTransform.GetComponent<Swipe>();
		
		if(Application.isLoadingLevel)
		{
			if(!interfaceLoadedOnce)
			{ 
				interfaceLoaded = false;
				//interf=null;
				interfaceLoadedOnce = true;
				return;
			}
		}
		else
			interfaceLoadedOnce = false;
		
		if(!interf)
		{
			//	Debug.Log("Instantiate Interface");
			if(Application.loadedLevelName != "LoadingScreen")
			{
				interf = (GameObject.Find("Interface").AddComponent<ButtonOLD>() as ButtonOLD);
				interfaceLoaded = true;
				actionBarPanel.LoadHUDData();
				itemManager.DeactivateRange();
				
				UpdateStealth();
			}
		}
		
		_equipBarNumber = ButtonOLD.switchMode;
		
		cooldownManager.playerCooldownList.UpdateCooldowns();
		if(pet)
		{
			cooldownManager.petCooldownList.UpdateCooldowns();
		}
		
		if(OldHP != HP)
		{
			/*if(PlayerPrefs.GetInt("vibroButtonOption") == 0) //if vibration flag turned on
		{
			if(!IsInvoking("Vibrate")) //if the vibration signal can be carrying
			{
				if (HP/(MaxHP+0.001f)<=0.1f  && m_deathState == DeathState.ALIVE)
				{
					//run vibration 2 time per second
					InvokeRepeating("Vibrate",0,0.5f);
					Debug.Log("Start vibration");
				}
			}
		}*/
			
			interf.HP = HP;
			OldHP = HP;
		}
		
		/*if(IsInvoking("Vibrate"))
	{
		//if current life > 10% or player has the status of a ghost
		if (HP/(MaxHP+0.001f)>0.1f || m_deathState != DeathState.ALIVE)   
		{
			CancelInvoke("Vibrate"); 
			Debug.Log("Cancel vibration");
		}
	}*/
		
		if(OldMaxHP != MaxHP)
		{
			interf.MaxHP = MaxHP;
			OldMaxHP = MaxHP;
		}
		
		if(OldPower != Power)
		{
			interf.Power = Power;
			OldPower = Power;
		}
		
		if(OldMaxPower != MaxPower)
		{
			interf.MaxPower = MaxPower;
			OldMaxPower = MaxPower;
		}
		
		if(petGuid != 0 && pet == null)
		{
			pet = WorldSession.GetRealChar(petGuid) as Unit;
			if(pet != null)
			{
				petLvl = (int)pet.GetLevel();
				actionBarPanel.RefreshSlots();
			}
		}
		
		if(target != null && target.GetValuesCount() >= 0x94)
		{
			/*code for Portrait camera of target
		if(lastTarget != target.transformParent){
		    if(lastTarget){
		        targetPortraitCamera.GetComponent<PortraitCam>().SetLayer(targetPortraitCamera.GetComponent<PortraitCam>().previousLayer);
		        Destroy(targetPortraitCamera);
		    }
		    
		    targetPortraitCamera = (Instantiate(portraitCameraPrefab, target.transformParent.position, Quaternion.identity) as GameObject);
		    targetPortraitCamera.GetComponent<PortraitCam>().Target = target.transformParent;
		    
		    
		    lastTarget = target.transformParent;
		}
		*/
			if(OldGuid != target.guid) // || Application.isLoadingLevel
			{
				
				targetGUID = target.guid;
				OldGuid = target.guid;
				ButtonOLD.targetState = true;
				interf.targetGuid = target.guid;
				interf.targetName = target.name;
				interf.targetHP = (target as Unit).HP;
				interf.targetMaxHP = (target as Unit).MaxHP;
				interf.targetPower = (target as Unit).Power;
				interf.targetMaxPower = (target as Unit).MaxPower;
				ButtonOLD.refreshAura = true;
				//trolololo
				//UpdateTargetPortrait();
			}
			else
			{
				if(OldTargetHP != (target as Unit).HP)
				{
					OldTargetHP = (target as Unit).HP;
					interf.targetHP = (target as Unit).HP;
				}
				if(OldTargetMaxHP != (target as Unit).MaxHP)
				{
					OldTargetMaxHP = (target as Unit).MaxHP;
					interf.targetMaxHP = (target as Unit).MaxHP;
				}
				if(OldTargetPower != (target as Unit).Power)
				{
					OldTargetPower = (target as Unit).Power;
					interf.targetPower = (target as Unit).Power;
				}
				if(OldTargetMaxPower != (target as Unit).MaxPower)
				{
					OldTargetMaxPower = (target as Unit).MaxPower;
					interf.targetMaxPower = (target as Unit).MaxPower;
				}
			}
		}
		else
		{
			OldGuid = 0;
			ButtonOLD.targetState=false;
		}
		
		if(ButtonOLD.menuState)
		{
			Portrait.AreWeInMenu = true;
			if(!WeNeedToRefreshSnapShots)
			{
				WeNeedToRefreshSnapShots = true;
			}
		}
		else if(WorldSession.interfaceManager.hideInterface)
		{
			Portrait.AreWeInMenu = true;
		}
		else
		{
			if(WeNeedToRefreshSnapShots)
			{
				PortraitReff.UpdateSelfSnapShot();
				if(target)
					PortraitReff.UpdateTargetSnapShot(target.transform);
			}
			if(WeNeedToRefreshSnapShots)
			{
				WeNeedToRefreshSnapShots = false;
			}
			Portrait.AreWeInMenu = false;
		}
		
		_equipBarNumber = ButtonOLD.switchMode;
		if(_equipButtonDoubleSizeTime > 0)
		{
			_equipButtonDoubleSizeTime = Mathf.Max(0, _equipButtonDoubleSizeTime - Time.deltaTime);
		}
		
		//	UpdateCurrentCharacternew Rect();
		
		if(mainCamTransform == null)// just teleported
		{
			//	SetPosition(Pos,or);
			return;
		}
		
		//TODO Block the movement if the management server intercepted.
		if(!ButtonOLD.menuState && !chatOpend && !ButtonOLD.chatState && !isServerControlled && 
		   !WorldSession.interfaceManager.hideInterface && !tutorialInstance.isNeedBlockMovement())
		{
			if(Input.GetKey ("w"))
			{
				MoveStartForward();
				isMoving = true;
			}
			else if(Input.GetKey ("s"))
			{
				MoveStartBackward();
				isMoving = true;
			}
			
			if(Input.GetKeyUp ("w"))
			{
				MoveStop();
				isMoving = false;
				
			}
			if(Input.GetKeyUp ("s"))
			{
				MoveStop();
				isMoving = false;
			}
		}
		
		if(chatOpend)
		{
			MoveStop();
			isMoving = false;
		}
		
		if(target != null)
		{
			if((target).GetScale() >= 2 && target.guid != guid)
			{
				targetIndicator.transform.localScale = new Vector3((target).GetScale()*0.5f, (target).GetScale()*0.5f, 1);
			}
			else
			{
				targetIndicator.transform.localScale = Vector3.one;
			}
			targetIndicator.transform.position = target.TransformParent.position + new Vector3(0, 0.1f, 0);

			targetIndicator.SetActive(true);
			
			if(target != newTarget)//daca obiectul selectat a ramas acelasi atunci nu facem nimic, 
				//in caz contrar scalam 'targetIndicator' pt noul obiect selectat
			{
				newTarget = target;
				
				int[] playerFactionID ;
				int[] friendlyFactionID ;
				int[] neutralFactionID ;	
//				int[] agresiveFactionID ;
				
				int playerFaction = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE);
				
				if ( playerFaction == 1 || playerFaction == 4 || playerFaction == 3 ) // Alliance FACTIONTEMPLATE id
				{
					playerFactionID = new int[]{ 1, 4, 3 };
					friendlyFactionID = new int[]{ 35, 2007 };
					neutralFactionID = new int[]{ 0, 25, 32 };	
//					agresiveFactionID = new int[]{ 73, 413, 18, 14, 514, 44, 37, 63, 18 };
				}
				else
				{
					playerFactionID = new int[]{ 1610, 2, 116 };
					friendlyFactionID = new int[]{ 35, 2007 }; // unknown if all questgivers / somesuch are all 35 
					neutralFactionID = new int[]{ 0, 25, 32 };	// surely not complete
//					agresiveFactionID = new int[]{ 73, 413, 18, 14, 514, 44, 37, 63, 18 }; // and many /any other..
				}
				
				
				int rel = (int)target.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE);
				
				//Debug.Log(" TARGET FACTIONTEMPLATE " + rel + "  ");
				//Debug.Log(" Player FACTIONTEMPLATE" + playerFaction);
				
				byte i;
				
				byte n = 0; 
				
				for(i = 0;i<playerFactionID.Length;i++)
					if(playerFactionID[i] == rel)
				{
					n = 1;
				}			  	
				
				for(i = 0;i<friendlyFactionID.Length;i++)
					if(friendlyFactionID[i] == rel && n==0)
				{
					n = 1; 
				}	
				
				for(i = 0;i<neutralFactionID.Length;i++)
					if(neutralFactionID[i] == rel && n==0)
				{
					n = 2; 
				}			
				
				//				for(i = 0;i<playerFactionID.Length;i++)
				//				  if(agresiveFactionID[i] == rel && n==0)
				//				  {
				//					n = 3; 
				//				  }	
				
				switch(n)
				{
					
				case 1:  targetIndicatorRenderer.sharedMaterial.SetTexture("_MainTex",selectTargetF);
					break;	
				case 2:  targetIndicatorRenderer.sharedMaterial.SetTexture("_MainTex",selectTargetN);
					break;						
				default:  targetIndicatorRenderer.sharedMaterial.SetTexture("_MainTex",selectTargetA);	
					break;
				}
			}
		}
		else
		{
			targetIndicator.SetActive(false);
			lootOnce=false;
			ButtonOLD.lootState=false;
			itemLoot = null;
			PortraitReff.UpdateTargetSnapShot(null);
			
		}
		
		float aux = refreshTimeLeft + refreshTime;
		if(aux <= Time.timeSinceLevelLoad)
		{
			refreshTimeLeft=aux;
			if((movementFlags & moveFlag) > 0)
			{
				SendHeartBit();
			}
		}
		
		if(Input.GetKeyDown ("space") && menuSelected == -1 && !chatOpend && CanJump && !WorldSession.interfaceManager.hideInterface)
		{
			StartJump();
		}
		
		if(moveFlag > 0 && !WorldSession.interfaceManager.hideInterface && !tutorialInstance.isNeedBlockMovement())
		{
			flag &= (MovementFlags)moveFlag;
			lastOr = TransformParent.rotation.eulerAngles.y;
			TransformParent.rotation = startRot;
			if(Global.goToCameraLook)
			{
				//swipeScript.b+=moveStick.angle*0.1f; 
			}
			Quaternion tmp = TransformParent.rotation;
			if(isUnderWater)
			{
				tmp.eulerAngles = mainCamTransform.rotation.eulerAngles;
				TransformParent.rotation = tmp;
			}
			else
			{
				tmp.eulerAngles = new Vector3(0, mainCamTransform.rotation.eulerAngles.y, 0);
				TransformParent.rotation = tmp;
			}
			/*if(!isSwimming)
		{
			aHandler.PlayMovementAnimation("run");
			Debug.Log("playing run anim........");
		}
		else
		{
			if(isUnderWater)
			{transformParent.rotation.eulerAngles = mainCamTransform.rotation.eulerAngles;
			Debug.Log("playing under water anim........");
			}
			if(flag)
			aHandler.PlayMovementAnimation("swim_back");
			else
			aHandler.PlayMovementAnimation("swim");
		}*/
			//print(moveStick.angle*180/Mathf.PI);
			//float angle = mainCamTransform.rotation.eulerAngles.y - moveStick.angle*180/Mathf.PI; 
			//print(angle);
			//print(mainCamTransform.rotation.eulerAngles.y);
			if(!Global.goToCameraLook)
				TransformParent.Rotate(0,-moveStick.angle*180/Mathf.PI,0);
			if(Mathf.Abs(TransformParent.rotation.eulerAngles.y-lastOr)>3)
			{
				//print("send rotation pos " + ion++);
				SendHeartBit();
			}
			
			//some client hacks to set different speeds.We have to make the server to adjust speed
			
			/*if(flag)
		goto = new Vector2(0,-movespeed/2);
		else
		goto = new Vector2(0,movespeed);
		if(charMount)
		goto = new Vector2(0,movespeed*2);
		
		Vector3 goto3 = goto.y*(transformParent.position - lookPoint.transform.position);
		
		goto=Rotate(-transformParent.rotation.eulerAngles.y,goto,Vector2.zero);
		
		//Transform trans;
		//trans.position = new Vector3(0,0,movespeed);
		//trans.Rotate(transformParent.rotation.eulerAngles);
		if(!collControl.ignoreVelocity)// && dontIgnoreThisFrameVelocity==5)
		{
			//dontIgnoreThisFrameVelocity = 0;
			//if(isUnderWater)
			//{
			parentRB.velocity.x=goto3.x;
			//parentRB.velocity.y=goto3.y;
			parentRB.velocity.z=goto3.z;
			//}
			//else
			//{
			//parentRB.velocity.x=goto.x;
			//parentRB.velocity.z=goto.y;
			//}
		}
		else 
		{
			//Vector3 newGoto
			goto+=collControl.normal2D*movespeed;
			if(goto.sqrMagnitude>(movespeed*movespeed))
			{
				goto.Normalize();
				goto*=movespeed;
			}
			parentRB.velocity.x=goto.x;
			parentRB.velocity.z=goto.y;
		}*/
			//parentRB.velocity.x=goto3d.x;
			//	if(isUnderWater)
			//	parentRB.velocity.y=goto3d.y;
			//	parentRB.velocity.z=goto3d.z;
			
		}
		else
		{
			parentRB.velocity = new Vector3(0, parentRB.velocity.y, 0);
			
			/*if(!isSwimming)
		{
			if(!CanJump)
				aHandler.PlayMovementAnimation("jump");
			else
				aHandler.PlayMovementAnimation("idle");
		}
		else
			aHandler.PlayMovementAnimation("swim_idle");*/
		}
		
//		if(mountManager.currentMount)
//		{
//			float angle = Vector3.Angle(transformParent.up,slopeNormal);
//			//Debug.Log("update mount ");   
//			if (angle>0.01f && angle < 30.0f)
//			{
//				Vector3 axis= Vector3.Cross(transformParent.up,slopeNormal).normalized;
//				Quaternion rot = Quaternion.AngleAxis(angle,axis);
//				//transformParent.rotation = rot * transformParent.rotation;
//				// targetIndicator.transform.rotation = rot * transformParent.rotation;
//			}
//		}
		/*
	if(portrait_cam_time > 0)
		portrait_cam_time--;
	if(target_cam_time > 0)
		target_cam_time--;
	*/
		if(lastSceneLoaded != mapId && !interfaceLoadedOnce)
		{	
			checkMiniMapAfterItsInitialised();
		}
		
		if(!compassMiniMap)
		{
			Transform compassTarnsform = Camera.main.transform.Find("Compass");
			if(compassTarnsform)
				compassMiniMap = compassTarnsform.gameObject;
			else
				return;
		}
		if(ShowMinimap)
			compassMiniMap.SetActive(false);
		
	}
	void  checkMiniMapAfterItsInitialised ()
	{	
		if(!GameObject.Find("world/MinimapCamera"))
			_showMenuButton=true;
		else
			_showMenuButton=false;
		
		lastSceneLoaded = mapId;
	}

	public void  StartJump ()
	{
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_FALLING;
		moveFlag = moveFlag | flag;
		flag = MovementFlags.MOVEMENTFLAG_FORWARD;
		if((moveFlag | flag) == 0)
		{
			BuildPkt(OpCodes.MSG_MOVE_JUMP);
		}
	}
	
	public void  AddChatInput (  string val )
	{
		Message msg = new Message();
		msg.message = val;
		AddChatInput( msg );
	}
	
	public void  AddChatInput ( Message val )
	{
		if ( val.CheckIfItsShowable() == false )
		{
			return;
		}
		val = RemoveTag(val);
		allText.Add(val);
		_chatScrollVector2.y+=2000;     // This will force the chatview to focus on the bottom text, leaving the old lines of text behind(up).- Sebek
	}
	
	public void  AddChatInput (  string sender ,   string message )
	{
		AddChatInput(new Message(sender,"",message));
	}
	
	Message getLastMessageOfType ( ChatMsg type )
	{
		Message ms;
		for(int i = allText.Count - 1; i >= 0; i++)//message ms in allText)
		{
			ms = allText[i];
			if(ms == null)
				continue;
			if(ms.type == type)
				return ms;
		}
		return null;
	}
	
	Message  RemoveTag ( Message val )
	{
		string msg = val.message;
		int pos = 0;
		int poz = 0;
		bool  editFlag = true;
		
		if(msg.Length == 0)
		{
			return val;
		}
		
		while(editFlag)
		{
			if(msg.Contains("|c"))
			{
				pos = msg.IndexOf("|c", 0);
				msg = msg.Remove(pos, 10);
			}
			else if(msg.Contains("|r"))
			{
				pos = msg.IndexOf("|r", 0);
				msg = msg.Remove(pos, 2);
			}
			else if(msg.Contains("|h"))
			{
				pos = msg.IndexOf("|h", 0);
				poz = msg.IndexOf("|h", pos + 1);
				msg = msg.Remove(pos, poz - pos + 2);
			}
			else if(msg.Contains("|H"))
			{
				pos = msg.IndexOf("|h", 0);
				msg = msg.Remove(pos, 2);
			}
			else
			{
				editFlag = false;
			}
		}
		
		val.message = msg;
		return val;
	}
	
	static public string LastChannel = null;
	
	public string SendMessage2 ( string s )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_MESSAGECHAT);
		uint aux32 = 0;
		uint lang = 0;
		//if ( PlayerEnum.getFactionByRace( race ) == FACTION.FURY )
		//	lang = 669;
		//else
		//	lang = 668;
		lang = ByteBuffer.Reverse(lang);
		string ret = "";
		bool  IsGMCommand = false;
		if(s == "")
			s = " ";
		if (s[0] == '.')
		{
			int start=-1;
			int stop=-1;
			int stopcom = -1;
			string number = string.Empty;
			int no;
			bool  commandTrue =true;
			for (i = 1; i < s.Length; i++)
			{
				if(s[i]==' ')
				{
					stopcom=i;
					break;	
				}
				
			}
			if (stopcom == -1)
				stopcom=s.Length;
			switch (s.Substring(0, stopcom))
			{
			case ".addgo":
				break;
			case ".additem":
				break;
			case ".learn":
				break;
			case ".npc":
				break;
			default:
				commandTrue = false;
				break;
			}
			
			if(commandTrue)
			{
				for (i = 1; i < s.Length; i++)
				{
					switch (s[i])
					{
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
						start=i;
						break;
					}
					if (start!=-1)
						break;
				}
				if(start !=-1)
				{
					for( i = start; i < s.Length; i++)
					{
						switch (s[i])
						{
						case '0':
							break;
						case '1':
							break;
						case '2':
							break;
						case '3':
							break;
						case '4':
							break;
						case '5':
							break;
						case '6':
							break;
						case '7':
							break;
						case '8':
							break;
						case '9':
							break;
						default:
							stop=i-1;
							break;
						}
						if(stop != -1)
						{
							if(s[stop+1] != ' ')
							{
								start = -1;
								stop = -1;
							}
							break;
						}
					}
					//end for
					if(start != -1 && stop == -1)
					{
						stop = s.Length-1;
					}
					if(start != -1 && stop != -1)
					{
						number = s.Substring(start, stop-start+1);
					}
				}
				
				if(number != null)
				{
					no = int.Parse(number);
					no = (no^243)>>2;
					s = string.Concat(s.Substring(0, start), "" + no, s.Substring(stop+1));
				}
			}		
		}
		if ( s[0] == '.' )
		{
			s = "/say " + s;
			IsGMCommand = true;
		}
		if ( s[0] != '/' && LastChannel != null )
		{
			s = LastChannel + " " + s;
		}
		if(s[0] == '/')
		{
			string channel;
			int index = s.IndexOf(" ");
			if(index!=-1)
			{
				channel = s.Substring(0,index);
				s = s.Substring(index+1);
			}
			else 
			{
				channel = s;
				s = "";
			}
			switch(channel)
			{
			case "/s":
			case "/say":
				ret = "";
				aux32 = (uint)ChatMsg.CHAT_MSG_SAY;
				aux32 = ByteBuffer.Reverse(aux32);
				pkt.Append(aux32);
				aux32 = 0;
				pkt.Append(lang);
				if ( !IsGMCommand )
				{
					LastChannel = channel;    // change last channel 
				}
				break;
			case "/y":
				ret = "/y ";
				aux32 = (uint)ChatMsg.CHAT_MSG_YELL;
				aux32 = ByteBuffer.Reverse(aux32);
				pkt.Append(aux32);
				aux32 = 0;
				pkt.Append(lang);
				break;
			case "/guild":
			case "/g":
				ret = "/g ";
				aux32 = (uint)ChatMsg.CHAT_MSG_GUILD;
				aux32 = ByteBuffer.Reverse(aux32);
				pkt.Append(aux32);
				aux32 = 0;
				pkt.Append(lang);
				if(!IsInGuild())
				{
					AddChatInput("You are not in a guild");
				}
				if(IsInGuild())
				{
					LastChannel = channel;    // change last channel    -if present turns your normal chat into /g chat without needing to type "/g" - Sebek
				}
				break;
			case "/b":
				ret = "/b ";
				aux32 = (uint)ChatMsg.CHAT_MSG_BATTLEGROUND;
				aux32 = ByteBuffer.Reverse(aux32);
				pkt.Append(aux32);
				aux32 = 0;
				pkt.Append(lang);
				break;
			case "/p":
				ret = "/p ";
				aux32 = (uint)ChatMsg.CHAT_MSG_PARTY;
				aux32 = ByteBuffer.Reverse(aux32);
				pkt.Append(aux32);
				aux32 = 0;
				pkt.Append(lang);
				LastChannel = channel;    // change last channel 
				break;
			case "/w":
				ret = "/w ";
				aux32 = (uint)ChatMsg.CHAT_MSG_WHISPER;
				aux32 = ByteBuffer.Reverse(aux32);
				pkt.Append(aux32);
				aux32 = 0;
				pkt.Append(lang);
				index = s.IndexOf(" ");
				if(index!=-1)
				{
					channel = s.Substring(0,index);/// wisp to
					s = s.Substring(index+1);
				}
				else 
				{
					channel = s;
					s = "";
				}
				pkt.Append(channel);
				break;
			case "/t":
				ret = "/t ";
				aux32 = (uint)ChatMsg.CHAT_MSG_WHISPER;
				aux32 = ByteBuffer.Reverse(aux32);
				pkt.Append(aux32);
				aux32 = 0;
				pkt.Append(lang);
				index = s.IndexOf(" ");
				if(index!=-1)
				{
					channel = s.Substring(0,index);/// wisp to
					s = s.Substring(index+1);
				}
				else 
				{
					channel = s;
					s = "";
				}
				pkt.Append(channel);
				break;
			case "/r": /////////////////// TO DO
				ret = "/r ";
				Message ms = getLastMessageOfType(ChatMsg.CHAT_MSG_WHISPER);
				if(ms == null)// no wisp to response to
					return null;
				aux32 = (uint)ChatMsg.CHAT_MSG_WHISPER;
				aux32 = ByteBuffer.Reverse(aux32);
				pkt.Append(aux32);
				aux32 = 0;
				pkt.Append(lang);
				pkt.Append(ms.sender);
				break;
			case "/leave":
				index = s.IndexOf(" ");
				if(index!=-1)
				{
					channel = s.Substring(0,index);
				}
				else
				{
					channel = s;
				}
				if(channel == s)
				{
					session.handleOpcode(OpCodes.CMSG_GROUP_DISBAND,null);
					/*pkt.SetOpcode(OpCodes.CMSG_LEAVE_CHANNEL);
					aux32 = 0;
					pkt.Append(aux32);
					pkt.Append(channel);*/
				}
				else
				{
					ChannelM.LeaveChannel( channel );
				}
				ret = "";
				break;
			case "/join":
				string[] accPass = Regex.Split(s," ");
				string ChName = ChannelM.UpdateKeyNameChannel( accPass[0] );
				if(ChName.Length >=1)
				{
					channel ch = null;
					try
					{
						ch = ChannelM.getChannel(ChName);
					}
					catch(  InvalidOperationException ex ) 
					{	
						AddChatInput( ex.Message );
						break;
					}
					
					ChannelM.SendJoinChannelToServer( ch.id, ChName);

//					try{
//						channel ch = channels.getChannel(accPass[0]);
//					}catch(  InvalidOperationException ex ) 
//					{
//						AddChatInput( ex.Message );
//						break;
//					}
//					//channels.LogChannels();
//					pkt.SetOpcode(OpCodes.CMSG_JOIN_CHANNEL);
//					pkt.Append( ByteBuffer.Reverse( ch.id ));
//					byte unk = 0;
//					pkt.Append(unk);
//					pkt.Append(unk);
//					pkt.Append(accPass[0]);
//					if(accPass.length >= 2)
//						pkt.Append(accPass[1]);
//					else pkt.Append("");
//					RealmSocket.outQueue.Add(pkt);
				}
				ret = "/"+accPass[0] + " ";
				break;
			case "/invite":
				index = s.IndexOf(" ");
				if(index!=-1)
				{
					channel = s.Substring(0,index);//reuse the variable
				}
				else
				{
					channel = s;
				}
				//print(channel);
				pkt.SetOpcode(OpCodes.CMSG_GROUP_INVITE);
				pkt.Append(channel);
				pkt.Append(aux32);//random value
				RealmSocket.outQueue.Add(pkt);
				ret = "";
				break;
			case "/kick":
				index = s.IndexOf(" ");
				if(index!=-1)
				{
					channel = s.Substring(0,index);//reuse the variable
				}
				else
				{
					channel = s;
				}
				pkt.SetOpcode(OpCodes.CMSG_GROUP_UNINVITE);
				pkt.Append(channel);
				RealmSocket.outQueue.Add(pkt);
				ret = "";
				break;
				//case "/test":
				//	channels.SaveChannelsToPlayerPrefs(name);
				//	break;
			case "/leaveall":
				ChannelM.LeaveAllChannels();
				break;
			case "/channels":
				ChannelM.SortChannelsById();
				for(i = 0 ; i<ChannelM.channels.Count;i++)
				{
					//if ( channels.channels[i].GetActive() )
					//Debug.Log("message be here:"+ChannelM.channels[i].MakeString());
					AddChatInput( ChannelM.channels[i].MakeString() );
				}
				break;
			case "/gcreate":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				guildmgr.CreateGuild( s );
				break;
			case "/ginvite":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				guildmgr.InvitePlayer( s );
				break;
			case "/gaccept":
				guildmgr.AcceptInvite();
				break;
			case "/gdecline":
				guildmgr.DeclineInvite();
				break;
			case "/ginfo":
				guildmgr.RequestInfo();
				break;
			case "/groster":
				guildmgr.RequestRanksInfo( this );  // req info for rank names
				guildmgr.GuildRoster();
				break;
			case "/gleave":
				guildmgr.LeaveGuild();
				break;
			case "/gpromote":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				guildmgr.SendPromotion( s );
				break;
			case "/gdemote":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				guildmgr.SendDemote( s );
				break;
			case "/gleader":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				guildmgr.ChangeLeader( s );
				break;
			case "/gmotd":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				guildmgr.SetMOTD( s );
				break;
			case "/ginfotext":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				guildmgr.SetInfo( s );
				break;
			case "/granks":
				guildmgr.RequestRanksInfo( this );
				break;
			case "/gchrank":
				var values= Regex.Split(s," ");
				if ( values.Length < 2 )
				{
					AddChatInput(new Message("","","Invalid arguments"));
					break;
				}
				//guild.EditRankByIndex( int.Parse(values[0]), values[1]);
				break;
			case "/gcheck":
				IsInGuild();
				break;
			case "/gremove":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				guildmgr.RemoveMember( s );
				break;
			case "/gaddrank":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				guildmgr.AddRank( s );
				break;
			case "/gdelrank":
				guildmgr.DeleteLowestRank();
				break;
			case "/gsetpnote":
				var valuesnote= Regex.Split(s," ");
				if ( valuesnote.Length < 2 )
				{
					AddChatInput(new Message("","","Invalid arguments"));
					break;
				}
				guildmgr.SetPlayerNote( valuesnote[0], valuesnote[1]);
				break;
			case "/gdisband":
				guildmgr.DisbandGuild();
				break;
			case "/glog":
				guildmgr.RequestGuildEventLog();
				break;
			case "/battle":
				if(target != null && target.IsPlayer())
				{
					duel.Request(this, (Player)target);
					break;
				}
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				Player opp;
				opp = (Player)session.GetPlayerByName(s);
				duel.Request(this, opp);
				break;
			case "/forfeit":
				UIButton tempButton = null;
				duel.Cancel(tempButton);
				break;
			case "/friend":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				SocialMgr.SendAddFriend(s, "");
				break;
			case "/ignore":
				index = s.IndexOf(" ");
				if(index!=-1 || s == "")
					break;
				SocialMgr.SendAddIgnore(s);
				break;
			default :
				for(i = 0 ; i<ChannelM.channels.Count;i++)
				{
					if(ChannelM.channels[i] == null)
						continue;
					if(channel == ("/"+ChannelM.channels[i].id)) // find channel
					{
						LastChannel = channel;    // change last channel 
						break;
					}
				}
				if(i == ChannelM.channels.Count) // no channel known
				{
					for(i = 0;i<ChannelM.channels.Count;i++)
					{
						if(ChannelM.channels[i] == null)
							continue;
						if(channel == "/"+ChannelM.channels[i].name)
							break;
					}
				}
				if(i == ChannelM.channels.Count)
				{
					// send it like chat message
					aux32 = (uint)ChatMsg.CHAT_MSG_SAY;
					aux32 = ByteBuffer.Reverse(aux32);
					pkt.Append(aux32);
					aux32 = 0;
					pkt.Append(lang);
					pkt.Append(channel+" "+s);   
					RealmSocket.outQueue.Add(pkt);
					return "";
				}
				//print("channel message");
				ret = "/" + ChannelM.channels[i].name + " ";
				aux32 = (uint)ChatMsg.CHAT_MSG_CHANNEL;
				aux32 = ByteBuffer.Reverse(aux32);
				pkt.Append(aux32);
				aux32 = 0;
				pkt.Append(lang);
				pkt.Append(ChannelM.channels[i].name);
				LastChannel = channel;    //not sure if this goes here, but hey! it works:)) -Sebek
				break;
			}
			if(s.Length <= 0 || (s[0] == '/' && channel == "/leave" && channel == "/join" && channel == "/invite" && channel == "/kick"))
			{
				return ret;
			}
		}
		else
		{
			//print("new message");
			aux32 = (uint)ChatMsg.CHAT_MSG_SAY;
			aux32 = ByteBuffer.Reverse(aux32);
			pkt.Append(aux32);
			aux32 = 0;
			pkt.Append(lang);
		}
		//print(s);
		s = s.Replace("\r\n", "\n");
		s = s.Replace("\r", "\n");
		pkt.AppendChatString(s);
		//Debug.Log("Am trimis: " + s);
		RealmSocket.outQueue.Add(pkt);
		
		//scrollPosition = (lines-nrOfVisibleLines/2)*lineHeight; 
		
		return ret;
		
		/*
		void WorldSession::SendChatMessage(uint32 type, uint32 lang, std::string msg, std::string to)
		{
			if((!_logged) || msg.empty())
				return;
			WorldPacket packet;
			packet << type << lang;
			switch(type){
				case CHAT_MSG_WHISPER:
				    if(to.empty())
				        return;
					packet << to << msg;
					break;
				case CHAT_MSG_CHANNEL:
				    if(to.empty() || !_channels->IsOnChannel(to))
				        return;
					packet << to << msg;
					break;
				default:
				    packet << msg;
			}
			packet.SetOpcode(CMSG_MESSAGECHAT);
			SendWorldPacket(packet);
		}
	*/
		
	}
	
	void  ChatWindow ( int id )
	{
		//messageTextStyle.wordWrap = true;
		
		//==
		//InvisibleStyle.verticalScrollbar.thumb = InvisibleStyle.verticalScrollbarThumb;
		GUI.skin = InvisibleStyle;
		GUILayout.BeginVertical();
		//_chatScrollVector2= GUI.VerSkin ticalScrollbar (0,0,0);   -merge experimentat cu asta
		_chatScrollVector2 = GUILayout.BeginScrollView(_chatScrollVector2);//, InvisibleStyle.horizontalScrollbar, InvisibleStyle.verticalScrollbar);//, InvisibleStyle);   //- Sebek
		for(i = 0; i <= allText.Count - 1; i++) // Adds the new lines of text in one direction or another   
			//for(i= allText.Count-1;i>=0;i-- ) - old one, text will appear on top and descend
		{
			ms = allText[i];
			_s = ms.getAllText();
			
			skin.normal.textColor = ms.messageColor;
			GUILayout.Label("" + _s, skin);	
			
			if(allText.Count >= 30) // Counter for lines of text to keep in chat "history", everything beyond that number is deleted - Sebek
			{
				allText.RemoveAt(0);
			}
		}	
		GUI.skin = null;
		GUILayout.EndScrollView();	
		GUILayout.EndVertical();
	}
	
	void  messageWnd ( int id )
	{
		MessageWnd mW = windows[id];
		if(mW.OnGUI != null)
		{
			mW.OnGUI(mW);
			return;
		}
		/// default window
		GUI.Label( new Rect(0,20,mW.rect.width,mW.rect.height),mW.text);
		if(mW.counterValues>=0)
		{
			if(time == 0)
				time = Time.timeSinceLevelLoad;
			float delta = Time.timeSinceLevelLoad-time;
			time = Time.timeSinceLevelLoad;
			mW.counterValues-=delta;
			int value = (int)mW.counterValues;
			GUI.Label( new Rect(0, 80, mW.rect.width, mW.rect.height), ""+value);
		}
		if(GUI.Button( new Rect(mW.rect.width*0.8f, mW.rect.height-20, mW.rect.width*0.2f, 20), "YES"))
		{
			time = 0;
			mW.Yes();
			session.handleOpcode(mW.getOpcode(),null);
			windows.RemoveAt(id);
		}
		if(GUI.Button( new Rect(0,mW.rect.height-20,mW.rect.width*0.2f,20),"NO"))//20 px height
		{
			time = 0;
			mW.No();
			session.handleOpcode(mW.getOpcode(),null);
			windows.RemoveAt(id);
		}
	}
	
	//da refresh la inventar
	public void  RefreshInventory ( int meniu  ,  int submeniu )
	{
		if(meniu > 0 && submeniu == 0)
		{
			Meniu = meniu;
			
			itemType = null;
			
			
			//			classtype  = 0;//george era cu t mare
			
			//	classtype  = -1;//george era cu t mare
			
			subMeniu = -1;
			itemStatTypes.strg = 0;
			itemStatTypes.intl = 0;
			itemStatTypes.agil = 0;
			itemStatTypes.descr = "";
			nameItemEquiped = "";
			itemSelected = null;
			bagSelected = null;
			stateItem = false; 
		}
		else if(meniu==0)
		{
			subMeniu = submeniu;
			itemStatTypes.strg = 0;
			itemStatTypes.intl = 0;
			itemStatTypes.agil = 0;
			itemStatTypes.descr = "";
			nameItemEquiped = "";
			itemSelected = null;
			bagSelected = null;
			stateItem = false;
		}
		else
		{
			itemType = null;
			
			
			//			classtype = 0;//george era cu t mare
			
			//	classtype = -1;//george era cu t mare
			
			Meniu = -1;
			subMeniu = -1;
			itemStatTypes.strg = 0;
			itemStatTypes.intl = 0;
			itemStatTypes.agil = 0;
			itemStatTypes.descr = "";
			nameItemEquiped = "";
			itemSelected = null;
			bagSelected = null;
			stateItem = false;
		}
		
	}

	void  showMainMeniu ()
	{
		switch(menuSelected)
		{
		case -1:
			break;
		case 0: 
			meniuOpend = false;
			ButtonOLD.menuState = false; 
			menuSelected = -1;
//			itemHint = -1;
			break;
		case 1:
			showInventory();
			break;
			//case 2:
			//showQuests();
			//break;
		case 3:
			showSpells();
			break;
		case 4:
			//Debug.Log("APASAT GUILD");
			showGuild();
			break;
		case 6:
			menuSelected=-1;
			break; 	
		case 7:
			//showOptions();
			break;				
		case 8:
			PurchaseManager.platID = 0;	 //0 is for Google Play	
			purchaseManager.DrawMithrilShop( this );
			break;
		case 9:
			PurchaseManager.platID = 1;//1 is for Amazon
			purchaseManager.DrawMithrilShop( this );
			break;
		case 11:
//			Debug.Log("apasat traits");
			break;
		case 101:
			gSocialUI.ShowSocial();
			break;
		}
	}
	
	void  ResetStatsRect ()
	{	
		StatsRect = new Rect(equipSize,4.8f*Screen.height/6,Screen.width/2-equipSize*2,1.1f*Screen.height/6);
		SmallerStatsRect = new Rect(equipSize*1.5f,4.9f*Screen.height/6,Screen.width/2-equipSize*3.0f,1.0f*Screen.height/6);
	}
	
	void  showCaracterInInventory ()
	{	
//		int lvl = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_LEVEL);	
		//GUI.Box(currentCharacterRect, name+ " - Level: "+lvl);
		
		GUI.DrawTexture(currentCharacterRect, tex, ScaleMode.StretchToFill, true);
		
		//Transform cam = GameObject.Find("inventoryCamera(Clone)").transform;
		float addY = playerColider.height;
		if (firstTimeCheck)
		{
			firstTimeCheck = false;
			if (mountManager.IsMounted()) 
			{	 
				inventoryCameraReff.localPosition *= 4; 
				inventoryCameraReff.LookAt(inventoryCameraReff.transform.parent.position+new Vector3(0,addY*0.5f,0));
			}
		}
	}
	
	void  showEquipSlots ()
	{
		
	}
	
	// Draw the right half of the inventory
	
	
	
	void  showAllAboutBagsInventory ()
	{
		//GUI.Box(BagSpotsRect,"");
		//GUI.Box(BagRect,"");
		//GUI.Box(InsideBagRect,"");
		
		int i = 0;
//		int j;
		nrofBoxes = itemManager.Items.Count;
		// Draw 7 bags
		
//		string bagname = "";
		currentBag = interf.inventoryManager.curBag;
		
//		int z = 0;
//		bool  isBreakTime = false;
		
		// Draw selected bag components	
		//itemele din bagul default au slotul de la 24 la 39.
		for(i = 0;i<32;i++)
		{
			inventoryPosition[i]= -1;
		}
		
		for(i = 0;i<itemManager.Items.Count;i++)
		{ 
			Item item = itemManager.Items[i];
			
			if((item.slot>=23 && item.slot<=38 && currentBag == 0 && item.bag==255) || (item.bag == currentBag && currentBag != 0))
			{
				if(currentBag == 0)
				{
					inventoryPosX = (item.GetInventorySlot()) % boxesPerWidth;
					inventoryPosY = (item.GetInventorySlot()) / boxesPerHeight;
					inventoryPosition[item.GetInventorySlot()] = i;
				}
				else
				{
					inventoryPosX = (item.GetInventorySlot()) % boxesPerWidth;
					inventoryPosY = (item.GetInventorySlot()) / boxesPerHeight;
					inventoryPosition[item.GetInventorySlot()] = i;
				}
				
				if (itemSelected != null && item.guid == itemSelected.guid)
					currentnewstyle = newskin.customStyles[1];
				else
					currentnewstyle = newskin.customStyles[0];
				
			}
		}
		
//		if (itemHint != -1) 
//		{
//			GUI.DrawTexture(itemHintRect, itemHintTexture); 
//			GUILayout.BeginArea(itemHintRect);
//			if(dragItem != null && dragItem.stats != null)		
//				GUILayout.Label(dragItem.stats, newskin.label);
//			/*GUILayout.Label("Type: "+dragItem.getClassName(), newskin.label);
//			GUILayout.Label("Subtype: "+dragItem.getSubClassName(), newskin.label);
//			GUILayout.Label("Required Level: "+dragItem.reqLvl.ToString(), newskin.label);*/
//			GUILayout.EndArea();
//		}
	}
	
	void  showCharacterStatsOriginal ()
	{
		GUI.Box(StatsRect ,"", newskin.box);
		
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10, StatsRect.y+StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"Armour: "+armour);//,newskin.label);
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10, StatsRect.y+2*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"Exp: "+ 
		          GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_XP)+"/"+GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_NEXT_LEVEL_XP),newskin.label);
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10, StatsRect.y+3*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"MIGHT: "+ 
		          GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT0),newskin.label);
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10, StatsRect.y+4*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"DEX: "+ 
		          GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT1),newskin.label);
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10, StatsRect.y+5*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"VIT: "+ 
		          GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT2),newskin.label);		
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10, StatsRect.y+6*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"PWR: "+ 
		          GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT3),newskin.label);	
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10+StatsRect.width/2, StatsRect.y+StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"WIS: "+ 
		          GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT4),newskin.label);	
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10+StatsRect.width/2, StatsRect.y+2*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7), "HP: "+
		          GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_HEALTH)+"/"+GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXHEALTH),newskin.label);	
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10+StatsRect.width/2, StatsRect.y+3*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7), "MP: "+
		          GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POWER1)+"/"+GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXPOWER1),newskin.label);	
		float minD = GetFloatValue((int)UpdateFields.EUnitFields.UNIT_FIELD_MINDAMAGE);
		float maxD = GetFloatValue((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXDAMAGE);
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10+StatsRect.width/2, StatsRect.y+4*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"Dmg: "+minD	+" - "+maxD,newskin.label);	
		minD = GetFloatValue((int)UpdateFields.EUnitFields.PLAYER_DODGE_PERCENTAGE);
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10+StatsRect.width/2, StatsRect.y+5*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"Dodge: "+minD+"%",newskin.label);	
		UpdateFields.EUnitFields mic = UpdateFields.EUnitFields.PLAYER_CRIT_PERCENTAGE;

		minD =	GetFloatValue((int)mic);
		GUI.Label( new Rect(StatsRect.x+StatsRect.width/10+StatsRect.width/2, StatsRect.y+6*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"Critical: "+minD+"%",newskin.label);	
		//	GUI.Label( new Rect(StatsRect.x+StatsRect.width/10+StatsRect.width/2, StatsRect.y+7*StatsRect.height/7, StatsRect.width/2, StatsRect.height/7),"Armour: "+armour ,newskin.label);	
		
		/*money = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COINAGE);
	s = money/10000;
	c = (money-s*10000)/100;
	m = (money-s*10000 - c*100);*/
		//PLAYER_FIELD_COINAGE 
		/*GUI.Label( new Rect(Screen.width*0.01f,tabMainHeight*5.4f, tabMainWidth*2.0f, tabMainHeight/2),"Money: "+s+"S "+c+"C "+m+"M" ,style2);	*/
	}
	
	string CreateCharacterStats ()
	{
		/*
		UNIT_FIELD_STAT0 + 
		STAT_STRENGTH                      = 0,
		STAT_AGILITY                       = 1,
		STAT_STAMINA                       = 2,
		STAT_INTELLECT                     = 3,
		STAT_SPIRIT                        = 4
	*/
		int posStat;
		int negStat;
		float minDamage;
		float maxDamage;
		float statFloat;
		UInt16 offset;
		string theStats = "";
		//theStats += "Armour: "+armour;
		theStats += "HP: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_HEALTH) + "/" + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXHEALTH);
		theStats += "\nMP: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POWER1) + "/"+GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXPOWER1);
		theStats += "\nExp: " + currentXp + "/" + maxXp;
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POSSTAT0);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_NEGSTAT0);
		theStats += "\nMight: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT0) + "(" + (posStat + negStat) + ")";
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POSSTAT1);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_NEGSTAT1);
		theStats += "\nDexterity: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT1) + "(" + (posStat + negStat) + ")";
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POSSTAT2);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_NEGSTAT2);
		theStats += "\nVitality: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT2) + "(" + (posStat + negStat) + ")";
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POSSTAT3);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_NEGSTAT3);
		theStats += "\nWisdom: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT3) + "(" + (posStat + negStat) + ")";
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POSSTAT4);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_NEGSTAT4);
		theStats += "\nWill: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT4) + "(" + (posStat + negStat) + ")";
		/*theStats += "\nStrength: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT0);
	theStats += "\nAgility: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT0 + 1);
	theStats += "\nStamina: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT0 + 2);
	theStats += "\nIntellect: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT0 + 3);
	theStats += "\nSpirit: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT0 + 4);*/
		//---------resistance
		offset = (ushort)SpellSchools.SPELL_SCHOOL_NORMAL;
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		theStats += "\nArmour: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset) + "(" + (posStat + negStat) + ")";
		offset = (ushort)SpellSchools.SPELL_SCHOOL_HOLY;
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		theStats += "\nWhite Magic Deterrance: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset) + "(" + (posStat + negStat) + ")";
		offset = (ushort)SpellSchools.SPELL_SCHOOL_SHADOW;
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		theStats += "\nBlack Magic Deterrance: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset) + "(" + (posStat + negStat) + ")";
		offset = (ushort)SpellSchools.SPELL_SCHOOL_ARCANE;
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		theStats += "\nSpirit Magic Deterrance: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset) + "(" +(posStat + negStat) + ")";
		offset = (ushort)SpellSchools.SPELL_SCHOOL_FIRE;
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		theStats += "\nInferno Magic Deterrance: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset) + "(" + (posStat + negStat) + ")";
		offset = (ushort)SpellSchools.SPELL_SCHOOL_FROST;
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		theStats += "\nIce Magic Deterrance: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset) + "(" + (posStat + negStat) + ")";
		offset = (ushort)SpellSchools.SPELL_SCHOOL_NATURE;
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		theStats += "\nEarth Magic Deterrance: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset) + "(" + (posStat + negStat) + ")";
		//---------meele
		minDamage = GetFloatValue((int)UpdateFields.EUnitFields.UNIT_FIELD_MINDAMAGE);
		maxDamage = GetFloatValue((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXDAMAGE);
		theStats += "\nDamage: " + Mathf.Floor(minDamage) + " - " + Mathf.Ceil(maxDamage);
		offset = (ushort)WeaponAttackType.BASE_ATTACK;
		statFloat = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_BASEATTACKTIME + offset) / 1000.0f;
		theStats += "\nHaste: " + statFloat.ToString("0.00f") + " attacks/sec";
		theStats += "\nMelee Strength: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_ATTACK_POWER);
		offset = (ushort)CombatRating.CR_HIT_MELEE;
		theStats += "\nMelee Strike Score: " + GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		statFloat = GetFloatValue((int)UpdateFields.EUnitFields.PLAYER_CRIT_PERCENTAGE);
		theStats += "\nMelee Chance to Crit: " + statFloat.ToString("0.00f") + "%";
		theStats += "\nMastery: " + GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_EXPERTISE);
		//---------range
		minDamage = GetFloatValue((int)UpdateFields.EUnitFields.UNIT_FIELD_MINRANGEDDAMAGE);
		maxDamage = GetFloatValue((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXRANGEDDAMAGE);
		theStats += "\nRanged Damage: " + Mathf.Floor(minDamage) + " - " + Mathf.Ceil(maxDamage);
		theStats += "\nRanged Strength: " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RANGED_ATTACK_POWER);
		offset = (ushort)CombatRating.CR_HIT_RANGED;
		theStats += "\nRanged Strike Score: " + GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		statFloat = GetFloatValue((int)UpdateFields.EUnitFields.PLAYER_RANGED_CRIT_PERCENTAGE);
		theStats += "\nRanged Chance to Crit: " + statFloat.ToString("0.00f") + "%";
		//---------spell
		offset = (ushort)SpellSchools.SPELL_SCHOOL_NORMAL;
		posStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_POS + offset);
		negStat = (int)GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + offset);
		theStats += "\nExtra Damage: " + (posStat + negStat);
		/*offset = SpellSchools.SPELL_SCHOOL_HOLY;
	posStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_POS + offset);
	negStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + offset);
	theStats += "\n  White Magic: " + (posStat + negStat);
	offset = SpellSchools.SPELL_SCHOOL_SHADOW;
	posStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_POS + offset);
	negStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + offset);
	theStats += "\n  Black Magic: " + (posStat + negStat);
	offset = SpellSchools.SPELL_SCHOOL_ARCANE;
	posStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_POS + offset);
	negStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + offset);
	theStats += "\n  Spirit Magic: " + (posStat + negStat);
	offset = SpellSchools.SPELL_SCHOOL_FIRE;
	posStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_POS + offset);
	negStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + offset);
	theStats += "\n  Inferno Magic: " + (posStat + negStat);
	offset = SpellSchools.SPELL_SCHOOL_FROST;
	posStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_POS + offset);
	negStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + offset);
	theStats += "\n  Ice Magic: " + (posStat + negStat);
	offset = SpellSchools.SPELL_SCHOOL_NATURE;
	posStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_POS + offset);
	negStat = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + offset);
	theStats += "\n  Earth Magic: " + (posStat + negStat);*/
		theStats += "\nExtra Alleviate: " + GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_HEALING_DONE_POS);
		offset = (ushort)CombatRating.CR_HIT_SPELL;
		theStats += "\nSpell Strike Score: " + GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		offset = (ushort)SpellSchools.SPELL_SCHOOL_NORMAL;
		statFloat = GetFloatValue((int)UpdateFields.EUnitFields.PLAYER_SPELL_CRIT_PERCENTAGE1 + offset);
		theStats += "\nSpell Chance to Crit: " + statFloat.ToString("0.00f") + "%";
		offset = (ushort)CombatRating.CR_HASTE_SPELL;
		theStats += "\nSpell Rush Score: " + GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		//---------defenses
		offset = (ushort)CombatRating.CR_DEFENSE_SKILL;
		theStats += "\nAegis: " + GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		float dodgePercent = GetFloatValue((int)UpdateFields.EUnitFields.PLAYER_DODGE_PERCENTAGE);
		theStats += "\nChance to Dodge: " + dodgePercent.ToString("0.00f") + "%";
		float parryPercent = GetFloatValue((int)UpdateFields.EUnitFields.PLAYER_PARRY_PERCENTAGE);
		theStats += "\nChance to Deflect: " + parryPercent.ToString("0.00f") + "%";
		float blockPercent = GetFloatValue((int)UpdateFields.EUnitFields.PLAYER_BLOCK_PERCENTAGE);
		theStats += "\nChance to Block: " + blockPercent.ToString("0.00f") + "%";
		uint flexibilitySumm = 0;
		offset = (ushort)CombatRating.CR_CRIT_TAKEN_MELEE;
		flexibilitySumm += GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		offset = (ushort)CombatRating.CR_CRIT_TAKEN_RANGED;
		flexibilitySumm += GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		offset = (ushort)CombatRating.CR_CRIT_TAKEN_SPELL;
		flexibilitySumm += GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		theStats += "\nFlexibility: " + Mathf.Ceil(flexibilitySumm / 3.0f);
		// dmg and shit needs to be read correctly and/or calculate it
		theStats += "\nGold: "+ GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COINAGE);
		theStats += "\nMithril Coins: " + mithrilCoins;
		int amber = 0;
		int zz = 0;
		ulong itemGuid;
		Item item;
		for(zz = 0; zz < 32; zz++)
		{
			itemGuid = GetUInt64Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_CURRENCYTOKEN_SLOT_1 + (zz * 2));
			if(itemGuid > 0)
			{
				item = itemManager.GetItemByGuid(itemGuid);
				if(item != null && item.entry == 43016)
				{
					amber = (int)item.GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT);
					break;
				}
			}
		}
		theStats += "\nAmber: "+ amber.ToString();
		amber = 0;
		for(zz = 0; zz < 32; zz++)
		{
			itemGuid = GetUInt64Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_CURRENCYTOKEN_SLOT_1 + (zz * 2));
			if(itemGuid > 0)
			{
				item = itemManager.GetItemByGuid(itemGuid);
				if(item != null && item.entry == 29434)
				{
					amber = (int)item.GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT);
					break;
				}
			}
		}
		theStats += "\nMark of Dokk: "+ amber.ToString();
		theStats += "\nPrestige: "+ GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_HONOR_CURRENCY);
		theStats += "\nPVP Kills: "+ GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_LIFETIME_HONORBALE_KILLS);
		theStats += "\nGear Rating " + gearRating;
		
		GetWeaponSkillStats();
		
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_DEFENSE);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_UNARMED);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_DAGGERS);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_STAVES);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_SWORDS);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_2H_SWORDS);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_AXES);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_2H_AXES);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_MACES);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_2H_MACES);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_POLEARMS);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_WANDS);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_THROWN);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_BOWS);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_CROSSBOWS);
		theStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_GUNS);
		
		theStats += "\n" + Fractions.GetReputationInfo();
		
		return theStats;
	}
	
	public void  GetWeaponSkillStats ()
	{
		PlayerSkills.ClearSkillValueDictionary();
		for(byte index = 0; index < 128; index++)
		{
			int offset = index * 3;
			uint skillID = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_SKILL_INFO_1_1 + offset);
			uint skillValue = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_SKILL_INFO_1_1 + offset + 1);
			uint skillBonus = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_SKILL_INFO_1_1 + offset + 2);
			
			uint newId = skillID & 0x0000ffff;
			uint startValue = skillValue & 0x0000ffff;
			uint maxValue = skillValue >> 16;
			
			startValue *= 2;
			maxValue *= 2;
			
			PlayerSkills.AddSkill(newId, startValue, maxValue);
		}
	}	
	
	/*	void  showQuests ()
	{
 try{

	int i;
	byte aux8 = QuestSlotOffsets.MAX_QUEST_OFFSET;
	byte aux8_1 = UpdateFields.EUnitFields.PLAYER_QUEST_LOG_1_1;
	uint id = 0;
	quest qst;
	
	//GUI.Box(questsLeftRect,"");
	//GUI.Box(questsRightRect,"");
	
	 GUILayout.BeginArea(questsRect);
	 scrollQViewVector = GUILayout.BeginScrollView(scrollQViewVector ,GUILayout.Width(questsLeftRect.width),GUILayout.Height(questsLeftRect.height));

		for(i = 0;i<25;i++)
		{
			id = GetUInt32Value((int)aux8_1 + i*aux8 + QuestSlotOffsets.QUEST_ID_OFFSET);	
			if(id)
			{
			 	qst = questManager.getQuest(id);
		 			if(!qst)
		 			{
		 				quest qst2 = new quest();
		 				qst2.questId = id;
		 				questManager.quests.Push(qst2);
		 				param[0] = id;
		 				session.handleOpcode(OpCodes.CMSG_QUEST_QUERY,param);
		 			}
		 			else
		 			{
		 				if(qst.title)
						if(GUILayout.Button(""+qst.title, newskin.customStyles[0],GUILayout.Width(tabMainWidth),GUILayout.Height(tabMainHeight)))
							selectedQuest = id;
		 			}	
			}
		 }	

	  GUILayout.EndScrollView();
	GUILayout.EndArea();
	
	
	if(selectedQuest)// print the description
	  {
	  	questManager.showPlayerQuestDetails(selectedQuest, questsRightRect);

	
		if(GUI.Button( new Rect(Screen.width*0.5f,Screen.height*0.8f,Screen.width*0.2f,Screen.height*0.1f),"Remove",newskin.customStyles[0]))
		{
			Debug.Log("World: Remove quest - QuestID:"+selectedQuest);
			questManager.removeQuest(selectedQuest);
			selectedQuest = 0;
		}
	  }
	
	}catch( Exception e ){MonoBehaviour.print(e);}
	
}
*/	
	
	
	IEnumerator showInventoryMessage ()
	{	
		GUI.Label(InventoryMessageRect, inventoryMessage, newskin.GetStyle("Element 31 (inventoryError)"));
		yield return new WaitForSeconds(3);
		inventoryMessage="";
	}
	
	public string getInventoryStatsInfo ()
	{
		string ret = string.Empty;
		if(showCharacterStats) 
		{
			ret = CreateCharacterStats();
		}
		else 
		{
			if(itemSelected != null && itemSelected.stats != null)
			{
				ret = itemSelected.stats;
			}
		}
		
		return ret;
	}
	
	// Draws the entire inventory
	void  showInventory ()
	{	//update for inventory
		currentXp = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_XP);
		maxXp = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_NEXT_LEVEL_XP);
		
		if(interf.inventoryManager.inventoryStatsMode != 2 && !interf.inventoryManager.inventoryContainer.hidden)
			showCaracterInInventory();
		
		showEquipSlots();
		showAllAboutBagsInventory();
		
		if(inventoryMessage!="")StartCoroutine(showInventoryMessage());	
		
		if(dragIsActive&&Input.GetMouseButton(0))
		{
			//GUI.Label(dragRect, dragItem.name, newskin.customStyles[0]);
		}
		
		//#if UNITY_ANDROID || UNITY_EDITOR	
		//		if(GUI.Button(new Rect(Screen.width*0.89f - UID.MITHRIL_WIDTH, Screen.height * 1.0f - UID.MITHRIL_HEIGHT, UID.MITHRIL_WIDTH, UID.MITHRIL_HEIGHT ), "", UID.skin.GetStyle("InventoryTapHereToEarnFreeMithril")))
		//		{
		//		#if UNITY_ANDROID
		//			CallJavaCode.ShowOffers();
		//		#endif
		//		}
		//#endif
	}
	
	void  showOptions ()
	{
		//if(options == false)
		//	return;
		GUI.Box(questsLeftRect,"");
		GUI.Box(questsRightRect,"");
		
		//GameObject obj = GameObject.FindWithTag("MainCamera");
		//	cam = obj.GetComponent<Camera>();
		
		Debug.Log("showing options");
		
		GUILayout.BeginArea(optionsRect); 
		GUILayout.BeginVertical();
		
		RenderSettings.fog = GUILayout.Toggle(RenderSettings.fog, "Enable/Disable Fog", GUILayout.Width(subTabWidth*2),GUILayout.Height(subTabHeight));
		
		GUILayout.Label("View Distance", GUILayout.Width(subTabWidth),GUILayout.Height(subTabHeight));
		cam.farClipPlane = GUILayout.HorizontalSlider(cam.farClipPlane, 300, 800, GUILayout.Width(subTabWidth),GUILayout.Height(subTabHeight));
		cam.layerCullDistances[12] = cam.farClipPlane * 0.2f;//medium objects
		cam.layerCullDistances[13] = cam.farClipPlane * 0.5f;//small objects
		RenderSettings.fogEndDistance = cam.farClipPlane;
		
		/*GUILayout.Label("LOD Distance", GUILayout.Width(subTabWidth),GUILayout.Height(subTabHeight));
	LODManager.lod_distance = GUILayout.HorizontalSlider(LODManager.lod_distance, 20, 100, GUILayout.Width(subTabWidth),GUILayout.Height(subTabHeight));*/
		
		GUILayout.EndVertical();
		GUILayout.EndArea();		
	}
	
	public void  _Attack ()
	{
		if(charMount)
		{
			return;
		}
		if(target == null || target.guid == guid || (target as Unit).m_deathState != DeathState.ALIVE)
		{
			targetTab.closestTarget();
		}
		focusOnTarget();
		if(target != null)
		{
			pkt = new WorldPacket(OpCodes.CMSG_ATTACKSWING,8);
			pkt.Append(ByteBuffer.Reverse(target.guid));
			RealmSocket.outQueue.Add(pkt);
		}
	}
	
	void  _Mount ( int val )
	{
		if(val != -1)
		{
			//Transform cam = GameObject.Find("inventoryCamera(Clone)").transform;
			float addY = playerColider.height*0.5f;	 	
			if(val == 3)
			{
				_Unmount();
				if (mountManager.IsMounted())
					inventoryCameraReff.localPosition /= 4;
			}
			else
			{
				if(mountManager.IsMounted())//if char is mounted send dismount package and it doesnt modify the toolbarSecondInt value, because we want to enter in this section again to cast mount spell
				{
					_Unmount();
					inventoryCameraReff.localPosition /= 4;
				}	 	 		
				Spell sp = dbs.sSpells.GetRecord(mountsSpell[val]) as Spell;
				//playerSpellManager.addSpell(sp);
				playerSpellManager.CastSpell(sp,0,Vector3.zero);	 			 
				inventoryCameraReff.localPosition *= 4;
			}
			inventoryCameraReff.LookAt(inventoryCameraReff.parent.position+new Vector3(0,addY,0)); 
		}
	}
	
	void  _Unmount ()
	{
		if(mountManager.IsMounted())//if char is mounted send dismount package and it doesnt modify the toolbarSecondInt value, because we want to enter in this section again to cast mount spell
		{
			pkt = new WorldPacket();
			pkt.SetOpcode(OpCodes.CMSG_CANCEL_MOUNT_AURA);
			RealmSocket.outQueue.Add(pkt);
		}	
	}
	
	void  showGuild ()
	{
		guildUI.showGuild( this );
	}
	
	void  showSpells ()
	{
		UIToolkit tempUIToolkit;
		// Debug.Log(dragIsActive);
		if(dragIsActive&&Input.GetMouseButton(0))
		{
			string spellimage = string.Empty;
			//decs - remove the old box and replace it with an icon
			if (classSpellDrag)
			{

				SpellEntry sp = dbs.sSpells.GetRecord((playerSpellManager.playerSpellBook[dragPos] as Spell).ID);
				SpellIconEntry icon  = dbs.sSpellIcons.GetRecord(sp.SpellIconID,false);
				spellimage  = icon.Icon;
				if (spellimage == null || spellimage=="")
				{
					spellimage = "noicon.png";
				}
				SinglePictureUtils.DestroySingleUISprite(spellIconDrag as UISprite);
				tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+spellimage);
				spellIconDrag = UIButton.create(tempUIToolkit, spellimage, spellimage, dragRect.x, dragRect.y, 0);
				spellIconDrag.setSize(dragRect.width * 1.2f,dragRect.height * 1.2f);
			}
			if(defaultSpellDrag)
			{
				switch(dragPos)
				{
				case 0:
					spellimage = interf.jumpImage;
					break;
				case 1:
					spellimage = interf.selectEnemyImage;
					break;
				case 2:
					spellimage = interf.objectTabImage;
					break;
				case 3:
					spellimage = interf.tradeImage;
					break;
				case 4:
					spellimage = interf.battleImage;
					break;
				case 5:
					spellimage = interf.inspectImage;
					break;
				}
				if (spellimage == null || spellimage == "")
				{
					spellimage = "noicon.png";
				}
				SinglePictureUtils.DestroySingleUISprite(spellIconDrag as UISprite);
				tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+spellimage);
				spellIconDrag = UIButton.create(tempUIToolkit,spellimage, spellimage, dragRect.x, dragRect.y,0);
				spellIconDrag.setSize(dragRect.width * 1.2f,dragRect.height * 1.2f);
			}
			if(usableItemDrag)
			{
				spellimage = DefaultIcons.GetItemIcon(entryDrag.entry);
				SinglePictureUtils.DestroySingleUISprite(spellIconDrag as UISprite);
				tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+spellimage);
				spellIconDrag = UIButton.create(tempUIToolkit, spellimage, spellimage, dragRect.x, dragRect.y, 0);
				spellIconDrag.setSize(dragRect.width * 1.2f,dragRect.height * 1.2f);
			}
		}
		else
		{
			//decs if we don't have the button clicked again delete the button
			SinglePictureUtils.DestroySingleUISprite(spellIconDrag as UISprite);
		}
	}
	
	public void  sendTarget ( BaseObject target2 )
	{	
		if(!target2 || blockTargetChange){ return; }
		isNPC = false;
		if( target2.IsCreature() 
		   && ((target2).GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) >= 1)
		   && (!(target2).HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_NOT_SELECTABLE)) ) //can we interact with this object 
		{
			isNPC = true;
		}
		
		if(isNPC)
		{
			float distance = Vector3.Distance(WorldSession.player.transform.position, target2.transform.position);
			if (distance < 5.0f)
			{
				(target2 as NPC).RotateToTarget(transform);
			}
			if(target && (target.guid == target2.guid))
			{
				int npcFaction = (int)(target as NPC).GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE);
				
				if (distance > 5.0f || !IsFriendly(npcFaction))
				{
					return;
				}
				
				blockTargetChange = true;
				WorldSession.interfaceManager.hideInterface = true;
				WorldSession.interfaceManager.SetWindowState(InterfaceWindowState.NPC_WINDOW_STATE);
				interf.stateManager();
				(target as NPC).BuildMenu();
				(target as NPC).menuState = 20;
				MoveStop();
			}
		}
		
		if(target2 as _GameObject)
		{
			if(target && (target.guid == target2.guid) && (target2 as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_MEETINGSTONE)
			{
				mMeetingStoneGuid = (target2).guid;
				mIsMeetingStoneMenu = true;
			}
		}
		
//		if(target && (target.guid == target2.guid) ){  return;	}
		
		uint spellIdForCast = 0;
		switch( (target2).GetTypeID() )
		{
		case TYPEID.TYPEID_GAMEOBJECT: //return 0x12;//GAMEOBJECT_END;
			if ( gVault != null && gVault.showVault )
			{
				gVault.ClearAll();
			}
			WorldPacket packet;
			if((target2 as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_CHEST)
			{
				/*WorldPacket pkt3 = new WorldPacket(OpCodes.CMSG_GAMEOBJ_USE ,8);
									pkt3.Append(ByteBuffer.Reverse(gameobjectTarget.guid));
									RealmSocket.outQueue.Add(pkt3);
									pkt3 = new WorldPacket(OpCodes.CMSG_GAMEOBJ_REPORT_USE ,8);
									pkt3.Append(ByteBuffer.Reverse(gameobjectTarget.guid));
									RealmSocket.outQueue.Add(pkt3);*/
				//playerSpellManager.castSpell(new Spell(6247),gameobjectTarget.guid,gameobjectTarget.transformParent.position);	
				//61437 or 68398  or 22810 or 3365  or 21651 or 6477 or 6478  or 6247 
				uint targetTemplateID = target2.GetEntry();
				float distanceToTarget = Vector3.Distance(TransformParent.position, target2.TransformParent.position);
				//session.SendLootRequest((target2 as BaseObject).guid);
				GameObjectTemplate chestTemplate = WorldSession.getGameObjectTemplate(targetTemplateID);
				if(chestTemplate != null)
				{
					if(distanceToTarget < 5)
					{
						int chestTemplateLockTypeID = chestTemplate.data[0];
						LockEntry lockEntry = dbs.sLock.GetRecord(chestTemplateLockTypeID);
						for(int inx = 0; inx < 8; inx++)
						{
							switch((LockKeyType)lockEntry.Type.array[inx])
							{
							case LockKeyType.LOCK_KEY_ITEM:
								//														packet = new WorldPacket();
								//														packet.SetOpcode(OpCodes.CMSG_GAMEOBJ_USE);
								//														packet.Append(ByteBuffer.Reverse(target2.guid));
								//														RealmSocket.outQueue.Add(packet);
								break;
							case LockKeyType.LOCK_KEY_SKILL:

								switch((LockType)lockEntry.Index.array[inx])
								{
								case LockType.LOCKTYPE_OPEN:				spellIdForCast = 3365;	break;
								case LockType.LOCKTYPE_CLOSE:				spellIdForCast = 6233;	break;
								case LockType.LOCKTYPE_QUICK_OPEN:			spellIdForCast = 6247;	break;
								case LockType.LOCKTYPE_QUICK_CLOSE:			spellIdForCast = 6247;	break;
								case LockType.LOCKTYPE_OPEN_TINKERING:		spellIdForCast = 6477;	break;
								case LockType.LOCKTYPE_OPEN_KNEELING:		spellIdForCast = 6478;	break;
								case LockType.LOCKTYPE_OPEN_ATTACKING:		spellIdForCast = 8386;	break;
								case LockType.LOCKTYPE_SLOW_OPEN:			spellIdForCast = 21651;	break;
								case LockType.LOCKTYPE_SLOW_CLOSE:			spellIdForCast = 21652;	break;
								case LockType.LOCKTYPE_OPEN_FROM_VEHICLE:	spellIdForCast = 61437;	break;
								case LockType.LOCKTYPE_MINING:    			spellIdForCast = GetMiningSpell();	break;
								case LockType.LOCKTYPE_HERBALISM:    		spellIdForCast = GetHerbalismSpell();	break;
								default:
									// TODO work with skill
									
									//																case LockType.LOCKTYPE_PICKLOCK:    	spellIdForCast =  SkillSpell.SKILL_LOCKPICKING;	break;
									//																case LockType.LOCKTYPE_HERBALISM:   	spellIdForCast =  SkillSpell.SKILL_HERBALISM;		break;
									//																case LockType.LOCKTYPE_MINING:      	spellIdForCast =  SkillSpell.SKILL_MINING;		break;
									//																case LockType.LOCKTYPE_FISHING:     	spellIdForCast =  SkillSpell.SKILL_FISHING;		break;
									//																case LockType.LOCKTYPE_INSCRIPTION: 	spellIdForCast =  SkillSpell.SKILL_INSCRIPTION;	break;
									break;
								}
								//SpellEntry spellForCast = dbs.sSpells.GetRecord(spellIdForCast);
								packet = new WorldPacket(OpCodes.CMSG_SET_SELECTION ,8);
								packet.Append(ByteBuffer.Reverse(target2.guid));
								RealmSocket.outQueue.Add(packet);
								playerSpellManager.CastSpell(spellIdForCast, target2.guid, target2.gameObject.transform.position);
								
								break;
							default:
								Debug.Log("TODO open locked chests by item or used unknown lock. LockType = " + lockEntry.Type.array[inx]);
								break;
							}
						}
						Debug.Log("Open Chest...");
					}
					else
					{
						ShowErrorMessage("You are too far to pick this up.");
						Debug.Log("You are too far to interact with that object...");
//						target2=null;
//						return;
					}
				}
				else
				{
					return;
				}
			}
			else if((target2 as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_MAILBOX)
			{
				mailHandler.SetMBoxGuid(target2.guid);
				mailHandler.SendGetMailList();
				break;
			}
			else if((target2 as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_GUILD_BANK)
			{
				//(target2 as _GameObject).initVault(this);
				gVault = new GuildVault(this, target2);
			}
			else if((target2 as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_BUTTON)
			{
				Debug.Log("Interact with"+(target2 as _GameObject).gameobjectType);
			}
			else if((target2 as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_FLAGDROP)
			{
				packet = new WorldPacket();
				packet.SetOpcode(OpCodes.CMSG_GAMEOBJ_USE);
				packet.Add(target2.guid);
				RealmSocket.outQueue.Add(packet);
			}
			else if((target2 as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_FLAGSTAND)
			{
				packet = new WorldPacket();
				packet.SetOpcode(OpCodes.CMSG_GAMEOBJ_USE);
				packet.Add(target2.guid);
				RealmSocket.outQueue.Add(packet);
			}
			else if((target2 as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_SPELLCASTER)
			{
				packet = new WorldPacket();
				packet.SetOpcode(OpCodes.CMSG_GAMEOBJ_USE);
				packet.Add(target2.guid);
				RealmSocket.outQueue.Add(packet);
			}
			else if((target2 as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_FISHINGNODE)
			{
				packet = new WorldPacket();
				packet.SetOpcode(OpCodes.CMSG_GAMEOBJ_USE);
				packet.Add(target2.guid);
				RealmSocket.outQueue.Add(packet);
			}
			else if((target2 as _GameObject).gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_SUMMONING_RITUAL)
			{
				packet = new WorldPacket();
				packet.SetOpcode(OpCodes.CMSG_GAMEOBJ_USE);
				packet.Add(target2.guid);
				RealmSocket.outQueue.Add(packet);
			}
			
			else
			{
				Debug.Log("GameObject type unhandled "+(target2 as _GameObject).gameobjectType);
			}
			//portrait reff update
			PortraitReff.UpdateTargetSnapShot((target2 as BaseObject).transform);
			break;
		case TYPEID.TYPEID_UNIT:
			if ((target2 as Unit).Name == "Battle Dummy")
			{			
				if (WorldSession.player.tutorialInstance && WorldSession.player.tutorialInstance.tutorialStep == TutorialStages.TUTORIAL_SELECT_DUMMY)
				{
					WorldSession.player.tutorialInstance.CompleteTutorialStage();						
				}
			}
			PortraitReff.UpdateTargetSnapShot((target2 as Unit).transform);
			
			bool  isLootable = (target2 as Unit).HasFlag(UpdateFields.EUnitFields.UNIT_DYNAMIC_FLAGS, (uint)UnitDynFlags.UNIT_DYNFLAG_LOOTABLE);
			bool  isSkinnable = (target2 as Unit).HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_SKINNABLE);
			Debug.Log("Unit "+(target2 as Unit).guid+"isLootable = "+isLootable+"  isSkinnable = "+isSkinnable);
			if(!isLootable && isSkinnable)
			{
				spellIdForCast = GetScalpingSpell();
				packet = new WorldPacket(OpCodes.CMSG_SET_SELECTION ,8);
				packet.Append(ByteBuffer.Reverse(target2.guid));
				RealmSocket.outQueue.Add(packet);
				playerSpellManager.CastSpell(spellIdForCast, target2.guid, target2.gameObject.transform.position);
			}
			break;
		case TYPEID.TYPEID_PLAYER:
			PortraitReff.UpdateTargetSnapShot((target2 as Player).transform);
			
			break;
		case TYPEID.TYPEID_CONTAINER:
			//PortraitReff.UpdateTargetSnapShot((target2 as Unit).transform);
			break;
			/*case TYPEID.TYPEID_OBJECT:
		case TYPEID.TYPEID_ITEM: 
	case TYPEID.TYPEID_DYNAMICOBJECT:*/
		case TYPEID.TYPEID_CORPSE:
			Debug.Log("Interact with corpse "+target2.guid);
			/*if((target2 as Corpse).BelongTo(guid))
						{
							if(m_deathState == DeathState.DEAD)
							{
								interf.reviveWindow.hideRevive = false;
							}		
						} 	*/						
			PortraitReff.UpdateTargetSnapShot((target2 as BaseObject).transform);
			break;
		default :
			if((target2).guid == this.guid)
			{
				PortraitReff.UpdateTargetSnapShot(this.transform);
			}
			break;
		}
		
		target = (target2);
		WorldPacket paket = new WorldPacket(OpCodes.CMSG_SET_SELECTION ,8);
		paket.Append(ByteBuffer.Reverse((target2).guid));
		RealmSocket.outQueue.Add(paket);
	}
	
	uint GetMiningSpell ()
	{
		uint ret = 0;
		SkillInfo skillInfoValue = PlayerSkills.GetSkillInfoRecord(SkillType.SKILL_MINING);
		
		if(skillInfoValue != null)
		{
			switch(skillInfoValue.MaxValue)
			{
			case 150:
				ret = 2575;
				break;
			case 300:
				ret = 2576;
				break;
			case 450:
				ret = 3564;
				break;
			case 600:
				ret = 10248;
				break;
			case 750:
				ret = 29354;
				break;
			case 900:
				ret = 50310;
				break;
			}
		}
		return ret;
	}
	
	uint GetHerbalismSpell ()
	{
		uint ret = 0;
		SkillInfo skillInfoValue = PlayerSkills.GetSkillInfoRecord(SkillType.SKILL_HERBALISM);
		if(skillInfoValue != null)
		{
			switch(skillInfoValue.MaxValue)
			{
			case 150:
				ret = 2366;
				break;
			case 300:
				ret = 2368;
				break;
			case 450:
				ret = 3570;
				break;
			case 600:
				ret = 11993;
				break;
			case 750:
				ret = 28695;
				break;
			case 900:
				ret = 50300;
				break;
			}
		}
		return ret;
	}
	
	uint GetScalpingSpell ()
	{
		uint ret = 0;
		SkillInfo skillInfoValue = PlayerSkills.GetSkillInfoRecord(SkillType.SKILL_SKINNING);
		if(skillInfoValue != null)
		{
			switch(skillInfoValue.MaxValue)
			{
			case 150:
				ret = 8613;
				break;
			case 300:
				ret = 8617;
				break;
			case 450:
				ret = 8618;
				break;
			case 600:
				ret = 10768;
				break;
			case 750:
				ret = 32678;
				break;
			case 900:
				ret = 50305;
				break;
			}
		}
		return ret;
	}
	
	//dmgType:
	//0 - melee (white)
	//1 - range/spell/aura (yellow)
	//2 - heal (green)
	//crit true for melee/range/spell/aura (red + big)
	//crit true for heal (green + big)
	public void  showDMG ( Unit obj, string dmg, uint schoolMask, byte dmgType, bool crit, ulong attackerGUID  /* DMG dmgList  */)
	{
		GameObject dmgMesh = null;
//		Unit attackerUnit;
//		if(attackerGUID == guid)
//		{
//			attackerUnit = WorldSession.player;
//		}
//		else
//		{
//			attackerUnit = session.getObj(attackerGUID) as Unit;
//		}
		if(obj.guid != guid && attackerGUID != guid && PlayerPrefs.GetInt("ShowDMGonOtherPlayers") == 0)
		{
			return;
		}
		
		if(PlayerPrefs.GetInt("allDmgButtonOption") == 0)
		{
			Debug.Log("---------" + guid + " --- " + GUID + " ---- " + obj.name + " --- " + obj.guid);
			//if(obj.guid == guid)
			if(attackerGUID == guid)
			{
				dmgMesh = GameObject.Instantiate(Resources.Load<GameObject>("prefabs/GameObjects/flytext"));
				dmgMesh.transform.position = obj.TransformParent.position+new Vector3(UnityEngine.Random.Range(-1.0f,1.0f),
				                                                                      UnityEngine.Random.Range(1,3),
				                                                                      0);			
				dmgMesh.GetComponent<TextMesh>().text = ""+dmg;			
				dmgMesh.transform.LookAt((mainCamTransform.position-dmgMesh.transform.position)*(-1) + dmgMesh.transform.position);// billboard
			}
		}
		else
		{
			dmgMesh = GameObject.Instantiate(Resources.Load<GameObject>("prefabs/GameObjects/flytext"));
			dmgMesh.transform.position = obj.TransformParent.position+new Vector3(UnityEngine.Random.Range(-1.0f,1.0f),
			                                                                      UnityEngine.Random.Range(1,3),
			                                                                      0);			
			dmgMesh.GetComponent<TextMesh>().text = ""+dmg;			
			dmgMesh.transform.LookAt((mainCamTransform.position-dmgMesh.transform.position)*(-1) + dmgMesh.transform.position);// billboard
		}
		if(dmgMesh != null)
		{
			Renderer renderer = dmgMesh.GetComponent<Renderer>();
			if(crit)
			{
				dmgMesh.GetComponent<TextMesh>().fontSize = 30;
				dmgMesh.GetComponent<TextMesh>().fontStyle = FontStyle.Bold;
				switch(dmgType)
				{
				case (2):
					renderer.material.color = Color.green;
					break;
				case (1):
				case (0):
					renderer.material.color = Color.red;
					break;
				default:
					renderer.material.color = Color.red;
					break;
				}
			}
			else
			{
				switch(dmgType)
				{
				case 2:
					renderer.material.color = Color.green;
					break;
				case 1:
					renderer.material.color = Color.yellow;
					break;
				case 0:
					renderer.material.color = Color.white;
					break;
				default:
					renderer.material.color = Color.white;
					break;
				}
			}
		}
	}
	
	//	bool TargetInMyGroup( MainPlayer player ,   OtherPlayer target ):bool
	//	{
	//		bool  ret = false;
	//		for(var i= 0; i < player.group.membersCount; i++)
	//		{
	//			if((player.group.partyMembers[i] as PartyPlayer).guid == target.guid)
	//			{
	//				ret = true;
	//				break;
	//			}
	//		}
	//		return ret;
	//	}
	
	void  IsPlayerFromEnemyDuelTeam ( Player target )
	{	
		uint playerTeam = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_DUEL_TEAM);
		uint targetTeam = target.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_DUEL_TEAM);
		
		if (playerTeam != targetTeam && targetTeam != 0 && playerTeam != 0)
			isFromEnemyDuelTeam = true;
		else
			isFromEnemyDuelTeam = false;
	}

	void  UpdateTime ()
	{
		var today= System.DateTime.Now;
		localTime = today.ToString("HH:mm");
		string currentMinute;
		if(today.Minute<10)
			currentMinute = "0"+today.Minute;
		else
			currentMinute = today.Minute.ToString();
		
		int currentHour =today.Hour;
		if(currentHour-PlayerPrefs.GetInt("GMTdifference") <0)
		{
			int randomTempInt =currentHour+24-PlayerPrefs.GetInt("GMTdifference");
			serverTime= randomTempInt.ToString() +":"+ currentMinute;
		}
		else
		{
			serverTime= (currentHour-PlayerPrefs.GetInt("GMTdifference")).ToString() + ":"+ currentMinute;
		}
	}

	void  OnGUI ()
	{	
		if(mIsMeetingStoneMenu)
		{
			ButtonOLD.summonState = true;
			if(GUI.Button( new Rect(Screen.width * 0.425f, Screen.height * 0.23f, Screen.width * 0.15f, Screen.height * 0.06f), "Exit", WorldSession.player.guildskin.button))
			{
				mIsMeetingStoneMenu = false;
				ButtonOLD.summonState = false;
			}
			if(GUI.Button( new Rect(Screen.width * 0.425f, Screen.height * 0.33f, Screen.width * 0.15f, Screen.height * 0.06f), "Summon", WorldSession.player.guildskin.button))
			{
				UIButton tmpBtn = null;
				interf.setSummonStoneGuid(mMeetingStoneGuid);
				interf.PartyMemberWnd(tmpBtn);
				mIsMeetingStoneMenu = false;
			}
		}
		
		actionBarPanel.DrawSpellHUD();
		lootManager.Show();
		
		if(battlegroundManager.isActive)
		{
			battlegroundManager.DrawBattlegroundWindow();
		}
		
		if(interf != null
		   && !(WorldSession.interfaceManager.hideInterface || ButtonOLD.menuState)
		   && interf.PortraitsFrame != null)
		{
			interf.PortraitsFrame.DrawPlayerAuras();
			if(target)
			{
				interf.PortraitsFrame.DrawTargetAuras();
			}
		}
		
		if(!WorldSession.interfaceManager.hideInterface && ((target != null) && (target.castTime >= 0) && (target.castTime < target.castTimeTotal)))
		{
			DrawEnemyCastBar();
		}
		//this needs to be changed sometime...
		if(ShowReviveDialogue && !(ButtonOLD.menuState || ButtonOLD.chatState))
		{
			//show window box + text
			Player _player = WorldSession.GetRealChar(CasterGUID) as Player;
			GUI.Box( new Rect(Screen.width * 0.3f, Screen.height * 0.4f, Screen.width * 0.4f, Screen.height * 0.35f), _player.name + " wants to resurrect you.");
			//GUI.Label( new Rect(Screen.width * 0.3f, Screen.height * 0.4f, Screen.width * 0.4f, Screen.height * 0.75f), "Someone wants to resurrect you.");
			if(GUI.Button( new Rect(Screen.width * 0.35f, Screen.height * 0.6f, Screen.width * 0.1f, Screen.height * 0.1f), "ACCEPT"))
			{
				ShowReviveDialogue = false;
				session.AcceptRevive();
			}
			if(GUI.Button( new Rect(Screen.width * 0.55f, Screen.height * 0.6f, Screen.width * 0.1f, Screen.height * 0.1f), "DECLINE"))
			{
				ShowReviveDialogue = false;
				session.DeclineRevive();
			}
		}	
		if(LoadSceneManager.LoadingStatus == LoadSceneManager.LOADING.LOADING_INPROGRESS)
		{
			return;
		}
		
		//		if(LoadSceneManager.LoadingStatus == LoadSceneManager.LOADING.LOADING_INPROGRESS)
		//		{
		//			GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height),LoadScreenTexture,ScaleMode.StretchToFill,true);	
		//
		//#if UNITY_ANDROID || UNITY_EDITOR	
		//			if(GUI.Button( new Rect(Screen.width * 0.25f, Screen.height * 0.99f - UID.MITHRIL_HEIGHT, UID.MITHRIL_WIDTH*2, UID.MITHRIL_HEIGHT ), ", UID.skin.GetStyle("TapHereToEarnFreeMithril")"))
		//			{
		//			#if UNITY_ANDROID && !UNITY_EDITOR
		//				CallJavaCode.ShowOffers();
		//			#endif
		//			}
		//#endif
		
		//#if UNITY_ANDROID || UNITY_EDITOR	
		//			if(GUI.Button( new Rect(Screen.width * 0.25f, Screen.height * 0.99f - UID.MITHRIL_HEIGHT, UID.MITHRIL_WIDTH*2, UID.MITHRIL_HEIGHT ), "", UID.skin.GetStyle("TapHereToEarnFreeMithril")))
		//			{
		//			#if UNITY_ANDROID
		//				CallJavaCode.ShowOffers();
		//			#endif
		//			}
		//#endif
		//
		//			return;
		//		}
		
		
		MessageWnd mw;
		for(i = 0; i < windows.Count; i++)
		{
			mw = windows[i];
			if(mw.Value() == 0)
			{
				mw.rect = GUI.Window(i, mw.rect, messageWnd, "", newskin.window);
			}
			else
			{
				windows.RemoveAt(i);
			}
		}
		
		if(_showMenuButton && !Portrait.AreWeInMenu)
			ShowMenuButton();
		
		xC = TransformParent.position.x;
		yC = TransformParent.position.z;
		zC = TransformParent.position.y;
		
		// GUI.Label( new Rect(Screen.width-150,20,150,20),"Version Date: 10/04/12");
		// GUI.Label( new Rect(Screen.width-200,0,200,20),"X:"+xC.ToString("F2") + " Y:"+yC.ToString("F2") +" Z:"+zC.ToString("F2"));
		
		
		if(!ShowMinimap)
		{
			GUI.Label( new Rect(Screen.width*0.76f , 0 , Screen.width*0.2f , Screen.height*/*0.032f*/ 0.1f),"X:"+xC.ToString("F2") + " Y:"+yC.ToString("F2") +" Z:"+zC.ToString("F2"),
			          (Screen.width>2000)?newskin.label : GUI.skin.label);
			#if UNITY_EDITOR
			GUI.Label( new Rect(Screen.width*0.76f , Screen.height*0.025f , Screen.width*0.2f , Screen.height*0.1f),"Map: "+mapId+" Zone: "+zoneId+" Area: "+areaId);
			GUI.Label( new Rect(Screen.width*0.76f , Screen.height*0.05f , Screen.width*0.2f , Screen.height*0.1f),"Version Date: I AM ALIVE!!!!");
			#endif
		}
		
		if(ButtonOLD.menuState)
		{	
			//MoveStop();
			//MoveStopStrafe();
			//MoveStopTurn();
			
			//if(inventoryTexture)
			//GUI.DrawTexture( new Rect(0,0, Screen.width, Screen.height),inventoryTexture,ScaleMode.StretchToFill,true);
			showMainMeniu();
			//			if(menuSelected == 3)
			//			{ 
			//				interf.spellState = true;
			//			}
			//			else
			//			{
			//				interf.SpellState(false);
			//			}
			//showequipSlotsWithSpellsAndStuff();
			
			if(questRewardItemInfoWindow.active)
			{
				questRewardItemInfoWindow.DrawIntemInfoWindow();
			}
			
			if (ButtonOLD.editMacrosState)
			{
				GUI.Label( new Rect(Screen.width * 0.15f, Screen.height * 0.1f, Screen.width * 0.81f, Screen.height * 0.1f), ButtonOLD.descText, newskin.label);
				Rect macrosRect = new Rect(Screen.width * 0.07f, Screen.height * 0.23f, Screen.width * 0.86f, Screen.height * 0.55f); 
				macrosText = GUI.TextArea(macrosRect, macrosText, newskin.textField); 
			}
			
			if(interf.inventoryManager.itemInfoWindow != null && interf.inventoryManager.itemInfoWindow.isEnable)
			{
				interf.inventoryManager.itemInfoWindow.DrawItemInfo();
			}
			else if(TextZone.showZone)
			{	 			
				TextZone.DrawTraitsInfo();
			}
			
			if(raidFlag)
			{
				RaidWindowManager.showGuiRaidWindow();
			}
			
			group.HideSpellIcons(true);
			
			return;
		}
		//=====
		else
		{
			//if(interf.hideInterface)
			//	return;
			
			if(inventoryCameraReff)
			{
				inventoryCameraReff.gameObject.SetActive(false);
			}
		}
		
		if(WorldSession.interfaceManager.hideInterface)
			group.HideSpellIcons(true);
		
		TextAnchor backUpText = newskin.label.alignment;
		if(!WorldSession.interfaceManager.hideInterface)
		{
			newskin.label.alignment = TextAnchor.UpperLeft;
			GUI.Label( new Rect(Screen.width * 0.09f, 0, Screen.width * 0.1f, Screen.height * 0.1f), "" + level, newskin.label);
			
			if(target)// && target.target_tex)
			{
				GUI.Label( new Rect(Screen.width * 0.54f, 0, Screen.width * 0.1f, Screen.height * 0.1f), "" + target.GetLevel(), newskin.label);
				
				if(target.guid!=OldLootGuid)
				{
					OldLootGuid=target.guid;
					lootOnce=true;
					ButtonOLD.lootState=false;
					itemLoot = null;
				}
				if(target.GetValuesCount()==0x94 && (target as Unit).HP==0 && lootOnce )
				{
					lootOnce=false;
					session.SendLootRequest(target.guid);
				}
			}
		}
		
		newskin.label.alignment = backUpText;
		
		if(!chatOpend && allText.Count > 2 )   
		{
			Message m;
			m = allText[allText.Count-1];
			textToSend = m.getAllText();		// Minimized Chat box lingering text -Sebek
			m = allText[allText.Count-2];
			textToSend += m.getAllText();
			m = allText[allText.Count-3];
			textToSend += m.getAllText();
			
		}
		GUI.SetNextControlName("Text1");
		if(!meniuOpend && !ButtonOLD.chatState && !WorldSession.interfaceManager.hideInterface)
		{
			rowSize = newskin.GetStyle("newChat (34)").lineHeight*1.14f;
			
			#if UNITY_EDITOR
			if(actionBarPanel.bottomPanelFlag)
				finalTextRect = new Rect(Screen.width/4, (Screen.height*77.8f/100)-(rowSize*(MinChatBoxSize-1)) , _equipSize*7, rowSize*MinChatBoxSize );	// text field is perfectly aligned in unity but not on ios devices ergo the #IF -Sebek
			else
				finalTextRect = new Rect(Screen.width/4, (Screen.height*93.8f/100)-(rowSize*(MinChatBoxSize-1)) , _equipSize*7, rowSize*MinChatBoxSize );
			#endif
			#if !UNITY_EDITOR
			if(actionBarPanel.bottomPanelFlag)
				finalTextRect = new Rect(Screen.width/4, (Screen.height*83/100)-(rowSize*(MinChatBoxSize-1)) , _equipSize*7, rowSize*MinChatBoxSize );
			else
				finalTextRect = new Rect(Screen.width/4, (Screen.height*93.8f/100)-(rowSize*(MinChatBoxSize-1)) , _equipSize*7, rowSize*MinChatBoxSize );
			#endif
			GUI.Window(-1, finalTextRect, ChatWindow, "", newskin.GetStyle("newChat (34)")); 
			_chatScrollVector2.y+=2000;
		}		
		
		if (ButtonOLD.chatState)
		{
			if(!chatOpend)
			{
				textToSend = "TOUCH HERE TO CHAT";
			}
			
			textToSend = GUI.TextField(textRect, textToSend, newskin.GetStyle("DefaultChat")); 
			
			if (GUI.GetNameOfFocusedControl() == "Text1" ) 
			{
				textRect.x=0;
				textRect.y = Screen.height*0.5f-textRect.height;//keyboard.area.y-textRect.height;
				textRect.width = Screen.width - 2.5f*auraWidth;
				//textRect.height = Scree
				
				GUI.DrawTexture( new Rect(textRect.width,Screen.height*0.5f - textRect.height ,auraWidth*2,textRect.height*2),_sendTexture);
				if(GUI.Button( new Rect(textRect.width,Screen.height*0.5f - textRect.height ,auraWidth*2,textRect.height*2),"", new GUIStyle()))  // Send Button , DOH!
				{
					if(Time.time - chatspam >2) 				 // Anti-spam for chat   - Sebek
					{
						SendMessage2(textToSend);
						textToSend="";
						chatspam=Time.time;
					}
				}
				
				if(chatOpend == false)
				{
					textToSend = ""; // Second chat window textfield
				}
				chatOpend = true;
				
				#if UNITY_STANDALONE || UNITY_EDITOR
				if(Event.current.keyCode == KeyCode.Return)
				{
					Event e = Event.current;
					if(e.isKey)
					{
						SendMessage2(textToSend);
						textToSend = "";
					}
				}
				#endif
			}
			else
			{
				#if UNITY_EDITOR
				textRect = new Rect(Screen.width/4, Screen.height*77.8f/100 , _equipSize*7, Screen.height/15 ); //2nd textfield, for the 1st chat window
				#endif
				#if !UNITY_EDITOR
				textRect = new Rect(Screen.width/4, Screen.height*82.5f/100 , _equipSize*7, Screen.height/15 );
				#endif
				chatOpend = false;
			}
		}
		
		if(chatOpend && !meniuOpend)
		{
			chatTextWindowRect = new Rect(0, 0, Screen.width - 1.5f*auraWidth, Screen.height/2-Screen.height/15);
			GUI.Window(-1, chatTextWindowRect, ChatWindow, " ", newskin.window); // Sebek
			_chatScrollVector2.y+=2000;		//Scrolls the 2nd chatbox down when it is selected for typing :D -Sebek
		}
		else if(!chatOpend && ButtonOLD.chatState && !meniuOpend)
		{
			chatTextWindowRect = new Rect(1.5f*auraWidth, Screen.width * 0.1095f/*1.05f*/, Screen.width - 3*auraWidth, Screen.height*3/4-Screen.width*0.0935f);
			GUI.Window(-1, chatTextWindowRect, ChatWindow, " ", newskin.window);
			
			/////// NEW INTERFACE CHAT WINDOW 1				-Sebek
			//			GUI.Window(-1, new Rect(Screen.width*35/100, Screen.height*57/100, Screen.width*54/100, Screen.height*30/100), ChatWindow, " ", newskin.window);
			/////// NEW INTERFACE CHAT WINDOW 2
			//			GUI.Window(-1, new Rect(Screen.width*35/100, Screen.height*57/100, Screen.width*48/100, Screen.height*30/100), ChatWindow, " ", newskin.window);
		}
		if(ButtonOLD.chatState)
		{
			float baseSize = 220 * Screen.height / 600; //600 was the height i made the minimap, so it's relative to that screen size i guess
			float _size = 17 * baseSize / 100;
			float buttonXSize = _size*1.4f;
			Rect closeRect = new Rect(chatTextWindowRect.x + chatTextWindowRect.width - buttonXSize*0.6f, chatTextWindowRect.y - buttonXSize*0.4f, buttonXSize, buttonXSize);
			GUI.Window(0, closeRect, DrawCloseXChatButton, "", new GUIStyle());
		}
		if(!meniuOpend && !WorldSession.interfaceManager.hideInterface)
		{
			actionBarPanel.DrawChatQuestArrow();
		}
		
		if(mapId == 489)
		{
			if(WorldSession.interfaceManager.playerMenuMng.extraMenuWindow.battleZoneWindow.battleZoneManager.timeLeft > 0)
			{
				string bzLog = "Time Left - " + WorldSession.interfaceManager.playerMenuMng.extraMenuWindow.battleZoneWindow.battleZoneManager.timeLeft;
				bzLog += "\n 0 : 0";
				GUI.Label( new Rect(Screen.width * 0.375f, Screen.height * 0.14f, Screen.width * 0.25f, Screen.height * 0.15f), 
				          bzLog, newskin.window);
			}
		}
		
		menuSelected = -1;
		
		if(goCast)
		{
			//if(!WorldSession.interfaceManager.castBarMng.gameObject.activeInHierarchy)
			//{
			//	float castDeltaTime = castTimeTotal - castTime;
			//}
			//			if(castDeltaTime > 0.0f)
			//			{
			//				var castTimeResult= castDeltaTime/castTimeTotal;
			//				interf.progressBar.value = isConductedSpell ? (1 - castTimeResult) : castTimeResult;
			//			}
			//			else
			{
				goCast = false;
				
				if(castTimeTotal > 0.0f)
				{
					SpellVisual scriptSpellVisual = FxEffectManager.GetSpellVisual(castSpellID);
					if(scriptSpellVisual)
					{
						int effectId = scriptSpellVisual.effect[0];
						FxEffectManager.DestroyAndRemoveEffect(effectId, guid);
					}
				}
				
			}
		}	
		//		interf.progressBar.hidden = !goCast;
		
		
//		if (firstTimeBar) // TODO: change position of init bar
//		{
//			////?
//			// Get the color of the bar by class type
//			switch(GetPowerTypeNumber())
//			{
//			case Powers.POWER_MANA:
//				barCurrent = barMana;
//				break;
//			case Powers.POWER_RAGE:
//				barCurrent = barWrath;
//				break;
//			case Powers.POWER_ENERGY:
//				barCurrent = barPower;
//				break;
//			}	
//			firstTimeBar = false;	
//			////?
//		}
		
		//npc interaction
		if(target)
		{
			try{
				if((target.GetTypeID() != TYPEID.TYPEID_GAMEOBJECT) && gVault != null && gVault.showVault)//||
					//((target as _GameObject).gameobjectType != (int)GameobjectTypes.GAMEOBJECT_TYPE_GUILD_BANK && gVault.showVault==true))
					//if ( gVault.showVault )
				{
					Debug.Log("Intraaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
					gVault.ClearAll();
				}
			}catch( Exception exception ){Debug.Log(exception);}
			
			/*
		else if(target._type != TYPEID.TYPEID_GAMEOBJECT ||
			(target as _GameObject).gameobjectType != (int)GameobjectTypes.GAMEOBJECT_TYPE_GUILD_BANK && gVault.showVault)
		{
			gVault.ClearAll();
		/*
			if((target as _GameObject).gVault.showVault)
			{
				(target as _GameObject).gVault.DisplayVault();
			}*
		}*/
		}
		
		trade.tradeUI.Show();
		
		if(InspectMgr.active)
		{
			InspectMgr.Show();
		}
		
		if(duel.duelRequested)
		{
			duel.DisplayDialog();
		}
		if(duel.displayTimer)
		{
			GUI.Label( new Rect(Screen.width/2, Screen.height * 0.15f, Screen.width * 0.1f, Screen.height * 0.1f), ""+duel.secondsToDuel);
		}
		if(gVault != null && ( gVault.showVault || gVault.WindowActive ))
		{
			gVault.DisplayVault();
		}
		//Display banker item stats 
		if(banker != null && isBankerItemInfo)
		{
			if(ButtonOLD.chatState || ButtonOLD.menuState)
			{
				isBankerItemInfo = false;
			}
			else
			{
				banker.DrawItemInfo();
			}
		}
		//Display vendor item stats 
		if(vendor != null && isVendorItemInfo)
		{
			if(ButtonOLD.chatState || ButtonOLD.menuState)
			{
				isVendorItemInfo = false;
			}
			else
			{
				vendor.DrawItemInfo();
			}
		}
		//		//Display Loot item stats
		//		if((Button.lootState || Button.needState) && isLootItemInfo && !Button.UIalert)
		//		{
		//			interf.DrawItemInfo();
		//		}
		
		if(mailHandler.mailUI.show)
		{
			mailHandler.mailUI.ShowUI();
		}
		
		if(mailHandler.mailUI.isMailItemInfo)
		{
			mailHandler.mailUI.DrawItemInfo();
		}
		
		
		//display party members
		
		if(!WorldSession.interfaceManager.hideInterface && !ButtonOLD.chatState && !trade.trading && !inv.isActive && !ButtonOLD.menuState)
		{
			group.HideSpellIcons(false);
			group.show();
		}
		
		if ( guildUI.showInvitationGuid )
		{
			guildUI.ShowInviteWindow( this );
			guildUI.showInvitationGuid = false;
		}
		if(LoggingOut)
			value = JoystickReturn.UNKNOWN;
		/*if(usedDildau == 0)
		moveStick.FakeDraw();*/
		if ((gVault == null || (!gVault.active && !gVault.WindowActive)) && !WorldSession.interfaceManager.hideInterface && !tutorialInstance.isNeedBlockMovement())
		{
			value = (meniuOpend)? JoystickReturn.UNKNOWN : moveStick.Draw();
		}
		else
		{
			value = JoystickReturn.UNKNOWN;
		}
		
		moveDir = value;
		switch ( value )
		{
		case JoystickReturn.BACKWARD: // 4:
			//Debug.Log("SendMovementFlags B: " + sendMovementFlags);
			moveStick.angle = moveStick.angle > 0 ? moveStick.angle - 3 : moveStick.angle + 3;
			//MoveStop();
			MoveStartBackward();
			isMoving = true;
			OnMovePlayer();
			if(usedDildau == 0)
			{					
				usedDildau = 8;
				PlayerPrefs.SetInt("usedDildau", 8);
			}
			
			break;
		case JoystickReturn.BACKWARD_LEFT: // 9:
			if ( !moveStick.HasChangedMovement )
				break;
			//MoveStop();
			MoveStartBackwardLeft();
			OnMovePlayer();
			moveStick.HasChangedMovement = false;
			break;
		case JoystickReturn.BACKWARD_RIGHT: // 10:
			if ( !moveStick.HasChangedMovement )
				break;
			//MoveStop();
			MoveStartBackwardRight();
			OnMovePlayer();
			moveStick.HasChangedMovement = false;
			break;
		case JoystickReturn.SIDE_RIGHT: // 6:
			if ( !moveStick.HasChangedMovement )
				break;
			//MoveStop();
			MoveStartStrafeRight();
			OnMovePlayer();
			moveStick.HasChangedMovement = false;
			if(usedDildau == 0)
			{					
				usedDildau = 8;
				PlayerPrefs.SetInt("usedDildau", 8);
			}
			break;
		case JoystickReturn.SIDE_LEFT: // 5:
			if ( !moveStick.HasChangedMovement )
				break;
			//MoveStop();
			MoveStartStrafeLeft();
			OnMovePlayer();
			moveStick.HasChangedMovement = false;
			break;
		case JoystickReturn.FORWARD_LEFT: // 7:
			if ( !moveStick.HasChangedMovement )
				break;
			//MoveStop();
			MoveStartForwardLeft();
			moveStick.HasChangedMovement = false;
			break;
		case JoystickReturn.FORWARD_RIGHT: // 8:
			if ( !moveStick.HasChangedMovement )
				break;
			//MoveStop();
			MoveStartForwardRight();
			OnMovePlayer();
			moveStick.HasChangedMovement = false;
			break;
		case JoystickReturn.FORWARD_A: // 1:
			//Debug.Log("SendMovementFlags FA: " + sendMovementFlags);
			if ( !moveStick.HasChangedMovement )
				break;
			//MoveStop();
			MoveStartForward();
			OnMovePlayer();
			moveStick.HasChangedMovement = false;
			if(usedDildau == 0)
			{
				usedDildau = 8;
				PlayerPrefs.SetInt("usedDildau", 8);
			}
			break;
		case JoystickReturn.FORWARD_B:  // 3
			//Debug.Log("SendMovementFlags FB: " + sendMovementFlags);
			//MoveStop();
			MoveStartForward();
			OnMovePlayer();
			if(usedDildau == 0)
			{
				usedDildau = 8;
				PlayerPrefs.SetInt("usedDildau", 8);
			}
			break;
		case JoystickReturn.STOP:  // 2
			MoveStop();
			if(usedDildau == 0)
			{					
				usedDildau = 8;
				PlayerPrefs.SetInt("usedDildau", 8);
			}
			break;
		}
		
		//GUI.Label( new Rect(0,0,Screen.width*0.3f,Screen.height*0.1f),""+transformParent.position); 
		
		// Player Pic
		//GUI.Box(playerNameRect, "");
		//GUI.Box(playerIconRect, "");
		//GUI.Label(playerNameRect, name, newskin.label); 
		// Player Health Bar 
		//GUI.Label( new Rect(playerNameRect.x+healthBarWidth*0.1f, playerNameRect.y+playerNameRect.height, healthBarWidth, healthBarHeight), HP+"/"+MaxHP);
		//GUI.DrawTexture( new Rect(playerNameRect.x, playerNameRect.y+playerNameRect.height, healthBarWidth, healthBarHeight), barEmpty);
		//GUI.DrawTexture( new Rect(playerNameRect.x, playerNameRect.y+playerNameRect.height, healthBarWidth*HP/(MaxHP+0.001f), healthBarHeight), barHealth);	
		// Player Mana Bar
		//GUI.Label( new Rect(playerNameRect.x+healthBarWidth*0.1f, playerNameRect.y+playerNameRect.height+healthBarHeight, healthBarWidth, healthBarHeight), Power +"/"+MaxPower);
		//GUI.DrawTexture( new Rect(playerNameRect.x, playerNameRect.y+playerNameRect.height+healthBarHeight, healthBarWidth, healthBarHeight), barEmpty);
		//GUI.DrawTexture( new Rect(playerNameRect.x, playerNameRect.y+playerNameRect.height+healthBarHeight, healthBarWidth*Power/(MaxPower+0.001f), healthBarHeight), barCurrent);
		
		if(target) 
		{
			ButtonOLD.targetState=true;
			
			
			// Target Pic
			//GUI.Box(targetNameRect,""); 
			//GUI.Box(targetIconRect, "");
			// Target Name
			//GUI.Label(targetNameRect, target.name, newskin.label);
			// Target Health Bar 
			//GUI.Label( new Rect(targetNameRect.x+healthBarWidth*0.1f, targetNameRect.y+targetNameRect.height, healthBarWidth, healthBarHeight), target.HP +"/"+target.MaxHP);
			//GUI.DrawTexture( new Rect(targetNameRect.x, targetNameRect.y+targetNameRect.height, healthBarWidth, healthBarHeight), barEmpty);
			//GUI.DrawTexture( new Rect(targetNameRect.x, targetNameRect.y+targetNameRect.height, healthBarWidth*target.HP/(0.00001f+target.MaxHP), healthBarHeight), barHealth);
			// Target Mana Bar
			//GUI.Label( new Rect(targetNameRect.x+healthBarWidth*0.1f, targetNameRect.y+targetNameRect.height+healthBarHeight, healthBarWidth, healthBarHeight), target.Power +"/"+target.MaxPower);
			//GUI.DrawTexture( new Rect(targetNameRect.x, targetNameRect.y+targetNameRect.height+healthBarHeight, healthBarWidth, healthBarHeight), barEmpty);
			//GUI.DrawTexture( new Rect(targetNameRect.x, targetNameRect.y+targetNameRect.height+healthBarHeight, healthBarWidth*target.Power/(target.MaxPower+0.00001f), healthBarHeight), barMana);
			
		}
		else
		{
			ButtonOLD.targetState=false;
		} 
		
		/*if(!Button.menuState)
	{
		if(noOldAura!=noAura)
		{
			noOldAura=noAura;
			Button.refreshAuraM=true;
		}
		
	 	noAura=playerSpellManager.auras.Count;
	
	// if(target)
		//Button.refreshAura=true;
		//target.showAura();
		
	}*/
		
		if(moveFlag > 0 & swim_flag > 0 )
			ButtonOLD.diveState=true;
		else
			ButtonOLD.diveState=false;
		
		/*		if(!Button.menuState && !Button.chatState && Button.lootState){
	lootScroll();
	}
	if(!Button.menuState && !Button.chatState && Button.needState){
	needScroll();
	}
*/		if(level < 61 && !trade.trading && !InspectMgr.active)
		{
			if(XPBoosted)
			{
				GUI.Label( new Rect(Screen.width *0.06f, Screen.height * 0.95f, Screen.width * 0.2f, Screen.height * 0.05f), "XP Boost: Active", newskin.customStyles[6]);
			}
			else
			{
				GUI.Label( new Rect(Screen.width *0.06f, Screen.height * 0.95f, Screen.width * 0.2f, Screen.height * 0.05f), "XP Boost: Inactive", newskin.customStyles[6]);
			}
		}
		/*if(showErrorMessage)
	{
		GUI.Label( new Rect((Screen.width/2), (Screen.height/2), 10000,Screen.height * 0.2f),errorMessage);
	}*/
		
	}
	
	void  DrawCloseXChatButton (int windowID)
	{
		float baseSize = 220 * Screen.height / 600; //600 was the height i made the minimap, so it's relative to that screen size i guess
		float _size = 17 * baseSize / 100;
		float buttonXSize = _size*1.4f;
		GUI.DrawTexture( new Rect(0, 0, buttonXSize, buttonXSize), closeXTexture, ScaleMode.ScaleToFit);
		if(GUI.Button( new Rect(0, 0, buttonXSize, buttonXSize), "", new GUIStyle()))
		{
			GUI.FocusControl("");
			chatOpend = false;
			actionBarPanel.StartChat();
		}
	}
	
	public IEnumerator DuelTimer ()
	{
		//Debug.Log("Duel starting in " + duel.secondsToDuel + " ...");
		AddChatInput("Battle starting: " + duel.secondsToDuel);
		yield return new WaitForSeconds(1);
		duel.secondsToDuel--;
		//Debug.Log("Duel starting in " + duel.secondsToDuel + " ...");
		AddChatInput("Battle starting: " + duel.secondsToDuel);
		yield return new WaitForSeconds(1);
		duel.secondsToDuel--;
		//Debug.Log("Duel starting in " + duel.secondsToDuel + " ...");
		AddChatInput("Battle starting: " + duel.secondsToDuel);
		yield return new WaitForSeconds(1);
		duel.displayTimer = false;
	}
	
	public IEnumerator LogOutTimer ()
	{
		while(logOutTimer > 0 && LoggingOut)
		{
			yield return new WaitForSeconds(1);
			logOutTimer--;
		}
	}
	
	Touch BeginTouch;
	Touch EndTouch;
	
	public void  MoveScrollByTouch (  Touch touch ,   Rect rect )
	{
		//_chatScrollVector2
		
		if( rect.Contains(touch.position))
		{
			if (  touch.phase == TouchPhase.Began )  
			{
				BeginTouch = touch;
				
			}
			else
				if (  touch.phase == TouchPhase.Moved ) 
			{
				EndTouch = touch;
				_chatScrollVector2.y -= ( BeginTouch.position.y - EndTouch.position.y )/5; //Sets swipe direction   -Sebek
				
			} 
		}
		
		
	}
	
	//we replace the id of the old spell on the equipslot from interface skill buttons
	
	bool  once = false;
	public void  UpdateStats ()
	{
		base.UpdateStats();
		
		/*		WorldPacket pkt = new WorldPacket();
	pkt.Append(GetUInt32Value((int)UpdateFields.EUnitFields. UNIT_FIELD_BYTES_1)) ;
	Debug.Log("field 3: "+pkt._storage[0]+" 2: "+pkt._storage[1]+" 1: "+pkt._storage[2]+" 0: "+pkt._storage[3]);

*/
		
		//Debug.Log(hasFlag(UpdateFields.EUnitFields.PLAYER_FLAGS, PlayerFlags.PLAYER_FLAGS_GHOST)+"Update stats in pula mea! "+HP+ m_deathState);
		if(m_deathState == DeathState.DEAD && !once)
		{
			/*	MessageWnd ms = new MessageWnd();
		ms.text = "Revive delay";
		ms.counterValues = 10;
		ms.NumberOfButtons = 0;
		ms.OnGUI = Revive;
		ms.rect = new Rect(Screen.width*0.35f,Screen.height*0.2f,Screen.width*0.3f,Screen.height*0.3f);
		windows.Push(ms);*/
			//Debug.Log("Update stats dead");
			if(interf)
			{
				interf.reviveWindow.hideGhost = false;
			}
			pkt = new WorldPacket();
			pkt.SetOpcode(OpCodes.MSG_CORPSE_QUERY);
			RealmSocket.outQueue.Add(pkt);
			
			once = true;
		} //Ghost- spirit was released
		
		if(HP > 1 && m_deathState != DeathState.ALIVE)/*&& !hasFlag(UpdateFields.EUnitFields.PLAYER_FLAGS, PlayerFlags.PLAYER_FLAGS_GHOST)*/
		{
			//	Debug.Log("Update Stats Alive");
			m_deathState = DeathState.ALIVE;
			interf.reviveWindow.reset();
			StopCoroutine(UpdateTimer());
			
			once = false;
		}
		
		else if(HP==0 && m_deathState == DeathState.ALIVE)// set the m_deathState to just_died for playing "die" animation in FixedUpdate()
		{
			//Debug.Log("Update Stats Just Died");
			saveLastDeadPosition();
			m_deathState = DeathState.JUST_DIED;
			if(interf)
			{
				interf.reviveWindow.hideRelease = false;
			}
		}
		
	}
	
	public void  BindPointUpdate ( WorldPacket pkt )
	{
		bind.SetBind(pkt);
		//		Debug.Log("????????????????SMSG_BINDPOINTUPDATE "+bind.homeBind+" map "+bind.homeBindMapId+ "  area"+bind.homeBindAreaId);
	}
	
	public void  PlayerBounded ( int newAreaId )
	{
		bind.homeBindAreaId = newAreaId;
		Debug.Log("????????????????SMSG_PLAYERBOUND "+newAreaId);
	}
	
	public uint mapId;
	uint zoneId;
	uint areaId;
	List<uint> mapList = new List<uint>();
	
	public void InitWorldStates( WorldPacket pkt )//unfinish pkt
	{
		
		mapId	= 	pkt.ReadReversed32bit();
		zoneId	= 	pkt.ReadReversed32bit();
		areaId	= 	pkt.ReadReversed32bit();
		
		if(mapId == 489)
		{
			pkt = new WorldPacket(OpCodes.CMSG_BATTLEFIELD_STATUS, 0);
			RealmSocket.outQueue.Add(pkt);
		}
		
		uint mapCount = pkt.ReadReversed16bit();
		for(uint index = 0; index < mapCount; index++)
		{
			uint newMapID = pkt.ReadReversed32bit();
			int newMapState = pkt.ReadInt();
			mapList.Add(newMapID);
		}
		
		ChannelM.InitChannels( name );
		
		UpdateStats();//harcore moldovenesc
	}	
	
	void  Revive ( byte status )
	{
		pkt = new WorldPacket(OpCodes.CMSG_RESURRECT_RESPONSE,9);
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append((status));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  CorpseQuery ( WorldPacket pkt )
	{			
		int found = pkt.Read();
		if(found == 1)
		{
			CorpseTemplate obj = new CorpseTemplate();
			obj.mapid = pkt.ReadReversed32bit();
			obj.position = new Vector3(pkt.ReadFloat(), pkt.ReadFloat(), pkt.ReadFloat());
			obj.mainMapId = pkt.ReadReversed32bit(); //corpse mapid
			pkt.ReadType(4);// 0
			
			Debug.Log("Corpse Found! "+obj.mapid+" - "+obj.position+" -Corpse "+obj.mainMapId);
			session.corpseList.Add(obj);
			
			corpseLocation.mapid = obj.mainMapId;
			corpseLocation.position	= TransformParent.position;
		}
		else
		{
			Debug.Log("Corpse NotFound!");
			session.removeObjectToSpawn(TYPEID.TYPEID_CORPSE);	
		}
	}
	
	int reclaimCorpseDelay = 0;
	bool  showReviveWindow = false;
//	UIButton uiBoxButton = null;
//	UIButton uiRevButton = null;
//	UITextInstance uiCountText = null; 
//	UITextInstance uiMessageText = null; 
//	float menuButtonWidth = Screen.width*0.30f;
//	float menuButtonHeight = Screen.height*0.30f;
//	UIText text;
//
//
//	void  ReviveWindow ()
//		{
//		//uiBoxButton = UIButton.create(UIPrime31.firstToolkit,"",itemImageDrag,touchPosition.x - xFromTouch,touchPosition.y - yFromTouch,0);
//		
//		if(!uiBoxButton)
//		{	 
//			uiBoxButton = UIButton.create(UIPrime31.myToolkit2,"cbDown.png", "cbDown.png", 0, 0);
//			uiBoxButton.centerize();
//			uiBoxButton.color = Color(0,0,0,0.5f);
//			uiBoxButton.setSize(menuButtonWidth,menuButtonHeight);
//			uiBoxButton.position = new Vector3(Screen.width*0.6f,-Screen.height*0.35f,0);
//			uiBoxButton.hidden=false;
//		}	
//		
//		if(!text)
//		{	 
//			text=UIText( UIPrime31.myToolkit1, "prototype", "prototype.png" );
//			text.alignMode = UITextAlignMode.Center;
//		}
//		
//		if(!uiCountText)
//		{
//			uiCountText = text.addTextInstance(""+10,uiBoxButton.position.x,-uiBoxButton.position.y,1.1f,-1,Color.red,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
//		}
//		
//		if(!uiMessageText)
//		{
//			uiMessageText = text.addTextInstance("u are dead! find your body or use revive altar ",uiBoxButton.position.x,-uiBoxButton.position.y-30,0.5f,-1,Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
//		}	
//		
//
//		if(Button.deadState == false)
//		{
//			uiCountText.clear();
//			int count = reclaimCorpseDelay;
//			uiCountText.text = ""+count;
//		}
//		
//		uiCountText.hidden = false;	
//		uiMessageText.hidden = false;	
//		uiBoxButton.hidden = false;
//		
//		if(reclaimCorpseDelay <= 0.0f)
//		{
//			uiCountText.hidden = true;
//		}
//		
//		if(Button.menuState)
//		{
//			uiCountText.hidden = true;	
//			uiMessageText.hidden = true;	
//			uiBoxButton.hidden = true;
//		}
//	}
	
	
	/*ReclaimCorpse();*/
	public void  CorpseReclaimDelay ( WorldPacket pkt )
	{
		reclaimCorpseDelay = (int)(pkt.ReadReversed32bit()*0.001f);
		Debug.Log("#######################################################################Corpse reclaim delay: "+reclaimCorpseDelay);
		StartCoroutine(UpdateTimer());
	}
	
	//Use this function for any center screen warnings.
	GameObject clone;
	public void  ShowErrorMessage (  string msg )
	{		
		if(!clone)
		{
			clone = UnityEngine.Object.Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/errorMessage"));
		}
		var guiText= clone.GetComponent<GUIText>();
		guiText.text=msg;
		Destroy(clone,3);
	}
	
	
	
	public IEnumerator UpdateTimer ()
	{
		while(reclaimCorpseDelay > 0)
		{
			yield return new WaitForSeconds(1);
			reclaimCorpseDelay--;
			if(interf)
				interf.reviveWindow.updateTimer  = ""+ reclaimCorpseDelay;
		}
		
		interf.reviveWindow.updateTimer = " ";
	}
	
	public void  StopUpdateTimerCoroutine ()
	{
		interf.reviveWindow.updateTimer = " ";
		StopCoroutine(UpdateTimer());
	}

	// Unused function
//	IEnumerator  CheckBag ( int _type )
//	{
//		var url = "http://mithril.worldofmidgard.com/worldofmidgard/functions/get_unlocked_slot_list.php";
//		string[] argname = new string["account_email","password","slot_type"];
//		string[] args = new string[LoginWindow.accountLogin, LoginWindow.passHash, "" + _type];
//		yield return new StartCoroutine(PhpJson.phpj(url,argname,args));
//	}
	
	
	void  DrawEnemyCastBar ()
	{
		Texture border;
		Texture bar;
		bar = Resources.Load<Texture>("GUI/InGame/castBarFront");
		border = Resources.Load<Texture>("GUI/InGame/castBarBack");	
		enemyCastBarRect = new Rect(Screen.width*0.53f,Screen.height*0.165f,Screen.width*0.188f,Screen.height * 0.037f);
		enemyCastBarProgress = new Rect( Screen.width * 0.537f, Screen.height * 0.178f, Screen.width* 0.176f, Screen.height * 0.013f);
		GUI.DrawTexture(enemyCastBarRect, border);
		GUI.DrawTexture( new Rect(enemyCastBarProgress.x, enemyCastBarProgress.y, enemyCastBarProgress.width*((0.0001f+(target as Unit).castTime)/(target as Unit).castTimeTotal), enemyCastBarProgress.height), bar);
	}
	
	
	//function checkUnlockedSlot(_type : int, callBackFunction:function())
	//{
	//	var url="http://mithril.worldofmidgard.com/worldofmidgard/functions/get_unlocked_slot_list.php";
	//	string[] argname=["account_email","password","slot_type"];
	//	string[] args=[LoginWindow.accountLogin, LoginWindow.passHash, "" + _type];
	//	yield return new StartCoroutine(phpjson.phpj(url,argname,args, callBackFunction));
	//}
	
	void  ShowMenuButton ()
	{
		GUI.DrawTexture(_mapRect, _map, ScaleMode.ScaleToFit);
		if(GUI.Button(_mapRect, "", new GUIStyle()))
		{
			interf.startMenu();
			WorldSession.interfaceManager.hideInterface = true;
		}
	}
	
	void  saveLastDeadPosition ()
	{
		lastDeadPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
	}
	
	void  focusOnTarget ()
	{
		if(WorldSession.player.target == null || WorldSession.player.target == WorldSession.player || (WorldSession.player.target as Unit).m_deathState != DeathState.ALIVE)
		{
			return;
		}
		if ( !WorldSession.player.isMoving ) // no sense in reorienting avatar while you are moving
		{
			Quaternion rotation;
			Vector3 distanceb = new Vector3(WorldSession.player.target.transform.position.x, 0, WorldSession.player.target.transform.position.z) - new Vector3(WorldSession.player.TransformParent.position.x, 0, WorldSession.player.TransformParent.position.z);
			//if(Vector3.Dot(Vector3.Cross(mainplayer.transformParent.forward,distanceb),Vector3.up) < 0.0f)
			//angl = -angl;
			rotation =  Quaternion.LookRotation(distanceb);
			Rigidbody rigidbody = WorldSession.player.TransformParent.GetComponent<Rigidbody>();
			rigidbody.rotation = rotation;
			WorldSession.player.BuildPkt(OpCodes.MSG_MOVE_SET_FACING, rotation);
		}
	}
	
	public void  UseMacros ( uint id )
	{
		string MacrosText;
		uint macroNumber = id -7;
		MacrosText = PlayerPrefs.GetString(WorldSession.player.name + " Macro " + macroNumber);
		string[] tempText = MacrosText.Split("\n"[0]);
		for(int _i = 0; _i < tempText.Length; _i++)
		{
			if(tempText[_i].Length > 0)
				WorldSession.player.SendMessage2(tempText[_i]);
		}	
	}
	
	public void  DoPetCommand ( uint spellid ,   uint flag ,   ulong petTargetGuid )
	{
		// Packet size in bytes
		int packetSize = 20;
		
		flag = (flag << 24);
		
		uint data = spellid | flag;
		
		pkt = new WorldPacket(OpCodes.CMSG_PET_ACTION, packetSize); 
		pkt.Add(pet.guid);
		pkt.Add(data);
		pkt.Add(petTargetGuid);
		
		RealmSocket.outQueue.Add(pkt);
	}
	
	public uint getIsCasting ()
	{
		return lastCastedSpellID;
	}
	
	public void  SetIsCasting ( uint spellID )
	{
		lastCastedSpellID = spellID;
	}
	
	public void  RequestGearRating ()
	{
		WorldPacket packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_GET_GEAR_RATING);
		RealmSocket.outQueue.Add(packet);
	}
	
	void  OnMovePlayer ()
	{
		if (tutorialInstance && WorldSession.player.tutorialInstance.tutorialStep == TutorialStages.TUTORIAL_JOYSTICK)
		{
			WorldSession.player.tutorialInstance.CompleteTutorialStage();
		}
	}
	
	public void  ReleaseTarget ()
	{
		target = null;
		WorldPacket paket = new WorldPacket(OpCodes.CMSG_SET_SELECTION, 8);
		UInt64 zeroGuid = 0;
		paket.Add(zeroGuid);
		RealmSocket.outQueue.Add(paket);
	}
	
	bool IsFriendly ( int targetFactionID )
	{
		bool  enemy = true;
		
		uint playerFactionID = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE);
		
		IDictionary playerFactionDictionary = (IDictionary)DefaultTabel.factionTemplate[playerFactionID.ToString()];
		IDictionary targetFactionDictionary = (IDictionary)DefaultTabel.factionTemplate[targetFactionID.ToString()];
		FractionInfo fractionInfo = Fractions.GetFactionInfo((int)(double)(targetFactionDictionary["Name"]));
		
		if(fractionInfo != null && fractionInfo.standing >= 3000 )
		{
			enemy = false;
		}
		else
		{
			int targetOurMask = (int)(double)targetFactionDictionary["ourMask"];
			//int playerFriendlyMask = (int)playerFactionDictionary["friendlyMask"];
			int playerEnemyMask = (int)(double)playerFactionDictionary["hostileMask"];
			
			//bool  friend = ((targetOurMask & playerFriendlyMask) != 0) ? true : false;
			enemy = ((targetOurMask & playerEnemyMask) != 0) ? true : false;
		}
		
		return !enemy;
	}
	
	public void  UpdateActionBarSlots ()
	{
		actionBarPanel.RefreshSlots();
	}
	
	public void  ClearPetActionButtons ()
	{
		actionBarManager.ClearPetSpellBook();
	}
	
	public void SetPetSpellToActionBar(List<uint> parametrs)
	{
		actionBarManager.SetPetSpellToSlot(parametrs[0], ActionButtonType.ACTION_BUTTON_SPELL, parametrs[1]);
	}
	
	public void  SetCastingSpell ( uint spellID )
	{
		SetIsCasting(spellID);
	}
	
	public void  UpdateStateManager ()
	{
		blockTargetChange = true;
		interf.stateManager();
		blockTargetChange = false;
	}
	
	public void  NeedUpdateInventory ()
	{
		if(inv.isActive)
		{
			inv.ClearItems();
			inv.ShowItems();
		}
		else
		{
			ButtonOLD.isNeedRefreshInventory = true;
			updateSlots = true;
		}
	}
	
	public void  NeedUpdateEquip ()
	{
		ButtonOLD.SetRefreshEquip(true);
	}
	
	public void  NeedUpdateBags ()
	{
		ButtonOLD.isNeedRefreshBags = true;
	}
	
	public void  NeedUpdateBanker ()
	{
		if(banker != null && banker.isActive)
		{
			banker.ClearBankerBags();
			banker.ShowBankerBags();
			banker.ShowBankerItems();
			banker.showType = 0;
			banker.Show();
		}
	}
	
	public void  UpdateAllAuras ( UpdateAuraStruct data )
	{
		if(data != null)
		{
			if((data.casterGuid == guid)
			   || (data.unit != null && data.unit.guid == guid))
			{
				ButtonOLD.refreshAuraM=true;
			}
			if((ButtonOLD.targetState)
			   && ((data.casterGuid == targetGUID)
			    || (data.unit != null && data.unit.guid == targetGUID)))
			{
				ButtonOLD.refreshAura=true;
			}
		}
	}
	
	public void  IsTargetFromMainPlayerPaty ( ulong guid )
	{
		isTargetFromPlayerPaty = (group.GetPartyPlayer(guid) != null);
	}
}