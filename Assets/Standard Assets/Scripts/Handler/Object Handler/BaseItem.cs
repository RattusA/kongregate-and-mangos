﻿public class BaseItem {	/// for objects that dont need monobeheviour  (ex: items, bags, )

	protected TYPEID _type;
	protected TYPE _typeMask;	
	protected ushort _valuescount = 0x6;
	protected uint[] _uint32values;
	public ulong guid;
	
	public BaseItem()
	{
		_type = TYPEID.TYPEID_OBJECT;
		_typeMask = TYPE.TYPE_OBJECT;
	}
	
	public void SetType(TYPEID type)
	{
		_type = type;
		switch(_type)
		{

		case TYPEID.TYPEID_ITEM:
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_ITEM/TYPEID.TYPEID_OBJECT " + type);
			_valuescount = 0x40;
			_typeMask = TYPE.TYPE_ITEM;
			break;
		case TYPEID.TYPEID_CONTAINER:
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_CONTAINER " + type);
			_valuescount = 0x8A;
			_typeMask = TYPE.TYPE_CONTAINER;
			break;
		case TYPEID.TYPEID_UNIT:
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_UNIT " + type);
			_valuescount = 0x94;
			_typeMask = TYPE.TYPE_UNIT;
			break;
		case TYPEID.TYPEID_PLAYER:
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_PLAYER " + type);
			_valuescount = 0x52E;
			_typeMask = TYPE.TYPE_PLAYER;
			break;
		case TYPEID.TYPEID_GAMEOBJECT:
			//MonoBehaviour.print("setType - received a GAMEOBJECT " + type);
			_valuescount = 0x12;
			_typeMask = TYPE.TYPE_GAMEOBJECT;
			break;
		case TYPEID.TYPEID_CORPSE:
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_CORPSE " + type);
			_valuescount = 0x24;
			_typeMask = TYPE.TYPE_CORPSE;
			break;
		case TYPEID.TYPEID_DYNAMICOBJECT:
			//MonoBehaviour.print("setType - received type TYPEID.TYPEID_DYNAMICOBJECT " + type);
			_valuescount = 0xC;
			_typeMask = TYPE.TYPE_DYNAMICOBJECT;
			break;
		}
		InitValues();
	}	
	
	public uint GetEntry()
	{
		return _uint32values[(int)UpdateFields.EObjectFields.OBJECT_FIELD_ENTRY];
	}

	public uint[] GetValues()
	{
		return _uint32values;
	}

	protected void InitValues()
	{
		_uint32values = new uint[_valuescount];
	}

	public ushort GetValuesCount()
	{
		return _valuescount;
	}

	public void SetUInt32Value(uint index, uint value)
	{
		_uint32values[index] = value;
	}

	public void SetFloatValue(uint index, float value)
	{
//		ByteBuffer bb = new ByteBuffer();
//		bb.Append(value);
//		_uint32values[index] = (uint)bb.ReadType(4);
		_uint32values[index] = System.BitConverter.ToUInt32(System.BitConverter.GetBytes(value), 0);
	}

	public void SetUInt64Value(uint index, ulong value)
	{
		ByteBuffer bb = new ByteBuffer();
		bb.Append(value);
		_uint32values[index + 1] = (uint)bb.ReadType(4);
		_uint32values[index] = (uint)bb.ReadType(4);
	}

	public uint GetUInt32Value(int index)
	{
		try
		{
			return _uint32values[index];
		}
		catch
		{
			return 0;
		}
	}

	public float GetFloatValue(int index)
	{
//		ByteBuffer bb = new ByteBuffer();
//		bb.Append(GetUInt32Value((int)index));
//		return bb.ReadFloat();
		return System.BitConverter.ToSingle(System.BitConverter.GetBytes(GetUInt32Value((int)index)), 0);
	}

	public ulong GetUInt64Value(int index)
	{
		uint fisrtValue = GetUInt32Value((int)index + 1);
		uint secondValue = GetUInt32Value((int)index);
		ulong ret = fisrtValue;
		ret = ret << 32;
		ret = ret | secondValue;
		return ret;
	}
	
	public ulong GetUInt64ValueReverse(int index)
	{
		uint fisrtValue = GetUInt32Value((int)index);
		uint secondValue = GetUInt32Value((int)index + 1);
		ulong ret = fisrtValue;
		ret = ret << 32;
		ret = ret | secondValue;
		return ret;
	}
	
	public TYPEID GetTypeID()
	{
		return _type;
	}

	public TYPE GetTypeMask()
	{
		return _typeMask;
	}

	public void CopyValues(uint[] val)
	{
		if(val != null)
		{
			InitValues();
			ushort index = 0;
			while(index < _uint32values.Length && index < val.Length)
			{
				_uint32values[index] = val[index];
				index++;
			}
		}
	}
}
