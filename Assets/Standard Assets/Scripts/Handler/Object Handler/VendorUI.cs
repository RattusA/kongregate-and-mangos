using UnityEngine;
using System.Collections.Generic;

public class VendorUI
{
	public bool  windowActive = false;
	byte showType;
	byte windowToReturnTo;
	UITextInstance[] vendorTexts;
	UITextInstance[] itemTexts;
	UIAbsoluteLayout vendorContainer;
	UIAbsoluteLayout vendorContainerForItems;
	UIAbsoluteLayout itemContainer;
	UISprite vendorBackground;
	UISprite vendorDecoration;
	UIButton closeButton;
	UIButton closeXButton;
	UIButton nextPrevButton;
	UIButton buySellButton;
	UIButton selectedItem;
	
	Vector3 tempItmPos;
	
	private MessageWnd messageWnd;
	private NPC npc;
	
	int i;
	int currentPage;
	int maxPage;
	int itemSelected;
	Vector2 itemDescVector;
	UIButton tempButton;
	UIButton bkg;
	
	float vendorVPos;
	float vendorHPos;
	
	string itemInfo;
	string sellPrice = "";
	
	public Item itemObject = null;
	Vector2 scrollViewItemVector;
	int launchCount = 0; //wait 1 launch
	private ScrollZone TextZone = new ScrollZone();

	public VendorUI ( NPC theNPC )
	{
		npc = theNPC;
		itemSelected = 0;
		currentPage = 0;
		maxPage = 0;
		vendorVPos = -Screen.height * 0.21f;
		vendorHPos = Screen.width * 0.05f;
		messageWnd = new MessageWnd();
	}
	
	public void  Enable ()
	{
		foreach(KeyValuePair<int, Bag> bag in WorldSession.player.itemManager.BaseBags)
		{
			bag.Value.PopulateSlots(WorldSession.player.itemManager);
		}
		
		showType = 0;
		windowToReturnTo = 0;
		vendorContainer = new UIAbsoluteLayout();
		vendorContainerForItems = new UIAbsoluteLayout();
		itemContainer = new UIAbsoluteLayout();
		vendorTexts = new UITextInstance[10];
		itemTexts = new UITextInstance[3];
		WorldSession.RequestMithrilCoinsCount();
		ShowInventorySlots();
		WorldSession.player.vendor = this;
	}
	
	void  Disable ()
	{
		DestroyInventorySlots();
		npc.menuState = 0;
		showType = 0;
		currentPage = 0;
		maxPage = 0;
		WorldSession.interfaceManager.hideInterface = false;
		WorldSession.player.interf.stateManager();
		WorldSession.player.blockTargetChange = false;
		WorldSession.player.vendor = null;
		WorldSession.player.isVendorItemInfo = false;
		TextZone.RemoveScrollZone();
	}
	
	public void  Show ()
	{
		if (ButtonOLD.menuState)
		{
			DestroyCurrentWindow();
			Disable();
		}
		
		switch(showType)
		{
		case 1:
			itemInfo = "";
			ShowItemSell();
			break;
		case 2:
			itemInfo = npc.vendor.vendorItemsExtra[itemSelected].statsString;
			ShowItemBuy();	
			break;
		case 0:
			itemInfo = "";
			if(!windowActive)
				ShowVendorWindow();
			break;
		}
	}
	
	void  buyMessageWindow ()
	{
		GUI.DrawTexture( new Rect(0, 0, messageWnd.rect.width, messageWnd.rect.height), WorldSession.player.Background);
		if(GUI.Button( new Rect(messageWnd.rect.width * 0.9f, 0, messageWnd.rect.width * 0.1f, messageWnd.rect.height * 0.1f), "X", WorldSession.player.guildskin.button))
		{
			showType = 0;
		}
		if(WorldSession.player.inv.currentBag == 0)
		{
			GUILayout.BeginArea( new Rect(messageWnd.rect.width * 0.05f, messageWnd.rect.height * 0.2f, messageWnd.rect.width * 0.9f, messageWnd.rect.height * 0.5f));
			itemDescVector = GUILayout.BeginScrollView(itemDescVector);
			GUILayout.Label(WorldSession.player.itemManager.BaseBagsSlots[itemSelected].stats,  WorldSession.player.newskin.customStyles[6]);
			GUILayout.EndScrollView();	
			GUILayout.EndArea();
			
			if(GUI.Button( new Rect(messageWnd.rect.width * 0.3f, messageWnd.rect.height * 0.8f, messageWnd.rect.width * 0.4f, messageWnd.rect.height * 0.1f), "Sell Item", WorldSession.player.guildskin.button))
			{
				npc.vendor.SendSellItem(WorldSession.player.itemManager.BaseBagsSlots[itemSelected].guid, 1);
				showType = 0;
			}
		}
		else
		{
			GUILayout.BeginArea( new Rect(messageWnd.rect.width * 0.05f, messageWnd.rect.height * 0.2f, messageWnd.rect.width * 0.9f, messageWnd.rect.height * 0.5f));
			itemDescVector = GUILayout.BeginScrollView(itemDescVector);
			GUILayout.Label(WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(itemSelected).stats,  WorldSession.player.newskin.customStyles[6]);
			GUILayout.EndScrollView();	
			GUILayout.EndArea();
			
			if(GUI.Button( new Rect(messageWnd.rect.width * 0.3f, messageWnd.rect.height * 0.8f, messageWnd.rect.width * 0.4f, messageWnd.rect.height * 0.1f), "Sell Item", WorldSession.player.guildskin.button))
			{
				npc.vendor.SendSellItem(WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(itemSelected).guid, 1);
				showType = 0;
			}
		}
	}
	
	void  sellMessageWindow ()
	{
		GUI.DrawTexture( new Rect(0, 0, messageWnd.rect.width, messageWnd.rect.height), WorldSession.player.Background);
		if(GUI.Button( new Rect(messageWnd.rect.width * 0.9f, 0, messageWnd.rect.width * 0.1f, messageWnd.rect.height * 0.1f), "X", WorldSession.player.guildskin.button))
		{
			showType = 0;
		}
		GUILayout.BeginArea( new Rect(messageWnd.rect.width * 0.05f, messageWnd.rect.height * 0.2f, messageWnd.rect.width * 0.9f, messageWnd.rect.height * 0.5f));
		itemDescVector = GUILayout.BeginScrollView(itemDescVector);
		GUILayout.Label(npc.vendor.vendorItems[itemSelected].stats + "\n" + npc.vendor.vendorItemsExtra[itemSelected].statsString,  WorldSession.player.newskin.customStyles[6]);
		GUILayout.EndScrollView();	
		GUILayout.EndArea();
		
		if(GUI.Button( new Rect(messageWnd.rect.width * 0.3f, messageWnd.rect.height * 0.8f, messageWnd.rect.width * 0.4f, messageWnd.rect.height * 0.1f), "Buy Item", WorldSession.player.guildskin.button))
		{
			npc.vendor.SendBuyItem(npc.vendor.vendorItemsExtra[itemSelected].itemId, npc.vendor.vendorItemsExtra[itemSelected].vendorSlot, 1);
			showType = 0;
		}
	}
	
	public void  showSellError ( SellFailure msg )
	{
		switch(msg)
		{
		case SellFailure.SELL_ERR_CANT_FIND_ITEM:
			VendorMessage("Can't find item!");
			break;
		case SellFailure.SELL_ERR_CANT_SELL_ITEM:
			VendorMessage("Can't sell item!");
			break;
		case SellFailure.SELL_ERR_CANT_FIND_VENDOR:
			VendorMessage("Can't find item!");
			break;
		case SellFailure.SELL_ERR_YOU_DONT_OWN_THAT_ITEM:
			VendorMessage("You don't own that item!");
			break;
		case SellFailure.SELL_ERR_UNK:
			VendorMessage("Error unknown!");
			break;
		case SellFailure.SELL_ERR_ONLY_EMPTY_BAG:
			VendorMessage("You can sell only empty bag!");
			break;
		default:
			VendorMessage("That item was not found.");
			break;
		}
	}
	
	public void  showBuyError ( BuyFailure msg )
	{
		switch(msg)
		{
		case BuyFailure.BUY_ERR_CANT_FIND_ITEM:
			VendorMessage("Can't find item!");
			break;
		case BuyFailure.BUY_ERR_ITEM_ALREADY_SOLD:
			VendorMessage("That item was already sold.");
			break;
		case BuyFailure.BUY_ERR_NOT_ENOUGHT_MONEY:
			VendorMessage("You don't have enough money.");
			break;
		case BuyFailure.BUY_ERR_SELLER_DONT_LIKE_YOU:
			VendorMessage("You must be friendly with that merchant.");
			break;
		case BuyFailure.BUY_ERR_DISTANCE_TOO_FAR:
			VendorMessage("You are too far.");
			break;
		case BuyFailure.BUY_ERR_ITEM_SOLD_OUT:
			VendorMessage("That item doesn't have any more of that item.");
			break;
		case BuyFailure.BUY_ERR_CANT_CARRY_MORE:
			VendorMessage("You can't buy more of that item.");
			break;
		case BuyFailure.BUY_ERR_RANK_REQUIRE:
			VendorMessage("Your rank is too low for that item.");
			break;
		case BuyFailure.BUY_ERR_REPUTATION_REQUIRE:
			VendorMessage("You must be friendly with that merchant.");
			break;
		default:
			VendorMessage("That item was not found.");
			break;
		}
	}
	
	public void  SetMaxPage ()
	{
		if(npc.vendor.vendorItems != null)
		{
			maxPage = ((npc.vendor.vendorItems.Count-1)/16);
			windowActive = false;
		}
	}
	
	void  ShowVendorSlots ()
	{
		DestroyCurrentWindow();
		string image;
		int alignPos = 0;
		Item item;
		float itmWidth = WorldSession.player.inv.itemWidth;
		float itmHeight = WorldSession.player.inv.itemHeight;
		int maxItemPos = 0;
		int firstPos = 0;
		
		bkg = UIButton.create(UIPrime31.myToolkit3, "inventory_slots.png", "inventory_slots.png", Screen.width * 0.05f, Screen.height * 0.295f, 5);
		bkg.setSize(Screen.width * 0.36f , Screen.height * 0.525f);
		vendorContainer.addChild(bkg);
		
		if(npc.vendor.vendorItems != null)
		{
			firstPos = currentPage * 16;
			if(currentPage < maxPage)
				maxItemPos = firstPos + 16;
			else
				maxItemPos = npc.vendor.vendorItems.Count;
			
			for(int i = firstPos ; i < maxItemPos ; i++)
			{					
				image = "fundal.png";
				item = npc.vendor.vendorItems[i];
				
				if(item != null && item.entry > 0)
				{
					image = DefaultIcons.GetItemIcon(item);
				}
				
				UIToolkit tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
				tempButton = UIButton.create( tempUIToolkit, image, image, 0, 0);
				tempButton.setSize(itmWidth, itmHeight);
				tempButton.info = i;
				tempButton.position = new Vector3(((bkg.position.x + bkg.width*0.071f) + (itmWidth*(alignPos%4))),
				                              ((bkg.position.y - bkg.height*0.071f) - (itmHeight*(alignPos/4))),
				                              -2);
				tempButton.onTouchUpInside += vendorDelegate;
				
				vendorContainerForItems.addChild(tempButton);
				alignPos++;
			}
		}
	}
	
	void  ShowInventorySlots ()
	{
		WorldSession.player.inv.isActive = true;
		WorldSession.player.inv.SetDelegate(itemDelegate);
		WorldSession.player.inv.ShowBags();
		WorldSession.player.inv.ShowItems();
	}
	
	void  DestroyInventorySlots ()
	{
		WorldSession.player.inv.ClearItems();
		WorldSession.player.inv.ClearBags();
		WorldSession.player.inv.isActive = false;
		WorldSession.player.inv.currentBag = 0;
		WorldSession.player.inv.SetDelegate(null);
	}
	
	void  ClearVendorItemSprites ()
	{
		while(vendorContainerForItems._children.Count > 0)
		{
			UISprite sprite = vendorContainerForItems._children[0];
			UIToolkit tempUIToolkit = sprite.manager;
			vendorContainerForItems.removeChild(sprite, false);
			sprite.destroy();
			if(tempUIToolkit)
			{
				GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
			}
		}
	}
	
	void  ClearVendorSlots ()
	{
		if(vendorContainer != null)
		{
			vendorContainer.removeAllChild(true);
		}
		ClearVendorItemSprites();
	}
	
	void  itemDelegate ( UIButton sender )
	{
		itemSelected = sender.info;
		showType = 1;
		selectedItem = sender;
		tempItmPos = sender.position;
	}
	
	void  vendorDelegate ( UIButton sender )
	{
		itemSelected = sender.info;
		showType = 2;
		selectedItem = sender;
		tempItmPos = sender.position;
	}
	
	void  DestroyItemWindow ()
	{
		itemContainer.removeAllChild(true);
		for(i=0 ; i < 3 ; i++)
		{
			if (itemTexts[i] != null)
			{
				itemTexts[i].clear();
				itemTexts[i] = null;
			}
		}
	}
	
	void  DestroyCurrentWindow ()
	{
		ClearVendorSlots();
		for (i=0 ; i<10 ; i++)
		{
			if(vendorTexts[i]!=null)
			{
				vendorTexts[i].clear();
				vendorTexts[i] = null;
			}
		}
	}
	
	void  FreezeCurrentWindow ( bool val )
	{
		foreach(var child in vendorContainer._children)
		{
			if(child is UIButton)
			{	
				(child as UIButton).disabled = val;
			}
		}
		foreach(var child in vendorContainerForItems._children)
		{
			if(child is UIButton)
			{	
				(child as UIButton).disabled = val;
			}
		}
		foreach(var child in WorldSession.player.inv.itemContainer._children)
		{
			if(child is UIButton)
			{	
				(child as UIButton).disabled = val;
			}
		}
	}
	
	void  CloseMessageDelegate ( UIButton sender )
	{
		sellPrice = "";
		WorldSession.player.inv.UnHide();
		DestroyCurrentWindow();
		windowActive = false;
		showType = windowToReturnTo;
		windowToReturnTo = 0;
	}
	
	void  ExitVendorDelegate ( UIButton sender )
	{
		DestroyCurrentWindow();
		Disable();
		WorldSession.interfaceManager.hideInterface = false;
	}
	
	void  PreviousButtonDelegate ( UIButton sender )
	{
		currentPage--;
		windowActive = false;
	}
	
	void  MoreButtonDelegate ( UIButton sender )
	{
		currentPage++;
		windowActive = false;
	}
	
	void  BuyItemDelegate ( UIButton sender )
	{
		sellPrice = "";
		npc.vendor.SendBuyItem(npc.vendor.vendorItemsExtra[itemSelected].itemId, npc.vendor.vendorItemsExtra[itemSelected].vendorSlot, 1);
		CloseItemDelegate(sender);
	}
	
	void  CloseItemDelegate ( UIButton sender )
	{
		sellPrice = "";
		WorldSession.player.isVendorItemInfo = false;
		selectedItem.position = tempItmPos;
		DestroyItemWindow();
		FreezeCurrentWindow(false);
		showType = 0;
	}
	
	void  ShowVendorWindow ()
	{
		DestroyCurrentWindow();
		windowActive = true;
		
		ShowVendorSlots();
		
		vendorBackground = UIPrime31.interfMix.addSprite("vendor_buttons_panel.png" , Screen.width*0.05f , Screen.height*0.815f , 5);
		vendorBackground.setSize(Screen.width*0.78f , Screen.height*0.18f);
		
		closeButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , vendorBackground.position.x + vendorBackground.width*0.78f , -vendorBackground.position.y + vendorBackground.height*0.34f , 4);
		closeButton.setSize(vendorBackground.width*0.2f , vendorBackground.height*0.34f);
		closeButton.onTouchUpInside += ExitVendorDelegate;
		
		vendorContainer.addChild(vendorBackground , closeButton);
		
		if(currentPage > 0)
		{
			nextPrevButton = UIButton.create(UIPrime31.interfMix , "previeus_button.png" , "previeus_button.png" , vendorBackground.position.x + vendorBackground.width*0.02f , -vendorBackground.position.y + vendorBackground.height*0.55f , 3);
			nextPrevButton.setSize(vendorBackground.width*0.22f , vendorBackground.height*0.34f);
			nextPrevButton.onTouchUpInside += PreviousButtonDelegate;
			vendorContainer.addChild(nextPrevButton);
		}
		if(currentPage < maxPage)
		{
			nextPrevButton = UIButton.create(UIPrime31.interfMix , "more_button.png" , "more_button.png" , vendorBackground.position.x + vendorBackground.width*0.02f , -vendorBackground.position.y + vendorBackground.height*0.13f , 3);
			nextPrevButton.setSize(vendorBackground.width*0.22f , vendorBackground.height*0.34f);
			nextPrevButton.onTouchUpInside += MoreButtonDelegate;
			vendorContainer.addChild(nextPrevButton);
		}
		
		vendorTexts[0] = WorldSession.player.interf.text1.addTextInstance("Gold: "+WorldSession.player.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COINAGE).ToString() , vendorBackground.position.x + vendorBackground.width*0.27f , -vendorBackground.position.y + vendorBackground.height*0.33f , UID.TEXT_SIZE*0.8f , 2 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Middle);
		vendorTexts[1] = WorldSession.player.interf.text1.addTextInstance("Mithril: "+WorldSession.player.mithrilCoins.ToString() , vendorBackground.position.x + vendorBackground.width*0.27f , -vendorBackground.position.y + vendorBackground.height*0.7f , UID.TEXT_SIZE*0.8f , 2 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Middle);
	}
	
	void  VendorMessage ( string message )
	{
		showType = 5;
		DestroyCurrentWindow();
		WorldSession.player.inv.Hide();
		
		vendorBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.3f , -5);
		vendorBackground.setSize(Screen.width*0.55f , Screen.height*0.45f);
		
		vendorDecoration = UIPrime31.questMix.addSprite("ornament4.png" , vendorBackground.position.x + vendorBackground.width*0.03f , -vendorBackground.position.y + vendorBackground.height*0.22f , -6);
		vendorDecoration.setSize(vendorBackground.width*0.9f , vendorBackground.height*0.33f);
		
		closeButton = UIButton.create(UIPrime31.myToolkit3 , "button_OK.png" , "button_OK.png" , vendorBackground.position.x + vendorBackground.width*0.3f , -vendorBackground.position.y + vendorBackground.height*0.72f , -6);
		closeButton.setSize(vendorBackground.width*0.4f , vendorBackground.height*0.16f);
		closeButton.onTouchUpInside += CloseMessageDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(vendorBackground.width*0.1f , vendorBackground.width*0.1f);
		closeXButton.position = new Vector3( vendorBackground.position.x + vendorBackground.width - closeXButton.width/1.5f , vendorBackground.position.y + closeXButton.height/5 , -6);
		closeXButton.onTouchUpInside += CloseMessageDelegate;
		
		vendorContainer.addChild(vendorBackground , vendorDecoration , closeButton , closeXButton);
		
		if (message.Length>20)
		{	
			int tempNum;
			tempNum = message.IndexOf(" " , 19);
			if(tempNum!=-1)
				vendorTexts[8] = WorldSession.player.interf.text3.addTextInstance(message.Substring(0 , tempNum)+"\n"+message.Substring(tempNum+1 , message.Length-tempNum-1) , vendorBackground.position.x + vendorBackground.width * 0.6f , -vendorBackground.position.y + vendorBackground.height*0.25f , UID.TEXT_SIZE*0.7f , -6 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
			else
				vendorTexts[8] = WorldSession.player.interf.text3.addTextInstance(message , vendorBackground.position.x + vendorBackground.width * 0.6f , -vendorBackground.position.y + vendorBackground.height*0.25f , UID.TEXT_SIZE*0.7f , -6 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		}
		else vendorTexts[8] = WorldSession.player.interf.text3.addTextInstance(message , vendorBackground.position.x + vendorBackground.width * 0.6f , -vendorBackground.position.y + vendorBackground.height*0.25f , UID.TEXT_SIZE*0.7f , -6 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
	}
	
	void  CreateItemWindow ()
	{
		showType = 5;
		FreezeCurrentWindow(true);
		DestroyItemWindow();
		
		vendorBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.1f , Screen.height*0.25f , -3);
		vendorBackground.setSize(Screen.width*0.7f , Screen.height*0.7f);
		
		selectedItem.position = new Vector3( vendorBackground.position.x + vendorBackground.width*0.03f , vendorBackground.position.y - vendorBackground.height*0.17f , -4);
		vendorDecoration = UIPrime31.questMix.addSprite("decoration_case.png" , selectedItem.position.x - selectedItem.width*0.03f , -selectedItem.position.y - selectedItem.height*0.03f , -5);
		vendorDecoration.setSize(selectedItem.width*1.2f , selectedItem.height*1.2f);
		itemContainer.addChild(vendorDecoration);
		
		vendorDecoration = UIPrime31.interfMix.addSprite("vendor_decoration.png" , vendorBackground.position.x + vendorBackground.width*0.025f , -vendorBackground.position.y + vendorBackground.height*0.05f , -4);
		vendorDecoration.setSize(vendorBackground.width*0.95f , vendorBackground.height*0.1f);
		itemContainer.addChild(vendorBackground , vendorDecoration);
		
		vendorDecoration = UIPrime31.interfMix.addSprite("vendor_decoration1.png" , vendorBackground.position.x + vendorBackground.width*0.015f , -vendorBackground.position.y + vendorBackground.height*0.25f , -4);
		vendorDecoration.setSize(vendorBackground.width*0.25f , vendorBackground.height*0.45f);
		itemContainer.addChild(vendorDecoration);
		
		vendorDecoration = UIPrime31.questMix.addSprite("background_case.png" , vendorBackground.position.x + vendorBackground.width*0.27f , -vendorBackground.position.y + vendorBackground.height*0.17f , -4);
		vendorDecoration.setSize(vendorBackground.width*0.7f , vendorBackground.height*0.8f);
		itemContainer.addChild(vendorDecoration);
		
		closeButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , vendorBackground.position.x + vendorBackground.width*0.03f , -vendorBackground.position.y + vendorBackground.height*0.85f , -4);
		closeButton.setSize(vendorBackground.width*0.22f , vendorBackground.height*0.093f);
		closeButton.onTouchUpInside += CloseItemDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(vendorBackground.width*0.08f , vendorBackground.width*0.08f);
		closeXButton.position = new Vector3( vendorBackground.position.x + vendorBackground.width - closeXButton.width/1.5f , vendorBackground.position.y + closeXButton.height/5 , -6);
		closeXButton.onTouchUpInside += CloseItemDelegate;
		
		CreateInfoScrollZone();
	}
	
	void  ShowItemBuy ()
	{	
		CreateItemWindow();
		
		buySellButton = UIButton.create(UIPrime31.interfMix , "buy_button.png" , "buy_button.png" , vendorBackground.position.x + vendorBackground.width*0.03f , -vendorBackground.position.y + vendorBackground.height*0.71f , -4);
		buySellButton.setSize(vendorBackground.width*0.22f , vendorBackground.height*0.09f);
		buySellButton.onTouchUpInside += BuyItemDelegate;
		
		itemContainer.addChild(buySellButton , closeButton , closeXButton);
		string tempText;
		string tempText2;
		int tempNum;
		
		string tempStr = ""; //disable colored tags
		
		Debug.Log(npc.vendor.vendorItems[itemSelected].stats.ToString());
		itemObject = npc.vendor.vendorItems[itemSelected];
		tempStr = itemObject.GetItemBasicStatsNoTag(); //disable colored tags
		
		tempText2 = tempStr.Substring(tempStr.IndexOf("\n" , 0) + 1 , tempStr.Length - tempStr.IndexOf("\n" , 0) - 1) + npc.vendor.vendorItemsExtra[itemSelected].statsString;
		tempText = "";
		sellPrice = "";
		while(tempText2.Length > 0)
		{
			if(tempText2.Length > 25 && (tempText2.IndexOf("\n" , 0) > 26 || tempText2.IndexOf("\n" , 0) == -1))
			{
				tempNum = tempText2.IndexOf(" " , 20);
				if(tempNum != -1)
				{	
					tempText += tempText2.Substring(0 , tempNum) + "\n";
					tempText2 = tempText2.Substring(tempNum+1 , tempText2.Length-tempNum-1);
				}
				else
				{
					tempText = tempText + tempText2;
					tempText2 = "";
				}
			}
			else
			{
				tempText = tempText + tempText2;
				tempText2 = "";
			}
		}
		
		itemTexts[0] = WorldSession.player.interf.text3.addTextInstance(tempStr.Substring(0 , (tempStr.IndexOf("\n" , 0) >= 0)? tempStr.IndexOf("\n" , 0) : 0) , vendorBackground.position.x + vendorBackground.width * 0.12f , -vendorBackground.position.y + vendorBackground.height*0.04f , UID.TEXT_SIZE*0.9f , -6 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		if(itemTexts[0].text == "")
		{
			itemTexts[0].clear();
			itemTexts[0].text = "unknown";
		}
		
		//itemTexts[1] = WorldSession.player.interf.text1.addTextInstance(tempText , vendorDecoration.position.x + vendorDecoration.width * 0.06f , -vendorDecoration.position.y + vendorDecoration.height*0.05f , UID.TEXT_SIZE*0.7f , -6 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		WorldSession.player.isVendorItemInfo = true;
		launchCount = 0;
	}
	
	void  ShowItemSell ()
	{	
		CreateItemWindow();
		
		buySellButton = UIButton.create(UIPrime31.interfMix,
		                                "sell_button.png",
		                                "sell_button.png",
		                                vendorBackground.position.x + vendorBackground.width*0.03f,
		                                -vendorBackground.position.y + vendorBackground.height*0.71f,
		                                -4);
		buySellButton.setSize(vendorBackground.width*0.22f , vendorBackground.height*0.09f);
		buySellButton.onTouchUpInside += SellItemDelegate;
		
		itemContainer.addChild(buySellButton , closeButton , closeXButton);
		
		string tempText;
		string tempText2;
		int tempNum;
		
		string tempStr = ""; //disable colored tags
		
		if(WorldSession.player.inv.currentBag == 0)
		{
			Debug.Log(WorldSession.player.itemManager.BaseBagsSlots[itemSelected].stats);
			itemObject = WorldSession.player.itemManager.BaseBagsSlots[itemSelected];
		}
		else
		{
			Debug.Log(WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(itemSelected).stats);
			itemObject = WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(itemSelected);
		}
		tempStr = itemObject.GetItemBasicStatsNoTag(); //disable colored tags
		
		if(itemObject.sellPrice > 0 || (itemObject.buyPrice == 0 && itemObject.sellPrice > 10000))
		{
			sellPrice = "Sell price: " + itemObject.sellPrice + " gold\n";
		}
		
		tempText2 = tempStr.Substring(tempStr.IndexOf("\n" , 0) + 1 , tempStr.Length - tempStr.IndexOf("\n" , 0) - 1);
		
		tempText = "";
		while(tempText2.Length > 0)
		{
			if(tempText2.Length > 25 && (tempText2.IndexOf("\n" , 0) > 26 || tempText2.IndexOf("\n" , 0) == -1))
			{
				tempNum = tempText2.IndexOf(" " , 20);
				if(tempNum != -1)
				{	
					tempText += tempText2.Substring(0 , tempNum) + "\n";
					tempText2 = tempText2.Substring(tempNum+1 , tempText2.Length-tempNum-1);
				}
				else
				{
					tempText = tempText + tempText2;
					tempText2 = "";
				}
			}
			else
			{
				tempText = tempText + tempText2;
				tempText2 = "";
			}
		}
		
		tempStr = tempStr.Substring(0 , (tempStr.IndexOf("\n" , 0) >= 0)? tempStr.IndexOf("\n" , 0) : 0);
		itemTexts[0] = WorldSession.player.interf.text3.addTextInstance(tempStr,
		                                                                vendorBackground.position.x + vendorBackground.width*0.12f,
		                                                                -vendorBackground.position.y + vendorBackground.height*0.04f,
		                                                                UID.TEXT_SIZE*0.8f,
		                                                                -6,
		                                                                Color.white,
		                                                                UITextAlignMode.Left,
		                                                                UITextVerticalAlignMode.Top);
		
		if(itemTexts[0].text == "")
		{
			itemTexts[0].clear();
			itemTexts[0].text = "unknown";
		}
		
		//		itemTexts[1] = WorldSession.player.interf.text1.addTextInstance(tempText,
		//					vendorDecoration.position.x + vendorDecoration.width * 0.06f,
		//					-vendorDecoration.position.y + vendorDecoration.height*0.05f,
		//					UID.TEXT_SIZE*0.7f,
		//					-6,
		//					Color.white,
		//					UITextAlignMode.Left,
		//					UITextVerticalAlignMode.Top);
		WorldSession.player.isVendorItemInfo = true;
		launchCount = 0;
	}
	
	public void  DrawItemInfo ()
	{
		if(launchCount < 1)
		{
			launchCount++;
			return;
		}
		float fontSize = WorldSession.player.statsFontSize*1.3f;
		if(dbs._sizeMod != 2)
		{
			fontSize = fontSize * (Screen.width / 1024.0f);
		}
		string stats = itemObject.GetItemBasicStatsOutName();
		stats += sellPrice;
		stats = "<size=" + (uint)fontSize + ">" + stats + npc.vendor.vendorUI.itemInfo + "</size>"; //"><color=" + WorldSession.player.mainTextColor + "</color>"
		
		TextZone.SetNewText(stats);
		TextZone.DrawTraitsInfo();
		//		GUILayout.BeginArea(statsRect);
		//			scrollViewItemVector = GUILayout.BeginScrollView(scrollViewItemVector);
		//				GUILayout.Label(stats, WorldSession.player.newskin.GetStyle("DescriptionAndStats"));
		//			GUILayout.EndScrollView();	
		//	 	GUILayout.EndArea();
	}
	
	//============ for the Sell item dialog
	UITextInstance sellItemQuestion;
	UITextInstance sellOneText;
	UITextInstance sellAllText;
	UITextInstance sellCancelText;
	UISprite sellItemPopup; 
	UISprite decorationTexture6;
	UISprite decorationTexture7;
	UIButton sellOneButton;
	UIButton sellAllButton;
	UIButton sellCancelButton;
	UIAbsoluteLayout sellItemContainer;
	UIText text1;
	UIText text3;
	
	void  SellItemDelegate ( UIButton sender )
	{
		CloseItemDelegate(sender);
		FreezeCurrentWindow(true);
		
		text1 = new UIText (UIPrime31.myToolkit1, "fontiny dark", "fontiny dark.png");
		text3 = new UIText (UIPrime31.myToolkit1, "fontiny simple", "fontiny simple.png");
		
		Item item;
		if(WorldSession.player.inv.currentBag == 0)
		{
			item = WorldSession.player.itemManager.BaseBagsSlots[itemSelected];
		}
		else
		{
			item = WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(itemSelected);
		}
		uint itemCount = item.GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT);
		
		sellItemContainer = new UIAbsoluteLayout();
		sellItemContainer.removeAllChild(true);
		
		sellItemPopup = UIPrime31.myToolkit4.addSprite( "popup_background.png", Screen.width/3, Screen.height/4, -3 );
		sellItemPopup.setSize(Screen.width/1.7f, Screen.height/3);
		
		decorationTexture6 = UIPrime31.myToolkit3.addSprite( "decoration_02.png",
		                                                    sellItemPopup.position.x + sellItemPopup.width/30,
		                                                    -sellItemPopup.position.y + sellItemPopup.height*0.2f,
		                                                    -4 );
		decorationTexture6.setSize(sellItemPopup.height*0.4f, sellItemPopup.height*0.4f );
		
		decorationTexture7 = UIPrime31.myToolkit3.addSprite( "decoration_01.png",
		                                                    decorationTexture6.position.x + decorationTexture6.width*0.8f,
		                                                    -decorationTexture6.position.y + decorationTexture6.height*0.8f,
		                                                    -4 );
		decorationTexture7.setSize(sellItemPopup.width*0.8f, sellItemPopup.height * 0.1f );
		
		sellOneButton = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png",
		                                sellItemPopup.position.x + sellItemPopup.width*0.12f, 
		                                -sellItemPopup.position.y + sellItemPopup.height*0.65f,
		                                -4 );
		sellOneButton.setSize( sellItemPopup.width/4, sellItemPopup.height/5); 
		sellOneButton.centerize();
		sellOneButton.info = 1;
		sellOneButton.onTouchDown += SellItem;
		
		sellAllButton = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png",
		                                sellItemPopup.position.x + sellItemPopup.width*0.39f,
		                                -sellItemPopup.position.y + sellItemPopup.height*0.65f,
		                                -4 );
		sellAllButton.setSize( sellItemPopup.width/4, sellItemPopup.height/5); 
		sellAllButton.centerize();
		sellAllButton.info = (int)itemCount;
		sellAllButton.onTouchDown += SellItem;
		sellAllButton.hidden = (itemCount == 1);
		
		sellCancelButton = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png",
		                                   sellItemPopup.position.x + sellItemPopup.width*0.66f,
		                                   -sellItemPopup.position.y + sellItemPopup.height*0.65f,
		                                   -4 );
		sellCancelButton.setSize( sellItemPopup.width/4, sellItemPopup.height/5); 
		sellCancelButton.centerize();
		sellCancelButton.onTouchDown += SellCancel;
		
		sellOneText = text1.addTextInstance("One", sellOneButton.position.x , -sellOneButton.position.y , 0.4f , -6 , Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);	// the destroy item dialog box
		
		sellAllText = text1.addTextInstance("All", sellAllButton.position.x , -sellAllButton.position.y , 0.4f , -6 , Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);	// the destroy item dialog box
		sellAllText.hidden = (itemCount == 1);
		
		sellCancelText = text1.addTextInstance("Cancel", sellCancelButton.position.x , -sellCancelButton.position.y , 0.4f ,-6 ,Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);	// the destroy item dialog box
		
		sellItemQuestion = text3.addTextInstance("Do you want to sell item(s)?", decorationTexture6.position.x + decorationTexture6.width*1.2f , -decorationTexture6.position.y + decorationTexture6.height/6 , 0.5f ,-6,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Top);	// the destroy item dialog box
		
		sellItemContainer.addChild(sellItemPopup, decorationTexture6, decorationTexture7, sellOneButton, sellAllButton, sellCancelButton); 
	}
	
	public void  SellItem ( UIButton sender )
	{
		ulong itemGuid;
		if(WorldSession.player.inv.currentBag == 0)
		{
			Debug.Log("INVENTORY BASEBAG");
			itemGuid = WorldSession.player.itemManager.BaseBagsSlots[itemSelected].guid;
		}
		else
		{
			itemGuid = WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(itemSelected).guid;
			//			Debug.Log("INVENTORY BAG NO. " + (WorldSession.player.itemManager.FindBagForItem(WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(itemSelected)) - 1).ToString());
		}
		npc.vendor.SendSellItem(itemGuid, (uint)sender.info);
		SellCancel(sender);
	}	
	
	public void  SellCancel (UIButton sender)
	{
		sellItemContainer.removeAllChild(true);;
		sellItemQuestion.clear();
		sellOneText.clear();
		sellAllText.clear();
		sellCancelText.clear();
		FreezeCurrentWindow(false);
	}
	//============
	
	void  CreateInfoScrollZone ()
	{
		float rectWidth = vendorBackground.width * 0.7f - vendorDecoration.width * 0.06f * 2;
		float rectHeigth = vendorBackground.height * 0.8f - vendorDecoration.height * 0.05f * 2;
		Rect statsRect = new Rect(vendorDecoration.position.x + vendorDecoration.width * 0.06f,
		                          -vendorDecoration.position.y + vendorDecoration.height*0.05f,
		                          rectWidth,
		                          rectHeigth);
		TextZone.SetFontSizeByRatio(1.0f);
		TextZone.InitSkinInfo(WorldSession.player.newskin.GetStyle("DescriptionAndStats"));
		TextZone.CreateScrollZoneWithText(statsRect, "", WorldSession.player.mainTextColor);
	}
	
}
