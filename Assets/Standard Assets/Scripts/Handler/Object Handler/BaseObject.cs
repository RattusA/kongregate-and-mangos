﻿using UnityEngine;

public class BaseObject : MonoBehaviour {

	//Managers
	public AuraManager auraManager = null;

	//custom spells vars
	public bool stealth = false;
	public bool shapeshift = false;	

	protected int _valuescount = 0x6;
	protected uint[] _uint32values;
	protected TYPEID _type = 0;
	protected TYPE _typeMask = 0;

	protected uint level = 1;

	Transform _sparkleMarker;
	private Transform transformParent;

	public float moveSpeed = 6.5f;
	public float scale;

	//cast bar
	public uint castSpellID = 0;
	public bool isConductedSpell = false;
	public float castTime = 0.0f;
	public float castTimeTotal = 0.0f;

	public ulong guid;

	public Transform TransformParent
	{
		get 
		{
			return (transformParent == null) ? transform : transformParent;
		}
		set
		{
			transformParent = value;
		}
	}

	public uint GetLevel()
	{
		return level;
	}

	public void SetType(TYPEID type)
	{
		_type = type;
		switch(_type)
		{
		case TYPEID.TYPEID_OBJECT:
			_valuescount = 0x40;			//0x06;
			_typeMask = TYPE.TYPE_ITEM;
			break;
		case TYPEID.TYPEID_ITEM: 
			_valuescount = 0x40;
			_typeMask = TYPE.TYPE_ITEM;
			break;
		case TYPEID.TYPEID_CONTAINER :
			_valuescount = 0x8A;
			_typeMask = TYPE.TYPE_CONTAINER;
			break;
		case TYPEID.TYPEID_UNIT :
			_valuescount = 0x94;
			_typeMask = TYPE.TYPE_UNIT;
			break;
		case TYPEID.TYPEID_PLAYER :
			_valuescount = 0x52E;
			_typeMask = TYPE.TYPE_PLAYER;
			break;
		case TYPEID.TYPEID_GAMEOBJECT :
			_valuescount = 0x12;
			_typeMask = TYPE.TYPE_GAMEOBJECT;
			break;
		case TYPEID.TYPEID_CORPSE :
			_valuescount = 0x24;
			_typeMask = TYPE.TYPE_CORPSE;
			break;
		case TYPEID.TYPEID_DYNAMICOBJECT :
			_valuescount = 0xC;
			_typeMask = TYPE.TYPE_DYNAMICOBJECT;
			break;
		}
		InitValues();
	}
	
	public bool IsCreature()
	{
		return (_typeMask == TYPE.TYPE_UNIT) && !IsPlayer();
	}
	
	public bool IsPlayer()
	{
		return (_type == TYPEID.TYPEID_PLAYER);
	}
	
	public TYPEID GetTypeID()
	{
		return _type;
	}
	
	public TYPE GetTypeMask()
	{
		return _typeMask;
	}
	
	public void InitValues()
	{
		_uint32values = new uint[_valuescount];
	}
	
	public int GetValuesCount()
	{
		return _valuescount;
	}
	
	public void SetUInt32Value(uint index, uint value)
	{
		_uint32values[index] = value;
	}
	
	public void SetFloatValue(uint index, float value)
	{
//		ByteBuffer bb = new ByteBuffer();
//		bb.Append(value);
//		_uint32values[index] = (uint)bb.ReadType(4);
		_uint32values[index] = System.BitConverter.ToUInt32(System.BitConverter.GetBytes(value), 0);
	}
	
	public void SetUInt64Value(uint index, ulong value)
	{
		ByteBuffer bb = new ByteBuffer();
		bb.Append(value);
		_uint32values[index + 1] = (uint)bb.ReadType(4);
		_uint32values[index] = (uint)bb.ReadType(4);
	}
	
	public uint GetUInt32Value(int index)
	{
		try
		{
			return _uint32values[index];
		}
		catch
		{
			return 0;
		}
	}
	
	public float GetFloatValue(int index)
	{
//		ByteBuffer bb = new ByteBuffer();
//		bb.Append(GetUInt32Value((int)index));
//		return bb.ReadFloat();
		return System.BitConverter.ToSingle(System.BitConverter.GetBytes(GetUInt32Value((int)index)), 0);
	}
	
	public ulong GetUInt64Value(int index)
	{
		uint fisrtValue = GetUInt32Value((int)index + 1);
		uint secondValue = GetUInt32Value((int)index);
		byte shift = 32;
		ulong ret = fisrtValue;
		ret = ret << shift;
		ret = ret | secondValue;
		return ret;
	}
	
	public ulong GetUInt64ValueReverse(int index)
	{
		uint fisrtValue = GetUInt32Value((int)index);
		uint secondValue = GetUInt32Value((int)index + 1);
		byte shift = 32;
		ulong ret = fisrtValue;
		ret = ret << shift;
		ret = ret | secondValue;
		return ret;
	}
	
	public uint GetEntry()
	{
		return GetUInt32Value((int)UpdateFields.EObjectFields.OBJECT_FIELD_ENTRY);
	}
	
	public float GetScale()
	{
		return GetFloatValue((int)UpdateFields.EObjectFields.OBJECT_FIELD_SCALE_X);
	}
	
	public void CopyValues(uint[] val)
	{
		if(val != null)
		{
			InitValues();
			ushort index = 0;
			while(index < _uint32values.Length && index < val.Length)
			{
				_uint32values[index] = val[index];
				index++;
			}
		}
	}
	
	public uint[] GetValues()
	{
		return _uint32values;
	}
	
	public bool HasFlag(UpdateFields.EUnitFields index, uint flag)
	{
		int intIndex = (int)index;
		if(intIndex < _valuescount)
		{
			uint ret = GetUInt32Value(intIndex);
			if((ret & flag) > 0)
			{
				return true;
			}
		}
		return false;
	}
	
	public virtual void Start()
	{
		if(auraManager == null)
		{
			auraManager = new AuraManager(this);
		}
		transformParent = transform.parent;
	}
	
	public Transform GetParent()
	{
		return transformParent;
	}
	
	public void SetPosition(Vector3 pos, float o)
	{
		Transform transformForModification = (transformParent == null) ? transform : transformParent;
		transformForModification.position = new Vector3(pos.x, pos.z, pos.y);
		Vector3 newEuler = transformForModification.eulerAngles;
		newEuler.y = (Mathf.PI * 0.5f - o) * 180f / Mathf.PI;
		transformForModification.eulerAngles = newEuler;
	}

	public float GetOrientation()
	{
		Transform transformForModification = (transformParent == null) ? transform : transformParent;
		float ret = 0.0f;
		ret = (Mathf.PI * 0.5f) - (transformForModification.eulerAngles.y * Mathf.PI / 180.0f);
		return ret;
	}
	
	public Vector3 GetPositionForServer()
	{
		return new Vector3(transformParent.position.x, transformParent.position.z, transformParent.position.y);
	}

	public void CreateSparkleEffect()
	{
		AssetBundleManager.SpellEffectCallBackStruct callBackStruct = new AssetBundleManager.SpellEffectCallBackStruct();
		callBackStruct.speed = 0;
		callBackStruct.delayTime = 0;
		callBackStruct.gender = Gender.GENDER_NONE;
		callBackStruct.effectNameId = -1;
		callBackStruct.linkedObject = null;
		callBackStruct.targetObject = null;
		callBackStruct.goalPosition = Vector3.zero;
		callBackStruct.resultObject = null;
		callBackStruct.assetPath = "prefabs/SpellEffects/LootMarker";
		WorldSession.GetBundleLoader().LoadSpellEffectBundle(ApplySparkleEffectBundle, callBackStruct);
	}
	
	public void DestroySparkleEffect()
	{
		if(_sparkleMarker != null)
		{
			Destroy(_sparkleMarker.gameObject);
		}
	}

	private void ApplySparkleEffectBundle(AssetBundleManager.SpellEffectCallBackStruct callBackStruct)
	{
		string bundleName = callBackStruct.assetPath;
		string assetBundleName = string.Format ("Resources/{0}_prefab.unity3d", bundleName);
		assetBundleName = assetBundleName.ToLower();
		Object bundle = null;
		AssetBundleManager.instance.currentLoadedBundles.TryGetValue(assetBundleName, out bundle);
		GameObject resourceToInstantiate = null;
		if(bundle == null)
		{
			#if UNITY_EDITOR
			Debug.LogError(string.Format ("cannot load {0}", assetBundleName));
			#endif
		}
		else
		{
			resourceToInstantiate = bundle as GameObject;
		}
		if(resourceToInstantiate == null)
		{
			resourceToInstantiate = Resources.Load<GameObject>(bundleName);
		}
		
		if(resourceToInstantiate != null)
		{
			GameObject lootMarker = Instantiate<GameObject>(resourceToInstantiate);
			if(lootMarker != null)
			{
				lootMarker.name = "LootMarker";
				lootMarker.transform.position = TransformParent.position;
				lootMarker.transform.parent = transformParent;
				_sparkleMarker = lootMarker.transform;
			}
		}
	}
}

public interface IBaseObject
{
	void CheckLootedFlag();
}

