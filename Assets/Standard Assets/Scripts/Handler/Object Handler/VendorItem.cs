﻿using UnityEngine;

public class VendorItem
{
	public uint vendorSlot;
	public uint itemId;
	public uint displayInfoId;
	public uint maxCount;
	public uint price;
	public uint maxDurability;
	public uint buyCount;
	public uint extendedCost;
	
	public string statsString;
	
	public VendorItem ()
	{
		vendorSlot = 0;
		itemId = 0;
		displayInfoId = 0;
		maxCount = 0;
		price = 0;
		maxDurability = 0;
		buyCount = 0;
		extendedCost = 0;
		statsString = "";
	}
	
	public void  BuildStatsString ()
	{
		if (maxDurability != 0) //OMG -Durability
			statsString += "Durability: " + maxDurability.ToString() + "\n";
		statsString += "Stack count: " + buyCount.ToString() + "\n";
		if(price > 0)
		{
			statsString += "Price: " + price.ToString() + " gold\n";
		}
		else
		{
			Debug.Log("Extended cost: " + extendedCost);
			switch(extendedCost)
			{
			case 1015:
				statsString += "Cost: 25 Mark of Dokk\n";
				break;
			case 1027:
				statsString += "Cost: 41 Mark of Dokk\n";
				break;
			case 1037:
				statsString += "Cost: 33 Mark of Dokk\n";
				break;
			case 1040:
				statsString += "Cost: 50 Mark of Dokk\n";
				break;
			case 1452:
				statsString += "Cost: 20 Mark of Dokk\n";
				break;
			case 1454:
				statsString += "Cost: 30 Mark of Dokk\n";
				break;
			case 1642:
				statsString += "Cost: 15 Mark of Dokk\n";
				break;
			case 1909:
				statsString += "Cost: 10 Mark of Dokk\n";
				break;
			case 2049:
				statsString += "Cost: 75 Mark of Dokk\n";
				break;
			case 2059:
				statsString += "Cost: 60 Mark of Dokk\n";
				break;
			case 2060:
				statsString += "Cost: 35 Mark of Dokk\n";
				break;
			case 2329:
				statsString += "Cost: 150 Mark of Dokk\n";
				break;
			case 2330:
				statsString += "Cost: 125 Mark of Dokk\n";
				break;
			case 2331:
				statsString += "Cost: 105 Mark of Dokk\n";
				break;
			case 2332:
				statsString += "Cost: 45 Mark of Dokk\n";
				break;
			case 2333:
				statsString += "Cost: 100 Mark of Dokk\n";
				break;
			case 2347:
				statsString += "Cost: 40 Mark of Dokk\n";
				break;
			case 2478:
				statsString += "Cost: 3 Amber\n";
				break;
			case 2479:
				statsString += "Cost: 5 Amber\n";
				break;
			case 2480:
				statsString += "Cost: 1 Amber\n";
				break;
			case 2541:
				statsString += "Cost: 10 Amber\n";
				break;
			case 2597:
				statsString += "Mithril Coins: " + 15 + "\n";
				break;
			case 2598:
				statsString += "Mithril Coins: " + 50 + "\n";
				break;
			case 2599:
				statsString += "Mithril Coins: " + 40 + "\n";
				break;
			case 2600:
				statsString += "Mithril Coins: " + 100 + "\n";
				break;
			case 2601:
				statsString += "Mithril Coins: " + 25 + "\n";
				break;
			case 2602:
				statsString += "Mithril Coins: " + 10 + "\n";
				break;
			case 2603:
				statsString += "Mithril Coins: " + 150 + "\n";
				break;
			case 2670:
				statsString += "Cost: 100 Amber\n";
				break;
			case 2671:
				statsString += "Mithril Coins: " + 5 + "\n";
				break;
			case 2673:
				statsString += "Mithril Coins: " + 60 + "\n";
				break;
			case 2674:
				statsString += "Mithril Coins: " + 95 + "\n";
				break;
			case 2675:
				statsString += "Mithril Coins: " + 75 + "\n";
				break;
			case 2998:
				statsString += "Mithril Coins: " + 199 + "\n";
				break;
			case 2999:
				statsString += "Mithril Coins: " + 299 + "\n";
				break;
			case 3000:
				statsString += "Mithril Coins: " + 1999 + "\n";
				break;
			case 3001:
				statsString += "Mithril Coins: " + 799 + "\n";
				break;
			case 3002:
				statsString += "Mithril Coins: " + 99 + "\n";
				break;
			case 3003:
				statsString += "Mithril Coins: " + 39 + "\n";
				break;
			case 3004:
				statsString += "Mithril Coins: " + 59 + "\n";
				break;
			case 3005:
				statsString += "Mithril Coins: " + 149 + "\n";
				break;
			case 3006:
				statsString += "Mithril Coins: " + 499 + "\n";
				break;
			case 3007:
				statsString += "Mithril Coins: " + 9 + "\n";
				break;
			case 3008:
				statsString += "Cost: 1 Amber\n";
				break;
			case 3009:
				statsString += "Cost: 70 Mark of Dokk\n";
				break;
			case 3010:
				statsString += "Mithril Coins: " + 399 + "\n";
				break;
			case 3011:
				statsString += "Mithril Coins: " + 599 + "\n";
				break;
			case 3012:
				statsString += "Mithril Coins: " + 699 + "\n";
				break;
			case 3013:
				statsString += "Mithril Coins: " + 899 + "\n";
				break;
			case 3014:
				statsString += "Mithril Coins: " + 999 + "\n";
				break;
			case 3015:
				statsString += "Cost: 2 Amber\n";
				break;
			case 3016:
				statsString += "Cost: 3 Amber\n";
				break;
			case 3017:
				statsString += "Cost: 4 Amber\n";
				break;
			case 3018:
				statsString += "Cost: 5 Amber\n";
				break;
			case 3019:
				statsString += "Cost: 6 Amber\n";
				break;
			case 3020:
				statsString += "Cost: 7 Amber\n";
				break;
			case 3021:
				statsString += "Cost: 8 Amber\n";
				break;
			case 3022:
				statsString += "Cost: 9 Amber\n";
				break;
			case 3023:
				statsString += "Cost: 10 Amber\n";
				break;
			case 3024:
				statsString += "Cost: 11 Amber\n";
				break;
			case 3025:
				statsString += "Cost: 12 Amber\n";
				break;
			case 3026:
				statsString += "Cost: 13 Amber\n";
				break;
			case 3027:
				statsString += "Cost: 14 Amber\n";
				break;
			case 3028:
				statsString += "Cost: 15 Amber\n";
				break;
			case 3029:
				statsString += "Cost: 16 Amber\n";
				break;
			case 3030:
				statsString += "Cost: 17 Amber\n";
				break;
			case 3031:
				statsString += "Cost: 18 Amber\n";
				break;
			case 3032:
				statsString += "Cost: 19 Amber\n";
				break;
			case 3033:
				statsString += "Cost: 20 Amber\n";
				break;
			case 3034:
				statsString += "Mithril Coins: " + 30 + "\n";
				break;
			case 3035:
				statsString += "Mithril Coins: " + 35 + "\n";
				break;
			case 3036:
				statsString += "Mithril Coins: " + 40 + "\n";
				break;
			case 3037:
				statsString += "Mithril Coins: " + 45 + "\n";
				break;
			case 3038:
				statsString += "Mithril Coins: " + 50 + "\n";
				break;
			case 3039:
				statsString += "Mithril Coins: " + 55 + "\n";
				break;
			case 3040:
				statsString += "Mithril Coins: " + 60 + "\n";
				break;
			case 3041:
				statsString += "Mithril Coins: " + 65 + "\n";
				break;
			case 3042:
				statsString += "Mithril Coins: " + 70 + "\n";
				break;
			case 3043:
				statsString += "Mithril Coins: " + 75 + "\n";
				break;
			case 3044:
				statsString += "Mithril Coins: " + 80 + "\n";
				break;
			case 3045:
				statsString += "Mithril Coins: " + 85 + "\n";
				break;
			case 3046:
				statsString += "Mithril Coins: " + 90 + "\n";
				break;
			case 3047:
				statsString += "Mithril Coins: " + 95 + "\n";
				break;
			case 3048:
				statsString += "Cost: 120 Mark of Dokk\n";
				break;
			case 3049:
				statsString += "Cost: 170 Mark of Dokk\n";
				break;
			case 3050:
				statsString += "Cost: 200 Mark of Dokk\n";
				break;
			case 3051:
				statsString += "Cost: 300 Mark of Dokk\n";
				break;
			case 3052:
				statsString += "Cost: 400 Mark of Dokk\n";
				break;
			case 3053:
				statsString += "Cost: 500 Mark of Dokk\n";
				break;
			case 3054:
				statsString += "Cost: 600 Mark of Dokk\n";
				break;
			case 3055:
				statsString += "Cost: 700 Mark of Dokk\n";
				break;
			case 3056:
				statsString += "Cost: 800 Mark of Dokk\n";
				break;
			case 3057:
				statsString += "Cost: 900 Mark of Dokk\n";
				break;
			case 3058:
				statsString += "Cost: 1000 Mark of Dokk\n";
				break;
			case 3059:
				statsString += "Cost: 1500 Mark of Dokk\n";
				break;
			case 3060:
				statsString += "Cost: 2000 Mark of Dokk\n";
				break;
			case 3061:
				statsString += "Mithril Coins: " + 1 + "\n";
				break;
			case 3062:
				statsString += "Mithril Coins: " + 2 + "\n";
				break;
			case 3063:
				statsString += "Mithril Coins: " + 3 + "\n";
				break;
			case 3064:
				statsString += "Mithril Coins: " + 4 + "\n";
				break;
			case 3066:
				statsString += "Mithril Coins: " + 6 + "\n";
				break;
			case 3067:
				statsString += "Mithril Coins: " + 7 + "\n";
				break;
			case 3068:
				statsString += "Mithril Coins: " + 8 + "\n";
				break;
			case 3071:
				statsString += "Mithril Coins: " + 11 + "\n";
				break;
			case 3072:
				statsString += "Mithril Coins: " + 12 + "\n";
				break;
			case 3073:
				statsString += "Mithril Coins: " + 13 + "\n";
				break;
			case 3074:
				statsString += "Mithril Coins: " + 14 + "\n";
				break;
			case 3076:
				statsString += "Mithril Coins: " + 16 + "\n";
				break;
			case 3077:
				statsString += "Mithril Coins: " + 17 + "\n";
				break;
			case 3078:
				statsString += "Mithril Coins: " + 18 + "\n";
				break;
			case 3079:
				statsString += "Mithril Coins: " + 19 + "\n";
				break;
			case 3080:
				statsString += "Mithril Coins: " + 20 + "\n";
				break;	
			case 3081:
				statsString += "Cost: " + 30 + " Amber\n";
				break;
			case 3082:
				statsString += "Cost: " + 40 + " Amber\n";
				break;
			case 3083:
				statsString += "Cost: " + 50 + " Amber\n";
				break;
			case 3084:
				statsString += "Cost: " + 60 + " Amber\n";
				break;
			case 3085:
				statsString += "Cost: " + 70 + " Amber\n";
				break;
			case 3086:
				statsString += "Cost: " + 80 + " Amber\n";
				break;
			case 3087:
				statsString += "Cost: " + 90 + " Amber\n";
				break;
			case 3088:
				statsString += "Cost: " + 100 + " Amber\n";
				break;
			case 3089:
				statsString += "Cost: " + 150 + " Amber\n";
				break;
			case 3090:
				statsString += "Cost: " + 200 + " Amber\n";
				break;
			case 3091:
				statsString += "Cost: " + 250 + "\n";
				break;
			case 3092:
				statsString += "Cost: " + 300 + "\n";
				break;
			case 3093:
				statsString += "Cost: " + 400 + "\n";
				break;
			case 3094:
				statsString += "Cost: " + 500 + "\n";
				break;
			}
		}
	}
}
