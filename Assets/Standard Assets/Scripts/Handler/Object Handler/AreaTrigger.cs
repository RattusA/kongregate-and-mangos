using UnityEngine;

public class AreaTrigger : MonoBehaviour
{
	public AreaTriggerEntry areaTrigger = new AreaTriggerEntry();

	void  OnTriggerEnter (Collider _collider)
	{
		MainPlayer player= _collider.gameObject.GetComponentInChildren<MainPlayer>() as MainPlayer;
		if(player)
		{
			//Debug.Log("Enter area trigger with id "+areaTrigger.ID);
			player.enteringAnAreaTrigger = areaTrigger.ID;
			WorldPacket pkt = new WorldPacket(OpCodes.CMSG_AREATRIGGER, 4);
			pkt.Append(ByteBuffer.Reverse(areaTrigger.ID));
			RealmSocket.outQueue.Add(pkt); 
		}
	}
	
	void  OnTriggerExit (Collider _collider)
	{
		MainPlayer player= _collider.gameObject.GetComponentInChildren<MainPlayer>() as MainPlayer;
		if(player)
		{
			//Debug.Log("Exit area trigger with id "+areaTrigger.ID);
			player.enteringAnAreaTrigger = 0;
		}
	}
}