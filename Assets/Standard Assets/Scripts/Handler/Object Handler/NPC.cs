using UnityEngine;
using System;
using System.Collections.Generic;

public class NPC : Unit
{
	UIText text = new UIText (UIPrime31.myToolkit1, "fontiny dark","fontiny dark.png");
	
	Vector3 lastpos;
	public Vector3[] path = null;
	public byte pathType;
	private uint time;
	private uint timeLeft;
	private uint pathNextPosition = 0;
	UIButton tempButton;
	bool  isActive = false;
	
	WorldSession session;
	public GossipManager gossip;
	public TrainerManager trainer;
	public Auctioneer auctioneer;
	//	StableMaster stableMaster;
	//	PetTraitsReset petTraitsReset;
	public QuestGiver questGiver;
	public Blacksmith blacksmith;
	public InnKeeper innkeeper;
	public Vendor vendor;
	public Banker banker = null;
	public ReviveAltar reviveAltar;
	public GuildMaster guildMaster;
	bool  fivepc = false;
	public float movetime;
	public float movetimeMax;
	
	//NPC INTERFACE 
	bool  canShowQuests = false;
	bool  canShowBlacksmith = false;
	public bool  canShowReviveAltar = false;
	public bool  canShowGuildMaster = false;
	bool  canShowVendor = false;
	bool  canShowTrainer = false;
	bool  canShowStableMaster = false;
	bool  canShowAuction = false;
	bool  canShowInnKeeper = false;
	bool  canShowBanker = false;
	bool  canShowGossip = false;
	UIAbsoluteLayout npcMenuContainer;
	UISprite npcMenuBackground;
	UIButton closeNPCMenuX;
	UIButton[] actionButton = new UIButton[4];
	UITextInstance[] actionButtonText = new UITextInstance[4];
	
	public byte menuState;
	List<string> options = new List<string>();
	List<Action> actions = new List<Action>();
	private float buttonHeight;
	private float listWidth;
	private float listHeight;
	private float buttonWidth;
	private Vector2 optionsScrollVector;
	
	//Fixed a bug with the sliding of the slope
	private bool  isStatic = false;
	
	//-----------Waypoint move parametrs
	public uint targetWay;
	public float timeLeftFotTargetWaypoint;
	public float timeFotTargetWaypoint;
	public bool  isSimpleMove;
	public float stepSpeed;
	public Vector3 startWayPoint;
	public Vector3 endWayPoint;
	
	//-----------Text and style for MiniMap quest helper
	public string helperText = "";
	public GUIStyle helperStyle = new GUIStyle();
	
	byte frameNumber = 0;
	static byte framesToSkipCalc = 5;
	public bool  needUpdate = false;
	public float startTime = 0;
	
	void  Awake ()
	{
//		float buttonHeight = Screen.height * 0.06f;
//		float listWidth  = Screen.width * 0.35f;
//		float listHeight = buttonHeight * 10;
//		float buttonWidth = listWidth * 0.95f;
		aHandler = new NPCAnimationHandler(this);
	}
	
	void  Start ()
	{
		//questStatus = __QuestGiverStatus.DIALOG_STATUS_NONE;
		helperText = "";
		menuState = 0;
		base.Start();
		
		way=TransformParent.position;
		
		UpdateStats();
		
		if(HP == 0)//if 
		{
			m_deathState = DeathState.DEAD;
		}
		
		AddQuestGiverToMainPlayer();
	}
	
	public void  SetPosition ( Vector3 pos, float o )
	{
		base.SetPosition(pos,o);
	}
	
	public void  SetTime ( uint t )
	{
		timeLeft = time = t;
		pathNextPosition = 0;// donno if it should be here
	}
	
	public void  BuildMenu ()
	{
		options.Add("Exit");
		actions.Add(ExitDelegate);
		if((isGossip() || isTrainer() || isBanker()) && !isInnKeeper())
		{
			if(isPortalMaster())
			{
				options.Add("Portal Master");
			}
			else
			{
				options.Add("Talk");
				canShowGossip = true;
			}
			actions.Add(GossipDelegate);
		}
		
		if(isQuestGiver())
		{
			options.Add("Quests");
			actions.Add(QuestsDelegate);
			canShowQuests = true;
		}
		
		if(isReviveAltar())
		{
			options.Add("Revive Altar");
			actions.Add(ReviveAltarDelegate);
			canShowReviveAltar = true;
		}
		
		/*uint flag = NPCFlags.UNIT_NPC_FLAG_SPIRITGUIDE; // spirithealer in battlegrounds
		if(GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag)
		{
			options.Add("teh spirit guide");
			actions.Add(the spirit guide Delegate);
		}*/
		
		if(isBlacksmith())
		{
			options.Add("Repair");
			actions.Add(RepairDelegate);
			canShowBlacksmith = true;
		}		
		
		if(isInnKeeper())
		{
			options.Add("Altar");
			actions.Add(AltarDelegate);
			canShowInnKeeper = true;
		}
		
		if(isVendor())
		{
			options.Add("Buy/Sell");
			actions.Add(VendorDelegate);
			canShowVendor = true;
		}	
		
		//		if(isBanker())
		//		{
		//			options.Add("Chest");
		//			actions.Add(BankerDelegate);
		//			canShowBanker = true;
		//		}
		
		if(isAuctioneer())
		{
			options.Add("Auction");
			actions.Add(AuctioneerDelegate);
			canShowAuction = true;
		}
		
		if(isGuildMaster())
		{
			options.Add("Guild");
			actions.Add(GuildMasterDelegate);
			canShowGuildMaster = true;
		}
	}
	
	void  OnGUI ()
	{
		if (isActive && ButtonOLD.menuState)
		{
			isActive = false;
			CloseNPCMenu(tempButton);
		}
		switch(menuState)
		{
		case 2:
			if(canShowQuests) ShowNPCMenu();
			break;
		case 3:
			if(!needUpdate)
				vendor.vendorUI.Show();
			break;
		case 5:
			//				trainer.Show();
			break;
		case 6:
			if (canShowReviveAltar)reviveAltar.Show();
			break;
		case 7:
			auctioneer.Display();
			break;
		case 8:
			innkeeper.Show();
			break;
		case 9:
			//				banker.Show();
			break;
		case 10:
			guildMaster.Show();
			break;
		case 15:
			blacksmith.Enable();
			blacksmith.Show();
			break;
		case 20:
			if (isQuestGiver())
			{
				menuState = 2;
				WorldSession.player.interf.npc = this;
			}
			else if(isBlacksmith() || isGuildMaster() || isVendor() || isTrainer() || isInnKeeper() || isBanker() || (isGossip() && !isPortalMaster()) || isAuctioneer() || isStabelMaster())
			{
				if(canShowGossip || canShowBlacksmith || canShowGuildMaster || canShowVendor || canShowTrainer || canShowInnKeeper || canShowBanker || canShowAuction || canShowStableMaster)
					ShowNPCMenu();
			}
			else if(isReviveAltar())
			{
				ReviveAltarDelegate();
			}
			else
				ShowMenu();
			//tutorial advance
			//				if (mainPlayer.tutorialInstance) {
			//					if (!mainPlayer.tutHandler.stepsDone[4]) {
			//						mainPlayer.tutHandler.stepsDone[4] = true;
			//					}
			//					if (mainPlayer.tutHandler.tutorialStep == 4) {
			//						mainPlayer.tutHandler.advanceTutorial();						
			//					}
			//				}
			break;
		}
	}
	
	void  ShowMenu ()
	{
		optionsScrollVector = GUI.BeginScrollView( new Rect(Screen.width * 0.1f, Screen.height * 0.2f, Screen.width * 0.3f, Screen.height * 0.6f), optionsScrollVector, new Rect(0, 0, Screen.width * 0.3f, Screen.height * 0.08f * options.Count));
		for(int i= 0; i < options.Count; i++)
		{
			if(GUI.Button( new Rect(0, Screen.height *0.08f * i, Screen.width * 0.15f, Screen.height * 0.06f), options[i], WorldSession.player.guildskin.button))
			{
				actions[i]();
				options.Clear();
				actions.Clear();
			}
		}
		GUI.EndScrollView();
	}
	
	public void  changeStatus ( byte status )
	{
		GameObject gg; 
		Transform rootChild = transform.Find("Bip01/state");
		Transform[] childs= rootChild.gameObject.GetComponentsInChildren<Transform>();
		
		if(childs != null)
		{
			foreach(Transform ch in childs)
			{
				if(ch && ch.gameObject.name != "state")
				{
					GameObject.Destroy(ch.gameObject);
				}
			}
		}
		
		switch((__QuestGiverStatus)status)
		{
		case __QuestGiverStatus.DIALOG_STATUS_AVAILABLE:
			gg= UnityEngine.Object.Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/objects/hasQuest"));
			gg.transform.parent = rootChild;
			gg.transform.localPosition =  new Vector3(-0.26f,1.7f,0.11f);
			helperText = "!";
			helperStyle.normal.textColor = Color.yellow;
			
			//MonoBehaviour.print("World: SMSG_QUESTGIVER_STATUS- AVAILABLE");
			break;
		case __QuestGiverStatus.DIALOG_STATUS_REWARD:
			gg = UnityEngine.Object.Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/objects/getReward"));
			gg.transform.parent = rootChild;
			gg.transform.localPosition =  new Vector3(-0.26f,1.7f,0.11f);

			gg.transform.localEulerAngles = new Vector3(gg.transform.localEulerAngles.x,
			                                            303.0f,
			                                            gg.transform.localEulerAngles.z);
			helperText = "?";
			helperStyle.normal.textColor = Color.yellow;
			
			//MonoBehaviour.print("World: SMSG_QUESTGIVER_STATUS- DIALOG_STATUS_REWARD");
			break;
		case __QuestGiverStatus.DIALOG_STATUS_INCOMPLETE:
			gg = UnityEngine.Object.Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/objects/unavailable2"));
			gg.transform.parent = rootChild;
			gg.transform.localPosition =  new Vector3(-0.26f,1.7f,0.11f);
			helperText = "?";
			helperStyle.normal.textColor = Color.white;
			
			//MonoBehaviour.print("World: SMSG_QUESTGIVER_STATUS- DIALOG_STATUS_INCOMPLETE");
			break;
		case __QuestGiverStatus.DIALOG_STATUS_UNAVAILABLE:
			gg = UnityEngine.Object.Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/objects/unavailable"));
			gg.transform.parent = rootChild;
			gg.transform.localPosition =  new Vector3(-0.26f,1.7f,0.11f);
			helperText = "!";
			helperStyle.normal.textColor = Color.white;
			
			//MonoBehaviour.print("World: SMSG_QUESTGIVER_STATUS- DIALOG_STATUS_UNAVAILABLE");
			break;
		default: //Debug.Log("No state found! State:"+status) ;
			helperText = "";
			helperStyle.normal.textColor = Color.white;
			break;
			
		}
	}

	public void  UpdateStats ()
	{
		base.UpdateStats();
		
		if(HP > 0)
		{
			m_deathState = DeathState.ALIVE;
		}
		else if(HP <= 0 && m_deathState != DeathState.DEAD)
		{
			m_deathState = DeathState.CORPSE;
			if(WorldSession.player.target && WorldSession.player.target.guid == guid)
			{
				WorldSession.player.OldLootGuid = 0;
				WorldSession.player.ReleaseTarget();
			}
		}
	}
	
	public void  FixedUpdate ()
	{
		float deltaTime = Time.deltaTime;
		base.FixedUpdate();
		
		//Debug.Log("Enter fixed");
		Animation animation = GetComponent<Animation>();
		if(!animation)
			return;
		
		bool  dead = false;
		if(m_deathState == DeathState.DEAD)
		{
			if(!animation.IsPlaying("die"))
			{
				aHandler.PlayMovementAnimation("dead");
			}
			isStatic = true;
			dead = true;
		}		 
		else if(m_deathState == DeathState.CORPSE)// getHP
		{
			aHandler.PlayMovementAnimation("die");
			m_deathState = DeathState.DEAD;
			path = null;
			target = null;
			
			isStatic = true;
			dead = true;
		}
		else
		{
			//Debug.Log("Enter fixed alive");
			if(movetime > 0)
			{
				string movementName = "run";
				AnimationState runAnimation = animation[movementName];
				if(runAnimation == null)
				{
					movementName = "walk";
				}
				aHandler.PlayMovementAnimation(movementName);
				
				isStatic = false;
				if(target == null)
				{
					OrientationChange(false);
				}
				if(isSimpleMove)
				{
					TransformParent.position = Vector3.Lerp(TransformParent.position, way, deltaTime / movetime);
					movetime -= deltaTime;
					if((way - TransformParent.position).sqrMagnitude < 0.01f)
					{
						movetime = 0;
						OrientationChange(true);
					}
					//Debug.Log("movetime = "+movetime+" deltaTime = "+deltaTime+" sqrMagnitude = "+(way - transformParent.position).sqrMagnitude);
				}
				else
				{
					TransformParent.position = Vector3.Lerp(TransformParent.position, endWayPoint, deltaTime / timeLeftFotTargetWaypoint);
					movetime -= deltaTime;
					timeLeftFotTargetWaypoint -= deltaTime;
					if((way - TransformParent.position).sqrMagnitude < 0.01f)
					{
						movetime = 0;
						OrientationChange(true);
					}
					else if((endWayPoint - TransformParent.position).sqrMagnitude < 0.005f)
					{
						TransformParent.position = endWayPoint;
						getNextDistance();
						//Debug.Log( "CALL getNextDistance." );
					}
					//Debug.Log("movetime = "+movetime+" timeLeftFotTargetWaypoint = "+timeLeftFotTargetWaypoint+" sqrMagnitude = "+((endWayPoint - transformParent.position).sqrMagnitude));
				}
			}
			else
			{
				aHandler.PlayMovementAnimation("idle");
				isStatic = true;
			}
			if(target)
			{
				OrientationChange(true);
				if ((target as Unit).HP == 0)
				{
					target = null;
				}
			}
		}
		
		if(!dead)
		{
			//Debug.Log("Enter fixed Exit");
			if (HP < 0.05f * MaxHP && !fivepc && Name.Equals("RockFace"))
			{
				TransformParent.localScale += new Vector3(1,1,1);
				fivepc = true;	
			}
		}
		if(parentRB)
		{
			Rigidbody rigidbody = parentRB.GetComponent<Rigidbody>();
			rigidbody.isKinematic = isStatic;
		}
	}
	
	public bool IsTriggeredReviveAltar ()
	{
		bool  rasp = (HasFlag(UpdateFields.EUnitFields.UNIT_NPC_FLAGS, (uint)NPCFlags.UNIT_NPC_FLAG_SPIRITHEALER) && 
		              HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_NOT_SELECTABLE));
		
		Debug.Log("Is revive altar "+rasp);
		return rasp;
	}
	
	public bool isReviveAltar ()
	{
		return (HasFlag(UpdateFields.EUnitFields.UNIT_NPC_FLAGS, (uint)NPCFlags.UNIT_NPC_FLAG_SPIRITHEALER));
	}
	
	public bool isPortalMaster ()
	{
		return (isGossip() && HasFlag(UpdateFields.EUnitFields.UNIT_NPC_FLAGS, (uint)NPCFlags.UNIT_NPC_FLAG_UNK1));
	}
	
	public bool isPortalMasterTriggered ()
	{
		//Debug.Log(guid + "  Is special? "+hasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, UnitFlags.UNIT_FLAG_NOT_SELECTABLE) );
		return (isGossip() && HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_NOT_SELECTABLE));
	}
	
	public bool isGossip ()
	{
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_GOSSIP;	
		//Debug.Log(guid+" isGossip "+(GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) );
		return (GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0;
	}	
	
	public bool isCreature ()
	{
		//Debug.Log("**************** " + GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) + "*****************");
		return GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) == 0 ? true : false;
	}
	
	public bool isInnKeeper ()
	{
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_INNKEEPER;	
		return (GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0;
	}
	
	public bool isVendor ()
	{
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_VENDOR;	
		return (GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0;
	}
	
	public bool isQuestGiver ()
	{
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_QUESTGIVER;	
		return (GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0;
	}
	
	public bool isBlacksmith ()
	{
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_REPAIR;	
		return (GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0;
	}
	
	public bool isTrainer ()
	{
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_TRAINER;	
		return (GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0;
	}
	
	public bool isStabelMaster ()
	{
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_STABLEMASTER;
		return (GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0;
	}
	
	public bool isBanker ()
	{
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_BANKER;	
		return (GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0;
	}	
	
	public bool isAuctioneer ()
	{
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_AUCTIONEER;	
		return (GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0;
	}
	
	public bool isGuildMaster ()
	{
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_PETITIONER;	
		return (GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0;
	}
	
	void  ExitDelegate ()
	{
		WorldSession.player.blockTargetChange = false;
		WorldSession.interfaceManager.hideInterface = false;
		//		WorldSession.interfaceManager.SetWindowState(InterfaceWindowState.ACTION_BAR_PANEL_STATE);
		WorldSession.player.interf.stateManager();
		isActive = false;
	}
	
	void  ActionButtonDelegate ( UIButton sender )
	{	
		//		if(mainPlayer.toggleLeftSmall.hidden == true)
		//		{
		//			//tempButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , Screen.height , 0);
		//			//tempButton.info = 1;
		//			//mainPlayer.ToggleHUDSpellState(tempButton);
		//			//tempButton.destroy();
		//			//tempButton = null;
		//			if(sender.info!=0)
		//				mainPlayer.ToggleHUDSpellState(1);
		//		}
		
		actions[sender.info]();
		DestroyNPCMenu();
	}
	
	void  CloseNPCMenu ( UIButton sender )
	{	
		menuState = 0;
		ExitDelegate();
		DestroyNPCMenu();
		options.Clear();
		actions.Clear();
	}
	
	void  GossipDelegate ()
	{
		gossip.GetGossipMenu();
		ExitDelegate();
	}
	
	void  QuestsDelegate ()
	{
		//questGiver = new QuestGiver(this);
		//advancing tutorial
		//		if (mainPlayer.tutorialInstance) {
		//			if (!mainPlayer.tutHandler.stepsDone[5]) {
		//				mainPlayer.tutHandler.stepsDone[5] = true;
		//			}   			
		//			if (mainPlayer.tutHandler.tutorialStep == 5) {
		//				mainPlayer.tutHandler.advanceTutorial();						
		//			}
		//		}
		//menuState = 2;
		questGiver.ShowListOfQuests();
	}
	
	void  ShowNPCMenu ()
	{	
		isActive = true;
		if(canShowGossip)
		{	
			gossip = new GossipManager(this.guid);
			canShowGossip = false;
		}
		if(canShowQuests)
		{	
			questGiver = new QuestGiver(this);
			canShowQuests = false;
		}
		if(canShowBlacksmith)
		{
			blacksmith = new Blacksmith(this);
			canShowBlacksmith = false;
		}
		if(canShowGuildMaster)
		{
			guildMaster = new GuildMaster(this);
			canShowGuildMaster = false;
		}
		if(canShowVendor)
		{
			vendor = new Vendor(this);
			canShowVendor = false;
		}
		//		if(canShowTrainer)
		//		{
		////			trainer = new TrainerManager(this.guid);
		//			canShowTrainer = false;
		//		}
		//		if(canShowStableMaster)
		//		{
		//			stableMaster = new StableMaster(this);
		//			petTraitsReset = new PetTraitsReset(this);
		//			canShowStableMaster = false;
		//		}
		if(canShowInnKeeper)
		{
			innkeeper = new InnKeeper(this);
			canShowInnKeeper = false;
		}
		//		if(canShowBanker)
		//		{
		//			banker = new Banker(this);
		//			canShowBanker = false;
		//		}
		if(canShowAuction)
		{
			//auctioneer = new Auctioneer(this);
			canShowAuction = false;
		}
		npcMenuContainer = new UIAbsoluteLayout();
		npcMenuContainer.removeAllChild(true);
		
		npcMenuBackground = UIPrime31.myToolkit4.addSprite("popup_background.png", Screen.width/4, Screen.height/4, 0);
		npcMenuBackground.setSize(Screen.width/2 , Screen.height/2);
		
		closeNPCMenuX = UIButton.create( UIPrime31.myToolkit3 , "close_button.png", "close_button.png", 0 , 0 , -1);
		closeNPCMenuX.setSize(npcMenuBackground.height * 0.15f , npcMenuBackground.height * 0.15f);
		closeNPCMenuX.position = new Vector3(npcMenuBackground.position.x + npcMenuBackground.width - closeNPCMenuX.width * 0.6f , npcMenuBackground.position.y + closeNPCMenuX.height * 0.4f , -1 );
		closeNPCMenuX.onTouchUpInside += CloseNPCMenu;
		
		npcMenuContainer.addChild(npcMenuBackground , closeNPCMenuX);
		
		for(int i= 0; i < options.Count; i++)
		{	
			if(options[i] != null)
			{
				actionButton[i] = UIButton.create( UIPrime31.myToolkit4 , "3_selected.png", "3_selected.png", 0 , 0 , -1);
				actionButton[i].setSize(npcMenuBackground.width*0.35f , npcMenuBackground.height*0.15f);
				actionButton[i].centerize();
				actionButton[i].position = new Vector3(npcMenuBackground.position.x + npcMenuBackground.width*0.275f, npcMenuBackground.position.y - npcMenuBackground.height*0.5f , -1);
				actionButton[i].info = i;
				actionButton[i].onTouchUpInside += ActionButtonDelegate;
				npcMenuContainer.addChild(actionButton[i]);
				
				actionButtonText[i] = text.addTextInstance(options[i], 0 , 0 , UID.TEXT_SIZE*0.8f , -2 , Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
			}
		}
		
		switch(options.Count)
		{
		case 2:
			actionButton[0].position += Vector3.right * (npcMenuBackground.width/2);
			break;
			
		case 3:
			actionButton[0].position = new Vector3(actionButton[0].position.x + npcMenuBackground.width/2,
			                                       npcMenuBackground.position.y - npcMenuBackground.height*0.3f,
			                                       actionButton[0].position.z);
			actionButton[1].position = new Vector3(actionButton[1].position.x,
			                                       actionButton[0].position.y,
			                                       actionButton[1].position.z);
			actionButton[2].position = new Vector3(npcMenuBackground.position.x + npcMenuBackground.width/2,
			                                       npcMenuBackground.position.y - npcMenuBackground.height*0.7f,
			                                       actionButton[2].position.z);
			break;
			
		case 4:
			actionButton[0].position = new Vector3(actionButton[0].position.x + npcMenuBackground.width/2,
			                                       npcMenuBackground.position.y - npcMenuBackground.height*0.3f,
			                                       actionButton[0].position.z);
			actionButton[1].position = new Vector3(actionButton[1].position.x,
			                                       actionButton[0].position.y,
			                                       actionButton[1].position.z);
			actionButton[2].position = new Vector3(actionButton[2].position.x,
			                                       npcMenuBackground.position.y - npcMenuBackground.height*0.7f,
			                                       actionButton[2].position.z);
			actionButton[3].position = new Vector3(actionButton[0].position.x,
			                                       actionButton[2].position.y,
			                                       actionButton[3].position.z);
			break;
		}
		
		for(int i=0 ; i < options.Count ; i++)
		{
			actionButtonText[i].position = new Vector3(actionButton[i].position.x,
			                                           actionButton[i].position.y,
			                                           actionButtonText[i].position.z);
		}
	}	
	
	void  DestroyNPCMenu ()
	{	
		for(int i=0 ; i < options.Count ; i++)
		{
			actionButtonText[i].hidden = true;
		}
		npcMenuContainer.removeAllChild(true);
		options.Clear();
		actions.Clear();
	}
	
	//	function TrainerDelegate()
	//	{
	//		trainer = new TrainerManager(this.guid);
	////		trainer.active = true;
	//		menuState = 5;
	//	}
	
	//	function StableMasterDelegate()
	//	{
	//		if(mainPlayer._class == Classes.CLASS_HUNTER)
	//		{
	//			canShowStableMaster = false;
	//			stableMaster.InitMainWindow();
	//		}
	//	}
	//	
	//	function PetTraitsResetDelegate()
	//	{
	//		if(mainPlayer._class == Classes.CLASS_HUNTER)
	//		{
	//			canShowStableMaster = false;
	//			petTraitsReset.Init();
	//		}
	//	}
	
	void  ReviveAltarDelegate ()
	{
		reviveAltar = new ReviveAltar(this);
		menuState = 6;
	}
	
	void  RepairDelegate ()
	{
		menuState = 15;
	}
	
	void  AltarDelegate ()
	{
		menuState = 8;
	}
	
	void  VendorDelegate ()
	{
		vendor.SendListInventory();
		needUpdate = true;
		startTime = Time.time;
		menuState = 3;
	}
	
	//	function BankerDelegate()
	//	{
	//		banker.SendBankerActivate();
	//	}
	
	void  AuctioneerDelegate ()
	{
		auctioneer = new Auctioneer(this);
		//Auctioneer.active = true;
		menuState = 7;
	}
	
	void  GuildMasterDelegate ()
	{
		//guildMaster = new GuildMaster(this);
		menuState = 10;
	}
	
	//-------------Waypoint move functions
	public void initMove ()
	{
		float moveDistance;
		Vector3 lastPoint;
		float currDistance;
		targetWay = 0;
		timeLeftFotTargetWaypoint = 0.0f;
		timeFotTargetWaypoint = 0.0f;
		if( path.Length == 1 )
		{
			isSimpleMove = true;
		}
		else
		{
			isSimpleMove = false;
			moveDistance = 0.0f;
			
			lastPoint = TransformParent.position;
			
			for(int i = 0 ; i < path.Length;  i++ )
			{
				moveDistance += Vector3.Distance(path[i], lastPoint);
				lastPoint = path[i];
			}
			
			if( moveDistance == 0 )
			{
				return;
			}
			
			stepSpeed = moveDistance / movetimeMax;
			
			startWayPoint = TransformParent.position;
			endWayPoint = path[0];
			currDistance = Vector3.Distance(startWayPoint, endWayPoint);
			
			timeFotTargetWaypoint = currDistance / stepSpeed;
			timeLeftFotTargetWaypoint = timeFotTargetWaypoint;
		}
	}
	
	void getNextDistance ()
	{
		if( targetWay < ( path.Length - 1) )
		{
			startWayPoint = path[targetWay];
			targetWay += 1;   
			endWayPoint = path[targetWay];
			
			float currDistance= Vector3.Distance(startWayPoint, endWayPoint);   
			timeFotTargetWaypoint = currDistance / stepSpeed;
			timeLeftFotTargetWaypoint = timeFotTargetWaypoint;
		}
		else
		{
			movetime = 0;  
		}
	}
	//----------------End waypoint move functions
	
	void  OnDestroy ()
	{
		DelQuestGiverFromMainPlayer();
	}
	
	//----------------Quest giver list control functions
	void  AddQuestGiverToMainPlayer ()
	{
		if(isQuestGiver())
		{
			if(WorldSession.player.questGiverList.Contains(this))
			{
				return;
			}
			
			helperStyle.alignment = TextAnchor.MiddleCenter;
			helperStyle.fontStyle = FontStyle.Bold;
			helperStyle.normal.textColor = Color.white;
			
			WorldSession.player.questGiverList.Add(this);
		}
	}
	
	void  DelQuestGiverFromMainPlayer ()
	{
		if(isQuestGiver())
		{
			WorldSession.player.questGiverList.Remove(this);
		}
	}
	//----------------End quest giver list control functions
	
	void  Update ()
	{
		if(needUpdate && (vendor != null) && (vendor.vendorUI != null))
		{
			float curTime = Time.time;
			if((curTime - startTime) > 0.8f)
			{
				vendor.vendorUI.Enable();
				needUpdate = false;
			}
		}
	}
}