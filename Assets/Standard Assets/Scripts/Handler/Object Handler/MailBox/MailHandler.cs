using System.Collections.Generic;

public enum MailResponseResult
{
	MAIL_OK                            = 0,
	MAIL_ERR_EQUIP_ERROR               = 1,
	MAIL_ERR_CANNOT_SEND_TO_SELF       = 2,
	MAIL_ERR_NOT_ENOUGH_MONEY          = 3,
	MAIL_ERR_RECIPIENT_NOT_FOUND       = 4,
	MAIL_ERR_NOT_YOUR_TEAM             = 5,
	MAIL_ERR_INTERNAL_ERROR            = 6,
	MAIL_ERR_DISABLED_FOR_TRIAL_ACC    = 14,
	MAIL_ERR_RECIPIENT_CAP_REACHED     = 15,
	MAIL_ERR_CANT_SEND_WRAPPED_COD     = 16,
	MAIL_ERR_MAIL_AND_CHAT_SUSPENDED   = 17,
	MAIL_ERR_TOO_MANY_ATTACHMENTS      = 18,
	MAIL_ERR_MAIL_ATTACHMENT_INVALID   = 19,
	MAIL_ERR_ITEM_HAS_EXPIRED          = 21,
};

public enum MailResponseType
{
	MAIL_SEND               = 0,
	MAIL_MONEY_TAKEN        = 1,
	MAIL_ITEM_TAKEN         = 2,
	MAIL_RETURNED_TO_SENDER = 3,
	MAIL_DELETED            = 4,
	MAIL_MADE_PERMANENT     = 5
};

public enum MailMessageType
{
	MAIL_NORMAL         = 0,
	MAIL_AUCTION        = 2,
	MAIL_CREATURE       = 3,                                /// client send CMSG_CREATURE_QUERY on this mailmessagetype
	MAIL_GAMEOBJECT     = 4,                                /// client send CMSG_GAMEOBJECT_QUERY on this mailmessagetype
	MAIL_ITEM           = 5,                                /// client send CMSG_ITEM_QUERY on this mailmessagetype
};

/* MAX_MAIL_ITEMS = 12 */
public class MailHandler
{
	private MainPlayer mainPlayer;
	public List<Letter> mailList = new List<Letter>();
	public MailUI mailUI;
	ulong guid;
	
	public MailHandler ( MainPlayer thePlayer )
	{
		mainPlayer = thePlayer;
		mailUI = new MailUI(thePlayer);
	}
	
	public Letter FindLetterBySenderGuid ( uint guid )
	{
		for(int i= 0; i < mailList.Count; i++)
		{
			if(mailList[i].sender == guid && mailList[i].senderName == "")
			{
				return mailList[i];
			}
		}
		return null;
	}
	
	public void  SetNameForGuid ( string name ,   uint guid )
	{
		for(int i= 0; i < mailList.Count; i++)
			if(mailList[i].sender == guid)
		{
			mailList[i].senderName = name;
		}
	}
	
	public void  SetMBoxGuid ( ulong theGuid )
	{
		guid = theGuid;
	}
	
	public void  HandleShowMailBox ( WorldPacket pkt )
	{
		//Debug.Log("WorldSession: Received SHOW_MAIL_BOX");
		mailUI.Enable();
	}
	
	public void HandleReceivedMail() // only one uint32 sent that is always 0
	{
		mainPlayer.AddChatInput("You have new mail.");
	}
	
	public void  HandleSendMailResult ( WorldPacket pkt )
	{
		uint mailId = pkt.ReadReversed32bit();
		MailResponseType mailAction = (MailResponseType)pkt.ReadReversed32bit();
		MailResponseResult mailError = (MailResponseResult)pkt.ReadReversed32bit();
		uint equipError;
		uint itemGuid;
		uint itemCount;
		
		//Debug.Log("WorldSession: SMSG_SEND_MAIL_RESULT action: " + mailAction + " error: " + mailError);
		if(mailError == MailResponseResult.MAIL_ERR_EQUIP_ERROR)
		{
			equipError = pkt.ReadReversed32bit();
			mailUI.EquipError(equipError);
			return;
		}
		else if(mailAction == MailResponseType.MAIL_ITEM_TAKEN)
		{
			itemGuid = pkt.ReadReversed32bit();
			itemCount = pkt.ReadReversed32bit();
		}
		switch(mailAction)
		{
		case MailResponseType.MAIL_SEND:
			mailUI.sendMailResult(mailError);
			break;
		case MailResponseType.MAIL_MONEY_TAKEN:
			mailUI.moneyTakenResult(mailError);
			break;
		case MailResponseType.MAIL_ITEM_TAKEN:
			mailUI.itemTakenResult(mailError);
			break;
		case MailResponseType.MAIL_RETURNED_TO_SENDER:
			mailUI.returnedToSenderResult(mailError);
			break;
		case MailResponseType.MAIL_DELETED:
			mailUI.deletedResult(mailError);
			break;
		case MailResponseType.MAIL_MADE_PERMANENT:
			mailUI.madePermanentResult(mailError);
			break;
		}
	}
	
	public void  HandleMailListResult ( WorldPacket pkt )
	{
		//Debug.Log("WorldSession: Handling Mail List Result!");
		mailList.Clear();
		uint realMail = pkt.ReadReversed32bit();
		byte mailCount = pkt.Read();
		Letter tmpMail;
		byte numItems;
		byte i;
		byte j;
		Item tmpItem;
		while(realMail > 0)
		{
			tmpMail = new Letter();
			tmpMail.messageSize = pkt.ReadReversed16bit();
			tmpMail.messageId = pkt.ReadReversed32bit();
			tmpMail.messageType = (MailMessageType)pkt.Read();
			tmpMail.sender = pkt.ReadReversed32bit();
			if(tmpMail.messageType == MailMessageType.MAIL_NORMAL)
				pkt.ReadReversed32bit();
			tmpMail.COD = pkt.ReadReversed32bit();
			pkt.ReadReversed32bit();
			pkt.ReadReversed32bit();
			tmpMail.money = pkt.ReadReversed32bit();
			tmpMail.Flags = pkt.ReadReversed32bit();
			tmpMail.expireTime = pkt.ReadFloat();
			pkt.ReadReversed32bit();
			tmpMail.subject = pkt.ReadString();
			tmpMail.body = pkt.ReadString();
			numItems = pkt.Read();
			for(i = 0; i < numItems; i++)
			{
				tmpItem = new Item();
				pkt.Read();
				tmpItem.guid = pkt.ReadReversed32bit();
				tmpItem.entry = pkt.ReadReversed32bit();
				for(j = 0; j < 7; j++)
				{
					pkt.ReadReversed32bit();
					pkt.ReadReversed32bit();
					pkt.ReadReversed32bit();
				}
				pkt.ReadReversed32bit();
				pkt.ReadReversed32bit();
				//				tmpItem.stackCount = pkt.ReadReversed32bit();
				pkt.ReadReversed32bit();
				pkt.ReadReversed32bit();
				pkt.ReadReversed32bit();
				pkt.Read();
				AppCache.sItems.SendQueryItem(tmpItem.entry);
				tmpMail.items.Add(tmpItem);
			}
			mailList.Add(tmpMail);
			if(tmpMail.messageType == MailMessageType.MAIL_NORMAL)
				SendQueryPlayerName(tmpMail.sender);
			realMail--;
		}
	}
	
	void  SendQueryPlayerName ( uint guid )
	{
		WorldPacket pkt = new WorldPacket();
		uint fake32 = 0;
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append(fake32);
		pkt.SetOpcode(OpCodes.CMSG_NAME_QUERY);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendMail ( NewMail mail )
	{
		byte unk8 = 0;
		uint unk32 = 0;
		ulong unk64 = 0;
		//Debug.Log("Sent CMSG_SEND_MAIL");
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_SEND_MAIL);
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append(mail.receiver);
		pkt.Append(mail.subject);
		pkt.Append(mail.body);
		pkt.Append(unk32);
		pkt.Append(unk32);
		pkt.Append((byte)mail.itemCount);
		for(int i= 0; i < mail.itemCount; i++)
		{
			pkt.Append(unk8);
			pkt.Append(ByteBuffer.Reverse(mail.items[i].guid));
		}
		pkt.Append(ByteBuffer.Reverse(mail.gold));
		pkt.Append(ByteBuffer.Reverse(mail.COD));
		pkt.Append(unk64);
		pkt.Append(unk8);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendGetMailList ()
	{
		//Debug.Log("Sent CMSG_GET_MAIL_LIST");
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GET_MAIL_LIST);
		pkt.Append(ByteBuffer.Reverse(guid));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendMailTakeMoney ( int mailId )
	{
		//Debug.Log("Sent CMSG_MAIL_TAKE_MONEY");
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_MAIL_TAKE_MONEY);
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append(ByteBuffer.Reverse(mailList[mailId].messageId));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendMailTakeItem ( int mailId ,   uint itemGuid )
	{
		//Debug.Log("Sent CMSG_MAIL_TAKE_ITEM with itemGuid: " + itemGuid);
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_MAIL_TAKE_ITEM);
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append(ByteBuffer.Reverse(mailList[mailId].messageId));
		pkt.Append(itemGuid);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendMailMarkAsRead ( int mailId )
	{
		//Debug.Log("Sent CMSG_MAIL_MARK_AS_READ");
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_MAIL_MARK_AS_READ);
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append(ByteBuffer.Reverse(mailList[mailId].messageId));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendReturnToSender ( int mailId )
	{
		ulong unk64 = 0;
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_MAIL_RETURN_TO_SENDER);
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append(ByteBuffer.Reverse(mailList[mailId].messageId));
		pkt.Append(unk64);
		RealmSocket.outQueue.Add(pkt);
		//Debug.Log("Sent CMSG_MAIL_RETURN_TO_SENDER");
	}
	
	public void  SendMailDelete ( int mailId )
	{
		//Debug.Log("Sent CMSG_MAIL_DELETE");
		uint unk32 = 0;
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_MAIL_DELETE);
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append(ByteBuffer.Reverse(mailList[mailId].messageId));
		pkt.Append(unk32);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void SendMailCreateTextItem( int mailId ) // store the letter's text
	{
		//Debug.Log("Sent CMSG_MAIL_CREATE_TEXT_ITEM");
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_MAIL_CREATE_TEXT_ITEM);
		pkt.Append(ByteBuffer.Reverse(guid));
		pkt.Append(ByteBuffer.Reverse(mailList[mailId].messageId));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  ReplaceItem ( Item theItem )
	{
		for(int letter= 0; letter < mailList.Count; letter++)
		{
			for(int q= 0; q < mailList[letter].items.Count; q++)
			{
				if(mailList[letter].items[q].entry == theItem.entry)
				{
					theItem.guid = mailList[letter].items[q].guid;
					theItem.GetItemBasicStats();
					mailList[letter].items[q] = null;
					mailList[letter].items[q] = theItem;
					break;
				}
			}
		}
	}
}

