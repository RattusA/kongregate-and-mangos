﻿using System.Collections.Generic;

public class NewMail
{
	public string receiver;
	public string subject;
	public string body;
	public int itemCount;
	public uint gold;
	public uint COD;
	public List<Item> items;
	public NewMail ()
	{
		receiver = "";
		subject = "";
		body = "";
		itemCount = 0;
		gold = 0;
		COD = 0;
		items = new List<Item>();
	}
}
