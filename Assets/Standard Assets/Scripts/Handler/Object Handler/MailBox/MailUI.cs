using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class MailUI
{
	MainPlayer mainPlayer;
	public bool  show;
	int selected;
	
	private MessageWnd messageWnd;
	private int showType;
	
	private Vector2 mailScrollVector;
	private Vector2 textScrollVector;
	private Vector2 itemScrollVector;
	
	private string subject = "";
	private string body = "";
	private string receiver = "";
	private string gold = "";
	private string cod = "";
	private bool  takeGold;
	private List<Item> items;
	
	private NewMail tmpMail;
	
	public bool  isMailItemInfo = false;

	// add items stuff
	int fee;
	int i;
	int itemSelected;
	Vector2 itemDescVector;
	UIButton tempButton;
	UIAbsoluteLayout sendContainerItems;
	UIAbsoluteLayout sendContainerDecoration;
	
	float sendVPos;
	
	//////MAIL uINTERFACE///////////////
	UIAbsoluteLayout mailContainer;
	UIAbsoluteLayout mailItemContainer;
	UIAbsoluteLayout mailMessageContainer;
	UISprite nameTextCase;
	UISprite subjectTextCase;
	UISprite bodyTextCase;
	UISprite goldTextCase;
	UIButton cancelButton;
	UIButton sendButton;
	UIButton attachItemButton;
	UISprite mailBackground;
	UISprite mailDecoration;
	UISprite mailDecoration2;
	UIButton composeMail;
	UIButton exitMail;
	UIButton closeXButton;
	UITextInstance[] mailTexts;
	bool  windowNotActive = false;
	bool  canShowGUI = false;
	int windowToReturnTo = 0;
	UISprite itemDecoration;
	UIButton doneButton;
	UIToggleButton codToggleButton;
	UIButton closeButton;
	UIToggleButton[] mailButtons;
	UITextInstance[] mailButtonText;
	UIScrollableVerticalLayout scrollableContainer;
	NonUIElements.TextBox mailBodyText;
	UIButton deleteMailButton;
	UISprite mailMessageBackground;
	UISprite mailMessageDecoration;
	UIButton closeMessageButton;
	UIButton closeMessageXButton;
	UIButton takeGoldButton;
	UIButton takeItemButton;
	UIButton returnButton;
	
	public MailUI ( MainPlayer thePlayer )
	{
		showType = 0;
		mainPlayer = thePlayer;
		show = false;
		selected = 0;
		messageWnd = new MessageWnd();
		takeGold = false;
		sendVPos = -Screen.height * 0.18f;
		items = new List<Item>();
		itemSelected = 0;
		fee = 30;
		mailTexts = new UITextInstance[10];
		mailContainer = new UIAbsoluteLayout();
		mailItemContainer = new UIAbsoluteLayout();
		mailMessageContainer = new UIAbsoluteLayout();
		mailButtons = new UIToggleButton[50];
		mailButtonText = new UITextInstance[50];
	}
	
	public void  Enable ()
	{
		show = true;
		showType = 0;
		mainPlayer.inv.SetDelegate(itemDelegate);
		mainPlayer.blockTargetChange = true;
		WorldSession.interfaceManager.hideInterface = true;
		mainPlayer.interf.stateManager();
		
		mainPlayer.ReleaseTarget();
		mainPlayer.PortraitReff.UpdateTargetSnapShot(null);
		mainPlayer.interf.PortraitsFrame.HideTargetPortrait(true);
	}
	
	public void  ShowUI ()
	{
		switch(showType)
		{
		case 11:
			for(i = 0; i < mainPlayer.mailHandler.mailList.Count; i++)
			{
				Vector3 tmpPosition = new Vector3(mailButtonText[i].position.x,
				                                  mailButtons[i].position.y-mailButtons[i].height*0.5f,
				                                  mailButtonText[i].position.z);
				mailButtonText[i].position = tmpPosition;
				mailButtonText[i].hidden = mailButtons[i].hidden;
			}
			break;
		case 13:
			for(i = 0; i < 20; i++)
			{
				if(mailButtonText[i]!=null)
				{
					Vector3 tmpPosition = new Vector3(mailButtonText[i].position.x,
					                                  mailButtons[i].position.y-mailButtons[i].height*0.5f,
					                                  mailButtonText[i].position.z);
					mailButtonText[i].position = tmpPosition;
					mailButtonText[i].hidden = mailButtons[i].hidden;
				}
			}
			break;
		case 1: // attach items
			if(windowNotActive)
			{
				windowToReturnTo = showType;
				NewMailWindowWithItems();
			}
			if(canShowGUI)
				newMailWindow();
			break;
		case 2: // new mail
			if(windowNotActive)
			{
				windowToReturnTo = showType;
				NewMailWindow();
			}	
			if(canShowGUI)
				newMailWindow();
			break;
		case 3: // take items
			TakeItemsWindow();
			break;
		case 4: // info on item to be taken
			ShowTakeItemInfo();
			break;
		case 10: // window with current selected mail
			OpenMail(mainPlayer.mailHandler.mailList[selected]);
			break;
		case 0: // main screen
			if(mainPlayer.mailHandler.mailList.Count > 0)
			{	
				ShowMailList();
			}
			else
			{
				NoMail();
			}
			break;
		}
	}
	
	void  infoMessageWindow ()
	{
		GUI.DrawTexture( new Rect(0, 0, messageWnd.rect.width, messageWnd.rect.height), mainPlayer.Background);
		if(GUI.Button( new Rect(messageWnd.rect.width * 0.9f, 0, messageWnd.rect.width * 0.1f, messageWnd.rect.height * 0.1f), 
		              "X", mainPlayer.guildskin.button))
		{
			ClearSendSlots();
			showType = 0;
		}
		GUILayout.BeginArea( new Rect(messageWnd.rect.width * 0.05f, messageWnd.rect.height * 0.2f, messageWnd.rect.width * 0.9f, messageWnd.rect.height * 0.6f));
		itemDescVector = GUILayout.BeginScrollView(itemDescVector);
		GUILayout.Label(mainPlayer.mailHandler.mailList[selected].items[itemSelected].stats, mainPlayer.newskin.customStyles[6]);
		GUILayout.EndScrollView();	
		GUILayout.EndArea();
		if(GUI.Button( new Rect(messageWnd.rect.width * 0.3f, messageWnd.rect.height * 0.8f, messageWnd.rect.width * 0.4f, messageWnd.rect.height * 0.12f), 
		              "Take Item", mainPlayer.guildskin.button))
		{
			mainPlayer.mailHandler.SendMailTakeItem(selected, (uint)(ByteBuffer.Reverse(mainPlayer.mailHandler.mailList[selected].items[itemSelected].guid) >> 32));
			ClearSendSlots();
			showType = 10;
		}
	}
	
	void  showMailWindow ()
	{
		GUI.Label( new Rect(messageWnd.rect.width*0.05f,0,messageWnd.rect.width*0.9f,messageWnd.rect.height*0.2f), 
		          mainPlayer.mailHandler.mailList[selected].subject, mainPlayer.newskin.customStyles[10] );
		
		GUILayout.BeginArea( new Rect(messageWnd.rect.width*0.05f, messageWnd.rect.height * 0.2f, messageWnd.rect.width*0.9f,messageWnd.rect.height*0.4f));
		textScrollVector = GUILayout.BeginScrollView(textScrollVector);
		GUILayout.Label(mainPlayer.mailHandler.mailList[selected].body, mainPlayer.newskin.customStyles[6]);
		GUILayout.EndScrollView();	
		GUILayout.EndArea();
		
		if(mainPlayer.mailHandler.mailList[selected].items.Count > 0)
		{
			if(GUI.Button( new Rect(messageWnd.rect.width*0.3f,messageWnd.rect.height*0.61f,messageWnd.rect.width*0.4f,messageWnd.rect.height*0.12f),
			              "Take items", mainPlayer.guildskin.button))
			{
				if(mainPlayer.mailHandler.mailList[selected].COD > 0)
				{
					messageWnd.title = "WARNING!";
					messageWnd.text = "You will pay " + mainPlayer.mailHandler.mailList[selected].COD + " gold for the items.";
					showType = 6;
				}
				else
				{
					showType = 3;
					ShowTakeSlots();
				}
			}
		}
		
		if(mainPlayer.mailHandler.mailList[selected].money > 0)
		{
			GUI.Label( new Rect(messageWnd.rect.width*0.05f, messageWnd.rect.height * 0.75f, messageWnd.rect.width*0.3f,messageWnd.rect.height*0.1f), "Gold:", mainPlayer.newskin.customStyles[6]);
			GUI.Label( new Rect(messageWnd.rect.width*0.35f, messageWnd.rect.height * 0.75f, messageWnd.rect.width*0.3f,messageWnd.rect.height*0.1f), mainPlayer.mailHandler.mailList[selected].money.ToString(), mainPlayer.newskin.customStyles[6]);
			if(GUI.Button( new Rect(messageWnd.rect.width*0.65f,messageWnd.rect.height*0.75f,messageWnd.rect.width*0.2f,messageWnd.rect.height*0.1f),"Take", mainPlayer.guildskin.button))
			{
				mainPlayer.mailHandler.SendMailTakeMoney(selected);
			}
		}
		
		if(GUI.Button( new Rect(messageWnd.rect.width*0.69f,messageWnd.rect.height*0.87f,messageWnd.rect.width*0.3f,messageWnd.rect.height*0.12f),"Close", mainPlayer.guildskin.button))
		{
			showType = 0;
		}
		
		if(GUI.Button( new Rect(messageWnd.rect.width*0.35f,messageWnd.rect.height*0.87f,messageWnd.rect.width*0.3f,messageWnd.rect.height*0.12f),"Return", mainPlayer.guildskin.button))
		{
			mainPlayer.mailHandler.SendReturnToSender(selected);
			showType = 0;
		}
		
		if(GUI.Button( new Rect(messageWnd.rect.width*0.01f,messageWnd.rect.height*0.87f,messageWnd.rect.width*0.3f,messageWnd.rect.height*0.12f),"Delete", mainPlayer.guildskin.button))
		{
			mainPlayer.mailHandler.SendMailDelete(selected);
			showType = 0;
		}
		/* another time...
		if(GUI.Button( new Rect(mW.rect.width*0.05f,mW.rect.height*0.80f,mW.rect.width*0.15f,mW.rect.height*0.12f),"Save copy", mainPlayer.guildskin.button))
		{
			mainPlayer.mailHandler.SendMailCreateTextItem(selected);
			WindowActive = false;
		}
*/
	}
	
	void  newMailWindow ()
	{
		if(showType==2)
		{
			receiver = GUI.TextField( new Rect(Screen.width*0.425f,Screen.height*0.327f,Screen.width*0.302f,Screen.height*0.06f),receiver, 25 , mainPlayer.guildskin.textField);//max 25 chars
			
			subject = GUI.TextField( new Rect(Screen.width*0.425f,Screen.height*0.424f,Screen.width*0.302f,Screen.height*0.06f),subject, 60 , mainPlayer.guildskin.textField);//max 60 chars
			
			body = GUI.TextField( new Rect(Screen.width*0.425f,Screen.height*0.52f,Screen.width*0.303f,Screen.height*0.178f),body, 500 , mainPlayer.guildskin.textField);//max 500 chars
			
			if(takeGold)
			{
				cod = GUI.TextField( new Rect(Screen.width*0.425f,Screen.height*0.745f,Screen.width*0.302f,Screen.height*0.06f),cod, mainPlayer.guildskin.textField);
				cod = Regex.Replace(cod, "[^0-9]", "");
			}
			else
			{
				gold = GUI.TextField( new Rect(Screen.width*0.425f,Screen.height*0.745f,Screen.width*0.302f,Screen.height*0.06f),gold, mainPlayer.guildskin.textField);
				gold = Regex.Replace(gold, "[^0-9]", "");
			}
		}
		else if(showType==1)
		{
			receiver = GUI.TextField( new Rect(Screen.width*0.505f,Screen.height*0.328f,Screen.width*0.275f,Screen.height*0.06f),receiver, 25 , mainPlayer.guildskin.textField);//max 25 chars
			
			subject = GUI.TextField( new Rect(Screen.width*0.431f,Screen.height*0.416f,Screen.width*0.35f,Screen.height*0.06f),subject, 60 , mainPlayer.guildskin.textField);//max 60 chars
			
			body = GUI.TextField( new Rect(Screen.width*0.43f,Screen.height*0.505f,Screen.width*0.351f,Screen.height*0.179f),body, 500 , mainPlayer.guildskin.textField);//max 500 chars
			
			if(takeGold)
			{
				cod = GUI.TextField( new Rect(Screen.width*0.505f,Screen.height*0.776f,Screen.width*0.275f,Screen.height*0.06f),cod, mainPlayer.guildskin.textField);
				cod = Regex.Replace(cod, "[^0-9]", "");
			}
			else
			{
				gold = GUI.TextField( new Rect(Screen.width*0.505f,Screen.height*0.776f,Screen.width*0.275f,Screen.height*0.06f),gold, mainPlayer.guildskin.textField);
				gold = Regex.Replace(gold, "[^0-9]", "");
			}
		}
	}
	
	void  errorMessageWindow ()
	{
		GUI.DrawTexture( new Rect(0, 0, messageWnd.rect.width, messageWnd.rect.height), mainPlayer.Background);
		GUI.Label( new Rect(messageWnd.rect.width*0.05f,0,messageWnd.rect.width*0.9f,messageWnd.rect.height*0.2f), messageWnd.title, mainPlayer.newskin.customStyles[4]);
		GUI.Label( new Rect(messageWnd.rect.width*0.05f, messageWnd.rect.height * 0.225f, messageWnd.rect.width*0.9f,messageWnd.rect.height*0.55f), messageWnd.text, mainPlayer.newskin.customStyles[6]);
		
		if(GUI.Button( new Rect(messageWnd.rect.width*0.4f, messageWnd.rect.height*0.87f, messageWnd.rect.width*0.2f, messageWnd.rect.height * 0.1f),"OK", mainPlayer.guildskin.button))
		{
			showType = 0;
		}
	}
	
	void  warnMessageWindow ()
	{
		GUI.DrawTexture( new Rect(0, 0, messageWnd.rect.width, messageWnd.rect.height), mainPlayer.Background);
		GUI.Label( new Rect(messageWnd.rect.width*0.05f,0,messageWnd.rect.width*0.9f,messageWnd.rect.height*0.2f), messageWnd.title, mainPlayer.newskin.customStyles[4]);
		GUI.Label( new Rect(messageWnd.rect.width*0.05f, messageWnd.rect.height * 0.225f, messageWnd.rect.width*0.9f,messageWnd.rect.height*0.5f), messageWnd.text, mainPlayer.newskin.customStyles[6]);
		
		if(GUI.Button( new Rect(messageWnd.rect.width*0.35f, messageWnd.rect.height*0.75f, messageWnd.rect.width*0.3f, messageWnd.rect.height * 0.1f),"OK", mainPlayer.guildskin.button))
		{
			ShowTakeSlots();
			showType = 3;
		}
		if(GUI.Button( new Rect(messageWnd.rect.width*0.35f, messageWnd.rect.height*0.87f, messageWnd.rect.width*0.3f, messageWnd.rect.height * 0.1f),"Cancel", mainPlayer.guildskin.button))
		{
			showType = 0;
		}
	}
	
	public void  sendMailResult ( MailResponseResult err )
	{
		switch(err)
		{
		case MailResponseResult.MAIL_OK:
			MailMessage("Mail successfully sent.");
			break;
		case MailResponseResult.MAIL_ERR_EQUIP_ERROR:
			MailMessage("You can't send that.");
			break;
		case MailResponseResult.MAIL_ERR_CANNOT_SEND_TO_SELF:
			MailMessage("Can't send to yourself.");
			break;
		case MailResponseResult.MAIL_ERR_NOT_ENOUGH_MONEY:
			MailMessage("You don't have enough gold.");
			break;
		case MailResponseResult.MAIL_ERR_RECIPIENT_NOT_FOUND:
			MailMessage("Character not found.");
			break;
		case MailResponseResult.MAIL_ERR_NOT_YOUR_TEAM:
			MailMessage("Character not found.");
			break;
		case MailResponseResult.MAIL_ERR_INTERNAL_ERROR:
			MailMessage("Server internal error.");
			break;
		case MailResponseResult.MAIL_ERR_DISABLED_FOR_TRIAL_ACC:
			MailMessage("Server internal error.");
			break;
		case MailResponseResult.MAIL_ERR_RECIPIENT_CAP_REACHED:
			MailMessage("Server internal error.");
			break;
		case MailResponseResult.MAIL_ERR_CANT_SEND_WRAPPED_COD:
			MailMessage("Server internal error.");
			break;
		case MailResponseResult.MAIL_ERR_MAIL_AND_CHAT_SUSPENDED:
			MailMessage("Server internal error.");
			break;
		case MailResponseResult.MAIL_ERR_TOO_MANY_ATTACHMENTS:
			MailMessage("There are to many items.");
			break;
		case MailResponseResult.MAIL_ERR_MAIL_ATTACHMENT_INVALID:
			MailMessage("You can't send that");
			break;
		case MailResponseResult.MAIL_ERR_ITEM_HAS_EXPIRED:
			MailMessage("Item has expired.");
			break;
		}
	}
	
	public void  moneyTakenResult ( MailResponseResult err )
	{
		if(err != MailResponseResult.MAIL_OK)
		{
			MailMessage("An error occurred.");
		}
		else
		{
			MailMessage("Successfully took gold.");
			mainPlayer.mailHandler.mailList[selected].money = 0;
		}
	}
	
	public void  EquipError ( uint err )
	{
		MailMessage(ItemFlags.InventoryErrorMessage(err));
	}
	
	public void  itemTakenResult ( MailResponseResult err )
	{
		if(err != MailResponseResult.MAIL_OK)
		{
			MailMessage("You don't have enough gold.");
		}
		else
		{
			mainPlayer.mailHandler.mailList[selected].items.RemoveAt(itemSelected);
			MailMessage("Successfully took item.");
		}
	}
	
	public void  returnedToSenderResult ( MailResponseResult err )
	{
		if(err != MailResponseResult.MAIL_OK)
		{
			MailMessage("An error occurred.");
		}
		else
		{
			MailMessage("Mail has been sent back.");
		}
	}
	
	public void  deletedResult ( MailResponseResult err )
	{
		if(err != MailResponseResult.MAIL_OK)
		{
			MailMessage("An error occurred.");
		}
		else
		{
			MailMessage("Mail successfully deleted.");
			mainPlayer.mailHandler.mailList.RemoveAt(selected);
		}
	}
	
	public void  madePermanentResult ( MailResponseResult err )
	{
		if(err != MailResponseResult.MAIL_OK)
		{
			MailMessage("An error occurred.");
		}
		else
		{
			MailMessage("Saved copy of the letter.");
		}
	}
	
	void  Disable ()
	{
		show = false;
		mainPlayer.blockTargetChange = false;
		WorldSession.interfaceManager.hideInterface = false;
		mainPlayer.interf.stateManager();
		if(mainPlayer.mailHandler.mailList != null)
		{
			mainPlayer.mailHandler.mailList.Clear();
		}
		windowToReturnTo = 0;
		tmpMail = null;
		receiver = "";
		subject = "";
		body = "";
		gold = "";
		cod = "";
		takeGold = false;
		if(codToggleButton!=null)
			codToggleButton.selected = false;
		items.Clear();
	}
	
	void  ShowSendSlots ()
	{
		sendContainerItems = new UIAbsoluteLayout();
		sendContainerDecoration = new UIAbsoluteLayout();
		string image;
		int vPos = 0;
		int hPos = 0;
		for(int i = 0; i < 12; i++)
		{
			image = "fundal.png";
			Item item = null;
			if(i < items.Count)
			{
				item  = items[i];
			}
			if(item != null && item.entry > 0)
			{
				image = DefaultIcons.GetItemIcon(item.entry);
			}
			
			if(vPos >= 6)
			{
				vPos = 0;
				hPos++;
			}
			UIToolkit tempButtonUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
			tempButton = UIButton.create( tempButtonUIToolkit, image, image, 0, 0);
			tempButton.info = i;
			tempButton.setSize(mainPlayer.inv.itemWidth, mainPlayer.inv.itemHeight);
			tempButton.position = new Vector3(((Screen.width * 0.1f) + (hPos * (mainPlayer.inv.itemWidth * 1.2f))),
			                              (sendVPos - (vPos * (mainPlayer.inv.itemHeight * 1.2f))),
			                              (-2));
			tempButton.onTouchDown += sendSlotDelegate;
			
			itemDecoration = UIPrime31.questMix.addSprite("icon_decoration.png",
			                                              (tempButton.position.x - (tempButton.width * 0.18f)),
			                                              (-tempButton.position.y),
			                                              (-3));
			itemDecoration.setSize((tempButton.width*1.2f), (tempButton.height*1.2f));
			
			sendContainerItems.addChild(tempButton);
			sendContainerDecoration.addChild(itemDecoration);
			vPos++;
		}
	}
	
	void  ShowTakeSlots ()
	{
		if(mainPlayer.mailHandler.mailList[selected].items.Count > 0)
		{
			string image = "noicon.png";
			Item item;
			int alignPos = 0;
			float itmWidth = mainPlayer.inv.itemWidth*0.929f;
			float itmHeight = mainPlayer.inv.itemHeight*1.01f;
			for(i = 0; i < mainPlayer.mailHandler.mailList[selected].items.Count; i++)
			{
				item  = mainPlayer.mailHandler.mailList[selected].items[i];
				
				if(item != null && item.entry > 0)
				{
					image = DefaultIcons.GetItemIcon(item.entry);
				}	
				UIToolkit tempButtonUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
				tempButton = UIButton.create( tempButtonUIToolkit, image, image, 0, 0);
				tempButton.setSize(itmWidth, itmHeight);
				tempButton.position = new Vector3(
					((mailDecoration.position.x+(mailDecoration.width*0.055f))+(itmWidth*(alignPos/2))),
					((mailDecoration.position.y-(mailDecoration.height*0.125f))-(itmHeight*(alignPos%2))),
					(-2));
				tempButton.onTouchDown += takeSlotDelegate;
				
				mailItemContainer.addChild(tempButton);
				alignPos++;
			}
		}
	}
	
	void  ClearSendSlots ()
	{
		UISprite temp;
		if(sendContainerItems != null)
		{
			while(sendContainerItems._children.Count > 0)
			{
				temp = sendContainerItems._children[0];
				UIToolkit tempUIToolkit = temp.manager;
				sendContainerItems.removeChild(temp, false);
				temp.destroy();
				if(tempUIToolkit)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
		}
		if(sendContainerDecoration != null)
		{
			while(sendContainerDecoration._children.Count > 0)
			{
				temp = sendContainerDecoration._children[0];
				sendContainerDecoration.removeChild(temp, false);
				temp.destroy();
			}
		}
	}
	
	void  itemDelegate ( UIButton sender )
	{
		if(items.Count > 11)
			return;
		if(mainPlayer.inv.currentBag == 0)
		{
			for(i = 0; i < items.Count; i++)
			{
				if(items[i].guid == mainPlayer.itemManager.BaseBagsSlots[sender.info].guid)
				{
					return;
				}
			}
			items.Add(mainPlayer.itemManager.BaseBagsSlots[sender.info]);
			mainPlayer.inv.itemContainer._children[sender.info].color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
			calculateFee();
			ClearSendSlots();
			ShowSendSlots();
		}
		else
		{
			for(i = 0; i < items.Count; i++)
			{
				if(items[i].guid == mainPlayer.itemManager.BaseBags[mainPlayer.inv.currentBag].GetItemFromSlot(sender.info).guid)
				{
					return;
				}
			}
			items.Add(mainPlayer.itemManager.BaseBags[mainPlayer.inv.currentBag].GetItemFromSlot(sender.info));
			mainPlayer.inv.itemContainer._children[sender.info].color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
			calculateFee();
			ClearSendSlots();
			ShowSendSlots();
		}
	}
	
	void  calculateFee ()
	{
		if(items.Count > 1)
			fee = items.Count * 30;
	}
	
	void  sendSlotDelegate ( UIButton sender )
	{
		items.Remove(items[sender.info]);
		calculateFee();
		ClearSendSlots();
		ShowSendSlots();
	}
	
	void  takeSlotDelegate ( UIButton sender )
	{
		itemSelected = sender.info;
		showType = 4;
	}
	
	void  takeAllItems ()
	{
		for(i = 0; i < mainPlayer.mailHandler.mailList[selected].items.Count; i++)
		{
			mainPlayer.mailHandler.SendMailTakeItem(selected, (uint)(ByteBuffer.Reverse(mainPlayer.mailHandler.mailList[selected].items[i].guid) >> 32));
		}
		mainPlayer.mailHandler.mailList[selected].items.Clear();
		ClearSendSlots();
		showType = 10;
	}
	
	void  NoMail ()
	{	
		showType = 12;
		
		DestroyCurrentWindow();
		
		mailBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.15f , Screen.height*0.35f , 5);
		mailBackground.setSize(Screen.width*0.6f , Screen.height*0.4f);
		
		mailDecoration = UIPrime31.questMix.addSprite("mail_decoration4.png" , mailBackground.position.x + mailBackground.width*0.04f , -mailBackground.position.y + mailBackground.height*0.05f , 4);
		mailDecoration.setSize(mailBackground.width*0.92f , mailBackground.height*0.7f);
		
		composeMail = UIButton.create(UIPrime31.questMix , "compose_button.png" , "compose_button.png" , mailBackground.position.x + mailBackground.width*0.1f , -mailBackground.position.y + mailBackground.height*0.75f , 4);
		composeMail.setSize(mailBackground.width*0.32f , mailBackground.height*0.18f);
		composeMail.onTouchDown += ComposeMailDelegate;
		
		exitMail = UIButton.create(UIPrime31.questMix , "exit_button.png" , "exit_button.png" , mailBackground.position.x + mailBackground.width*0.58f , -mailBackground.position.y + mailBackground.height*0.75f , 4);
		exitMail.setSize(mailBackground.width*0.32f , mailBackground.height*0.18f);
		exitMail.onTouchDown += ExitMailDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(mailBackground.height*0.2f , mailBackground.height*0.2f);
		closeXButton.position = new Vector3( mailBackground.position.x + mailBackground.width - closeXButton.width/1.5f , mailBackground.position.y + closeXButton.height/3 , 4);
		closeXButton.onTouchDown += ExitMailDelegate;
		
		mailContainer.addChild(mailBackground , mailDecoration , composeMail , exitMail , closeXButton);
		
		mailTexts[0] = mainPlayer.interf.text3.addTextInstance("You have no mail." , mailBackground.position.x + mailBackground.width * 0.5f , -mailBackground.position.y + mailBackground.height*0.35f , UID.TEXT_SIZE*0.9f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
	}
	
	void  ComposeMailDelegate ( UIButton sender )
	{
		showType = 2;
		windowNotActive = true;
	}
	
	void  ExitMailDelegate ( UIButton sender )
	{
		DestroyCurrentWindow();
		Disable();
		WorldSession.interfaceManager.hideInterface = false;
	}
	
	void  DestroyCurrentWindow ()
	{
		if(mailContainer != null)
		{
			mailContainer.removeAllChild(true);
		}
		if(mailItemContainer != null)
		{
			while(mailItemContainer._children.Count > 0)
			{
				UISprite temp = mailItemContainer._children[0];
				UIToolkit tempUIToolkit = temp.manager;
				mailItemContainer.removeChild(temp, false);
				temp.destroy();
				if(tempUIToolkit)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
		}
		
		for(int i = 0; i<10 ; i++)
		{
			if(mailTexts[i]!=null)
			{
				mailTexts[i].clear();
				mailTexts[i] = null;
			}
		}
		
		if(scrollableContainer!=null)
		{
			scrollableContainer.removeAllChild(true);
			scrollableContainer.setSize(0 , 0);
			scrollableContainer = null;
		}
		
		for(int i=0 ; i < 50 ; i++)
		{
			if(mailButtonText[i]!=null)
			{
				mailButtonText[i].clear();
				mailButtonText[i]=null;
			}
		}
		
		if(mailBodyText!=null)
		{
			mailBodyText.destroy();
			mailBodyText = null;
		}
	}
	
	void  CancelComposeDelegate ( UIButton sender )
	{
		DestroyCurrentWindow();
		receiver = "";
		subject = "";
		body = "";
		gold = "";
		cod = "";
		fee = 30;
		takeGold = false;
		if(codToggleButton!=null)
			codToggleButton.selected = false;
		items.Clear();
		showType = 0;
	}
	
	void  AttachItemDelegate ( UIButton sender )
	{
		DestroyCurrentWindow();
		canShowGUI = false;
		
		doneButton = UIButton.create(UIPrime31.myToolkit3 , "button_OK.png" , "button_OK.png" , Screen.width*0.3f , Screen.height*0.9f , -2);
		doneButton.setSize(Screen.width*0.2f , Screen.height*0.07f);
		doneButton.onTouchDown += DoneAddItemsDelegate;
		
		foreach(KeyValuePair<int, Bag> bag in mainPlayer.itemManager.BaseBags)
		{
			bag.Value.PopulateSlots(mainPlayer.itemManager);
		}
		
		mainPlayer.inv.isActive = true;
		mainPlayer.inv.ShowBags();
		mainPlayer.inv.ShowItems();
		ShowSendSlots();
	}
	
	void  DoneAddItemsDelegate ( UIButton sender )
	{
		mainPlayer.inv.currentBag = 0;
		mainPlayer.inv.ClearBags();
		mainPlayer.inv.ClearItems();
		mainPlayer.inv.isActive = false;
		ClearSendSlots();
		if(items.Count > 0)
			showType = 1;
		else
			showType = 2;
		sender.destroy();
		windowNotActive = true;
	}
	
	void  CODDelegate ( UIToggleButton sender  ,   bool val )
	{
		if(val)
		{
			sender.position = new Vector3(sender.position.x - sender.width*0.09f,
			                              sender.position.y + sender.height*0.42f,
			                              sender.position.z);
			sender.setSize(sender.width*1.12f , sender.height*1.43f);
			takeGold = true;
			mailTexts[6].clear();
			mailTexts[6].text = "Sell price:";
		}
		else
		{
			sender.position = new Vector3(mailBackground.position.x + mailBackground.width * 0.6f,
			                              mailBackground.position.y - mailBackground.height*0.69f,
			                              4);
			sender.setSize(mailBackground.width*0.05f , mailBackground.width*0.05f);
			mailTexts[6].clear();
			mailTexts[6].text = "Gold to send:";
			takeGold = false;
		}
	}
	
	void  MailOpenDelegate ( UIToggleButton sender  ,   bool val )
	{	
		selected = sender.info;
		showType = 10;
		mainPlayer.mailHandler.SendMailMarkAsRead(selected);
	}
	
	void  DeleteMailDelegate ( UIButton sender )
	{
		mainPlayer.mailHandler.SendMailDelete(selected);
	}
	
	void  CloseMessageDelegate ( UIButton sender )
	{
		DestroyMessageWindow();
		showType = windowToReturnTo;
		windowNotActive = true;
		windowToReturnTo = 0;
	}
	
	void  DestroyMessageWindow ()
	{
		mailMessageContainer.removeAllChild(true);
		if(mailTexts[8]!=null)
		{
			mailTexts[8].clear();
			mailTexts[8] = null;
		}
	}
	
	void  TakeGoldDelegate ( UIButton sender )
	{
		mainPlayer.mailHandler.SendMailTakeMoney(selected);
		windowToReturnTo = 10;
	}
	
	void  TakeItemsDelegate ( UIButton sender )
	{
		if(mainPlayer.mailHandler.mailList[selected].COD > 0)
		{
			windowToReturnTo = 3;
			MailMessage("You will pay " + mainPlayer.mailHandler.mailList[selected].COD + " gold for items.");
		}
		else
		{
			showType = 3;
		}
	}
	
	void  ReturnDelegate ( UIButton sender )
	{
		mainPlayer.mailHandler.SendReturnToSender(selected);
		ExitMailDelegate(sender);
	}
	
	void  TakeAllItemsDelegate ( UIButton sender )
	{
		takeAllItems();
		ClearSendSlots();
	}
	
	void  ExitItemsTakeDelegate ( UIButton sender )
	{
		showType = 10;
		ClearSendSlots();
	}
	
	void  TakeSelectedItemDelegate ( UIButton sender )
	{
		mainPlayer.mailHandler.SendMailTakeItem(selected, (uint)(ByteBuffer.Reverse(mainPlayer.mailHandler.mailList[selected].items[itemSelected].guid) >> 32));
		ClearSendSlots();
		isMailItemInfo = false;
		windowToReturnTo = 10;
	}
	
	void  ExitItemTakeDelegate ( UIButton sender )
	{
		isMailItemInfo = false;
		showType = 3;
	}
	
	void  SendMailDelegate ( UIButton sender )
	{
		DestroyCurrentWindow();
		if(receiver == "")
		{
			MailMessage("Please complete the receiver field!");
			
		}
		else if(subject == "")
		{
			if(subject.IndexOf(" ") == 0)
			{
				MailMessage("Subject field is wrong!");
			}
			else
			{
				MailMessage("Please complete the subject field!");
			}	
		}
		else
		{
			tmpMail = new NewMail();
			tmpMail.receiver = receiver;
			tmpMail.subject = subject;
			tmpMail.body = body;
			if(takeGold)
			{
				tmpMail.gold = 0;
				if (string.IsNullOrEmpty (cod))
				{
					tmpMail.COD = 0;
				}
				else
				{
					tmpMail.COD = uint.Parse (cod);
				}
			}
			else
			{
				if (string.IsNullOrEmpty (gold))
				{
					tmpMail.gold = 0;
				}
				else
				{
					tmpMail.gold = uint.Parse (gold);
				}
				tmpMail.COD = 0;
			}
			if(items.Count > 0)
			{
				for(i = 0; i < items.Count; i++)
				{
					tmpMail.items.Add(items[i]);
				}
			}
			tmpMail.itemCount = items.Count;
			mainPlayer.mailHandler.SendMail(tmpMail);
			//	windowToReturnTo = 2;
		}
	}
	
	void  MailMessage ( string mess )
	{	
		showType = 12;
		DestroyCurrentWindow();
		if(mess == "Mail successfully sent.")
		{
			windowToReturnTo = 0;
			tmpMail = null;
			receiver = "";
			subject = "";
			body = "";
			gold = "";
			cod = "";
			takeGold = false;
			if(codToggleButton!=null)
				codToggleButton.selected = false;
			items.Clear();
			//showType = 0;
		}	
		
		mailMessageBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.3f , -5);
		mailMessageBackground.setSize(Screen.width*0.55f , Screen.height*0.45f);
		
		mailMessageDecoration = UIPrime31.questMix.addSprite("ornament4.png" , mailMessageBackground.position.x + mailMessageBackground.width*0.03f , -mailMessageBackground.position.y + mailMessageBackground.height*0.22f , -6);
		mailMessageDecoration.setSize(mailMessageBackground.width*0.9f , mailMessageBackground.height*0.33f);
		
		closeMessageButton = UIButton.create(UIPrime31.myToolkit3 , "button_OK.png" , "button_OK.png" , mailMessageBackground.position.x + mailMessageBackground.width*0.3f , -mailMessageBackground.position.y + mailMessageBackground.height*0.72f , -6);
		closeMessageButton.setSize(mailMessageBackground.width*0.4f , mailMessageBackground.height*0.16f);
		closeMessageButton.onTouchDown += CloseMessageDelegate;
		
		closeMessageXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeMessageXButton.setSize(mailMessageBackground.width*0.1f , mailMessageBackground.width*0.1f);
		closeMessageXButton.position = new Vector3( mailMessageBackground.position.x + mailMessageBackground.width - closeMessageXButton.width/1.5f , mailMessageBackground.position.y + closeMessageXButton.height/5 , -6);
		closeMessageXButton.onTouchDown += CloseMessageDelegate;
		
		mailMessageContainer.addChild(mailMessageBackground , mailMessageDecoration , closeMessageButton , closeMessageXButton);
		
		if (mess.Length>20)
		{	
			int tempNum;
			tempNum = mess.IndexOf(" " , 19);
			if(tempNum!=-1)
				mailTexts[8] = mainPlayer.interf.text3.addTextInstance(mess.Substring(0 , tempNum)+"\n"+mess.Substring(tempNum+1 , mess.Length-tempNum-1) , mailMessageBackground.position.x + mailMessageBackground.width * 0.6f , -mailMessageBackground.position.y + mailMessageBackground.height*0.25f , UID.TEXT_SIZE*0.7f , -6 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
			else
				mailTexts[8] = mainPlayer.interf.text3.addTextInstance(mess , mailMessageBackground.position.x + mailMessageBackground.width * 0.6f , -mailMessageBackground.position.y + mailMessageBackground.height*0.25f , UID.TEXT_SIZE*0.7f , -6 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		}
		else mailTexts[8] = mainPlayer.interf.text3.addTextInstance(mess , mailMessageBackground.position.x + mailMessageBackground.width * 0.6f , -mailMessageBackground.position.y + mailMessageBackground.height*0.25f , UID.TEXT_SIZE*0.7f , -6 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
	}
	
	void  NewMailWindow ()
	{
		DestroyCurrentWindow();
		windowNotActive = false;
		canShowGUI = true;
		
		mailBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.15f , Screen.height*0.15f , 5);
		mailBackground.setSize(Screen.width*0.6f , Screen.height*0.8f);
		
		mailDecoration = UIPrime31.questMix.addSprite("mail_decoration7.png" , mailBackground.position.x + mailBackground.width*0.04f , -mailBackground.position.y + mailBackground.height*0.07f , 4);
		mailDecoration.setSize(mailBackground.width*0.92f , mailBackground.height*0.5f);
		
		nameTextCase = UIPrime31.questMix.addSprite("short_text_case.png" , mailBackground.position.x + mailBackground.width*0.45f , -mailBackground.position.y + mailBackground.height*0.21f , 4);
		nameTextCase.setSize(mailBackground.width*0.52f , mailBackground.height*0.1f);
		
		subjectTextCase = UIPrime31.questMix.addSprite("short_text_case.png" , mailBackground.position.x + mailBackground.width*0.45f , -mailBackground.position.y + mailBackground.height*0.33f , 4);
		subjectTextCase.setSize(mailBackground.width*0.52f , mailBackground.height*0.1f);
		
		bodyTextCase = UIPrime31.questMix.addSprite("big_textbox.png" , mailBackground.position.x + mailBackground.width*0.45f , -mailBackground.position.y + mailBackground.height*0.45f , 4);
		bodyTextCase.setSize(mailBackground.width*0.52f , mailBackground.height*0.25f);
		
		goldTextCase = UIPrime31.questMix.addSprite("short_text_case.png" , mailBackground.position.x + mailBackground.width*0.45f , -mailBackground.position.y + mailBackground.height*0.73f , 4);
		goldTextCase.setSize(mailBackground.width*0.52f , mailBackground.height*0.1f);
		
		cancelButton = UIButton.create(UIPrime31.myToolkit5 , "cancel.png" , "cancel.png" , mailBackground.position.x + mailBackground.width*0.04f , -mailBackground.position.y + mailBackground.height*0.87f , 4);
		cancelButton.setSize(mailBackground.width*0.25f , mailBackground.height*0.09f);
		cancelButton.onTouchDown += CancelComposeDelegate;
		
		sendButton = UIButton.create(UIPrime31.questMix , "send_button.png" , "send_button.png" , mailBackground.position.x + mailBackground.width*0.71f , -mailBackground.position.y + mailBackground.height*0.87f , 4);
		sendButton.setSize(mailBackground.width*0.25f , mailBackground.height*0.09f);
		sendButton.onTouchDown += SendMailDelegate;
		
		attachItemButton = UIButton.create(UIPrime31.questMix , "attach_item_button.png" , "attach_item_button.png" , mailBackground.position.x + mailBackground.width*0.3f , -mailBackground.position.y + mailBackground.height*0.87f , 4);
		attachItemButton.setSize(mailBackground.width*0.4f , mailBackground.height*0.09f);
		attachItemButton.onTouchDown += AttachItemDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(mailBackground.height*0.1f , mailBackground.height*0.1f);
		closeXButton.position = new Vector3( mailBackground.position.x + mailBackground.width - closeXButton.width/1.5f , mailBackground.position.y + closeXButton.height/5 , 4);
		closeXButton.onTouchDown += CancelComposeDelegate;
		
		mailTexts[0] = mainPlayer.interf.text2.addTextInstance("NEW MAIL" , mailBackground.position.x + mailBackground.width * 0.5f , -mailBackground.position.y + mailBackground.height*0.03f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		
		mailTexts[1] = mainPlayer.interf.text3.addTextInstance("Fee: 30 Gold" , mailBackground.position.x + mailBackground.width * 0.5f , -mailBackground.position.y + mailBackground.height*0.14f , UID.TEXT_SIZE*0.6f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		
		mailTexts[2] = mainPlayer.interf.text3.addTextInstance("To:" , mailBackground.position.x + mailBackground.width * 0.42f , -mailBackground.position.y + mailBackground.height*0.2f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Right , UITextVerticalAlignMode.Top);
		
		mailTexts[3] = mainPlayer.interf.text3.addTextInstance("Subject:" , mailBackground.position.x + mailBackground.width * 0.42f , -mailBackground.position.y + mailBackground.height*0.32f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Right , UITextVerticalAlignMode.Top);
		
		mailTexts[4] = mainPlayer.interf.text3.addTextInstance("Text:" , mailBackground.position.x + mailBackground.width * 0.42f , -mailBackground.position.y + mailBackground.height*0.45f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Right , UITextVerticalAlignMode.Top);
		
		mailTexts[5] = mainPlayer.interf.text3.addTextInstance("Gold \nto send:" , mailBackground.position.x + mailBackground.width * 0.42f , -mailBackground.position.y + mailBackground.height*0.65f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Right , UITextVerticalAlignMode.Top);
		
		mailContainer.addChild(mailBackground , mailDecoration , nameTextCase , subjectTextCase , bodyTextCase , goldTextCase , cancelButton , sendButton , attachItemButton , closeXButton);
		
		
	}
	
	void  NewMailWindowWithItems ()
	{
		DestroyCurrentWindow();
		windowNotActive = false;
		canShowGUI = true;
		
		mailBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.05f , Screen.height*0.15f , 5);
		mailBackground.setSize(Screen.width*0.75f , Screen.height*0.8f);
		
		mailDecoration = UIPrime31.questMix.addSprite("mail_decoration8.png" , mailBackground.position.x + mailBackground.width*0.45f , -mailBackground.position.y + mailBackground.height*0.09f , 4);
		mailDecoration.setSize(mailBackground.width*0.45f , mailBackground.height*0.05f);
		
		mailDecoration2 = UIPrime31.questMix.addSprite("mail_decoration3.png" , mailBackground.position.x + mailBackground.width*0.015f , -mailBackground.position.y + mailBackground.height*0.1f , 4);
		mailDecoration2.setSize(mailBackground.width*0.4f , mailBackground.height*0.7f);
		
		nameTextCase = UIPrime31.questMix.addSprite("short_text_case.png" , mailBackground.position.x + mailBackground.width*0.6f , -mailBackground.position.y + mailBackground.height*0.21f , 4);
		nameTextCase.setSize(mailBackground.width*0.38f , mailBackground.height*0.1f);
		
		subjectTextCase = UIPrime31.questMix.addSprite("short_text_case.png" , mailBackground.position.x + mailBackground.width*0.5f , -mailBackground.position.y + mailBackground.height*0.32f , 4);
		subjectTextCase.setSize(mailBackground.width*0.48f , mailBackground.height*0.1f);
		
		bodyTextCase = UIPrime31.questMix.addSprite("big_textbox.png" , mailBackground.position.x + mailBackground.width*0.5f , -mailBackground.position.y + mailBackground.height*0.43f , 4);
		bodyTextCase.setSize(mailBackground.width*0.48f , mailBackground.height*0.25f);
		
		goldTextCase = UIPrime31.questMix.addSprite("short_text_case.png" , mailBackground.position.x + mailBackground.width*0.6f , -mailBackground.position.y + mailBackground.height*0.77f , 4);
		goldTextCase.setSize(mailBackground.width*0.38f , mailBackground.height*0.1f);
		
		cancelButton = UIButton.create(UIPrime31.myToolkit5 , "cancel.png" , "cancel.png" , mailBackground.position.x + mailBackground.width*0.04f , -mailBackground.position.y + mailBackground.height*0.88f , 4);
		cancelButton.setSize(mailBackground.width*0.2f , mailBackground.height*0.09f);
		cancelButton.onTouchDown += CancelComposeDelegate;
		
		sendButton = UIButton.create(UIPrime31.questMix , "send_button.png" , "send_button.png" , mailBackground.position.x + mailBackground.width*0.71f , -mailBackground.position.y + mailBackground.height*0.88f , 4);
		sendButton.setSize(mailBackground.width*0.2f , mailBackground.height*0.09f);
		sendButton.onTouchDown += SendMailDelegate;
		
		attachItemButton = UIButton.create(UIPrime31.questMix , "attach_item_button.png" , "attach_item_button.png" , mailBackground.position.x + mailBackground.width*0.3f , -mailBackground.position.y + mailBackground.height*0.88f , 4);
		attachItemButton.setSize(mailBackground.width*0.32f , mailBackground.height*0.09f);
		attachItemButton.onTouchDown += AttachItemDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(mailBackground.height*0.1f , mailBackground.height*0.1f);
		closeXButton.position = new Vector3( mailBackground.position.x + mailBackground.width - closeXButton.width/1.5f , mailBackground.position.y + closeXButton.height/5 , 4);
		closeXButton.onTouchDown += CancelComposeDelegate;
		
		codToggleButton = UIToggleButton.create(UIPrime31.myToolkit4 , "box.png" , "box_checked.png" , "box.png" , mailBackground.position.x + mailBackground.width * 0.6f , -mailBackground.position.y + mailBackground.height*0.69f , 4);
		codToggleButton.setSize(mailBackground.width*0.05f , mailBackground.width*0.05f);
		codToggleButton.onToggle += CODDelegate;
		
		mailTexts[0] = mainPlayer.interf.text2.addTextInstance("NEW MAIL" , mailBackground.position.x + mailBackground.width * 0.65f , -mailBackground.position.y + mailBackground.height*0.03f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		
		mailTexts[1] = mainPlayer.interf.text3.addTextInstance("Fee: "+fee+" Gold" , mailBackground.position.x + mailBackground.width * 0.65f , -mailBackground.position.y + mailBackground.height*0.14f , UID.TEXT_SIZE*0.6f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		
		mailTexts[2] = mainPlayer.interf.text3.addTextInstance("To:" , mailBackground.position.x + mailBackground.width * 0.58f , -mailBackground.position.y + mailBackground.height*0.21f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Right , UITextVerticalAlignMode.Top);
		
		mailTexts[5] = mainPlayer.interf.text3.addTextInstance("C.O.D." , mailBackground.position.x + mailBackground.width * 0.58f , -mailBackground.position.y + mailBackground.height*0.69f , UID.TEXT_SIZE*0.7f , 4 , Color.white , UITextAlignMode.Right , UITextVerticalAlignMode.Top);
		
		mailTexts[6] = mainPlayer.interf.text3.addTextInstance("Gold to send:" , mailBackground.position.x + mailBackground.width * 0.58f , -mailBackground.position.y + mailBackground.height*0.77f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Right , UITextVerticalAlignMode.Top);
		
		if(items.Count > 0)
		{
			string image = "noicon.png";
			Item item;
			int alignPos = 0;
			float itmWidth = mainPlayer.inv.itemWidth*0.95f;
			float itmHeight = mainPlayer.inv.itemWidth*0.86f;
			for(i = 0; i < items.Count; i++)
			{
				item  = items[i];
				
				if(item != null && item.entry > 0)
				{
					image = DefaultIcons.GetItemIcon(item.entry);
				}	
				
				UIToolkit tempButtonUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
				tempButton = UIButton.create( tempButtonUIToolkit, image, image, 0, 0);
				tempButton.setSize(itmWidth, itmHeight);
				tempButton.position = new Vector3(mailBackground.position.x + mailBackground.width*0.05f + (alignPos%3) * itmWidth*1.3f,
				                                  mailBackground.position.y - mailBackground.height*0.05f - itmHeight*1.25f*(alignPos/3),
				                                  -2);
				tempButton.disabled = true;
				
				itemDecoration = UIPrime31.questMix.addSprite("icon_decoration.png",
				                                              tempButton.position.x - tempButton.width*0.18f,
				                                              -tempButton.position.y,
				                                              -3);
				itemDecoration.setSize(tempButton.width*1.2f, tempButton.height*1.2f);
				
				mailItemContainer.addChild(tempButton);
				mailContainer.addChild(itemDecoration);
				alignPos++;
			}
		}
		mailContainer.addChild(mailBackground, mailDecoration, mailDecoration2, nameTextCase, subjectTextCase);
		mailContainer.addChild(bodyTextCase, goldTextCase, cancelButton, sendButton, attachItemButton, closeXButton);
		mailContainer.addChild(codToggleButton);
	}
	
	void  ShowMailList ()
	{	
		DestroyCurrentWindow();
		
		mailBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.05f , Screen.height*0.15f , 5);
		mailBackground.setSize(Screen.width*0.75f , Screen.height*0.8f);
		
		mailDecoration = UIPrime31.questMix.addSprite("background_case.png" , mailBackground.position.x + mailBackground.width*0.03f , -mailBackground.position.y + mailBackground.height*0.15f , 4);
		mailDecoration.setSize(mailBackground.width*0.94f , mailBackground.height*0.7f);
		
		mailDecoration2 = UIPrime31.questMix.addSprite("mail_decoration5.png" , mailBackground.position.x + mailBackground.width*0.06f , -mailBackground.position.y + mailBackground.height*0.03f , 3);
		mailDecoration2.setSize(mailBackground.width*0.4f , mailBackground.height*0.115f);
		
		composeMail = UIButton.create(UIPrime31.questMix , "compose_button.png" , "compose_button.png" , mailBackground.position.x + mailBackground.width*0.6f , -mailBackground.position.y + mailBackground.height*0.87f , 4);
		composeMail.setSize(mailBackground.width*0.25f , mailBackground.height*0.09f);
		composeMail.onTouchDown += ComposeMailDelegate;
		
		closeButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , mailBackground.position.x + mailBackground.width*0.15f , -mailBackground.position.y + mailBackground.height*0.87f , 4);
		closeButton.setSize(mailBackground.width*0.25f , mailBackground.height*0.09f);
		closeButton.onTouchDown += ExitMailDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(mailBackground.height*0.1f , mailBackground.height*0.1f);
		closeXButton.position = new Vector3( mailBackground.position.x + mailBackground.width - closeXButton.width/1.5f , mailBackground.position.y + closeXButton.height/5 , 4);
		closeXButton.onTouchDown += ExitMailDelegate;
		
		scrollableContainer = new UIScrollableVerticalLayout( (int)(mailDecoration.height*0.02f) );
		scrollableContainer.position = new Vector3( mailDecoration.position.x + mailDecoration.width*0.014f , mailDecoration.position.y - mailDecoration.height*0.1f , 0 );
		scrollableContainer.setSize(mailDecoration.width , mailDecoration.height * 0.8f);
		scrollableContainer.marginModify = true;
		
		for(int i= 0; i < mainPlayer.mailHandler.mailList.Count; i++)
		{
			mailButtons[i] = UIToggleButton.create(UIPrime31.interfMix , "selected_mail_frame.png" , "selected_mail_frame.png" , "selected_mail_frame.png" , 0 , 0 , 6);
			mailButtons[i].setSize(mailDecoration.width*0.97f , mailDecoration.height*0.125f);
			mailButtons[i].info = i;
			mailButtons[i].onToggle += MailOpenDelegate;
			scrollableContainer.addChild(mailButtons[i]);
			
			mailButtonText[i] = mainPlayer.interf.text1.addTextInstance((mainPlayer.mailHandler.mailList[i].subject.Length>39)? mainPlayer.mailHandler.mailList[i].subject.Substring(0, 38) : mainPlayer.mailHandler.mailList[i].subject, mailDecoration.position.x + mailDecoration.width*0.05f , 0 , UID.TEXT_SIZE*0.7f , 1 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Middle);
		}
		
		mailContainer.addChild(mailBackground , mailDecoration , mailDecoration2 , composeMail , closeButton , closeXButton);
		
		mailTexts[0] = mainPlayer.interf.text2.addTextInstance("MAIL BOX" , mailBackground.position.x + mailBackground.width * 0.15f , -mailBackground.position.y + mailBackground.height*0.02f , UID.TEXT_SIZE*0.9f , 4 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		showType = 11;
	}
	
	void  OpenMail ( Letter theMail )
	{	
		showType = 12;
		DestroyCurrentWindow();
		
		mailBackground = UIPrime31.myToolkit4.addSprite("popup_background.png",
		                                                Screen.width*0.1f,
		                                                (mainPlayer.mailHandler.mailList[selected].money > 0 || mainPlayer.mailHandler.mailList[selected].items.Count > 0)? Screen.height*0.12f : Screen.height*0.15f,
		                                                5);
		mailBackground.setSize(Screen.width*0.7f , Screen.height*0.8f);
		
		mailDecoration = UIPrime31.questMix.addSprite("mail_decoration.png" , mailBackground.position.x + mailBackground.width*0.02f , -mailBackground.position.y + mailBackground.height*0.035f , 4);
		mailDecoration.setSize(mailBackground.width*0.95f , mailBackground.height*0.6f);
		
		bodyTextCase = UIPrime31.questMix.addSprite("background_case.png" , mailBackground.position.x + mailBackground.width*0.03f , -mailBackground.position.y + mailBackground.height*0.15f , 4);
		bodyTextCase.setSize(mailBackground.width*0.68f , mailBackground.height*0.8f);
		
		closeButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , mailBackground.position.x + mailBackground.width*0.72f , -mailBackground.position.y + mailBackground.height*0.82f , 4);
		closeButton.setSize(mailBackground.width*0.27f , mailBackground.height*0.1f);
		closeButton.onTouchDown += CancelComposeDelegate;
		
		deleteMailButton = UIButton.create(UIPrime31.myToolkit5 , "delete.png" , "delete.png" , mailBackground.position.x + mailBackground.width*0.72f , -mailBackground.position.y + mailBackground.height*0.69f , 4);
		deleteMailButton.setSize(mailBackground.width*0.27f , mailBackground.height*0.1f);
		deleteMailButton.onTouchDown += DeleteMailDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(mailBackground.height*0.1f , mailBackground.height*0.1f);
		closeXButton.position = new Vector3( mailBackground.position.x + mailBackground.width - closeXButton.width/1.5f , mailBackground.position.y + closeXButton.height/5 , 4);
		closeXButton.onTouchDown += CancelComposeDelegate;
		
		mailTexts[0] = mainPlayer.interf.text3.addTextInstance("From:" , mailBackground.position.x + mailBackground.width * 0.1f , -mailBackground.position.y + mailBackground.height*0.03f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		mailTexts[1] = mainPlayer.interf.text2.addTextInstance(theMail.senderName , mailBackground.position.x + mailBackground.width * 0.25f , -mailBackground.position.y + mailBackground.height*0.03f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		scrollableContainer = new UIScrollableVerticalLayout( bodyTextCase.height*0.02f );
		scrollableContainer.position = new Vector3( bodyTextCase.position.x + bodyTextCase.width*0.014f , bodyTextCase.position.y - bodyTextCase.height*0.05f , 0 );
		scrollableContainer.setSize(bodyTextCase.width , bodyTextCase.height * 0.8f );
		scrollableContainer.marginModify = true;
		
		mailBodyText = new NonUIElements.TextBox("" , new Vector3(bodyTextCase.position.x + bodyTextCase.width*0.03f, -bodyTextCase.position.y - bodyTextCase.height*0.03f , 3), new Vector2(0 , 0) , 5 ,Color.white, UID.TEXT_SIZE*0.7f ,"fontiny dark");
		mailBodyText.size = new Vector2((mainPlayer.mailHandler.mailList[selected].items.Count > 0)? bodyTextCase.width * 0.85f : bodyTextCase.width * 0.95f,
		                                bodyTextCase.height);
		mailBodyText.text = ("\n"+ theMail.body);
		string tempText= mailBodyText.text.Substring(2 , mailBodyText.text.Length-2);
		int tempNum;
		mailBodyText.destroy();
		for(int i= 0 ; i<20 ; i++)
		{
			if (!string.IsNullOrEmpty (tempText))
			{
				if (tempText.Length > 1)
				{
					tempNum = tempText.IndexOf ("\n", 0);
					if (tempNum != -1)
					{
						mailButtonText [i] = mainPlayer.interf.text1.addTextInstance (tempText.Substring (0, tempNum), bodyTextCase.position.x + bodyTextCase.width * 0.03f, 0, UID.TEXT_SIZE * 0.7f, 3, Color.white, UITextAlignMode.Left, UITextVerticalAlignMode.Top);			
						tempText = tempText.Substring (tempNum + 1, tempText.Length - tempNum - 1);

						mailButtons [i] = UIToggleButton.create (UIPrime31.interfMix, "selected_mail_frame.png", "selected_mail_frame.png", "selected_mail_frame.png", 0, 0, 6);
						mailButtons [i].setSize (mailDecoration.width * 0.01f, mailDecoration.height * 0.09f);
						scrollableContainer.addChild (mailButtons [i]);
					}
					else
					{
						mailButtonText [i] = mainPlayer.interf.text1.addTextInstance (tempText.Substring (0, tempText.Length - 1), bodyTextCase.position.x + bodyTextCase.width * 0.03f, 0, UID.TEXT_SIZE * 0.7f, 3, Color.white, UITextAlignMode.Left, UITextVerticalAlignMode.Top);
						tempText = null;
						mailButtons [i] = UIToggleButton.create (UIPrime31.interfMix, "selected_mail_frame.png", "selected_mail_frame.png", "selected_mail_frame.png", 0, 0, 6);
						mailButtons [i].setSize (mailDecoration.width * 0.01f, mailDecoration.height * 0.09f);
						scrollableContainer.addChild (mailButtons [i]);
					}
				}
				else
				{
					mailButtonText [i] = mainPlayer.interf.text1.addTextInstance (tempText, bodyTextCase.position.x + bodyTextCase.width * 0.03f, 0, UID.TEXT_SIZE * 0.7f, 3, Color.white, UITextAlignMode.Left, UITextVerticalAlignMode.Top);
					tempText = null;
					mailButtons [i] = UIToggleButton.create (UIPrime31.interfMix, "selected_mail_frame.png", "selected_mail_frame.png", "selected_mail_frame.png", 0, 0, 6);
					mailButtons [i].setSize (mailDecoration.width * 0.01f, mailDecoration.height * 0.09f);
					scrollableContainer.addChild (mailButtons [i]);
				}
			}
		}
		
		mailContainer.addChild(mailBackground , mailDecoration , bodyTextCase , closeButton , deleteMailButton , closeXButton);
		
		if(mainPlayer.mailHandler.mailList[selected].money > 0)
		{	
			deleteMailButton.position += Vector3.up * mailBackground.height*0.05f;
			closeButton.position += Vector3.up * mailBackground.height*0.05f;
			
			mailBackground.setSize(Screen.width*0.7f , Screen.height*0.87f);
			
			mailTexts[2] = mainPlayer.interf.text3.addTextInstance("Gold received: " + mainPlayer.mailHandler.mailList[selected].money.ToString(), mailBackground.position.x + mailBackground.width * 0.4f , -mailBackground.position.y + mailBackground.height*0.93f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
			
			takeGoldButton = UIButton.create(UIPrime31.interfMix , "take_gold.png" , "take_gold.png" , mailBackground.position.x + mailBackground.width*0.72f , -mailBackground.position.y + mailBackground.height*0.83f , 4);
			takeGoldButton.setSize(closeButton.width , closeButton.height*0.95f);
			takeGoldButton.onTouchDown += TakeGoldDelegate;
			mailContainer.addChild(takeGoldButton);
		}
		
		if(mainPlayer.mailHandler.mailList[selected].items.Count > 0)
		{
			bodyTextCase.setSize(mailBackground.width*0.6f , bodyTextCase.height);
			bodyTextCase.position += Vector3.right * mailBackground.width*0.11f;
			scrollableContainer.position += Vector3.right * mailBackground.width*0.11f;
			scrollableContainer.setSize(bodyTextCase.width , bodyTextCase.height * 0.8f );
			for(i = 0 ; i<20 ; i++)
				if(mailButtonText[i]!=null)
					mailButtonText[i].position += Vector3.right * mailBackground.width*0.11f;
			
			mailTexts[0].position += Vector3.right * mailBackground.width*0.1f;
			mailTexts[1].position += Vector3.right * mailBackground.width*0.1f;
			
			deleteMailButton.setSize(mailBackground.width*0.23f , mailBackground.height*0.09f);
			deleteMailButton.position += Vector3.right * mailBackground.height*0.04f;
			
			closeButton.setSize(mailBackground.width*0.23f , mailBackground.height*0.09f);
			closeButton.position += Vector3.right * mailBackground.height*0.04f;
			
			mailDecoration.hidden = true;
			mailDecoration = UIPrime31.questMix.addSprite("mail_decoration2.png" , mailBackground.position.x + mailBackground.width*0.145f , -mailBackground.position.y + mailBackground.height*0.045f , 4);
			mailDecoration.setSize(mailBackground.width*0.83f , mailBackground.height*0.25f);
			
			mailBackground.setSize(Screen.width*0.79f , Screen.height*0.87f);
			mailBackground.position -= Vector3.right * Screen.width*0.09f;
			
			if(mainPlayer.mailHandler.mailList[selected].money > 0)
			{
				takeGoldButton.setSize(closeButton.width , closeButton.height*0.95f);
				takeGoldButton.position = new Vector3(deleteMailButton.position.x,
				                                  takeGoldButton.position.y,
				                                  takeGoldButton.position.z);
			}
			
			takeItemButton = UIButton.create(UIPrime31.questMix , "take_item_button.png" , "take_item_button.png" , deleteMailButton.position.x , -mailBackground.position.y + mailBackground.height*0.35f , 4);
			takeItemButton.setSize(closeButton.width , deleteMailButton.height*0.95f);
			takeItemButton.onTouchDown += TakeItemsDelegate;
			
			returnButton = UIButton.create(UIPrime31.questMix , "return_button.png" , "return_button.png" , deleteMailButton.position.x , -mailBackground.position.y + mailBackground.height*0.47f , 4);
			returnButton.setSize(closeButton.width , deleteMailButton.height*0.95f);
			returnButton.onTouchDown += ReturnDelegate;
			
			mailContainer.addChild(mailDecoration , takeItemButton , returnButton);
			
			string image = "noicon.png";
			Item item;
			int alignPos = 0;
			float itmWidth = mainPlayer.inv.itemWidth*0.8f;
			float itmHeight = mainPlayer.inv.itemWidth*0.75f;
			for(i = 0; i < mainPlayer.mailHandler.mailList[selected].items.Count; i++)
			{
				item  = mainPlayer.mailHandler.mailList[selected].items[i];
				
				if(item != null && item.entry > 0)
				{
					image = DefaultIcons.GetItemIcon(item.entry);
				}	
				
				UIToolkit tempButtonUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
				tempButton = UIButton.create( tempButtonUIToolkit, image, image, 0, 0);
				tempButton.setSize(itmWidth, itmHeight);
				tempButton.position = new Vector3(mailBackground.position.x + mailBackground.width*0.04f + (alignPos%2) * itmWidth*1.25f, mailBackground.position.y - mailBackground.height*0.05f - itmHeight*1.22f*(alignPos/2), -2);
				tempButton.disabled = true;
				
				itemDecoration = UIPrime31.questMix.addSprite("icon_decoration.png" , tempButton.position.x - tempButton.width*0.18f , -tempButton.position.y , -3);
				itemDecoration.setSize(tempButton.width*1.2f , tempButton.height*1.2f);
				
				mailItemContainer.addChild(tempButton);
				mailContainer.addChild(itemDecoration);
				alignPos++;
			}
			
			if(mainPlayer.mailHandler.mailList[selected].COD > 0)
			{
				goldTextCase = UIPrime31.questMix.addSprite("short_text_case.png" , mailBackground.position.x + mailBackground.width*0.65f , -mailBackground.position.y + mailBackground.height*0.88f , 4);
				goldTextCase.setSize(mailBackground.width*0.3f , mailBackground.height*0.09f);
				mailContainer.addChild(goldTextCase);
				
				mailTexts[3] = mainPlayer.interf.text3.addTextInstance("Payment for items:" , mailBackground.position.x + mailBackground.width * 0.44f , -goldTextCase.position.y + goldTextCase.height*0.6f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
				mailTexts[4] = mainPlayer.interf.text3.addTextInstance(mainPlayer.mailHandler.mailList[selected].COD.ToString() , goldTextCase.position.x + goldTextCase.width * 0.1f , -goldTextCase.position.y + goldTextCase.height*0.6f , UID.TEXT_SIZE*0.7f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Middle);
				
			}
		}
		showType = 13;
	}
	
	void  TakeItemsWindow ()
	{
		showType = 12;
		DestroyCurrentWindow();
		
		mailBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.1f , Screen.height*0.25f , 5);
		mailBackground.setSize(Screen.width*0.7f , Screen.height*0.5f);
		
		takeItemButton = UIButton.create(UIPrime31.interfMix , "take_all.png" , "take_all.png" , mailBackground.position.x + mailBackground.width*0.73f , -mailBackground.position.y + mailBackground.height*0.27f , 4);
		takeItemButton.setSize(mailBackground.width*0.25f , mailBackground.height*0.14f);
		takeItemButton.onTouchDown += TakeAllItemsDelegate;
		
		doneButton = UIButton.create(UIPrime31.questMix , "exit_button.png" , "exit_button.png" , mailBackground.position.x + mailBackground.width*0.73f , -mailBackground.position.y + mailBackground.height*0.55f , 4);
		doneButton.setSize(mailBackground.width*0.25f , mailBackground.height*0.14f);
		doneButton.onTouchDown += ExitItemsTakeDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(mailBackground.height*0.15f , mailBackground.height*0.15f);
		closeXButton.position = new Vector3( mailBackground.position.x + mailBackground.width - closeXButton.width/1.5f , mailBackground.position.y + closeXButton.height/5 , 4);
		closeXButton.onTouchDown += ExitItemsTakeDelegate;
		
		mailDecoration = UIPrime31.interfMix.addSprite("items_container.png" , mailBackground.position.x + mailBackground.width*0.03f , -mailBackground.position.y + mailBackground.height*0.17f , 4);
		mailDecoration.setSize(mailBackground.width*0.68f , mailBackground.height*0.6f);
		
		mailContainer.addChild(mailBackground , takeItemButton , doneButton, closeXButton, mailDecoration);
		
		mailTexts[0] = mainPlayer.interf.text3.addTextInstance((mainPlayer.mailHandler.mailList[selected].subject.Length>39) ? mainPlayer.mailHandler.mailList[selected].subject.Substring(0, 38) : mainPlayer.mailHandler.mailList[selected].subject, mailBackground.position.x + mailBackground.width * 0.5f , -mailBackground.position.y + mailBackground.height*0.1f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		
		ShowTakeSlots();
		
		if(mainPlayer.mailHandler.mailList[selected].COD > 0)
		{
			goldTextCase = UIPrime31.questMix.addSprite("short_text_case.png", mailBackground.position.x + mailBackground.width*0.6f , -mailBackground.position.y + mailBackground.height*0.8f , 4);
			goldTextCase.setSize(mailBackground.width*0.35f , mailBackground.height*0.14f);
			mailContainer.addChild(goldTextCase);
			
			mailTexts[1] = mainPlayer.interf.text3.addTextInstance("Payment for items:" , mailBackground.position.x + mailBackground.width * 0.35f , -goldTextCase.position.y + goldTextCase.height*0.6f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
			mailTexts[2] = mainPlayer.interf.text1.addTextInstance(mainPlayer.mailHandler.mailList[selected].COD.ToString() , goldTextCase.position.x + goldTextCase.width * 0.1f , -goldTextCase.position.y + goldTextCase.height*0.6f , UID.TEXT_SIZE*0.7f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Middle);
		}
	}
	
	void  ShowTakeItemInfo ()
	{
		showType = 12;
		DestroyCurrentWindow();
		
		mailBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.1f , Screen.height*0.2f , 1);
		mailBackground.setSize(Screen.width*0.7f , Screen.height*0.6f);
		
		mailDecoration = UIPrime31.questMix.addSprite("mail_decoration.png" , mailBackground.position.x + mailBackground.width*0.05f , -mailBackground.position.y + mailBackground.height*0.05f , 2);
		mailDecoration.setSize(mailBackground.width*0.9f , mailBackground.height*0.75f);
		
		takeItemButton = UIButton.create(UIPrime31.questMix , "take_item_button.png" , "take_item_button.png" , mailBackground.position.x + mailBackground.width*0.66f , -mailBackground.position.y + mailBackground.height*0.82f , 2);
		takeItemButton.setSize(mailBackground.width*0.28f , mailBackground.height*0.13f);
		takeItemButton.onTouchDown += TakeSelectedItemDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 2);
		closeXButton.setSize(mailBackground.height*0.13f , mailBackground.height*0.13f);
		closeXButton.position = new Vector3( mailBackground.position.x + mailBackground.width - closeXButton.width/1.5f , mailBackground.position.y + closeXButton.height/5 , 2);
		closeXButton.onTouchDown += ExitItemTakeDelegate;
		
		mailContainer.addChild(mailBackground , mailDecoration , takeItemButton , closeXButton);
		
		/*
		FIXME_VAR_TYPE tempPos= mainPlayer.mailHandler.mailList[selected].items[itemSelected].stats.IndexOf("\n" , 0);
		FIXME_VAR_TYPE tempText= mainPlayer.mailHandler.mailList[selected].items[itemSelected].stats.Substring(0 , tempPos);
		mailTexts[0] = mainPlayer.interf.text2.addTextInstance(tempText , mailBackground.position.x + mailBackground.width * 0.45f , -mailBackground.position.y + mailBackground.height*0.03f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		tempText = mainPlayer.mailHandler.mailList[selected].items[itemSelected].stats.Substring(tempPos+1 , mainPlayer.mailHandler.mailList[selected].items[itemSelected].stats.Length - tempPos - 1);
		mailTexts[1] = mainPlayer.interf.text3.addTextInstance(tempText , mailBackground.position.x + mailBackground.width * 0.45f , -mailBackground.position.y + mailBackground.height*0.2f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		*/
		isMailItemInfo = true;
	}
	
	public void  UniversalExitMail ()
	{
		show = false;
		showType = 12;
		DestroyCurrentWindow();
		DestroyMessageWindow();
		ClearSendSlots();
		Disable();
	}
	
	Vector2 scrollViewItemVector;	
	
	public void  DrawItemInfo ()
	{
		
		Rect ItemNameRect = new Rect(Screen.width * 0.15f, Screen.height * 0.22f, Screen.width * 0.5f, Screen.height * 0.08f);
		Rect DescriptionRect = new Rect(Screen.width * 0.15f, Screen.height * 0.32f, Screen.width * 0.5f, Screen.height * 0.4f);
		
		int tempPos= mainPlayer.mailHandler.mailList[selected].items[itemSelected].stats.IndexOf("\n" , 0);
		string tempText= mainPlayer.mailHandler.mailList[selected].items[itemSelected].stats.Substring(0 , tempPos);
		
		GUI.Label(ItemNameRect, tempText, mainPlayer.ItemSkin.customStyles[2]);
		
		tempText = mainPlayer.mailHandler.mailList[selected].items[itemSelected].stats.Substring(tempPos+1 , mainPlayer.mailHandler.mailList[selected].items[itemSelected].stats.Length - tempPos - 1);
		
		GUI.Label(DescriptionRect, tempText, mainPlayer.ItemSkin.customStyles[2]);
		
		
		/*
		GUILayout.BeginArea(ItemNameRect);
			itemDescVector = GUILayout.BeginScrollView(itemDescVector);
				GUILayout.Label(mainPlayer.mailHandler.mailList[selected].items[itemSelected].stats, ItemSkin.customStyles[2]);
			GUILayout.EndScrollView();	
		GUILayout.EndArea();
		*/
	}
}