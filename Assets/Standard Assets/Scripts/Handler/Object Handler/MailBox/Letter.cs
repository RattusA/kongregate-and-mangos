﻿using System.Collections.Generic;

public class Letter
{
	public ushort messageSize;
	public uint messageId;
	public MailMessageType messageType;
	public uint sender;
	public string senderName;
	public ulong receiverGuid;
	public string subject;
	public string body;
	public List<Item> items;
	public float expireTime;
	public uint deliverTime;
	public uint money;
	public uint COD;
	public uint Flags;
	public Letter ()
	{
		senderName = "";
		items = new List<Item>();
	}
}
