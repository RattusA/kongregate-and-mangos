using UnityEngine;
using System;
using System.Collections;

public enum DeathState
{
	ALIVE          = 0,			// show as alive
	JUST_DIED      = 1,			// temporary state at die, for creature auto converted to CORPSE, for player at next update call
	CORPSE         = 2,			// corpse state, for player this also meaning that player not leave corpse
	DEAD           = 3,			// for creature despawned state (corpse despawned), for player CORPSE/DEAD not clear way switches (FIXME), and use m_deathtimer > 0 check for real corpse state
	JUST_ALIVED    = 4,			// temporary state at resurrection, for creature auto converted to ALIVE, for player at next update call
	CORPSE_FALLING = 5 			// corpse state in case when corpse still falling to ground
};

public class Unit:BaseObject, IBaseObject
{
	//Bones
	public Transform leftHand = null;
	public Transform rightHand = null;
	public Transform upperBody = null;
	public Transform lowerBody = null;
	public Transform legs = null;
	//Handlers
	public AnimationHandler aHandler = null;

	//Stats
	protected TextMesh textName = null;

	public Gender gender;
	public string Name = "";
	public bool isMainPlayer = false;

	public int HP = 1;
	public int MaxHP = 1;
	public int Power;
	public int MaxPower;

	public int OldHP;
	public int OldMaxHP;
	public int OldPower;
	public int OldMaxPower;

	public uint armour = 0;
	public uint playerFlag = 0;
	public DeathState m_deathState;	
	
	//target info
	public BaseObject target = null;
	public BaseObject newTarget = null;
	
	//positions	
	public Vector3 pos;
	public Vector3 way;
	
	//actions
	public bool isSwimming = false;
	public bool isAttacking = false;
	public bool isUnderWater = false;	
	public bool isMoving = true;
	public bool isRooted = false;
	public MovementFlags moveFlag;
	
	//unity objects
	static GameObject mCamera;

	public Rigidbody parentRB;
	public byte enemyStatus = 0;
		
	public virtual void Start()
	{
		base.Start();
		UpdateStats();

		try
		{
			parentRB = TransformParent.gameObject.GetComponentInChildren<Rigidbody>();
		}
		catch(Exception exception)
		{
			Debug.LogException(exception);
		}

		leftHand = transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L ForeArm/Bip01 L Hand/LeftHandDummy");
		if(leftHand == null)
		{
			leftHand = transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Neck/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L Forearm/Bip01 L Hand/LeftHandDummy");
		}

		rightHand = transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R ForeArm/Bip01 R Hand/RigthHandDummy");
		if(rightHand == null)
		{
			rightHand = transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R Forearm/Bip01 R Hand/RigthHandDummy");
		}

		upperBody = transform.Find("Bip01");
		legs = transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bone11/Bone12/Bone13");
		
		scale = GetScale(); //unit scale
		level = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_LEVEL);
	}
		
	public virtual void UpdateStats()
	{
		UpdateHP();
		Power = GetPower();
		MaxPower = GetMaxPower();
		level = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_LEVEL);
	}
		
	public void SetSpeed(UnitMoveType movetype, float speed)
	{
		moveSpeed = speed;
	}
		
	public virtual void SetPosition(Vector3 position, float orientation)
	{
		base.SetPosition(position, orientation);
	}
		
	uint[] _playerFactionID = {0, 1, 2, 3, 4, 5, 6, 116};
	uint[] _friendlyFactionID = {35, 29, 126, 69};
	uint[] _neutralFactionID = {7, 189};	
	uint[] _agresiveFactionID = {14, 44, 45};

	/**
	 *	Creates the name and label of the creature/Player and sets enemy status based on having or not flag = 0
	 *	NPC with flag 0 has no flag-marked role and is therefor monster
	 *	this will have to be reevaluated as the game functionality improves
	 */

	public void InitName(string text)
	{
		// If npc is a questgiver atach to him a object to store his status
		uint flag = (uint)NPCFlags.UNIT_NPC_FLAG_QUESTGIVER;
		if((GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) & flag) > 0)
		{
			GameObject gm = new GameObject();
			gm.name = "state";
			gm.transform.parent =  transform.Find("Bip01");
			gm.transform.localPosition = Vector3.zero;
		}
		
		if(text == "")
		{
			text = "Lizzie";
		}
		name = text;
		
		// ENEMY STATUS !!!
		if(GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) == 0 && GetTypeID() != TYPEID.TYPEID_PLAYER)
		{
			enemyStatus = 1;
		}

		uint dispalayID = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_DISPLAYID);
		bool isSelectable = !HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_NOT_SELECTABLE);
		if((!textName) && 
		   ((GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_NPC_FLAGS) != 0) || ((GetTypeID() == TYPEID.TYPEID_PLAYER) && !isMainPlayer)) && 
		   ((isSelectable) || (dispalayID == 601)))
			// if the text mesh wasn't built, and also if the unit is remarkable as NPC role (unlike monsters) or is a player but not the main player
		{
			GameObject go = new GameObject();
			go.name = "NameHolder";
			go.transform.parent = transform.parent;
			go.transform.localPosition = Vector3.zero;

			//Initiail
			MeshRenderer ms = go.AddComponent<MeshRenderer>();
			textName = go.AddComponent<TextMesh>();
			textName.anchor = TextAnchor.MiddleCenter;
			textName.alignment = TextAlignment.Center;
			
			uint rel = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE);//get creature/player faction id
			
			byte i;
			byte n = 0; 
			
			//search witch faction the creature belongs
			for(i = 0; i < _playerFactionID.Length; i++)
				if(_playerFactionID[i] == rel)
			{
				n = 1;
				break;
			}			  	
			
			for(i = 0; i< _friendlyFactionID.Length && n==0; i++)
				if(_friendlyFactionID[i] == rel)
			{
				n = 2;
				break;
			}			  	
			
			for(i = 0; i<_neutralFactionID.Length && n==0; i++)
				if(_neutralFactionID[i] == rel)
			{
				n = 3;
				break;
			}		
			
			for(i = 0; i<_agresiveFactionID.Length && n==0; i++)
				if(_agresiveFactionID[i] == rel)
			{
				n = 4;
				break;
			}		  	
			
			switch(n)
			{		
			case 1:
				textName.font = Resources.Load<Font> ("fonts/player"); 
				textName.font.material = Resources.Load<Material> ("fonts/playermat");
				textName.font.material.color = Color.blue;
				break;	
				
			case 2:
				textName.font = Resources.Load<Font> ("fonts/friendlyNPC");
				textName.font.material = Resources.Load<Material> ("fonts/friendlyNPCmat");
				textName.font.material.color = Color.green;
				break;
				
			case 3:
				textName.font = Resources.Load<Font> ("fonts/neutralNPC"); 
				textName.font.material = Resources.Load<Material> ("fonts/neutralNPCmat");
				textName.font.material.color = Color.yellow;
				break;
			case 4:
				textName.font = Resources.Load<Font> ("fonts/agresiveNPC"); 
				textName.font.material = Resources.Load<Material> ("fonts/agresiveNPCmat");
				textName.font.material.color = Color.red;
				break;
			default:
				textName.font = Resources.Load<Font> ("fonts/dontKnowNPC"); 
				textName.font.material = Resources.Load<Material> ("fonts/dontKnowNPCmat");
				textName.font.material.color = Color.black;
				break;
			}

			if(dispalayID == 601)
			{
				textName.font = Resources.Load<Font> ("fonts/dontKnowNPC"); 
				textName.font.material = Resources.Load<Material> ("fonts/whiteTxtMat");
				textName.font.material.color = Color.yellow;
				textName.fontSize *= 6;
			}
			
			/// calculated when we think is iOS only
			float IPhoneRaport = 480;
			IPhoneRaport /= 320;
			float deviceRaport = Screen.width;
			deviceRaport /= Screen.height;
			
			if(IPhoneRaport == deviceRaport)
			{
				textName.characterSize = 0.2f;/// iphone
			}
			else
			{
				textName.characterSize = 0.1f;// ipad
			}
			
			ms.material = textName.font.material;//
			if(!mCamera)
				mCamera = GameObject.Find("Main Camera");
			
			scale = GetScale(); //init Unit scale
			if(scale > 0)//scale < 1  means the its MainPlayer 
			{
				textName.characterSize = scale * 0.1f;
			}
			//place the textMesh above the player
			if(dispalayID == 601)
			{
				go.transform.localScale *= 5;
				go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 7.5f);
			}
			else
			{
				go.transform.localPosition = new Vector3(go.transform.localPosition.x,
				                                         transform.parent.GetComponent<Collider>().bounds.size.y + textName.lineSpacing * 0.45f,
				                                         go.transform.localPosition.z);
			}
			
			textName.text = name; //initialize the textMesh.text with the name of the player/creature
		}
		
		Name = name;
	}
		
//	int auraWidth1 = Screen.width/13;
//	int auraHeight1 = Screen.height/20;
//	int auraStartX1 = Screen.width*0.45+Screen.width/6;
//	int auraStartY1 = Screen.height/20*0.1;
		
	public int GetPower()
	{
		uint returnValue = 0;
		Powers powerType = GetPowerTypeNumber();
		switch(powerType)
		{
		case Powers.POWER_MANA:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POWER1);
			break;
		case Powers.POWER_RAGE:
			returnValue = (uint)Mathf.RoundToInt(GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POWER2) * 0.1f);
			break;
		case Powers.POWER_FOCUS:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POWER3);
			break;
		case Powers.POWER_ENERGY:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POWER4);
			break;
		case Powers.POWER_HAPPINESS:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POWER5);
			break;
		case Powers.POWER_RUNE:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POWER6);
			break;
		case Powers.POWER_RUNIC_POWER:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POWER7);
			break;
		}
		return (int)returnValue;
	}
		
	public int GetMaxPower()
	{
		uint returnValue = 0;
		Powers powerType = GetPowerTypeNumber();
		switch(powerType)
		{
		case Powers.POWER_MANA:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXPOWER1);
			break;
		case Powers.POWER_RAGE:
			returnValue = (uint)Mathf.RoundToInt(GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXPOWER2) * 0.1f);
			break;
		case Powers.POWER_FOCUS:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXPOWER3);
			break;
		case Powers.POWER_ENERGY:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXPOWER4);
			break;
		case Powers.POWER_HAPPINESS:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXPOWER5);
			break;
		case Powers.POWER_RUNE:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXPOWER6);
			break;
		case Powers.POWER_RUNIC_POWER:
			returnValue = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXPOWER7);
			break;
		}
		return (int)returnValue;
	}
		
	public Powers GetPowerTypeNumber()
	{
		byte shift = 24;
		uint powerType = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_BYTES_0) >> shift;
		return (Powers)powerType;
	}
	
	public string GetPowerType()
	{
		string returnValue = "";
		Powers powNr = GetPowerTypeNumber();
		switch(powNr)
		{
		case Powers.POWER_MANA:
			returnValue = "Mana";
			break;
		case Powers.POWER_RAGE:
			returnValue = "Wrath";
			break;
		case Powers.POWER_FOCUS:
			returnValue = "Focus";
			break;
		case Powers.POWER_ENERGY:
			returnValue = "Energy";
			break;
		case Powers.POWER_HAPPINESS:
			returnValue = "Happiness";
			break;
		case Powers.POWER_RUNE:
			returnValue = "Rune";
			break;
		case Powers.POWER_RUNIC_POWER:
			returnValue = "Rune Power";
			break;
		}
		return returnValue;
	}
		
	public void OrientationChange(bool hasTarget)
	{
		Vector3 waypoint;
		Vector3 distanceb;
		float angl;
		
		waypoint = way;

		if(hasTarget && target != null)
		{
			waypoint = target.TransformParent.position;
		}
		
		Quaternion rotation;
		distanceb = new Vector3(waypoint.x, 0, waypoint.z) - 
					new Vector3(TransformParent.position.x, 0, TransformParent.position.z);
		if(distanceb.sqrMagnitude == 0)
		{
			rotation = TransformParent.rotation;
		}
		else
		{
			rotation = Quaternion.LookRotation(distanceb);
		}

		angl = Quaternion.Angle(TransformParent.rotation, rotation);
		if(angl < -5.0f || angl > 5.0f )
		{
			TransformParent.GetComponent<Rigidbody>().rotation = rotation;
		}
	}
		
	public virtual void FixedUpdate()
	{
		if(mCamera == null)
		{
			mCamera = GameObject.Find("Main Camera");
		}

		if(textName && mCamera)
		{
			Vector3 vectorToCamera  = textName.transform.position - mCamera.transform.position;
			textName.transform.eulerAngles = new Vector3(textName.transform.eulerAngles.x,
			                                             orient8(vectorToCamera.z, vectorToCamera.x),
			                                             textName.transform.eulerAngles.z);
		}
		
		if((castTimeTotal > 0) && (castTime <= castTimeTotal))
		{
			castTime += Time.deltaTime;
		}

		if(((castTime >= 0) && (castTime > castTimeTotal)) || m_deathState == DeathState.DEAD)
		{
			castTimeTotal = 0;
			castTime = 0;
			SpellVisual scriptSpellVisual = FxEffectManager.GetSpellVisual(castSpellID);
			if(scriptSpellVisual != null)
			{
				int effectId = scriptSpellVisual.effect[0];
				FxEffectManager.DestroyAndRemoveEffect(effectId, guid);
			}
		}
	}
	
	public void UpdateHP()
	{
		HP = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_HEALTH);
		MaxHP = (int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXHEALTH);
	}
		
		
	static uint[] orient8Lookup = {11, 9, 5, 7, 13, 15, 3, 1};
	public float orient8(float x, float y)
	{ 
		return 22.5f * orient8Lookup[(x > 0 ? 4 : 0) + (y > 0 ? 2 : 0) + (Mathf.Abs(x) > Mathf.Abs(y) ? 1 : 0)];
	}

	public void Attack(Unit targ)
	{
		if(targ)
		{
			if(targ.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_HEALTH) > 0)
			{
				aHandler.PlayActionAnimation("");
			}
		}
	}
		
	public bool IsDead()
	{
		bool ret = true;
		if(m_deathState == DeathState.ALIVE || m_deathState == DeathState.JUST_ALIVED)
		{
			ret = false;
		}
		return ret;
	}
		
	public void StartCoroutineAnimation(string actionName)
	{
		StartCoroutine(WaitFrameAndPlayAnimation(actionName));
	}
		
	public IEnumerator WaitFrameAndPlayAnimation(string actionName)
	{
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		aHandler.PlayActionAnimation(actionName);
	}
		
	public void RotateToTarget(Transform target)
	{
		if(HP > 0)
		{
			Quaternion rotation;
			Vector3 distanceb = new Vector3(target.transform.position.x, 0, target.transform.position.z) - 
								new Vector3(TransformParent.position.x, 0, TransformParent.position.z);

			rotation = Quaternion.LookRotation(distanceb);
			TransformParent.GetComponent<Rigidbody>().rotation = rotation;
		}
	}

	public void CheckLootedFlag()
	{
		uint dynFlag = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_DYNAMIC_FLAGS);
		uint lootableFlag = (uint)UnitDynFlags.UNIT_DYNFLAG_LOOTABLE;
		if((dynFlag & lootableFlag) > 0)
		{
			CreateSparkleEffect();
		}
		else
		{
			DestroySparkleEffect();
		}
	}
}
