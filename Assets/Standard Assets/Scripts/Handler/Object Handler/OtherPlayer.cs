using UnityEngine;

public class OtherPlayer : Player
{
	bool  fw = false;
	bool  left = false;
	bool  right = false;
	float rotationSpeed = 20;
	Vector3 nextPosition;
	bool  isSpwned = true;
	
	void  Awake ()
	{
		base.Awake();
	}
	
	void  Start ()
	{
		base.Start();
		//It is necessary to reduce the number of calls to update the character models
		if(isSpwned && CheckEquipmentAfterSpawn())
			isSpwned = false;
		
		if(Application.loadedLevelName == "SelectCharacterWindow")
		{
			enabled = false;
			parentRB.useGravity = false;
			return;
		}
		
		UpdateStats();
		if(HP == 0)
		{
			m_deathState = DeathState.DEAD;
			//aHandler.PlayMovementAnimation("dead");
		}

		//update player customizations
		ByteBuffer _bytes = new ByteBuffer();
		_bytes.Append(GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_BYTES));
		
		skinColor  = _bytes._storage[3];
		face       = _bytes._storage[2];
		hairStyle  = _bytes._storage[1];
		hairColor  = _bytes._storage[0];
		
		_bytes = new ByteBuffer();
		_bytes.Append(GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_BYTES_2));	    
		facialHair = _bytes._storage[3];
		
		for(int inx = 0; inx < auraManager.auras.Count; inx++)
		{
			Aura aur = auraManager.auras[inx];
			auraManager.UpdateAura(aur);
		}
	}
	
	void  SetNextPosition ( Vector3 pos )
	{
		nextPosition = pos;
		TransformParent.LookAt(nextPosition);
	}
	
	void  SetPosition ( Vector3 pos, float o )
	{
		base.SetPosition(pos,o);
	}
	
	void  SetOrientation ( float o )
	{
		Quaternion tmp = TransformParent.rotation;
		tmp.eulerAngles = new Vector3(TransformParent.rotation.eulerAngles.x,
		                              (Mathf.PI*0.5f - o) * 180.0f/Mathf.PI,
		                              TransformParent.rotation.eulerAngles.z);
		TransformParent.rotation = tmp;
	}
	
	void  startRotateLeft ()
	{
		left = true;
		right = false;
	}
	
	void  startRotateRight ()
	{
		left = false;
		right = true;
	}
	
	void  stopRotate ()
	{
		left = right = false;
	}
	
	void  startMoveForWard ()
	{
		if(!isRooted)
		{
			MovementFlags flag = MovementFlags.MOVEMENTFLAG_FORWARD;
			moveFlag = moveFlag | flag;
			flag = MovementFlags.MOVEMENTFLAG_BACKWARD;
			moveFlag = moveFlag & ~flag;
		}
	}
	
	void  stopMove ()
	{
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD;
		if((moveFlag & flag) > 0)
		{
			flag = MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_WALK_MODE;
			moveFlag = moveFlag & ~flag;
		}
	}
	
	void  stopStrafe ()
	{
		MovementFlags flag = (MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT);
		if((moveFlag & flag) > 0)
		{
			moveFlag = moveFlag & ~flag;
		}
	}
	
	void  startMoveBackward ()
	{
		if(!isRooted)
		{
			MovementFlags flag = (MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_WALK_MODE); // backward walk is always slow; flag must be set, otherwise causing weird movement in other client
			moveFlag = moveFlag | flag ;
			flag = MovementFlags.MOVEMENTFLAG_FORWARD;
			flag = ~flag;
			moveFlag = moveFlag & flag;
		}
	}
	void  startMoveStrafeLeft ()
	{
		if(!isRooted)
		{
			MovementFlags flag = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT; 
			moveFlag = moveFlag | flag ;
			flag = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
			flag = ~flag;
			moveFlag = moveFlag & flag;
		}
	}
	void  startMoveStrafeRight ()
	{
		if(!isRooted) 
		{
			MovementFlags flag = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT; 
			moveFlag = moveFlag | flag ;
			flag = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
			flag = ~flag;
			moveFlag = moveFlag & flag;
		}
	}
	
	void  Update ()
	{
		base.Update();
		if(moveFlag == 0)
		{
			if(!botFlag)
			{
				parentRB.velocity = new Vector3(0, parentRB.velocity.y, 0);
			}
		}
		/* trololololo
if(target_cam_time > 0)
	target_cam_time--;
*/
	}
	
	void  UpdateStats ()
	{
		base.UpdateStats();
		
		if(HP > 1)
		{
			//Debug.Log("Update Stats Alive");
			m_deathState = DeathState.ALIVE;
			return;
		}
		
		if(HP == 0 && m_deathState == DeathState.ALIVE)// set the m_deathState to just_died for playing "die" animation in FixedUpdate()
		{
			//	Debug.Log("Update Stats Just Died");
			m_deathState = DeathState.JUST_DIED;
			return;
		}
	}
	
	void  FixedUpdate ()
	{
		if(armory != null && armory.IsAllLoaded())
		{
			armory.EnableAllParts();
		}
		
		base.FixedUpdate();
		
		if(left)
		{
			TransformParent.Rotate(0,-rotationSpeed*Time.deltaTime,0);
		}
		if(right)
		{
			TransformParent.Rotate(0,rotationSpeed*Time.deltaTime,0);
		}
	}
	
	void  LateUpdate ()
	{
		if(isSpwned && CheckEquipmentAfterSpawn())
		{
			isSpwned = false;
			armory.InitArmory(0);
			armory.UpdateArmory(0);
		}
	}
	
	bool CheckEquipmentAfterSpawn ()
	{
		bool  result = true;
		uint itemID = 0;
		for(EquipmentSlots slot = EquipmentSlots.EQUIPMENT_SLOT_HEAD; slot < EquipmentSlots.EQUIPMENT_SLOT_END; slot++)
		{
			if( slot != EquipmentSlots.EQUIPMENT_SLOT_HEAD
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_CHEST
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_LEGS
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_FEET
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_HANDS
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_BACK
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_TABARD
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_MAINHAND
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_OFFHAND
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_RANGED)
			{
				continue;
			}
			itemID = GetUInt32Value((int) UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_1_ENTRYID + 2*(int)slot);
			if(!AppCache.sItems.HaveRecord(itemID) && itemID > 0)
			{
				result = false;
				break;
			}
		}
		return result;
	}
	
}