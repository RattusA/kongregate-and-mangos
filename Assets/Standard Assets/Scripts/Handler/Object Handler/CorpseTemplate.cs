﻿using UnityEngine;

public class CorpseTemplate
{
	public ulong guid;
	public int entry;
	public bool  found;
	public uint mapid;
	public Vector3 position;
	public uint mainMapId;
}

