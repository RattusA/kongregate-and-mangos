using UnityEngine;
using System.Collections.Generic;

public class Player : Unit
{
	public ItemManager itemManager = null;
	public MountManager mountManager = null;
	public AssambleCharacter armory = null;

	public CraftManager craftManager = null;

//	public PlayerAnimationHandler aHandler = null;
	public bool charMount = false;
	Vector3 goto3d;
	
	//player info	
	public Races race;
	public Classes playerClass;
	
	public byte face;	
	public byte skinColor;
	public byte hairStyle;
	public byte hairColor;
	public byte facialHair;
	
	public int mask;

	//temp
	public uint mountDisplayID;	//not utilized yet
	public string inventoryMessage = "";	//hold inventory messages. Its uptated in wolrdsession.js
	
	//geometric vars
	public Vector3 slopeNormal = Vector3.up;
	public CollisionControl collControl;
	public GameObject lookPoint = null;
	public bool dive; // TODO togle for diving flag
	public bool CanJump = true;

	public PlayerSpellManager playerSpellManager = null;
	public PetSpellManager petSpellManager = null;
	public CooldownManager cooldownManager = null;

	public ulong petGuid;
	public Unit pet = null;
	public int petFamily = 0;
	public int petLvl = 0;
	public string petHappines= "UNHAPPY";

	public OpCodes lastpkt;
	public JoystickReturn moveDir;
	float player_height;

	public uint lastCastedSpellID = 0;
	public uint mithrilCoins = 0;
	public uint gearRating = 0;

	MovementFlags forward_flag = MovementFlags.MOVEMENTFLAG_FORWARD;
	MovementFlags backward_flag = MovementFlags.MOVEMENTFLAG_BACKWARD;
	public MovementFlags swim_flag = MovementFlags.MOVEMENTFLAG_SWIMMING;
	MovementFlags strafe_left = MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
	MovementFlags strafe_right = MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
	float final_speed;
	
	bool runStart = false;
	public Vector3[] path;
	public float movetime;
	public float movetimeMax;
	Vector3 startWay;
	public bool isServerControlled = false;
	
	uint targetWay;
	float timeLeftFotTargetWaypoint;
	float timeFotTargetWaypoint;
	bool isSimpleMove;
	float stepSpeed;
	Vector3 startWayPoint;
	Vector3 endWayPoint;
	
	public static string[] weaponAnimations =
	{
		"onehandaxe",
		"twohandssword",
		"bow",
		"crossbow",
		"onehandsword",
		"twohandssword",
		"spear",
		"onehandsword",
		"twohandssword",
		"",
		"staff",
		"",
		"",
		"basic",
		"",
		"onehandsword",
		"onehandsword",
		"twohandssword",
		"crossbow",
		"",
		"twohandssword", //fishing rod
		"dual"//slot 21 is for dual attack
		//!!!!!trebuie rezolvat pt crossbow, bow, basic si dual!!!!!
		//,"magic"//its not a slot
	};
	
	public bool botFlag = false;

	/// <summary>
	/// The is target from player paty.
	/// This function is used only for an object with a script MainPlayer.
	/// Required for the function in Javascript and get the result in C #
	/// </summary>
	public bool isTargetFromPlayerPaty = false;
	public bool IsTargetFromPlayerPaty(ulong guid)
	{
		gameObject.SendMessage("IsTargetFromMainPlayerPaty", guid);
		return isTargetFromPlayerPaty;
	}
	/// <summary>
	/// The is from enemy duel team.
	/// This function is used only for an object with a script MainPlayer.
	/// Required for the function in Javascript and get the result in C #
	/// </summary>
	public bool isFromEnemyDuelTeam = false;
	public bool IsFromEnemyDuelTeam(Player targetPlayer)
	{
		gameObject.SendMessage("IsPlayerFromEnemyDuelTeam", targetPlayer);
		return isFromEnemyDuelTeam;
	}

	//nicu activating/deactivating weapon renders, when flying when on mount, etc...
	public void ShowHideWeapons(bool val) 
	{
		//fixed
		foreach(Transform go in armory.leftHand)
		{
			go.gameObject.SetActive(val);
		}

		foreach(Transform go in armory.rightHand)
		{
			go.gameObject.SetActive(val);
		}
		
	}
	
	public static GameObject LoadPlayerModel(Races race, Gender gender)
	{
		GameObject player;
		string genderText = "";
		switch(gender)
		{
		case Gender.GENDER_FEMALE:
			genderText = "Female";
			break;
		case Gender.GENDER_MALE:
			genderText = "Male";
			break;
		}

		switch(race)
		{
		case Races.RACE_HUMAN:
			player = GameResources.InstantiateChar("Human_" + genderText);
			break;
		case Races.RACE_NIGHTELF:
			player = GameResources.InstantiateChar("Elf_" + genderText);
			break;
		case Races.RACE_DWARF:
			player = GameResources.InstantiateChar("Dwarf_" + genderText);
			break;
		case Races.RACE_BLOODELF:
			player = GameResources.InstantiateChar("DarkElf_" + genderText);
			break;
		case Races.RACE_ORC:
			player = GameResources.InstantiateChar("Orc_" + genderText);
			break;
		case Races.RACE_TROLL:
			player = GameResources.InstantiateChar("BloodDrak_" + genderText);
			break;
		default:
			player = GameResources.InstantiateChar("BloodDrak_Male" + genderText);
			break;
		}

		player.AddComponent<CollisionControl>();
		return player;
	}  
	
	public uint GetPlayerMoney()
	{
		return GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COINAGE);
	}
	
	public bool IsInGuild()
	{
		uint guildId = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_GUILDID);
		return guildId > 0;
	}
	
	public static FACTION GetFactionByRace(Races race) 
	{
		FACTION ret = FACTION.NOT_ASSIGNED;
		switch(race)
		{
		case Races.RACE_HUMAN:
		case Races.RACE_NIGHTELF:
		case Races.RACE_DWARF:
			ret = FACTION.ALLIANCE;
			break;
		case Races.RACE_BLOODELF:
		case Races.RACE_ORC:
		case Races.RACE_TROLL:
			ret = FACTION.FURY;
			break;
		}
		return ret;
	}
	
	public virtual void MoveStartForward()
	{
		if(isRooted)
		{
			return;
		}
		
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_FORWARD;
		if((moveFlag & flag) > 0)
		{
			return;
		}

		moveFlag = moveFlag | flag;
		flag = MovementFlags.MOVEMENTFLAG_BACKWARD;
		moveFlag = moveFlag & ~flag;
	}  
	
	public virtual void MoveStartBackward()
	{
		if(isRooted)
		{
			return;
		}
		
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_BACKWARD;
		if((moveFlag & flag) > 0)
		{
			return;
		}

		flag = (MovementFlags.MOVEMENTFLAG_BACKWARD | MovementFlags.MOVEMENTFLAG_WALK_MODE);
		moveFlag = moveFlag | flag ;
		flag = MovementFlags.MOVEMENTFLAG_FORWARD;
		moveFlag = moveFlag & ~flag;
	}
	
	public virtual void StartSwimming()
	{
		if(isRooted)
		{
			return;
		}

		MovementFlags flag = MovementFlags.MOVEMENTFLAG_SWIMMING;
		moveFlag |= flag;
		lastpkt = OpCodes.MSG_MOVE_START_SWIM;
	}
	
	public void StopSwimming()
	{
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_SWIMMING;
		moveFlag &= ~flag;
		lastpkt = OpCodes.MSG_MOVE_START_SWIM;
	}
	
	public virtual void MoveStop()
	{
		MovementFlags flag = (MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD | 
						   MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT);
		if((moveFlag & flag) == 0)
		{
			return;
		}

		flag = (MovementFlags.MOVEMENTFLAG_FORWARD | MovementFlags.MOVEMENTFLAG_BACKWARD | 
		        	  MovementFlags.MOVEMENTFLAG_WALK_MODE | MovementFlags.MOVEMENTFLAG_STRAFE_LEFT | 
		        	  MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT);
		moveFlag = moveFlag & ~flag;
	}
	
	public virtual void SetPosition(Vector3 pos, float o)
	{
		base.SetPosition(pos, o);
	}

	public virtual void Awake()
	{
		aHandler = new PlayerAnimationHandler(this) as AnimationHandler;
		itemManager = new ItemManager(this);
		mountManager = new MountManager(this);
	}

	public virtual void Start()
	{
		base.Start();		
		collControl = TransformParent.GetComponent<CollisionControl>();
		
		if(mountManager != null)
		{
			mountManager.Init();//create a list of mounts
		}
		
		mountDisplayID = GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MOUNTDISPLAYID);

		if(lookPoint == null)
		{
			lookPoint = new GameObject();
			lookPoint.name = "Look Point";
			lookPoint.transform.parent = transform.parent;
			lookPoint.transform.localPosition = Vector3.back;
		}

		player_height = 1.8f;
		playerFlag = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FLAGS);
	}	
	
	
	public void Attack(Unit targ)
	{
		if(targ == null)//exit if no target
		{
			aHandler.PlayMovementAnimation("idle");
			return;
		}
		
		Vector3 distanceb = new Vector3(targ.transform.position.x, 0, targ.transform.position.z) - 
			new Vector3(TransformParent.position.x, 0, TransformParent.position.z);
		float angl = Vector3.Angle(TransformParent.forward,distanceb);
		
		if(Vector3.Dot(Vector3.Cross(TransformParent.forward,distanceb), Vector3.up) < 0.0)
			angl=-angl;
		TransformParent.Rotate(0, angl, 0);

		if(itemManager.RangeMode)
		{
			itemManager.DeactivateRange();
		}
		StartCoroutineAnimation("");
	}
	
	/**
	 *	Update character movement by server request
	 */

	public void UpdateServerMovement()
	{
		if(isServerControlled)
		{
			if(movetime > 0)
			{
				if(runStart)
				{
					aHandler.PlayMovementAnimation("run");
					runStart = false;
				}
				
				//TODO need change transform.position
				TransformParent.position = Vector3.Lerp(startWay, way, (movetimeMax - movetime) * 1.0f / movetimeMax);
				
				float delta = Time.deltaTime;
				movetime -= 1000 * delta;
				Debug.Log("Movetime: Player " + movetime + " delta: " + delta);
			}
			else
			{
				TransformParent.position = way;
				aHandler.PlayMovementAnimation("idle");
				ReleaseControlByServer();
			}
		}
	}
	
	public void TakeControlByServer(Vector3[] path, int duration)
	{
		if(path.Length > 0)
		{
			this.runStart = true;
			this.path = path;
			this.movetime = duration;
			this.movetimeMax = duration;
			this.startWay = TransformParent.position;
			this.way = path[0];
			this.isServerControlled = true;
		}
	}
	
	public void ReleaseControlByServer()
	{
		movetime = 0;
		isServerControlled = false;
	}
	
	public virtual void FixedUpdate()
	{
		MovementFlags flag = MovementFlags.MOVEMENTFLAG_FALLING;
		flag = moveFlag & flag;
		
		if((flag > 0) && !isRooted)
		{
			parentRB.velocity = new Vector3(parentRB.velocity.x, parentRB.velocity.y + 5.0f, parentRB.velocity.z);
			CanJump = false;
			flag = MovementFlags.MOVEMENTFLAG_FALLING;
			moveFlag = moveFlag & ~flag;
		} 	
		
		base.FixedUpdate();
		
		if(isServerControlled)
		{
			return;
		}
		
		if(m_deathState == DeathState.JUST_DIED && movetime <= 0)
		{
			aHandler.PlayMovementAnimation("die");		
			m_deathState = DeathState.CORPSE;//now we can send the release spirit packet to server		
			
			return; //wait a frame 
		}
		
		if ((m_deathState == DeathState.CORPSE) && movetime <= 0)
		{
			if(!(aHandler.currentMovementAnimation.Contains("die") && 
			     GetComponent<Animation>().IsPlaying(aHandler.currentMovementAnimation)))//wait to finish playing die animation
				aHandler.PlayMovementAnimation("dead");	//play dead animation after die animation has finish
			
			return;	//the spirit wasnt realeased yet. we are still rooted
		}
		
		if(botFlag)
		{		
			float deltaTime = Time.deltaTime;
			
			if(target)
			{
				OrientationChange(true);
				if ((target as Unit).HP == 0)
				{
					target = null;
				}
			}
			if(movetime > 0)
			{
				aHandler.PlayMovementAnimation("run");
				
				if(target == null)
				{
					OrientationChange(false);
				}
				
				if(isSimpleMove)
				{
					TransformParent.position = Vector3.Lerp(TransformParent.position, way, deltaTime / movetime);
					
					movetime -= deltaTime;
					if((way - TransformParent.position).sqrMagnitude < 0.01f)
					{
						movetime = 0;
						OrientationChange(true);
					}
				}
				else
				{
					TransformParent.position = Vector3.Lerp(TransformParent.position, endWayPoint, deltaTime / timeLeftFotTargetWaypoint);
					movetime -= deltaTime;
					timeLeftFotTargetWaypoint -= deltaTime;
					if((way - TransformParent.position).sqrMagnitude < 0.01f)
					{
						movetime = 0;
						OrientationChange(true);
					}
					else if((endWayPoint - TransformParent.position).sqrMagnitude < 0.005f)
					{
						TransformParent.position = endWayPoint;
						GetNextDistance();
					}
				}
			}
			else
			{
				aHandler.PlayMovementAnimation("idle");
				botFlag = false;
			}
		}
		else if(moveFlag > 0)
		{			
			if(((moveFlag & forward_flag) > 0 || (moveFlag & strafe_left) > 0 || (moveFlag & strafe_right) > 0) && (moveFlag & backward_flag) == 0)
			{
				aHandler.movingBackwards = false;
				
				if((moveFlag & swim_flag) > 0)
				{
					aHandler.PlayMovementAnimation("swim");
				}
				else if(!CanJump)
				{
					aHandler.PlayMovementAnimation("jump");
				}
				else
				{
					if((moveFlag & forward_flag) > 0)
						if (!((moveFlag & strafe_left) > 0 || (moveFlag & strafe_right) > 0))
							aHandler.PlayMovementAnimation("run");
					if ((moveFlag & strafe_left) > 0)
					{
						aHandler.PlayMovementAnimation("strafe_run_left"); //Debug.Log("LEFT");
					}
					else if ((moveFlag & strafe_right) > 0)
					{
						aHandler.PlayMovementAnimation("strafe_run_right"); //Debug.Log("RIGHT");
					}
					else
					{
						aHandler.PlayMovementAnimation("run"); //Debug.Log("FORWARD");
					}
				}
				if(!CanJump)
				{
					final_speed = moveSpeed * 0.5f;
				}
				else
				{
					final_speed = moveSpeed;
				}
			}
			else if((moveFlag & backward_flag) > 0)
			{
				aHandler.movingBackwards = true;
				
				if((moveFlag & swim_flag) > 0)
				{
					aHandler.PlayMovementAnimation("swim_back");
				}
				else if(!CanJump)
				{
					aHandler.movingBackwards = false;
					aHandler.PlayMovementAnimation("jump");
				}
				else
				{
					aHandler.PlayMovementAnimation("run"); // Debug.Log("BACK");
				}
				
				if(!CanJump)
				{
					final_speed = -moveSpeed * 0.3f;
				}
				else
				{
					final_speed = -moveSpeed * 0.5f;
				}
			}
			else
			{
				if((moveFlag & swim_flag) > 0)
				{
					aHandler.PlayMovementAnimation("swim_idle");
					//Debug.Log("swim_idle");
				}
				else
				{
					if(!botFlag)
						aHandler.PlayMovementAnimation("idle");
				}
				final_speed = 0;
			}
			
			goto3d = final_speed * (TransformParent.position - lookPoint.transform.position);
			if(!isMainPlayer)
			{
				if((moveFlag & forward_flag) > 0)
				{
					if((moveFlag & strafe_left) > 0)
					{
						goto3d = Quaternion.AngleAxis(-45, Vector3.up) * goto3d;
					}
					else if((moveFlag & strafe_right) > 0)
					{
						goto3d = Quaternion.AngleAxis(45, Vector3.up) * goto3d;
					}
				}
				else if((moveFlag & backward_flag) > 0)
				{
					if((moveFlag & strafe_left) > 0)
					{
						goto3d = Quaternion.AngleAxis(45, Vector3.up) * goto3d;
					}
					else if((moveFlag & strafe_right) > 0)
					{
						goto3d = Quaternion.AngleAxis(-45, Vector3.up) * goto3d;
					}
				}
				else
				{
					if((moveFlag & strafe_left) > 0)
					{
						goto3d = Quaternion.AngleAxis(-90, Vector3.up) * goto3d;
					}
					else if((moveFlag & strafe_right) > 0)
					{
						goto3d = Quaternion.AngleAxis(90, Vector3.up) * goto3d;
					}
				}
			}
			
			switch(moveDir)
			{
			case JoystickReturn.FORWARD_A:
				break;
			case JoystickReturn.FORWARD_B:
				break;
			case JoystickReturn.FORWARD_LEFT:
				goto3d = Quaternion.AngleAxis(-45, Vector3.up) * goto3d;
				break;
			case JoystickReturn.FORWARD_RIGHT:
				goto3d = Quaternion.AngleAxis(45, Vector3.up) * goto3d;
				break;
			case JoystickReturn.SIDE_RIGHT:
				goto3d = Quaternion.AngleAxis(90, Vector3.up) * goto3d;
				break;
			case JoystickReturn.SIDE_LEFT:
				goto3d = Quaternion.AngleAxis(-90, Vector3.up) * goto3d;
				break;
			case JoystickReturn.BACKWARD_LEFT:
				goto3d = Quaternion.AngleAxis(45, Vector3.up) * goto3d;
				break;
			case JoystickReturn.BACKWARD_RIGHT:
				goto3d = Quaternion.AngleAxis(-45, Vector3.up) * goto3d;
				break;
			}
			parentRB.velocity = new Vector3(goto3d.x, parentRB.velocity.y, goto3d.z);
			
			if(isUnderWater)
			{
				parentRB.velocity = new Vector3(parentRB.velocity.x, goto3d.y, goto3d.z);
			}
		}
		else if(!CanJump)
		{
			aHandler.PlayMovementAnimation("jump");
		}
		else
		{
			aHandler.PlayMovementAnimation("idle");
			final_speed = 0;
		}		
	}
	
	//-------------Waypoint move functions
	public void InitMove()
	{
		float moveDistance ;
		Vector3 lastPoint;
		float currDistance;
		targetWay = 0;
		timeLeftFotTargetWaypoint = 0.0f;
		timeFotTargetWaypoint = 0.0f;

		if(path.Length == 1)
		{
			isSimpleMove = true;
		}
		else
		{
			isSimpleMove = false;
			moveDistance = 0.0f;
			
			lastPoint = TransformParent.position;
			
			for(int i = 0 ; i < path.Length;  i++ )
			{
				moveDistance += Vector3.Distance(path[i], lastPoint);
				lastPoint = path[i];
			}
			
			if(moveDistance == 0)
			{
				return;
			}
			
			stepSpeed = moveDistance / movetimeMax;
			
			startWayPoint = TransformParent.position;
			endWayPoint = path[0];
			currDistance = Vector3.Distance(startWayPoint, endWayPoint);
			
			timeFotTargetWaypoint = currDistance / stepSpeed;
			timeLeftFotTargetWaypoint = timeFotTargetWaypoint;
		}
	}
	
	public void GetNextDistance()
	{
		if(targetWay < (path.Length - 1))
		{
			startWayPoint = path[targetWay];
			targetWay += 1;   
			endWayPoint = path[targetWay];
			
			var currDistance = Vector3.Distance(startWayPoint, endWayPoint);   
			timeFotTargetWaypoint = currDistance / stepSpeed;
			timeLeftFotTargetWaypoint = timeFotTargetWaypoint;
		}
		else
		{
			movetime = 0;  
		}
	}
	
	public virtual void UpdateStats()
	{
		base.UpdateStats();
		playerFlag = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FLAGS);
	}
	
	public Dictionary<int, uint> GetVisibleItemsEntrys()
	{
		Dictionary<int, uint> items = new Dictionary<int, uint>();
		for(int inx = (int)EquipmentSlots.EQUIPMENT_SLOT_START; inx < (int)EquipmentSlots.EQUIPMENT_SLOT_END; inx++)
		{
			int valueIndex = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_1_ENTRYID + 2 * inx;
			items[inx] = GetUInt32Value(valueIndex);	
		}
		return items;
	}
	
	public bool SameFaction(Player pl)
	{
		int[] allianceFactionID = {1, 3, 4};
		List<int> allianceFactionListID = new List<int>(allianceFactionID);
		int[] furyFactionID = {2, 116, 1610};
		List<int> furyFactionListID = new List<int>(furyFactionID);
		if(Global.In_array((int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE), allianceFactionListID)
		   && Global.In_array((int)pl.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE), allianceFactionListID))
		{
			return true;
		}

		if(Global.In_array((int)GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE), furyFactionListID)
		   && Global.In_array((int)pl.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE), furyFactionListID))
		{
			return true;
		}
	
		return false;
	}
	
	public void OnDestroy()
	{
		if(mountManager.currentMount)
		{
			mountManager.Unmount();
		}
	}

	public void UpdateCraftInfo()
	{
		craftManager.ResetCraftInfo();

		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_BLACKSMITHING, true));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_LEATHERWORKING, true));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_ALCHEMY, true));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_HERBALISM, true));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_MINING, true));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_TAILORING, true));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_ENGINERING, true));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_ENCHANTING, true));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_SKINNING, true));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_JEWELCRAFTING, true));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_INSCRIPTION, true));
		
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_FIRST_AID, false));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_FISHING, false));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_COOKING, false));
		StartCoroutine(craftManager.UpdateCraftInfo((uint)CraftSkill.SKILL_RIDING, false));
	}
	
	public void PlayNewAnimationFromTime(string animationName, float startTime)
	{
		aHandler.PlayMovementAnimationFromTime(animationName, startTime);
	}
	
	public void UpdateStealth()
	{
		if(stealth)
		{
			Transform go = transform.Find("1(Clone)");
			if(!go && TransformParent)
			{
				go = TransformParent.Find("1(Clone)");
			}
			
			if(go)
			{
				go.GetComponentInChildren<Lurk>().AnimHolder = TransformParent.Find(name).GetComponent<Animation>();
			}
		}
	}

	public void DeactivateRange(ulong _guid)
	{
		if(guid == _guid)
			itemManager.DeactivateRange();
	}

	public void Update()
	{
		RefreshVisibilityOfWeaponsSlots();
	}

	private void RefreshVisibilityOfWeaponsSlots()
	{
		if(armory != null)
		{
			GameObject mainHand = armory.currentVisibleArmours[(int)EquipmentSlots.EQUIPMENT_SLOT_MAINHAND];
			GameObject offHand = armory.currentVisibleArmours[(int)EquipmentSlots.EQUIPMENT_SLOT_OFFHAND];
			GameObject rangeSlot = armory.currentVisibleArmours[(int)EquipmentSlots.EQUIPMENT_SLOT_RANGED];
			if(charMount)
			{
				if(rangeSlot != null && rangeSlot.activeSelf)
				{
					rangeSlot.SetActive(false);
				}
				if(mainHand != null && mainHand.activeSelf)
				{
					mainHand.SetActive(false);
				}
				if(offHand != null && offHand.activeSelf)
				{
					offHand.SetActive(false);
				}
			}
			else
			{
				if(itemManager.RangeMode)
				{
					if(rangeSlot != null && !rangeSlot.activeSelf)
					{
						rangeSlot.SetActive(true);
						itemManager.ActivateRange();
					}
					if(mainHand != null && mainHand.activeSelf)
					{
						mainHand.SetActive(false);
					}
					if(offHand != null && offHand.activeSelf)
					{
						offHand.SetActive(false);
					}
				}
				else
				{
					if(rangeSlot != null && rangeSlot.activeSelf)
					{
						itemManager.DeactivateRange();
						rangeSlot.SetActive(false);
					}
					if(mainHand != null && !mainHand.activeSelf)
					{
						mainHand.SetActive(true);
					}
					if(offHand != null && !offHand.activeSelf)
					{
						offHand.SetActive(true);
					}
				}
			}
		}
	}
}