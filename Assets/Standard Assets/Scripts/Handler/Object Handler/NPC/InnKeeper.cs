using UnityEngine;

public class InnKeeper
{
	// private variables
	private WorldPacket pkt;
	private NPC npc;
	private bool  canShowUI = false;
	
	//UInterface
	private UIAbsoluteLayout innKeeperContainer;
	private UISprite innKeeperBackground;
	private UISprite innKeeperDecoration;
	private UIButton innKeeperYesButton;
	private UIButton innKeeperNoButton;
	private UIButton innKeeperCloseButton;
	private UITextInstance[] innKeeperTexts;
	
	public InnKeeper ( NPC npc2 )
	{
		npc = npc2;
		canShowUI = true;
	}
	
	public void  Show ()
	{
		if(ButtonOLD.menuState)
		{
			Disable();
		}
		if(canShowUI)
		{
			innKeeperContainer = new UIAbsoluteLayout();
			innKeeperTexts = new UITextInstance[2];
			ShowInnKeeperUI();
			canShowUI = false;
		}
	}
	
	void  Disable ()
	{
		DestroyInnKeeperUI();
		npc.menuState = 0;
		WorldSession.interfaceManager.hideInterface = false;
		WorldSession.player.interf.stateManager();
		WorldSession.player.blockTargetChange = false;
		WorldSession.interfaceManager.hideInterface = false;
	}
	
	void  ShowInnKeeperUI ()
	{
		DestroyInnKeeperUI();
		
		innKeeperBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.08f , Screen.height*0.25f , 5);
		innKeeperBackground.setSize(Screen.width*0.7f , Screen.height*0.6f);
		
		innKeeperDecoration = UIPrime31.questMix.addSprite("party_decoration.png" , innKeeperBackground.position.x + innKeeperBackground.width*0.05f , -innKeeperBackground.position.y + innKeeperBackground.height*0.1f , 4);
		innKeeperDecoration.setSize(innKeeperBackground.width*0.9f , innKeeperBackground.height*0.5f);
		
		innKeeperYesButton = UIButton.create(UIPrime31.questMix , "yes.png" , "yes.png" , innKeeperBackground.position.x + innKeeperBackground.width*0.15f , -innKeeperBackground.position.y + innKeeperBackground.height*0.75f , 4);
		innKeeperYesButton.setSize(innKeeperBackground.width*0.25f , innKeeperBackground.height*0.12f);
		innKeeperYesButton.onTouchDown += AttuneAccept;
		
		innKeeperNoButton = UIButton.create(UIPrime31.questMix , "no.png" , "no.png" , innKeeperBackground.position.x + innKeeperBackground.width*0.6f , -innKeeperBackground.position.y + innKeeperBackground.height*0.75f , 4);
		innKeeperNoButton.setSize(innKeeperBackground.width*0.25f , innKeeperBackground.height*0.12f);
		innKeeperNoButton.onTouchDown += AttuneDecline;
		
		innKeeperCloseButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		innKeeperCloseButton.setSize(innKeeperBackground.height*0.13f , innKeeperBackground.height*0.13f);
		innKeeperCloseButton.position = new Vector3( innKeeperBackground.position.x + innKeeperBackground.width - innKeeperCloseButton.width/1.5f , innKeeperBackground.position.y + innKeeperCloseButton.height/3 , 4);
		innKeeperCloseButton.onTouchDown += AttuneDecline;
		
		innKeeperContainer.addChild(innKeeperBackground , innKeeperDecoration , innKeeperYesButton , innKeeperNoButton , innKeeperCloseButton);
		
		innKeeperTexts[0] = WorldSession.player.interf.text2.addTextInstance("Your Home Rune its currently\nattuned at : "+MainPlayer.bind.GetMapName()+":\n"+MainPlayer.bind.homeBind , innKeeperBackground.position.x + innKeeperBackground.width * 0.52f , -innKeeperBackground.position.y + innKeeperBackground.height*0.08f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		
		innKeeperTexts[1] = WorldSession.player.interf.text3.addTextInstance("Do you want to\nattune home rune to this altar?" , innKeeperBackground.position.x + innKeeperBackground.width * 0.57f , -innKeeperBackground.position.y + innKeeperBackground.height*0.47f , UID.TEXT_SIZE*0.8f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		
		
		/*		
		GUI.DrawTexture( new Rect(0, Screen.height * 0.05f, Screen.width, Screen.height*0.9f), WorldSession.player.Background);
		GUI.Label( new Rect(Screen.width*0.3f,Screen.height*0.2f, Screen.width * 0.4f, Screen.height * 0.2f), "Altar", UID.skin.customStyles[4]);
		GUI.Label( new Rect(Screen.width * 0.2f, Screen.height * 0.35f, Screen.width * 0.6f, Screen.height * 0.2f), "Your Home Rune its currently attuned at : "+WorldSession.player.bind.GetMapName()+": "+WorldSession.player.bind.homeBind, UID.skin.GetStyle("gossip_text"));
		GUI.Label( new Rect(Screen.width * 0.2f, Screen.height * 0.55f, Screen.width * 0.6f, Screen.height * 0.2f), "Do you want to attune home rune to this altar?", UID.skin.GetStyle("gossip_text"));

		if(GUI.Button( new Rect(Screen.width * 0.25f, closeButtonVPos, closeButtonWidth, closeButtonHeight), "Close", WorldSession.player.guildskin.button))
		{
			Disable();
		}

		if(GUI.Button( new Rect(Screen.width * 0.63f, closeButtonVPos, closeButtonWidth, closeButtonHeight), "Yes", WorldSession.player.guildskin.button))
		{
			AttuneHomeRune();
			Disable();
		}
*/	}
	
	void  AttuneAccept ( UIButton sender )
	{
		AttuneHomeRune();
		Disable();
	}
	
	void  AttuneDecline ( UIButton sender )
	{
		Disable();
	}
	
	void  DestroyInnKeeperUI ()
	{
		innKeeperContainer.removeAllChild(true);
		
		for(int i= 0 ; i < 2 ; i++)
			if(innKeeperTexts[i] != null)
		{
			innKeeperTexts[i].clear();
			innKeeperTexts[i] = null;
		}
	}
	
	private void  AttuneHomeRune ()
	{
		pkt = new WorldPacket(OpCodes.CMSG_BINDER_ACTIVATE, 136);
		pkt.Append(ByteBuffer.Reverse(npc.guid));
		Debug.Log("send innkeeper "+npc.guid);
		RealmSocket.outQueue.Add(pkt);
		pkt = null;
	}
}