using UnityEngine;
using System;
using System.Collections.Generic;

public class QuestGiver
{
	UIText text1 = new UIText (UIPrime31.myToolkit1, "fontiny dark","fontiny dark.png");
	UIText text2 = new UIText (UIPrime31.myToolkit1, "fontiny title","fontiny title.png");
	UIText text3 = new UIText (UIPrime31.myToolkit1, "fontiny simple","fontiny simple.png");
	
	NPC npc;
	ulong guid;
	
	//"NO QUEST FOR YOU" INTERFACE
	UIAbsoluteLayout noQuestPopupContainer;
	UISprite noQuestPopup;
	UIButton closeNoQuest;
	UIButton closeNoQuestX;
	UITextInstance noQuestText;
	UITextInstance closeNoQuestText;
	
	//dimensiuni
	float buttonHeight;
	float listWidth;
	uint nrVisibleQuest = 10;
	float listHeight;
	float buttonWidth;
	float closeButtonHeight;
	float closeButtonWidth;
	float closeButtonVPos;
	ulong[] param;
	bool  goCompleteQuest = false;		
	public bool  goShowQuestDetails = false;
	bool  goFinishQuest = false;
	
	MainPlayer mainPlr = WorldSession.player;
//	quest qsttt;
	
	byte itemChoiceSelected=1;
	string[] itemsName;
	public string[] itemsName2;
	
	public Vector2 scrollQViewVector = Vector2.zero;
	public Vector2 scrollDViewVector = Vector2.zero;
	public int selectedQuestPosition = -1;	
	
	public bool completeQuest = false;	
	public bool reward = false;
	public int rewItemChoice = -1;
	public QuestStatus qstState = QuestStatus.QUEST_STATUS_INCOMPLETE;	
	
	public quest selectedQuest = null;
	
	public List<quest> quests;
	public List<Item> questItems;
	
	GUIStyle style;
	
	public string[] itemsReqItems;
	
	public int completeQuestStages = -1;
	
	public QuestGiver ( NPC npcc )
	{
		this.guid = npcc.guid;
		this.npc = npcc;
		
		style = mainPlr.newskin.customStyles[6];
		param = new ulong[3];	
		
		quests = new List<quest>();
		questItems = new List<Item>();
		
		
		goCompleteQuest = false;		
		goShowQuestDetails = false;
		goFinishQuest = false;
		itemChoiceSelected  = 1;
		
		//	Debug.Log(" New QuestGiverHandler: guid: "+this.npc.guid + " recived guid: "+npcc.guid);
		GetQuestList();
		closeButtonVPos = Screen.height * 0.85f;
		closeButtonHeight = Screen.height * 0.1f;
		closeButtonWidth = Screen.width * 0.12f;
	}
	
	void  GetQuestList ()
	{
		param  = new ulong[1];
		param[0] = npc.guid;
		//MonoBehaviour.print("guid NPC: "+param[0]);
		WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_HELLO/*CMSG_GOSSIP_HELLO*/ ,param);
	}
	
	void  Disable ()
	{
		npc.menuState = 0;
		WorldSession.interfaceManager.hideInterface = false;
		WorldSession.player.interf.stateManager();
		WorldSession.player.blockTargetChange = false;
	}
	
	public void  addItem ( Item item )
	{
		if(questItems == null)
		{
			questItems = new List<Item>();
			questItems.Add(item);
			return;
		}
		
		int k =0;
		foreach(Item itm in questItems)
		{
			if(itm.entry == item.entry)
			{
				questItems.RemoveAt(k);		
				//break;
			}
			++k;
		}	
		
		questItems.Add(item);
	}
	
	string getItemInfo ( uint itemEntry )
	{
		string info= "";
		foreach(Item itm in questItems)
		{
			if(itemEntry == itm.entry)
			{
				info = info +"Name: "+ itm.name;
				info = info +"\nLevel: "+ itm.itemLvl;
				//info = info +"\nDamage: "+itm.damage[0].damageMin +" - " +itm.damage[0].damageMax;
				Debug.Log(info);
				break;
			}
		}
		return info;
	}
	
	//add quest to NPC
	public bool addQuest ( quest qst2 )
	{
		bool ret = true;
		//MonoBehaviour.print("[NPC  Quest:] quest added: "+qst2.questId);
		if(quests == null)
		{
			quests = new List<quest>();
		}
		
		foreach(quest qst in quests)
		{
			if(qst2.questId == qst.questId)
			{
				ret = false;
				break;
			}
		}

		if(ret)
		{
			quests.Add(qst2);
		}
		
		return ret;
	}
	
	public int getQuestIndex ( uint id )
	{
		byte i =0;
		foreach(quest q in quests)
		{
			if(q.questId == id)
				return i;
			i++;
		}
		return -1;
	}
	
	public void  prepareReqQuestItems ( quest qst )
	{
		itemsReqItems = new string[6];
		
		if(qst.getReqItemsCount > 0)
		{
			for(int i = 0; i < qst.getReqItemsCount; ++i)
			{
				if(qst.reqItemId[i] > 0)
				{
					ItemEntry itm = AppCache.sItems.GetItemEntry(qst.reqItemId[i]);
					if(itm.ID > 0)
					{
						itemsReqItems[i] = itm.Name;
					}
					else
					{
						itemsReqItems[i] = "reqItem"+i;
					}
				}
			}
		}	
		
	}
	
	public void  prepareRewQuestItems ( quest qst )
	{
		byte i=0;
		itemsName = new string[6];
		itemsName2 = new string[6];
		
		if(qst.getRewChoiceItemCount > 0)
		{
			for(; i < qst.getRewChoiceItemCount; ++i)
			{
				if(qst.rewChoiceItemId[i] > 0)
				{
					ItemEntry itm = AppCache.sItems.GetItemEntry(qst.rewChoiceItemId[i]);
					if(itm.ID > 0)
					{
						itemsName2[i] = itm.Name;
					}
					else
					{
						itemsName2[i] = "rewChoice item"+i;
					}
				}
			}
		}	
		
		if(qst.getItemsCount > 0)
		{
			for(; i < qst.getItemsCount; ++i)//george
			{
				if(qst.rewItemId[i] > 0)
				{
					ItemEntry itm2 = AppCache.sItems.GetItemEntry(qst.rewItemId[i]);
					if(itm2.ID > 0)
					{
						itemsName[i] = itm2.Name;
					}
					else
					{
						itemsName[i] = "rew item"+i;
					}
					
				}
			}
		}
	}
	
	void  showReqQuestItems ( quest qst )
	{
		byte i=0;
		
		if(qst.getReqItemsCount>0)
		{
			GUILayout.Label("\nRequired:", style);
			
			for(i = 0;i<qst.getReqItemsCount;++i)
			{
				if(GUILayout.Button(""+itemsReqItems[i],GUILayout.Width(200)))		  				 
				{
				}
			}
		}
	}
	
	void  showRewQuestItems ( quest qst )
	{
		byte i=0;
		
		if(qst.getRewChoiceItemCount>0)
		{
			GUILayout.Label("\nReward Choice Item:", style);
			
			for(i = 0;i<qst.getRewChoiceItemCount;++i)
				if(GUILayout.Button(""+itemsName2[i],GUILayout.Width(200)))		  				 
			{
				Debug.Log("Please choose: "+i);
				itemChoiceSelected = i; // 
			}
		}
		
		if(qst.getItemsCount>0)
		{
			GUILayout.Label("\nReward items:", style);
			for(i = 0; i< qst.getItemsCount;++i)
				if(GUILayout.Button(""+itemsName[i],GUILayout.Width(200)))
			{
			}
		}
	}
	
	void showQuestDetails( quest qst )      // quest afisat la NPC
	{
		GUILayout.BeginArea( new Rect(listWidth*1.1f, Screen.height*0.2f, listWidth*1.7f, Screen.height * 0.5f));
		scrollDViewVector = GUILayout.BeginScrollView(scrollDViewVector ,GUILayout.Width(listWidth*1.7f),GUILayout.Height(Screen.height * 0.5f));
		GUILayout.Label(""+qst.title, style);
		try{
			qst.details = qst.details.Replace("CHAR_CLASS", Global.GetClassAsString( WorldSession.player.playerClass ) );
			
		}
		catch( Exception e )
		{
			MonoBehaviour.print(e.ToString());
		}
		
		//====
		try{
			qst.details = qst.details.Replace("CHAR_NAME", WorldSession.player.name );
			qst.details = qst.details.Replace("NICKNAME", WorldSession.player.name );
		}
		catch( Exception e )
		{
			MonoBehaviour.print(e.ToString());
		}
		//Debug.Log("Class: " + WorldSession.player._class);
		switch(qstState)
		{
		case QuestStatus.QUEST_STATUS_INCOMPLETE: //new quest
			if(goShowQuestDetails == true)
			{
				GUILayout.Label("\n"+qst.details, style);
				GUILayout.Label("\nObjectives:\n"+qst.objectives, style);
				showRewQuestItems(qst);
				
			}	
			break;
			
		case QuestStatus.QUEST_STATUS_COMPLETE: //quest complete
			if(completeQuestStages == 2)
			{
				//GUILayout.Label("Objectives:\n"+qst.objectives, style);
				GUILayout.Label("\n"+qst.offerRewText+"\n\nReward:", style);
				
				showRewQuestItems(qst);
				
				if(qst.XPAmount>0)
					GUILayout.Label("XP: "+qst.XPAmount, style);
				if(qst.moneyAmaunt>0)
					GUILayout.Label("Gold: "+qst.moneyAmaunt, style);
			}
			else if(completeQuestStages == 0)
			{
				GUILayout.Label("Objectives:\n"+qst.objectives, style);
				GUILayout.Label("Progress:\n"+qst.progress, style);
				showReqQuestItems(qst);
			}
			
			break;
			
		case QuestStatus.QUEST_STATUS_UNAVAILABLE: // quest failed
			GUILayout.Label("\nQuest failed.", style);
			break;
		default: //quest in incomplete
			GUILayout.Label("Objectives:\n"+qst.objectives, style);
			GUILayout.Label("Progress:\n"+qst.progress, style);
			showReqQuestItems(qst);
			break;
		}		
		
		GUILayout.EndScrollView();
		GUILayout.EndArea();
	}		 
	
	public void  ShowListOfQuests ()
	{
		if(quests == null)
		{
			quests = new List<quest>();
		}
		else
		{
			if(quests.Count == 0)
			{
				NoQuestForYou();
			}
			else
			{
				WorldSession.player.interf.ShowQuestListInterface(quests);		
			}
		}
	}
	
	void  showListOfQuests ()
	{
		if(quests == null)
		{
			quests = new List<quest>();
			return;
		}
		if(quests.Count == 0)
		{
			GUI.DrawTexture( new Rect(0, Screen.height * 0.05f, Screen.width, Screen.height*0.9f), WorldSession.player.Background);
			GUI.Label( new Rect(Screen.width*0.3f, Screen.height * 0.2f, Screen.width * 0.4f, Screen.height * 0.2f), "I have no quests for you.", style);
			if(GUI.Button( new Rect(Screen.width * 0.83f, closeButtonVPos, closeButtonWidth, closeButtonHeight), "Close", WorldSession.player.guildskin.button))
			{
				Disable();
			}
			return;
		}
		
		//	 Debug.Log("qstate "+qstState+" details "+goShowQuestDetails+" gocomplete "+goCompleteQuest+" complete "+completeQuest);
		
		buttonHeight = Screen.height*0.06f;
		listWidth  = Screen.width*0.35f;
		listHeight = buttonHeight*(10);
		buttonWidth = listWidth*0.95f;
		
		GUI.DrawTexture( new Rect(0, Screen.height * 0.05f, Screen.width, Screen.height*0.9f), WorldSession.player.Background);
		GUILayout.BeginArea( new Rect(Screen.width*0.01f,Screen.height*0.3f,listWidth,listHeight));
		scrollQViewVector = GUILayout.BeginScrollView(scrollQViewVector ,GUILayout.Width(buttonWidth),GUILayout.Height(listHeight));
		
		
		for(byte i = 0; i < quests.Count && quests.Count >= 1; i++)
		{
			quest q = quests[i];
			if(!string.IsNullOrEmpty(q.title))
			{
				if(GUILayout.Button(q.title, GUILayout.Height(buttonHeight)))
				{					
					selectedQuestPosition = i;
					
					qstState = WorldSession.player.questManager.questState(q.questId);
					//Debug.Log("--------------------  "+qstState);
					
					quest qq = quests[selectedQuestPosition];
					
					param = null;
					param = new ulong[3];
					param[0] = npc.guid;
					param[1] = qq.questId;
					param[2] = 0;
					
					//						//hiding tutorial tip
					//						if (WorldSession.player.tutorialInstance) {
					//							if (!WorldSession.player.tutHandler.stepsDone[6]) {
					//								WorldSession.player.tutHandler.stepsDone[6] = true;
					//							}			
					//							if (WorldSession.player.tutHandler.tutorialStep == 6) {											
					//								WorldSession.player.tutHandler.showHint = false;						
					//							}
					//						}
					
					WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_QUERY_QUEST,param);
					WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_COMPLETE_QUEST,param);	
					completeQuestStages = -1;
				}
			}
		}
		
		GUILayout.EndScrollView();
		GUILayout.EndArea();

		if(selectedQuestPosition > -1 && selectedQuestPosition<quests.Count)
		{   quest cq = quests[selectedQuestPosition];
			
			if(qstState == QuestStatus.QUEST_STATUS_NONE)
			{
				//Debug.Log("quest in progress!!!!");
			}
			
			if(qstState == QuestStatus.QUEST_STATUS_UNAVAILABLE)
			{
				//Debug.Log("quest fail!!!!");
			}
			
			
			
			if(qstState == QuestStatus.QUEST_STATUS_INCOMPLETE && goShowQuestDetails==true)
			{
				if(GUI.Button( new Rect(listWidth*2.2f,listHeight+buttonHeight*2,buttonWidth*0.5f,buttonHeight*1.2f),"Get Quest", WorldSession.player.guildskin.button))
				{
					//MonoBehaviour.print("Get Quest!");
					param[0] = npc.guid;
					param[1] = cq.questId;
					param[2] = 0;
					WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_ACCEPT_QUEST,param);
					
					WorldPacket pkt = new WorldPacket();
					pkt.SetOpcode(OpCodes.CMSG_QUESTGIVER_STATUS_MULTIPLE_QUERY);
					RealmSocket.outQueue.Add(pkt);
					
					//advance tutorial code
					//				if (WorldSession.player.tutorialInstance) {			
					//					if (WorldSession.player.tutHandler.tutorialStep == 8) {
					//						WorldSession.player.tutHandler.showHint = true;						
					//					}
					//				}					
					//				if (WorldSession.player.tutorialInstance) {
					//					if (!WorldSession.player.tutHandler.stepsDone[6]) {
					//						WorldSession.player.tutHandler.stepsDone[6] = true;
					//					}			
					//					if (WorldSession.player.tutHandler.tutorialStep == 6) {
					//						WorldSession.player.tutHandler.advanceTutorial();						
					//					}
					//				}				
					Disable();
				}
			}
			
			if(qstState == QuestStatus.QUEST_STATUS_COMPLETE)
			{
				switch(completeQuestStages)
				{
				case 0: 
					if(GUI.Button( new Rect(listWidth*2.2f,listHeight+buttonHeight*2,buttonWidth*0.5f,buttonHeight*1.2f),"Continue", WorldSession.player.guildskin.button))
					{
						param[0] = npc.guid; // npcGuid
						param[1] = cq.questId; //quest ID
						WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_REQUEST_REWARD,param);
					}
					break;
				case 2: 
					if(GUI.Button( new Rect(listWidth*1.2f,listHeight+buttonHeight*2,buttonWidth*0.5f,buttonHeight*1.2f),"Complete", WorldSession.player.guildskin.button))
					{
						//	MonoBehaviour.print("GetReward");
						param[0] = npc.guid; // npcGuid
						param[1] = cq.questId; //quest ID
						param[2] = itemChoiceSelected;// ceva
						WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_CHOOSE_REWARD,param);	
						itemChoiceSelected=0;	
						
						WorldPacket pkt2 = new WorldPacket();
						pkt2.SetOpcode(OpCodes.CMSG_QUESTGIVER_STATUS_MULTIPLE_QUERY);
						RealmSocket.outQueue.Add(pkt2);
						
						//						if (WorldSession.player.tutorialInstance)
						//						{
						//							if (WorldSession.player.tutHandler.tutorialStep == 5)
						//							{
						//								WorldSession.player.tutHandler.showHint = false;						
						//							}
						//						}	
						Disable();		
					}
					break;
				}
				
			}
			
			showQuestDetails(quests[selectedQuestPosition]);
		}
		
		if(GUI.Button( new Rect(Screen.width * 0.83f, closeButtonVPos, closeButtonWidth, closeButtonHeight), "Close", WorldSession.player.guildskin.button))
		{
			Disable();
		}
	}
	
	void  NoQuestForYou ()
	{	
		noQuestPopupContainer = new UIAbsoluteLayout();
		noQuestPopupContainer.removeAllChild(true);
		
		noQuestPopup = UIPrime31.myToolkit4.addSprite( "popup_background.png", Screen.width * 0.15f , Screen.height * 0.3f , 0 );
		noQuestPopup.setSize(Screen.width * 0.55f , Screen.height * 0.3f);
		
		closeNoQuestX = UIButton.create( UIPrime31.myToolkit3 , "close_button.png", "close_button.png", 0 , 0 , -1);
		closeNoQuestX.setSize(noQuestPopup.height * 0.25f , noQuestPopup.height * 0.25f);
		closeNoQuestX.position = new Vector3(noQuestPopup.position.x + noQuestPopup.width - closeNoQuestX.width * 0.4f , noQuestPopup.position.y + closeNoQuestX.height * 0.4f , -1 );
		closeNoQuestX.onTouchDown += CloseNoQuestDelegate;
		
		closeNoQuest = UIButton.create(UIPrime31.myToolkit4 , "3_selected.png", "3_unselected.png", 0 , 0 , -1);
		closeNoQuest.setSize(noQuestPopup.width * 0.4f , noQuestPopup.height * 0.25f);
		closeNoQuest.position = new Vector3(noQuestPopup.position.x + noQuestPopup.width/2 - closeNoQuest.width/2 , noQuestPopup.position.y - noQuestPopup.height * 0.6f , -1);
		closeNoQuest.onTouchDown += CloseNoQuestDelegate;
		
		noQuestText = text3.addTextInstance("I have no quest for you!" , noQuestPopup.position.x + noQuestPopup.width/2 , -noQuestPopup.position.y + noQuestPopup.height * 0.3f , 0.6f , -1 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		
		closeNoQuestText = text1.addTextInstance("CLOSE" , closeNoQuest.position.x + closeNoQuest.width/2 , -closeNoQuest.position.y + closeNoQuest.height/2 , 0.5f , -2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		
		noQuestPopupContainer.addChild(noQuestPopup , closeNoQuestX , closeNoQuest);
	}
	
	void  CloseNoQuestDelegate ( UIButton sender )
	{
		DestroyNoQuestInterface();
		Disable();
		WorldSession.interfaceManager.hideInterface = false;
	}	
	
	void  DestroyNoQuestInterface ()
	{
		noQuestPopupContainer.removeAllChild(true);
		noQuestText.clear();
		noQuestText = null;
		closeNoQuestText.clear();
		closeNoQuestText = null;
	}
}
