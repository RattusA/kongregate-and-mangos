﻿using UnityEngine;

public class ReviveAltar
{
	//client variables
	private WorldPacket pkt;
	private NPC npc;	
	private float buttonHeight;
	private float listWidth;
	private float listHeight;
	private float buttonWidth;
	private Vector2 scrollQViewVector;
	private int selected = -1;
	private string[] actions;
	
	UISprite revAltarBackground;
	UISprite revAltarDecor;
	UIButton revAltarExit;
	UIButton revAltarButton;
	UIButton revAltarExitX;
	UIAbsoluteLayout revAltarContainer;
	UITextInstance reviveText;
	UITextInstance reviveTextMessage;
	
	public ReviveAltar ( NPC npc2 )
	{
		buttonHeight = Screen.height*0.06f;
		listWidth  = Screen.width*0.35f;
		listHeight = buttonHeight*(10);
		buttonWidth = listWidth*0.95f;
		npc = npc2;
		Debug.Log("new rev altar "+npc.guid);
	}
	
	void  Disable ()
	{
		npc.menuState = 0;
		WorldSession.interfaceManager.hideInterface = false;
		WorldSession.player.interf.stateManager();
		WorldSession.player.blockTargetChange = false;
	}
	
	public void  Show ()
	{
		npc.canShowReviveAltar = false;
		WorldSession.player.interf.reviveWindow.uiReviveButton.disabled = true;
		revAltarContainer = new UIAbsoluteLayout();
		revAltarContainer.removeAllChild(true);
		
		revAltarBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.225f , Screen.height*0.22f , 3);
		revAltarBackground.setSize(Screen.width*0.55f , Screen.height*0.45f);
		
		revAltarDecor = UIPrime31.questMix.addSprite("ornament1.png" , revAltarBackground.position.x + revAltarBackground.width*0.05f , -revAltarBackground.position.y + revAltarBackground.height*0.05f , 2);
		revAltarDecor.setSize(revAltarBackground.width*0.9f , revAltarBackground.height*0.9f);
		
		revAltarExit = UIButton.create(UIPrime31.questMix , "exit_button.png" , "exit_button.png" , revAltarBackground.position.x + revAltarBackground.width*0.45f , -revAltarBackground.position.y + revAltarBackground.height*0.25f , 1);
		revAltarExit.setSize(revAltarBackground.width*0.4f , revAltarBackground.height*0.19f);
		revAltarExit.onTouchDown += ExitReviveAltar;
		
		revAltarButton = UIButton.create(UIPrime31.questMix , "revive_altar_button.png" , "revive_altar_button.png" , revAltarBackground.position.x + revAltarBackground.width*0.47f , -revAltarBackground.position.y + revAltarBackground.height*0.55f , 1);
		revAltarButton.setSize(revAltarBackground.width*0.36f , revAltarBackground.height*0.37f);
		revAltarButton.onTouchDown += ReviveAltarButtonDelegate;
		
		revAltarExitX = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		revAltarExitX.setSize(revAltarBackground.width*0.09f , revAltarBackground.width*0.09f);
		revAltarExitX.position = new Vector3( revAltarBackground.position.x + revAltarBackground.width - revAltarExitX.width/1.5f , revAltarBackground.position.y + revAltarExitX.height/3 , 1);
		revAltarExitX.onTouchDown += ExitReviveAltar;
		
		revAltarContainer.addChild(revAltarBackground , revAltarDecor , revAltarExit , revAltarButton , revAltarExitX);
	}
	
	void  ShowListOfMenu ()
	{	
		revAltarContainer = new UIAbsoluteLayout();
		revAltarContainer.removeAllChild(true);
		
		revAltarBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.2f , 3);
		revAltarBackground.setSize(Screen.width*0.6f , Screen.height*0.6f);
		
		revAltarDecor = UIPrime31.questMix.addSprite("ornament2.png" , revAltarBackground.position.x + revAltarBackground.width*0.04f , -revAltarBackground.position.y + revAltarBackground.height*0.15f , 2);
		revAltarDecor.setSize(revAltarBackground.width*0.92f , revAltarBackground.height*0.65f);
		
		revAltarExit = UIButton.create(UIPrime31.questMix , "close_button2.png" , "close_button2.png" , revAltarBackground.position.x + revAltarBackground.width*0.05f , -revAltarBackground.position.y + revAltarBackground.height*0.82f , 1);
		revAltarExit.setSize(revAltarBackground.width*0.35f , revAltarBackground.height*0.12f);
		revAltarExit.onTouchDown += CloseReviveAltar;
		
		revAltarButton = UIButton.create(UIPrime31.questMix , "revive_me_button.png" , "revive_me_button.png" , revAltarBackground.position.x + revAltarBackground.width*0.6f , -revAltarBackground.position.y + revAltarBackground.height*0.82f , 1);
		revAltarButton.setSize(revAltarBackground.width*0.35f , revAltarBackground.height*0.12f);
		revAltarButton.onTouchDown += ReviveMeDelegate;
		
		revAltarExitX = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		revAltarExitX.setSize(revAltarBackground.width*0.07f , revAltarBackground.width*0.07f);
		revAltarExitX.position = new Vector3( revAltarBackground.position.x + revAltarBackground.width - revAltarExitX.width/1.5f , revAltarBackground.position.y + revAltarExitX.height/3 , 1);
		revAltarExitX.onTouchDown += CloseReviveAltar;
		
		revAltarContainer.addChild(revAltarBackground , revAltarDecor , revAltarExit , revAltarButton , revAltarExitX);
		
		reviveText = WorldSession.player.interf.text2.addTextInstance("Revive!" , revAltarBackground.position.x + revAltarBackground.width * 0.5f , -revAltarBackground.position.y + revAltarBackground.height*0.1f , UID.TEXT_SIZE , 2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		
		string tempText = "With Altar Revive you loose \n25% durability of all gear you \ncarry, both equipment and in \nthe bags. You also loose 75% \nof your stats for 10 minutes";
		
		reviveTextMessage = WorldSession.player.interf.text3.addTextInstance(tempText , revAltarBackground.position.x + revAltarBackground.width * 0.5f , -revAltarBackground.position.y + revAltarBackground.height*0.47f , UID.TEXT_SIZE*0.8f , 2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		
		/*		GUI.DrawTexture( new Rect(0, Screen.height * 0.05f, Screen.width, Screen.height*0.9f), WorldSession.player.Background);
		GUI.Label( new Rect(Screen.width*0.3f,Screen.height*0.2f, Screen.width * 0.4f, Screen.height * 0.2f), "Revive!", UID.skin.GetStyle("gossip_text"));
		
		GUI.Label( new Rect(Screen.width * 0.2f, Screen.height * 0.35f, Screen.width * 0.6f, Screen.height * 0.3f), "With Altar Revive you loose 25% durability and 75% of stats for 10 minutes.", UID.skin.GetStyle("gossip_text"));
		if(GUI.Button( new Rect(Screen.width * 0.73f, closeButtonVPos, closeButtonWidth, closeButtonHeight),"Revive me", WorldSession.player.guildskin.button))
		{
			RevivePlayer(npc.guid);
			
			Disable();
		}

		if(GUI.Button( new Rect(Screen.width * 0.15f, closeButtonVPos, closeButtonWidth, closeButtonHeight), "Close", WorldSession.player.guildskin.button))
		{
			Disable();
		}
*/		
	}
	
	void  ReviveAltarButtonDelegate ( UIButton sender )
	{
		revAltarContainer.removeAllChild(true);
		ShowListOfMenu();
	}
	
	void  ExitReviveAltar ( UIButton sender )
	{
		revAltarContainer.removeAllChild(true);
		WorldSession.player.interf.reviveWindow.uiReviveButton.disabled = false;
		Disable();
	}
	
	void  CloseReviveAltar ( UIButton sender )
	{
		DestroyReviveWindow();
		ExitReviveAltar(sender);
	}
	
	void  ReviveMeDelegate ( UIButton sender )
	{
		RevivePlayer(npc.guid);
		DestroyReviveWindow();
		WorldSession.player.interf.reviveWindow.uiReviveButton.disabled = false;
		Disable();
	}
	
	void  DestroyReviveWindow ()
	{
		revAltarContainer.removeAllChild(true);
		if(reviveText != null)
		{
			reviveText.clear();
			reviveText = null;
		}
		if(reviveTextMessage != null)
		{
			reviveTextMessage.clear();
			reviveTextMessage = null;
		}
	}
	
	void  RevivePlayer ( System.UInt64 guid )
	{
		WorldPacket pkt= new WorldPacket(OpCodes.CMSG_SPIRIT_HEALER_ACTIVATE, 8);
		pkt.Append(ByteBuffer.Reverse(guid));
		RealmSocket.outQueue.Add(pkt);
	}
}