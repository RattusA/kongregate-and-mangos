using UnityEngine;
using System.Collections.Generic;

public class Blacksmith // entry 2116
{
	private WorldPacket pkt;
	private NPC npc;
	private MessageWnd messageWnd;

	UIText textDark = new UIText (UIPrime31.myToolkit1, "fontiny dark","fontiny dark.png");
	UIText textTitle = new UIText (UIPrime31.myToolkit1, "fontiny title","fontiny title.png");
	UIText textSimple = new UIText (UIPrime31.myToolkit1, "fontiny simple","fontiny simple.png");
	
	byte showType;
	int i;
	Vector2 itemDescVector;
	UIButton tempButton;
	UIAbsoluteLayout equipContainer;
	UIAbsoluteLayout itemEquipContainer;
	
	float equipVPos;
	float equipHPos;
	float slotWidth;
	
	UISprite repairButtonsBackground;
	UIButton repairAllButton;
	UIButton guildRepairAllButton;
	UIButton closeButton;
	UITextInstance currentGold;
	UITextInstance repairCost;
	
	UISprite itemRepairBackground;
	UISprite iconDecoration;
	UIButton guildRepair;
	UIButton repairButton;
	UIButton closeButtonX;
	UIAbsoluteLayout repItemContainer;
	Vector3 tempPos;
	UITextInstance[] itemStats = new UITextInstance[9];
	
	float repairButtonHeight;
	float normalButtonWidth;
	float buttonVPos;
	
	Item repairThis;
	bool  canShow = false;
	
	public Blacksmith ( NPC npc2)
	{
		npc = npc2;
		showType = 0;
		equipVPos = -Screen.height * 0.333f;
		equipHPos = Screen.width * 0.039f;
		slotWidth = Screen.width * 0.078f;
		buttonVPos = Screen.height * 0.85f;
		repairButtonHeight = Screen.height * 0.12f;
		normalButtonWidth = Screen.width * 0.12f;
		messageWnd = new MessageWnd();
		canShow = true;
	}
	
	public void  Enable ()
	{
		if(!canShow)
			return;
		canShow = false;
		foreach(KeyValuePair<int, Bag> bag in WorldSession.player.itemManager.BaseBags)
		{
			bag.Value.PopulateSlots(WorldSession.player.itemManager);
		}
		
		ShowEquipSlots();
		WorldSession.player.inv.isActive = true;
		WorldSession.player.inv.SetDelegate(invRepairDelegate);
		WorldSession.player.inv.ShowBags();
		WorldSession.player.inv.ShowItems();
		repairThis = null;
	}
	
	void  Disable ()
	{
		repairThis = null;
		ClearEquipSlots();
		WorldSession.player.inv.ClearItems();
		WorldSession.player.inv.ClearBags();
		WorldSession.player.inv.isActive = false;
		WorldSession.player.inv.currentBag = 0;
		WorldSession.player.inv.SetDelegate(null);
		npc.menuState = 0;
		WorldSession.interfaceManager.hideInterface = false;
		WorldSession.player.interf.stateManager();
		WorldSession.player.blockTargetChange = false;
	}
	
	public void  Show ()
	{
		if (ButtonOLD.menuState)
		{
			if(currentGold!=null)
			{
				currentGold.clear();
				currentGold = null;
			}
			Disable();
		}
	}
	
	void  repairMessageWindow ( UIButton button )
	{	
		HideItems(true);
		repItemContainer = new UIAbsoluteLayout();
		
		itemRepairBackground = UIPrime31.questMix.addSprite("itm_repair_backgr.png" , Screen.width*0.07f , Screen.height*0.16f , 5);
		itemRepairBackground.setSize(Screen.width*0.7f , Screen.height*0.82f);
		
		guildRepair = UIButton.create(UIPrime31.questMix , "guild_repair.png" , "guild_repair.png" , itemRepairBackground.position.x + itemRepairBackground.width*0.06f , -itemRepairBackground.position.y + itemRepairBackground.height*0.7f , 4);
		guildRepair.setSize(itemRepairBackground.width*0.25f , itemRepairBackground.height*0.17f);
		guildRepair.info = 1;
		guildRepair.onTouchUpInside += RepairDelegate;
		
		repairButton = UIButton.create(UIPrime31.questMix , "repair_button.png" , "repair_button.png" , itemRepairBackground.position.x + itemRepairBackground.width*0.035f , -itemRepairBackground.position.y + itemRepairBackground.height*0.88f , 4);
		repairButton.setSize(itemRepairBackground.width*0.3f , itemRepairBackground.height*0.09f);
		repairButton.info = 0;
		repairButton.onTouchUpInside += RepairDelegate;
		
		closeButtonX = UIButton.create( UIPrime31.myToolkit3 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeButtonX.setSize(itemRepairBackground.height*0.1f , itemRepairBackground.height*0.1f);
		closeButtonX.position = new Vector3( itemRepairBackground.position.x + itemRepairBackground.width - closeButtonX.width/1.5f , itemRepairBackground.position.y + closeButtonX.height/3 , 4);
		closeButtonX.onTouchUpInside += CloseXDelegate;
		
		tempPos = new Vector3(button.position.x , button.position.y , button.position.z);
		tempButton = button;
		tempButton.position = new Vector3( itemRepairBackground.position.x + itemRepairBackground.width*0.03f , itemRepairBackground.position.y - itemRepairBackground.height*0.12f , 4);	
		tempButton.disabled = true;
		tempButton.hidden = false;
		
		iconDecoration = UIPrime31.questMix.addSprite("decoration_case.png" , tempButton.position.x , -tempButton.position.y , 3);
		iconDecoration.setSize(tempButton.width*1.2f , tempButton.height*1.2f);
		
		currentGold.hidden = false;
		currentGold.position += new Vector3(Screen.width*0.1f,
		                                    Screen.height*0.05f,
		                                    0);
		
		itemStats[0] = textSimple.addTextInstance(repairThis.name , itemRepairBackground.position.x + itemRepairBackground.width * 0.12f , -itemRepairBackground.position.y + itemRepairBackground.height*0.01f , UID.TEXT_SIZE*0.8f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		repItemContainer.addChild(itemRepairBackground , guildRepair , repairButton , closeButtonX , iconDecoration);
		
		string stats = string.Empty;
		byte temp = 1;
		while(temp!=0)
		{
			if(repairThis.GetClassName()!="Default")
			{
				if(repairThis.GetSubClassName()=="Default") stats = repairThis.GetClassName();
				else stats = repairThis.GetClassName()+": "+repairThis.GetSubClassName();
				
				itemStats[temp] = textDark.addTextInstance(stats , itemRepairBackground.position.x + itemRepairBackground.width * 0.37f , (temp==1)? itemStats[temp-1].position.y + itemRepairBackground.height*0.11f : itemStats[temp-1].position.y + itemRepairBackground.height*0.074f , UID.TEXT_SIZE*0.75f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
				temp+=1;
			}
			
			if(repairThis.HaveDurability()!="")
			{ 
				stats = "Durability: "+repairThis.HaveDurability();
				itemStats[temp] = textDark.addTextInstance(stats , itemRepairBackground.position.x + itemRepairBackground.width * 0.37f , (temp==1)? itemStats[temp-1].position.y + itemRepairBackground.height*0.11f : itemStats[temp-1].position.y + itemRepairBackground.height*0.074f , UID.TEXT_SIZE*0.75f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
				temp+=1;
			}
			
			if(repairThis.reqLvl > 0)
			{   
				stats = "Level: "+repairThis.reqLvl;
				itemStats[temp] = textDark.addTextInstance(stats , itemRepairBackground.position.x + itemRepairBackground.width * 0.37f , (temp==1)? itemStats[temp-1].position.y + itemRepairBackground.height*0.11f : itemStats[temp-1].position.y + itemRepairBackground.height*0.074f , UID.TEXT_SIZE*0.75f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
				temp+=1;
			}
			
			if(repairThis.classType == ItemClass.ITEM_CLASS_WEAPON)
			{ 	//display weapon damage
				stats = "Damage: "+ repairThis.damage[0].damageMin+" - "+repairThis.damage[0].damageMax;
				itemStats[temp] = textDark.addTextInstance(stats , itemRepairBackground.position.x + itemRepairBackground.width * 0.37f , (temp==1)? itemStats[temp-1].position.y + itemRepairBackground.height*0.11f : itemStats[temp-1].position.y + itemRepairBackground.height*0.074f , UID.TEXT_SIZE*0.75f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
				temp+=1;
				// display weapon speed:
				stats = "Speed: "+ repairThis.delay * 1.0f / 1000 + " attacks/s";
				itemStats[temp] = textDark.addTextInstance(stats , itemRepairBackground.position.x + itemRepairBackground.width * 0.37f , (temp==1)? itemStats[temp-1].position.y + itemRepairBackground.height*0.11f : itemStats[temp-1].position.y + itemRepairBackground.height*0.074f , UID.TEXT_SIZE*0.75f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
				temp+=1;
				// display maximum average weapon damage per second:
				stats = "MaxDPS: "+ Mathf.Round((repairThis.damage[0].damageMin + repairThis.damage[0].damageMax)/ 2 /(repairThis.delay * 1.0f / 1000) *10)/10;
				itemStats[temp] = textDark.addTextInstance(stats , itemRepairBackground.position.x + itemRepairBackground.width * 0.37f , (temp==1)? itemStats[temp-1].position.y + itemRepairBackground.height*0.11f : itemStats[temp-1].position.y + itemRepairBackground.height*0.074f , UID.TEXT_SIZE*0.75f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
				temp+=1;
			}
			else if(repairThis.classType == ItemClass.ITEM_CLASS_ARMOR)
			{
				stats = "Armor: "+repairThis.armour;
				itemStats[temp] = textDark.addTextInstance(stats , itemRepairBackground.position.x + itemRepairBackground.width * 0.37f , (temp==1)? itemStats[temp-1].position.y + itemRepairBackground.height*0.11f : itemStats[temp-1].position.y + itemRepairBackground.height*0.074f , UID.TEXT_SIZE*0.75f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
				temp+=1;
			}
			
			if (repairThis.classType != ItemClass.ITEM_CLASS_QUEST)
			{
				switch(repairThis.quality)
				{
				case ItemQualities.ITEM_QUALITY_POOR:
					stats = "Quality: Inferior";
					break;
				case ItemQualities.ITEM_QUALITY_NORMAL:
					stats = "Quality: Normal";
					break;
				case ItemQualities.ITEM_QUALITY_UNCOMMON:
					stats = "Quality: Rare";
					break;
				case ItemQualities.ITEM_QUALITY_RARE:
					stats = "Quality: Epic";
					break;
				case ItemQualities.ITEM_QUALITY_EPIC:
					stats = "Quality: Legendary";
					break;
				case ItemQualities.ITEM_QUALITY_LEGENDARY:
					stats = "Quality: Mythic";
					break;
				case ItemQualities.ITEM_QUALITY_ARTIFACT:
					stats = "Quality: Artifact";
					break;
				case ItemQualities.ITEM_QUALITY_HEIRLOOM:
					stats = "Quality: Heirloom";
					break;
				}
				itemStats[temp] = textDark.addTextInstance(stats , itemRepairBackground.position.x + itemRepairBackground.width * 0.37f , (temp==1)? itemStats[temp-1].position.y + itemRepairBackground.height*0.11f : itemStats[temp-1].position.y + itemRepairBackground.height*0.074f , UID.TEXT_SIZE*0.75f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
				temp+=1;
			}
			
			stats = "Sell price: " + repairThis.sellPrice;
			itemStats[temp] = textDark.addTextInstance(stats , itemRepairBackground.position.x + itemRepairBackground.width * 0.37f , (temp==1)? itemStats[temp-1].position.y + itemRepairBackground.height*0.11f : itemStats[temp-1].position.y + itemRepairBackground.height*0.074f , UID.TEXT_SIZE*0.75f , 3 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
			temp = 0;
		}
	}
	
	void  ShowEquipSlots ()
	{
		string image;
		equipContainer = new UIAbsoluteLayout();
		itemEquipContainer = new UIAbsoluteLayout();
		int vPos = 0;
		int hPos = 0;
		Item item = null;
		tempButton = UIButton.create(UIPrime31.questMix , "equip_container.png" , "equip_container.png" , 0, 0);
		tempButton.setSize(Screen.width * 0.45f, Screen.height * 0.65f);
		tempButton.position = new Vector3(Screen.width * 0.01f, -Screen.height * 0.17f, 5);
		equipContainer.addChild(tempButton);
		for(int i = 0; i < 19; i++)
		{
			image = "fundal.png";
			WorldSession.player.itemManager.EquippedSlots.TryGetValue(i, out item);
			if(item != null && item.entry > 0)
			{
				image = DefaultIcons.GetItemIcon(item.entry);
			}
			
			if(hPos >= 5)
			{
				hPos = 0;
				vPos++;
			}
			UIToolkit tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
			tempButton = UIButton.create( tempUIToolkit, image, image, 0, 0);
			tempButton.info = i;
			tempButton.setSize(slotWidth, WorldSession.player.inv.itemHeight);
			tempButton.position = new Vector3(equipHPos + hPos * slotWidth, equipVPos - WorldSession.player.inv.itemHeight * vPos, -2);
			if(item != null && item.entry > 0)
			{
				tempButton.onTouchDown += equipRepairDelegate;
			}
			else
			{
				tempButton.onTouchDown += null;
			}
			itemEquipContainer.addChild(tempButton);
			hPos++;
		}
		ShowRepairButtons();
	}
	
	void  ShowRepairButtons ()
	{
		repairButtonsBackground = UIPrime31.questMix.addSprite("buttons_bar.png" , Screen.width*0.01f , Screen.height*0.82f , 5);
		repairButtonsBackground.setSize(Screen.width*0.83f , Screen.height*0.175f);
		
		repairAllButton = UIButton.create(UIPrime31.questMix , "repair_all.png" , "repair_all.png" , repairButtonsBackground.position.x + repairButtonsBackground.width*0.73f , -repairButtonsBackground.position.y + repairButtonsBackground.height*0.12f , 4);
		repairAllButton.setSize(repairButtonsBackground.width*0.25f , repairButtonsBackground.height*0.38f);
		repairAllButton.info = 0;
		repairAllButton.onTouchUpInside += RepairAllDelegate;
		
		guildRepairAllButton = UIButton.create(UIPrime31.questMix , "guild_repair_all.png" , "guild_repair_all.png" , repairButtonsBackground.position.x + repairButtonsBackground.width*0.02f , -repairButtonsBackground.position.y + repairButtonsBackground.height*0.12f , 4);
		guildRepairAllButton.setSize(repairButtonsBackground.width*0.22f , repairButtonsBackground.height*0.8f);
		guildRepairAllButton.info = 1;
		guildRepairAllButton.onTouchUpInside += RepairAllDelegate;
		
		closeButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , repairButtonsBackground.position.x + repairButtonsBackground.width*0.73f , -repairButtonsBackground.position.y + repairButtonsBackground.height*0.54f , 4);
		closeButton.setSize(repairButtonsBackground.width*0.25f , repairButtonsBackground.height*0.38f);
		closeButton.onTouchUpInside += CloseDelegate;
		
		currentGold = textDark.addTextInstance(WorldSession.player.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COINAGE).ToString() ,repairButtonsBackground.position.x + repairButtonsBackground.width*0.5f , -repairButtonsBackground.position.y + repairButtonsBackground.height*0.32f , UID.TEXT_SIZE*0.8f , 3 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		
		equipContainer.addChild(repairButtonsBackground , repairAllButton , guildRepairAllButton , closeButton);
	}
	
	void  ClearEquipSlots ()
	{
		if(equipContainer != null)
		{
			foreach(var sprite in equipContainer._children)
				sprite.destroy();
			equipContainer = null;
		}
		if(itemEquipContainer != null)
		{
			while(itemEquipContainer._children.Count > 0)
			{
				UISprite sprite = itemEquipContainer._children[0];
				UIToolkit tempUIToolkit = sprite.manager;
				itemEquipContainer.removeChild(sprite, false);
				sprite.destroy();
				sprite = null;
				if(tempUIToolkit != null)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
		}
	}
	void  RepairDelegate ( UIButton sender )
	{
		RepairItem(repairThis.guid, sender.info);
		CloseXDelegate(sender);
	}
	void  RepairAllDelegate ( UIButton sender )
	{
		RepairItem(0 , sender.info);
	}
	void  CloseDelegate ( UIButton sender )
	{
		currentGold.clear();
		currentGold = null;
		Disable();
		WorldSession.interfaceManager.hideInterface = false;
	}
	void  CloseXDelegate ( UIButton sender )
	{
		for(int i=0 ; i<9 ; i++)
		{
			if(itemStats[i]!=null)
			{
				itemStats[i].clear();
				itemStats[i] = null;
			}
		}
		repItemContainer.removeAllChild(true);
		tempButton.position = tempPos;
		tempButton.disabled = false;
		currentGold.position -= new Vector3(Screen.width*0.1f,
		                                    Screen.height*0.05f,
		                                    currentGold.position.z);
		HideItems(false);
	}
	private void  RepairItem ( ulong itemGUID, int guildBank )
	{
		pkt = new WorldPacket(OpCodes.CMSG_REPAIR_ITEM, 136);
		pkt.Append(ByteBuffer.Reverse(npc.guid));
		pkt.Append(ByteBuffer.Reverse(itemGUID));
		pkt.Append((byte)guildBank);
		RealmSocket.outQueue.Add(pkt);
		pkt = null;
		
	}
	
	void  invRepairDelegate ( UIButton sender )
	{
		//if(showType == 0)
		//{
		if(WorldSession.player.inv.currentBag == 0)
			repairThis = WorldSession.player.itemManager.BaseBagsSlots[sender.info];
		else
			repairThis = WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(sender.info);
		//	showType = 1;
		repairMessageWindow(sender);
		//}
	}
	
	void  equipRepairDelegate ( UIButton sender )
	{
		//if(showType == 0)
		//{
		repairThis = WorldSession.player.itemManager.EquippedSlots[sender.info];
		repairMessageWindow(sender);
		//	showType = 1;
		//}
	}
	void  HideItems ( bool val )
	{
		equipContainer.hidden = val;
		itemEquipContainer.hidden = val;
		currentGold.hidden = val;
		if (val)WorldSession.player.inv.Hide();
		else WorldSession.player.inv.UnHide();
	}
}