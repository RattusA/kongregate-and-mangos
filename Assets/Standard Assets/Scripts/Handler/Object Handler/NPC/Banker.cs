using UnityEngine;
using System.Collections.Generic;

public class Banker
{
	private NPC npc;
	
	WorldPacket tempPacket;
	
	public uint showType;
	int itemSelected;
	int bagSelected;
	byte numSlots;
	Vector2 itemDescVector;
	UIButton tempButton;
	UISprite bankerBackground;
	UIAbsoluteLayout bankerBagContainer;
	UIAbsoluteLayout bankerContainer;
	
	float bankerVPos;
	float bankerHPos;
	float closeButtonHeight;
	float closeButtonWidth;
	float closeButtonVPos;
	
	float bankBagHeight;
	float bankBagWidth;
	float bankItemHeight;
	float bankItemVPos;
	float bankItemHPos;
	float slotWidth;
	float slotButtonHeight;
	float slotButtonWidth;
	Vector2 slotScrollVector;
	int q;
	
	public bool  isActive = false;
	
	//USER INTERFACE
	private UISprite closeBackFrame;
	private UIButton closeButton;
	private UIButton buySlotButton;
	private UIButton otherButtons;
	private UIAbsoluteLayout buttonsContainer;
	private UIAbsoluteLayout alertContainer;
	private UITextInstance UIalert;
	
	public Item itemObject = null;
	private UIButton selectedItemButton;
	private Vector3 selectedItemPosition;
	private UISprite windowBackground;
	private UISprite windowDecoration;
	private UIButton windowButton;
	private UIAbsoluteLayout popupContainer;
	private UITextInstance[] UITexts;
	private Item[] itemsInCurrentBankBag;
	private UIButton[] itemSlotButton;
	private System.Action<UIButton> functionDelegate;
	
	Vector2 scrollViewItemVector;
	bool  firstDraw = false; //wait to next frame;
	int launchCount = 0; //wait 1 launch

	public Banker ( NPC npc2 )
	{
		npc = npc2;
		bagSelected = 255;
		itemSelected = 0;
		q = 0;
		closeButtonVPos = Screen.height * 0.85f;
		closeButtonHeight = Screen.height * 0.1f;
		slotButtonHeight = Screen.height * 0.11f;
		closeButtonWidth = Screen.width * 0.12f;
		slotButtonWidth = Screen.width * 0.14f;
		
		bankBagWidth = Screen.width * 0.07f;
		bankBagHeight = Screen.height * 0.095f;
		showType = 0;
		popupContainer = new UIAbsoluteLayout();
		UITexts = new UITextInstance[4];
	}
	
	void  Enable ()
	{
		buttonsContainer = new UIAbsoluteLayout();
		alertContainer = new UIAbsoluteLayout();
		itemsInCurrentBankBag = new Item[28];
		itemSlotButton = new UIButton[28];
		bagSelected = 255;
		itemSelected = 0;
		numSlots = getNumSlots();
		npc.menuState = 9;
		
		foreach(KeyValuePair<int, Bag> bag in WorldSession.player.itemManager.BaseBags)
		{
			bag.Value.PopulateSlots(WorldSession.player.itemManager);
		}
		
		WorldSession.player.inv.isActive = true;
		WorldSession.player.inv.SetDelegate(itemDelegate);
		WorldSession.player.inv.ShowBags();
		WorldSession.player.inv.ShowItems();
		WorldSession.player.banker = this;
		WorldSession.player.canShowMenu = false;
		isActive = true;
	}
	
	void  Disable ()
	{
		DestroyPopup();
		WorldSession.player.inv.ClearItems();
		WorldSession.player.inv.ClearBags();
		WorldSession.player.inv.isActive = false;
		WorldSession.player.inv.currentBag = 0;
		WorldSession.player.inv.SetDelegate(null);
		npc.menuState = 0;
		WorldSession.interfaceManager.hideInterface = false;
		WorldSession.player.interf.stateManager();
		WorldSession.player.blockTargetChange = false;
		ClearBankerBags();
		ClearBankerItems();
		bagSelected = 255;
		itemSelected = 0;
		DestroyButtons();
		WorldSession.player.banker = null;
		WorldSession.player.isBankerItemInfo = false;
		WorldSession.player.canShowMenu = true;
		isActive = false;
	}
	
	public void  Show ()
	{
		if (ButtonOLD.menuState)
		{
			Disable();
		}
		switch(showType)
		{
		case 0:
			ClearBankerBags();
			ShowBankerBags();
			ShowBankerItems();
			ShowDefaultButtons();
			break;
		case 1:
			ShowEquipBag();
			break;
		case 3:
			ShowStoreItem();
			ItemInfoWindow();
			break;
		case 4:
			ShowGetItem();
			ItemInfoWindow();
			break;
		case 5:
			ShowUnequipBag();
			break;
		}
	}
	
	void  itemDelegate ( UIButton sender )
	{
		if(WorldSession.player.inv.currentBag == 0)
		{				
			if(WorldSession.player.itemManager.BaseBagsSlots[sender.info].GetTypeID() == TYPEID.TYPEID_CONTAINER)
			{	
				itemSelected = sender.info;
				showType = 1;
			}
			else if(WorldSession.player.itemManager.BaseBagsSlots[sender.info].entry > 0)
			{
				Debug.Log("Not Bag1");
				itemSelected = sender.info;
				if(selectedItemButton != null)
				{
					selectedItemButton.position = selectedItemPosition;
				}
				selectedItemButton = sender;
				itemObject = WorldSession.player.itemManager.BaseBagsSlots[sender.info];
				selectedItemPosition = selectedItemButton.position;
				showType = 3;
			}
		}
		else
		{
			if(WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(sender.info).GetTypeID() == TYPEID.TYPEID_CONTAINER)
			{
				itemSelected = sender.info;
				showType = 1;
			}
			else if(WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(sender.info).entry > 0)
			{
				Debug.Log("Not Bag2");
				itemSelected = sender.info;
				if(selectedItemButton != null)
				{
					selectedItemButton.position = selectedItemPosition;
				}
				selectedItemButton = sender;
				itemObject = WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(sender.info);
				selectedItemPosition = selectedItemButton.position;
				showType = 3;
			}
		}
	}
	
	void  bankBagDelegate ( UIButton sender )
	{
		bagSelected = sender.info;
		showType = 5;
		//		ClearBankerItems();
		ShowBankerItems();
	}
	
	void  bankItemDelegate ( UIButton sender )
	{
		itemSelected = sender.info;
		if(selectedItemButton != null)
		{
			selectedItemButton.position = selectedItemPosition;
		}
		selectedItemButton = sender;
		
		ulong itemGuid = 0;
		if(bagSelected == 255)
		{
			itemGuid = WorldSession.player.GetUInt64Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_BANK_SLOT_1 + (sender.info * 2));
		}
		else
		{
			ulong bagGuid = WorldSession.player.GetUInt64Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_BANKBAG_SLOT_1 + (bagSelected * 2));
			Bag bagExtend = WorldSession.player.itemManager.GetItemByGuid(bagGuid) as Bag;
			
			if(bagExtend != null && bagExtend.entry > 0)
			{
				itemGuid = bagExtend.GetUInt64Value((int)UpdateFields.EContainerFields.CONTAINER_FIELD_SLOT_1 + (sender.info * 2));
			}
		}
		
		Item item = WorldSession.player.itemManager.GetItemByGuid(itemGuid);
		//ItemEntry itm = AppCache.sItems.GetItemEntry(item.entry);
		itemObject = item;
		selectedItemPosition = selectedItemButton.position;
		showType = 4;
	}
	
	uint SetBagSlotPrice ()
	{
		uint returnValue = 0;
		switch(numSlots)
		{
		case 0:
			returnValue = 1000;
			break;
		case 1:
			returnValue = 10000;
			break;
		case 2:
			returnValue = 100000;
			break;
		default:
			returnValue = 250000;
			break;
		}
		return returnValue;
	}
	
	public void  ShowBankerBags ()
	{
		bankerBagContainer = new UIAbsoluteLayout();
		Debug.Log("NUMARUL DE BAG-URI ESTE : " + numSlots);
		string image = string.Empty;
		Item item;
		ulong itemGuid = 0;
		UIToolkit tempUIToolkit;
		for(int i = 0; i < numSlots; i++)
		{
			itemGuid = WorldSession.player.GetUInt64Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_BANKBAG_SLOT_1 + (i * 2));
			Debug.Log("bag guid = " + itemGuid);
			if(itemGuid > 0)
			{
				item = WorldSession.player.itemManager.GetItemByGuid(itemGuid);
				if(item != null && item.entry > 0)
				{
					image = DefaultIcons.GetItemIcon(item.entry);
				}
				if(string.IsNullOrEmpty (image) || image == "noicon.png")
				{
					image="box_1.png";
				}
				tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
				tempButton = UIButton.create( tempUIToolkit, image, image,
				                             Screen.width * 0.025f + bankBagWidth * (i+1),
				                             Screen.height * 0.17f,
				                             0);
				tempButton.setSize(bankBagWidth, bankBagHeight);
				tempButton.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
				tempButton.info = i;
				tempButton.onTouchUpInside += bankBagDelegate;
				bankerBagContainer.addChild(tempButton);
			}
			else
			{
				tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/box_1.png");
				tempButton = UIButton.create( tempUIToolkit, "box_1.png", "box_1.png",
				                             Screen.width * 0.025f + bankBagWidth * (i+1),
				                             Screen.height * 0.17f,
				                             0);
				tempButton.setSize(bankBagWidth, bankBagHeight);
				tempButton.color = new Color(0.2f, 0.2f, 0.2f, 0.5f);
				tempButton.info = i;
				tempButton.onTouchUpInside += null;
				bankerBagContainer.addChild(tempButton);
			}
		}
		
		tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/box_1.png");
		tempButton = UIButton.create( tempUIToolkit, "box_1.png", "box_1.png",
		                             Screen.width * 0.025f,
		                             Screen.height * 0.17f,
		                             0);
		tempButton.setSize(bankBagWidth, bankBagHeight);
		tempButton.info = 255;
		tempButton.onTouchUpInside += bankBagDelegate;
		bankerBagContainer.addChild(tempButton);
		Debug.Log("num of used slots:" + getFirstFreeBankerBagSlot());
	}
	
	public void  ShowBankerItems ()
	{
		string image;
		if(bankerContainer == null)
		{
			bankerContainer = new UIAbsoluteLayout();
		}
		int aux;
		Item item;
		UIToolkit tempUIToolkit;
		uint bagCapacity;
		Bag bagExtend = null;
		
		if(bankerBackground == null)
		{
			bankerBackground = UIPrime31.interfMix.addSprite("chest_container.png", Screen.width*0.03f , Screen.height*0.27f , 5);
			bankerBackground.setSize(Screen.width*0.4f , Screen.height*0.6f);
		}
		
		slotWidth = bankerBackground.width*0.1755f;
		bankItemHeight = bankerBackground.height*0.15f;
		bankItemHPos = bankerBackground.position.x + bankerBackground.width*0.062f;
		bankItemVPos = bankerBackground.position.y - bankerBackground.height*0.056f;
		
		Debug.Log("SHOW BANKER ITEMS WITH BAG " + bagSelected);
		if(bagSelected == 255)
		{
			bagCapacity = 28;
		}
		else
		{
			ulong bagGuid = WorldSession.player.GetUInt64Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_BANKBAG_SLOT_1 + (bagSelected * 2));
			bagExtend = WorldSession.player.itemManager.GetItemByGuid(bagGuid) as Bag;
			bagCapacity = bagExtend.GetUInt32Value((int)UpdateFields.EContainerFields.CONTAINER_FIELD_NUM_SLOTS);
		}
		for(int i = 0; i < 28; i++)
		{
			ulong itemGuid = 0;
			item = null;
			int inx = i;
			if(i < bagCapacity)
			{
				if(bagSelected == 255)
				{
					itemGuid = WorldSession.player.GetUInt64Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_BANK_SLOT_1 + (i * 2));
				}
				else if(bagExtend != null && bagExtend.entry > 0)
				{
					itemGuid = bagExtend.GetUInt64Value((int)UpdateFields.EContainerFields.CONTAINER_FIELD_SLOT_1 + (i * 2));
				}
				
				if(i<25)
				{
					aux = i;
				}
				else
				{
					aux = i+1;
				}
				
				if(itemGuid > 0)
				{
					item = WorldSession.player.itemManager.GetItemByGuid(itemGuid);
				}
				
				//				if( ((itemsInCurrentBankBag[inx] == null) && (item) && (item.entry > 0))
				//					|| ((itemsInCurrentBankBag[inx]) && (item) && (itemsInCurrentBankBag[inx].guid != item.guid))
				//					|| ((itemsInCurrentBankBag[inx]) && ((!item) || (item.entry <= 0)))
				//					|| (!itemSlotButton[inx]))
				//				{
				if(itemSlotButton != null && itemSlotButton[inx] != null)
				{
					tempUIToolkit = itemSlotButton[inx].manager;
					bankerContainer.removeChild(itemSlotButton[inx], false);
					itemSlotButton[inx].destroy();
					itemSlotButton[inx] = null;	
					if(tempUIToolkit != null)
					{
						GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
					}
				}
				
				if(item != null && item.entry > 0)
				{
					image = DefaultIcons.GetItemIcon(item.entry);
					functionDelegate = bankItemDelegate;
					//tempButton.onTouchUpInside += ItemInfoWindow;
				}
				else
				{
					image = "fundal.png";
					functionDelegate = null;
					//tempButton.onTouchUpInside += null;
					itemsInCurrentBankBag[inx] = null;
				}
				
				tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
				itemSlotButton[inx] = UIButton.create( tempUIToolkit, image, image, 0, 0);
				itemSlotButton[inx].info = inx;
				itemSlotButton[inx].setSize(slotWidth, bankItemHeight);
				itemSlotButton[inx].position = new Vector3(bankItemHPos + (aux%5) * slotWidth, bankItemVPos - bankItemHeight * (aux/5), -2);
				if(functionDelegate != null)
				{
					itemSlotButton[inx].onTouchDown += functionDelegate;
				}
				bankerContainer.addChild(itemSlotButton[inx]);
				//				}
				
			}
			else
			{
				ClearItemSlot(inx);
			}
		}
	}
	
	void  ClearItemSlot ( int index )
	{
		UIToolkit tempUIToolkit;
		
		if(itemSlotButton != null && itemSlotButton[index] != null)
		{
			tempUIToolkit = itemSlotButton[index].manager;
			bankerContainer.removeChild(itemSlotButton[index], false);
			itemSlotButton[index].destroy();
			itemSlotButton[index] = null;			
			if(tempUIToolkit != null)
			{
				GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
			}
		}
		itemsInCurrentBankBag[index] = null;
	}
	
	public void  ClearBankerBags ()
	{
		if(bankerBagContainer != null)
		{
			while(bankerBagContainer._children.Count > 0)
			{
				UISprite sprite = bankerBagContainer._children[0];
				UIToolkit tempUIToolkit = sprite.manager;
				bankerBagContainer.removeChild(sprite, false);
				sprite.destroy();
				sprite = null;
				if(tempUIToolkit != null)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
			bankerBagContainer = null;
		}
	}
	
	void  ClearBankerItems ()
	{
		if(bankerContainer != null)
		{
			while(bankerContainer._children.Count > 0)
			{
				UISprite sprite = bankerContainer._children[0];
				UIToolkit tempUIToolkit = sprite.manager;
				bankerContainer.removeChild(sprite, false);
				sprite.destroy();
				sprite = null;
				if(tempUIToolkit != null)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
			bankerContainer = null;
		}
		if(bankerBackground != null)
		{
			bankerBackground.destroy();
			bankerBackground = null;
		}
	}
	
	void  SendBankerActivate ()
	{
		tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_BANKER_ACTIVATE);
		tempPacket.Append(ByteBuffer.Reverse(npc.guid));
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
	
	void  SendBuyBankSlot ()
	{
		tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_BUY_BANK_SLOT);
		tempPacket.Append(ByteBuffer.Reverse(npc.guid));
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
	
	void  SendSwapItem ( byte srcSlot ,   byte srcBag ,   byte dstSlot ,   byte dstBag )
	{
		tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_SWAP_ITEM);
		tempPacket.Append(dstBag);
		tempPacket.Append(dstSlot);
		tempPacket.Append(srcBag);
		tempPacket.Append(srcSlot);
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
	
	void  SendAutostoreBankItem ()
	{
		byte srcBag;
		byte srcSlot;
		if(bagSelected == 255)
		{
			srcBag = 255;
			srcSlot = (byte)(itemSelected + 39);
		}
		else
		{
			srcBag = (byte)(bagSelected + 67);
			srcSlot = (byte)itemSelected;
		}
		tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_AUTOSTORE_BANK_ITEM);
		tempPacket.Append(srcBag);
		tempPacket.Append(srcSlot);
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
	
	void  SendAutobankItem ()
	{
		byte srcBag;
		byte srcSlot;
		
		if(WorldSession.player.inv.currentBag == 0)
		{
			srcBag = 255;
			srcSlot = (byte)(itemSelected + 23);
		}
		else
		{
			Item item = WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(itemSelected);
			srcBag = item.GetSlotByBag();
			srcSlot = item.slot;
		}
		tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_AUTOBANK_ITEM);
		tempPacket.Append(srcBag);
		tempPacket.Append(srcSlot);
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
	
	byte getNumSlots ()
	{
		ByteBuffer bb = new ByteBuffer();
		bb.Append(WorldSession.player.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_BYTES_2));
		bb.Read();
		return bb.Read();
	}
	
	void  UnequipBag ( byte slot )
	{
		byte emptySlot;
		byte destBag;
		
		for(int i = 0; i <= WorldSession.player.itemManager.BaseBags.Count; i++)
		{
			destBag = (byte)((i == 0) ? (0):(i+19));
			emptySlot = WorldSession.player.itemManager.GetFirstEmptySlotInBag(destBag);
			if(emptySlot < 255)
			{
				if(destBag == 0)
				{
					destBag = 255;
					emptySlot = (byte)(emptySlot+23);
				}
				SendSwapItem((byte)(slot + 67), 255, emptySlot, destBag);
				break;
			}
		}
	}
	
	void  ShowDefaultButtons ()
	{
		showType = 6;
		DestroyButtons();
		
		closeBackFrame = UIPrime31.interfMix.addSprite("medium_frame.png" , Screen.width*0.08f , Screen.height*0.89f , 3);
		closeBackFrame.setSize(Screen.width*0.7f , Screen.height*0.1f);
		
		closeButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , closeBackFrame.position.x + closeBackFrame.width*0.03f , -closeBackFrame.position.y + closeBackFrame.height*0.22f , 2);
		closeButton.setSize(closeBackFrame.width*0.19f , closeBackFrame.height*0.6f);
		closeButton.onTouchUpInside += CloseDelegate;
		buttonsContainer.addChild(closeButton);
		
		buySlotButton = UIButton.create(UIPrime31.interfMix , "buy_slot_button.png" , "buy_slot_button.png" , closeBackFrame.position.x + closeBackFrame.width*0.24f , -closeBackFrame.position.y + closeBackFrame.height*0.22f , 2);
		buySlotButton.setSize(closeBackFrame.width*0.23f , closeBackFrame.height*0.6f);
		buySlotButton.onTouchUpInside += BuySlotDelegate;
		buttonsContainer.addChild(buySlotButton);
	}
	
	void  DestroyButtons ()
	{
		if(closeBackFrame != null)
		{
			closeBackFrame.destroy();
			closeBackFrame = null;
		}
		
		if(closeButton != null)
		{
			closeButton.destroy();
			closeButton = null;
		}
		
		if(buySlotButton != null)
		{
			buySlotButton.destroy();
			buySlotButton = null;
		}
		
		buttonsContainer.removeAllChild(true);
	}
	
	void  ShowStoreItem ()
	{
		ShowDefaultButtons();
		
		otherButtons = UIButton.create(UIPrime31.interfMix , "store_item_button.png" , "store_item_button.png" , closeBackFrame.position.x + closeBackFrame.width*0.49f , -closeBackFrame.position.y + closeBackFrame.height*0.22f , 2);
		otherButtons.setSize(buySlotButton.width , buySlotButton.height);
		otherButtons.onTouchDown += StoreItemDelegate;
		//otherButtons.onTouchUpInside += ItemInfoWindow;
		buttonsContainer.addChild(otherButtons);
	}
	
	void  ShowGetItem ()
	{
		ShowDefaultButtons();
		
		otherButtons = UIButton.create(UIPrime31.interfMix , "get_item_button.png" , "get_item_button.png" , closeBackFrame.position.x + closeBackFrame.width*0.49f , -closeBackFrame.position.y + closeBackFrame.height*0.22f , 2);
		otherButtons.setSize(buySlotButton.width , buySlotButton.height);
		otherButtons.onTouchUpInside += GetItemDelegate;
		buttonsContainer.addChild(otherButtons);
	}
	
	void  ShowEquipBag ()
	{
		ShowStoreItem();
		
		otherButtons = UIButton.create(UIPrime31.interfMix , "equip_bag_button.png" , "equip_bag_button.png" , closeBackFrame.position.x + closeBackFrame.width*0.72f , -closeBackFrame.position.y + closeBackFrame.height*0.22f , 2);
		otherButtons.setSize(buySlotButton.width , buySlotButton.height);
		otherButtons.onTouchUpInside += EquipBagDelegate;
		buttonsContainer.addChild(otherButtons);
	}
	
	void  ShowUnequipBag ()
	{
		ShowDefaultButtons();
		
		otherButtons = UIButton.create(UIPrime31.interfMix , "unequip_button.png" , "unequip_button.png" , closeBackFrame.position.x + closeBackFrame.width*0.49f , -closeBackFrame.position.y + closeBackFrame.height*0.22f , 2);
		otherButtons.setSize(buySlotButton.width , buySlotButton.height);
		otherButtons.onTouchUpInside += UnequipBagDelegate;
		buttonsContainer.addChild(otherButtons);
	}
	
	void  StoreItemDelegate ( UIButton sender )
	{
		SendAutobankItem();
		itemSelected = 0;
		ShowDefaultButtons();
		//		showType = 0;
	}
	
	void  GetItemDelegate ( UIButton sender )
	{
		SendAutostoreBankItem();
		ShowDefaultButtons();
		//		showType = 0;
	}
	
	void  EquipBagDelegate ( UIButton sender )
	{
		//		bankerBagContainer.hidden = true;
		//		bankerContainer.hidden = true;
		//		WorldSession.player.inv.Hide();
		//		showType = 2;
		byte freeBagSlot = getFirstFreeBankerBagSlot();
		Debug.Log("First Free Banker Bag Slot = "+freeBagSlot);
		if(freeBagSlot == 255)
		{
			AlertWindow("You have no free slots for bags.");
		}
		else
		{
			if(WorldSession.player.inv.currentBag != 0)
			{
				Item item = WorldSession.player.itemManager.BaseBags[WorldSession.player.inv.currentBag].GetItemFromSlot(itemSelected);
				byte itemSlot = item.slot;
				//				byte bagSlot = WorldSession.player.itemManager.getBagSlotByBagIndex(WorldSession.player.inv.currentBag);
				byte bagSlot= (byte)WorldSession.player.inv.currentBag;
				Debug.Log("scrslot : " + itemSlot + "scrbag : " + bagSlot);
				SendSwapItem(itemSlot, bagSlot, (byte)(freeBagSlot+67), 255);
			}
			else
			{
				
				Debug.Log("scrslot : " + itemSelected);
				SendSwapItem((byte)(itemSelected+23), 255, (byte)(freeBagSlot+67), 255);
			}
		}
		showType = 0;
	}
	
	void  UnequipBagDelegate ( UIButton sender )
	{
		UnequipBag((byte)bagSelected);
		bagSelected = 255;
		itemSelected = 0;
		showType = 0;
	}
	
	void  BuySlotDelegate ( UIButton sender )
	{
		tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_CHARACTER_BANK_NEXT_SLOT_COST);
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
	
	void  CloseDelegate ( UIButton sender )
	{
		Disable();
		WorldSession.interfaceManager.hideInterface = false;
	}
	
	public void  AlertWindow ( string message )
	{
		DestroyAlert();
		
		UISprite alertBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.3f , -5);
		alertBackground.setSize(Screen.width*0.55f , Screen.height*0.45f);
		
		UISprite alertDecoration = UIPrime31.questMix.addSprite("ornament4.png" , alertBackground.position.x + alertBackground.width*0.03f , -alertBackground.position.y + alertBackground.height*0.22f , -6);
		alertDecoration.setSize(alertBackground.width*0.9f , alertBackground.height*0.33f);
		
		UIButton alertButton = UIButton.create(UIPrime31.interfMix , "buy_button.png" , "buy_button.png" , alertBackground.position.x + alertBackground.width*0.1f , -alertBackground.position.y + alertBackground.height*0.72f , -6);
		alertButton.setSize(alertBackground.width*0.3f , alertBackground.height*0.13f);
		alertButton.onTouchUpInside += ReallyBuySlotDelegate;
		alertContainer.addChild(alertButton);
		
		alertButton = UIButton.create(UIPrime31.myToolkit3 , "canceltbutton.png" , "canceltbutton.png" , alertBackground.position.x + alertBackground.width*0.6f , -alertBackground.position.y + alertBackground.height*0.72f , -6);
		alertButton.setSize(alertBackground.width*0.3f , alertBackground.height*0.13f);
		alertButton.onTouchUpInside += CloseAlertDelegate;
		alertContainer.addChild(alertButton);
		
		alertButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		alertButton.setSize(alertBackground.width*0.1f , alertBackground.width*0.1f);
		alertButton.position = new Vector3( alertBackground.position.x + alertBackground.width - alertButton.width/1.5f , alertBackground.position.y + alertButton.height/5 , -6);
		alertButton.onTouchUpInside += CloseAlertDelegate;
		
		alertContainer.addChild(alertBackground , alertDecoration , alertButton);
		
		UIalert = WorldSession.player.interf.text3.addTextInstance(message , alertBackground.position.x + alertBackground.width * 0.6f , -alertBackground.position.y + alertBackground.height*0.25f , UID.TEXT_SIZE*0.7f , -6 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
	}
	
	void  CloseAlertDelegate ( UIButton sender )
	{
		DestroyAlert();
	}
	
	void  ReallyBuySlotDelegate ( UIButton sender )
	{
		SendBuyBankSlot();
		DestroyAlert();
	}
	
	void  DestroyAlert ()
	{
		alertContainer.removeAllChild(true);
		if(UIalert != null)
		{
			UIalert.clear();
			UIalert = null;
		}
	}
	
	private void ItemInfoWindow()//( UIButton sender )
	{
		#if UNITY_EDITOR
		Debug.Log("Banker create popup window with item info ");
		#endif
		DestroyPopup();
		
		windowBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.1f , Screen.height*0.25f , -3);
		windowBackground.setSize(Screen.width*0.7f , Screen.height*0.7f);
		popupContainer.addChild(windowBackground);
		
		selectedItemButton.position = new Vector3( windowBackground.position.x + windowBackground.width*0.03f , windowBackground.position.y - windowBackground.height*0.17f , -4);
		windowDecoration = UIPrime31.questMix.addSprite("decoration_case.png" , selectedItemButton.position.x - selectedItemButton.width*0.03f , -selectedItemButton.position.y - selectedItemButton.height*0.03f , -5);
		windowDecoration.setSize(selectedItemButton.width*1.2f , selectedItemButton.height*1.2f);
		popupContainer.addChild(windowDecoration);
		
		windowDecoration = UIPrime31.interfMix.addSprite("vendor_decoration.png" , windowBackground.position.x + windowBackground.width*0.025f , -windowBackground.position.y + windowBackground.height*0.05f , -4);
		windowDecoration.setSize(windowBackground.width*0.95f , windowBackground.height*0.1f);
		popupContainer.addChild(windowBackground , windowDecoration);
		
		windowDecoration = UIPrime31.interfMix.addSprite("vendor_decoration1.png" , windowBackground.position.x + windowBackground.width*0.015f , -windowBackground.position.y + windowBackground.height*0.25f , -4);
		windowDecoration.setSize(windowBackground.width*0.25f , windowBackground.height*0.45f);
		popupContainer.addChild(windowDecoration);
		
		windowDecoration = UIPrime31.questMix.addSprite("background_case.png" , windowBackground.position.x + windowBackground.width*0.27f , -windowBackground.position.y + windowBackground.height*0.17f , -4);
		windowDecoration.setSize(windowBackground.width*0.7f , windowBackground.height*0.8f);
		popupContainer.addChild(windowDecoration);
		
		windowButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , windowBackground.position.x + windowBackground.width*0.03f , -windowBackground.position.y + windowBackground.height*0.85f , -4);
		windowButton.setSize(windowBackground.width*0.22f , windowBackground.height*0.093f);
		windowButton.onTouchUpInside += CloseItemInfoDelegate;
		popupContainer.addChild(windowButton);
		
		windowButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		windowButton.setSize(windowBackground.width*0.08f , windowBackground.width*0.08f);
		windowButton.position = new Vector3( windowBackground.position.x + windowBackground.width - windowButton.width/1.5f , windowBackground.position.y + windowButton.height/5 , -6);
		windowButton.onTouchUpInside += CloseItemInfoDelegate;
		popupContainer.addChild(windowButton);
		
		UITexts[0] = WorldSession.player.interf.text3.addTextInstance("" + itemObject.name , windowBackground.position.x + windowBackground.width * 0.12f , -windowBackground.position.y + windowBackground.height*0.04f , UID.TEXT_SIZE*0.9f , -4 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		//UITexts[1] = thePlayer.interf.text1.addTextInstance(selectedItem.getClassName() + ": " + selectedItem.getSubClassName() , windowDecoration.position.x + windowDecoration.width * 0.06f , -windowDecoration.position.y + windowDecoration.height*0.05f , UID.TEXT_SIZE*0.8f , -6 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		WorldSession.player.isBankerItemInfo = true;
		launchCount = 0;
	}
	
	public void  DrawItemInfo ()
	{
		if(launchCount < 1)
		{
			launchCount++;
			return;
		}
		float fontSize = WorldSession.player.statsFontSize*1.3f;
		if(dbs._sizeMod != 2)
		{
			fontSize = fontSize * (Screen.width / 1024.0f);
		}
		float rectWidth = windowBackground.width * 0.7f - windowDecoration.width * 0.06f * 2;
		float rectHeigth = windowBackground.height * 0.8f - windowDecoration.height * 0.05f * 2;
		Rect statsRect = new Rect(windowDecoration.position.x + windowDecoration.width * 0.06f,
		                          -windowDecoration.position.y + windowDecoration.height * 0.05f,
		                          rectWidth,
		                          rectHeigth);
		string stats = itemObject.GetItemBasicStatsOutName();
		stats = "<size=" + (int)fontSize + "><color=" + WorldSession.player.mainTextColor + ">" + stats + "</color></size>";
		GUILayout.BeginArea(statsRect);
		scrollViewItemVector = GUILayout.BeginScrollView(scrollViewItemVector);
		GUILayout.Label(stats, WorldSession.player.newskin.GetStyle("DescriptionAndStats"));
		GUILayout.EndScrollView();	
		GUILayout.EndArea();
	}
	
	private void  DestroyPopup ()
	{
		WorldSession.player.isBankerItemInfo = false;
		if(selectedItemButton != null)
			selectedItemButton.position = selectedItemPosition;
		popupContainer.removeAllChild(true);
		//if(tabInfoText) tabInfoText.destroy();
		
		for(int i= 0 ; i < 4 ; i++)
		{
			if(UITexts[i] != null)
			{
				UITexts[i].clear();
				UITexts[i] = null;
			}
		}
	}
	
	private void  CloseItemInfoDelegate ( UIButton sender )
	{
		//itemSelected.position = tempItmPos;
		DestroyPopup();
		//initShow();
	}
	
	private byte getFirstFreeBankerBagSlot ()
	{
		byte freeBagSlot = 255;
		ulong bagGuid;
		for(int i = 0; i < numSlots; i++)
		{
			bagGuid = WorldSession.player.GetUInt64Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_BANKBAG_SLOT_1 + (i * 2));
			if(bagGuid == 0)
			{
				freeBagSlot = (byte)i;
				break;
			}
		}
		return freeBagSlot;
	}
}
