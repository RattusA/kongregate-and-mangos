using UnityEngine;

public class GuildMaster
{
	private NPC npc;
	//private bool  composingGuild = false;
	private string guildComposeField = "";
	
	private MessageWnd messageWnd = new MessageWnd();
	private bool  WindowActive = false;
	private string guildName;
	
	public bool  waitingForConfirmation = false;
	
	bool  canShowGuildMaster = false;
	bool  canShowTextField = false;
	UISprite guildMBackground;
	UIButton guildMCreate;
	UIButton guildMExit;
	UIButton guildMExitX;
	UISprite guildMDecor;
	UIAbsoluteLayout guildMContainer;
	
	UIAbsoluteLayout guildMNamingContainer;
	UISprite textField;
	UITextInstance guildMText;
	
	UIAbsoluteLayout popupContainer;
	UISprite popupBackground;
	UISprite popupDecor;
	UIButton okButton;
	UIButton closePopupX;
	UITextInstance popupText;
	int firstOrSecond = 0;
	
	public GuildMaster ( NPC theNPC )
	{
		npc = theNPC;
		canShowGuildMaster = true;
		canShowTextField = false;
		guildMNamingContainer = new UIAbsoluteLayout();
		guildMContainer = new UIAbsoluteLayout();
		popupContainer = new UIAbsoluteLayout();
	}
	
	void  Disable ()
	{
		npc.menuState = 0;
		WorldSession.interfaceManager.hideInterface = false;
		WorldSession.player.interf.stateManager();
		WorldSession.player.blockTargetChange = false;
	}
	
	public void  Show ()
	{	
		if(ButtonOLD.menuState)
		{
			popupContainer.removeAllChild(true);
			if(popupText!=null)
			{
				popupText.clear();
				popupText = null;
			}
			
			guildMNamingContainer.removeAllChild(true);
			if(guildMText != null)
			{
				guildMText.clear();
				guildMText = null;
			}
			guildMContainer.hidden = false;
			canShowTextField = false;
			
			guildMContainer.removeAllChild(true);
			guildComposeField = "";
			
			Disable();
		}
		
		if(canShowGuildMaster)
		{
			canShowGuildMaster = false;
			guildMContainer.removeAllChild(true);
			
			guildMBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.25f , 3);
			guildMBackground.setSize(Screen.width*0.6f , Screen.height*0.5f);
			
			guildMDecor = UIPrime31.questMix.addSprite("ornament3.png" , guildMBackground.position.x + guildMBackground.width*0.05f , -guildMBackground.position.y + guildMBackground.height*0.05f , 2);
			guildMDecor.setSize(guildMBackground.width*0.9f , guildMBackground.height*0.9f);
			
			guildMCreate = UIButton.create(UIPrime31.questMix , "create_guild.png" , "create_guild.png" , guildMBackground.position.x + guildMBackground.width*0.48f , -guildMBackground.position.y + guildMBackground.height*0.25f , 2);
			guildMCreate.setSize(guildMBackground.width*0.35f , guildMBackground.height*0.35f);
			guildMCreate.onTouchDown += CreateGuildDelegate;
			
			guildMExit = UIButton.create(UIPrime31.questMix , "exit_button.png" , "exit_button.png" , guildMBackground.position.x + guildMBackground.width*0.46f , -guildMBackground.position.y + guildMBackground.height*0.7f , 2);
			guildMExit.setSize(guildMBackground.width*0.39f , guildMBackground.height*0.17f);
			guildMExit.onTouchDown += ExitGuildMDelegate;
			
			guildMExitX = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
			guildMExitX.setSize(guildMBackground.height*0.2f , guildMBackground.height*0.2f);
			guildMExitX.position = new Vector3( guildMBackground.position.x + guildMBackground.width - guildMExitX.width/1.5f , guildMBackground.position.y + guildMExitX.height/3 , 2);
			guildMExitX.onTouchDown += ExitGuildMDelegate;
			
			guildMContainer.addChild(guildMBackground , guildMDecor , guildMCreate , guildMExit , guildMExitX);
		}
		
		if (canShowTextField)
		{
			guildComposeField = GUI.TextField( new Rect(Screen.width * 0.145f, Screen.height * 0.49f,Screen.width * 0.61f,Screen.height * 0.085f), guildComposeField, WorldSession.player.guildskin.textField);
		}
	}
	
	public void  checkIfGuildCreated ()
	{
		//Delay(1000);
		//yield return new WaitForSeconds(0.1f);
		//if ( thePlayer.isInGuild() && thePlayer.guild.Name == guildName )
		waitingForConfirmation = false;
		if ( WorldSession.player.guild.Name == guildName )
		{	
			Debug.Log("!!!!!!!!!!!!!!!GUILD CREATED MESSAGE!!!!!!!!!!!!!!!!");
			ShowGuildMasterPopup("Guild created.");
			WindowActive = true;
			//Debug.Log("GUILD CREATED >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		}
		else
		{
			sendFailedCreateGuildWindow();
		}
		
	}
	
	void  CreateGuildDelegate ( UIButton sender )
	{	
		if(WorldSession.player.IsInGuild())
		{
			ShowGuildMasterPopup("You are already in a guild.");
		}
		else
		{
			guildMContainer.hidden = true;
			InitNamingGuild();
			canShowTextField = true;
		}
	}
	
	void  ExitGuildMDelegate ( UIButton sender )
	{
		guildMContainer.removeAllChild(true);
		guildComposeField = "";
		Disable();
		WorldSession.interfaceManager.hideInterface = false;
	}
	
	public void  sendFailedCreateGuildWindow ()
	{
		waitingForConfirmation = false;
		ShowGuildMasterPopup("Failed to create guild.");
		WindowActive = true;
		//Debug.Log("FAILED TO CREATE GUILD >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
	
	void  InitNamingGuild ()
	{
		guildMNamingContainer.removeAllChild(true);
		
		guildMBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.1f , Screen.height*0.25f , 3);
		guildMBackground.setSize(Screen.width*0.7f , Screen.height*0.5f);
		
		guildMDecor = UIPrime31.questMix.addSprite("ornament4.png" , guildMBackground.position.x + guildMBackground.width*0.05f , -guildMBackground.position.y + guildMBackground.height*0.05f , 2);
		guildMDecor.setSize(guildMBackground.width*0.8f , guildMBackground.height*0.35f);
		
		guildMCreate = UIButton.create(UIPrime31.myToolkit5 , "create.png" , "create.png" , guildMBackground.position.x + guildMBackground.width*0.1f , -guildMBackground.position.y + guildMBackground.height*0.75f , 2);
		guildMCreate.setSize(guildMBackground.width*0.3f , guildMBackground.height*0.18f);
		guildMCreate.onTouchDown += CreateDelegate;
		
		guildMExit = UIButton.create(UIPrime31.questMix , "exit_button.png" , "exit_button.png" , guildMBackground.position.x + guildMBackground.width*0.6f , -guildMBackground.position.y + guildMBackground.height*0.75f , 2);
		guildMExit.setSize(guildMBackground.width*0.3f , guildMBackground.height*0.17f);
		guildMExit.onTouchDown += ExitGuildNamingDelegate;
		
		guildMExitX = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		guildMExitX.setSize(guildMBackground.height*0.17f , guildMBackground.height*0.17f);
		guildMExitX.position = new Vector3( guildMBackground.position.x + guildMBackground.width - guildMExitX.width/1.5f , guildMBackground.position.y + guildMExitX.height/3 , 2);
		guildMExitX.onTouchDown += ExitGuildNamingDelegate;
		
		textField = UIPrime31.questMix.addSprite("long_text_case.png" , guildMBackground.position.x + guildMBackground.width*0.05f , -guildMBackground.position.y + guildMBackground.height*0.45f , 2);
		textField.setSize(guildMBackground.width*0.9f , guildMBackground.height*0.23f);
		
		guildMText = WorldSession.player.interf.text2.addTextInstance("Enter guild name:" , guildMBackground.position.x + guildMBackground.width*0.55f , -guildMBackground.position.y + guildMBackground.height*0.2f  , UID.TEXT_SIZE , 2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		
		guildMNamingContainer.addChild(guildMBackground , guildMDecor , guildMCreate , guildMExit , guildMExitX , textField);
	}
	
	void  CreateDelegate ( UIButton sender )
	{
		if ( guildComposeField != "" )
		{
			waitingForConfirmation = true;
			guildName = guildComposeField;
			WorldSession.player.guildmgr.CreateGuild( guildComposeField );
			WorldSession.player.guildmgr.RequestInfo();
		}
		guildComposeField = "";
		ExitGuildNamingDelegate(sender);
	}
	
	void  ExitGuildNamingDelegate ( UIButton sender )
	{
		guildMNamingContainer.removeAllChild(true);
		if(guildMText != null)
		{
			guildMText.clear();
			guildMText = null;
		}
		guildMContainer.hidden = false;
		canShowTextField = false;
	}
	
	void  ShowGuildMasterPopup ( string message )
	{
		popupContainer.removeAllChild(true);
		if(popupText!=null)
		{	
			popupText.clear();
			popupText = null;
		}
		firstOrSecond = (!guildMContainer.hidden)? 1 : ((!guildMNamingContainer.hidden)? 2 : 0);
		
		guildMContainer.hidden = (firstOrSecond == 1)? true : false;
		guildMNamingContainer.hidden = (firstOrSecond == 2)? true : false;
		
		popupBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.23f , 3);
		popupBackground.setSize(Screen.width*0.6f , Screen.height*0.45f);
		
		popupDecor = UIPrime31.questMix.addSprite("ornament4.png" , popupBackground.position.x + popupBackground.width*0.05f , -popupBackground.position.y + popupBackground.height*0.17f , 2);
		popupDecor.setSize(popupBackground.width*0.9f , popupBackground.width*0.18f);
		
		okButton = UIButton.create(UIPrime31.myToolkit5 , "ok.png" , "ok.png" , popupBackground.position.x + popupBackground.width*0.35f , -popupBackground.position.y + popupBackground.height*0.65f , 2);
		okButton.setSize(popupBackground.width*0.3f , popupBackground.height*0.2f);
		okButton.onTouchDown += PopupCloseDelegate;
		
		closePopupX = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closePopupX.setSize(popupBackground.width*0.09f , popupBackground.width*0.09f);
		closePopupX.position = new Vector3( popupBackground.position.x + popupBackground.width - closePopupX.width/1.5f , popupBackground.position.y + closePopupX.height/3 , 2);
		closePopupX.onTouchDown += PopupCloseDelegate;
		
		popupContainer.addChild(popupBackground , popupDecor , okButton , closePopupX);
		
		if (message.Length>12)
		{	
			int tempNum;
			tempNum = message.IndexOf(" ", 11);
			Debug.Log("message.Length = "+ message.Length +"tempNum = "+ tempNum);
			if(tempNum!=-1)
				popupText = WorldSession.player.interf.text3.addTextInstance(message.Substring(0 , tempNum)+"\n"+message.Substring(tempNum+1 , message.Length-tempNum-1) , popupBackground.position.x + popupBackground.width * 0.55f , -popupBackground.position.y + popupBackground.height*0.08f , UID.TEXT_SIZE*0.9f , 2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
			else 
				popupText = WorldSession.player.interf.text3.addTextInstance(message , popupBackground.position.x + popupBackground.width * 0.55f , -popupBackground.position.y + popupBackground.height*0.15f , UID.TEXT_SIZE*0.9f , 2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		}
		else popupText = WorldSession.player.interf.text3.addTextInstance(message , popupBackground.position.x + popupBackground.width * 0.55f , -popupBackground.position.y + popupBackground.height*0.15f , UID.TEXT_SIZE*0.9f , 2 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		Debug.Log("////////MESAJ2 AFISAT : "+popupText.text);
	}
	
	void  PopupCloseDelegate ( UIButton sender )
	{
		popupContainer.removeAllChild(true);
		if(popupText!=null)
		{
			popupText.clear();
			popupText = null;
		}
		guildMContainer.hidden = (firstOrSecond == 1)? false : true;
		guildMNamingContainer.hidden = (firstOrSecond == 2)? false : true;
	}
}
