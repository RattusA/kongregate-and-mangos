﻿using UnityEngine;
using System.Collections;

public enum GameobjectTypes
{
	GAMEOBJECT_TYPE_DOOR                   = 0,
	GAMEOBJECT_TYPE_BUTTON                 = 1,
	GAMEOBJECT_TYPE_QUESTGIVER             = 2,
	GAMEOBJECT_TYPE_CHEST                  = 3,
	GAMEOBJECT_TYPE_BINDER                 = 4,
	GAMEOBJECT_TYPE_GENERIC                = 5,
	GAMEOBJECT_TYPE_TRAP                   = 6,
	GAMEOBJECT_TYPE_CHAIR                  = 7,
	GAMEOBJECT_TYPE_SPELL_FOCUS            = 8,
	GAMEOBJECT_TYPE_TEXT                   = 9,
	GAMEOBJECT_TYPE_GOOBER                 = 10,
	GAMEOBJECT_TYPE_TRANSPORT              = 11,
	GAMEOBJECT_TYPE_AREADAMAGE             = 12,
	GAMEOBJECT_TYPE_CAMERA                 = 13,
	GAMEOBJECT_TYPE_MAP_OBJECT             = 14,
	GAMEOBJECT_TYPE_MO_TRANSPORT           = 15,
	GAMEOBJECT_TYPE_DUEL_ARBITER           = 16,
	GAMEOBJECT_TYPE_FISHINGNODE            = 17,
	GAMEOBJECT_TYPE_SUMMONING_RITUAL       = 18,
	GAMEOBJECT_TYPE_MAILBOX                = 19,
	GAMEOBJECT_TYPE_AUCTIONHOUSE           = 20,
	GAMEOBJECT_TYPE_GUARDPOST              = 21,
	GAMEOBJECT_TYPE_SPELLCASTER            = 22,
	GAMEOBJECT_TYPE_MEETINGSTONE           = 23,
	GAMEOBJECT_TYPE_FLAGSTAND              = 24,
	GAMEOBJECT_TYPE_FISHINGHOLE            = 25,
	GAMEOBJECT_TYPE_FLAGDROP               = 26,
	GAMEOBJECT_TYPE_MINI_GAME              = 27,
	GAMEOBJECT_TYPE_LOTTERY_KIOSK          = 28,
	GAMEOBJECT_TYPE_CAPTURE_POINT          = 29,
	GAMEOBJECT_TYPE_AURA_GENERATOR         = 30,
	GAMEOBJECT_TYPE_DUNGEON_DIFFICULTY     = 31,
	GAMEOBJECT_TYPE_DO_NOT_USE_YET         = 32,
	GAMEOBJECT_TYPE_DESTRUCTIBLE_BUILDING  = 33,
	GAMEOBJECT_TYPE_GUILD_BANK             = 34,
	GAMEOBJECT_TYPE_TRAPDOOR               = 35,
	MAX_GAMEOBJECT_TYPE                    = 36
};

public class _GameObject:BaseObject, IBaseObject
{
	public Vector3 objOrientation;
	public Vector3 objPosition;
	public string objName;
	public uint entryID;
	public GameobjectTypes gameobjectType;
	
	public _GameObject()
	{		
		objOrientation = Vector3.zero;
		objPosition = Vector3.zero;
	}
	
	public void Start()
	{
		base.Start();
		if(gameobjectType == GameobjectTypes.GAMEOBJECT_TYPE_BUTTON)
		{
			Transform[] allCh = transform.GetComponentsInChildren<Transform>();
			foreach(Transform chd in allCh)
			{
				if(chd.gameObject.name == "portal")
				{
					chd.gameObject.BroadcastMessage("SetPortalGuid", guid);
					break;
				}
			}
		}
	}
	
	public void InitName(string name)
	{
		objName = name;
		gameObject.name = objName;
		//animation.Play("open");
		//............................
	}

	public void Update()
	{
		uint flagValue = GetUInt32Value((int)UpdateFields.EGameObjectFields.GAMEOBJECT_FLAGS);
		uint flag = (uint)UpdateFields.GameObjectFlags.GO_FLAG_IN_USE;	
		if((flagValue & flag) > 0)
		{
			gameObject.SetActive(false);
			MeshCollider collider = gameObject.GetComponent<MeshCollider>();
			if(collider != null)
			{
				collider.enabled = false;
			}
		}
	}
	
	public void CheckLootedFlag()
	{
		uint dynFlag;
		switch(gameobjectType)
		{
		case GameobjectTypes.GAMEOBJECT_TYPE_FISHINGNODE:
			dynFlag =  GetUInt32Value((int)UpdateFields.EGameObjectFields.GAMEOBJECT_BYTES_1);
			Debug.Log ("FISHINGNODE flag GAMEOBJECT_BYTES_1 = "+dynFlag);
			break;
		default:
			dynFlag = GetUInt32Value((int)UpdateFields.EGameObjectFields.GAMEOBJECT_DYNAMIC) & 0xffff;
			uint sparkleFlag = (uint)GameObjectDynamicLowFlags.GO_DYNFLAG_LO_SPARKLE;
			if((dynFlag & sparkleFlag) > 0)
			{
				CreateSparkleEffect();
			}
			else
			{
				DestroySparkleEffect();
			}
			break;
		}
	}


}

public enum HighGuid : ushort
{
	HIGHGUID_ITEM           = 0x4700,                       // blizz 4000 
	HIGHGUID_CONTAINER      = 0x4000,                       // blizz 4000 
	HIGHGUID_PLAYER         = 0x0000,                       // blizz 0000 
	HIGHGUID_GAMEOBJECT     = 0xF110,                       // blizz F110 
	HIGHGUID_TRANSPORT      = 0xF120,                       // blizz F120 (for GAMEOBJECT_TYPE_TRANSPORT) 
	HIGHGUID_UNIT           = 0xF130,                       // blizz F130 
	HIGHGUID_PET            = 0xF140,                       // blizz F140 
	HIGHGUID_VEHICLE        = 0xF150,
	HIGHGUID_DYNAMICOBJECT  = 0xF100,                       // blizz F100 
	HIGHGUID_CORPSE         = 0xF101,                       // blizz F100 
	HIGHGUID_MO_TRANSPORT   = 0x1FC0,                       // blizz 1FC0 (for GAMEOBJECT_TYPE_MO_TRANSPORT) 
};

public class ObjectInfo
{
	public static ushort GUID_HIPART(ulong x)
	{
		byte shift = 48;
		return (ushort)(x >> shift);
	}
	
	public static TYPEID GetTypeIdByGuid(ulong guid)
	{
		TYPEID returnValue = TYPEID.TYPEID_OBJECT;
		ushort hiGuid = GUID_HIPART(guid);

		switch(hiGuid)
		{
		case (ushort)HighGuid.HIGHGUID_PLAYER:
			returnValue = TYPEID.TYPEID_PLAYER;
			break;
		case (ushort)HighGuid.HIGHGUID_CONTAINER:
			returnValue = TYPEID.TYPEID_CONTAINER;
			break;
		case (ushort)HighGuid.HIGHGUID_CORPSE:
			returnValue = TYPEID.TYPEID_CORPSE;
			break;
		case (ushort)HighGuid.HIGHGUID_ITEM: // == HIGHGUID_CONTAINER
			returnValue = TYPEID.TYPEID_ITEM;
			break;
		case (ushort)HighGuid.HIGHGUID_DYNAMICOBJECT:
			returnValue = TYPEID.TYPEID_DYNAMICOBJECT;
			break;
		case (ushort)HighGuid.HIGHGUID_GAMEOBJECT:
		case (ushort)HighGuid.HIGHGUID_TRANSPORT: // not sure
			returnValue = TYPEID.TYPEID_GAMEOBJECT;
			break;
		case (ushort)HighGuid.HIGHGUID_UNIT:
			returnValue = TYPEID.TYPEID_UNIT;
			break;
		}
		return returnValue;
	}
}