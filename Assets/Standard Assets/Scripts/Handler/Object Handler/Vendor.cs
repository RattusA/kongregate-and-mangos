﻿using System.Collections.Generic;

public class Vendor
{
	private NPC npc;
	
	WorldPacket tempPacket;
	public VendorUI vendorUI;
	public List<Item> vendorItems;
	public List<VendorItem> vendorItemsExtra;
	
	public Vendor ( NPC npc2 )
	{
		npc = npc2;
		vendorItems = null;
		vendorItemsExtra = null;
		vendorUI = new VendorUI(npc2);
	}
	
	public void  ReplaceItem ( Item theItem )
	{
		for(var q= 0; q < vendorItems.Count; q++)
		{
			if(vendorItems[q].entry == theItem.entry)
			{
				theItem.GetItemBasicStats();
				vendorItems[q] = null;
				vendorItems[q] = theItem;
				return;
			}
		}
	}
	
	public void  SendSellItem ( ulong itemGuid ,   uint count )
	{
		tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_SELL_ITEM);
		tempPacket.Append(ByteBuffer.Reverse(npc.guid));
		tempPacket.Append(ByteBuffer.Reverse(itemGuid));
		tempPacket.Append(ByteBuffer.Reverse(count));
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
	
	public void  SendBuyItem ( uint itemEntry ,   uint slot ,   uint count )
	{
		byte unk8 = 0;
		tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_BUY_ITEM);
		tempPacket.Append(ByteBuffer.Reverse(npc.guid));
		tempPacket.Append(ByteBuffer.Reverse(itemEntry));
		tempPacket.Append(ByteBuffer.Reverse(slot));
		tempPacket.Append(ByteBuffer.Reverse(count));
		tempPacket.Append(unk8);
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
	
	public void  SendListInventory ()
	{
		tempPacket = new WorldPacket();
		tempPacket.SetOpcode(OpCodes.CMSG_LIST_INVENTORY);
		tempPacket.Append(ByteBuffer.Reverse(npc.guid));
		RealmSocket.outQueue.Add(tempPacket);
		tempPacket = null;
	}
}