﻿//using UnityEngine;
//using System.Collections;
//
//public class _OtherPlayer : Player {
//
//	bool fw = false;
//	bool left = false;
//	bool right = false;
//	float rotationSpeed = 20;
//	Vector3 nextPosition;
//	bool isSpwned = true;
//	
//	public void Awake()
//	{
//		base.Awake();
//	}
//	
//	public void Start()
//	{
//		base.Start();
//		//It is necessary to reduce the number of calls to update the character models
//		if(isSpwned && CheckEquipmentAfterSpawn())
//		{
//			isSpwned = false;
//		}
//		
//		if(Application.loadedLevelName == "SelectCharacterWindow")
//		{
//			enabled = false;
//			parentRB.useGravity = false;
//			return;
//		}
//		
//		UpdateStats();
//		if(HP == 0)
//		{
//			m_deathState = DeathState.DEAD;
//		}
//
//		//update player customizations
//		ByteBuffer _bytes = new ByteBuffer();
//		_bytes.Append(GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_BYTES));
//		
//		skinColor  = _bytes._storage[3];
//		face       = _bytes._storage[2];
//		hairStyle  = _bytes._storage[1];
//		hairColor  = _bytes._storage[0];
//		
//		_bytes = new ByteBuffer();
//		_bytes.Append(GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_BYTES_2));	    
//		facialHair = _bytes._storage[3];
//	}
//	
//	public void SetNextPosition(Vector3 pos)
//	{
//		nextPosition = pos;
//		transformParent.LookAt(nextPosition);
//	}
//	
//	public void SetPosition(Vector3 pos, float o)
//	{
//		base.SetPosition(pos, o);
//	}
//	
//	public void SetOrientation(float o)
//	{
//		transformParent.rotation.eulerAngles.y = (Mathf.PI * 0.5f - o) * 180.0 / Mathf.PI;
//	}
//	
//	public void StartRotateLeft()
//	{
//		left = true;
//		right = false;
//	}
//	
//	public void StartRotateRight()
//	{
//		left = false;
//		right = true;
//	}
//	
//	public void StopRotate()
//	{
//		left = right = false;
//	}
//	
//	public void StartMoveForWard()
//	{
//		if(isRooted)
//		{
//			return;
//		}
//
//		uint flag = (uint)MovementFlags.MOVEMENTFLAG_FORWARD;
//		moveFlag = moveFlag | flag;
//		flag = (uint)MovementFlags.MOVEMENTFLAG_BACKWARD;
//		moveFlag = moveFlag & ~flag;
//	}
//	
//	public void stopMove()
//	{
//		uint flag = (uint)MovementFlags.MOVEMENTFLAG_FORWARD | (uint)MovementFlags.MOVEMENTFLAG_BACKWARD;
//		if((moveFlag & flag) == 0)
//		{
//			return;
//		}
//		flag = (uint)MovementFlags.MOVEMENTFLAG_FORWARD | (uint)MovementFlags.MOVEMENTFLAG_BACKWARD | (uint)MovementFlags.MOVEMENTFLAG_WALK_MODE;
//		moveFlag = moveFlag & ~flag;
//	}
//	
//	public void stopStrafe()
//	{
//		uint flag = ((uint)MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT | (uint)MovementFlags.MOVEMENTFLAG_STRAFE_LEFT);
//		if(!(moveFlag & flag))
//		{
//			return;
//		}
//		moveFlag = moveFlag & ~flag;
//	}
//	
//	public void startMoveBackward()
//	{
//		if(isRooted)
//		{
//			return;
//		}
//
//		uint flag = ((uint)MovementFlags.MOVEMENTFLAG_BACKWARD | (uint)MovementFlags.MOVEMENTFLAG_WALK_MODE);
//		moveFlag = moveFlag | flag ;
//		flag = (uint)MovementFlags.MOVEMENTFLAG_FORWARD;
//		flag = ~flag;
//		moveFlag = moveFlag & flag; 
//	}
//	public void startMoveStrafeLeft()
//	{
//		if(isRooted)
//		{
//			return;
//		}
//
//		uint flag = (uint)MovementFlags.MOVEMENTFLAG_STRAFE_LEFT; 
//		moveFlag = moveFlag | flag ;
//		flag = (uint)MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT;
//		flag = ~flag;
//		moveFlag = moveFlag & flag; 
//	}
//	public void startMoveStrafeRight()
//	{
//		if(isRooted)
//		{
//			return;
//		}
//		uint flag = (uint)MovementFlags.MOVEMENTFLAG_STRAFE_RIGHT; 
//		moveFlag = moveFlag | flag ;
//		flag = (uint)MovementFlags.MOVEMENTFLAG_STRAFE_LEFT;
//		flag = ~flag;
//		moveFlag = moveFlag & flag; 
//	}
//
//	public void Update()
//	{
//		if(moveFlag == 0)
//		{
//			if(!botFlag)
//			{
//				parentRB.velocity.x = parentRB.velocity.z = 0;
//			}
//		}
//	}
//	
//	public void UpdateStats()
//	{
//		base.UpdateStats();
//		
//		if(HP > 1)
//		{
//			m_deathState = DeathState.ALIVE;
//			return;
//		}
//		
//		if(HP == 0 && m_deathState == DeathState.ALIVE)
//		{
//			m_deathState = DeathState.JUST_DIED;
//			return;
//		}
//	}
//	
//	public void FixedUpdate()
//	{
//		if(armory.IsAllLoaded())
//		{
//			armory.EnableAllParts();
//		}
//
//		base.FixedUpdate();
//
//		if(left)
//		{
//			transformParent.Rotate(0, -rotationSpeed * Time.deltaTime, 0);
//		}
//		if(right)
//		{
//			transformParent.Rotate(0, rotationSpeed * Time.deltaTime, 0);
//		}
//	}
//	
//	public void LateUpdate()
//	{
//		if(isSpwned && CheckEquipmentAfterSpawn())
//		{
//			isSpwned = false;
//			armory.InitArmory(0);
//			armory.UpdateArmory(0);
//		}
//
//		foreach(Aura aura in auraManager.auras)
//		{
//			auraManager.UpdateAura(aura);
//		}
//	}
//	
//	public bool CheckEquipmentAfterSpawn()
//	{
//		bool ret = true;
//		uint itemID = 0;
//		for(uint slot = (uint)EquipmentSlots.EQUIPMENT_SLOT_HEAD; slot < (uint)EquipmentSlots.EQUIPMENT_SLOT_END; slot++)
//		{
//			itemID = GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_1_ENTRYID + 2 * (uint)slot);
//			if(!AppCache.sItems.HaveRecord(itemID) && itemID > 0)
//			{
//				ret = false;
//				break;
//			}
//		}
//		return ret;
//	}
//}
