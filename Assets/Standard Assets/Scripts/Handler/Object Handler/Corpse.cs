using UnityEngine;
using System;

public enum CorpseType
{
	CORPSE_BONES				=	0,
	CORPSE_RESURRECTABLE_PVE	=	1,
	CORPSE_RESURRECTABLE_PVP	=	2	
};

public enum CorpseFlags
{
	CORPSE_FLAG_NONE        = 0x00,
	CORPSE_FLAG_BONES       = 0x01,
	CORPSE_FLAG_UNK1        = 0x02,
	CORPSE_FLAG_UNK2        = 0x04,
	CORPSE_FLAG_HIDE_HELM   = 0x08,
	CORPSE_FLAG_HIDE_CLOAK  = 0x10,
	CORPSE_FLAG_LOOTABLE    = 0x20
};

public class  Corpse : BaseObject
{
	const int CORPSE_RECLAIM_RADIUS = 39;
	
	uint entryID;
	CorpseType type;
	
	string objName;
	Vector3 objOrientation;
	Vector3 objPosition;
	GameObject gameobject;
	
	bool  belongToMainPlayer = false;
	
	public override void Start ()
	{
		base.Start();
		transform.parent.localRotation = Quaternion.identity;//hardcoded
		gameobject = gameObject;
	}
	
	void  initName (string name){
		objName = name;
		gameobject.name = objName;
	}
	
	/*
	    OBJECT_FIELD_GUID                         = 0x0000, // Size: 2, Type: LONG, Flags: PUBLIC
    OBJECT_FIELD_TYPE                         = 0x0002, // Size: 1, Type: INT, Flags: PUBLIC
    OBJECT_FIELD_ENTRY                        = 0x0003, // Size: 1, Type: INT, Flags: PUBLIC
    OBJECT_FIELD_SCALE_X                      = 0x0004, // Size: 1, Type: FLOAT, Flags: PUBLIC
    OBJECT_FIELD_PADDING                      = 0x0005, // Size: 1, Type: INT, Flags: NONEOBJECT_UPDATE_TYPE
    
     CORPSE_FIELD_OWNER                        = EObjectFields.OBJECT_END + 0x0000, // Size: 2, Type: LONG, Flags: PUBLIC
    CORPSE_FIELD_PARTY                        = EObjectFields.OBJECT_END + 0x0002, // Size: 2, Type: LONG, Flags: PUBLIC
    CORPSE_FIELD_DISPLAY_ID                   = EObjectFields.OBJECT_END + 0x0004, // Size: 1, Type: INT, Flags: PUBLIC
    CORPSE_FIELD_ITEM                         = EObjectFields.OBJECT_END + 0x0005, // Size: 19, Type: INT, Flags: PUBLIC
    CORPSE_FIELD_BYTES_1                      = EObjectFields.OBJECT_END + 0x0018, // Size: 1, Type: BYTES, Flags: PUBLIC
    CORPSE_FIELD_BYTES_2                      = EObjectFields.OBJECT_END + 0x0019, // Size: 1, Type: BYTES, Flags: PUBLIC
    CORPSE_FIELD_GUILD                        = EObjectFields.OBJECT_END + 0x001A, // Size: 1, Type: INT, Flags: PUBLIC
    CORPSE_FIELD_FLAGS                        = EObjectFields.OBJECT_END + 0x001B, // Size: 1, Type: INT, Flags: PUBLIC
    CORPSE_FIELD_DYNAMIC_FLAGS                = EObjectFields.OBJECT_END + 0x001C, // Size: 1, Type: INT, Flags: DYNAMIC
    CORPSE_FIELD_PAD                          = EObjectFields.OBJECT_END + 0x001D, // Size: 1, Type: INT, Flags: NONE
	*/
	
	bool BelongTo ( ulong _guid )
	{
		return (_guid == GetUInt64Value((int)UpdateFields.ECorpseFields.CORPSE_FIELD_OWNER) ? true : false);
	} 
	
	public void  AssebleCorpse ()
	{
		Debug.Log("type "+_type);
		
		uint flags = GetUInt32Value((int)33);
		Debug.Log("Flags "+ flags);
		Animation animation;
		if((flags & (uint)CorpseFlags.CORPSE_FLAG_BONES) > 0)//player
		{
			try{
				ByteBuffer _bytes = new ByteBuffer();
				_bytes.Append(GetUInt32Value((int)30));
				uint race = _bytes._storage[2];
				uint gender = _bytes._storage[1];
				Debug.Log("race: "+race+"  gender: "+gender+"  skin:"+_bytes._storage[0]);
				_bytes = null;	
				
				GameObject player2 = Player.LoadPlayerModel((Races)race, (Gender)gender);
				OtherPlayer playerScript = player2.GetComponentInChildren<OtherPlayer>() as OtherPlayer;
				
				GameObject player = playerScript.gameObject;
				player.transform.parent = transform;
				player.transform.localPosition = Vector3.zero;
				
				DestroyImmediate(playerScript);
				DestroyImmediate(player2);
				
				animation = player.GetComponent<Animation>();
				animation.Play("dead");
				
//				string playerRaceName = dbs.sCharacterRace.GetRecord(race).Name;
//				string playerGenderName  = dbs.sCharacterGender.GetRecord(gender).Name;
//				string mPath = "prefabs/chars/"+ playerRaceName +"_"+playerGenderName +"/";
//				uint displayId;
//				uint inventoryType;
//				string itemName = "";
//				for(int slot = (int)EquipmentSlots.EQUIPMENT_SLOT_HEAD; slot <= (int)EquipmentSlots.EQUIPMENT_SLOT_HANDS; ++slot)
//				{
//					displayId = ((_uint32values[11+slot]<<8)>>8);
//					inventoryType = (_uint32values[11+slot]>>24);
//					
//				}
				
				//Init character face mesh and adding it to LODManager Models array	
				/*int LODDetail = 0 ;
			System.Collections.Generic.List. components<Component> = null;
			Transform bo;	
			FIXME_VAR_TYPE comp= gameObject.GetComponentsInChildren<Transform>();
			string defaultMeshesName  = "Default_Face"+"  "+"Default_Arm"+"  "+"Default_Belt"+"  "+"Default_Chest"+"  "+"Default_Head";
			for(int i = 0; i< comp.Length; i++)
			{
				Transform ob = comp[i] as Transform;
				if(defaultMeshesName.Contains(ob.gameObject.name))
				{
					components = new System.Collections.Generic.List.<Component>();
					components.AddRange(ob.gameObject.GetComponentsInChildren<Transform>());
					components.Sort(new CompareByName());
					for(int j = 1; j<components.Count; j++)
					{
						bo = components[j] as Transform;
						if(j == LODDetail+1)
							bo.gameObject.active = true;sss
						else
							bo.gameObject.active = false;
					}
				}
			}*/
				
			}catch( Exception exception )
			{
				Debug.Log(exception);
				GameObject player = UnityEngine.Object.Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/objects/cub"));
				player.transform.parent = transform;
				player.transform.localPosition = Vector3.zero;
			}
		}
		else
		{
			GameObject player = UnityEngine.Object.Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/objects/bones"));
			player.transform.parent = transform;
			player.transform.localPosition = new Vector3(0, 0.6f, 0);
			//corections for bones prefab
			animation = player.GetComponent<Animation>();
			AnimationState currMountAnimation = animation.CrossFadeQueued("idle", 0.3f, QueueMode.PlayNow);
			currMountAnimation.speed = 0.35f;
			
		}
		Debug.Log("owner "+ GetUInt64Value(6));
		Debug.Log("displayid"+GetUInt32Value((int)10));
		
		
		
		
		if(BelongTo(MainPlayer.GUID))
		{
			belongToMainPlayer = true;
		}
	}
	
	void  Update ()
	{
		if(belongToMainPlayer == false)
			return;
		
		if(WorldSession.player.m_deathState != DeathState.DEAD)	
			return;
		
		if((Vector3.Distance(TransformParent.position, WorldSession.player.TransformParent.position) - CORPSE_RECLAIM_RADIUS) < 0.001f )
		{
			WorldSession.player.interf.reviveWindow.hideRevive = false;
		} 
		else
		{
			WorldSession.player.interf.reviveWindow.hideGhost = false;
		}
	}
	
}