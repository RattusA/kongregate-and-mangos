﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SlotType {

	SLOTTYPE_EQUIPMENT,
	SLOTTYPE_EQUIPMENT_BAGS,
	SLOTTYPE_MAIN_PLAYER_INVENTORY,
	SLOTTYPE_PLAYER_INVENTORY,
	SLOTTYPE_BANK_BAGS,
	SLOTTYPE_MAIN_BANK_INVENTORY,
	SLOTTYPE_BANK_INVENTORY,
	NONE,
};

public class ItemStat
{
	public ItemModType itemStatType;
	public int itemStatValue;
}

public class ItemDamage
{
	public float damageMin;
	public float damageMax;
	public uint damageType;
}

public class ItemSpell
{
	public uint ID;
	public uint trigger;
	public uint charges; 
	public float PPMRate;
	public uint cooldown;
	public int category;
	public int categoryCooldown;
}

public class ItemSocket
{
	public uint color;
	public uint content;
}

public class Item : BaseItem {

	protected const int MAX_ENCHANTMENT_OFFSET = 3;

	public uint entry;
	public ItemClass classType;
	public int subClass;
	public int unk32;
	public string name;
	public uint displayInfoID;
	public InventoryType inventoryType;
	public uint containerSlots;
	public ItemQualities quality;
	public uint flags;
	public uint flags2;
	public uint buyPrice;
	public uint sellPrice;
	
	public uint allowableClass;
	public uint allowableRace;
	public uint itemLvl;
	public uint reqLvl;
	public uint reqSkill;
	public uint armour;
	public uint reqSkillRank;
	public uint reqSpell;
	public uint reqHonorRank;
	public uint reqCityRank;
	public uint reqRepFaction;
	public uint reqRepRank;
	public uint count;
	public int maxCount;
	public int stackable;
	
	public uint statsCount;
	public ItemStat[] itemStats = new ItemStat[10];
	public ItemDamage[] damage = new ItemDamage[2];
	public uint scallingStatDistribution;
	public uint scalingStatValue;

	public uint holyRes;
	public uint fireRes;
	public uint natureRes;
	public uint frostRes;
	public uint shadowRes;
	public uint arcaneRes;
	public uint delay;
	public uint ammoType;
	public float rangedModRange;
	public ItemSpell[] spells = new ItemSpell[5];
	public uint bonding;
	public string description;
	public uint pageText;
	public uint languageID;
	public uint pageMaterial;
	public uint startQuest;
	public uint lockID;
	public int material;
	public uint sheath;
	public uint randomProperty;
	public uint randomSuffix;
	public uint block;
	public uint itemSet;
	public uint durability;
	public uint maxDurability;
	public uint area;
	public uint map;
	public uint bagFamily;
	public uint totemCategory;
	public ItemSocket[] socket = new ItemSocket[3];
	public uint socketBonus;
	public uint gemProperties;
	public int reqDisenchantSkill;
	public float armourDamageModifier;
	public uint duration;
	public uint itemLimitCategory;
	public uint holidayId;
	public uint foodType;
	public uint minMoneyLoot;
	public uint maxMoneyLoot;
	public uint extraFlags;

	public uint charges;

	public byte bag;
	public byte slot;
	public SlotType slotType = SlotType.NONE;

	public string stats = "";
	public bool isInContainer = false;

	public GemSlotManager gemSlotManager;

	public bool isPermanentEnchant = false;
	public bool isTemporaryEnchant = false;
	public bool isPrismaticSlotEnchant = false;

	public Item() : base()
	{
		_type = TYPEID.TYPEID_ITEM;
		_typeMask = TYPE.TYPE_ITEM;
		_valuescount = 0x40;
		this.entry = 0;
		InitValues();
		stats = "";
		gemSlotManager = new GemSlotManager(this);

		for(int i = 0; i < 3; i++)
		{
			socket[i] = new ItemSocket();
		}
	}

	public Item(uint entry) : this()
	{
		this.entry = entry;
	}
	
	public Item(ItemEntry itemEntry) : this()
	{
		this.entry = itemEntry.ID;
		this.classType = (ItemClass)itemEntry.Class;
		this.subClass = itemEntry.SubClass;
		this.displayInfoID = itemEntry.DisplayID;
		this.inventoryType = (InventoryType)itemEntry.InventorySlot;
		this.name = itemEntry.Name;
	}
	
	public void SetSlot(byte newBagSlot, byte newItemSlot)
	{
		bag = newBagSlot;
		slot = newItemSlot;
	}

	public int GetBagBySlot()
	{
		int ret = 255;
		switch(slotType)
		{
		case SlotType.SLOTTYPE_EQUIPMENT_BAGS:
			ret = slot - (int)InventorySlots.INVENTORY_SLOT_BAG_START + 1;
			break;
		case SlotType.SLOTTYPE_BANK_BAGS:
			ret = slot - (int)BankBagSlots.BANK_SLOT_BAG_START + 1;
			break;
		}
		return ret;
	}

	public byte GetSlotByBag()
	{
		int ret = bag;
		if(bag != 255)
		{
			switch(slotType)
			{
			case SlotType.SLOTTYPE_PLAYER_INVENTORY:
				ret = bag + (int)InventorySlots.INVENTORY_SLOT_BAG_START - 1;
				break;
			case SlotType.SLOTTYPE_BANK_INVENTORY:
				ret = bag + (int)BankBagSlots.BANK_SLOT_BAG_START - 1;
				break;
			}
		}
		return (byte)ret;
	}

	public byte GetSlotByInventorySlot()
	{
		int ret = slot;
		if(slot != 255)
		{
			switch(slotType)
			{
			case SlotType.SLOTTYPE_MAIN_PLAYER_INVENTORY:
				ret = slot + (int)InventoryPackSlots.INVENTORY_SLOT_ITEM_START;
				break;
			case SlotType.SLOTTYPE_MAIN_BANK_INVENTORY:
				ret = slot + (int)BankItemSlots.BANK_SLOT_ITEM_START;
				break;
			}
		}
		return (byte)ret;
	}

	public int GetInventorySlot()
	{
		int ret = slot;
		if(slot >= (int)InventoryPackSlots.INVENTORY_SLOT_ITEM_START && 
		   slot < (int)InventoryPackSlots.INVENTORY_SLOT_ITEM_END)
		{
			ret = slot - (int)InventoryPackSlots.INVENTORY_SLOT_ITEM_START;
		}
		else if(slot >= (int)BankItemSlots.BANK_SLOT_ITEM_START && 
		        slot < (int)BankItemSlots.BANK_SLOT_ITEM_END)
		{
			ret = slot - (int)BankItemSlots.BANK_SLOT_ITEM_START;
		}
		return ret;
	}
	
	public void UseItem()
	{
		byte cast_count = 0, unkFlags = 0;
		uint spellID = 0;
		int guidFieldIndex = (int)UpdateFields.EObjectFields.OBJECT_FIELD_GUID;
		ulong itemGuid = GetUInt64Value(guidFieldIndex);
		uint glyphIndex = 0;

		SpellCastTargets targets = new SpellCastTargets();
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_USE_ITEM);
		pkt.Append(GetSlotByBag());
		pkt.Append(slot);		
		pkt.Append(cast_count);
		pkt.Add(spellID);
		pkt.Add(itemGuid);						//get item guid
		pkt.Add(glyphIndex);
		pkt.Append(unkFlags);
		pkt.Append(targets.Write(itemGuid));
		
		RealmSocket.outQueue.Add(pkt);
	}

	public bool IsEquiped()
	{
		bool ret = false;
		switch(slotType)
		{
		case SlotType.SLOTTYPE_EQUIPMENT:
		case SlotType.SLOTTYPE_BANK_BAGS:
		case SlotType.SLOTTYPE_EQUIPMENT_BAGS:
			ret = true;
			break;
		}
		return ret;
	}

	public void EquipItem()
	{
		if(IsEquiped())
		{
			return;
		}	

		switch(classType)
		{
		case ItemClass.ITEM_CLASS_CONTAINER:
			if(GetValuesCount() == 0x8A)
			{
				SendAutoEquipPkt();
			}
			break;
		case ItemClass.ITEM_CLASS_PROJECTILE:
			switch(subClass)
			{
			case (int)ItemSubclassProjectile.ITEM_SUBCLASS_BOLT:
			case (int)ItemSubclassProjectile.ITEM_SUBCLASS_ARROW:
			case (int)ItemSubclassProjectile.ITEM_SUBCLASS_BULLET:
				SendEquipAmmo(entry);
				break;
			}
			break;
		default:
			SendAutoEquipPkt();
			break;
		}
	}
	
	public static void SendEquipAmmo(uint id)
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_SET_AMMO);
		pkt.Add(id);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void SendAutoEquipPkt()
	{
		byte bagSlot = GetSlotByBag();
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_AUTOEQUIP_ITEM);
		pkt.Append(bagSlot);
		pkt.Append(slot);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public bool DestroyItem(byte _count)
	{
		byte aux8 = 0;
		byte bagSlot = GetSlotByBag();

		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_DESTROYITEM);
		pkt.Append(bagSlot);
		pkt.Append(slot);
		pkt.Append(_count);
		pkt.Append(aux8);
		pkt.Append(aux8);
		pkt.Append(aux8);
		RealmSocket.outQueue.Add(pkt);
		
		return true;
	}
	
	public uint GetCharges()
	{
		return GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_SPELL_CHARGES);
	}
	
	public string GetSubClassName()
	{
		string returnValue = "Default";
		if(classType == ItemClass.ITEM_CLASS_WEAPON)
		{
			switch(subClass)
			{
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_AXE: 
				returnValue = "Axes/OneHanded";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_AXE2:
				returnValue = "Axes/TwoHanded";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_BOW:
				returnValue = "Bows";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_GUN:
				returnValue = "Guns";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_MACE: 
				returnValue = "Maces/OneHanded";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_MACE2:
				returnValue = "Maces/TwoHanded";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_POLEARM:
				returnValue = "Polearms";		   
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_SWORD: 
				returnValue = "Swords/OneHanded";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_SWORD2:
				returnValue = "Swords/TwoHanded";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_obsolete:
				returnValue = "Obsolete";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_STAFF:
				returnValue = "Staves";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_EXOTIC:
				returnValue = "Exotic/OneHanded";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_EXOTIC2 :				
				returnValue = "Exotic/TwoHanded";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_FIST:
				returnValue = "Claws";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_MISC:
				returnValue = "Misc";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_DAGGER:
				returnValue = "Daggers";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_THROWN:
				returnValue = "Thrown";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_SPEAR:
				returnValue = "Polearms";	
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_CROSSBOW:
				returnValue = "CrossBows";	
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_WAND:
				returnValue = "Wand";
				break;
			case (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_FISHING_POLE:
				returnValue = "FishingPole";
				break;
			default: 
				returnValue = "Default";
				break;
			}
		}
		else if (classType == ItemClass.ITEM_CLASS_ARMOR)
		{
			switch(subClass)
			{
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_MISC:
				returnValue = "Misc";
				break;
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_CLOTH:
				returnValue = "Cloth";
				break;
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_LEATHER:
				returnValue = "Leather";
				break;
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_MAIL:
				returnValue = "Mail";
				break;
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_PLATE:
				returnValue = "Plate";
				break;
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_BUCKLER:
				returnValue = "Buckler";
				break;
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_SHIELD:
				returnValue = "Shield";
				break;
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_LIBRAM:
				returnValue = "Libram";
				break;
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_IDOL:
				returnValue = "Idol";
				break;
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_TOTEM:
				returnValue = "Totem";
				break;
			case (int)ItemSubclassArmor.ITEM_SUBCLASS_ARMOR_SIGIL:
				returnValue = "Sigil";
				break;
			default: 
				returnValue = "Default";
				break;
			}
		}
		else if(classType == ItemClass.ITEM_CLASS_CONSUMABLE)
		{
			switch(subClass)
			{
			case (int)ItemSubclassConsumable.ITEM_SUBCLASS_CONSUMABLE:
				returnValue = "Consumable";
				break;
			case (int)ItemSubclassConsumable.ITEM_SUBCLASS_POTION:
				returnValue = "Elixir";
				break;
			case (int)ItemSubclassConsumable.ITEM_SUBCLASS_ELIXIR:
				returnValue = "Potion";
				break;
			case (int)ItemSubclassConsumable.ITEM_SUBCLASS_FLASK:
				returnValue = "Flask";
				break;
			case (int)ItemSubclassConsumable.ITEM_SUBCLASS_SCROLL:
				returnValue = "Extract";
				break;
			case (int)ItemSubclassConsumable.ITEM_SUBCLASS_FOOD:
				returnValue = "Food";
				break;
			case (int)ItemSubclassConsumable.ITEM_SUBCLASS_ITEM_ENHANCEMENT:
				returnValue = "Enhancement";
				break;
			case (int)ItemSubclassConsumable.ITEM_SUBCLASS_BANDAGE:
				returnValue = "Wound Binding";
				break;
			default: 
				returnValue = "Default";
				break;
			}
		}
		return returnValue;
	}
	
	public string GetClassName()
	{
		string returnValue = "Default";
		switch(classType)
		{
		case ItemClass.ITEM_CLASS_WEAPON :
			returnValue = "Weapon";
			break;
		case ItemClass.ITEM_CLASS_ARMOR :
			returnValue = "Armor";
			break;
		case ItemClass.ITEM_CLASS_CONSUMABLE:
			returnValue = "Consumable";
			break;
		case ItemClass.ITEM_CLASS_CONTAINER:
			returnValue = "Container";
			break;
		case ItemClass.ITEM_CLASS_GEM:
			returnValue = "Gem";
			break;
		case ItemClass.ITEM_CLASS_REAGENT:
			returnValue = "Reagent";
			break;
		case ItemClass.ITEM_CLASS_PROJECTILE:
			returnValue = "Projectile";
			break;
		case ItemClass.ITEM_CLASS_TRADE_GOODS:
			returnValue = "Trade Goods";
			break;
		case ItemClass.ITEM_CLASS_GENERIC:
			returnValue = "Gemeric";
			break;
		case ItemClass.ITEM_CLASS_RECIPE:
			returnValue = "Recipe";
			break;
		case ItemClass.ITEM_CLASS_MONEY:
			returnValue = "Money";
			break;
		case ItemClass.ITEM_CLASS_QUIVER:
			returnValue = "Quiver";
			break;
		case ItemClass.ITEM_CLASS_QUEST:
			returnValue = "Quest Item";
			break;
		case ItemClass.ITEM_CLASS_KEY:
			returnValue = "Key";
			break;
		case ItemClass.ITEM_CLASS_PERMANENT:
			returnValue = "Permanent";  
			break;
			/*			case ItemClass.ITEM_CLASS_MISCELLANEOUS:
				returnValue = "Miscellaneous";
				break;
			case ItemClass.ITEM_CLASS_GYLPH:
				returnValue = "Gylph";
				break;
*/				
		default :
			returnValue = "Default";
			break;
		}
		return returnValue;
	}

	/**
	 *	For displaying item statistics and requirements within inventory view
	 */

	public string GetItemMainStats()
	{
		string itemStats = "";
		string qualityString = "";
		string qualityColor = "#442200ff";
//		string qualityFontStyle = "";
		if(classType != ItemClass.ITEM_CLASS_QUEST)
		{
			switch(quality)
			{
			case ItemQualities.ITEM_QUALITY_POOR:
				qualityColor = "grey";
				qualityString = "<color=" + qualityColor + ">" + ItemFlags.GetQualityName(quality) + "</color>";
				break;
			case ItemQualities.ITEM_QUALITY_NORMAL:
				qualityColor = "black";
				qualityString = "<color=" + qualityColor + ">" + ItemFlags.GetQualityName(quality) + "</color>";
				break;
			case ItemQualities.ITEM_QUALITY_UNCOMMON:
				qualityColor = "blue";
				qualityString = "<color=" + qualityColor + ">" + ItemFlags.GetQualityName(quality) + "</color>";
				break;
			case ItemQualities.ITEM_QUALITY_RARE:
				qualityColor = "#5f14a2ff";	//purple
				qualityString = "<b><color=" + qualityColor + ">" + ItemFlags.GetQualityName(quality) + "</color></b>";
				break;
			case ItemQualities.ITEM_QUALITY_EPIC:
				qualityColor = "red";
				qualityString = "<b><color=" + qualityColor + ">" + ItemFlags.GetQualityName(quality) + "</color></b>";
				break;
			case ItemQualities.ITEM_QUALITY_LEGENDARY:
				qualityString = "Mythic";
				break;
			case ItemQualities.ITEM_QUALITY_ARTIFACT:
				qualityString = "Artifact";
				break;
			case ItemQualities.ITEM_QUALITY_HEIRLOOM:
				qualityString = "Heirloom";
				break;
			}
		}
		
		if(quality == ItemQualities.ITEM_QUALITY_RARE || quality == ItemQualities.ITEM_QUALITY_EPIC)
		{
			itemStats = "<b><color=" + qualityColor + ">" + name + "</color></b>";
		}
		else
		{
			itemStats = "<color=" + qualityColor + ">" + name + "</color>";
		}
		
		string classLimit = GetClassLimit();
		if(!string.IsNullOrEmpty(classLimit))
		{
			itemStats += "\n" + classLimit;
		}
		
		itemStats += "\nQuality: " + qualityString;
		
		if(description != null && description.Length > 1)
		{
			itemStats += "\n" + description;
		}

		if(GetClassName() != "Default")
		{
			if(GetSubClassName() == "Default")
			{
				itemStats += "\n" + GetClassName();
			}
			else
			{
				itemStats += "\n" + GetClassName() + ": " + GetSubClassName();
			}
		}

		itemStats += GetCraftingInfo();

		if(GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT) >= 1 && stackable > 1)
		{
			count = GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT);
			itemStats += "\nCount: " + count;
		}

		if(GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_MAXDURABILITY) != 0)
		{
			itemStats += "\nDurability: " + GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_DURABILITY) + 
					"/" + GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_MAXDURABILITY);
		}

		if(reqLvl > 0)
		{
			itemStats += "\nLevel: " + reqLvl;
		}

		return itemStats;
	}

	/**
	 *	For displaying item statistics and requirements within inventory view
	 */

	public string GetItemStats()
	{
		string stats = "";
		if(classType == ItemClass.ITEM_CLASS_WEAPON)
		{ 	
			stats += "\nDamage: "+ damage[0].damageMin+" - "+damage[0].damageMax;
			
			// display weapon speed:
			stats += "\nHaste: "+ delay * 1.0 / 1000 + " attacks/sec";
			
			// display maximum average weapon damage per second:
			stats += "\nMaxDPS: "+ Mathf.Round((damage[0].damageMin + damage[0].damageMax) / 2 / (delay * 1.0f / 1000) * 10) /10;
		}
		else if(classType == ItemClass.ITEM_CLASS_ARMOR)
		{
			stats += "\nArmor: "+armour;
		}
		else if(classType == ItemClass.ITEM_CLASS_GEM && (flags & (int)ItemPrototypeFlags.ITEM_FLAG_UNIQUE_EQUIPPED) > 0)
		{
			GemPropertiesEntry gemProperty = dbs.sGemProperties.GetRecord(gemProperties);
			if(gemProperty != null)
			{
				SpellItemEnchantmentEntry enchant = dbs.sSpellItemEnchantment.GetRecord(gemProperty.spellitemenchantement);
				if(enchant != null)
				{
					stats += enchant.description[0];
				}
			}
		}

		stats += GetSocketInfo();

		if(holyRes != 0)
		{
			stats += "\nWhite Magic Deterrance: " + holyRes;
		}

		if(shadowRes != 0)
		{
			stats += "\nBlack Magic Deterrance: " + shadowRes;
		}

		if(arcaneRes != 0)
		{
			stats += "\nSpirit Magic Deterrance: " + arcaneRes;
		}

		if(fireRes != 0)
		{
			stats += "\nInferno Magic Deterrance: " + fireRes;
		}

		if(frostRes != 0)
		{
			stats += "\nIce Magic Deterrance: " + frostRes;
		}

		if(natureRes != 0)
		{
			stats += "\nEarth Magic Deterrance: " + natureRes;
		}
		
		stats += "\n";
		for(byte i = 0; i < statsCount; i++)
		{
			switch(itemStats[i].itemStatType)
			{
			case ItemModType.ITEM_MOD_HEALTH:  stats +="HP"; break;
			case ItemModType.ITEM_MOD_MANA:  stats +="Mana"; break;
			case ItemModType.ITEM_MOD_AGILITY:  stats +="DEX"; break;
			case ItemModType.ITEM_MOD_STRENGTH:  stats +="MIGHT"; break;
			case ItemModType.ITEM_MOD_STAMINA:  stats +="VIT"; break;
			case ItemModType.ITEM_MOD_INTELLECT:  stats +="WIS"; break;
			case ItemModType.ITEM_MOD_SPIRIT:  stats +="WILL"; break;
			case ItemModType.ITEM_MOD_SPELL_POWER:  stats +="Spell Strength"; break;
			case ItemModType.ITEM_MOD_HEALTH_REGEN:  stats +="Health Regen"; break;
			case ItemModType.ITEM_MOD_MANA_REGENERATION:  stats +="Mana Regen"; break;
			case ItemModType.ITEM_MOD_HASTE_SPELL_RATING:  stats +="Spell Rush Score"; break;
			case ItemModType.ITEM_MOD_HIT_MELEE_RATING:  stats +="Melee Strike Score"; break;
			case ItemModType.ITEM_MOD_CRIT_MELEE_RATING:  stats +="Melee Crit Score"; break;
			case ItemModType.ITEM_MOD_ATTACK_POWER:  stats +="Attack Strength"; break;
			case ItemModType.ITEM_MOD_DEFENSE_SKILL_RATING:   stats +="Aegis Score"; break;
			case ItemModType.ITEM_MOD_DODGE_RATING:  stats +="Divert Score"; break;
			case ItemModType.ITEM_MOD_PARRY_RATING:  stats +="Deflection Score"; break;
			case ItemModType.ITEM_MOD_BLOCK_RATING:  stats +="Block Score"; break;
			case ItemModType.ITEM_MOD_HIT_RATING:  stats +="Strike Score"; break;
			case ItemModType.ITEM_MOD_CRIT_RATING:  stats +="Crit Score"; break;
			case ItemModType.ITEM_MOD_HASTE_RATING:  stats +="Rush Score"; break;
			case ItemModType.ITEM_MOD_EXPERTISE_RATING:  stats +="Mastery Score"; break;
			case ItemModType.ITEM_MOD_HIT_RANGED_RATING:  stats +="Ranged Strike Score"; break;
			case ItemModType.ITEM_MOD_HIT_SPELL_RATING:  stats +="Spell Strike Score"; break;
			case ItemModType.ITEM_MOD_CRIT_RANGED_RATING:  stats +="Ranged Crit Score"; break;
			case ItemModType.ITEM_MOD_CRIT_SPELL_RATING:  stats +="Spell Crit Score"; break;
			case ItemModType.ITEM_MOD_HIT_TAKEN_MELEE_RATING:  stats +="Strike Taken Melee Score"; break;
			case ItemModType.ITEM_MOD_HIT_TAKEN_RANGED_RATING:  stats +="Strike Taken Ranged Score"; break;
			case ItemModType.ITEM_MOD_HIT_TAKEN_SPELL_RATING:  stats +="Strike Taken Spell Score"; break;
			case ItemModType.ITEM_MOD_CRIT_TAKEN_MELEE_RATING:  stats +="Crit Taken Melee Score"; break;
			case ItemModType.ITEM_MOD_CRIT_TAKEN_RANGED_RATING:  stats +="Crit Taken Ranged Score"; break;
			case ItemModType.ITEM_MOD_CRIT_TAKEN_SPELL_RATING:  stats +="Crit Taken Spell Score"; break;
			case ItemModType.ITEM_MOD_HASTE_MELEE_RATING:  stats +="Melee Rush Score"; break;
			case ItemModType.ITEM_MOD_HASTE_RANGED_RATING:  stats +="Ranged Rush Score"; break;
			case ItemModType.ITEM_MOD_HIT_TAKEN_RATING:  stats +="Strike Taken Score"; break;
			case ItemModType.ITEM_MOD_CRIT_TAKEN_RATING:  stats +="Crit Taken Score"; break;
			case ItemModType.ITEM_MOD_RESILIENCE_RATING:  stats +="Resistance Score"; break;
			case ItemModType.ITEM_MOD_RANGED_ATTACK_POWER:  stats +="Ranged Attack Strength"; break;
			case ItemModType.ITEM_MOD_FERAL_ATTACK_POWER:  stats +="Wild Attack Strength"; break;
			case ItemModType.ITEM_MOD_SPELL_HEALING_DONE:  stats +="Spell Alleviate Performed"; break;
			case ItemModType.ITEM_MOD_SPELL_DAMAGE_DONE:  stats +="Spell Damage Performed"; break;
			case ItemModType.ITEM_MOD_ARMOR_PENETRATION_RATING:  stats +="Armor Perforation Score"; break;
			case ItemModType.ITEM_MOD_SPELL_PENETRATION:  stats +="Spell infiltration Score"; break;
			case ItemModType.ITEM_MOD_BLOCK_VALUE:  stats +="Block Amount"; break;
				
			default : continue;
			}
			
			stats += ": " + itemStats[i].itemStatValue+ "\n";
		}
		stats += GetReForgeStats();
		return stats;
	}

	public string GetSocketInfo()
	{
		string ret = "";
		for(int index = 0; index < socket.Length; index++)
		{
			ItemSocket itemSocket = socket[index];
			if(itemSocket.color == (uint)SocketColor.SOCKET_COLOR_NONE)
			{
				Debug.LogWarning("TODO!");
				continue;
			}

			ret += "\n";

			switch(itemSocket.color)
			{
			case 8:
				ret += "<color=red>Red Socket: ";
				break;
			case 4:
				ret += "<color=blue>Blue Socket: ";
				break;
			case 2:
				ret += "<color=#808000ff>Yellow Socket: ";
				break;
			default:
				ret += "<color=cyan>Meta Socket: ";
				break;
			}

			uint enchantmentID = GetEnchantmentId((int)EnchantmentSlot.SOCK_ENCHANTMENT_SLOT + index);
			if(enchantmentID > 0)
			{
				SpellItemEnchantmentEntry gemEnchantment = dbs.sSpellItemEnchantment.GetRecord(enchantmentID);
				if(gemEnchantment != null)
				{
					ret += gemEnchantment.description.Strings[0];
				}
			}
			else
			{
				ret += "Empty";
			}
			ret += "</color>";
		}
		return ret;
	}

	/**
	 *	For displaying re-forge statistics
	 */

	public string GetReForgeStats()
	{
		string stats = "";
		int maxSlots = (int)UpdateFields.EnchantmentSlot.MAX_ENCHANTMENT_SLOT;
		int firstSlot = (int)UpdateFields.EnchantmentSlot.PERM_ENCHANTMENT_SLOT;
		for(int slot = firstSlot; slot < maxSlots; ++slot)
		{
			uint slotId = GetEnchantmentId(slot);
			if(slotId <= 0 ||
			   slot == (int)UpdateFields.EnchantmentSlot.SOCK_ENCHANTMENT_SLOT ||
			   slot == (int)UpdateFields.EnchantmentSlot.SOCK_ENCHANTMENT_SLOT_2 ||
			   slot == (int)UpdateFields.EnchantmentSlot.SOCK_ENCHANTMENT_SLOT_3 ||
			   slot == (int)UpdateFields.EnchantmentSlot.PRISMATIC_ENCHANTMENT_SLOT)
			{
				continue;
			}

			SpellItemEnchantmentEntry enchantment = dbs.sSpellItemEnchantment.GetRecord(slotId);
			if(enchantment.ID > 0)
			{
				stats += "\n  "+enchantment.description.Strings[0];
			}
		}
		if(stats != "")
		{
			stats = "Re-forge stats:"+stats+"\n";
		}

		return stats;
	}
	
	public string GetItemMainStatsOutName()
	{
		int firstIndex = -1;
		string stats = GetItemMainStats();
		firstIndex = stats.IndexOf("\n");
		stats = stats.Substring(firstIndex + 1);
		return stats;
	}

	/**
	 *	For displaying item statistics and requirements within inventory view
	 */
	
	public string GetItemBasicStats()
	{
		stats = GetItemMainStats();
		stats += GetItemStats();
		return stats;
	}

	/**
	 *	For displaying item statistics and requirements within inventory view
	 */

	public string GetItemBasicStatsOutName()
	{
		int firstIndex = -1;
		string stats = GetItemBasicStats();
		firstIndex = stats.IndexOf("\n");
		stats = stats.Substring(firstIndex + 1);
		return stats;
	}

	/**
	 *	For displaying item statistics and requirements within inventory view
	 */

	public string GetItemBasicStatsNoTag()
	{
		string stats = GetItemMainStats();
		string tempString = "";
		int lessIndex = -1;
		int moreIndex = -1;

		lessIndex = stats.IndexOf("<");
		moreIndex = stats.IndexOf(">");

		while(lessIndex >= 0 && moreIndex >= 0 && lessIndex < moreIndex)
		{
			tempString = "" + stats.Substring(0, lessIndex) + stats.Substring(moreIndex + 1);
			stats = tempString;
			lessIndex = -1;
			moreIndex = -1;
			lessIndex = stats.IndexOf("<");
			moreIndex = stats.IndexOf(">");
		}
		stats += GetItemStats();
		return stats;
	}
	
	public bool SoulBound()
	{
		uint flag = 1;
		if((GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_FLAGS) & flag) == flag)
		{
			return true;
		}
		return false;
	}
	
	public bool AccountBound()
	{
		uint flag = (uint)ItemPrototypeFlags.ITEM_FLAG_BOA;
		if((flags & flag) == flag)
		{
			return true;
		}
		return false;
	}
	
	public string HaveDurability()
	{
		string ret = "";
		if(GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_MAXDURABILITY) != 0)
		{
			ret += GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_DURABILITY) + "/";
			ret += GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_MAXDURABILITY);
		}
		return ret;
	}
	
	public ItemEntry ToItemEntry()
	{
		ItemEntry itemEntry = new ItemEntry();
		itemEntry.ID = entry;
		itemEntry.Class = (int)classType;
		itemEntry.SubClass = subClass;
		itemEntry.DisplayID = displayInfoID;
		itemEntry.InventorySlot = (int)inventoryType;
		itemEntry.Name = name;
		return itemEntry;
	}

	//Is have reforge enchants
	public bool HasAnyEnchant()
	{
		bool ret = false;
		for(int enchantSlot = 0; enchantSlot < (int)UpdateFields.EnchantmentSlot.SOCK_ENCHANTMENT_SLOT; ++enchantSlot)
		{
			if(HasEnchant(enchantSlot))
			{
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	public bool HasEnchant(int enchantSlot)
	{
		uint enchantId = GetEnchantmentId(enchantSlot);
		return (enchantId > 0);
	}
	
	public uint GetEnchantmentId(int enchantSlot)
	{
		int firstByte = (int)UpdateFields.EItemFields.ITEM_FIELD_ENCHANTMENT_1_1;
		int slotNumber = firstByte + enchantSlot * MAX_ENCHANTMENT_OFFSET;
		slotNumber += (int)UpdateFields.EnchantmentOffset.ENCHANTMENT_ID_OFFSET;
		return GetUInt32Value((int)slotNumber);
	}

	public uint GetEnchantmentDuration(int enchantSlot)
	{
		int firstByte = (int)UpdateFields.EItemFields.ITEM_FIELD_ENCHANTMENT_1_1;
		int slotNumber = firstByte + enchantSlot * MAX_ENCHANTMENT_OFFSET;
		slotNumber += (int)UpdateFields.EnchantmentOffset.ENCHANTMENT_DURATION_OFFSET;
		return GetUInt32Value((int)slotNumber);
	}

	public uint GetEnchantmentCharges(int enchantSlot)
	{
		int firstByte = (int)UpdateFields.EItemFields.ITEM_FIELD_ENCHANTMENT_1_1;
		int slotNumber = firstByte + enchantSlot * MAX_ENCHANTMENT_OFFSET;
		slotNumber += (int)UpdateFields.EnchantmentOffset.ENCHANTMENT_CHARGES_OFFSET;
		return GetUInt32Value((int)slotNumber);
	}
	
	public string GetClassLimit()
	{
		string ret = "";
		string color = "#0000ffff";
		List<string> classLimit = new List<string>();

		if(allowableClass > 0 && allowableClass < (uint)ClassesMask.ALL_CLASSES )
		{
			uint mask = (uint)ClassesMask.WARRIOR;
			if((allowableClass & mask) > 0)
			{
				classLimit.Add("Fighter");
			}

			mask = (uint)ClassesMask.PALADIN;
			if((allowableClass & mask) > 0)
			{
				classLimit.Add("Templar");
			}

			mask = (uint)ClassesMask.HUNTER;
			if((allowableClass & mask) > 0)
			{
				classLimit.Add("Ranger");
			}

			mask = (uint)ClassesMask.ROGUE;
			if((allowableClass & mask) > 0)
			{
				classLimit.Add("Rogue");
			}

			mask = (uint)ClassesMask.PRIEST;
			if((allowableClass & mask) > 0)
			{
				classLimit.Add("Confessor");
			}

			mask = (uint)ClassesMask.MAGE;
			if((allowableClass & mask) > 0)
			{
				classLimit.Add("Mage");
			}

			mask = (uint)ClassesMask.WARLOCK;
			if((allowableClass & mask) > 0)
			{
				classLimit.Add("Necromacer");
			}

			if(classLimit.Count > 0)
			{
				ret = string.Join(", ", classLimit.ToArray());
				ret = "<b>For <color=" + color + ">" + ret + "</color> only</b>";
			}
		}
		return ret;
	}

	public bool IsPotion()
	{
		bool ret = false;
		if(classType == ItemClass.ITEM_CLASS_CONSUMABLE && 
		   subClass == (uint)ItemSubclassConsumable.ITEM_SUBCLASS_POTION)
		{
			ret = true;
		}
		return ret;
	}

	public ItemSpell GetOnActiveSpell()
	{
		ItemSpell ret = null;
		foreach(ItemSpell itemSpell in spells)
		{
			if(itemSpell.ID > 0 && itemSpell.trigger == 0)
			{
				ret = itemSpell;
				break;
			}
		}
		return ret;
	}
	
	public ItemSpell GetOnActiveSpell(uint entry)
	{
		ItemSpell ret = null;
		Item item = AppCache.sItems.GetItem(entry);
		if(item != null)
		{
			ret = item.GetOnActiveSpell();
		}
		return ret;
	}

	public bool HasPrismaticSlot()
	{
		bool ret = false;
		if(classType == ItemClass.ITEM_CLASS_ARMOR 
		   && (inventoryType == InventoryType.INVTYPE_WAIST
		    || inventoryType == InventoryType.INVTYPE_WRISTS
		    || inventoryType == InventoryType.INVTYPE_HANDS))
		{
//			ret = true; // TODO find slot enchant ITEM_FIELD_ENCHANTMENT_1_1
		}
		return ret;
	}

	public string GetCraftingInfo()
	{
		string ret = "";
		if( reqSkill > 0 )
		{
			ret += "\nRequired skill: "+CraftProfession.GetProfessionName((CraftSkill)reqSkill);
			ret += " ("+(reqSkillRank * 2)+")";
		}
		return ret;
	}
}