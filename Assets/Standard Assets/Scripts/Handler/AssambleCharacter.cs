using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

enum CustomizationSlot//its a extension of the CharacterSlot enum + default meshes = 21
{
	CUSTOM_FACE					= 19,	
	CUSTOM_HAIR					= 20,
	CUSTOM_FACIALHAIR			= 21,
	MAX_CUSTOMIZATIONS			= 3
}

public class CompareByName : Comparer<Component> 
{
	public override int Compare(Component x, Component y)
	{
		return string.Compare(x.name, y.name); 
	}
}

public class StructHelmetWithHair : object
{
	public int DisplayId = 0;
	public Gender GenderType = Gender.GENDER_NONE;
	public Races Race = Races.RACE_NONE;
	
	public static StructHelmetWithHair Create(int displ)
	{
		return Create(displ, Gender.GENDER_NONE, Races.RACE_NONE);
	}
	
	public static StructHelmetWithHair Create(int displ, Gender gender)
	{
		return Create(displ, gender, Races.RACE_NONE);
	}
	
	public static StructHelmetWithHair Create(int displ, Gender gender, Races race)
	{
		return new StructHelmetWithHair(displ, gender, race);
	}
	
	public StructHelmetWithHair(int newDispl, Gender newGender, Races newRace)
	{
		DisplayId = newDispl;
		GenderType = newGender;
		Race = newRace;
	}
}

public class AssambleCharacter : object
{
	public GameObject player;
	Player PlayerScript;
	public PlayerEnum playerEnum = null;
	Component scriptLODManager;

	public Transform leftHand;
	public Transform rightHand;
	
	public GameObject[] currentVisibleArmours;	//ref to equip meshes// holds curent active armor meshes 
	int[] visibleItems = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	public bool[] loadedVisualSlots;
    public bool[] loadedFaceParts;
    public bool isNeedHideVisualSlots = false;
	public int mask;
	string charName;
	Color charSkinColor = Color.white;
	Color charHairColor = Color.white;
	List<StructHelmetWithHair> helmetWithHeadHair = new List<StructHelmetWithHair>();
	List<StructHelmetWithHair> helmetWithFaceHair = new List<StructHelmetWithHair>();
	
	public AssambleCharacter()
	{
		InitHelmetWithHair();
		playerEnum = new PlayerEnum();
	}

	public AssambleCharacter(Player playerScript) : this()
	{
		PlayerScript = playerScript;
		player = PlayerScript.gameObject;
		playerEnum.guid = PlayerScript.guid;
		playerEnum.classType = PlayerScript.playerClass;
		playerEnum.gender = PlayerScript.gender;	
		playerEnum.race = PlayerScript.race;	
		
		playerEnum.skinColor = PlayerScript.skinColor;
		playerEnum.face  = PlayerScript.face;
		playerEnum.hairStyle = PlayerScript.hairStyle;
		playerEnum.hairColor = PlayerScript.hairColor;
		playerEnum.facialHair = PlayerScript.facialHair;
		
		mask = ((int)playerEnum.race << 12) + ((int)playerEnum.gender << 10);	
		int race = (int)playerEnum.race;
		int gender = (int)playerEnum.gender;
		charName = dbs.sCharacterRace.GetRecord(race).Name+"_"+dbs.sCharacterGender.GetRecord(gender).Name;
		
		rightHand = GetRightHand(player);
		leftHand = GetLeftHand(player);	
		
		anim = PlayerScript.GetComponent<Animation>();
	}
	
	public AssambleCharacter(PlayerEnum _playerEnum)
	{
		InitHelmetWithHair();
		playerEnum = _playerEnum;
	}
	
	public virtual void Destroy()
	{
		UnityEngine.Object.Destroy(player);
		player = null;
		PlayerScript = null;
		leftHand = null;
		rightHand = null; 
		
		currentVisibleArmours = null;
	}	
	
	protected Animation anim;
	protected ItemEntry meeleRightHandItem = null;
	protected ItemEntry meeleLeftHandItem = null;
	protected ItemEntry rangeItem = null;
	
	public GameObject Assemble(PlayerEnum _playerEnum)
	{
		playerEnum = _playerEnum;
		Assemble();
		
		return player.transform.parent.gameObject;
	}
	
	public virtual void Assemble()//UpdateItems, UpdateSkin(), ..........
	{
		LoadPlayer();
		UpdateAnimation("idle");
	}
	
	public Transform GetLeftHand(GameObject _player)
	{
		Transform _leftHand = _player.transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L ForeArm/Bip01 L Hand/LeftHandDummy");
		if(!_leftHand)//some models have difrent bone hierarchy
		{
			_leftHand = _player.transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Neck/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L Forearm/Bip01 L Hand/LeftHandDummy");
		}
		return _leftHand;
	}
	
	public Transform GetRightHand(GameObject _player)
	{
		Transform _rightHand = _player.transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R ForeArm/Bip01 R Hand/RightHandDummy");
		if(!_rightHand)//some models have difrent bone hierarchy
		{
			_rightHand = _player.transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R Forearm/Bip01 R Hand/RightHandDummy");
		}
		return _rightHand;
	}
	
	public void UpdateAnimation(string val)
	{
		anim.Play(val);
	}		
	
	public void LoadPlayer()
	{
		try
		{
//			GameObject.Find("LoadZoneManager").GetComponent<BundleLoader>().StopBundleDownloading();
			
			if(player)
			{
				UnityEngine.Object.Destroy(player.transform.parent.gameObject);
				player = null;
			}
			
			isNeedHideVisualSlots = true;
			loadedVisualSlots = new bool[(int)EquipmentSlots.EQUIPMENT_SLOT_END];
			for(int inx = (int)EquipmentSlots.EQUIPMENT_SLOT_START; inx < (int)EquipmentSlots.EQUIPMENT_SLOT_END; inx++)
			{
				loadedVisualSlots[inx] = false;
			}
            loadedFaceParts = new bool[(int)CustomizationSlot.MAX_CUSTOMIZATIONS];
            for (int inx = (int)CustomizationSlot.CUSTOM_FACE; inx <= (int)CustomizationSlot.CUSTOM_FACIALHAIR; inx++)
            {
                loadedFaceParts[inx - (int)CustomizationSlot.CUSTOM_FACE] = false;
            }
			
			int race = (int)playerEnum.race;
			int gender = (int)playerEnum.gender;
			
			//1111 1100 11111111 / race gender00 index / race = 4biti, gender = 2biti, unussed = 2biti, index = 8biti
			mask = (race<<12) + (gender<<10);
			
			string playerRaceName = dbs.sCharacterRace.GetRecord(race).Name;
			string playerGenderName = dbs.sCharacterGender.GetRecord(gender).Name;
			charName  = playerRaceName + "_" + playerGenderName;
			
			GameObject _player = GameResources.InstantiateChar(charName);
			player = (_player.transform.Find("obj")).gameObject;
			
			rightHand = GetRightHand(player);
			leftHand = GetLeftHand(player);	
			
			_player.AddComponent<SelfRotation>();
			_player.transform.position = new Vector3(0, 0.5f, 0.5f);
			_player.transform.eulerAngles = new Vector3(0, 151.7f, 0);
			_player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
			
			
			anim = player.GetComponent<Animation>();
			if(anim)
			{
				Component otherPlayer = player.GetComponent<OtherPlayer>();
				PlayerScript = (Player)otherPlayer;
				InitArmory();
				UpdateAnimation("idle");
			}
		}
		catch(Exception exception)
		{
			Debug.LogException(exception);
		}
	}
	
	public void InitArmory(byte index)
	{
		Dictionary<int, uint> newItems = PlayerScript.GetVisibleItemsEntrys();
		uint _displayID = 0;
		Item item;

		if(currentVisibleArmours != null)
		{
			foreach(GameObject visibleArmour in currentVisibleArmours)
			{
				if(visibleArmour != null)
				{
					UnityEngine.Object.Destroy(visibleArmour);
				}
			}
		}

		isNeedHideVisualSlots = true;
		int visibleArmoursSlotsCount = (int)EquipmentSlots.EQUIPMENT_SLOT_END + (int)CustomizationSlot.MAX_CUSTOMIZATIONS;
        loadedFaceParts = new bool[(int)CustomizationSlot.MAX_CUSTOMIZATIONS];
        for (int inx = (int)CustomizationSlot.CUSTOM_FACE; inx <= (int)CustomizationSlot.CUSTOM_FACIALHAIR; inx++)
        {
            loadedFaceParts[inx - (int)CustomizationSlot.CUSTOM_FACE] = false;
        }
        currentVisibleArmours = new GameObject[visibleArmoursSlotsCount];
		loadedVisualSlots = new bool[(int)EquipmentSlots.EQUIPMENT_SLOT_END];

		for(EquipmentSlots slot = EquipmentSlots.EQUIPMENT_SLOT_START; slot < EquipmentSlots.EQUIPMENT_SLOT_END; slot++)
		{
			loadedVisualSlots[(int)slot] = false;
			if( slot != EquipmentSlots.EQUIPMENT_SLOT_HEAD
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_CHEST
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_LEGS
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_FEET
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_HANDS
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_BACK
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_TABARD
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_MAINHAND
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_OFFHAND
			   && slot != EquipmentSlots.EQUIPMENT_SLOT_RANGED)
			{
				continue;
			}
			playerEnum.items[(int)slot] = new PlayerEnumItem();

			uint itemID;
			newItems.TryGetValue((int)slot, out itemID);
			if(itemID > 0)
			{
				item = AppCache.sItems.GetItem(itemID);
				if((playerEnum.guid > 0) && (item.entry == 0))
				{
					AppCache.sItems.AddRequestToQueue(itemID, playerEnum.guid);
				}

				_displayID = DefaultModels.GetModel(item);			
				playerEnum.items[(int)slot].displayId = _displayID;
				playerEnum.items[(int)slot].inventoryType = item.inventoryType;
				playerEnum.items[(int)slot].subClass = (uint)item.subClass;
			}
			
		}

		InitArmory();
		if(!PlayerScript.isMainPlayer && !PlayerScript.TransformParent.gameObject.GetComponent("LODManager"))
		{
			/// Attach script
			scriptLODManager = PlayerScript.TransformParent.gameObject.AddComponent(Type.GetType("LODManager"));
			/// Comment code below after items will have lod
			/// Send player meshes to the LODManager script
			if(scriptLODManager != null)
			{
				scriptLODManager.SendMessage("SetModels", currentVisibleArmours);
			}
		}
	}
	
	public void InitArmory()
	{
		int visibleArmoursSlotsCount = (int)EquipmentSlots.EQUIPMENT_SLOT_END + (int)CustomizationSlot.MAX_CUSTOMIZATIONS;
		currentVisibleArmours = new GameObject[visibleArmoursSlotsCount];
		
		UpdateSkinColor();
		UpdateFace();
		UpdateHairStyle();
		UpdateHairColor();
		UpdateFacialHair();

		uint displayId;
		for(EquipmentSlots slot = EquipmentSlots.EQUIPMENT_SLOT_START; slot < EquipmentSlots.EQUIPMENT_SLOT_END; slot++)
		{
			if(slot == EquipmentSlots.EQUIPMENT_SLOT_MAINHAND ||
			   slot == EquipmentSlots.EQUIPMENT_SLOT_OFFHAND ||
			   slot == EquipmentSlots.EQUIPMENT_SLOT_RANGED)
			{
				continue;
			}

			displayId = playerEnum.items[(int)slot].displayId;
			if(scriptLODManager)
			{
//				UpdateArmor(displayId, slot, scriptLODManager.stage);
				UpdateArmor(displayId, slot, 0);
			}
			else
			{
				UpdateArmor(displayId, slot);
			}
		}
		
		//Init character face mesh and adding it to LODManager Models array	
		int LODDetail;
		List<Component> components = null;
		int startIndexForDefaultMehses = 14;
		List<Transform> comp = new List<Transform>(player.GetComponentsInChildren<Transform>());
		string defaultMeshesName = "Default_Face"+"  "+"Default_Arm"+"  "+"Default_Belt"+"  "+"Default_Chest"+"  "+"Default_Head";

		foreach(Transform transformComponent in comp)
		{
			if(defaultMeshesName.Contains(transformComponent.gameObject.name))
			{
				if(transformComponent.gameObject.name == "Default_Face")
				{
					UnityEngine.Object.Destroy(transformComponent.gameObject);
					continue;
				}
				
				currentVisibleArmours[startIndexForDefaultMehses] = transformComponent.gameObject;//we chose index 14 because for now we have only 13 visible meshes or 13 equiped armours. 
				LODDetail = 0;//set level of detail to high. this player is mainplayer

				components = new List<Component>();
				components.AddRange(transformComponent.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>() as Component[]);
				components.Sort(new CompareByName());
				foreach(Component component in components)
				{
					(component as SkinnedMeshRenderer).localBounds = new Bounds(new Vector3(0, 0, 0.7f),
					                                                            new Vector3(1.6f, 1.7f, 2.6f));
					component.gameObject.SetActive(false);
				}
				components[LODDetail].gameObject.SetActive(true);
				startIndexForDefaultMehses++;
			}
		}
	}
	
	public void UpdateArmory(byte index)
	{
		//Debug.Log("UpdateArmory(index : byte) " + Time.time);
		Dictionary<int, uint> newItems = PlayerScript.GetVisibleItemsEntrys();
		uint _displayID = 0;
		
		Item item;
		for(int slotInx = (int)EquipmentSlots.EQUIPMENT_SLOT_START; slotInx < (int)EquipmentSlots.EQUIPMENT_SLOT_END; slotInx++)
		{
			playerEnum.items[slotInx] = new PlayerEnumItem();

			uint itemID;
			newItems.TryGetValue(slotInx, out itemID);
			item = AppCache.sItems.GetItem(itemID);
			if((playerEnum.guid > 0) && (itemID > 0) && (item.entry == 0))
			{
				AppCache.sItems.AddRequestToQueue(itemID, playerEnum.guid);
			}
			_displayID = DefaultModels.GetModel(item);			
			playerEnum.items[slotInx].displayId = _displayID;
			playerEnum.items[slotInx].inventoryType = item.inventoryType;
			playerEnum.items[slotInx].subClass = (uint)item.subClass;			
		}
		UpdateArmory();	
	}
	
	public void UpdateArmory()
	{
		uint displayId;
		ItemEntry item;
		
		for(EquipmentSlots slot = EquipmentSlots.EQUIPMENT_SLOT_HEAD; slot <= EquipmentSlots.EQUIPMENT_SLOT_HANDS; slot++)
		{
			displayId = playerEnum.items[(int)slot].displayId;
			if(visibleItems[(int)slot] == displayId)	
			{
				continue;
			}

			visibleItems[(int)slot] = (int)displayId;
			UpdateArmor(displayId, slot);
		}

		int mainHandSlot = (int)EquipmentSlots.EQUIPMENT_SLOT_MAINHAND;
		displayId = playerEnum.items[mainHandSlot].displayId;
		if(visibleItems[mainHandSlot] != (int)displayId)
		{
			visibleItems[mainHandSlot] = (int)displayId;
			if(displayId>0)
			{
				UnEquipItem(PlayerScript, EquipmentSlots.EQUIPMENT_SLOT_MAINHAND);
				EquipWeapon(displayId, EquipmentSlots.EQUIPMENT_SLOT_MAINHAND);
				PlayerScript.itemManager.DeactivateRange();
			}
			else
			{
				UnEquipItem(PlayerScript, EquipmentSlots.EQUIPMENT_SLOT_MAINHAND);
			}
		}

		int offHandSlot = (int)EquipmentSlots.EQUIPMENT_SLOT_OFFHAND;
		displayId = playerEnum.items[offHandSlot].displayId;
		if(visibleItems[offHandSlot] != (int)displayId)
		{
			visibleItems[offHandSlot] = (int)displayId;
			if(displayId > 0)
			{
				UnEquipItem(PlayerScript, EquipmentSlots.EQUIPMENT_SLOT_OFFHAND);
				EquipWeapon(displayId, EquipmentSlots.EQUIPMENT_SLOT_OFFHAND);
				PlayerScript.itemManager.DeactivateRange();
			}	
			else
			{
				UnEquipItem(PlayerScript, EquipmentSlots.EQUIPMENT_SLOT_OFFHAND);
			}
		}
	}
	
	public void UpdateArmor(uint displayID, EquipmentSlots inventoryType)
	{
		UpdateArmor(displayID, (EquipmentSlots)inventoryType, 0);
	}	
	
	public string SelectPathToArmorPrefab ()
	{
		uint playerRace = (uint)playerEnum.race;
		uint playerGender = (uint)playerEnum.gender;
		
		string playerRaceName = dbs.sCharacterRace.GetRecord(playerRace).Name;
		string playerGenderName = dbs.sCharacterGender.GetRecord(playerGender).Name;
		
		string modelPath = "prefabs/chars/" + playerRaceName + "_" + playerGenderName + "/";
		return modelPath;
	}
	
	public bool HavePlayerObject(GameObject playObject) 
	{
		if(playObject != null)
		{
			return true;
		}
		return false;
	}

	public bool HavePlayerObjectPlayer(Player playScript)
	{
		if(PlayerScript != null)
		{
			return HavePlayerObject(PlayerScript.gameObject);
		}
		return false;
	}

	public void UpdateArmor(uint displayID, EquipmentSlots slot, int lodDetail)
	{
		if(displayID > 0)
		{
			LoadArmorBundle(displayID, slot, lodDetail);
		}
		else
		{
			LoadDefaultArmorBundle(slot, lodDetail);
		}
	}
	
	public void LoadArmorBundle(uint displayID, EquipmentSlots slot, int lodDetail)
	{
		string modelPath = SelectPathToArmorPrefab();
		ItemDisplayInfoEntry displ = dbs.sItemDisplayInfo.GetRecord(displayID);
		if(HavePlayerObject(player))
		{
			string itemName = displ.ModelMesh;
			LoadMesh(modelPath + itemName, slot, player, lodDetail, charSkinColor, displ.ID, CheckLoadedArmorBundle);
		}
	}
	
	public GameObject ObtainMeshFromBundle(string modelPath)
	{
		string assetBundleName = string.Format ("Resources/{0}_prefab.unity3d",modelPath);
		assetBundleName = assetBundleName.ToLower();
		UnityEngine.Object bundle = null;
		if(AssetBundleManager.instance.currentLoadedBundles.ContainsKey(assetBundleName))
		{
			bundle = AssetBundleManager.instance.currentLoadedBundles[assetBundleName];
		}
		GameObject go = null;
		if(bundle == null )
		{
#if UNITY_EDITOR
			Debug.LogError("cannot load "+assetBundleName);
#endif
		}
		else
		{
			go = bundle as GameObject;
		}
		if(go == null)
		{
			try
			{
				go = GameResources.Load<GameObject> (string.Format ("prefabs/{0}", modelPath.Replace ("prefabs/", "")));
			}
			catch(Exception exception)
			{
				exception = null;
			}
		}
		return go;
	}
	
	public void CheckLoadedArmorBundle(AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		string[] modelPath = wearMeshCallBackStruct.assetPath;		
		GameObject obj = ObtainMeshFromBundle(modelPath[0]);

		if(obj == null)
		{
			#if UNITY_EDITOR
			Debug.Log(string.Format ("we don't have a resource to instantiate:  {0} {1}", modelPath [0], wearMeshCallBackStruct.slot));
			#endif
			LoadDefaultArmorBundle(wearMeshCallBackStruct.slot, wearMeshCallBackStruct.LoDDetail);
		}
		else
		{
			ApplyArmorMesh(wearMeshCallBackStruct);
		}
	}
	
	public void LoadDefaultArmorBundle(EquipmentSlots slot, int lodDetail)
	{
		string modelPath = SelectPathToArmorPrefab();
		string itemName = ItemManager.staticGetMeshNameFromSlot(slot);
		if(HavePlayerObject(player))
		{
			LoadMesh(modelPath + itemName, slot, player, lodDetail, charSkinColor, 0, ApplyArmorMesh);
		}
	}
	
	public void ApplyArmorMesh(AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		int slot = (int)wearMeshCallBackStruct.slot;
		GameObject obj = LoadMeshFromBundle(wearMeshCallBackStruct);

		if(currentVisibleArmours[slot] != null)
		{
			UnityEngine.Object.Destroy(currentVisibleArmours[slot]);
		}

		if(isNeedHideVisualSlots && obj)
		{
			obj.SetActive(false);
		}
		currentVisibleArmours[slot] =  obj;
		
		if(slot == (int)EquipmentSlots.EQUIPMENT_SLOT_HEAD)
		{
			int displayID = wearMeshCallBackStruct.displayID;
			string currentAnimationName = PlayerScript.aHandler.GetCurentAnimationName(player.GetComponent<Animation>());
			float animationTime = 0.0f;
			if(currentAnimationName != "")
			{
				animationTime = player.GetComponent<Animation>()[currentAnimationName].time;
			}

			// update hair
			if(displayID <= 0 || obj == null || InHelmetWithHair(displayID, helmetWithHeadHair))
			{
				UpdateHairStyle();
			}
			else
			{
				if(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_HAIR] != null)
				{
					UnityEngine.Object.Destroy(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_HAIR]);
				}
			}

			// update facial hair or tatoo
			if (displayID <= 0 || obj == null || InHelmetWithHair(displayID, helmetWithFaceHair))
			{
				UpdateFacialHair();
			}
			else
			{
				if(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR] != null)
				{
					UnityEngine.Object.Destroy(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR]);
				}
			}

			// Restoring animation by saved name to saved time
			player.GetComponent<Animation>().enabled = false;
			player.GetComponent<Animation>().enabled = true;
			int removeStrStart = currentAnimationName.IndexOf(" - Queued Clone", StringComparison.Ordinal);
			currentAnimationName = currentAnimationName.Substring(0, ((removeStrStart < 0) ? currentAnimationName.Length:removeStrStart));
			if(player.GetComponent<Animation>()[currentAnimationName])
			{
				float deltaTime = (PlayerScript.aHandler.movingBackwards) ? (-Time.fixedDeltaTime):(Time.fixedDeltaTime);
				PlayerScript.PlayNewAnimationFromTime(currentAnimationName, animationTime+deltaTime);
			}
		}
		loadedVisualSlots[(int)wearMeshCallBackStruct.slot] = true;
	}
	
	public void ClearAnimations(Animation anim)
	{
		foreach(AnimationState state in anim)
		{
			if(state.name.Contains(" - Queued Clone"))
			{
				anim.RemoveClip(state.clip);
			}
		}
	}
	
	public uint GetAnimationNames(Animation anim)
	{
		List<string> tmpList = new List<string>();
		
		// enumerate all states
		foreach(AnimationState state in anim)
		{
			tmpList.Add(state.name);
		}
		return (uint)tmpList.Count;
	}

	private void LoadDefaultWeapon(EquipmentSlots slot)
	{
		if(slot == EquipmentSlots.EQUIPMENT_SLOT_MAINHAND
		   || slot == EquipmentSlots.EQUIPMENT_SLOT_OFFHAND
		   || slot == EquipmentSlots.EQUIPMENT_SLOT_RANGED)
		{
			if(HavePlayerObject(player))
			{
				LoadMesh("prefabs/weapons/Default/weapon", slot, player, 0, charSkinColor, 0, ApplyWeapon);
			}
		}
		else
		{
			Debug.Log (string.Format ("LoadDefaultWeapon(): incorrected slot {0}", slot.ToString ()));
		}
	}

	private void LoadWeaponBundle(uint displayID, EquipmentSlots slot)
	{
		if(slot == EquipmentSlots.EQUIPMENT_SLOT_MAINHAND
		   || slot == EquipmentSlots.EQUIPMENT_SLOT_OFFHAND
		   || slot == EquipmentSlots.EQUIPMENT_SLOT_RANGED)
		{
			ItemDisplayInfoEntry display = dbs.sItemDisplayInfo.GetRecord(displayID);
			if(HavePlayerObject(player))
			{
				LoadMesh(display.ModelMesh, slot, player, 0, charSkinColor, display.ID, CheckLoadedWeaponBundle);
			}
		}
		else
		{
			Debug.Log (string.Format ("LoadWeaponBundle(): incorrected slot {0}", slot.ToString ()));
		}
	}

	public void CheckLoadedWeaponBundle(AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		string[] modelPath = wearMeshCallBackStruct.assetPath;		
		GameObject obj = ObtainMeshFromBundle(modelPath[0]);
		if(obj == null)
		{
			#if UNITY_EDITOR
			Debug.Log(string.Format ("we don't have a resource to instantiate:  {0} {1}", modelPath [0], wearMeshCallBackStruct.slot));
			#endif
			LoadDefaultWeapon(wearMeshCallBackStruct.slot);
		}
		else
		{
			ApplyWeapon(wearMeshCallBackStruct);
		}
	}

	public void ApplyWeapon(AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		GameObject go = LoadMeshFromBundle(wearMeshCallBackStruct);
		
		if(go != null)
		{
			Quaternion rot = Quaternion.identity;
			rot.eulerAngles = Vector3.zero;
			Vector3 pos = Vector3.zero;
			
			EquipmentSlots slot = wearMeshCallBackStruct.slot;
			if(slot == EquipmentSlots.EQUIPMENT_SLOT_OFFHAND || slot == EquipmentSlots.EQUIPMENT_SLOT_RANGED)
			{
				go.transform.parent = leftHand;
			}
			else
			{
				go.transform.parent = rightHand;
			}
			
			go.transform.localPosition = Vector3.zero;
			go.transform.localRotation = Quaternion.identity;
			try
			{
				go.layer = go.transform.parent.gameObject.layer;
				Transform[] childrens2 = go.GetComponentsInChildren<Transform>();
				foreach(Transform ch2 in childrens2)
				{
					ch2.gameObject.layer = go.layer;
				}
				
			}
			catch(Exception exception)
			{
				Debug.Log (string.Format ("Weapon {0} layer error! {1}", wearMeshCallBackStruct.displayID, exception)); 
			}
			
			go.name = "item";
			
			if(currentVisibleArmours[(int)slot] != null)
			{
				UnityEngine.Object.Destroy(currentVisibleArmours[(int)slot]);
			}
			
			currentVisibleArmours[(int)slot] = go;			
		}	
	}

	public void EquipWeapon(uint displayID, EquipmentSlots slot)
	{
		if(displayID > 0)
		{
			LoadWeaponBundle(displayID, slot);
		}
		else
		{
			LoadDefaultWeapon(slot);
		}
	}
	
	public void LoadMesh(string mItemPath, EquipmentSlots slot, GameObject mModel, uint displayID,
	                     AssetBundleManager.OnEndDownloadWearBundle meshReceivedCallBack)
	{
		LoadMesh(mItemPath, slot, mModel, 0, Color.white, displayID, meshReceivedCallBack);
	}
	
	public void LoadMesh(string mItemPath, EquipmentSlots slot, GameObject mModel, Color skinColor, uint displayID,
	                     AssetBundleManager.OnEndDownloadWearBundle meshReceivedCallBack)
	{
		LoadMesh(mItemPath, slot, mModel, 0, skinColor, displayID, meshReceivedCallBack);
	}
	
	public void LoadMesh(string mItemPath, EquipmentSlots slot, GameObject mModel, int loDdetail, uint displayID,
	                     AssetBundleManager.OnEndDownloadWearBundle meshReceivedCallBack)
	{
		LoadMesh(mItemPath, slot, mModel, loDdetail, Color.white, displayID, meshReceivedCallBack);		
	}
	
	public void LoadMesh(string mItemPath, EquipmentSlots slot, GameObject mModel, int loDdetail, Color skinColor, uint displayID,
	                     AssetBundleManager.OnEndDownloadWearBundle bundleLoadedCallBack)
	{
		string[] splitItemPath  = mItemPath.Split("."[0]);
		AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct;
		wearMeshCallBackStruct.assetPath = splitItemPath;
		wearMeshCallBackStruct.slot = slot;
		wearMeshCallBackStruct.mModel = mModel;
		wearMeshCallBackStruct.LoDDetail = loDdetail;
		wearMeshCallBackStruct.skinColor = skinColor;
		wearMeshCallBackStruct.displayID = (int)displayID;
		if(displayID <= 0 && slot == EquipmentSlots.EQUIPMENT_SLOT_HEAD)
		{
			bundleLoadedCallBack(wearMeshCallBackStruct);
		}
		else
		{
			GetBundleLoader().LoadWearBundle(bundleLoadedCallBack, wearMeshCallBackStruct);
		}
	}
	
	public GameObject LoadMeshFromBundle(AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		string[] modelPath = wearMeshCallBackStruct.assetPath;
		GameObject mModel = wearMeshCallBackStruct.mModel;
		int LoDDetail = wearMeshCallBackStruct.LoDDetail;
		Color skinColor = wearMeshCallBackStruct.skinColor;

		GameObject mMesh = null;

		Color clothColor;
		try
		{
			GameObject resourceToInstantiate = ObtainMeshFromBundle(modelPath[0]);
			if(resourceToInstantiate == null)
				return null;
			mMesh = (GameObject)UnityEngine.Object.Instantiate(resourceToInstantiate, Vector3.zero, Quaternion.identity);

			BoneList scriptBones = mMesh.GetComponent<BoneList>();
			List<Transform> components = new List<Transform>(mMesh.GetComponentsInChildren<Transform>());
			
			byte k;
			
			SkinnedMeshRenderer render;
			string currentBones = "";
			Transform[] MyBones = null;
			string[] meshBonesName = null;
			
			Texture2D tex = null;
			Texture2D maskTex = null;
			string shaderName = "";

			if(scriptBones != null)
			{
				for(int j = 0; j< scriptBones.bonesKey.Length; j++)
				{	
					foreach(Transform component in components)//skip first child because is the parent and search mesh with name scriptBones.mBonesKey[j]
					{
						if(component.name != scriptBones.bonesKey[j])//not the mesh we are looking for
						{
							continue;
						}

						render = component.GetComponent<SkinnedMeshRenderer>();
						//use this to test if current mehs has same bone dependency like previous mesh.
						//cut some bones searchings
						if(currentBones != scriptBones.bonesValue[j])
						{
							meshBonesName = scriptBones.bonesValue[j].Split("^"[0]);	//gets mesh bones name	
							MyBones	= new Transform[meshBonesName.Length-1];
							//search specific character bones to be added to SkinnedMeshRenderer component
							//Match the bones, so the animations would play.
							for(k = 0; k<meshBonesName.Length-1; k++ )
							{
								MyBones[ k ] = staticFindChildByName(meshBonesName[ k ], mModel.transform );	
							}
							
							currentBones = scriptBones.bonesValue[j];
						}
						render.bones = MyBones;
						component.gameObject.layer = mModel.layer;//adding character layer to mesh
						render.localBounds = new Bounds(new Vector3(0, 0, 0.7f), new Vector3(1.6f, 1.7f ,2.6f));//updateWhenOffscreen = true;
						
						if(j != LoDDetail)
						{
							component.gameObject. SetActive(false);
						}
						component.localScale = Vector3.one;
						if(!string.IsNullOrEmpty(component.name))
						{
							for(k = 0; k < render.sharedMaterials.Length; k++)
							{
								render.materials[k] = FixShader(render.materials[k]);
								shaderName = render.materials[k].shader.name;
								if(shaderName.Contains("TwoMasksCharacterCustomization"))
								{
									render.materials[k].SetColor("_Color", skinColor);
									if(modelPath.Length > 3)
									{
										clothColor = SelectColor(modelPath);
									}
									else
									{
										clothColor = render.materials[k].GetColor("_Color2");
									}
									render.materials[k].SetColor("_Color2", clothColor);
								}
								else if(shaderName.Contains("CharacterCustomization"))
								{					
									render.materials[k].SetColor("_Color", skinColor);
								}
							}
						}
						
						break; //mesh done. exit.
					}
					
				}
			}
			
			mMesh.transform.parent = mModel.transform;	//parent new meshes to Character transform
			mMesh.transform.localPosition = Vector3.zero;//reset meshes localposition
			
			UnityEngine.Object.Destroy(scriptBones);//destroy Bones script to clear some memory			
			return mMesh;//return ref
			
		}
		catch(Exception exception)
		{
			#if UNITY_EDITOR
			Debug.Log("Armour piece not properly equiped! \n"+ exception);
			#endif 
			if(mMesh != null)
			{
				UnityEngine.Object.Destroy(mMesh);
			}
			return null;
		}
	}


	public Transform staticFindChildByName(string ThisName, Transform ThisGObj) 
	{
		Transform ReturnObj;
		if(ThisGObj.name == ThisName)
		{
			return ThisGObj.transform;
		}
		
		foreach(Transform child in ThisGObj )
		{
			ReturnObj = staticFindChildByName( ThisName, child );
			if( ReturnObj )
				return ReturnObj;
		}
		return null;
	}
	
	private void LoadDefaultShield(EquipmentSlots slot)
	{
		if(slot == EquipmentSlots.EQUIPMENT_SLOT_OFFHAND)
		{
			if(HavePlayerObject(player))
			{
				LoadMesh("prefabs/weapons/Default/shield", slot, player, 0, charSkinColor, 0, ApplyShield);
			}
		}
		else
		{
			Debug.Log (string.Format ("LoadDefaultShield(): incorrected slot {0}", slot.ToString ()));
		}
	}
	
	private void LoadShieldBundle(uint displayID, EquipmentSlots slot)
	{
		if(slot == EquipmentSlots.EQUIPMENT_SLOT_OFFHAND)
		{
			ItemDisplayInfoEntry display = dbs.sItemDisplayInfo.GetRecord(displayID);
			if(HavePlayerObject(player))
			{
				LoadMesh(display.ModelMesh, slot, player, 0, charSkinColor, display.ID, CheckLoadedShieldBundle);
			}
		}
		else
		{
			Debug.Log (string.Format ("LoadShieldBundle(): incorrected slot {0}", slot.ToString ()));
		}
	}
	
	public void CheckLoadedShieldBundle(AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		string[] modelPath = wearMeshCallBackStruct.assetPath;		
		GameObject obj = ObtainMeshFromBundle(modelPath[0]);
		if(obj == null)
		{
			#if UNITY_EDITOR
			Debug.Log(string.Format ("we don't have a resource to instantiate:  {0} {1}", modelPath [0], wearMeshCallBackStruct.slot));
			#endif
			LoadDefaultShield(wearMeshCallBackStruct.slot);
		}
		else
		{
			ApplyShield(wearMeshCallBackStruct);
		}
	}
	
	public void ApplyShield(AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		GameObject go = LoadMeshFromBundle(wearMeshCallBackStruct);
		if(go != null)
		{
			go.transform.parent = leftHand;
			go.transform.localPosition = Vector3.zero;
			go.transform.localRotation = Quaternion.identity;
			go.transform.localPosition = new Vector3(0, -0.05f, 0);
			
			go.layer = go.transform.parent.gameObject.layer;
			Transform[] childrens2 = go.GetComponentsInChildren<Transform>();
			foreach(Transform ch2 in childrens2)
			{
				ch2.gameObject.layer = go.layer;
			}
			go.name = "shield";
			
			if(currentVisibleArmours[(int)wearMeshCallBackStruct.slot])
			{
				UnityEngine.Object.Destroy(currentVisibleArmours[(int)wearMeshCallBackStruct.slot]);
			}
			
			currentVisibleArmours[(int)wearMeshCallBackStruct.slot] = go;
		}
	}

	public void EquipShield(uint displayID, EquipmentSlots slot)
	{
		if(displayID > 0)
		{
			LoadShieldBundle(displayID, slot);
		}
		else
		{
			LoadDefaultShield(slot);
		}
	}
	
	public void UpdateFace(byte face)
	{
		playerEnum.face = face;
		UpdateFace();
	}
	
	public void UpdateFace()
	{
        loadedFaceParts[(int)(CustomizationSlot.CUSTOM_FACE - CustomizationSlot.CUSTOM_FACE)] = false;
        CustomFaceEntry customFace = dbs.sCustomFace.GetRecord(mask + playerEnum.face);
		CustomSkinColorEntry customSkinColor = dbs.sCustomSkinColor.GetRecord(mask + playerEnum.skinColor);
		string modelMeshName = "";
		Color modelColor;
		
		modelMeshName = customFace.ID != 0 ? customFace.ModelMesh : dbs.sCustomFace.GetRecord((mask + 0)).ModelMesh;
		modelColor = customSkinColor.ID != 0 ? new Color(customSkinColor.r, customSkinColor.g, customSkinColor.b, 1) : Color.white;
		if(HavePlayerObjectPlayer(PlayerScript))
		{
			LoadMesh("prefabs/chars/" + charName + "/Customizations/Face/" + modelMeshName, EquipmentSlots.EQUIPMENT_SLOT_CUSTOMIZATION, 
			         PlayerScript.gameObject, 0, modelColor, 0, ApplyUpdateFace);	
		}
	}
	
	public void ApplyUpdateFace(AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		GameObject modelMesh = LoadMeshFromBundle(wearMeshCallBackStruct);
		if(modelMesh != null)
		{
			if(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACE] != null)
			{
				UnityEngine.Object.Destroy(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACE]);
			}
			currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACE] = modelMesh;
		}
		if(isNeedHideVisualSlots && modelMesh)
		{
			modelMesh.SetActive(false);
		}
		loadedVisualSlots[(int)EquipmentSlots.EQUIPMENT_SLOT_HEAD] = true;
        loadedFaceParts[(int)(CustomizationSlot.CUSTOM_FACE - CustomizationSlot.CUSTOM_FACE)] = true;
    }

    public void UpdateHairStyle()
	{
        loadedFaceParts[(int)(CustomizationSlot.CUSTOM_HAIR - CustomizationSlot.CUSTOM_FACE)] = false;
        CustomHairStyleEntry customHair = dbs.sCustomHairStyle.GetRecord(mask + playerEnum.hairStyle);
		CustomHairColorEntry customHairColor  = dbs.sCustomHairColor.GetRecord(mask + playerEnum.hairColor);
		string modelMeshName = "";
		Color modelColor;
		
		modelMeshName = customHair.ID != 0 ? customHair.ModelMesh  : dbs.sCustomHairStyle.GetRecord((mask + 0)).ModelMesh;// 0 = default hair style
		modelColor = customHairColor.ID != 0 ? new Color(customHairColor.r, customHairColor.g, customHairColor.b, 1)  : Color.white;
		if(HavePlayerObjectPlayer(PlayerScript))
		{
			LoadMesh("prefabs/chars/" + charName + "/Customizations/" + modelMeshName, EquipmentSlots.EQUIPMENT_SLOT_CUSTOMIZATION, 
			         PlayerScript.gameObject, modelColor, 0, ApplyUpdateHairStyle);
		}
	}
	
	public void ApplyUpdateHairStyle(AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		GameObject modelMesh = LoadMeshFromBundle(wearMeshCallBackStruct);

		if(modelMesh)
		{
			if(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_HAIR] != null)
			{
				UnityEngine.Object.Destroy(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_HAIR]);
			}
			currentVisibleArmours[(int)CustomizationSlot.CUSTOM_HAIR] = modelMesh;
		}		
		if(isNeedHideVisualSlots && modelMesh)
		{
			modelMesh.SetActive(false);
		}
		loadedVisualSlots[(int)EquipmentSlots.EQUIPMENT_SLOT_HEAD] = true;
        loadedFaceParts[(int)(CustomizationSlot.CUSTOM_HAIR - CustomizationSlot.CUSTOM_FACE)] = true;
    }

    public void UpdateFacialHair()
	{
        loadedFaceParts[(int)(CustomizationSlot.CUSTOM_FACIALHAIR - CustomizationSlot.CUSTOM_FACE)] = false;
        CustomFacialHairEntry customFacialHair = dbs.sCustomFacialHair.GetRecord(mask+playerEnum.facialHair);
		Color modelColor;
		
		if(playerEnum.gender == Gender.GENDER_FEMALE)
		{
			var customSkinColor = dbs.sCustomSkinColor.GetRecord(mask+playerEnum.skinColor);
			modelColor = customSkinColor.ID != 0 ? new Color(customSkinColor.r, customSkinColor.g, customSkinColor.b, 1)  : Color.white;
		}
		else
		{
			var customHairColor = dbs.sCustomHairColor.GetRecord(mask+playerEnum.hairColor);			
			modelColor = customHairColor.ID != 0 ? new Color(customHairColor.r, customHairColor.g, customHairColor.b, 1)  : Color.white;
		}
		
		if(customFacialHair.ID == 0)
		{
			if(playerEnum.gender == Gender.GENDER_FEMALE)
			{
				UpdateFace();
			}
			else if(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR])
			{
				UnityEngine.Object.Destroy(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR]);
			}		
			loadedFaceParts[(int)(CustomizationSlot.CUSTOM_FACIALHAIR - CustomizationSlot.CUSTOM_FACE)] = true;
		}
		else if(HavePlayerObjectPlayer(PlayerScript))
		{
			string modelMeshName = customFacialHair.ModelMesh;
			LoadMesh("prefabs/chars/" + charName + "/Customizations/" + modelMeshName, EquipmentSlots.EQUIPMENT_SLOT_CUSTOMIZATION, 
			         PlayerScript.gameObject, modelColor, (uint)customFacialHair.ID, ApplyUpdateFacialHair);
		}
	}
	
	public void ApplyUpdateFacialHair(AssetBundleManager.WearMeshCallBackStruct wearMeshCallBackStruct)
	{
		GameObject modelMesh = LoadMeshFromBundle(wearMeshCallBackStruct);
		if(modelMesh)
		{
			if(playerEnum.gender == Gender.GENDER_FEMALE)
			{
				if(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR] != null)
				{
					UnityEngine.Object.Destroy(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR]);		
				}

				int customFacialHairID = wearMeshCallBackStruct.displayID;
				if(customFacialHairID == 13318 || customFacialHairID == 13324 || customFacialHairID == 13330 )
				{
					UnityEngine.Object.Destroy(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR]);
					GameObject obj = Resources.Load<GameObject> ("prefabs/chars/Dwarf_Female/Customizations/FacialHair/FacialHair9");
					if(obj != null)
					{
						obj = UnityEngine.Object.Instantiate<GameObject> (obj);
						obj.transform.parent = PlayerScript.transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 Head");
						currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR] = obj;	
						obj.transform.localPosition  = new Vector3(-0.04315f, 0.067f, -0.004f);
						obj.transform.localRotation = Quaternion.identity;
					}
				}

				if(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACE] != null)
				{
					UnityEngine.Object.Destroy(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACE]);
				}
				currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACE] = modelMesh;	
			}
			else
			{
				if(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR] != null)
				{
					UnityEngine.Object.Destroy(currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR]);
				}
				currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR] = modelMesh;
			}
		}		
		
		if(isNeedHideVisualSlots && modelMesh)
		{
			modelMesh.SetActive(false);
		}
		loadedVisualSlots[(int)EquipmentSlots.EQUIPMENT_SLOT_HEAD] = true;
        loadedFaceParts[(int)(CustomizationSlot.CUSTOM_FACIALHAIR - CustomizationSlot.CUSTOM_FACE)] = true;
    }

    public void UpdateHairColor()
	{
		CustomHairColorEntry customHairColor = dbs.sCustomHairColor.GetRecord(mask+playerEnum.hairColor);// 0 = default hair color
		Color modelColor = customHairColor.ID != 0 ? new Color(customHairColor.r, customHairColor.g, customHairColor.b, 1) : Color.white;

		GameObject modelMesh;
		byte i;
		Material[] mats;

		modelMesh = currentVisibleArmours[(int)CustomizationSlot.CUSTOM_HAIR];
		if(modelMesh != null)
		{
			foreach(Transform tr in modelMesh.transform)
			{
				Renderer renderer = tr.gameObject.GetComponent<Renderer>();
				if(renderer != null)
				{
					mats = renderer.sharedMaterials;
					for(i = 0; i < mats.Length; i++)
					{
						mats[i].SetColor("_Color", modelColor);
					}
				}
			}
		}

		modelMesh = currentVisibleArmours[(int)CustomizationSlot.CUSTOM_FACIALHAIR];
		if(modelMesh != null)
		{
			foreach(Transform tr in modelMesh.transform)
			{
				Renderer renderer = tr.gameObject.GetComponent<Renderer>();
				if(renderer != null)
				{
					mats = renderer.sharedMaterials;
					for(i = 0; i < mats.Length; i++)
					{
						mats[i].SetColor("_Color", modelColor);
					}
				}
			}
		}
		charHairColor = modelColor;
	}
	
	public void UpdateSkinColor()
	{
		CustomSkinColorEntry customSkinColor = dbs.sCustomSkinColor.GetRecord(mask + playerEnum.skinColor);
		Color modelColor = customSkinColor.ID != 0 ? new Color(customSkinColor.r, customSkinColor.g, customSkinColor.b, 1) : Color.white;
		GameObject modelMesh;
		
		for(int equipmentSlotIndex = (int)EquipmentSlots.EQUIPMENT_SLOT_START; equipmentSlotIndex < (int)EquipmentSlots.EQUIPMENT_SLOT_END; ++equipmentSlotIndex)
		{	
			modelMesh = currentVisibleArmours[equipmentSlotIndex];
			if(modelMesh != null)
			{
				foreach(Transform tr in modelMesh.transform)
				{
					Renderer renderer = tr.gameObject.GetComponent<Renderer>();
					if(renderer != null)
					{
						List<Material> materials = new List<Material>(renderer.sharedMaterials);
						foreach(Material material in materials)
						{
							material.SetColor("_Color", modelColor);
						}
					}
				}
			}			
		}
		
		charSkinColor = modelColor;
	}
	
	public void UnEquipItem(Player _player, EquipmentSlots slot)
	{
		GameObject go = null;

		try
		{
			Component animLoader = _player.gameObject.GetComponent("AnimationsLoader");
			switch(slot)
			{
			case EquipmentSlots.EQUIPMENT_SLOT_MAINHAND:
				if(animLoader != null)
				{
					animLoader.SendMessage("RemoveAnimations", 1);
				}

				if(currentVisibleArmours[(int)slot] != null)
				{
					UnityEngine.Object.Destroy(currentVisibleArmours[(int)slot]);
				}
				break;
			case EquipmentSlots.EQUIPMENT_SLOT_OFFHAND:
				if(animLoader != null)
				{
					animLoader.SendMessage("RemoveAnimations", 3);
				}

				if(currentVisibleArmours[(int)slot] != null)
				{
					UnityEngine.Object.Destroy(currentVisibleArmours[(int)slot]);
				}
				break;
			case EquipmentSlots.EQUIPMENT_SLOT_RANGED:
				if(animLoader != null)
				{
					animLoader.SendMessage("RemoveAnimations", 1);
				}

				if(currentVisibleArmours[(int)slot] != null)
				{
					UnityEngine.Object.Destroy(currentVisibleArmours[(int)slot]);
				}
				_player.itemManager.RefreshMainHand();
				_player.itemManager.RefreshOffHand();
				break;
			default:
				UpdateArmor(0, slot);
				_player.UpdateStealth();
				break;
			}
		}
		catch(Exception exception)
		{
			Debug.LogException(exception);
			return;
		}
	}
	
	public Color SelectColor(string[] colorName)
	{
		Color resultColor;
		float red = Int32.Parse(colorName[1]) % 256 / 255.0f;
		float green = Int32.Parse(colorName[2]) % 256 / 255.0f;
		float blue = Int32.Parse(colorName[3]) % 256 / 255.0f;
		float alpha = 1.0f;
		if(colorName.Length == 5)
		{
			alpha = Int32.Parse(colorName[4]) % 256 / 255.0f;
		}
		resultColor = new Color(red, green, blue, alpha);
		return resultColor;
	}
	
	public bool InHelmetWithHair(int displID, IList<StructHelmetWithHair> arrayList)
	{
		bool result = false;
		StructHelmetWithHair tmp;
		for(int index = 0; index < arrayList.Count; index++)
		{
			tmp = arrayList[index];
			result = (tmp.DisplayId == displID);
			result = result && (tmp.GenderType == playerEnum.gender || tmp.GenderType == Gender.GENDER_NONE);
			result = result && (tmp.Race == playerEnum.race || tmp.Race == Races.RACE_NONE);
			if(result)
			{
				break;
			}
		}
		return result;
	}
	
	public Material FixShader(Material material)
	{
		Material returnMaterial;
		if (material.shader.name.Contains("TwoMasksCharacterCustomization"))
		{
			material.shader = Shader.Find("Custom/TwoMasksCharacterCustomization");
		}
		else if (material.shader.name.Contains("CharacterCustomization"))
		{
			material.shader = Shader.Find("Custom/CharacterCustomization");
		}
		returnMaterial = material;
		return returnMaterial;
	}
	
	public bool IsAllLoaded()
	{
		bool ret = true;
		for(byte slot = 0; slot < (byte)EquipmentSlots.EQUIPMENT_SLOT_END; slot++)
		{
			if( slot != (byte)EquipmentSlots.EQUIPMENT_SLOT_HEAD
			   && slot != (byte)EquipmentSlots.EQUIPMENT_SLOT_CHEST
			   && slot != (byte)EquipmentSlots.EQUIPMENT_SLOT_LEGS
			   && slot != (byte)EquipmentSlots.EQUIPMENT_SLOT_FEET
			   && slot != (byte)EquipmentSlots.EQUIPMENT_SLOT_HANDS
			   && slot != (byte)EquipmentSlots.EQUIPMENT_SLOT_BACK
			   && slot != (byte)EquipmentSlots.EQUIPMENT_SLOT_TABARD)
			{
				continue;
			}
			ret &= loadedVisualSlots[slot];
		}
        for (int inx = (int)CustomizationSlot.CUSTOM_FACE; inx <= (int)CustomizationSlot.CUSTOM_FACIALHAIR; inx++)
        {
            ret &= loadedFaceParts[inx - (int)CustomizationSlot.CUSTOM_FACE];
        }
        return ret;
	}
	
	public void EnableAllParts()
	{
		isNeedHideVisualSlots = false;
		foreach(GameObject visibleArmour in currentVisibleArmours)
		{
			if(visibleArmour != null)
			{
				visibleArmour.SetActive(true);
			}
		}
	}

	public void InitHelmetWithHair()
	{
		//head hair
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(0));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(48, Gender.GENDER_MALE, Races.RACE_NIGHTELF));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(49, Gender.GENDER_MALE, Races.RACE_NIGHTELF));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(50, Gender.GENDER_MALE, Races.RACE_NIGHTELF));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(51, Gender.GENDER_MALE, Races.RACE_NIGHTELF));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(48, Gender.GENDER_MALE, Races.RACE_BLOODELF));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(49, Gender.GENDER_MALE, Races.RACE_BLOODELF));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(50, Gender.GENDER_MALE, Races.RACE_BLOODELF));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(51, Gender.GENDER_MALE, Races.RACE_BLOODELF));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9286));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9287));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9288));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9289));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9290));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9291));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9545, Gender.GENDER_FEMALE));		
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9819));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9820));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9821));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9822));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9823));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9824));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9825));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9826));
		
		//face hair
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(0));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(48, Gender.GENDER_MALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(49, Gender.GENDER_MALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(50, Gender.GENDER_MALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(51, Gender.GENDER_MALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(48, Gender.GENDER_FEMALE, Races.RACE_NIGHTELF));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(49, Gender.GENDER_FEMALE, Races.RACE_NIGHTELF));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(50, Gender.GENDER_FEMALE, Races.RACE_NIGHTELF));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(51, Gender.GENDER_FEMALE, Races.RACE_NIGHTELF));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9141, Gender.GENDER_MALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9142, Gender.GENDER_MALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9143, Gender.GENDER_MALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9144, Gender.GENDER_MALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9145, Gender.GENDER_MALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9146, Gender.GENDER_MALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9221, Gender.GENDER_FEMALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9222, Gender.GENDER_FEMALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9223, Gender.GENDER_FEMALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9224, Gender.GENDER_FEMALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9225, Gender.GENDER_FEMALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9226, Gender.GENDER_FEMALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9227, Gender.GENDER_FEMALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9228, Gender.GENDER_FEMALE));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9253));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9254));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9255));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9256));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9257));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9258));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9259));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9260));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(9261));
		helmetWithHeadHair.Add(StructHelmetWithHair.Create(9545, Gender.GENDER_FEMALE));		
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(45, Gender.GENDER_FEMALE, Races.RACE_ORC));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(46, Gender.GENDER_FEMALE, Races.RACE_ORC));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(57, Gender.GENDER_FEMALE, Races.RACE_ORC));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(45, Gender.GENDER_FEMALE, Races.RACE_TROLL));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(46, Gender.GENDER_FEMALE, Races.RACE_TROLL));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(57, Gender.GENDER_FEMALE, Races.RACE_TROLL));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(45, Gender.GENDER_NONE, Races.RACE_BLOODELF));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(46, Gender.GENDER_NONE, Races.RACE_BLOODELF));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(57, Gender.GENDER_NONE, Races.RACE_BLOODELF));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(45, Gender.GENDER_FEMALE, Races.RACE_HUMAN));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(46, Gender.GENDER_FEMALE, Races.RACE_HUMAN));
		helmetWithFaceHair.Add(StructHelmetWithHair.Create(57, Gender.GENDER_FEMALE, Races.RACE_HUMAN));
	}

	public BundleLoader GetBundleLoader()
	{
		BundleLoader ret = null;
		GameObject go = GameObject.Find("LoadZoneManager");
		if(go != null)
		{
			ret = go.GetComponent<BundleLoader>();
		}
		return ret;
	}
}