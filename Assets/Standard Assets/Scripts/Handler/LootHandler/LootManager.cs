using UnityEngine;
using System.Collections.Generic;

public class LootManager
{
	public class MasterLootMember
	{
		public string name;
		public ulong guid;
		public Transform transform;
		
		public MasterLootMember ( string name, ulong guid )
		{
			this.name = name;
			this.guid = guid;
		}
	}
	
	protected MainPlayer mainPlayer;
	public TexturePacker HUDToolkit;
	public Texture2D mainDecoration;
	public Texture2D infoDecoration;
	
	private LootWindow lootWindow;
	
	public List<LootItem> lootItems = new List<LootItem>();
	public List<LootItem> needItems = new List<LootItem>();
	public List<MasterLootMember> masterLootMembers = new List<MasterLootMember>();
	
	public bool  lootState = false;
	public bool  needState = false;
	public bool  rollState = false;
	public bool  masterLootState = false;
	
	public List<uint> listRequestedLootItems = new List<uint>();
	public string[] itemTexts = new string[3];
	
	public float rollTime = 0.0f;
	public ulong needGuid = 0;
	public ulong lootGuid = 0;

	public LootManager ( MainPlayer player )
	{
		mainPlayer = player;
		lootWindow = new LootWindow(mainPlayer);
	}
	
	public void  Show ()
	{
		HUDToolkit = new TexturePacker("HUDToolkit");
		mainDecoration = Resources.Load<Texture2D>("GUI/BigTextures/DialogScreenBackground");
		infoDecoration = Resources.Load<Texture2D>("GUI/BigTextures/InfoScreenBackground");
		if(ButtonOLD.menuState && ButtonOLD.chatState)
		{
			return;
		}
		
		if(rollState)
		{
			lootWindow.DrawRollRequest();
			return;
		}
		else if(lootState || needState)
		{
			lootWindow.DrawLootWindow();
		}
	}
	
	public void  Update ()
	{
		if(rollState && rollTime > 0.1f)
		{
			rollTime -= Time.deltaTime;
			
			if(rollTime <= 0.1f)
			{
				rollState = false;
			}
		}
		
		if(needItems.Count > 0)
		{
			bool  updateNeedLoot = false;
			int needItemIndex = 0;
			
			while(needItemIndex < needItems.Count)
			{
				needItems[needItemIndex].lootTime -= Time.deltaTime;
				if(needItems[needItemIndex].lootTime < 0)
				{
					needItems[needItemIndex].finish = true;
					needItems.RemoveAt(needItemIndex);
					updateNeedLoot = true;
				}
				else
				{
					needItemIndex++;
				}
			}
			
			if(updateNeedLoot)
			{
				RefreshRollLoot();
			}
		}
	}
	
	public void  AcceptRollRequest ()
	{
		WorldSession.interfaceManager.hideInterface = true;
		RefreshRollLoot();
		
		rollTime = 0;
		rollState = false;
		needState = true;
		
//		HUDToolkit = new TexturePacker("HUDToolkit");
//		mainDecoration = Resources.Load<Texture2D>("GUI/BigTextures/DialogScreenBackground");
//		infoDecoration = Resources.Load<Texture2D>("GUI/BigTextures/InfoScreenBackground");
		if(HUDToolkit == null || mainDecoration == null || infoDecoration == null)
		{
			WorldSession.interfaceManager.hideInterface = false;
		}

		lootWindow.CreateLooTInfoZone();
	}
	
	public void  RefreshLoot ()
	{
		byte lootItemIndex = 0;
		bool  haveItemsInfo = CheckItemsInfo(lootItems);
		
		if(haveItemsInfo)
		{
			if(lootItems.Count <= 0)
			{
//				lootWindow.TextZone.RemoveScrollZone();
				SendLootRelease(lootGuid);
				
				if(masterLootState)
				{
					masterLootState = false;
					masterLootMembers.Clear();
				}
				
				mainPlayer.OldLootGuid = 0;
				mainPlayer.ReleaseTarget();
				mainPlayer.lootOnce = true;
				
				lootState = false;
				
				HUDToolkit.Destroy();
				HUDToolkit = null;
				
				mainDecoration = null;
				infoDecoration = null;
				
				WorldSession.interfaceManager.hideInterface = false;
				mainPlayer.MinChatBoxSize = PlayerPrefs.GetInt(mainPlayer.name + " ChatSize") + 1;
				mainPlayer.interf.stateManager();
			}
			
			for(lootItemIndex = 0; lootItemIndex < lootItems.Count && lootItemIndex < 10; lootItemIndex++)
			{
				uint itemID = lootItems[lootItemIndex].itemId;
				string image = DefaultIcons.GetItemIcon(itemID);
				image = image.Substring(0, image.LastIndexOf('.'));
				Texture texture = Resources.Load<Texture>("Icons/" + image);
				
				float buttonX = Screen.width * (0.0725f + 0.095f * (lootItemIndex % 2));
				float buttonY = Screen.height * (0.2928f + 0.14f * (lootItemIndex / 2));
				float itemSize = Screen.width * 0.07285f;
				Rect positionRect = new Rect(buttonX, buttonY, itemSize, itemSize);
				
				float decorationX = buttonX - itemSize * 0.05f;
				float decorationY = buttonY - itemSize * 0.05f;
				float decorationSize = itemSize * 1.2f;
				Rect decorationRect = new Rect(decorationX, decorationY, decorationSize, decorationSize);
				
				lootItems[lootItemIndex].SetRectInfo(texture, positionRect, decorationRect);
			}	
		}
	}
	
	public void  RefreshRollLoot ()
	{
		byte lootItemIndex = 0;
		byte rollItemIndex = 0;
		
		bool  haveItemsInfo = CheckItemsInfo(needItems);
		
		if(haveItemsInfo)
		{
			for(lootItemIndex = 0; lootItemIndex < lootItems.Count; lootItemIndex++)
			{
				for(rollItemIndex = 0; rollItemIndex < needItems.Count; rollItemIndex++)
				{
					if((lootItems[lootItemIndex] as LootItem).itemId == (needItems[rollItemIndex] as LootItem).itemId)
					{	
						lootItems.RemoveAt(lootItemIndex);
					}
				}
			}
			
			if(needItems.Count <= 0)
			{
				RefreshLoot();
				needState = false;
			}
			
			for(rollItemIndex = 0; rollItemIndex < needItems.Count && rollItemIndex < 10; rollItemIndex++)
			{
				uint itemID = (needItems[rollItemIndex] as LootItem).itemId;
				string image = DefaultIcons.GetItemIcon(itemID);
				image = image.Substring(0, image.LastIndexOf('.'));
				Texture texture = Resources.Load<Texture>("Icons/" + image);
				
				float buttonX = Screen.width * (0.0725f + 0.095f * (rollItemIndex % 2));
				float buttonY = Screen.height * (0.2928f + 0.14f * (rollItemIndex / 2));
				float itemSize = Screen.width * 0.07285f;
				Rect positionRect = new Rect(buttonX, buttonY, itemSize, itemSize);
				
				float decorationX = buttonX - itemSize * 0.05f;
				float decorationY = buttonY - itemSize * 0.05f;
				float decorationSize = itemSize * 1.2f;
				Rect decorationRect = new Rect(decorationX, decorationY, decorationSize, decorationSize);
				
				needItems[rollItemIndex].SetRectInfo(texture, positionRect, decorationRect);
			}
		}
	}
	
	private bool CheckItemsInfo(List<LootItem> itemList)
	{
		bool  result = true;
		ItemEntry item;
		for(int i = 0; i < itemList.Count; i++)
		{
			uint itemID = (itemList[i] as LootItem).itemId;
			
			if(!AppCache.sItems.HaveRecord(itemID))
			{
				item = new ItemEntry();
				Debug.Log("Wait answer from server.");
			}
			else
			{
				item = AppCache.sItems.GetItem(itemID).ToItemEntry();
			}
			
			if(item.ID == 0)
			{
				result = false;
			}
		}
		return result;
	}
	
	/*************************/
	/* Loot Packets - Answer */
	/*************************/
	
	public void  HandleLootResponseOpCode ( WorldPacket pkt )
	{
		lootGuid = pkt.ReadReversed64bit();
		byte loot_type = pkt.Read();
		uint money = pkt.ReadReversed32bit();
		byte itemsCount = pkt.Read();
		
		if(money > 0)
		{
			WorldPacket pktMoney = new WorldPacket();
			pktMoney.SetOpcode(OpCodes.CMSG_LOOT_MONEY);
			RealmSocket.outQueue.Add(pktMoney);
		}
		
		for(byte itemsInLoot = 0; itemsInLoot < itemsCount; itemsInLoot++)
		{
			byte order = pkt.Read();
			uint itemId = pkt.ReadReversed32bit();
			uint count = pkt.ReadReversed32bit();
			uint displayInfoId = pkt.ReadReversed32bit();
			uint randomSuffix = pkt.ReadReversed32bit();
			uint randomPropertyID = pkt.ReadReversed32bit();
			byte slot_type = pkt.Read();
			
			if(!AppCache.sItems.HaveRecord(itemId) && !listRequestedLootItems.Contains(itemId))
			{
				listRequestedLootItems.Add(itemId);
				AppCache.sItems.SendQueryItem(itemId);
			}
			
			LootItem lootItem= new LootItem(lootGuid, order, itemId, count,
			                                displayInfoId, randomSuffix, randomPropertyID, slot_type);
			lootItems.Add(lootItem);
		}
		
		if(itemsCount > 0)
		{
			if(lootItems.Count > 0)
			{
				WorldSession.interfaceManager.hideInterface = true;
				lootState = true;
				
				RefreshLoot();
				lootWindow.CreateLooTInfoZone();
				
//				HUDToolkit = new TexturePacker("HUDToolkit");
//				mainDecoration = Resources.Load<Texture2D>("GUI/BigTextures/DialogScreenBackground");
//				infoDecoration = Resources.Load<Texture2D>("GUI/BigTextures/InfoScreenBackground");
			}
		}
		else
		{
			//			if(mainPlayer.target.guid == lootGuid)
			//			{
			SendLootRelease(lootGuid);
			mainPlayer.ReleaseTarget();
			//			}
		}
	}
	
	public void  HandleLootStartRoll ( WorldPacket pkt )
	{
		ulong lootedTargetGUID = pkt.ReadReversed64bit();
		uint mapId = pkt.ReadReversed32bit();
		uint itemSlot = pkt.ReadReversed32bit();
		uint itemId = pkt.ReadReversed32bit();
		uint randomSuffix = pkt.ReadReversed32bit();
		uint randomPropertyID = pkt.ReadReversed32bit();
		uint count = pkt.ReadReversed32bit();
		uint countDown = pkt.ReadReversed32bit();
		ushort zero = pkt.Read();
		
		if(!AppCache.sItems.HaveRecord(itemId) && !listRequestedLootItems.Contains(itemId))
		{
			listRequestedLootItems.Add(itemId);
			AppCache.sItems.SendQueryItem(itemId);
		}
		
		needGuid = lootedTargetGUID;
		LootItem rollItem= new LootItem(lootedTargetGUID, (byte)itemSlot, itemId, count, 
		                                      0, randomSuffix, randomPropertyID, (byte)0);
		rollItem.NeedItem();
		needItems.Add(rollItem);
		
		if((mainPlayer.target == null || lootedTargetGUID != mainPlayer.target.guid) && !needState)
		{		
			if(rollTime < 1)
			{
				rollTime = countDown / 1000;
			}
			rollState = true;
		}
		else
		{
			WorldSession.interfaceManager.hideInterface = true;
			needState = true;
			
//			HUDToolkit = new TexturePacker("HUDToolkit");
//			mainDecoration = Resources.Load<Texture2D>("GUI/BigTextures/DialogScreenBackground");
//			infoDecoration = Resources.Load<Texture2D>("GUI/BigTextures/InfoScreenBackground");
			
			RefreshRollLoot();
			lootWindow.CreateLooTInfoZone();
		}
	}
	
	public void  HandleLootRemovedOpCode ( WorldPacket pkt )
	{
		byte slot = pkt.Read();
		for (int index = 0; index < lootItems.Count; index++)
		{
			if((lootItems[index] as LootItem).order == slot)
			{
				lootItems.RemoveAt(index);
				RefreshLoot();
			}
		}
		
		lootWindow.selectedItemID = 255;
	}
	
	public void  HandleMasterLoot ( WorldPacket pkt )
	{
		masterLootState = true;
		
		uint memberCount = pkt.Read();
		for(int index = 0; index < memberCount; index++)
		{
			ulong memberGUID = pkt.ReadReversed64bit();
			string memberName = mainPlayer.group.getPlayerName(memberGUID);
			MasterLootMember member = new MasterLootMember(memberName, memberGUID);
			masterLootMembers.Add(member);
		}
	}
	
	/**************************/
	/* Loot Packets - Request */
	/**************************/
	
	public void  SendAutoStoreItemLoot ( byte slot )
	{
		if(lootItems.Count == 0)
		{
			return;
		}
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_AUTOSTORE_LOOT_ITEM);
		pkt.Append(slot);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  HandleLootReleaseResponseOpcode ( WorldPacket pkt )
	{
		ulong lootGuid = pkt.ReadReversed64bit();
		byte flag = pkt.Read();
		lootItems.Clear();
		RefreshLoot();
	}
	
	public void  SendLootRelease ( ulong guid )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_LOOT_RELEASE);
		pkt.Append(ByteBuffer.Reverse(guid));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  GiveLootToPlayer ( byte slot ,   ulong guid )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_LOOT_MASTER_GIVE);
		pkt.Add(mainPlayer.target.guid);
		pkt.Append(slot);
		pkt.Add(guid);
		RealmSocket.outQueue.Add(pkt);
	}
	
	/***********************************************/
	public void  HandleLootRollOpCode ( WorldPacket pkt )
	{
		ulong lootedTargetGUID = pkt.ReadReversed64bit();
		uint itemSlot = pkt.ReadReversed32bit(); 
		ulong guid = pkt.ReadReversed64bit();
		uint itemId = pkt.ReadReversed32bit(); 
		uint randomSuffix = pkt.ReadReversed32bit();
		uint randomPropertyID = pkt.ReadReversed32bit();
		byte rollNumber = pkt.Read();
		byte rollType = pkt.Read();
		byte zero = pkt.Read();
		
		string rollT;
		string afis;
		
		string name = mainPlayer.need.GetName(itemId);
		
		switch (rollType)
		{
		case 0: 
			//rollT = "Need";
			rollT = "Demand";
			afis = mainPlayer.group.getPlayerName(guid)+" selected "+rollT+" for item: "+name;
			break;
		case 1 : 
			//rollT = "Need";
			rollT = "Demand";
			afis = mainPlayer.group.getPlayerName(guid)+" selected "+rollT+" and rolled "+rollNumber+" for item: "+name;
			break;
		case 2 : 
			//rollT = "Greed";
			rollT = "Desire";
			if(rollNumber==128)
				afis = mainPlayer.group.getPlayerName(guid)+" selected "+rollT+" for item: "+name;
			else 
				afis = mainPlayer.group.getPlayerName(guid)+" selected "+rollT+" and rolled "+rollNumber+" for item: "+name;
			break;
		default : 
			//rollT= "Pass";
			rollT= "Deny";
			afis = mainPlayer.group.getPlayerName(guid)+" selected "+rollT+" for item: "+name;
			break;
		}
		
		Message ms = new Message();
		ms.message = afis;
		mainPlayer.AddChatInput(ms);
	}
	
	public void  HandleLootRollWinOpCode ( WorldPacket pkt )
	{
		ulong lootedTargetGUID = pkt.ReadReversed64bit();
		uint itemSlot = pkt.ReadReversed32bit(); 
		uint itemId = pkt.ReadReversed32bit();
		uint randomSuffix = pkt.ReadReversed32bit();
		uint randomPropertyID = pkt.ReadReversed32bit();
		ulong targetGuid = pkt.ReadReversed64bit();
		byte rollNumber = pkt.Read();
		byte rollType = pkt.Read();
		string rollT;
		string name = mainPlayer.need.GetName(itemId);
		
		mainPlayer.need.EraseName(itemId);
		
		switch (rollType)
		{
		case 0: 
			//rollT = "Pass";
			rollT = "Deny";
			break;
		case 1 : 
			//rollT = "Need";
			rollT = "Demand";
			break;
		case 2 : 
			//rollT = "Greed";
			rollT = "Desire";
			break;
		default : 
			rollT= ""+rollType;
			break;
		}
		
		string afis;
		afis = mainPlayer.group.getPlayerName(targetGuid)+": WON item: "+name+ " with "+rollT+" rolling: "+rollNumber;
		Message ms = new Message();
		ms.message = afis;
		mainPlayer.AddChatInput(ms);
	}
	
	public void  HandleAllPassed ( WorldPacket pkt )
	{
		ulong lootedTargetGUID = pkt.ReadReversed64bit();
		uint itemSlot = pkt.ReadReversed32bit(); 
		uint itemId = pkt.ReadReversed32bit();
		uint randomPropertyID = pkt.ReadReversed32bit();
		uint randomSuffix = pkt.ReadReversed32bit();
		string afis;
		string name = mainPlayer.need.GetName(itemId);
		mainPlayer.need.EraseName(itemId);
		//afis="All have passed: " + name;
		afis="All have denied: " + name;
		Message ms = new Message();
		ms.message = afis;
		mainPlayer.AddChatInput(ms);
	}
}