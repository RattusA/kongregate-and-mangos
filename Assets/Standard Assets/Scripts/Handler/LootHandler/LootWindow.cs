using UnityEngine;

public class LootWindow
{
	protected float sw = Screen.width;
	protected float sh = Screen.height;
	protected MainPlayer mainPlayer;
	
	// Loot Window
	private Rect lootWindowRect;
	private Rect lootTopDecoration;
	private Rect lootBotDecoration;
	private Rect lootInfoRect;
	private Rect closeXBtnRect;
	
	private Rect needBtnRect;
	private Rect greedBtnRect;
	private Rect passBtnRect;
	
	private Rect takeOneBtnRect;
	private Rect takeAllBtnRect;
	private Rect cancelLootRect;
	
	// Text Rect
	private Rect itemNameRect;
	private Rect scrollZoneRect;
	
	private string itemName = "";
	private string itemStats = "";
	private GUIStyle itemNameStyle;
	private uint fontSize;
	
	// Roll Request Window
	private Rect yesRect;
	private Rect noRect;
	private Rect rollTimeRect;
	
	private Texture rollLootBtn;
	private Texture rollCancelBtn;
	private GUIStyle rollStyle;
	
	private bool  masterLootFlag = false;
	private byte playerIndex = 255;
	
	private Texture notActiveImage;
	public byte selectedItemID = 255;
	
	private Rect charListWindowRect;
	
	private Rect characterListWindowRect;
	private Rect acceptButtonRect;
	private Rect cancelButtonRect;
	
	private Vector2 scrollPosition = Vector2.zero;
	private Rect scrollZone;
	private Rect fullScrollZone;
	
	private GUIStyle textStyle = new GUIStyle();
	public ScrollZone TextZone = new ScrollZone();

	// FUNCTIONS
	public LootWindow ( MainPlayer player )
	{
		mainPlayer = player;
		
		lootWindowRect = new Rect(sw * 0.05f, sh * 0.15f, sw * 0.75f, sh * 0.84f);
		lootTopDecoration = new Rect(sw * 0.06875f, sh * 0.192f, sw * 0.7125f, sh * 0.084f);
		lootBotDecoration = new Rect(sw * 0.0675f, sh * 0.36f, sw * 0.18f, sh * 0.378f);
		lootInfoRect = new Rect(sw * 0.2525f, sh * 0.2928f, sw * 0.525f, sh * 0.546f);
		closeXBtnRect = new Rect(sw * 0.775f, sh * 0.115f, sw * 0.045f, sw * 0.045f);
		needBtnRect = new Rect(sw * 0.2375f, sh * 0.8792f, sw * 0.17f, sh * 0.0772f);
		greedBtnRect = new Rect(sw * 0.425f, sh * 0.8792f, sw * 0.17f, sh * 0.0772f);
		passBtnRect = new Rect(sw * 0.6125f, sh * 0.8792f, sw * 0.17f, sh * 0.0772f);
		takeOneBtnRect = new Rect(sw * 0.2375f, sh * 0.8792f, sw * 0.17f, sh * 0.0772f);
		takeAllBtnRect = new Rect(sw * 0.425f, sh * 0.8792f, sw * 0.17f, sh * 0.0772f);
		cancelLootRect = new Rect(sw * 0.6125f, sh * 0.8792f, sw * 0.17f, sh * 0.0772f);
		itemNameRect = new Rect(sw * 0.15f, sh * 0.1836f, sw * 0.6f, sh * 0.07f);
		scrollZoneRect = new Rect(sw * 0.284f, sh * 0.32f, sw * 0.47f, sh * 0.5f);
		yesRect = new Rect(sw * 0.2f, sh * 0.7f, sw * 0.25f, sh * 0.1f);
		noRect = new Rect(sw * 0.55f, sh * 0.7f, sw * 0.25f, sh * 0.1f);
		rollTimeRect = new Rect(sw * 0.4f, sh * 0.7f, sw * 0.2f, sh * 0.1f);
		charListWindowRect = new Rect(sw * 0.2525f, sh * 0.18f, sw * 0.475f, sh * 0.8f);
		characterListWindowRect = new Rect(0, 0, sw * 0.475f, sh * 0.8f);
		acceptButtonRect = new Rect(sw * 0.055f, sh * 0.7f, sw * 0.14f, sh * 0.075f);
		cancelButtonRect = new Rect(sw * 0.28f, sh * 0.7f, sw * 0.14f, sh * 0.075f);
		scrollZone = new Rect(sw * 0.05f, sh * 0.05f, sw * 0.38f, sh * 0.6f);
		fullScrollZone = new Rect(0, 0, sw * 0.36f, sh * 0.8f);

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		rollStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, Screen.height * 0.09f, 
		                                    Color.white, true, FontStyle.Bold);
		itemNameStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, Screen.height * 0.06f, 
		                                        Color.white, true, FontStyle.Normal);
		textStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.05f, 
		                                    Color.black, true, FontStyle.Normal);
		
		fontSize = (uint)(mainPlayer.statsFontSize * 1.3f);
		if(dbs._sizeMod != 2)
		{
			fontSize = (uint)(fontSize * (Screen.width / 1024.0f));
		}
		
		notActiveImage = Resources.Load<Texture>("GUI/InGame/notActiveImage");
		
		rollLootBtn = Resources.Load<Texture>("GUI/InGame/btn_roll");
		rollCancelBtn = Resources.Load<Texture>("GUI/InGame/canceltbutton");
	}
	
	public void  DrawRollRequest ()
	{														
		if(GUI.Button(yesRect, rollLootBtn, GUIStyle.none))
		{
			if(mainPlayer.lootManager.lootState)
			{
				CancelLooting();
			}			
			mainPlayer.lootManager.AcceptRollRequest();
		}
		
		if(GUI.Button(noRect, rollCancelBtn, GUIStyle.none))
		{
			mainPlayer.lootManager.rollState = false;
		}
		
		GUI.Label(rollTimeRect, "" + Mathf.Round(mainPlayer.lootManager.rollTime), rollStyle);
	}
	
	public void  CreateLooTInfoZone ()
	{
		TextZone.InitSkinInfo(mainPlayer.newskin.GetStyle("DescriptionAndStats"));
		TextZone.CreateScrollZoneWithText(scrollZoneRect, "", mainPlayer.mainTextColor);
	}
	
	public void  DrawLootWindow ()
	{		
		GUI.DrawTexture(lootWindowRect, mainPlayer.lootManager.mainDecoration);
		GUI.DrawTexture(lootInfoRect, mainPlayer.lootManager.infoDecoration);
		mainPlayer.lootManager.HUDToolkit.DrawTexture(lootTopDecoration, "TopDecoration.png");
		mainPlayer.lootManager.HUDToolkit.DrawTexture(lootBotDecoration, "LeftDecoration3.png");
		
		if(selectedItemID == 255
		   && (mainPlayer.lootManager.lootItems.Count > 0 || mainPlayer.lootManager.needItems.Count > 0))
		{
			SelectItem(0);
		}
		
		if(TextZone.showZone && !ButtonOLD.menuState)
		{	 			
			TextZone.DrawTraitsInfo();
		}
		GUI.Label(itemNameRect, itemName, itemNameStyle);
		
		if(mainPlayer.lootManager.needState)
		{
			DrawGroupLootButtons();
		}
		else
		{
			DrawFreeLootButtons();
		}
		
		if(masterLootFlag)
		{
			GUI.ModalWindow(1, charListWindowRect, SelectCharFromList, "");
		}
	}
	
	private void  SelectCharFromList (int id)
	{
		GUI.DrawTexture(characterListWindowRect, mainPlayer.lootManager.mainDecoration);
		
		if(playerIndex != 255)
		{
			GUIAtlas.interfMix3.DrawTexture(acceptButtonRect, "accept_button2.png");
			if(GUI.Button(acceptButtonRect, "", GUIStyle.none))
			{
				byte slot = mainPlayer.lootManager.lootItems[selectedItemID].order;
				mainPlayer.lootManager.GiveLootToPlayer(slot, mainPlayer.lootManager.masterLootMembers[playerIndex].guid);
				playerIndex = 255;
				masterLootFlag = false;
			}
		}
		
		GUIAtlas.interfMix3.DrawTexture(cancelButtonRect, "cancel_button2.png");
		if(GUI.Button(cancelButtonRect, "", GUIStyle.none))
		{
			playerIndex = 255;
			masterLootFlag = false;
		}
		
		scrollPosition = GUI.BeginScrollView(scrollZone, scrollPosition, fullScrollZone, false, true);
		for(byte index = 0; index < mainPlayer.lootManager.masterLootMembers.Count; index++)
		{
			float padding = index * sh * 0.08f + sh * 0.01f;
			Rect position = new Rect(0, padding, sw * 0.35f, sh * 0.07f);
			
			GUIAtlas.interfMix3.DrawTexture(position, "empty_button.png");
			if(WorldSession.GetRealChar(mainPlayer.lootManager.masterLootMembers[index].guid))
			{
				if(playerIndex == index)
				{
					textStyle.normal.textColor = Color.red;
				}
				else
				{
					textStyle.normal.textColor = Color.black;
				}
			}
			else
			{
				if(playerIndex == index)
				{
					playerIndex = 255;
				}
				textStyle.normal.textColor = Color.grey;
			}
			
			if(GUI.Button(position, mainPlayer.lootManager.masterLootMembers[index].name, textStyle))
			{
				if(WorldSession.GetRealChar(mainPlayer.lootManager.masterLootMembers[index].guid))
				{
					playerIndex = index;
				}
			}
		}
		GUI.EndScrollView();
	}
	
	private void  SelectItem ( byte ID )
	{
		selectedItemID = ID;
		GetItemDetails();
		TextZone.SetNewText(itemStats);
	}
	
	private void  GetItemDetails ()
	{
		LootItem lootItem;
		if(mainPlayer.lootManager.needState)
		{
			lootItem = mainPlayer.lootManager.needItems[selectedItemID];
		}
		else
		{
			lootItem = mainPlayer.lootManager.lootItems[selectedItemID];
		}
		
		Item currentItem = AppCache.sItems.GetItem(lootItem.itemId);
		
		itemName = currentItem.name;
		itemStats = currentItem.GetItemBasicStatsOutName();
		itemStats = "<size=" + fontSize + "><color=" + mainPlayer.mainTextColor + ">" + itemStats + "</color></size>";
	}
	
	/*********************/
	/* Free Loot Buttons */
	/*********************/
	
	private void  DrawFreeLootButtons ()
	{
		if(!mainPlayer.lootManager.masterLootState)
		{
			mainPlayer.lootManager.HUDToolkit.DrawTexture(takeOneBtnRect, "TakeThisButton.png");
			if(GUI.Button(takeOneBtnRect, "", GUIStyle.none))
			{
				TakeItem();
			}
			
			mainPlayer.lootManager.HUDToolkit.DrawTexture(takeAllBtnRect, "TakeAllButton.png");
			if(GUI.Button(takeAllBtnRect, "", GUIStyle.none))
			{
				TakeAllItems();
			}
		}
		else if(MainPlayer.GUID == mainPlayer.group.looterGUID)
		{
			mainPlayer.lootManager.HUDToolkit.DrawTexture(takeOneBtnRect, "GetItemButton.png");
			if(GUI.Button(takeOneBtnRect, "", GUIStyle.none))
			{
				int membersCount = mainPlayer.lootManager.masterLootMembers.Count;
				if(membersCount < 6)
				{
					fullScrollZone = new Rect(0, 0, sw * 0.36f, sh * 0.6f);
				}
				else
				{	
					fullScrollZone = new Rect(0, 0, sw * 0.36f, sh * 0.08f * membersCount + sh * 0.03f);
				}
				masterLootFlag = true;
			}
		}
		
		
		mainPlayer.lootManager.HUDToolkit.DrawTexture(cancelLootRect, "CloseButton.png");
		if(GUI.Button(cancelLootRect, "", GUIStyle.none))
		{
			CancelLooting();
			mainPlayer.lootManager.SendLootRelease(mainPlayer.lootManager.lootGuid);
		}
		
		for(byte lootItemIndex = 0; lootItemIndex < mainPlayer.lootManager.lootItems.Count; lootItemIndex++)
		{
			LootItem lootItem = mainPlayer.lootManager.lootItems[lootItemIndex];
			GUI.DrawTexture(lootItem.position, lootItem.image);
			
			if(lootItemIndex != selectedItemID)
			{
				GUI.DrawTexture(lootItem.position, notActiveImage);
			}
			
			if(GUI.Button(lootItem.position, "", GUIStyle.none))
			{
				SelectItem(lootItemIndex);
			}
			
			mainPlayer.lootManager.HUDToolkit.DrawTexture(lootItem.decoration, "ItemDecorationCase.png");
		}
	}
	
	private void  TakeItem ()
	{
		if(selectedItemID == 255)
		{
			return;
		}
		
		byte slot = mainPlayer.lootManager.lootItems[selectedItemID].order;
		mainPlayer.lootManager.SendAutoStoreItemLoot(slot);
		selectedItemID = 255;
	}
	
	private void  TakeAllItems ()
	{
		int numberOfLootItems = mainPlayer.lootManager.lootItems.Count;
		while(numberOfLootItems > 0)
		{
			mainPlayer.lootManager.SendAutoStoreItemLoot((mainPlayer.lootManager.lootItems[--numberOfLootItems] 
			                                              as LootItem).order);
		}
		mainPlayer.lootManager.RefreshLoot();
	}
	
	private void  CancelLooting ()
	{
		TextZone.RemoveScrollZone();
		mainPlayer.lootManager.lootState = false;
		
		mainPlayer.OldLootGuid = 0;
		mainPlayer.ReleaseTarget();
		mainPlayer.PortraitReff.UpdateTargetSnapShot(null);
		mainPlayer.lootOnce = true;
		
		selectedItemID = 255;
	}
	
	/**********************/
	/* Group Loot Buttons */
	/**********************/
	
	private void  DrawGroupLootButtons ()
	{
		mainPlayer.lootManager.HUDToolkit.DrawTexture(needBtnRect, "DemandButton.png");
		if(GUI.Button(needBtnRect, "", GUIStyle.none))
		{
			NeedRoll();
			if(mainPlayer.lootManager.needItems.Count == 0)
			{
				return;
			}
		}
		
		mainPlayer.lootManager.HUDToolkit.DrawTexture(greedBtnRect, "DesireButton.png");
		if(GUI.Button(greedBtnRect, "", GUIStyle.none))
		{
			GreedRoll();
			if(mainPlayer.lootManager.needItems.Count == 0)
			{
				return;
			}
		}
		
		mainPlayer.lootManager.HUDToolkit.DrawTexture(passBtnRect, "DenyButton.png");
		if(GUI.Button(passBtnRect, "", GUIStyle.none))
		{
			CancelRoll();
			if(mainPlayer.lootManager.needItems.Count == 0)
			{
				return;
			}
		}
		
		for(byte rollItemIndex = 0; rollItemIndex < mainPlayer.lootManager.needItems.Count; rollItemIndex++)
		{
			LootItem lootItem = mainPlayer.lootManager.needItems[rollItemIndex];
			GUI.DrawTexture(lootItem.position, lootItem.image);
			if(rollItemIndex != selectedItemID)
			{
				GUI.DrawTexture(lootItem.position, notActiveImage);
			}
			
			if(GUI.Button(lootItem.position, "", GUIStyle.none))
			{
				SelectItem(rollItemIndex);
			}
			
			mainPlayer.lootManager.HUDToolkit.DrawTexture(lootItem.decoration, "ItemDecorationCase.png");
		}
	}
	
	private void  NeedRoll ()
	{
		byte bite = 1;
		mainPlayer.session.SendLootRoll(mainPlayer.lootManager.needGuid, 
		                                mainPlayer.lootManager.needItems[selectedItemID].order, bite);
		
		RemoveLootRecord(mainPlayer.lootManager.needItems[selectedItemID].order);
	}
	
	private void  GreedRoll ()
	{
		byte bite = 2;
		mainPlayer.session.SendLootRoll(mainPlayer.lootManager.needGuid, 
		                                mainPlayer.lootManager.needItems[selectedItemID].order, bite);
		RemoveLootRecord(mainPlayer.lootManager.needItems[selectedItemID].order);
	}
	
	private void  CancelRoll ()
	{
		byte bite = 0;
		mainPlayer.session.SendLootRoll(mainPlayer.lootManager.needGuid, 
		                                mainPlayer.lootManager.needItems[selectedItemID].order, bite);
		RemoveLootRecord(mainPlayer.lootManager.needItems[selectedItemID].order);
	}
	
	private void  RemoveLootRecord ( byte slot )
	{
		for (int index = 0; index < mainPlayer.lootManager.needItems.Count; index++)
		{
			if(mainPlayer.lootManager.needItems[index].order == slot)
			{
				mainPlayer.lootManager.needItems.RemoveAt(index);
			}
		}
		
		mainPlayer.lootManager.RefreshRollLoot();
		selectedItemID = 255;
	}
}