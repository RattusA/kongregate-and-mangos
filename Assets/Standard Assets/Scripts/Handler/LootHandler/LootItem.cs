﻿using UnityEngine;

public class LootItem
{
	public ulong guid = 0;
	public byte order = 0;
	public uint itemId = 0;
	public uint count = 0;
	public uint displayInfoId = 0;
	public uint randomSuffix = 0;
	public uint randomPropertyID = 0;
	public byte slot_type = 0;
	
	public float lootTime = 0;
	public bool  finish = false;
	
	public Texture image = null;
	public Rect position;
	public Rect decoration;
	
	public LootItem (ulong guid, byte order, uint itemId, uint count, uint displayInfoId, uint randomSuffix,
	                 uint randomPropertyID, byte slot_type )
	{
		this.guid = guid;
		this.order = order;
		this.itemId = itemId;
		this.count = count;
		this.displayInfoId = displayInfoId;
		this.randomSuffix = randomSuffix;
		this.randomPropertyID = randomPropertyID;
		this.slot_type = slot_type;
	}
	
	public void  NeedItem ()
	{
		this.lootTime = 60.0f;
		this.finish = false;
	}
	
	public void  SetRectInfo ( Texture image ,   Rect position ,   Rect decoration )
	{
		this.image = image;
		this.position = position;
		this.decoration = decoration;
	}
}