﻿using UnityEngine;

public class StableMainWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private GUIStyle style = new GUIStyle();
	private GUIStyle buttonStyle = new GUIStyle();
	
	private Texture2D[] slotIcon = new Texture2D[5];
	private StableMaster stable = null;
	
	public StableMainWindow ()
	{
		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		style.font = font;
		style.alignment = TextAnchor.MiddleCenter;
		style.fontSize = (int)(sh * 0.06f);
		style.normal.textColor = Color.white;
		style.wordWrap = true;
		
		buttonStyle.font = font;
		buttonStyle.alignment = TextAnchor.MiddleCenter;
		buttonStyle.fontSize = (int)(sh * 0.06f);
		buttonStyle.normal.textColor = Color.black;
	}
	
	public void  Show ()
	{
		CheckTexture();
		if(stable == null)
		{
			stable = WorldSession.stableMaster;
		}
		
		GUI.DrawTexture( new Rect(sw * 0.1875f, sh * 0.25f, sw * 0.625f, sh * 0.58333f), stable.mainDecoration);
		
		stable.stableMasterToolkit.DrawTexture( new Rect(sw * 0.25f, sh * 0.5f,  sw * 0.0625f,  sw * 0.0625f), "SquareButton.png");
		DrawOpenedSlot( new Rect(sw * 0.25f, sh * 0.5f,  sw * 0.0625f,  sw * 0.0625f), 0);
		
		GUI.Label( new Rect(sw * 0.2f, sh * 0.275f, sw * 0.6f, sh * 0.2f), "Boarding Master", style);
		GUI.Label( new Rect(sw * 0.2f, sh * 0.6f, sw * 0.6f, sh * 0.09f), "Select Slot to Board Pet.", style);
		
		
		if(WorldSession.stableMaster.stableSlots > 0)
			DrawOpenedSlot( new Rect(sw * 0.4375f, sh * 0.5f, sw * 0.0625f,  sw * 0.0625f), 1);
		else
			DrawClosedSlot( new Rect(sw * 0.4375f, sh * 0.5f, sw * 0.0625f,  sw * 0.0625f));
		
		if(WorldSession.stableMaster.stableSlots > 1)
			DrawOpenedSlot( new Rect(sw * 0.53125f, sh * 0.5f,  sw * 0.0625f,  sw * 0.0625f), 2);
		else
			DrawClosedSlot( new Rect(sw * 0.53125f, sh * 0.5f,  sw * 0.0625f,  sw * 0.0625f));
		
		if(WorldSession.stableMaster.stableSlots > 2)
			DrawOpenedSlot( new Rect(sw * 0.625f, sh * 0.5f,  sw * 0.0625f,  sw * 0.0625f), 3);
		else
			DrawClosedSlot( new Rect(sw * 0.625f, sh * 0.5f,  sw * 0.0625f,  sw * 0.0625f));
		
		if(WorldSession.stableMaster.stableSlots > 3)
			DrawOpenedSlot( new Rect(sw * 0.71875f, sh * 0.5f,  sw * 0.0625f,  sw * 0.0625f), 4);
		else
			DrawClosedSlot( new Rect(sw * 0.71875f, sh * 0.5f,  sw * 0.0625f,  sw * 0.0625f));
		
		
		stable.stableMasterToolkit.DrawTexture( new Rect(sw * 0.3625f, sh * 0.7f, sw * 0.275f, sh * 0.08f), "EmptyButtom.png");
		if(GUI.Button( new Rect(sw * 0.3625f, sh * 0.7f, sw * 0.275f, sh * 0.08f), "Close", buttonStyle))
			CloseWindow();
	}
	
	private void  CheckTexture ()
	{
		for(byte index = 0; index < 5; index++)
		{
			if(WorldSession.stableMaster.stableArray[index].petEntry == 0)
			{
				if(slotIcon[index] != null)
				{
					slotIcon[index] = null;
				}
			}
			else
				if(slotIcon[index] == null) 
					slotIcon[index] = GetSlotTexture(index);
		}
	}
	
	private Texture2D GetSlotTexture ( byte index )
	{
		Texture2D mobIcon = null;
		StableMaster stable = WorldSession.stableMaster;
		if(stable != null)
		{
			MobTemplate mobTemplate = WorldSession.player.session.getMobTemplate(stable.stableArray[index].petEntry);
			if(mobTemplate != null)
			{
				string iconName;
				iconName = dbs.staticWOWToWOM(mobTemplate.modelId[0]).ToString();
				mobIcon = Resources.Load<Texture2D>("Portrait/Icons/" + iconName);
				if(mobIcon == null)
				{
					iconName = mobTemplate.modelId[0].ToString();
					mobIcon = Resources.Load<Texture2D>("Portrait/Icons/" + iconName);
				}
			}
		}
		if(mobIcon == null)
		{
			mobIcon = Resources.Load<Texture2D>("Portrait/DefaultTexture");
		}
		return mobIcon;
	}
	
	private void  DrawOpenedSlot ( Rect positionRect ,   byte index )
	{
		StableMaster stable = WorldSession.stableMaster;
		if(stable != null)
		{
			stable.stableMasterToolkit.DrawTexture(positionRect, "SquareButton.png");
			if(GUI.Button(positionRect, "", style))
			{
				if(stable.stableArray[index].petEntry == 0)
					return;
				
				stable.mobTemplate = WorldSession.player.session.getMobTemplate(stable.stableArray[index].petEntry);
				stable.selectedPet = stable.stableArray[index];
				stable.mobIcon = slotIcon[index];
				
				ClearIconArray();
				stable.ShowPetWindow();
			}
		}
		if(slotIcon[index])
		{
			GUI.DrawTexture(positionRect, slotIcon[index]);
		}
	}
	
	private void  DrawClosedSlot ( Rect positionRect )
	{
		stable.stableMasterToolkit.DrawTexture(positionRect, "SquareDecoration.png");
		if(GUI.Button(positionRect, "", style))
			WorldSession.stableMaster.ShowMessageWindow();
	}
	
	private void  CloseWindow ()
	{
		ClearIconArray();
		WorldSession.stableMaster.Close();
	}
	
	private void  ClearIconArray ()
	{	
		for(byte index = 0; index < 5; index++)
			slotIcon[index] = null;
	}
}