﻿using UnityEngine;

public class StableMessageWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Rect windowDecorationRect;
	private Rect mainText;
	private Rect hintText;
	
	private Rect leftButton;
	private Rect rightButton;
	private Rect centerButton;
	
	private GUIStyle style = new GUIStyle();
	private GUIStyle buttonStyle = new GUIStyle();
	
	private StableMaster stable = null;
	
	public StableMessageWindow ()
	{
		windowDecorationRect = new Rect(sw * 0.2f, sh * 0.25f, sw * 0.6f, sh * 0.48333f);
		mainText = new Rect(sw * 0.2f, sh * 0.275f, sw * 0.6f, sh * 0.2f);
		hintText = new Rect(sw * 0.2f, sh * 0.5f, sw * 0.6f, sh * 0.09f);
		leftButton = new Rect(sw * 0.225f, sh * 0.6f, sw * 0.25f, sh * 0.08333f);
		rightButton = new Rect(sw * 0.525f, sh * 0.6f, sw * 0.25f, sh * 0.08333f);
		centerButton = new Rect(sw * 0.375f, sh * 0.6f, sw * 0.25f, sh * 0.08333f);

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		style.font = font;
		style.alignment = TextAnchor.MiddleCenter;
		style.fontSize = (int)(sh * 0.06f);
		style.normal.textColor = Color.white;
		style.wordWrap = true;
		
		buttonStyle.font = font;
		buttonStyle.alignment = TextAnchor.MiddleCenter;
		buttonStyle.fontSize = (int)(sh * 0.06f);
		buttonStyle.normal.textColor = Color.black;
	}
	
	public void  Show ()
	{		
		if(stable == null)
		{
			stable = WorldSession.stableMaster;
		}
		
		GUI.DrawTexture(windowDecorationRect, stable.mainDecoration);
		
		switch(WorldSession.stableMaster.messageWindowState)
		{
		case StableMessageState.MESSAGE_BUY_SLOT:
			ShowBuySlotWindow();
			break;
		}
	}
	
	private void  ShowBuySlotWindow ()
	{
		GUI.Label(mainText,	"Do you want buy new slot?", style);
		GUI.Label(hintText,	"Slot cost: 350.000f gold", style);
		
		stable.stableMasterToolkit.DrawTexture(leftButton, "EmptyButtom.png");
		if(GUI.Button(leftButton, "Buy Slot", buttonStyle))
		{
			BuySlot();
			return;
		}
		
		stable.stableMasterToolkit.DrawTexture(rightButton, "EmptyButtom.png");
		if(GUI.Button(rightButton, "Cancel", buttonStyle))
		{
			CloseWindow();
			return;
		}
	}
	
	private void  BuySlot ()
	{
		WorldSession.stableMaster.BuyStableSlot();
		CloseWindow();
	}
	
	private void  CloseWindow ()
	{
		WorldSession.stableMaster.SetWindowState(StableManagerState.MAIN_WINDOW);
	}
}