﻿using UnityEngine;
using System.Collections;

public enum StableManagerState
{
	MAIN_WINDOW		=	1,
	PET_INFO		=	2,
	WARNING_WINDOW	=	3
};

public enum StableMessageState
{
	MESSAGE_BUY_SLOT	=	1,
	MESSAGE_ERROR		=	2	
};

public enum StableResultCode
{
	STABLE_ERR_MONEY 			=	0x01, // "you don't have enough money" 
	STABLE_ERR_STABLE 			=	0x06, // currently used in most fail cases
	STABLE_SUCCESS_STABLE 		=	0x08, // stable success
	STABLE_SUCCESS_UNSTABLE 	=	0x09, // unstable/swap success
	STABLE_SUCCESS_BUY_SLOT 	=	0x0A, // buy slot success
	STABLE_ERR_EXOTIC 			=	0x0C, // "you are unable to control exotic creatures" 
};

public class StablePet
{
	public uint petNumber = 0;
	public uint petEntry = 0;
	public uint petLevel = 0;
	public string petName = "";
	public byte stableSlot = 0;
}

public class StableMaster : MonoBehaviour
{
	private StableMainWindow stableMainWindow;
	private StablePetInfoWindow stablePetInfoWindow;
	private StableMessageWindow stableMessageWindow;
	
	private bool  isShow = false;
	private StableManagerState windowState = StableManagerState.MAIN_WINDOW;
	public StableMessageState messageWindowState = StableMessageState.MESSAGE_BUY_SLOT;
	
	private ulong npcGuid = 0;
	private byte petCount = 0;
	public byte stableSlots = 0;
	
	public StablePet[] stableArray = new StablePet[5];
	
	public StablePet selectedPet;
	public MobTemplate mobTemplate = null;
	public Texture2D mobIcon = null;
	
	public TexturePacker stableMasterToolkit;
	public Texture2D mainDecoration;
	
	
	StableMaster ()
	{
		stableMainWindow = new StableMainWindow();
		stablePetInfoWindow = new StablePetInfoWindow();
		stableMessageWindow = new StableMessageWindow();
		for(byte i = 0; i < 5; i++)
			stableArray[i] = new StablePet();
	}
	
	public void  ReadSlotsInfo ( WorldPacket pkt )
	{
		npcGuid = pkt.ReadReversed64bit();
		petCount = pkt.Read();
		stableSlots = pkt.Read();
		
		if(WorldSession.player.pet != null)
		{
			stableArray[0].petNumber = pkt.ReadReversed32bit();
			stableArray[0].petEntry = pkt.ReadReversed32bit();
			stableArray[0].petLevel = pkt.ReadReversed32bit();
			stableArray[0].petName = pkt.ReadString();
			stableArray[0].stableSlot = pkt.Read();
			SendQueryPetInfo(stableArray[0].petEntry);
		}
		
		for(byte pos = 1; pos <= petCount; pos++)
		{
			stableArray[pos].petNumber = pkt.ReadReversed32bit();
			stableArray[pos].petEntry = pkt.ReadReversed32bit();
			stableArray[pos].petLevel = pkt.ReadReversed32bit();
			stableArray[pos].petName = pkt.ReadString();
			stableArray[pos].stableSlot = pkt.Read();
			SendQueryPetInfo(stableArray[pos].petEntry);
		}
		
		Show();
	}
	
	public void  SendQueryPetInfo ( uint entry )
	{
		ulong guid = 0;
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_CREATURE_QUERY);
		pkt.Add(entry);
		pkt.Add(guid);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  PutPetToStable ()
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_STABLE_PET);
		pkt.Add(npcGuid);
		RealmSocket.outQueue.Add(pkt);
		Close();
	}
	
	public void  GetPetFromStable ( uint number )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_UNSTABLE_PET);
		pkt.Add(npcGuid);
		pkt.Add(number);
		RealmSocket.outQueue.Add(pkt);
		Close();
	}
	
	public void  SwapPetsInStable ( uint number )
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_STABLE_SWAP_PET);
		pkt.Add(npcGuid);
		pkt.Add(number);
		RealmSocket.outQueue.Add(pkt);
		Close();
	}
	
	public void  BuyStableSlot ()
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_BUY_STABLE_SLOT);
		pkt.Add(npcGuid);
		RealmSocket.outQueue.Add(pkt);
		Close();
	}
	
	
	void  OnGUI ()
	{
		if(!isShow)
			return;
		
		switch(windowState)
		{
		case StableManagerState.MAIN_WINDOW:
			stableMainWindow.Show();
			break;
		case StableManagerState.PET_INFO:
			stablePetInfoWindow.Show();
			break;
		case StableManagerState.WARNING_WINDOW:
			stableMessageWindow.Show();
			break;
		}
	}
	
	private void  Show ()
	{
		stableMasterToolkit = new TexturePacker("StableMaster");
		mainDecoration = Resources.Load<Texture2D>("GUI/BigTextures/DialogScreenBackground");
		
		windowState = StableManagerState.MAIN_WINDOW;
		isShow = true;
	}
	
	public void  Close ()
	{
		windowState = StableManagerState.MAIN_WINDOW;
		isShow = false;
		
		WorldSession.interfaceManager.hideInterface = false;
		WorldSession.player.interf.stateManager();
		WorldSession.player.blockTargetChange = false;
		
		stableMasterToolkit.Destroy();
		stableMasterToolkit = null;
		mainDecoration = null;
		
		Resources.UnloadUnusedAssets();
	}
	
	public void  SetWindowState ( StableManagerState newState )
	{
		windowState = newState;
	}
	
	public void  ShowPetWindow ()
	{
		SetWindowState(StableManagerState.PET_INFO);
	}
	
	public void  ShowMessageWindow ()
	{
		if(npcGuid != WorldSession.player.guid)
		{
			windowState = StableManagerState.WARNING_WINDOW;
		}
	}
}