﻿using UnityEngine;

public class StablePetInfoWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Rect windowDecorationRect;
	private Rect textAreaRect;
	private Rect textField;
	private string infoText = "";
	
	private Rect takeThisButton;
	private Rect exitButton;
	
	private Rect playerPet;
	
	private GUIStyle style = new GUIStyle();
	private GUIStyle buttonStyle = new GUIStyle();
	
	private StableMaster stable = null;
	private bool  loadData = true;
	
	public StablePetInfoWindow ()
	{
		windowDecorationRect = new Rect(sw * 0.125f, sh * 0.16667f, sw * 0.75f, sh * 0.65f);
		textAreaRect = new Rect(sw * 0.4f, sh * 0.205f, sw * 0.45f, sh * 0.57f);
		textField = new Rect(sw * 0.42f, sh * 0.215f, sw * 0.41f, sh * 0.54f);
		takeThisButton = new Rect(sw * 0.145f, sh * 0.6f, sw * 0.235f, sh * 0.08f);
		exitButton = new Rect(sw * 0.145f, sh * 0.7f, sw * 0.235f, sh * 0.08f);
		playerPet = new Rect(sw * 0.2f, sh * 0.3f, sw * 0.125f, sw * 0.125f);

		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		style.font = font;
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = (int)(sh * 0.055f);
		style.normal.textColor = Color.black;
		style.wordWrap = true;
		
		buttonStyle.font = font;
		buttonStyle.alignment = TextAnchor.MiddleCenter;
		buttonStyle.fontSize = (int)(sh * 0.06f);
		buttonStyle.normal.textColor = Color.black;
	}
	
	public void  Show ()
	{
		StableMaster stable = WorldSession.stableMaster;
		if(loadData)
		{
			infoText = "Name: " + stable.selectedPet.petName;
			infoText += "\nType: " + DefaultTabel.petFamilyName[stable.mobTemplate.family.ToString()];
			infoText += "\nLevel: " + stable.selectedPet.petLevel;
			
			int selectedPetFamityID = WorldSession.player.interf.GetActualTallentTree();
			switch(selectedPetFamityID)
			{
			case 0:
				infoText += "\nTrait Path: Viciousness";
				break;
			case 1:
				infoText += "\nTrait Path: Perseverance";
				break;
			case 2:
				infoText += "\nTrait Path: Devious";
				break;
			}
			
			loadData = false;
		}
		
		if(stable != null)
		{
			GUI.DrawTexture(windowDecorationRect, stable.mainDecoration);
			stable.stableMasterToolkit.DrawTexture(textAreaRect, "NotePannel.png");
			GUI.Label(textField, infoText, style);
			
			stable.stableMasterToolkit.DrawTexture(takeThisButton, "EmptyButtom.png");
			PetAction();
			
			stable.stableMasterToolkit.DrawTexture(exitButton, "EmptyButtom.png");
			if(GUI.Button(exitButton, "Close", buttonStyle))
			{
				CloseWindow();
			}
			
			stable.stableMasterToolkit.DrawTexture(playerPet, "SquareButton.png");
			GUI.DrawTexture(playerPet, WorldSession.stableMaster.mobIcon);
		}
	}
	
	private void  PetAction ()
	{
		StableMaster stable = WorldSession.stableMaster;
		if(stable.selectedPet.stableSlot == 1)
		{
			if(GUI.Button(takeThisButton, "Store Pet", buttonStyle))
			{
				StorePet();
			}
		}
		else 
			if(stable.stableArray[0].petEntry == 0)
		{
			if(GUI.Button(takeThisButton, "Take Out", buttonStyle))
			{
				TakePet();
			}
		}
		else
		{
			if(GUI.Button(takeThisButton, "Swap Pet", buttonStyle))
			{
				SwapPets();
			}
		}
	}
	
	private void  TakePet ()
	{
		WorldSession.stableMaster.GetPetFromStable(WorldSession.stableMaster.selectedPet.petNumber);
		CloseWindow();
	}
	
	private void  SwapPets ()
	{
		WorldSession.stableMaster.SwapPetsInStable(WorldSession.stableMaster.selectedPet.petNumber);
		CloseWindow();
		CloseWindow();
	}
	
	private void  StorePet ()
	{
		WorldSession.stableMaster.PutPetToStable();
		CloseWindow();
	}
	
	private void  CloseWindow ()
	{
		infoText = "";
		loadData = true;
		WorldSession.stableMaster.mobTemplate = null;
		WorldSession.stableMaster.mobIcon = null;
		WorldSession.stableMaster.SetWindowState(StableManagerState.MAIN_WINDOW);
	}
}