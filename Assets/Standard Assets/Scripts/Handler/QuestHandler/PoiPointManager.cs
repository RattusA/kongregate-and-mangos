﻿using System.Collections.Generic;

public class PoiPointManager
{
	public uint poiID; //The "pointID" of the "point of interest" contained in the sister table quest_poi_points
	public Dictionary<uint, QuestStage> poiStages = new Dictionary<uint, QuestStage>();
	
	public QuestStage getQuestStage ( uint id )
	{
		QuestStage ret = null;
		
		if(poiStages.ContainsKey(id))
		{
			poiStages.TryGetValue(id, out ret);
		}
		return ret;
	}
}