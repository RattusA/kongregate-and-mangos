using UnityEngine;
using System.Collections.Generic;

public class QuestManager
{
	MainPlayer player;
	public List<quest> quests = new List<quest>();
	
	int maxNrQuest = (int)QuestSlotOffsets.MAX_QUEST_OFFSET;
	int firstQuestEntry = (int)UpdateFields.EUnitFields.PLAYER_QUEST_LOG_1_1;
	int questIdOffset = (int)QuestSlotOffsets.QUEST_ID_OFFSET;
	
	uint qstId=0;
	
	public uint markedQuest;
	public QuestArrowController questArrowController;
	
	public QuestManager ( MainPlayer plr )
	{
		player = plr;
		byte questCount = getQuestCount();
		markedQuest = 0;
		if(questCount > 0)
		{
			markedQuest = getQuestBySlotId((byte)(questCount-1));
		}
		GameObject questArrow = Resources.Load<GameObject>("prefabs/QuestArrow");
		if(questArrow)
		{
			questArrow = GameObject.Instantiate(questArrow);
			questArrow.name = "QuestArrow";
			questArrowController = questArrow.transform.GetComponent<QuestArrowController>();
			questArrowController.gameObject.transform.parent = player.gameObject.transform.parent;
			questArrowController.gameObject.transform.position = player.gameObject.transform.position;
		}
	}
	
	public void  getPlayerListQuests ()
	{
		quests.Clear();
		
		for(int slot = 0; slot < 25; slot++)
		{
			qstId = player.GetUInt32Value((int)(firstQuestEntry + slot * maxNrQuest + questIdOffset));
			//	Debug.Log("World: Found Quest: "+qstId+"  slot:"+slot);
			if(qstId > 0)
			{
				quest qst = new quest();
				qst.questId =qstId;// Slots[i].entry;
				quests.Add(qst);
				
				// make a request for questInfo
				ulong[] param = new ulong[2];
				param[0] = qstId;
				player.session.handleOpcode(OpCodes.CMSG_QUEST_QUERY,param);
			}
		}	
	}
	
	public QuestStatus questState ( uint qstId )
	{
		uint ret = 3;
		for(int slot = 0; slot < 25; slot++)
		{
			if(qstId == player.GetUInt32Value((int)(firstQuestEntry + slot * maxNrQuest + questIdOffset)))
			{
				ret = player.GetUInt32Value((int)(firstQuestEntry + slot * maxNrQuest + QuestSlotOffsets.QUEST_STATE_OFFSET));
				break;
			}
		}
		return (QuestStatus)ret;
	}	
	
	public ushort[] mobsLeft ( uint qstId )
	{
		ushort[] ret = null;
		ByteBuffer buffer = new ByteBuffer();
		for(int slot = 0; slot < 25; slot++)
		{
			if(qstId == player.GetUInt32Value((int)(firstQuestEntry + slot * maxNrQuest + QuestSlotOffsets.QUEST_ID_OFFSET)))
			{
				buffer.Append(player.GetUInt32Value((int)(firstQuestEntry + slot * maxNrQuest + QuestSlotOffsets.QUEST_COUNTS_OFFSET)));
				ret = new ushort[4];
				for(int j = 0; j < 4; j++)
				{
					ret[j] = (ushort)buffer.ReadType(2);
				}
				break;
			}
		}
		return ret;
	}
	
	public void  showPlayerQuestDetails ( uint questID ,   Rect questsRightRect )
	{
		GUISkin someskin2 = player.newskin;
		//		someskin2.label.font = Resources.Load("fonts/font20");
		//for smaller font
		someskin2.GetStyle("LabelSmall").padding.left = (int)(Screen.width*0.05f);
		someskin2.GetStyle("LabelSmall").padding.right = (int)(Screen.width*0.05f);
		someskin2.GetStyle("LabelSmall").alignment = TextAnchor.UpperCenter;
		
		//for bigger font
		someskin2.GetStyle("LabelBig").padding.left = (int)(Screen.width*0.05f);
		someskin2.GetStyle("LabelBig").padding.right = (int)(Screen.width*0.05f);
		someskin2.GetStyle("LabelBig").alignment = TextAnchor.UpperCenter;
		
		quest qst = getQuest(questID);
		ushort[] req;
		GUI.Label( new Rect(questsRightRect.x, questsRightRect.y*1.2f							,questsRightRect.width	, questsRightRect.height*0.2f)	,"Title: "+qst.title,	someskin2.GetStyle("LabelBig")); 
		someskin2.GetStyle("LabelBig").alignment = TextAnchor.UpperLeft;
		//someskin2.label.font = Resources.Load("fonts/font18");
		GUI.Label( new Rect(questsRightRect.x, questsRightRect.y+questsRightRect.height*0.15f	,questsRightRect.width	, 4*questsRightRect.height*0.2f)	,"Details:", someskin2.GetStyle("LabelBig")); 
		//someskin2.label.font = Resources.Load("fonts/font14");
		someskin2.GetStyle("LabelSmall").wordWrap = true;
		GUI.Label( new Rect(questsRightRect.x, questsRightRect.y+questsRightRect.height*0.22f	,questsRightRect.width	, 4*questsRightRect.height*0.2f)	,""+qst.details, someskin2.GetStyle("LabelSmall"));
		//someskin2.label.font = Resources.Load("fonts/font18");
		GUI.Label( new Rect(questsRightRect.x, questsRightRect.y+questsRightRect.height*0.64f	,questsRightRect.width	, 4*questsRightRect.height*0.2f)	,"Objectives: ", someskin2.GetStyle("LabelBig"));
		//someskin2.label.font = Resources.Load("fonts/font14");
		GUI.Label( new Rect(questsRightRect.x, questsRightRect.y+questsRightRect.height*0.7f	,questsRightRect.width	, 4*questsRightRect.height*0.2f)	,""+qst.objectives, someskin2.GetStyle("LabelSmall"));
		
		req = mobsLeft(questID);
		ushort i = req[1];
		int offset = 0;
		req[1] = req[0];
		req[0] = i;
		for(i = 0; i < 4; i++)
		{
			if(qst.reqCreatureOrGold[i] > 0)
			{
				//					someskin2.label.font = Resources.Load("fonts/font14_2");
				//					someskin2.label.font.material.color = Color(0,1,0);
				GUI.Label( new Rect(questsRightRect.x, 
				                    questsRightRect.y + questsRightRect.height*0.80f + (offset++)*someskin2.label.fontSize,
				                    questsRightRect.width, 
				                    4*questsRightRect.height*0.2f),
				          req[i] + " / " + qst.reqCreatureOrGoldCount[i] + 
				          (questState(questID) == QuestStatus.QUEST_STATUS_COMPLETE ? "  Completed!" : ""),
				          someskin2.label); 
			}
		}
		
		for(i = 0; i < 6; i++)
		{
			if(qst.reqItemId[i] > 0)
			{
				uint itmCount =  player.itemManager.GetItemsTotalCount(qst.reqItemId[i]);
				//					someskin2.label.font = Resources.Load("fonts/font14_2");
				//					someskin2.label.font.material.color = Color(0,1,0);
				GUI.Label( new Rect(questsRightRect.x, 
				                    questsRightRect.y + questsRightRect.height*0.80f + (offset++)*someskin2.label.fontSize,
				                    questsRightRect.width, 
				                    4*questsRightRect.height*0.2f),
				          itmCount + " / " + qst.reqItemCount[i] + 
				          (questState(questID) == QuestStatus.QUEST_STATUS_COMPLETE ? "  Completed!" : ""),
				          someskin2.label);
			}
		}
	}

	/////////////////
	//    uint32 GetQuestSlotQuestId(uint16 slot) const { return GetUInt32Value((int)PLAYER_QUEST_LOG_1_1 + slot * MAX_QUEST_OFFSET + QUEST_ID_OFFSET); }
	//////////////
	// max quests 25
	public byte getQuestCount ()
	{
		byte count = 0;
		
		for(int slot=0;slot<25;slot++)
		{
			uint entry = player.GetUInt32Value((int)(firstQuestEntry + slot * maxNrQuest + QuestSlotOffsets.QUEST_ID_OFFSET));
			if(entry > 0)
				count++;
		}
		return count;
	}
	
	public byte getQuestCount ( Unit player )
	{
		byte count = 0;
		
		for(int slot=0;slot<25;slot++)
		{
			if(player.GetUInt32Value((int)(firstQuestEntry + slot * maxNrQuest + QuestSlotOffsets.QUEST_ID_OFFSET)) > 0)
				count++;
		}
		return count;
	}
	
	
	public quest getQuest ( uint id )
	{
		quest ret = null;
		foreach(quest q in quests)
		{
			if(q.questId == id)
			{
				ret = q;
				break;
			}
		}
		return ret;
	}
	
	public int getQuestIndex ( uint id )
	{
		int ret = -1;
		for(int i = 0; i < quests.Count; i++)
		{
			if(quests[i].questId == id)
			{
				ret = i;
				break;
			}
		}
		return ret;
	}
	
	byte getSlot ( uint entry )
	{
		byte ret = 25;
		for(byte slot = 0; slot < 25; slot++)
		{
			if(player.GetUInt32Value((int)(firstQuestEntry + slot * maxNrQuest + QuestSlotOffsets.QUEST_ID_OFFSET))== entry)
			{
				ret = slot;
				break;
			}
		}
		return ret;
		
	}
	
	public bool hasSlotWithQuestId ( uint questId )
	{
		bool ret = true;
		byte questSlot = getSlot(questId);
		if(questSlot == 25)
			ret = false;
		return ret;
	}
	
	public uint getQuestBySlotId ( byte slot )
	{
		return player.GetUInt32Value((int)(firstQuestEntry + slot * maxNrQuest + QuestSlotOffsets.QUEST_ID_OFFSET));
	}
	
	public void  setMarkedQuest ( uint questId )
	{
		markedQuest = questId;
		if(markedQuest == 0 && getQuestCount() > 0)
		{
			questId = getQuestBySlotId((byte)(getQuestCount()-1));
		}
		markedQuest = questId;
		//questArrowController.questArrowModel.SetActive(true);
		questArrowController.setQuestArrow(questId);
	}
	
	public bool isQuestCompleteOrFailed ( uint questId )
	{
		bool ret = true;
		byte slot = getSlot(questId);
		if(player.GetUInt32Value((int)(firstQuestEntry + slot*maxNrQuest + QuestSlotOffsets.QUEST_STATE_OFFSET)) == 0)
		{
			ret = false;
		}
		return ret;
	}
	
	public void  removeQuest ( uint id )
	{
		for(int i = 0; i < quests.Count; i++)
		{
			if(id == quests[i].questId)
			{	
				Debug.Log("Quest lngth " + quests.Count + " id is " + id + " slot is " + i);
				quests.RemoveAt(i);
				Debug.Log("World: CMSG_QUESTLOG_REMOVE_QUEST: slot:"+i);
				ulong[] param = new ulong[1];
				param[0] = (ulong)i;
				player.session.handleOpcode(OpCodes.CMSG_QUESTLOG_REMOVE_QUEST, param);
				
				
				WorldPacket pkt = new WorldPacket();
				pkt.SetOpcode(OpCodes.CMSG_QUESTGIVER_STATUS_MULTIPLE_QUERY);
				RealmSocket.outQueue.Add(pkt);	
				getPlayerListQuests();
				return;				
			}
			Debug.Log("Quest lngth " + quests.Count + " id is " + id + " slot is " + i);
		}
	}
	
}