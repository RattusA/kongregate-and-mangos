﻿using UnityEngine;

public class QuestRewardItemInfoWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Rect mainWindowRect;
	private Rect topDecorationRect;
	private Rect botDecorationRect;
	
	private Rect itemNameRect;
	private Rect itemDecoretionRect;
	private Rect selectedItemRect;
	
	private Rect itemInfoRect;
	private Rect itemInfoZoneRect;
	
	private Rect closeButtonRect;
	private Rect selectButtonRect;
	private Rect closeXButtonRect;
	
	public bool active = false;
	private string itemName = "";
	private Texture itemImage;
	private GUIStyle style;
	private int selectedItemIndex;
	
	private TexturePacker questRewardToolkit;
	private Texture2D mainDecoration;
	private Texture2D textDecoration;
	
	private Texture2D topDecoration;
	private ScrollZone TextZone = new ScrollZone();

	public QuestRewardItemInfoWindow()
	{
		mainWindowRect = new Rect(sw * 0.1f, sh * 0.25f, sw * 0.7f, sh * 0.7f);
		topDecorationRect = new Rect(sw * 0.1175f, sh * 0.285f, sw * 0.665f, sh * 0.07f);
		botDecorationRect = new Rect(sw * 0.1105f, sh * 0.425f, sw * 0.175f, sh * 0.315f);
		itemNameRect = new Rect(sw * 0.18f, sh * 0.29f, sw * 0.55f, sh * 0.06f);
		itemDecoretionRect = new Rect(sw * 0.117f, sh * 0.365f, sw * 0.092f, sw * 0.092f);
		selectedItemRect = new Rect(sw * 0.121f, sh * 0.369f, sw * 0.0767f, sw * 0.0767f);
		itemInfoRect = new Rect(sw * 0.289f, sh * 0.369f, sw * 0.49f, sh * 0.56f);
		itemInfoZoneRect = new Rect(sw * 0.31f, sh * 0.39f, sw * 0.46f, sh * 0.53f);
		closeButtonRect = new Rect(sw * 0.121f, sh * 0.845f, sw * 0.154f, sh * 0.0651f);
		selectButtonRect = new Rect(sw * 0.121f, sh * 0.747f, sw * 0.154f, sh * 0.0651f);
		closeXButtonRect = new Rect(sw * 0.77f, sh * 0.225f, sw * 0.056f, sw * 0.056f);
	}

	public void  CreateItemInfoWindow ( int itemIndex )
	{
		selectedItemIndex = itemIndex;
		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		style = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.06f, Color.white, true, FontStyle.Normal);
		
		questRewardToolkit = new TexturePacker("GossipAtlas");
		mainDecoration = Resources.Load<Texture2D>("GUI/BigTextures/DialogScreenBackground");
		textDecoration = Resources.Load<Texture2D>("GUI/BigTextures/InfoScreenBackground");
		
		topDecoration = Resources.Load<Texture2D>("GUI/BigTextures/LongDecorations/Decoration_1"); 
		
		active = true;
		Item itm = AppCache.sItems.GetItem(WorldSession.player.interf.qst.rewChoiceItemId[itemIndex]);
		string image = DefaultIcons.GetItemIcon(WorldSession.player.interf.qst.rewChoiceItemId[itemIndex]);
		image = image.Substring(0, image.LastIndexOf('.'));
		itemImage = Resources.Load<Texture>("Icons/" + image);
		
		itemName = WorldSession.player.interf.npc.questGiver.itemsName2[itemIndex];
		
		TextZone.SetFontSizeByRatio(1.2f);
		TextZone.InitSkinInfo(WorldSession.player.newskin.GetStyle("DescriptionAndStats"));
		TextZone.CreateScrollZoneWithText(itemInfoZoneRect, itm.GetItemBasicStats(), "#000000ff");
	}
	
	public void  DrawIntemInfoWindow ()
	{
		GUI.DrawTexture(mainWindowRect, mainDecoration);
		GUI.DrawTexture(topDecorationRect, topDecoration);
		questRewardToolkit.DrawTexture(botDecorationRect, "WindowDecoration.png");
		
		
		GUI.Label(itemNameRect, itemName, style);
		GUI.DrawTexture(selectedItemRect, itemImage);
		questRewardToolkit.DrawTexture(itemDecoretionRect, "ItemDecoration.png");
		GUI.DrawTexture(itemInfoRect, textDecoration);
		
		questRewardToolkit.DrawTexture(closeButtonRect, "CloseButton.png");
		if(GUI.Button(closeButtonRect, "", GUIStyle.none))
		{
			CloseItemInfoWindow();
			return;
		}
		
		if(WorldSession.player.interf.qstState == QuestStatus.QUEST_STATUS_COMPLETE)
		{
			questRewardToolkit.DrawTexture(selectButtonRect, "GetItemButton.png");
			if(GUI.Button(selectButtonRect, "", GUIStyle.none))
			{
				SelectItem();
			}
		}
		
		TextZone.DrawTraitsInfo();
	}
	
	public void  CloseItemInfoWindow ()
	{
		active = false;
		TextZone.RemoveScrollZone();
		WorldSession.player.interf.DisableNPCQuestWindow(false);
		
		questRewardToolkit.Destroy();
		questRewardToolkit = null;
		
		mainDecoration = null;
		textDecoration = null;
		topDecoration = null;
	}
	
	private void  SelectItem ()
	{
		active = false;
		WorldSession.player.interf.completeQuestText.hidden = false;
		WorldSession.player.interf.completeQuestButton.hidden = false;
		WorldSession.player.interf.completeQuestButton.info = selectedItemIndex;
		TextZone.RemoveScrollZone();
		WorldSession.player.interf.DisableNPCQuestWindow(false);
	}
}