﻿using UnityEngine;
using System.Collections.Generic;

public enum QuestSlotOffsets
{
	QUEST_ID_OFFSET         = 0,
	QUEST_STATE_OFFSET      = 1,							// enum QuestSlotStateMask
	QUEST_COUNTS_OFFSET     = 2,                            // 2 and 3
	QUEST_TIME_OFFSET       = 4,
	MAX_QUEST_OFFSET  		= 5
};

//static byte MAX_QUEST_OFFSET = 5;
//#define MAX_QUEST_OFFSET 5

public enum QuestSlotStateMask
{
	QUEST_STATE_NONE     = 0x0000,
	QUEST_STATE_COMPLETE = 0x0001,
	QUEST_STATE_FAIL     = 0x0002,
	
};

public enum QuestUpdateState
{
	QUEST_UNCHANGED  = 0,
	QUEST_CHANGED    = 1,
	QUEST_NEW		= 2		
};

public enum QuestStatus
{
	QUEST_STATUS_NONE           = 0,
	QUEST_STATUS_COMPLETE       = 1,
	QUEST_STATUS_UNAVAILABLE    = 2,
	QUEST_STATUS_INCOMPLETE     = 3,
	QUEST_STATUS_AVAILABLE      = 4,                        // unused in fact
	QUEST_STATUS_FAILED         = 5,
	MAX_QUEST_STATUS
};

public enum __QuestGiverStatus
{
	DIALOG_STATUS_NONE                     = 0,
	DIALOG_STATUS_UNAVAILABLE              = 1,
	DIALOG_STATUS_LOW_LEVEL_AVAILABLE      = 2,
	DIALOG_STATUS_LOW_LEVEL_REWARD_REP     = 3,
	DIALOG_STATUS_LOW_LEVEL_AVAILABLE_REP  = 4,
	DIALOG_STATUS_INCOMPLETE               = 5,
	DIALOG_STATUS_REWARD_REP               = 6,
	DIALOG_STATUS_AVAILABLE_REP            = 7,
	DIALOG_STATUS_AVAILABLE                = 8,
	DIALOG_STATUS_REWARD2                  = 9,             // no yellow dot on minimap
	DIALOG_STATUS_REWARD                   = 10             // yellow dot on minimap
};

public enum QuestFailedReasons
{
	INVALIDREASON_DONT_HAVE_REQ                       = 0,  // this is default case
	INVALIDREASON_QUEST_FAILED_LOW_LEVEL              = 1,  // You are not high enough level for that quest.
	INVALIDREASON_QUEST_FAILED_WRONG_RACE             = 6,  // That quest is not available to your race.
	INVALIDREASON_QUEST_ALREADY_DONE                  = 7,  // You have completed that quest.
	INVALIDREASON_QUEST_ONLY_ONE_TIMED                = 12, // You can only be on one timed quest at a time.
	INVALIDREASON_QUEST_ALREADY_ON                    = 13, // You are already on that quest.
	INVALIDREASON_QUEST_FAILED_EXPANSION              = 16, // This quest requires an expansion enabled account.
	INVALIDREASON_QUEST_ALREADY_ON2                   = 18, // You are already on that quest.
	INVALIDREASON_QUEST_FAILED_MISSING_ITEMS          = 21, // You don't have the required items with you. Check storage.
	INVALIDREASON_QUEST_FAILED_NOT_ENOUGH_MONEY       = 23, // You don't have enough money for that quest.
	INVALIDREASON_QUEST_FAILED_TOO_MANY_DAILY_QUESTS  = 26, // You have already completed 25 daily quests today.
	INVALIDREASON_QUEST_FAILED_CAIS                   = 27, // You cannot complete quests once you have reached tired time.
	INVALIDREASON_DAILY_QUEST_DONE_TODAY              = 29  // You have completed that daily quest today.
};

public enum QuestShareMessages
{
	QUEST_PARTY_MSG_SHARING_QUEST           = 0,            // ERR_QUEST_PUSH_SUCCESS_S
	QUEST_PARTY_MSG_CANT_TAKE_QUEST         = 1,            // ERR_QUEST_PUSH_INVALID_S
	QUEST_PARTY_MSG_ACCEPT_QUEST            = 2,            // ERR_QUEST_PUSH_ACCEPTED_S
	QUEST_PARTY_MSG_DECLINE_QUEST           = 3,            // ERR_QUEST_PUSH_DECLINED_S
	QUEST_PARTY_MSG_BUSY                    = 4,            // ERR_QUEST_PUSH_BUSY_S
	QUEST_PARTY_MSG_LOG_FULL                = 5,            // ERR_QUEST_PUSH_LOG_FULL_S
	QUEST_PARTY_MSG_HAVE_QUEST              = 6,            // ERR_QUEST_PUSH_ONQUEST_S
	QUEST_PARTY_MSG_FINISH_QUEST            = 7,            // ERR_QUEST_PUSH_ALREADY_DONE_S
	QUEST_PARTY_MSG_CANT_BE_SHARED_TODAY    = 8,            // ERR_QUEST_PUSH_NOT_DAILY_S
	QUEST_PARTY_MSG_SHARING_TIMER_EXPIRED   = 9,            // ERR_QUEST_PUSH_TIMER_EXPIRED_S
	QUEST_PARTY_MSG_NOT_IN_PARTY            = 10,           // ERR_QUEST_PUSH_NOT_IN_PARTY_S
	QUESY_PARTY_MSG_DIFFERENT_SERVER_DAILY  = 11            // ERR_QUEST_PUSH_DIFFERENT_SERVER_DAILY_S
};

public enum __QuestTradeSkill
{
	QUEST_TRSKILL_NONE           = 0,
	QUEST_TRSKILL_ALCHEMY        = 1,
	QUEST_TRSKILL_BLACKSMITHING  = 2,
	QUEST_TRSKILL_COOKING        = 3,
	QUEST_TRSKILL_ENCHANTING     = 4,
	QUEST_TRSKILL_ENGINEERING    = 5,
	QUEST_TRSKILL_FIRSTAID       = 6,
	QUEST_TRSKILL_HERBALISM      = 7,
	QUEST_TRSKILL_LEATHERWORKING = 8,
	QUEST_TRSKILL_POISONS        = 9,
	QUEST_TRSKILL_TAILORING      = 10,
	QUEST_TRSKILL_MINING         = 11,
	QUEST_TRSKILL_FISHING        = 12,
	QUEST_TRSKILL_SKINNING       = 13,
	QUEST_TRSKILL_JEWELCRAFTING  = 14,
};



// values based at QuestInfo.dbc
public enum QuestTypes
{
	QUEST_TYPE_ELITE               = 1,
	QUEST_TYPE_LIFE                = 21,
	QUEST_TYPE_PVP                 = 41,
	QUEST_TYPE_RAID                = 62,
	QUEST_TYPE_DUNGEON             = 81,
	QUEST_TYPE_WORLD_EVENT         = 82,
	QUEST_TYPE_LEGENDARY           = 83,
	QUEST_TYPE_ESCORT              = 84,
	QUEST_TYPE_HEROIC              = 85,
	QUEST_TYPE_RAID_10             = 88,
	QUEST_TYPE_RAID_25             = 89
};

public enum QuestFlags
{
	// Flags used at server and sent to client
	QUEST_FLAGS_NONE           = 0x00000000,
	QUEST_FLAGS_STAY_ALIVE     = 0x00000001,                // Not used currently
	QUEST_FLAGS_PARTY_ACCEPT   = 0x00000002,                // If player in party, all players that can accept this quest will receive confirmation box to accept quest CMSG_QUEST_CONFIRM_ACCEPT/SMSG_QUEST_CONFIRM_ACCEPT
	QUEST_FLAGS_EXPLORATION    = 0x00000004,                // Not used currently
	QUEST_FLAGS_SHARABLE       = 0x00000008,                // Can be shared: Player::CanShareQuest()
	//QUEST_FLAGS_NONE2        = 0x00000010,                // Not used currently
	QUEST_FLAGS_EPIC           = 0x00000020,                // Not used currently - 1 quest in 3.3f
	QUEST_FLAGS_RAID           = 0x00000040,                // Not used currently
	QUEST_FLAGS_TBC            = 0x00000080,                // Not used currently: Available if TBC expansion enabled only
	QUEST_FLAGS_UNK2           = 0x00000100,                // Not used currently: _DELIVER_MORE Quest needs more than normal _q-item_ drops from mobs
	QUEST_FLAGS_HIDDEN_REWARDS = 0x00000200,                // Items and money rewarded only sent in SMSG_QUESTGIVER_OFFER_REWARD (not in SMSG_QUESTGIVER_QUEST_DETAILS or in client quest log(SMSG_QUEST_QUERY_RESPONSE))
	QUEST_FLAGS_AUTO_REWARDED  = 0x00000400,                // These quests are automatically rewarded on quest complete and they will never appear in quest log client side.
	QUEST_FLAGS_TBC_RACES      = 0x00000800,                // Not used currently: Blood elf/Draenei starting zone quests
	QUEST_FLAGS_DAILY          = 0x00001000,                // Daily quest. Can be done once a day. Quests reset at regular intervals for all players.
	QUEST_FLAGS_FLAGS_PVP      = 0x00002000,                // activates PvP on accept
	QUEST_FLAGS_UNK4           = 0x00004000,                // ? Membership Card Renewal
	QUEST_FLAGS_WEEKLY         = 0x00008000,                // Weekly quest. Can be done once a week. Quests reset at regular intervals for all players.
	QUEST_FLAGS_AUTOCOMPLETE   = 0x00010000,                // auto complete
	QUEST_FLAGS_UNK5           = 0x00020000,                // has something to do with ReqItemId and SrcItemId
	QUEST_FLAGS_UNK6           = 0x00040000,                // use Objective text as Complete text
	QUEST_FLAGS_AUTO_ACCEPT    = 0x00080000,                // quests in starting areas
};

public enum QuestSpecialFlags
{
	// Mangos flags for set SpecialFlags in DB if required but used only at server
	QUEST_SPECIAL_FLAG_REPEATABLE           = 0x001,        // |1 in SpecialFlags from DB
	QUEST_SPECIAL_FLAG_EXPLORATION_OR_EVENT = 0x002,        // |2 in SpecialFlags from DB (if required area explore, spell SPELL_EFFECT_QUEST_COMPLETE casting, table `*_script` command SCRIPT_COMMAND_QUEST_EXPLORED use, set from script DLL)
	QUEST_SPECIAL_FLAG_MONTHLY              = 0x004,        // |4 in SpecialFlags. Quest reset for player at beginning of month.
	
	// Mangos flags for internal use only
	QUEST_SPECIAL_FLAG_DELIVER              = 0x008,        // Internal flag computed only
	QUEST_SPECIAL_FLAG_SPEAKTO              = 0x010,        // Internal flag computed only
	QUEST_SPECIAL_FLAG_KILL_OR_CAST         = 0x020,        // Internal flag computed only
	QUEST_SPECIAL_FLAG_TIMED                = 0x040,        // Internal flag computed only
};

public class QuestStage
{
	public int objectiveIndex; //-1 = a talk quest (no objectives). 0 to 3 = kill mob objectives 4 to x = item gather objectives 
	public uint mapID; //The "mapID" is the ID of the map the POI resides on. 
	public uint mapAreaID; //This is the "zoneID" of the current POI. From WorldMapArea.dbc. 
	public uint floorID; //This is the AreaID of the POI, labeled incorrectly. 
	public uint unk3; //No idea what this is for. Assume a flag possibly. 
	public uint unk4; //1 = area, 3 = single point, 5 = unknown. 
	public List<Vector3> poiPositionsList = new List<Vector3>();//Vector3
}