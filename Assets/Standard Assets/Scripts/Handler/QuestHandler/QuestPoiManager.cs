﻿using System.Collections.Generic;

public class QuestPoiManager
{
	static public uint questID; //The ID of the quest targeted by the entry 
	static public Dictionary<uint, PoiPointManager> poiPointList = new Dictionary<uint, PoiPointManager>(); //PoiPointManager
	
	static public PoiPointManager getQuestPoiPoints ( uint id )
	{
		PoiPointManager ret = null;
		if(poiPointList.ContainsKey(id))
		{
			poiPointList.TryGetValue(id, out ret);
		}
		return ret;
	}
	
	static public QuestStage getQuestStage ( uint questId, uint poiId )
	{
		QuestStage ret = null;
		PoiPointManager poiPoints = getQuestPoiPoints(questId);
		if(poiPoints != null)
		{
			ret = poiPoints.getQuestStage(poiId);
		}
		return ret;
	}
	
}
