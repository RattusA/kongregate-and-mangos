﻿
public class quest
{
	public uint questId;
	
	public uint level;
	
	public string title;//name
	public string details;//detalii
	public string objectives;//objectives
	public string progress;
	public byte autoFinish;//auto finish
	public uint getQuestFlags;//quest flags
	public uint getSuggestedPlayers;
	public byte isFinished; // IsFinished? value is sent back to server in quest accept packet
	/*    QUEST_FLAGS_HIDDEN_REWARDS = 0x00000200 == 512    // Items and money rewarded only sent in SMSG_QUESTGIVER_OFFER_REWARD (not in SMSG_QUESTGIVER_QUEST_DETAILS or in client quest log(SMSG_QUEST_QUERY_RESPONSE))*/
	public uint questMethod;
	uint hiddenRewChoiceItems; // Rewarded chosen items hidden
	uint hiddenRewItems; // Rewarded items hidden
	uint hiddenRewMoney;// Rewarded money hidden
	uint hiddenRewXp;// Rewarded XP hidden
	
	//
	public uint getRewChoiceItemCount;	
	public uint[] rewChoiceItemId = new uint[6];
	public uint[] rewChoiceItemCount = new uint[6];
	public uint[] displayChoiceInfoId = new uint[6];	
	
	public uint getItemsCount;
	public uint[] rewItemId = new uint[6];
	public uint[] rewItemCount = new uint[6];
	public uint[] displayInfoId = new uint[6];
	
	// send rewMoneyMaxLevel explicit for max player level, else send RewOrReqMoney
	/*daca playerLevel == Max*/
	public uint getRewMoneyMaxLevel;
	//    else
	public uint getRewOrReqMoney;
	
	public uint xpValue; 
	// uint rewOrRegMoney;	
	
	public uint rewHonorAddition;
	public float rewHonorMultiplier;
	public uint rewSpell;
	public uint rewCastSpell;
	public uint charTitleId;
	public uint bonusTalents;
	uint bonusAreaPoints; // bonus arena points
	uint repRewShowMask;// rep reward show mask?
	
	public uint[] rewRepFaction = new uint[5];	
	public uint[] rewRepValueId = new uint[5];
	public uint[] rewRepValue = new uint[5];
	
	public uint questEmoteCount; ///QUEST_EMOTE_COUNT = 4
	
	public uint[] detailsEmote = new uint[4];
	public uint[] detailsEmoteDelay = new uint[4];
	
	
	//coordonate    
	public uint pointMapId;
	public float pointX;
	public float pointY;
	public uint pointOpt; 
	
	//detalii
	//	string title;
	//	string objectives;
	//	string details;
	public string endText;
	public string completedText;
	
	public uint[] reqCreatureOrGold = new uint[4];//#define QUEST_OBJECTIVES_COUNT 4
	public uint[] reqCreatureOrGoldCount = new uint[4];
	public uint[] reqSourceId = new uint[4];
	public uint[] reqSourceCount = new uint[4];
	
	public int getReqItemsCount;
	public uint[] reqItemId  = new uint[6];//#define QUEST_ITEM_OBJECTIVES_COUNT 6
	public uint[] reqItemCount = new uint[6];
	
	//string[] objectiveText = new string[4];//#define QUEST_OBJECTIVES_COUNT 4
	public string[] objectiveText = new string[6];
	
	public uint minLevel;// min required level to obtain (added for 3.3f). Assumed allowed (database) range is -1 to 255 (still using uint32, since negative value would not be of any known use for client)
	public uint zoneOrSort;// zone or sort to display in quest log
	public uint type; // quest type
	public uint nextQuestInChain; // client will request this quest from NPC, if not 0
	public uint rewXPId;// column index in QuestXP.dbc (row based on quest level)
	public uint scrItemId; // source item id
	public uint questFlags; //quest flags
	//uint charTitleId; // CharTitleId, new 2.4f.0, player gets this title (id from CharTitles) 
	public uint playersSlain; //players slain
	//uint bonusTalents; //bonus talents
	uint bonusArenaPoints; //bonus arena points
	uint rewRepShowMask; //rew rep show mask?
	public uint suggestedPlayers;
	public uint repObjectiveFaction;
	public uint rewOrRegMoney;
	public uint repObjectiveValue;
	public uint rewMoneyMaxLevel;
	
	public string offerRewText;
	public uint XPAmount;
	public uint moneyAmaunt;

	public quest ()
	{
		rewItemId 	  = new uint[6]; 
		rewItemCount  = new uint[6];
		displayInfoId = new uint[6];
		
		rewChoiceItemId		= new uint[6];
		rewChoiceItemCount  = new uint[6];
		displayChoiceInfoId = new uint[6];
		
	}
}
