﻿using UnityEngine;
using System.Collections.Generic;

public class QuestArrowController : MonoBehaviour
{
	PoiPointManager questPoi;
	uint questEntry;
	public GameObject questArrowModel;
	public bool  useQuestArrow;
	
	void  Start ()
	{
		setUseQuestArrow(PlayerPrefs.GetInt("UseQuestArrow", 1) == 1);
	}
	
	public void  setUseQuestArrow ( bool value )
	{
		useQuestArrow = value;
	}
	
	void  LateUpdate ()
	{
		questArrowModel.SetActive(false);
		if(useQuestArrow && !(WorldSession.player.HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_IN_COMBAT)))
		{
			QuestManager questManager = WorldSession.player.questManager;
			if(!questManager.hasSlotWithQuestId(questEntry))
				questManager.setMarkedQuest(0);
			Vector3 targetPoi = getTargetPoi();
			transform.LookAt(targetPoi);
		}
	}
	
	public void  setQuestArrow ( uint questId )
	{
		questEntry = questId;
		questPoi = QuestPoiManager.getQuestPoiPoints(questId);
	}
	
	Vector3 getTargetPoi ()
	{
		Vector3 playerPosition = gameObject.transform.parent.position;
		List<Vector3> targetPoiList = SelectPoi(WorldSession.player.questManager.isQuestCompleteOrFailed(questEntry));
		Vector3 targetPoi = Vector3.zero;
		float sqrMagnitudeOld = 0;
		if(targetPoiList.Count > 0)
		{
			for(int inx = 0; inx < targetPoiList.Count; inx++)
			{
				Vector3 poi = new Vector3(targetPoiList[inx].x,
				                          playerPosition.y,
				                          targetPoiList[inx].z);
				Vector3 distanceOffset = poi - playerPosition;
				float sqrMagnitude = distanceOffset.sqrMagnitude;
				if(sqrMagnitudeOld == 0 || sqrMagnitudeOld > sqrMagnitude)
				{
					sqrMagnitudeOld = sqrMagnitude;
					targetPoi = poi;
				}
			}
			float distance = Vector3.Distance(targetPoi, playerPosition);
			if(distance >= 20)
				questArrowModel.SetActive(true);
		}
		return targetPoi;
	}
	
	List<Vector3> SelectPoi( bool needQuestReceiver )
	{
		List<Vector3> ret = new List<Vector3>();
		if(questPoi != null)
		{
			foreach(QuestStage poi in questPoi.poiStages.Values)
			{
				if(poi.objectiveIndex == -1 && needQuestReceiver)
					ret.Add(poi.poiPositionsList[0]);
				if(poi.objectiveIndex >= 0 && !needQuestReceiver && isTheLocation(poi.mapID))
					ret.Add(poi.poiPositionsList[0]);
			}
		}
		return ret;
	}
	
	bool isTheLocation ( uint poiMapID )
	{
		bool  ret = false;
		uint curentMap = WorldSession.player.mapId;
		if(poiMapID == 0)
		{
			switch(curentMap)
			{
			case 0:
			case 35:
			case 25:
			case 723:
			case 609:
			case 586:
				ret = true;
				break;
			default:
				ret = false;
				break;
			}
		}
		else if(curentMap == poiMapID)
		{
			ret = true;
		}
		else
		{
			ret = false;
		}
		return ret;
	}
}