using UnityEngine;
using System.Collections.Generic;

public class NPCAnimationHandler : AnimationHandler {

	public Unit unitScript = null;
	
	public NPCAnimationHandler(Unit unit)
	{
		unitScript = unit;
		movementAnimations = new List<string>();
		actionAnimations = new List<string>();
		
		PrepareAnimations();
	}
	
	protected void PrepareAnimations()
	{
		foreach(AnimationState state in unitScript.GetComponent<Animation>())
		{
			if(state.name.IndexOf("hit") == 0)
			{
				actionAnimations.Add(state.name);
			}
			else
			{
				movementAnimations.Add(state.name);
			}
		}
	}
	
	public override void PlayMovementAnimation(string val)// ex : idle, run , walk, swim,fly, die
	{	
		if(!unitScript.GetComponent<Animation>()[val])
		{
			return;
		}

		if(currentMovementAnimation != val)
		{	
			if(unitScript.GetComponent<Animation>().IsPlaying("die"))
			{
				return;		
			}
			
			//for stealth and hex
			if(unitScript.stealth || unitScript.shapeshift)
			{
				unitScript.TransformParent.gameObject.BroadcastMessage("PlayMirroredMovementAnimation", val, SendMessageOptions.DontRequireReceiver);	
			}
			
			//for golems walk/run animations we need increased speed of animation (hardcoded like a bauss)
			if((val == "walk" || val == "run") && 
			   ((unitScript.transform.parent.name == "1351(Clone)") ||
			 	(unitScript.transform.parent.name == "1(Clone)") ||
			 	(unitScript.transform.parent.name == "381(Clone)") ||
			 	(unitScript.transform.parent.name == "503(Clone)") ||
			 	(unitScript.transform.parent.name == "607(Clone)")))
			{
				AnimationState _tempAnimation = unitScript.GetComponent<Animation>().CrossFadeQueued(val, 0.3f, QueueMode.PlayNow);
				_tempAnimation.speed = 5;
			}
			else
			{
				unitScript.GetComponent<Animation>().CrossFadeQueued(val, 0.3f, QueueMode.PlayNow);
			}
			currentMovementAnimation = val;
		}	
	}
	
	public override void PlayActionAnimation(string val)// ex: hit, gather,cast, shoot
	{
		int nr = Random.Range(1, 3);
		string animation_string = "hit" + nr;// hit and 3 must be defined in a Global variable
		
		if(val == "damage")
		{
			animation_string = val;
		}
		
		if(!unitScript.GetComponent<Animation>()[animation_string])
		{
			Debug.Log("Animation not found: " + animation_string);
			animation_string = "hit1";
		}
		
		unitScript.GetComponent<Animation>().Blend(animation_string, 5, 0.3f);
		currentActionAnimation = animation_string;
	}
	
	public override void ReplayMovementAnimation()
	{
		unitScript.GetComponent<Animation>().Rewind();
	}
}
