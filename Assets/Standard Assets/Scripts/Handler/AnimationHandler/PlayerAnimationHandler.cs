using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAnimationHandler : AnimationHandler {

	Player _player = null;
	
	public PlayerAnimationHandler(Player plr)
	{
		_player = plr;
		movementAnimations = new List<string>();
		actionAnimations = new List<string>();
	}

	void PrepareAnimations()
	{
		foreach(AnimationState state in _player.GetComponent<Animation>())
		{
			if(state.name.IndexOf("atk") > -1 || state.name.IndexOf("hit")>-1)
			{
				actionAnimations.Add(state.name);
			}
			else
			{
				movementAnimations.Add(state.name);
			}
		}

	}
	
	public string GetWeaponAnim(string val)
	{
		string _animation = "";
		uint _weapon_ranged;
		uint _weapon_main;
		uint _weapon_offhand;
		ushort aux;
		ItemEntry item = null;

		aux = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_18_ENTRYID;
		_weapon_ranged = _player.GetUInt32Value((int)aux);
		
		aux = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_17_ENTRYID;
		_weapon_offhand = _player.GetUInt32Value((int)aux);
		
		aux = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_16_ENTRYID;
		_weapon_main = _player.GetUInt32Value((int)aux);
		
		if(_weapon_ranged != 0 && _player.itemManager.RangeMode)
		{
			//if we have ranged weapon
			item = AppCache.sItems.GetItemEntry(_weapon_ranged);
			_animation = Player.weaponAnimations[item.SubClass]+"_"+val;
		}
		else if(_weapon_offhand > 0 || _weapon_main > 0)
		{
			//if we do not have ranged weapon
			if(_weapon_main > 0 && _weapon_offhand > 0)
			{
				//if we have 2 weapons/shield
				item = AppCache.sItems.GetItemEntry(_weapon_offhand);
				if(item != null && item.Class != (int)ItemClass.ITEM_CLASS_WEAPON)
				{
					//if we have shield
					item = AppCache.sItems.GetItemEntry(_weapon_main);
					_animation = Player.weaponAnimations[item.SubClass]+"_"+val;
				}
				else
				{
					//we have 2 weapons
					_animation = Player.weaponAnimations[21]+"_"+val;
				}
			}
			else
			{
				//single weapon
				if(_weapon_offhand > 0)
				{
					item = AppCache.sItems.GetItemEntry(_weapon_offhand);
					if(item.Class == 4 /*&& item.subClass == ITEM_SUBCLASS_ARMOR_BUCKLER || ITEM_SUBCLASS_ARMOR_SHIELD*/)//if its a shield
					{
						item = null;//we play only basic atks	if in offhand we have a shield
					}
				}
				if(_weapon_main > 0)
				{
					item = AppCache.sItems.GetItemEntry(_weapon_main);
				}
				
				if(item != null)
				{
					_animation = Player.weaponAnimations[item.SubClass];
					string race = dbs.sCharacterRace.GetRecord((int)_player.race).Name;
					if(_animation == "staff" && race == "Orc")
					{
						_animation = "spear" + "_" + val;
					}
					else
					{
						_animation += "_" + val;
					}
				}
			}
		}
		else
		{
			_animation = Player.weaponAnimations[13]+"_"+val;
		}
		
		
		if(item != null && _player.GetComponent<Animation>()[_animation] == null)
		{
			return (item.SubClass == 0 || item.SubClass == 4 || item.SubClass == 7 || item.SubClass == 15 || item.SubClass == 16) ? ("onehandsword_"+val) : ("twohandshammer_"+val);
		}
		
		return _animation;
	}
		/*	
0  	"onehandaxe",
1	"twohandshammer",
2	"bow",
3	"",
4	"onehandaxe",
5	"twohandshammer",
6	"twohandshammer",
7	"onehandsword",
8	"twohandsword",
9	"",
10	"staff",
11	"",
12	"",
13	"basic",
14	"",
15	"onehandsword",
16	"onehandsword",
17	"twohandhammer",
18	"crossbow",
19	"",
20	"twohandsword", //fising rod
21	"dual"
*/
		
		
	public override void PlayMovementAnimation(string val)
	{
		PlayMovementAnimationFromTime(val, 0.0f);
	}
	
	public override void PlayMovementAnimationFromTime(string val, float startTime)
	{	
		float fadeTime = 0;
		string initialVal = val;
		val = GetWeaponAnim(val);

		if(_player.GetComponent<Animation>()[val] == null)
		{
			val = initialVal;

			if(_player.GetComponent<Animation>()[val] == null)
			{
				return;
			}
		}

		string currentAnimationName = GetCurentAnimationName(_player.GetComponent<Animation>());
		if((!_player.mountManager.IsMounted()) && ((currentMovementAnimation != val) || 
		 (!IsAnimationPlayed(_player.GetComponent<Animation>(), currentAnimationName))))
		{			
			//for stealth and hex spells
			if(_player.stealth || _player.shapeshift)
			{
				_player.TransformParent.gameObject.BroadcastMessage("PlayMirroredMovementAnimation", val, SendMessageOptions.DontRequireReceiver);	
			}
			
			AnimationState currPlayerAnimation;
			if(string.IsNullOrEmpty(currentAnimationName))
			{
				_player.GetComponent<Animation>().Play(val);
				currPlayerAnimation = _player.GetComponent<Animation>()[GetCurentAnimationName(_player.GetComponent<Animation>())];
			}
			else
			{
				fadeTime = (currentMovementAnimation != val) ? 0.3f : 0.0f;
				currPlayerAnimation = _player.GetComponent<Animation>().CrossFadeQueued(val, fadeTime, QueueMode.PlayNow);
			}
			if(currPlayerAnimation)
			{
				currPlayerAnimation.time = startTime;
				currPlayerAnimation.speed = movingBackwards ? -0.5f : 1;
				currentMovementAnimation = val;
			}
		}
		//if the player is mounted then play mount and player animations
		//if(player.charMount)
		else if(_player.mountManager.IsMounted())
		{
			//we only have 1 animation for player, for all states, so we use mount_idle and ignore the rest...
			GameObject mount = _player.mountManager.currentMount;
			//if we have mount
			if(mount)
			{
				AnimationState currMountAnimation = null;
				float mountAnimationSpeed = 1.0f;
				string mountCurrentAnimationName = GetCurentAnimationName(mount.GetComponent<Animation>());
				if(mountCurrentAnimationName == mountCurrentAnimationName)
				{
					if(initialVal == "run")
					{
						//if its a flying mount
						if(((_player.mountManager.GetMount(mount.name)).movementType == 1) && (mount.GetComponent<Animation>()["fly"]))
						{
							fadeTime = (!mountCurrentAnimationName.Contains(initialVal)) ? 0.2f : 0.0f;
							initialVal = "fly";
							_player.transform.localPosition = new Vector3(_player.transform.localPosition.x,
							                                              2.0f,
							                                              _player.transform.localPosition.z);
						}
						else
						{
							fadeTime = (!mountCurrentAnimationName.Contains(initialVal)) ? 0.3f : 0.0f;
							initialVal = "run";
							mountAnimationSpeed = movingBackwards ? -0.5f : 1.0f;
							//player.animation.Play("mount_run");
						}
					}				
					else if(initialVal == "idle")
					{
						fadeTime = (!mountCurrentAnimationName.Contains(initialVal)) ? 0.3f : 0.0f;
						initialVal = "idle";
						//if we've flown, we put the player back
						_player.transform.localPosition = new Vector3(_player.transform.localPosition.x,
						                                              0.0f,
						                                              _player.transform.localPosition.z);				
					}
					if(string.IsNullOrEmpty(mountCurrentAnimationName))
					{
						mount.GetComponent<Animation>().Play(initialVal);
						currMountAnimation = mount.GetComponent<Animation>()[GetCurentAnimationName(_player.GetComponent<Animation>())];
					}
					else if((!mountCurrentAnimationName.Contains(initialVal)) || (!IsAnimationPlayed(mount.GetComponent<Animation>(), mountCurrentAnimationName)))
					{
						currMountAnimation = mount.GetComponent<Animation>().CrossFadeQueued(initialVal, fadeTime, QueueMode.PlayNow);
					}

					if(currMountAnimation != null)
					{
						currMountAnimation.speed = mountAnimationSpeed;
					}
				}
				if((initialVal == "run") && (_player.GetComponent<Animation>()["run"]))
				{
					_player.GetComponent<Animation>().Play("mount_run");
					currentMovementAnimation = "mount_run";
				}
				else if((initialVal == "fly") && (_player.GetComponent<Animation>()["fly"]))
				{
					_player.GetComponent<Animation>().Play("mount_fly");
					currentMovementAnimation = "mount_fly";
				}
				else
				{
					_player.GetComponent<Animation>().Play("mount_idle");
					currentMovementAnimation = "mount_idle";
				}
				
			}
		}
		else
		{
			//			var lastPlayerAnimation:String = GetCurentAnimationName(player.animation);
			//			var lastAnimationTime:float;
			//			if(lastPlayerAnimation != "")
			//				lastAnimationTime = player.animation[lastPlayerAnimation].time;
			//			Debug.LogWarning("For Palyer = "+player.name+" CurrentAnimation = "+lastPlayerAnimation+" currentMovementAnimation = "+currentMovementAnimation+" time = "+lastAnimationTime);
		}
	}
	
	public override void PlayActionAnimation(string val)
	{
		string animation_string;
		
		if(val == "magic_atk_1" || val == "magic_atk_2")
		{
			if(_player.GetComponent<Animation>()[val] == null)
			{
				return;
			}
			
			_player.GetComponent<Animation>().Blend(val, 5, 0.3f);
			currentActionAnimation = val;			
			return;
		}
		if(val.Equals ("Fishing_throw_line"))
		{
			if(_player.GetComponent<Animation>()[val] == null)
			{
				return;
			}
			
			_player.GetComponent<Animation>().Blend(val, 5, 0.3f);
			currentActionAnimation = val;			
			return;
		}

		
		string initialVal = val;
		val = GetWeaponAnim(val + "atk_1");
		val = val.Substring(0, val.LastIndexOf("_") + 1);
		
		if(!string.IsNullOrEmpty(val))
		{
			byte index = 1;
			while(true)
			{
				if(_player.GetComponent<Animation>()[val + index])
				{
					index++;
				}
				else
				{
					break;
				}
			}
			int anim_nr = Random.Range(1, index);
			animation_string = val + anim_nr;
		}
		else
		{
			animation_string = "hit1";
		}

		if(_player.GetComponent<Animation>()[animation_string] == null)
		{
			return;
		}
		
		if(currentMovementAnimation.IndexOf("idle") == -1)
		{
			if(_player.GetComponent<Animation>()[animation_string + "_moving"])
			{
				_player.GetComponent<Animation>().Blend(animation_string + "_moving", 5, 0.3f);
			}
		}
		else
		{
			_player.GetComponent<Animation>().Blend(animation_string, 5, 0.3f);
			currentActionAnimation = animation_string;
		}
	}
	
	public override void ReplayMovementAnimation()
	{
		PlayMovementAnimation("idle");
		_player.GetComponent<Animation>().Rewind();
	}
	
	public uint GetAnimationNames(Animation anim)
	{
		List<string> tmpList = new List<string>();
		
		foreach(AnimationState state in anim)
		{
			tmpList.Add(state.name);
		}

		return (uint)tmpList.Count;
	}
}

