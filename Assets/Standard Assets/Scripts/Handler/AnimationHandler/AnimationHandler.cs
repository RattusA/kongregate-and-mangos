﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimationHandler {

	public static List<string> movementAnimations = null;
	public static List<string> actionAnimations = null;
	
	public string currentMovementAnimation = "";
	public string currentActionAnimation = "";
	
	public bool movingBackwards = false;
	
	public void BakeAnimations(Unit unit)
	{
		foreach(string st in movementAnimations)
		{
			if(unit.GetComponent<Animation>()[st])
			{
				unit.GetComponent<Animation>()[st].weight = 0.1f;
			}
		}
		
		if(unit.upperBody)
		{
			foreach(string str in actionAnimations)
			{
				if(unit.GetComponent<Animation>()[str])
				{
					unit.GetComponent<Animation>().AddClip(unit.GetComponent<Animation>()[str].clip, str + "_moving");
					Transform[] tr = unit.transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 L Thigh").GetComponentsInChildren<Transform>();
					Transform[] transforms = unit.transform.Find("Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 R Thigh").GetComponentsInChildren<Transform>();

					IgnoreAnimForBones(unit.GetComponent<Animation>()[str + "_moving"], unit.transform.Find("Bip01"), transforms);
				}
			}
		}
	}
	
	void IgnoreAnimForBones(AnimationState anim, Transform root1, Transform[] bones)
	{
		Transform[] obj = root1.gameObject.GetComponentsInChildren<Transform>();
		bool found;
		foreach(Transform tr in obj)
		{
			found = false;
			foreach(Transform ignore in bones)
			{
				if(tr == ignore)
				{
					found = true;
					break;
				}
			}

			if(!found)
			{
				anim.AddMixingTransform(tr, false);
			}
		}
	}
	
	public bool MovementAnimation(string val)
	{
		foreach(string st in movementAnimations)
		{
			if(val == st)
			{
				return true;
			}
		}
		return false;
	}
	
	public string GetCurentAnimationName(Animation anim)
	{
		string ret = string.Empty;
		foreach(AnimationState state in anim)
		{
			if(anim.IsPlaying(state.name))
			{
				ret = state.name;
				break;
			}
		}
		return ret;
	}
	
	public bool IsAnimationPlayed(Animation anim, string name)
	{
		bool ret = false;
		if((!string.IsNullOrEmpty(name)) && (anim[name]) &&
		   (anim[name].normalizedTime < 1.0f) && (anim[name].normalizedTime > -1.0f))
		{
			ret = true;
		}
		return ret;
	}

	public virtual void PlayMovementAnimation(string val)// ex : idle, run , walk, swim,fly, die
	{
		//Debug.Log("AnimationHandler - PlayMovementAnimation:"+ val);
	}
	
	public virtual void PlayMovementAnimationFromTime(string val, float startTime)
	{	

	}
	
	public virtual void PlayActionAnimation(string val)// ex: hit, gather,cast, shoot
	{
		Debug.Log("AnimationHandler - PlayActionAnimation:"+ val);
	}
	
	public virtual void ReplayMovementAnimation()
	{
		
	}
}