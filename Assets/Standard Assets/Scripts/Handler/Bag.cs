﻿
public class Bag : Item
{
	protected int Capacity;
	protected Item[] Slots;

	public Bag() : base()
	{
		SetType(TYPEID.TYPEID_CONTAINER);
	}

	public Bag(Item item) : base(item.entry)
	{
		name = item.name;
		guid = item.guid;
		Capacity = (int)item.containerSlots;
		bag = item.bag;
		slot = item.slot;
		displayInfoID = item.displayInfoID;
		
		Slots = new Item[Capacity];

		InitValues();
	}

	public void InitializeBag()
	{
		Capacity = (int)containerSlots;
		Slots = new Item[Capacity];
	}

	public void SetItemToSlot(int itemSlot, Item item)
	{
		if(item != null)
		{
			Slots[itemSlot] = item;
		}
	}

	public Item GetItemFromSlot(int itemSlot)
	{
		return Slots[itemSlot];
	}
	
	public void PopulateSlots(ItemManager itemMng)
	{
		for(int index = 0; index < GetCapacity(); index++)
		{
			int valueIndex = (int)UpdateFields.EContainerFields.CONTAINER_FIELD_SLOT_1 + (2 * index);
			ulong itemGuid = GetUInt64Value(valueIndex);
			if(itemGuid != 0)
			{
				Item item = itemMng.GetItemByGuid(itemGuid);
				if(item != null)
				{
					item.slotType = SlotType.SLOTTYPE_PLAYER_INVENTORY;
					item.bag = (byte)this.GetBagBySlot();
					item.slot = (byte)index;
					item.isInContainer = true;
					Slots[index] = item;
				}
				else
				{
					Slots[index] = null;
				}
			}
			else
			{
				Slots[index] = null;
			}
		}
	}

	public byte GetFirstEmptySlot()
	{
		for(byte index = 0; index < Capacity; index++)
		{
			if(Slots[index] == null)
			{
				return index;
			}
		}
		return 255;
	}
	
	public int GetCapacity()
	{
		if(Capacity == 0)
		{
			int code = (int)UpdateFields.EContainerFields.CONTAINER_FIELD_NUM_SLOTS;
			Capacity = (int)GetUInt32Value((int)code);
		}
		return Capacity;
	}
	
	public bool IsEmpty()
	{
		foreach(Item item in Slots)
		{
			if(item != null)
			{
				return false;
			}
		}
		return true;
	}
	
	public bool ContainsItem(Item item)
	{
		foreach(Item itm in Slots)
		{
			if(item.guid == itm.guid)
			{
				return true;
			}
		}
		return false;
	}

	public bool IsSlotEmpty(byte itemSlot)
	{
		return (Slots[itemSlot] == null) ? true : false;
	}

	public ushort EmptySlotsCount()
	{
		return (ushort)(Capacity - Slots.Length);
	}
}