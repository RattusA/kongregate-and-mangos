using UnityEngine;
using System.Collections.Generic;

public class InspectManager
{
	private MainPlayer mainPlayer;
	private WorldPacket pkt;
	private BaseObject target;
	private Vector2 itemDescVector;
	private MessageWnd messageWnd;
	static public List<UIToolkit> iconsUIToolkitList = new List<UIToolkit>();
	
	public bool  active;
	int selectedItem;
	List<Item> items;
	UIAbsoluteLayout itemContainer;
	GameObject cam;
	Rect CharacterRect;
	float addY;
	
	UISprite inventoryLeftBackGround;
	UISprite characterBackGround;
	UISprite statsBackGround;
	UIButton closeButton;
	Vector2 scrollViewItemVector;
	Player targetedPlayerScript;
	private ScrollZone TextZone = new ScrollZone();

	public InspectManager ( MainPlayer player )
	{
		mainPlayer = player;
		items = new List<Item>();
		active = false;
		selectedItem = 0;
		CharacterRect = new Rect(Screen.width * 0.15f, Screen.height * 0.15f, Screen.width * 0.3f, Screen.height * 0.5f);
		messageWnd = new MessageWnd();
	}
	
	public void  InspectTarget ( BaseObject theTarget )
	{
		target = theTarget;
		SendInspect();
	}
	
	public void  InspectTarget ( ulong theTarget )
	{
		target = WorldSession.GetRealChar(theTarget);
		
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_INSPECT);
		pkt.Append(ByteBuffer.Reverse(theTarget));
		RealmSocket.outQueue.Add(pkt);
	}
	
	void  Enable ()
	{
		mainPlayer.blockTargetChange = true;
		WorldSession.interfaceManager.hideInterface = true;
		mainPlayer.interf.stateManager();
		ShowSlots();
		InitCamera();
		ChangeAllLayer(target.gameObject, LayerMask.NameToLayer("inspect"));
		Rect smallStatRect = new Rect(statsBackGround.position.x + statsBackGround.width*0.1f,
		                              -statsBackGround.position.y + statsBackGround.height * 0.1f,
		                              statsBackGround.width * 0.8f,
		                              statsBackGround.height * 0.8f);
		TextZone.SetFontSizeByRatio(1.0f);
		TextZone.InitSkinInfo(mainPlayer.newskin.GetStyle("DescriptionAndStats"));
		TextZone.CreateScrollZoneWithText(smallStatRect, "", mainPlayer.mainTextColor);
		active = true;
	}
	
	void  Disable ()
	{
		active = false;
		TextZone.RemoveScrollZone();
		WorldSession.interfaceManager.hideInterface = false;
		mainPlayer.interf.stateManager();
		mainPlayer.blockTargetChange = false;
		ClearSlots();
		selectedItem = 0;
		if(target)
			ChangeAllLayer(target.gameObject, 0);
	}
	
	public void  Show ()
	{
		if(!target)
		{
			Disable();
		}
		else
		{
			Camera camera = cam.GetComponent<Camera>();
			GUI.DrawTexture(CharacterRect, camera.targetTexture, ScaleMode.StretchToFill, true);
			if (items[selectedItem] != null && items[selectedItem].stats != null)
			{
				infoMessageWindow();
			}
		}
	}
	
	void  infoMessageWindow ()
	{
		GUI.Box( new Rect(statsBackGround.position.x + statsBackGround.width*0.1f,
		                  -statsBackGround.position.y + statsBackGround.height * 0.1f,
		                  statsBackGround.width * 0.8f, statsBackGround.height * 0.8f),
		        "",
		        mainPlayer.newskin.box);
		
		TextZone.DrawTraitsInfo();
	}
	
	void  InitCamera ()
	{
		cam =  Object.Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/inspectCamera"));
		cam.transform.parent = target.transform.parent.transform;
		
		CapsuleCollider col = target.transform.parent.GetComponent<CapsuleCollider>();
		addY = col.height;
		
		cam.transform.localPosition = new Vector3(-0.02f, addY * 0.5f, 2);
		cam.transform.LookAt(target.transform.parent.transform.position + new Vector3(0, addY * 0.5f, 0));
	}
	
	public void  UpdateRotation ()
	{
		if((Input.GetMouseButton(0) && CharacterRect.Contains(Input.mousePosition)) || (Input.GetMouseButton(0)))
		{
			float spin = Input.GetAxis("Mouse X");
			if(spin > 0)
			{
				cam.transform.RotateAround(target.transform.parent.transform.position, Vector3.up, spin * 20.0f);
				cam.transform.LookAt(target.transform.parent.transform.position + new Vector3(0, addY * 0.5f, 0));
			}
			else
			{
				cam.transform.RotateAround(target.transform.parent.transform.position, Vector3.up, spin * 20.0f);
				cam.transform.LookAt(target.transform.parent.transform.position + new Vector3(0, addY * 0.5f, 0));
			}
		}
	}
	
	void  ChangeAllLayer ( GameObject obj, int theLayer )
	{
		foreach (var child in obj.GetComponentsInChildren<Transform>())
		{
			child.gameObject.layer = theLayer;
		}
	}
	
	 void  ShowSlots ()
	{
		ClearSlots();
		UIButton btn;
		byte i;
		string image;
		float itemVPos = -Screen.height * 0.166f;
		float itemHPos = Screen.width * 0.085f;
		float itemHPos2 = Screen.width * 0.46f;
		float itemHeight = Screen.height * 0.081f;

		//character sheet background
		inventoryLeftBackGround = UIPrime31.myToolkit3.addSprite("main_tab.png",  Screen.width*0.05f, Screen.height*0.16f, 0);
		inventoryLeftBackGround.setSize(Screen.width*0.5f, Screen.height*0.83f);
		
		characterBackGround = UIPrime31.myToolkit3.addSprite("character_background.png", inventoryLeftBackGround.position.x + inventoryLeftBackGround.width*0.185f , -inventoryLeftBackGround.position.y + inventoryLeftBackGround.height*0.01f , -1);
		characterBackGround.setSize(inventoryLeftBackGround.width*0.63f , inventoryLeftBackGround.height*0.65f );
		
		//stats background
		statsBackGround = UIPrime31.myToolkit3.addSprite("stats_background.png", characterBackGround.position.x , -characterBackGround.position.y + characterBackGround.height*1.005f, -1);
		statsBackGround.setSize(characterBackGround.width, inventoryLeftBackGround.height*0.325f);
		
		closeButton = UIButton.create(UIPrime31.myToolkit5, "close.png", "close.png", Screen.width*0.65f, Screen.height*0.75f, -1);
		closeButton.setSize(Screen.width*0.22f, Screen.height*0.1f);
		closeButton.onTouchDown += CloseDelegate;
		
		itemContainer.addChild(characterBackGround, inventoryLeftBackGround, statsBackGround, closeButton);
		for(i = 0; i < items.Count; i++)
		{
			Item item = items[i];
			if(item.entry > 0)
			{
				image = DefaultIcons.GetItemIcon(item.entry);
			}
			else 
			{
				image = "fundal.png";
			}
			UIToolkit btnUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
			btn = UIButton.create( btnUIToolkit, image, image, 0, 0);
			btn.info = i;
			
			#if UNITY_IPHONE
			btn.setSize(itemHeight*0.9f, itemHeight); //*1.18f
			#else
			btn.setSize(itemHeight*1.1f, itemHeight); //*1.18f
			#endif
			if(i < 10)
			{
				btn.position = new Vector3(itemHPos,
				                       itemVPos - i*itemHeight,
				                       -2);
			}
			else
			{
				btn.position = new Vector3(itemHPos2,
				                       itemVPos -(i-9)*itemHeight,
				                       -2);
			}		
			if(item.entry > 0)
			{
				btn.onTouchDown += slotDelegate;
				selectedItem = btn.info;
			}
			else
			{
				btn.onTouchDown += null;
				btn.hidden = true;
			}
			iconsUIToolkitList.Add(btn.manager);
			itemContainer.addChild(btn);
		}
	}
	
	void  ClearSlots ()
	{
		if(itemContainer != null)
		{
			itemContainer.removeAllChild(true);
		}
		else
		{
			itemContainer = new UIAbsoluteLayout();
		}
		foreach(UIToolkit tempUIToolkit in iconsUIToolkitList)
		{
			GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
		}
		iconsUIToolkitList.Clear();
	}
	
	void  slotDelegate ( UIButton sender )
	{
		selectedItem = sender.info;
		Item item = items[selectedItem];
		TextZone.SetNewText(item.stats);
	}
	
	void  CloseDelegate ( UIButton sender )
	{
		items.Clear();
		Disable();
		GameObject.Destroy(cam);
	}
	
	void  SendInspect ()
	{
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_INSPECT);
		pkt.Append(ByteBuffer.Reverse(target.guid));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  HandleInspectResults ( WorldPacket pkt )
	{	
		ulong pktGuid = pkt.GetPacketGuid();
		ReadPlayerTalensInfo(pkt);
		
		// iteeeeeeeeeeeeems
		ReadItemsEnchantmentsInfo(pkt);
		Enable();
	}
	
	public void  ReplaceItem ( Item theItem )
	{
		for(int q= 0; q < items.Count; q++)
		{
			if(items[q].entry == theItem.entry)
			{
				theItem.GetItemBasicStats();
				items[q] = null;
				items[q] = theItem;
				return;
			}
		}
	}
	
	void  ReadPlayerTalensInfo ( WorldPacket packet )
	{
		uint freeTalentPoints = packet.ReadReversed32bit();
		byte specsCount = packet.Read();
		byte activeSpec = packet.Read();
		uint talentID;
		byte talentCurrentRank;
		ushort glyphID;
		if(specsCount > 0)
		{
			for(byte specIdx = 0; specIdx < specsCount; specIdx++) // redo when there will be talens
			{
				uint talentCount = packet.Read(); // talentIdCount
				for(int i= 0; i < talentCount; i++)
				{
					talentID = packet.ReadReversed32bit();
					talentCurrentRank = packet.Read();
				}
				byte maxGlyph = packet.Read();
				for(int j= 0; j < SharedDefines.MAX_GLYPH_SLOT_INDEX; j++)
				{
					glyphID = packet.ReadReversed16bit();
				}
			}
		}
	}
	
	void  ReadItemsEnchantmentsInfo ( WorldPacket packet )
	{
		items = new List<Item>();
		targetedPlayerScript = target.GetComponent<OtherPlayer>();
		//		Dictionary. newItems<int, uint> = targetedPlayerScript.GetVisibleItemsEntrys();
		uint itemEntry;
		int slotUsedMask = (int)packet.ReadReversed32bit(); // slotUsedMask...
		Item tmpItem;
		ushort enchId;
		ushort itemRandomPropertyID;
		ulong itemGuid;
		uint itemSuffixfactor;
		int flag = 1;
		for(byte slot = 0; slot < (byte)EquipmentSlots.EQUIPMENT_SLOT_END; slot++)
		{
			flag = (1 << slot);
			tmpItem = null;
			if((slotUsedMask & flag) > 0)
			{
				itemEntry = packet.ReadReversed32bit();
				//				newItems[slot];
				if(itemEntry > 0)
				{
					tmpItem = AppCache.sItems.GetItem(itemEntry);
					tmpItem.entry = itemEntry;
					int enchMask = (int)packet.ReadReversed16bit();
					int enchFlag = 1;
					for(int enchantInx= 0; enchantInx < (int)UpdateFields.EnchantmentSlot.MAX_ENCHANTMENT_SLOT; enchantInx++)
					{
						enchFlag = (1 << enchantInx);
						if((enchMask & enchFlag) > 0)
						{
							enchId = packet.ReadReversed16bit();
						}
					}
					itemRandomPropertyID = packet.ReadReversed16bit();
					itemGuid = packet.GetPacketGuid();
					tmpItem.guid = itemGuid;
					itemSuffixfactor = packet.ReadReversed32bit();
				}
			}
			if(tmpItem == null)
			{
				tmpItem = new Item();
				tmpItem.entry = 0;
			}
			tmpItem.GetItemBasicStats();
			items.Add(tmpItem);
		}
	}
}
