﻿using UnityEngine;
using System.Collections;

public class SelfRotation : MonoBehaviour {

	Vector2 startPosition = Vector2.zero;
	Vector2 dPos;

	int angleMultiply = 20;
	int touchNumber = -1;
	float b = 180f;

	void Update()
	{
		transform.rotation.eulerAngles.Set(transform.rotation.eulerAngles.x, b, transform.rotation.eulerAngles.z);
	}
	
	void LateUpdate()
	{
		if(Input.touchCount > 0)
		{
			foreach(Touch touch in Input.touches)	
			{
				switch(touch.phase)
				{
				case TouchPhase.Began:
					if(touchNumber == -1)
					{
						startPosition = touch.position;
						touchNumber = touch.fingerId;
					}
					break;
				case TouchPhase.Ended:
					if(touchNumber == touch.fingerId)
					{
						startPosition = Vector2.zero;
						touchNumber = -1;
					}
					break;
				default:
					if(startPosition == Vector2.zero ||
					   touch.position.y <= Screen.width * 0.2f)
					{
						continue;
					}

					if(touchNumber == touch.fingerId)
					{
						dPos = startPosition - touch.position;
						b += dPos.x * angleMultiply * Mathf.PI / 180; 
						startPosition = touch.position;
					}
					break;
				}
			}
		}

		if(Application.platform != RuntimePlatform.IPhonePlayer && Application.platform != RuntimePlatform.Android)
		{
			if(Input.GetMouseButtonDown(0))
			{
				startPosition = Input.mousePosition;
			}
			if(Input.GetMouseButton(0))
			{
				dPos = startPosition - (Vector2)Input.mousePosition;
				b += dPos.x * angleMultiply * Mathf.PI/180; 
				startPosition = Input.mousePosition;
			}
		}
	}
}
