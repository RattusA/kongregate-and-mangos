using UnityEngine;
using System;
using System.Collections.Generic;

public class CastedEffects {

	public Dictionary<int, GameObject> effectsDictionary;

	public CastedEffects()
	{
		effectsDictionary = new Dictionary<int, GameObject>();
	}
}

public static class FxEffectManager
{
	static Dictionary<ulong, CastedEffects> _castedEffectsDictionary;

	static FxEffectManager()
	{
		_castedEffectsDictionary = new Dictionary<ulong, CastedEffects>();
	}

	public static void CastEffect(float speed, float delayTime, Gender gender,
	                              int effectNameId, BaseObject linkedObject, BaseObject targetObject)
	{
		CastEffect(speed, delayTime, gender, effectNameId, linkedObject, targetObject, Vector3.zero);
	}
	
	public static void CastEffect(float speed, float delayTime, Gender gender,
	                              int effectNameId, BaseObject linkedObject, BaseObject targetObject, Vector3 positionAoE)
	{
		Vector3 effectPosition;
		if(targetObject != null)
		{
			effectPosition = targetObject.TransformParent.position;
		}
		else
		{
			effectPosition = positionAoE;
		}

		AssetBundleManager.SpellEffectCallBackStruct callBackStruct = new AssetBundleManager.SpellEffectCallBackStruct();
		callBackStruct.speed = speed;
		callBackStruct.delayTime = delayTime;
		callBackStruct.gender = gender;
		callBackStruct.effectNameId = effectNameId;
		callBackStruct.linkedObject = linkedObject;
		callBackStruct.targetObject = targetObject;
		callBackStruct.goalPosition = effectPosition;
		callBackStruct.resultObject = null;
		callBackStruct.assetPath = string.Format ("prefabs/SpellEffects/{0}", effectNameId);
		if(!HasEffect(effectNameId, linkedObject.guid))
		{
			WorldSession.GetBundleLoader().LoadSpellEffectBundle(ApplySpellEffectBundle, callBackStruct);
		}
		else
		{
			CustomizeEffect( GetEffect(effectNameId, linkedObject.guid), callBackStruct );
		}
	}

	public static void ApplySpellEffectBundle(AssetBundleManager.SpellEffectCallBackStruct callBackStruct)
	{
		int effectNameId = callBackStruct.effectNameId;
		BaseObject linkedObject = callBackStruct.linkedObject;
		Vector3 goalPosition = callBackStruct.goalPosition;
		GameObject effectObject = null;
		if(!HasEffect(effectNameId, linkedObject.guid))
		{
			string bundleName = callBackStruct.assetPath;
			string assetBundleName = string.Format ("Resources/{0}_prefab.unity3d", bundleName);
			assetBundleName = assetBundleName.ToLower();
			UnityEngine.Object bundle = null;
			AssetBundleManager.instance.currentLoadedBundles.TryGetValue(assetBundleName, out bundle);
			UnityEngine.Object resourceToInstantiate = null;
			if(bundle == null)
			{
				#if UNITY_EDITOR
				Debug.LogError(string.Format ("cannot load {0}", assetBundleName));
				#endif
			}
			else
			{
				resourceToInstantiate = bundle as UnityEngine.Object;
			}
			if(resourceToInstantiate == null)
			{
				resourceToInstantiate = Resources.Load<UnityEngine.Object>(bundleName);
			}

			if(resourceToInstantiate != null)
			{
				effectObject = UnityEngine.Object.Instantiate (resourceToInstantiate, goalPosition, linkedObject.TransformParent.rotation) as GameObject;
				if(effectObject)
				{
					effectObject.name = effectNameId.ToString();
					effectObject.AddComponent<FxEffectLifeCycle>(); 
					CastedEffects linkedEffects = new CastedEffects();
					_castedEffectsDictionary.TryGetValue(linkedObject.guid, out linkedEffects);
					if(linkedEffects == null)
					{
						linkedEffects = new CastedEffects();
					}
					linkedEffects.effectsDictionary.Add(effectNameId, effectObject);
					if(!_castedEffectsDictionary.ContainsKey(linkedObject.guid))
					{
						_castedEffectsDictionary.Add(linkedObject.guid, linkedEffects);
					}
				}
			}
		}
		else
		{
			effectObject = GetEffect(effectNameId, linkedObject.guid);
		}
		CustomizeEffect(effectObject, callBackStruct);
	}

	private static void CustomizeEffect(GameObject effectObject, AssetBundleManager.SpellEffectCallBackStruct callBackStruct)
	{
		if(effectObject != null)
		{
			float speed = callBackStruct.speed;
			float delayTime = callBackStruct.delayTime;
			Gender gender = callBackStruct.gender;
			BaseObject linkedObject = callBackStruct.linkedObject;
			BaseObject targetObject = callBackStruct.targetObject;

			effectObject.BroadcastMessage("IncrementSpellCastCount");
			if(targetObject && targetObject.guid > 0)
			{
				effectObject.transform.parent = targetObject.TransformParent;
			}
			
			EffectAnimation effectScript = effectObject.GetComponent<EffectAnimation>();
			if(!effectScript)
			{
				effectScript = effectObject.GetComponentInChildren<EffectAnimation>();
			}
			
			if(effectScript)
			{
				if(effectScript.Projectile != null)
				{
					effectScript.SetStartPosition(linkedObject.TransformParent);
					effectScript.SetTarget(targetObject.TransformParent);
				}
			}
			effectObject.BroadcastMessage("SetSpeed", speed, SendMessageOptions.DontRequireReceiver);
			effectObject.BroadcastMessage("SetDelayTime", delayTime, SendMessageOptions.DontRequireReceiver);
			effectObject.BroadcastMessage("LaunchSound", gender, SendMessageOptions.DontRequireReceiver);
			effectObject.BroadcastMessage("StartPlayingAnimation");
		}
	}
	
	public static void CleanEffects()
	{
		_castedEffectsDictionary.Clear();
	}
	
	public static SpellVisual GetSpellVisual(uint spellID)
	{
		SpellVisual spellVisualComponent = null;
		if(dbs.sSpells != null)
		{
			SpellEntry spell = dbs.sSpells.GetRecord(spellID);
			if(spell.ID > 0)
			{	
				string visualEffectName = spell.SpellVisual.array[0].ToString() + "-" + spell.SpellName.Strings[0].Replace(":", "");
				GameObject spellVisualPrefab = Resources.Load<GameObject> ("prefabs/SpellEffects/SpellVisual/" + visualEffectName);
				if(spellVisualPrefab)
				{
					spellVisualComponent = spellVisualPrefab.GetComponent<SpellVisual>();
				}
			}
		}
		return spellVisualComponent;
	}
	
	public static GameObject GetEffect(int effectNameId, ulong linkedObjectGuid)
	{
		GameObject ret = null;
		CastedEffects linkedEffects;
		_castedEffectsDictionary.TryGetValue(linkedObjectGuid, out linkedEffects);
		if(linkedEffects != null && linkedEffects.effectsDictionary.ContainsKey(effectNameId))
		{
			linkedEffects.effectsDictionary.TryGetValue(effectNameId, out ret);
		}
		return ret;
	}
	
	public static bool HasEffect(int effectNameId, ulong linkedObjectGuid)
	{
		bool ret = true;
		GameObject gameObject;
		gameObject = GetEffect(effectNameId, linkedObjectGuid);
		if(!gameObject)
		{
			DestroyAndRemoveEffect(effectNameId, linkedObjectGuid);
			ret = false;
		}
		return ret;
	}
	
	public static void DestroyAndRemoveEffect(int effectNameId, ulong linkedObjectGuid)
	{
		DestroyEffect(effectNameId, linkedObjectGuid);
		GameObject gameObject = GetEffect(effectNameId, linkedObjectGuid);
		if(gameObject == null)
		{
			CastedEffects linkedEffects = new CastedEffects();
			_castedEffectsDictionary.TryGetValue(linkedObjectGuid, out linkedEffects);
			if(linkedEffects != null && linkedEffects.effectsDictionary.ContainsKey(effectNameId))
			{
				linkedEffects.effectsDictionary.Remove(effectNameId);
			}
			if(linkedEffects != null && linkedEffects.effectsDictionary.Count < 1)
			{
				_castedEffectsDictionary.Remove(linkedObjectGuid);
			}
		}
	}
	
	public static void DestroyEffect(int effectNameId, ulong linkedObjectGuid)
	{
		GameObject gameObject = GetEffect(effectNameId, linkedObjectGuid);
		if(gameObject)
		{
			GameObject.Destroy(gameObject);
		}
	}
}