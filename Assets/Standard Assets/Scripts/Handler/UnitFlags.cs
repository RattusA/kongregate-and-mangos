﻿using System;

[Flags]
public enum MovementFlags : int
{
	MOVEMENTFLAG_NONE               = 0x00000000,
	MOVEMENTFLAG_FORWARD            = 0x00000001,
	MOVEMENTFLAG_BACKWARD           = 0x00000002,
	MOVEMENTFLAG_STRAFE_LEFT        = 0x00000004,
	MOVEMENTFLAG_STRAFE_RIGHT       = 0x00000008,
	MOVEMENTFLAG_TURN_LEFT          = 0x00000010,
	MOVEMENTFLAG_TURN_RIGHT         = 0x00000020,
	MOVEMENTFLAG_PITCH_UP           = 0x00000040,
	MOVEMENTFLAG_PITCH_DOWN         = 0x00000080,
	MOVEMENTFLAG_WALK_MODE          = 0x00000100,               // Walking
	MOVEMENTFLAG_ONTRANSPORT        = 0x00000200,
	MOVEMENTFLAG_LEVITATING         = 0x00000400,
	MOVEMENTFLAG_ROOT               = 0x00000800,
	MOVEMENTFLAG_FALLING            = 0x00001000,
	MOVEMENTFLAG_FALLINGFAR         = 0x00002000,
	MOVEMENTFLAG_PENDINGSTOP        = 0x00004000,
	MOVEMENTFLAG_PENDINGSTRAFESTOP  = 0x00008000,
	MOVEMENTFLAG_PENDINGFORWARD     = 0x00010000,
	MOVEMENTFLAG_PENDINGBACKWARD    = 0x00020000,
	MOVEMENTFLAG_PENDINGSTRAFELEFT  = 0x00040000,
	MOVEMENTFLAG_PENDINGSTRAFERIGHT = 0x00080000,
	MOVEMENTFLAG_PENDINGROOT        = 0x00100000,
	MOVEMENTFLAG_SWIMMING           = 0x00200000,               // appears with fly flag also
	MOVEMENTFLAG_ASCENDING          = 0x00400000,               // swim up also
	MOVEMENTFLAG_DESCENDING         = 0x00800000,               // swim down also
	MOVEMENTFLAG_CAN_FLY            = 0x01000000,               // can fly in 3.3?
	MOVEMENTFLAG_FLYING             = 0x02000000,               // Actual flying mode
	MOVEMENTFLAG_SPLINE_ELEVATION   = 0x04000000,               // used for flight paths
	MOVEMENTFLAG_SPLINE_ENABLED     = 0x08000000,               // used for flight paths
	MOVEMENTFLAG_WATERWALKING       = 0x10000000,               // prevent unit from falling through water
	MOVEMENTFLAG_SAFE_FALL          = 0x20000000,               // active rogue safe fall spell (passive)
	MOVEMENTFLAG_HOVER              = 0x40000000
};
[Flags]
public enum UnitFlags
{
	UNIT_FLAG_NONE           = 0x00000000,
	UNIT_FLAG_DISABLE_MOVE   = 0x00000004,
	UNIT_FLAG_UNKNOWN1       = 0x00000008,                  // essential for all units..
	UNIT_FLAG_RENAME         = 0x00000010,                  // rename creature, not working in 2.0.8
	UNIT_FLAG_RESTING        = 0x00000020,
	UNIT_FLAG_UNKNOWN2       = 0x00000100,                  // 2.0.8
	UNIT_FLAG_UNKNOWN3       = 0x00000800,                  // in combat ?2.0.8
	UNIT_FLAG_PVP            = 0x00001000,
	UNIT_FLAG_MOUNT   		 = 0x08000000,
	UNIT_FLAG_UNKNOWN4       = 0x00004000,                  // 2.0.8
	UNIT_FLAG_PACIFIED       = 0x00020000,
	UNIT_FLAG_DISABLE_ROTATE = 0x00040000,                  // may be it's stunned flag?
	UNIT_FLAG_IN_COMBAT      = 0x00080000,
	UNIT_FLAG_DISARMED       = 0x00200000,                  // disable melee spells casting..., "Required melee weapon" added to melee spells tooltip.
	UNIT_FLAG_CONFUSED       = 0x00400000,
	UNIT_FLAG_FLEEING        = 0x00800000,
	UNIT_FLAG_UNKNOWN5       = 0x01000000,					// used in spell Eyes of the Beast for pet...
	UNIT_FLAG_NOT_SELECTABLE = 0x02000000,
	UNIT_FLAG_SKINNABLE      = 0x04000000,
	UNIT_FLAG_UNKNOWN6       = 0x20000000,                  // used in Feing Death spell
	UNIT_FLAG_SHEATHE        = 0x40000000
};
[Flags]
public enum NPCFlags
{
	UNIT_NPC_FLAG_NONE                  = 0x00000000,
	UNIT_NPC_FLAG_GOSSIP                = 0x00000001,       // 100%
	UNIT_NPC_FLAG_QUESTGIVER            = 0x00000002,       // guessed, probably ok
	UNIT_NPC_FLAG_UNK1                  = 0x00000004,		//we chose this flag for portal master
	UNIT_NPC_FLAG_UNK2                  = 0x00000008,
	UNIT_NPC_FLAG_TRAINER               = 0x00000010,       // 100%
	UNIT_NPC_FLAG_TRAINER_CLASS         = 0x00000020,       // 100%
	UNIT_NPC_FLAG_TRAINER_PROFESSION    = 0x00000040,       // 100%
	UNIT_NPC_FLAG_VENDOR                = 0x00000080,       // 100%
	UNIT_NPC_FLAG_VENDOR_AMMO           = 0x00000100,       // 100%, general goods vendor
	UNIT_NPC_FLAG_VENDOR_FOOD           = 0x00000200,       // 100%
	UNIT_NPC_FLAG_VENDOR_POISON         = 0x00000400,       // guessed
	UNIT_NPC_FLAG_VENDOR_REAGENT        = 0x00000800,       // 100%
	UNIT_NPC_FLAG_REPAIR                = 0x00001000,       // 100%
	UNIT_NPC_FLAG_FLIGHTMASTER          = 0x00002000,       // 100%
	UNIT_NPC_FLAG_SPIRITHEALER          = 0x00004000,       // guessed
	UNIT_NPC_FLAG_SPIRITGUIDE           = 0x00008000,       // guessed
	UNIT_NPC_FLAG_INNKEEPER             = 0x00010000,       // 100%
	UNIT_NPC_FLAG_BANKER                = 0x00020000,       // 100%
	UNIT_NPC_FLAG_PETITIONER            = 0x00040000,       // 100% 0xC0000 = guild petitions, 0x40000 = arena team petitions
	UNIT_NPC_FLAG_TABARDDESIGNER        = 0x00080000,       // 100%
	UNIT_NPC_FLAG_BATTLEMASTER          = 0x00100000,       // 100%
	UNIT_NPC_FLAG_AUCTIONEER            = 0x00200000,       // 100%
	UNIT_NPC_FLAG_STABLEMASTER          = 0x00400000,       // 100%
	UNIT_NPC_FLAG_GUILD_BANKER          = 0x00800000,       // cause client to send 997 opcode
	UNIT_NPC_FLAG_UNK3                  = 0x01000000,       // cause client to send 1015 opcode
	UNIT_NPC_FLAG_GUARD                 = 0x10000000,       // custom flag for guards
};

public enum WeaponAttackType
{
	BASE_ATTACK   = 0,
	OFF_ATTACK    = 1,
	RANGED_ATTACK = 2
}

public enum CombatRating
{
	CR_WEAPON_SKILL             = 0,
	CR_DEFENSE_SKILL            = 1,
	CR_DODGE                    = 2,
	CR_PARRY                    = 3,
	CR_BLOCK                    = 4,
	CR_HIT_MELEE                = 5,
	CR_HIT_RANGED               = 6,
	CR_HIT_SPELL                = 7,
	CR_CRIT_MELEE               = 8,
	CR_CRIT_RANGED              = 9,
	CR_CRIT_SPELL               = 10,
	CR_HIT_TAKEN_MELEE          = 11,
	CR_HIT_TAKEN_RANGED         = 12,
	CR_HIT_TAKEN_SPELL          = 13,
	CR_CRIT_TAKEN_MELEE         = 14,
	CR_CRIT_TAKEN_RANGED        = 15,
	CR_CRIT_TAKEN_SPELL         = 16,
	CR_HASTE_MELEE              = 17,
	CR_HASTE_RANGED             = 18,
	CR_HASTE_SPELL              = 19,
	CR_WEAPON_SKILL_MAINHAND    = 20,
	CR_WEAPON_SKILL_OFFHAND     = 21,
	CR_WEAPON_SKILL_RANGED      = 22,
	CR_EXPERTISE                = 23,
	CR_ARMOR_PENETRATION        = 24
}