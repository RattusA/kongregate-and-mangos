using UnityEngine;
using System.Collections;

public enum ItemSource
{
	INVENTORY_SLOT = 0,
	EQUIP_SLOT = 1,
	BAG_SLOT = 2
}

public class InventoryInfoWindow
{
	UISprite inventoryBackground;
	UISprite inventoryDecoration;
	UIButton closeButton;
	UIButton closeXButton;
	UIButton selectedItem;
	UIButton equipButton;
	UIButton gemEquipButton;
	UIAbsoluteLayout itemContainer;
	UITextInstance[] itemTexts;
	UITextInstance equipButtonText;
	UITextInstance gemEquipButtonText;
	MainPlayer mainPlayer;
	int itemSelected;
	private InventoryManagerOLD manager;
	public Item itemObject = null;
	public bool  isEnable;
	int inventoryStatsModeOld;
	Rect itemRect;
	UISprite itemIcon;
	UIToolkit iconUIToolkit;
	string stats = "";

	/// <summary>
	/// For wait 1 launch.
	/// </summary>
	int launchCount = 0;

	private ScrollZone TextZone = new ScrollZone();

	public InventoryInfoWindow ( InventoryManagerOLD inventoryManager )
	{
		mainPlayer = WorldSession.player;
		manager = inventoryManager;
		itemContainer = new UIAbsoluteLayout();	
		itemTexts = new UITextInstance[3];
	}
	
	public void  itemDelegate ( UIButton sender, ItemSource source )
	{
		inventoryStatsModeOld = manager.inventoryStatsMode;
		manager.inventoryStatsMode = 2;
		manager.switchInventoryInfo();
		itemSelected = sender.info;
		selectedItem = sender;
		itemRect = new Rect(0, 0, selectedItem.width, selectedItem.height);
		ShowItemInfoWindow(source);
		isEnable = true;
		
		mainPlayer.itemImageDrag = null;
		SinglePictureUtils.DestroySingleUISprite(mainPlayer.itemButtonDrag as UISprite);
	}
	
	 void  ShowItemInfoWindow ( ItemSource source )
	{
		itemObject = mainPlayer.itemSelected;
		
		CreateItemWindow();
		
		uint flagContainer = (uint)ItemPrototypeFlags.ITEM_FLAG_LOOTABLE;
		uint flagUsable = (uint)ItemPrototypeFlags.ITEM_FLAG_USABLE;
		
		if(itemObject.socket[0].color != 0)
		{
			gemEquipButton = UIButton.create(UIPrime31.myToolkit2,
			                                 "btn_1_normal (2).png",
			                                 "btn_1_normal (2).png",
			                                 inventoryBackground.position.x + inventoryBackground.width*0.03f,
			                                 -inventoryBackground.position.y + inventoryBackground.height*0.57f,
			                                 -5);
			gemEquipButton.setSize(inventoryBackground.width*0.22f , inventoryBackground.height*0.09f);
			gemEquipButton.onTouchUpInside += EquipGemButtonDelegate;
			itemContainer.addChild(gemEquipButton);
			if(gemEquipButtonText != null)
			{
				gemEquipButtonText.clear();
			}
			string gemEquipButtonName = "Gems";
			gemEquipButtonText = mainPlayer.interf.text1.addTextInstance(gemEquipButtonName,
			                                                             (inventoryBackground.position.x + inventoryBackground.width*0.03f) + gemEquipButton.width/2,
			                                                             (-inventoryBackground.position.y + inventoryBackground.height*0.57f) + gemEquipButton.height/2,
			                                                             UID.TEXT_SIZE*0.6f,
			                                                             -5,
			                                                             Color.white,
			                                                             UITextAlignMode.Center,
			                                                             UITextVerticalAlignMode.Middle);
		}
		
		if((itemObject.classType == ItemClass.ITEM_CLASS_CONTAINER) || (itemObject.inventoryType != InventoryType.INVTYPE_NON_EQUIP))
		{
			equipButton = UIButton.create(UIPrime31.myToolkit2,
			                              "btn_1_normal (2).png",
			                              "btn_1_normal (2).png",
			                              inventoryBackground.position.x + inventoryBackground.width*0.03f,
			                              -inventoryBackground.position.y + inventoryBackground.height*0.71f,
			                              -4);
			equipButton.setSize(inventoryBackground.width*0.22f , inventoryBackground.height*0.09f);
			equipButton.onTouchUpInside += EquipButtonDelegate;
			itemContainer.addChild(equipButton);
			if(equipButtonText != null)
			{
				equipButtonText.clear();
			}
			string equipButtonName;
			if(itemObject.IsEquiped())
			{
				equipButtonName = "UnEquip";
			}
			else
			{
				equipButtonName = "Equip";
			}
			equipButtonText = mainPlayer.interf.text1.addTextInstance(equipButtonName,
			                                                          (inventoryBackground.position.x + inventoryBackground.width*0.03f) + equipButton.width/2,
			                                                          (-inventoryBackground.position.y + inventoryBackground.height*0.71f) + equipButton.height/2,
			                                                          UID.TEXT_SIZE*0.6f,
			                                                          -5,
			                                                          Color.white,
			                                                          UITextAlignMode.Center,
			                                                          UITextVerticalAlignMode.Middle);
		}
		else if((itemObject.flags & flagContainer) > 0)
		{
			equipButton = UIButton.create(UIPrime31.myToolkit2,
			                              "btn_1_normal (2).png",
			                              "btn_1_normal (2).png",
			                              inventoryBackground.position.x + inventoryBackground.width*0.03f,
			                              -inventoryBackground.position.y + inventoryBackground.height*0.71f,
			                              -4);
			equipButton.setSize(inventoryBackground.width*0.22f , inventoryBackground.height*0.09f);
			equipButton.onTouchUpInside += OpenContainerButtonDelegate;
			itemContainer.addChild(equipButton);
			if(equipButtonText != null)
			{
				equipButtonText.clear();
			}
			equipButtonText = mainPlayer.interf.text1.addTextInstance("Open",
			                                                          (inventoryBackground.position.x + inventoryBackground.width*0.03f) + equipButton.width/2,
			                                                          (-inventoryBackground.position.y + inventoryBackground.height*0.71f) + equipButton.height/2,
			                                                          UID.TEXT_SIZE*0.6f,
			                                                          -5,
			                                                          Color.white,
			                                                          UITextAlignMode.Center,
			                                                          UITextVerticalAlignMode.Middle);
		}
		else if((itemObject.flags & flagUsable) > 0)
		{
			equipButton = UIButton.create(UIPrime31.myToolkit2,
			                              "btn_1_normal (2).png",
			                              "btn_1_normal (2).png",
			                              inventoryBackground.position.x + inventoryBackground.width*0.03f,
			                              -inventoryBackground.position.y + inventoryBackground.height*0.71f,
			                              -4);
			equipButton.setSize(inventoryBackground.width*0.22f , inventoryBackground.height*0.09f);
			equipButton.onTouchUpInside += UseItem;
			itemContainer.addChild(equipButton);
			if(equipButtonText != null)
			{
				equipButtonText.clear();
			}
			equipButtonText = mainPlayer.interf.text1.addTextInstance("Use",
			                                                          (inventoryBackground.position.x + inventoryBackground.width*0.03f) + equipButton.width/2,
			                                                          (-inventoryBackground.position.y + inventoryBackground.height*0.71f) + equipButton.height/2,
			                                                          UID.TEXT_SIZE*0.6f,
			                                                          -5,
			                                                          Color.white,
			                                                          UITextAlignMode.Center,
			                                                          UITextVerticalAlignMode.Middle);
		}
		
		string tempText;
		string tempText2;
		int tempNum;
		
		string tempStr = ""; //disable colored tags
		
		tempStr = itemObject.GetItemBasicStatsNoTag(); //disable colored tags
		
		tempText2 = tempStr.Substring(tempStr.IndexOf("\n" , 0) + 1 , tempStr.Length - tempStr.IndexOf("\n" , 0) - 1);
		
		tempText = "";
		while(tempText2.Length > 0)
		{
			if(tempText2.Length > 25 && (tempText2.IndexOf("\n" , 0) > 26 || tempText2.IndexOf("\n" , 0) == -1))
			{
				tempNum = tempText2.IndexOf(" " , 20);
				if(tempNum != -1)
				{	
					tempText += tempText2.Substring(0 , tempNum) + "\n";
					tempText2 = tempText2.Substring(tempNum+1 , tempText2.Length-tempNum-1);
				}
				else
				{
					tempText = tempText + tempText2;
					tempText2 = "";
				}
			}
			else
			{
				tempText = tempText + tempText2;
				tempText2 = "";
			}
		}
		
		tempStr = tempStr.Substring(0, (tempStr.IndexOf("\n", 0) >= 0)? tempStr.IndexOf("\n", 0) : 0);
		itemTexts[0] = mainPlayer.interf.text3.addTextInstance(tempStr,
		                                                       inventoryBackground.position.x + inventoryBackground.width*0.12f,
		                                                       -inventoryBackground.position.y + inventoryBackground.height*0.04f,
		                                                       UID.TEXT_SIZE*0.8f,
		                                                       -6,
		                                                       Color.white,
		                                                       UITextAlignMode.Left,
		                                                       UITextVerticalAlignMode.Top);
		
		if(itemTexts[0].text == "")
		{
			itemTexts[0].clear();
			itemTexts[0].text = "unknown";
		}
		
		//		itemTexts[1] = mainPlayer.interf.text1.addTextInstance(tempText,
		//					inventoryDecoration.position.x + inventoryDecoration.width * 0.06f,
		//					-inventoryDecoration.position.y + inventoryDecoration.height*0.05f,
		//					UID.TEXT_SIZE*0.7f,
		//					-6,
		//					Color.white,
		//					UITextAlignMode.Left,
		//					UITextVerticalAlignMode.Top);
		launchCount = 0;
		
		uint fontSize = (uint)(mainPlayer.statsFontSize*1.3f);
		if(dbs._sizeMod != 2)
		{
			fontSize = (uint)(fontSize * (Screen.width / 1024.0f));
		}
		stats = itemObject.GetItemBasicStatsOutName();
		stats = "<size=" + fontSize + ">" + stats + "</size>"; //"><color=" + WorldSession.player.mainTextColor + "</color>"
	}
	
	void  CreateItemWindow ()
	{
		FreezeCurrentWindow(true);
		DestroyItemWindow();
		
		inventoryBackground = UIPrime31.myToolkit4.addSprite("popup_background.png", Screen.width*0.1f, Screen.height*0.25f, -3);
		inventoryBackground.setSize(Screen.width*0.7f, Screen.height*0.7f);
		itemContainer.addChild(inventoryBackground);
		
		itemRect = new Rect(inventoryBackground.position.x + inventoryBackground.width*0.03f,
		                    inventoryBackground.position.y - inventoryBackground.height*0.17f,
		                    itemRect.width,
		                    itemRect.height);
		CreateIconForItem(mainPlayer.itemSelected);		
		
		inventoryDecoration = UIPrime31.questMix.addSprite("decoration_case.png", itemRect.x - itemRect.width*0.03f, -itemRect.y - itemRect.height*0.03f, -5);
		inventoryDecoration.setSize(itemRect.width*1.2f, itemRect.height*1.2f);
		itemContainer.addChild(inventoryDecoration);
		
		inventoryDecoration = UIPrime31.interfMix.addSprite("vendor_decoration.png", inventoryBackground.position.x + inventoryBackground.width*0.025f, -inventoryBackground.position.y + inventoryBackground.height*0.05f, -4);
		inventoryDecoration.setSize(inventoryBackground.width*0.95f, inventoryBackground.height*0.1f);
		itemContainer.addChild(inventoryDecoration);
		
		inventoryDecoration = UIPrime31.interfMix.addSprite("vendor_decoration1.png", inventoryBackground.position.x + inventoryBackground.width*0.015f, -inventoryBackground.position.y + inventoryBackground.height*0.25f, -4);
		inventoryDecoration.setSize(inventoryBackground.width*0.25f, inventoryBackground.height*0.45f);
		itemContainer.addChild(inventoryDecoration);
		
		inventoryDecoration = UIPrime31.questMix.addSprite("background_case.png", inventoryBackground.position.x + inventoryBackground.width*0.27f, -inventoryBackground.position.y + inventoryBackground.height*0.17f, -4);
		inventoryDecoration.setSize(inventoryBackground.width*0.7f, inventoryBackground.height*0.8f);
		itemContainer.addChild(inventoryDecoration);
		
		closeButton = UIButton.create(UIPrime31.questMix , "close_button.png", "close_button.png", inventoryBackground.position.x + inventoryBackground.width*0.03f, -inventoryBackground.position.y + inventoryBackground.height*0.85f, -4);
		closeButton.setSize(inventoryBackground.width*0.22f, inventoryBackground.height*0.093f);
		closeButton.onTouchUpInside += CloseItemDelegate;
		itemContainer.addChild(closeButton);
		
		//		closeXButton = UIButton.create( UIPrime31.myToolkit4, "close_button.png", "close_button.png", 0 , 0 , 0);
		//		closeXButton.setSize(inventoryBackground.width*0.08f, inventoryBackground.width*0.08f);
		//		closeXButton.position = Vector3( inventoryBackground.position.x + inventoryBackground.width - closeXButton.width/1.5f, inventoryBackground.position.y + closeXButton.height/5, -6);
		//		closeXButton.onTouchUpInside += CloseItemDelegate;
		//		itemContainer.addChild(closeXButton);
	}
	
	void  FreezeCurrentWindow ( bool val )
	{
		foreach(var child in manager.inventoryContainer._children)
		{
			if(child is UIButton)
			{	
				(child as UIButton).disabled = val;
			}
		}
		foreach(var child in manager.inventoryBagContainer._children)
		{
			if(child is UIButton)
			{	
				(child as UIButton).disabled = val;
			}
		}
	}
	
	void  DestroyItemWindow ()
	{
		itemContainer.removeAllChild(true);
		if(iconUIToolkit)
		{
			GameObject.DestroyImmediate(iconUIToolkit.gameObject, true);
		}
		for(byte i = 0; i < 3; i++)
		{
			if (itemTexts[i] != null)
			{
				itemTexts[i].clear();
				itemTexts[i] = null;
			}
		}
		if(equipButtonText != null)
		{
			equipButtonText.clear();
			equipButtonText = null;
		}
		
		if(gemEquipButtonText != null)
		{
			gemEquipButtonText.clear();
			gemEquipButtonText = null;
		}
	}
	
	public void  CloseItemDelegate ( UIButton sender  )
	{
		isEnable = false;
		DestroyItemWindow();
		FreezeCurrentWindow(false);
		manager.inventoryStatsMode = inventoryStatsModeOld;
		manager.switchInventoryInfo();
	}
	
	void  CreateInfoScrollZone ( string statText  )
	{
		float rectWidth = inventoryBackground.width * 0.7f - inventoryDecoration.width * 0.06f * 2;
		float rectHeigth = inventoryBackground.height * 0.8f - inventoryDecoration.height * 0.05f * 2;
		Rect statsRect = new Rect(inventoryDecoration.position.x + inventoryDecoration.width * 0.06f,
		                          -inventoryDecoration.position.y + inventoryDecoration.height*0.05f,
		                          rectWidth,
		                          rectHeigth);
		TextZone.SetFontSizeByRatio(1.0f);
		TextZone.InitSkinInfo(mainPlayer.newskin.GetStyle("DescriptionAndStats"));
		TextZone.CreateScrollZoneWithText(statsRect, statText, mainPlayer.mainTextColor);
	}
	
	public void  DrawItemInfo ()
	{
		if(launchCount < 1)
		{
			launchCount++;
			return;
		}
		CreateInfoScrollZone(stats);
		TextZone.DrawTraitsInfo();
	}
	
	void  EquipButtonDelegate ( UIButton sender )
	{
		if(itemObject.IsEquiped())
		{
			manager.UnequipButtonDelegate(sender);
		}
		else if(itemObject.inventoryType != 0)
		{
			itemObject.EquipItem();
		}
		CloseItemDelegate(sender);
	}
	
	void  EquipGemButtonDelegate ( UIButton sender  )
	{
		WorldSession.interfaceManager.playerMenuMng.InventoryWnd.GemEquipWnd.mng.GetGemSlotInfo(mainPlayer.itemSelected);
		WorldSession.interfaceManager.playerMenuMng.InventoryWnd.SetWindowState(PlayerInventoryWindowState.GEM_EQUIP_STATE);
		CloseItemDelegate(null);
		
	}
	
	void  OpenContainerButtonDelegate ( UIButton sender  )
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_OPEN_ITEM, 2);
		byte bag = mainPlayer.itemSelected.bag;
		byte slot = (byte)(mainPlayer.itemSelected.slot - 1);
		if(bag == 0)
		{
			bag = 255;
		}
		pkt.Append(bag);
		pkt.Append(slot);
		RealmSocket.outQueue.Add(pkt);
		CloseItemDelegate(sender);
		mainPlayer.interf.backMode(sender);
	}
	
	void  UseItem ( UIButton sender  )
	{
		itemObject.UseItem();
		CloseItemDelegate(sender);
	}
	
	void  CreateIconForItem ( Item item  )
	{
		string image = DefaultIcons.GetItemIcon(item);
		iconUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
		itemIcon = iconUIToolkit.addSprite(image, itemRect.x, -itemRect.y, -4);
		itemIcon.setSize(itemRect.width, itemRect.height);
		itemContainer.addChild(itemIcon);
	}
	
}