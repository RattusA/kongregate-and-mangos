using UnityEngine;
using System;
using System.Collections.Generic;

public class Inventory
{
	MainPlayer mainPlayer;
	public bool  isActive;
	
	int i;
	UIButton tempButton;
	UIButton bagBackground;
	UIAbsoluteLayout bagContainer;
	public UIAbsoluteLayout itemContainer;
	public int currentBag;
	
	public float itemHeight;
	public float itemWidth;
	float itemVPos;
	
	string bagicon;
	float bagVPos;
	float bagHPos;
	float bagHeight;
	float bagWidth;
	float bagContainerWidth;
	Item[] itemsInCurrentBag = new Item[16];
	UIButton[] bagSlot = new UIButton[16];
	Action<UIButton> itemDelegate;
	
	public Inventory ( MainPlayer thePlayer )
	{
		mainPlayer = thePlayer;
		isActive = false;
		currentBag = 0;
		bagicon = "";
		bagHeight = Screen.height * 0.12f;
		bagContainerWidth = Screen.width * 0.36f;
		bagWidth = bagContainerWidth*0.22f;
		bagVPos = -Screen.height * 0.17f;
		bagHPos = Screen.width * 0.47f;
		itemHeight = Screen.height * 0.111f;
		itemWidth = bagContainerWidth  * 0.213f;
		itemVPos = -Screen.height * 0.333f;
	}
	
	public void  Hide ()
	{
		bagContainer.hidden = true;
		bagBackground.hidden = true;
		itemContainer.hidden = true;
	}
	
	public void  UnHide ()
	{
		bagContainer.hidden = false;
		bagBackground.hidden = false;
		itemContainer.hidden = false;
	}
	
	public void SetDelegate(Action<UIButton> itemDlg)
	{
		itemDelegate = itemDlg;
	}
	
	public void  ShowBags ()
	{
		bagContainer = new UIAbsoluteLayout();
		
		UIToolkit bagUIToolkit;
		bagUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/box_1.png");
		tempButton = UIButton.create( bagUIToolkit, "box_1.png", "box_1.png", 0, 0);
		tempButton.setSize(bagWidth, bagHeight);
		tempButton.position = new Vector3(bagHPos, bagVPos, 0);
		tempButton.info = 0;
		tempButton.onTouchUpInside += bagDelegate;
		bagContainer.addChild(tempButton);
		
		foreach(KeyValuePair<int, Bag> bag in mainPlayer.itemManager.BaseBags)
		{
			bagicon = DefaultIcons.GetItemIcon((mainPlayer.itemManager.BaseBags[bag.Key] as Bag).entry);
			if (string.IsNullOrEmpty (bagicon))
			{
				bagicon = "box_1.png";
			}
			bagUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+bagicon);
			tempButton = UIButton.create(bagUIToolkit, bagicon, bagicon, 0, 0);
			tempButton.setSize(bagWidth, bagHeight);
			tempButton.position = new Vector3(bagHPos + bag.Key * bagWidth, bagVPos, 0);
			tempButton.info = bag.Key;
			tempButton.onTouchUpInside += bagDelegate;
			bagContainer.addChild(tempButton);
		}
		
		bagBackground = UIButton.create( UIPrime31.myToolkit3 , "inventory_slots.png" , "inventory_slots.png" ,  0, 0);
		bagBackground.setSize(bagContainerWidth, Screen.height * 0.525f);
		bagBackground.position = new Vector3(Screen.width * 0.48f, -Screen.height * 0.295f , 5);
	}
	
	public void  ShowItems ()
	{
		if(itemContainer == null)
		{
			itemContainer = new UIAbsoluteLayout();
		}
		string image;
		Item item = null;
		int vPos = 0;
		int hPos = 0;
		float posError = itemWidth*0.47f;
		int bagCapacity;
		UIToolkit tempUIToolkit;
		if(currentBag == 0)
		{
			bagCapacity = 16;
		}
		else
		{
			bagCapacity = mainPlayer.itemManager.BaseBags[currentBag].GetCapacity();
		}
		for(int i = 0; i < 16; i++)
		{
			if(i >= bagCapacity)
			{
				if(bagSlot != null && bagSlot[i] != null)
				{
					tempUIToolkit = bagSlot[i].manager;
					itemContainer.removeChild(bagSlot[i], false);
					bagSlot[i].destroy();
					bagSlot[i] = null;			
					if(tempUIToolkit)
					{
						GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
					}
				}
				itemsInCurrentBag[i] = null;
			}
			else
			{
				if(hPos >= 4)
				{
					hPos = 0;
					vPos++;
				}
				if(currentBag == 0)
				{
					mainPlayer.itemManager.BaseBagsSlots.TryGetValue(i, out item);
				}
				else
				{
					item = mainPlayer.itemManager.BaseBags[currentBag].GetItemFromSlot(i);
				}
				
				if(((item != null) && (itemsInCurrentBag[i] == null) && (item.entry > 0)) ||
				   ((item != null) && (itemsInCurrentBag[i] != null) && (itemsInCurrentBag[i].guid != item.guid)) ||
				   (bagSlot[i] == null))
				{
					if(bagSlot != null && bagSlot[i] != null)
					{
						tempUIToolkit = bagSlot[i].manager;
						itemContainer.removeChild(bagSlot[i], false);
						bagSlot[i].destroy();
						bagSlot[i] = null;			
						if(tempUIToolkit)
						{
							GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
						}
					}
					image = "fundal.png";
					if(item != null && item.entry > 0)
					{
						image = DefaultIcons.GetItemIcon(item);
						itemsInCurrentBag[i] = item;
					}
					else
					{
						itemsInCurrentBag[i] = null;
					}
					
					UIToolkit itemUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
					bagSlot[i] = UIButton.create( itemUIToolkit, image, image, 0, 0);
					bagSlot[i].info = i;
					bagSlot[i].setSize(itemWidth, itemHeight);
					bagSlot[i].position = new Vector3(bagHPos + posError + hPos * itemWidth, itemVPos - itemHeight * vPos, -2);
					if(item != null && item.entry > 0)
					{
						bagSlot[i].onTouchDown += itemDelegate;
					}
					else
					{
						bagSlot[i].onTouchDown += null;
					}
					itemContainer.addChild(bagSlot[i]);
				}
				hPos++;
			}
		}
		isActive = true;
		
		if(mainPlayer.target != null
		   && mainPlayer.isNPC
		   && (mainPlayer.target as NPC).isVendor())
		{
			//Debug.Log("REFRESH MONEY ON VENDOR INTERFACE");
			(mainPlayer.target as NPC).vendor.vendorUI.windowActive = false;
		}
	}
	
	public void  ClearBags ()
	{
		if(bagContainer != null)
		{
			UISprite temp;
			while(bagContainer._children.Count > 0)
			{
				temp = bagContainer._children[0];
				UIToolkit tempUIToolkit = temp.manager;
				bagContainer.removeChild(temp, false);
				temp.destroy();
				if(tempUIToolkit)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
		}
		if(bagBackground != null)
		{
			bagBackground.destroy();
		}
	}
	
	public void  ClearItems ()
	{
		if(itemContainer != null)
		{
			for(int i = 0; i < 16; i++)
			{
				itemsInCurrentBag[i] = null;
				if(bagSlot != null && bagSlot[i] != null)
				{
					UIToolkit tempUIToolkit = bagSlot[i].manager;
					itemContainer.removeChild(bagSlot[i], false);
					bagSlot[i].destroy();
					bagSlot[i] = null;			
					if(tempUIToolkit)
					{
						GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
					}
				}
			}
		}
		isActive = false;
	}
	
	void  bagDelegate ( UIButton sender )
	{
		currentBag = sender.info;
		ClearItems();
		ShowItems();
	}
}