﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum CorectionTypeEnum
{
	CORECTION_ITEM			=	0x01,
	CORECTION_MOUNT			=	0x02,
	CORECTION_PORTAL		=	0x03,
	CORECTION_SPELLEFFECT	=	0x04
};

public class CorectionFields {

	public CorectionTypeEnum corectionType;

	public uint info1;
	public uint info2;
	public uint info3;
	
	public Vector3 pos;
	public Vector3 rot;
	
	public CorectionFields(CorectionTypeEnum ct, uint inf1, uint inf2, uint inf3, Vector3 ps, Vector3 rt)
	{
		corectionType = ct;
	
		info1 = inf1;
		info2 = inf2;
		info3 = inf3;
		
		pos = ps;
		rot = rt;
	}
}
	
public class CorectionDBC
{
	public static List<CorectionFields> DBC = new List<CorectionFields>();
	
	public static void Init()
	{
		//items						//itemcorection, iteminvtype = 4, itemsubclass = 7, equipeSlot = 16,  pos, rot 
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 17, 1, 16, 
		                            new Vector3(-0.1f, 0, 0),
		                            new Vector3(0, 0, 180)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 17, 5, 16, 
		                            new Vector3(-0.1f, 0, 0),
		                            new Vector3(0, 180 , 180)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 17, 6, 16, 
		                            new Vector3(-0.087f, 0.018f, 0.035f),
		                            new Vector3(0, 8.061f, 3.93f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 17, 10, 16, 
		                            new Vector3(-0.0736f, 0.0264f, -0.033f),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 17, 8, 16, 
		                            new Vector3(-0.0637f, 0.027f, -0.05f),
		                            new Vector3(2.278f, 355.45f, 11.73f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 17, 13, 16, 
		                            new Vector3(-0.1f, 0, 0),
		                            new Vector3(0, 180, 180))); 
		
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 13, 16, 
		                            new Vector3(-0.16f, 0.06f, 0),
		                            new Vector3(0, 90, 90))); 
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 15, 16, 
		                            new Vector3(-0.16f, 0.06f, 0),
		                            Vector3.zero)); 
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 0, 16, 
		                            new Vector3(-0.16f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 2, 16, 
		                            new Vector3(-0.16f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 3, 16, 
		                            new Vector3(-0.16f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 4, 16, 
		                            new Vector3(-0.033f, 0.0356f, -0.051f),
		                            new Vector3(353.4f, 341.37f, 9.07f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 6, 16, 
		                            new Vector3(-0.16f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 7, 16, 
		                            new Vector3(-0.16f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 10, 16, 
		                            new Vector3(-0.16f, 0.06f, 0),
		                            Vector3.zero));	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 18, 16, 
		                            new Vector3(-0.16f, 0.06f, 0),
		                            Vector3.zero));	
		
		
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 13, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(0, 90, 90))); 
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 15, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(0, 90, 90))); 
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 0, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 2, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 3, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 4, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 6, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 7, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 10, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 13, 18, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));	
		
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 21, 13, 16,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(0, 90, 90))); 
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 21, 15, 16,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(0, 150, 0))); 
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 21, 0, 16,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 21, 2, 16,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 21, 3, 16,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 21, 4, 16,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 21, 6, 16,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 21, 7, 16,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            Vector3.zero));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 21, 10, 16,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            Vector3.zero));	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 21, 18, 16,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            Vector3.zero));	
		
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 25, 15, 18,
		                            new Vector3(-0.08f, 0, 0),
		                            Vector3.zero));	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 15, 15, 18,
		                            new Vector3(-0.08f, 0, 0),
		                            Vector3.zero));	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 28, 15, 18,
		                            new Vector3(-0.08f, 0, 0),
		                            Vector3.zero));	
		
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 22, 13, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(0, 90, 90))); 
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 22, 15, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0))); 
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 22, 0, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 22, 2, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180,0,0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 22, 3, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 22, 4, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 22, 6, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 22, 7, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 22, 10, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 22, 18, 17,
		                            new Vector3(-0.1f, 0.06f, 0),
		                            new Vector3(180, 0, 0)));	
		
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_ITEM, 14, 6, 17,
		                            new Vector3(0.014f, -0.071f, 0.035f),
		                            Vector3.zero));	
		
		//mounts
		//mountCorection, plrRace = 1 , plrGender = 1, pos, rot)
		//dragonasu aka 47
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 1, 1,
		                            new Vector3(-0.06f, -0.517f, -0.011f),
		                            new Vector3(2.2f, 269.71f, 181.3f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 1, 0,
		                            new Vector3(0.034f, -0.4f, 0.085f),
		                            new Vector3(358.9f, 261.8f, 181.82f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 2, 1,
		                            new Vector3(0.120f, -0.372f, 0.042f),
		                            new Vector3(356.65f, 261.9f, 183)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 2, 0,
		                            new Vector3(-0.4f, -0.637f, 0.117f),
		                            new Vector3(346.46f, 261.15f, 181.27f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 3, 1,
		                            new Vector3(-0.2f, -0.43f, 0.104f),
		                            new Vector3(14.16f, 261.31f, 181.61f)));			//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 3, 0,
		                            new Vector3(-0.094f, -0.4f, 0.107f),
		                            new Vector3(6.25f, 261.8f, 181.68f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 4, 1,
		                            new Vector3(0.038f, -0.32f, 0.082f),
		                            new Vector3(356.65f, 261.9f, 183.0f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 4, 0,
		                            new Vector3(-0.11f, -0.3f, 0.052f),
		                            new Vector3(6.25f, 261.72f, 181.68f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 8, 1,
		                            new Vector3(-0.074f, -0.416f, 0.094f),
		                            new Vector3(6.26f, 261.8f, 181.68f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 8, 0,
		                            new Vector3(0.032f, -0.41f, 0.122f),
		                            new Vector3(2.07f, 261.77f, 181)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 10, 1,
		                            new Vector3(-0.078f, -0.23f, 0.07f),
		                            new Vector3(6.25f, 261.8f, 181.67f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 470, 10, 0,
		                            new Vector3(-0.0771f, -0.3f, 0.09834f),
		                            new Vector3(6.26f, 261.8f, 181.7f)));
		//rihno
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 1, 1,
		                            new Vector3(0.5f, 0.105f, 0.034f),
		                            new Vector3(354.7f, 266.6f, 181.4f)));				//human
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 1, 0,
		                            new Vector3(0.4f, 0.295f, 0.07f),
		                            new Vector3(0.9f, 270.31f, 173.51f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 2, 1,
		                            new Vector3(0.406f, 0.343f, 0.055f),
		                            new Vector3(358.43f, 269.1f, 174.74f)));			//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 2, 0,
		                            new Vector3(0.7f, 0.2371f, 0.11f),
		                            new Vector3(345.76f, 265.4f, 178.85f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 3, 1,
		                            new Vector3(0.1151782f, 0.2184825f, 0.0795245f),
		                            new Vector3(15.2f, 266.23f, 176.1f)));				//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 3, 0,
		                            new Vector3(0.483f, -0.091f, 0.022f),
		                            new Vector3(346.75f, 269.63f, 177.35f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 4, 1,
		                            new Vector3(0.2268f, 0.55f, 0.122f),
		                            new Vector3(11.26f, 261.23f, 178.07f)));			//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 4, 0,
		                            new Vector3(0.478f, 0.221f, -0.031f),
		                            new Vector3(352.54f, 272.23f, 178.73f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 8, 1,
		                            new Vector3(0.53f, 0.072f, 0.021f),
		                            new Vector3(350.1f, 267.44f, 183.422f)));			//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 8, 0,
		                            new Vector3(0.43f, 0.27f, 0.1f),
		                            new Vector3(1.43f, 264.37f, 176.84f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 10, 1,
		                            new Vector3(0.341f, 0.4934f, -0.107f),
		                            new Vector3(6.7f, 274.3f, 181.9f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 471, 10, 0,
		                            new Vector3(0.46f, 0.3f, 0.148f),
		                            new Vector3(355.4f, 262.84f, 178.04f)));
		//cockatrice
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 1, 1, 
		                            new Vector3(0.55f, -0.4f, 0.020f),
		                            new Vector3(306, 265.66f, 185.8f)));				//human
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 1, 0,
		                            new Vector3(0.62f, -0.19f, 0.072f),
		                            new Vector3(314.02f, 262.21f, 186.68f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 2, 1,
		                            new Vector3(0.534f, -0.046f, 0.050f),
		                            new Vector3(314.67f, 262.9f, 186.8f)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 2, 0,
		                            new Vector3(0.62f, -0.354f, 0.063f),
		                            new Vector3(300.1f, 266, 180.8f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 3, 1,
		                            new Vector3(1.361f, -0.0261f, 0.042f),
		                            new Vector3(346.75f, 269.63f, 178.035f)));			//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 3, 0,
		                            new Vector3(0.53f, 0.241f, 0.112f),
		                            new Vector3(346.75f, 269.63f, 177.35f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 4, 1,
		                            new Vector3(0.738f, -0.01f, 0.0478f),
		                            new Vector3(314.67f, 262.9f, 186.8f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 4, 0,
		                            new Vector3(0.645f, -0.088f, -0.0022f),
		                            new Vector3(314.57f, 270, 182.41f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 8, 1,
		                            new Vector3(0.57f, -0.422f, 0.1f),
		                            new Vector3(305.25f, 258.44f, 191.262f)));			//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 8, 0,
		                            new Vector3(0.6f, -0.000236f, -0.013f),
		                            new Vector3(323.18f, 267.02f, 184.53f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 10, 1,
		                            new Vector3(0.77f, -0.038f, -0.027f),
		                            new Vector3(315.0f, 268.75f, 183.6f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 458, 10, 0,
		                            new Vector3(0.64f, -0.06f, 0.06f),
		                            new Vector3(317.0f, 254.2f, 199.16f)));
		
		//Frost Dragon
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 1, 1,
		                            new Vector3(-0.06f, -0.517f, -0.011f),
		                            new Vector3(2.2f, 269.71f, 181.3f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 1, 0,
		                            new Vector3(0.034f, -0.4f, 0.085f),
		                            new Vector3(358.9f, 261.8f, 181.82f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 2, 1,
		                            new Vector3(0.120f, -0.372f, 0.042f),
		                            new Vector3(356.65f, 261.9f, 183)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 2, 0,
		                            new Vector3(-0.4f, -0.637f, 0.117f),
		                            new Vector3(346.46f, 261.15f, 181.27f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 3, 1,
		                            new Vector3(-0.2f, -0.43f, 0.104f),
		                            new Vector3(14.16f, 261.31f, 181.61f)));			//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 3, 0,
		                            new Vector3(-0.094f, -0.4f, 0.107f),
		                            new Vector3(6.25f, 261.8f, 181.68f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 4, 1,
		                            new Vector3(0.038f, -0.32f, 0.082f),
		                            new Vector3(356.65f, 261.9f, 183.0f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 4, 0,
		                            new Vector3(-0.11f, -0.3f, 0.052f),
		                            new Vector3(6.25f, 261.72f, 181.68f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 8, 1,
		                            new Vector3(-0.074f, -0.416f, 0.094f),
		                            new Vector3(6.26f, 261.8f, 181.68f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 8, 0,
		                            new Vector3(0.032f, -0.41f, 0.122f),
		                            new Vector3(2.07f, 261.77f, 181)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 10, 1,
		                            new Vector3(-0.078f, -0.23f, 0.07f),
		                            new Vector3(6.25f, 261.8f, 181.67f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64977, 10, 0,
		                            new Vector3(-0.0771f, -0.3f, 0.09834f),
		                            new Vector3(6.26f, 261.8f, 181.7f)));
		
		//Green Dragon
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 1, 1,
		                            new Vector3(-0.06f, -0.517f, -0.011f),
		                            new Vector3(2.2f, 269.71f, 181.3f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 1, 0,
		                            new Vector3(0.034f, -0.4f, 0.085f),
		                            new Vector3(358.9f, 261.8f, 181.82f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 2, 1,
		                            new Vector3(0.120f, -0.372f, 0.042f),
		                            new Vector3(356.65f, 261.9f, 183)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 2, 0,
		                            new Vector3(-0.4f, -0.637f, 0.117f),
		                            new Vector3(346.46f, 261.15f, 181.27f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 3, 1,
		                            new Vector3(-0.2f, -0.43f, 0.104f),
		                            new Vector3(14.16f, 261.31f, 181.61f)));			//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 3, 0,
		                            new Vector3(-0.094f, -0.4f, 0.107f),
		                            new Vector3(6.25f, 261.8f, 181.68f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 4, 1,
		                            new Vector3(0.038f, -0.32f, 0.082f),
		                            new Vector3(356.65f, 261.9f, 183.0f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 4, 0,
		                            new Vector3(-0.11f, -0.3f, 0.052f),
		                            new Vector3(6.25f, 261.72f, 181.68f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 8, 1,
		                            new Vector3(-0.074f, -0.416f, 0.094f),
		                            new Vector3(6.26f, 261.8f, 181.68f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 8, 0,
		                            new Vector3(0.032f, -0.41f, 0.122f),
		                            new Vector3(2.07f, 261.77f, 181)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 10, 1,
		                            new Vector3(-0.078f, -0.23f, 0.07f),
		                            new Vector3(6.25f, 261.8f, 181.67f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 64658, 10, 0,
		                            new Vector3(-0.0771f, -0.3f, 0.09834f),
		                            new Vector3(6.26f, 261.8f, 181.7f)));
		
		//Red Dragon
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 1, 1,
		                            new Vector3(-0.06f, -0.517f, -0.011f),
		                            new Vector3(2.2f, 269.71f, 181.3f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 1, 0,
		                            new Vector3(0.034f, -0.4f, 0.085f),
		                            new Vector3(358.9f, 261.8f, 181.82f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 2, 1,
		                            new Vector3(0.120f, -0.372f, 0.042f),
		                            new Vector3(356.65f, 261.9f, 183)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 2, 0,
		                            new Vector3(-0.4f, -0.637f, 0.117f),
		                            new Vector3(346.46f, 261.15f, 181.27f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 3, 1,
		                            new Vector3(-0.2f, -0.43f, 0.104f),
		                            new Vector3(14.16f, 261.31f, 181.61f)));			//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 3, 0,
		                            new Vector3(-0.094f, -0.4f, 0.107f),
		                            new Vector3(6.25f, 261.8f, 181.68f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 4, 1,
		                            new Vector3(0.038f, -0.32f, 0.082f),
		                            new Vector3(356.65f, 261.9f, 183.0f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 4, 0,
		                            new Vector3(-0.11f, -0.3f, 0.052f),
		                            new Vector3(6.25f, 261.72f, 181.68f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 8, 1,
		                            new Vector3(-0.074f, -0.416f, 0.094f),
		                            new Vector3(6.26f, 261.8f, 181.68f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 8, 0,
		                            new Vector3(0.032f, -0.41f, 0.122f),
		                            new Vector3(2.07f, 261.77f, 181)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 10, 1,
		                            new Vector3(-0.078f, -0.23f, 0.07f),
		                            new Vector3(6.25f, 261.8f, 181.67f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35020, 10, 0,
		                            new Vector3(-0.0771f, -0.3f, 0.09834f),
		                            new Vector3(6.26f, 261.8f, 181.7f)));		
		
		//Wolf Mount
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 1, 1,
		                            new Vector3(0.567f, -0.04f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 1, 0,
		                            new Vector3(0.567f, 0.128f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 2, 1,
		                            new Vector3(0.6f, 0.2f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 2, 0,
		                            new Vector3(0.3f, -0.1f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 3, 1,
		                            new Vector3(0.45f, 0, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 3, 0,
		                            new Vector3(0.6f, -0.12f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 4, 1,
		                            new Vector3(0.467f, 0.278f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 4, 0,
		                            new Vector3(0.567f, 0.128f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 8, 1,
		                            new Vector3(0.567f, -0.04f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 8, 0,
		                            new Vector3(0.567f, 0.128f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 10, 1,
		                            new Vector3(0.467f, 0.278f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35711, 10, 0,
		                            new Vector3(0.567f, 0.128f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));		
		
		//Tiger Mount
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 1, 1,
		                            new Vector3(0.567f, -0.04f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 1, 0,
		                            new Vector3(0.567f, 0.128f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 2, 1,
		                            new Vector3(0.6f, 0.2f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 2, 0,
		                            new Vector3(0.15f, -0.1f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 3, 1,
		                            new Vector3(0.45f, 0, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 3, 0,
		                            new Vector3(0.6f, -0.12f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 4, 1,
		                            new Vector3(0.467f, 0.272f, 0.0447f),
		                            new Vector3(10, -92.195f, -178.31f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 4, 0,
		                            new Vector3(0.567f, 0.128f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 8, 1,
		                            new Vector3(0.567f, -0.04f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 8, 0,
		                            new Vector3(0.567f, 0.128f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 10, 1,
		                            new Vector3(0.467f, 0.272f, 0.0447f),
		                            new Vector3(10, -92.195f, -178.31f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35018, 10, 0,
		                            new Vector3(0.567f, 0.128f, 0.0447f),
		                            new Vector3(0, -92.195f, -178.31f)));
		
		//Boar Mount
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 1, 1,
		                            new Vector3(0.97f, -0.28f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 1, 0,
		                            new Vector3(1.01f, -0.1f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 2, 1,
		                            new Vector3(1.043f, -0.028f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 2, 0,
		                            new Vector3(0.47f, -0.4f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 3, 1,
		                            new Vector3(0.893f, -0.228f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 3, 0,
		                            new Vector3(0.94f, -0.41f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 4, 1,
		                            new Vector3(0.91f, 0.044f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 4, 0,
		                            new Vector3(1.01f, -0.1f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 8, 1,
		                            new Vector3(0.97f, -0.28f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 8, 0,
		                            new Vector3(1.01f, -0.1f, 0.0756f ),
		                            new Vector3(0, -93.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 10, 1,
		                            new Vector3(0.91f, 0.044f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 42776, 10, 0,
		                            new Vector3(1.01f, -0.1f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));
		
		//Rhino Mount
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 1, 1,
		                            new Vector3(0.81f, 0.032f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 1, 0,
		                            new Vector3(0.81f, 0.2f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 2, 1,
		                            new Vector3(0.843f, 0.272f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 2, 0,
		                            new Vector3(0.393f, -0.028f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 3, 1,
		                            new Vector3(0.693f, 0.072f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 3, 0,
		                            new Vector3(0.843f, -0.048f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 4, 1,
		                            new Vector3(0.71f, 0.344f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 4, 0,
		                            new Vector3(0.81f, 0.2f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 8, 1,
		                            new Vector3(0.81f, 0.032f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 8, 0,
		                            new Vector3(0.81f, 0.2f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 10, 1,
		                            new Vector3(0.71f, 0.344f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 58983, 10, 0,
		                            new Vector3(0.81f, 0.2f, 0.0756f),
		                            new Vector3(0, -93.195f, -178.31f)));
		
		//Horse Mount
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 1, 1,
		                            new Vector3(0.3f, 0.03f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 1, 0,
		                            new Vector3(0.3f, 0.14f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 2, 1,
		                            new Vector3(0.333f, 0.212f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 2, 0,
		                            new Vector3(-0.117f, -0.088f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 3, 1,
		                            new Vector3(0.183f, 0.012f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));				//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 3, 0,
		                            new Vector3(0.333f, -0.108f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 4, 1,
		                            new Vector3(0.2f, 0.284f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 4, 0,
		                            new Vector3(0.3f, 0.14f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 8, 1,
		                            new Vector3(0.3f, 0.03f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 8, 0,
		                            new Vector3(0.3f, 0.14f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 10, 1,
		                            new Vector3(0.2f, 0.284f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 35022, 10, 0,
		                            new Vector3(0.3f, 0.14f, 0.0756f),
		                            new Vector3(0, -94.195f, -178.31f)));
		
		//Brown Horse Armoured
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 1, 1,
		                            new Vector3(0.31f, 0.032f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 1, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 2, 1,
		                            new Vector3(0.343f, 0.272f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 2, 0,
		                            new Vector3(-0.107f, -0.028f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 3, 1,
		                            new Vector3(0.193f, 0.072f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 3, 0,
		                            new Vector3(0.343f, -0.048f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 4, 1,
		                            new Vector3(0.21f, 0.344f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 4, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 8, 1,
		                            new Vector3(0.31f, 0.032f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 8, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 10, 1,
		                            new Vector3(0.21f, 0.344f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 10969, 10, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		
		
		//Black Horse Armoured
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 1, 1,
		                            new Vector3(0.31f, 0.032f, 0.0756f),
		                            new Vector3(0, -94.195f,-180.31f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 1, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 2, 1,
		                            new Vector3(0.343f, 0.272f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 2, 0,
		                            new Vector3(-0.107f, -0.028f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 3, 1,
		                            new Vector3(0.193f, 0.072f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 3, 0,
		                            new Vector3(0.343f, -0.048f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 4, 1,
		                            new Vector3(0.21f, 0.344f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 4, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 8, 1,
		                            new Vector3(0.31f, 0.032f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 8, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 10, 1,
		                            new Vector3(0.21f, 0.344f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 17463, 10, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		
		//White Horse Armoured
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 1, 1,
		                            new Vector3(0.31f, 0.032f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//human	female
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 1, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//male
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 2, 1,
		                            new Vector3(0.343f, 0.272f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//orc
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 2, 0,
		                            new Vector3(-0.107f, -0.028f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 3, 1,
		                            new Vector3(0.193f, 0.072f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//dwarf			
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 3, 0,
		                            new Vector3(0.343f, -0.048f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 4, 1,
		                            new Vector3(0.21f, 0.344f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//elf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 4, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 8, 1,
		                            new Vector3(0.31f, 0.032f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//bloodDrack	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 8, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 10, 1,
		                            new Vector3(0.21f, 0.344f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));				//nightelf	
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_MOUNT, 43899, 10, 0,
		                            new Vector3(0.31f, 0.2f, 0.0756f),
		                            new Vector3(0, -94.195f, -180.31f)));
		
		//portals
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_PORTAL, 1, 0, 0,
		                            Vector3.zero,
		                            Vector3.zero));//portalCorection, mapID = 1, pos, rot  
		
		//spellEffects
		DBC.Add(new CorectionFields(CorectionTypeEnum.CORECTION_SPELLEFFECT, 133, 0, 0, 
		                            Vector3.zero,
		                            Vector3.zero));
	}

	public static CorectionFields GetField(uint ct, uint inf1, uint inf2, uint inf3)
	{
		CorectionFields field = DBC.Find(element => 
		                                (element.info1 == inf1 && element.info2 == inf2 && element.info3 == inf3));
		return field;
	} 
	
	public static CorectionFields GetCorectionPrototype(uint ct, uint inf1, uint inf2, uint inf3)
	{
		return GetField(ct, inf1, inf2, inf3);
	}
}
