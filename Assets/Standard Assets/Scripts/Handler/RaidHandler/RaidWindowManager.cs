﻿using UnityEngine;
using System.Collections.Generic;

static public class RaidWindowManager
{
	static private byte raidState = 0;
	static private RaidMainWindow raidMainWindow = new RaidMainWindow();
	static private RaidGroupWindow raidGroupWindow = new RaidGroupWindow();
	static private RaidSettingsWindow raidSettingsWindow = new RaidSettingsWindow();
	static private RaidPlayerSettingsWindow raidPlayerSettingsWindow = new RaidPlayerSettingsWindow();
	
	static public List<InstanceCooldown> instanceCooldownList = new List<InstanceCooldown>();
	
	static private UISprite raidBackGround;
	static public PartyPlayer selectedPlayer;
	
	static public void  show ()
	{
		if(WorldSession.player.raidFlag)
		{
			closeRaidWindow();
		}
		
		Rect raidBackGroundRect = new Rect(0, Screen.height * 0.08f, Screen.width * 1.002f, Screen.height * 0.92f);
		raidBackGround = UIPrime31.myToolkit3.addSprite("basic_background.png", raidBackGroundRect, 10);
		raidBackGround.hidden = false;
		
		raidState = 0;
		selectedPlayer = null;
		WorldSession.player.raidFlag = true;
		showRaidWindow();
	}
	
	static private void  showRaidWindow ()
	{	
		switch(raidState)
		{
		case 1:
			raidGroupWindow.showWindow();
			break;
		case 2:
			raidSettingsWindow.showWindow();
			break;
		case 3:
			raidPlayerSettingsWindow.showRightWindow();
			break;
		case 4:
			raidPlayerSettingsWindow.showLeftWindow();
			break;
		case 5:
			WorldSession.player.interf.AnimViewMode(null);
			break;
		}
	}
	
	static public void  showGuiRaidWindow ()
	{	
		switch(raidState)
		{
		case 0:
			raidMainWindow.showWindow();
			break;
		case 1:
			raidGroupWindow.showGuiWindow();
			break;
		case 3:
			raidPlayerSettingsWindow.showRightGuiWindow();
			break;
		case 4:
			raidPlayerSettingsWindow.showLeftGuiWindow();
			break;
		case 5:
			WorldSession.player.battlegroundManager.DrawBattlegroundWindow();
			break;
		}
	}
	
	static public void  closeRaidWindow ()
	{	
		if(WorldSession.player.raidFlag == false)
		{
			return;
		}
		
		WorldSession.player.raidFlag = false;
		
		if(raidBackGround != null)
		{
			raidBackGround.destroy();
		}
		
		raidGroupWindow.closeRaidGroupWindow();
		raidSettingsWindow.closeRaidSettingsWindow();
		raidPlayerSettingsWindow.closePlayerSettingsWindow();
		
		//		WorldSession.interfaceManager.WindowState = InterfaceWindowState.ACTION_BAR_PANEL_STATE;
		WorldSession.player.menuSelected = 7;
		
	}
	
	static public void  setState ( byte newState  )
	{
		raidState = newState;
		showRaidWindow();
	}
	
	static public byte getState ()
	{
		return raidState;
	}
}