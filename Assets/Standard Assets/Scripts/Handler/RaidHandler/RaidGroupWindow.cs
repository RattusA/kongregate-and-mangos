﻿using UnityEngine;
using System.Collections;

public class RaidGroupWindow
{
	/**
	 * UI variables
	 */
	
	private UIAbsoluteLayout raidGroupWindowContainer;
	private UISprite decoration;
	
	private UIButton scrollUpArrow;
	private UIButton scrollDownArrow;
	
	private UIButton prepare;
	private UIButton close;
	private UIButton info;
	
	/**
	 * GUI variables
	 */
	
	private GUISkin raidSkin;
	private bool[ ] buttonFlag = new bool[ 40];
	
	private byte scrollSpeed = 50;
	private Vector2 scrollPosition = Vector2.zero;
	private Rect scrollZone;
	private Rect fullScrollZone;
	
	private Texture2D guiTexture;
	private Texture2D guiIcon;
	private Texture2D guiIcon2;
	
	private byte selected = 255;
	
	public RaidGroupWindow ()
	{		
		scrollZone = new Rect(Screen.width * 0.01f, Screen.height * 0.39f, Screen.width * 0.91f, Screen.height * 0.6f);
		fullScrollZone = new Rect(0, 0, Screen.width * 0.88f, Screen.width * 1.6f);
		
		guiTexture = Resources.Load<Texture2D>("GUI/InGame/raid_frame");
		guiIcon = Resources.Load<Texture2D>("GUI/InGame/icon");
		guiIcon2 = Resources.Load<Texture2D>("GUI/Maps/close_button");
		raidSkin = Resources.Load<GUISkin>("GUI/RaidSkin");
		raidGroupWindowContainer = new UIAbsoluteLayout();
	}
	
	public void  showWindow ()
	{
		Rect decorationRect = new Rect(Screen.width * 0.02f, Screen.height * 0.13f, Screen.width * 0.83f, Screen.width * 0.0583f);
		decoration = UIPrime31.myToolkit2.addSprite("mainWindowDecoration.png", decorationRect, 3);
		
		ButtonInfo scrollUpArrowRect = new ButtonInfo(Screen.width * 0.935f, Screen.height * 0.13f, decorationRect.height * 1.1f, decorationRect.height * 1.1f);
		scrollUpArrow = UIButton.create(UIPrime31.myToolkit2, "ArrowUp.png", "ArrowUp.png", scrollUpArrowRect.positionX, scrollUpArrowRect.positionY, 1);
		scrollUpArrow.setSize(scrollUpArrowRect.sizeX, scrollUpArrowRect.sizeY);
		scrollUpArrow.onTouchDown += scrollUpArrowDelegate;
		
		ButtonInfo scrollDownArrowRect = new ButtonInfo(Screen.width * 0.935f, Screen.height * 0.9f, decorationRect.height * 1.1f, decorationRect.height * 1.1f);
		scrollDownArrow = UIButton.create(UIPrime31.myToolkit2, "ArrowDown.png", "ArrowDown.png", scrollDownArrowRect.positionX, scrollDownArrowRect.positionY, 1);
		scrollDownArrow.setSize(scrollDownArrowRect.sizeX, scrollDownArrowRect.sizeY);
		scrollDownArrow.onTouchDown += scrollDownArrowDelegate;
		
		ButtonInfo prepareRect = new ButtonInfo(Screen.width * 0.03f, Screen.height * 0.25f, Screen.width * 0.34f, Screen.width * 0.05f);
		prepare = UIButton.create(UIPrime31.myToolkit2, "PrepareButton.png", "PrepareButton.png", prepareRect.positionX, prepareRect.positionY, 1);
		prepare.setSize(prepareRect.sizeX,prepareRect.sizeY);
		prepare.onTouchUpInside += prepareDeleagte;
		
		ButtonInfo closeRect = new ButtonInfo(Screen.width * 0.42f, Screen.height * 0.25f, Screen.width * 0.16f, Screen.width * 0.05f);
		close = UIButton.create(UIPrime31.myToolkit2, "btn_close.png", "btn_close.png", closeRect.positionX, closeRect.positionY, 1);
		close.setSize(closeRect.sizeX, closeRect.sizeY);
		close.onTouchUpInside += closeDeleagte;
		
		ButtonInfo infoRect = new ButtonInfo(Screen.width * 0.63f, Screen.height * 0.25f, Screen.width * 0.34f, Screen.width * 0.05f);
		info = UIButton.create(UIPrime31.myToolkit2, "btn_info.png", "btn_info.png", infoRect.positionX, infoRect.positionY, 1);
		info.setSize(infoRect.sizeX, infoRect.sizeY);
		info.onTouchUpInside += infoDeleagte;
		
		raidGroupWindowContainer.addChild(decoration, scrollUpArrow, scrollDownArrow, prepare, close, info);
		
		updateButtonFlags();
	}
	
	private void  prepareDeleagte (UIButton sender)
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.MSG_RAID_READY_CHECK);
		RealmSocket.outQueue.Add(pkt);
		
		GroupManager updateGroup = WorldSession.player.group;
		updateGroup.setReadyCheck(WorldSession.player.guid, 1);
	}
	
	private void  scrollUpArrowDelegate (UIButton sender)
	{
		if(scrollPosition.y > scrollSpeed)
		{
			scrollPosition.y -= scrollSpeed;
		}
		else
		{
			scrollPosition.y = 0;
		}
	}
	
	private void  scrollDownArrowDelegate (UIButton sender)
	{
		if(scrollPosition.y < (fullScrollZone.height - scrollSpeed))
		{
			scrollPosition.y += scrollSpeed;
		}
		else
		{
			scrollPosition.y = fullScrollZone.height;
		}
	}
	
	private void  closeDeleagte (UIButton sender)
	{
		closeRaidGroupWindow();
		RaidWindowManager.setState(0);
	}
	
	private void  infoDeleagte (UIButton sender)
	{
		if(RaidWindowManager.selectedPlayer == null)
		{
			closeRaidGroupWindow();
			RaidWindowManager.setState(2);
		}
		else
		{
			closeRaidGroupWindow();
			RaidWindowManager.setState(4);
		}
	}
	
	private void  updateButtonFlags ()
	{
		for(byte i = 0; i < 40; i++)
		{
			buttonFlag[i] = false;
		}
	}
	
	public void  closeRaidGroupWindow ()
	{
		if(raidGroupWindowContainer._children.Count > 0)
		{
			raidGroupWindowContainer.removeAllChild(true);
			
			decoration.destroy();
			scrollUpArrow.destroy();
			scrollDownArrow.destroy();
			prepare.destroy();
			close.destroy();
			info.destroy();
		}
	}
	
	/**
	 *	GUI part of Raid Inspect Window
	 */
	
	public void  showGuiWindow ()
	{
		scrollPosition = GUI.BeginScrollView(scrollZone, scrollPosition, fullScrollZone, false, true);
		
		drawRaidGroups();
		
		float szw = scrollZone.width;
		
		GUI.DrawTexture(new Rect(szw * 0.02f, szw * 0.02f, szw * 0.95f, szw * 0.425f), guiTexture, ScaleMode.ScaleToFit, true, 0.0f);
		GUI.DrawTexture(new Rect(szw * 0.02f, szw * 0.47f, szw * 0.95f, szw * 0.425f), guiTexture, ScaleMode.ScaleToFit, true, 0.0f);
		GUI.DrawTexture(new Rect(szw * 0.02f, szw * 0.92f, szw * 0.95f, szw * 0.425f), guiTexture, ScaleMode.ScaleToFit, true, 0.0f);
		GUI.DrawTexture(new Rect(szw * 0.02f, szw * 1.37f, szw * 0.95f, szw * 0.425f), guiTexture, ScaleMode.ScaleToFit, true, 0.0f);
		
		GUI.EndScrollView();
	}
	
	private void  drawRaidGroups ()
	{
		float szw = scrollZone.width;
		
		for(byte i = 0; i < 8; i++)
		{
			int divMod = i / 2;
			int modMod = i % 2;
			
			float positionX = szw * 0.10f + szw * 0.415f * modMod;
			float positionY = szw * 0.03f + szw * 0.450f * divMod;
			
			string labelText = "Group " + (i + 1);
			GUI.Label(new Rect(positionX, positionY, szw * 0.3f, szw * 0.125f), labelText, raidSkin.customStyles[0]);
			
			positionX = szw * 0.25f + szw * 0.415f * modMod;
			GUI.Label(new Rect(positionX, positionY, szw * 0.3f, szw * 0.125f), "Name", raidSkin.customStyles[1]);
			
			positionX = szw * 0.39f + szw * 0.415f * modMod;
			GUI.Label(new Rect(positionX, positionY, szw * 0.3f, szw * 0.125f), "Lvl.", raidSkin.customStyles[1]);
			
			drawPlayersInGroups(i);
		}
	}
	
	private void  drawPlayersInGroups ( byte groupIndex  )
	{
		float szw = scrollZone.width;
		
		int divMod = groupIndex / 2;
		int modMod = groupIndex % 2;
		
		float positionX = szw * 0.09f + szw * 0.4095f * modMod;
		float positionY = szw * 0.07f + szw * 0.4500f * divMod;	
		
		GroupManager updateGroup = WorldSession.player.group;
		if(updateGroup.groupType != GroupType.GROUPTYPE_RAID)
		{
			closeDeleagte(null);
		}
		for(byte i = 0; i < 5; i++)
		{
			byte buttonIndex = (byte)(groupIndex * 5 + i);
			string playerName = updateGroup.getPlayerName(groupIndex, i);
			playerName = " " + playerName;
//			string playerLvl = updateGroup.getPlayerLvl(groupIndex ,i);
			
			float newPositionY = positionY + szw * 0.062f * i;
			buttonFlag[buttonIndex] = GUI.Toggle(new Rect(positionX, newPositionY, szw * 0.405f, szw * 0.062f), buttonFlag[buttonIndex], playerName, raidSkin.customStyles[2]);			
			
			
			float newPositionX = positionX + szw * 0.25f;
			GUI.Label(new Rect(newPositionX, newPositionY, szw * 0.1f, szw * 0.062f), "00", raidSkin.customStyles[3]);
			
			newPositionX += szw * 0.06f;
			PartyPlayer newPlayer = updateGroup.getPlayerFromGroup(groupIndex, i);
			if(newPlayer != null)
				if(newPlayer.checkFlag == 1)
					GUI.DrawTexture(new Rect(newPositionX, newPositionY, szw * 0.04f, szw * 0.06f), guiIcon, ScaleMode.ScaleToFit, true, 0.0f);
			else if(newPlayer.checkFlag == 0)
				GUI.DrawTexture(new Rect(newPositionX, newPositionY, szw * 0.04f, szw * 0.06f), guiIcon2, ScaleMode.ScaleToFit, true, 0.0f);
		}
		
		checkButtonFlag();
	}
	
	private void  checkButtonFlag ()
	{
		GroupManager updateGroup = WorldSession.player.group;
		
		for(byte index = 0; index < 40; index++)
		{
			if(selected != 255 && buttonFlag[index] && index != selected && RaidWindowManager.selectedPlayer != null)
			{
				updateGroup.swapPosition(RaidWindowManager.selectedPlayer.name, (byte)(index / 5));
				buttonFlag[selected] = false;
				buttonFlag[index] = false;
			}
			else if(buttonFlag[index] && index != selected)
			{	
				RaidWindowManager.selectedPlayer = updateGroup.getPlayerFromGroup((byte)(index / 5), (byte)(index % 5));
				selected = index;
			}
		}
		
		if(selected != 255 && buttonFlag[selected] == false)
		{
			RaidWindowManager.selectedPlayer = null;
			selected = 255;
		}
	}
	
}
