﻿using UnityEngine;

public class RaidPlayerSettingsWindow
{
	private UISprite decorationLeft;
	private UISprite decorationRight;
	
	private UIButton leftClose;
	private UIButton rightClose;
	
	private Vector2 scrollPosition = Vector2.zero;
	private Rect scrollZone;
	
	private Vector2 scrollPosition2 = Vector2.zero;
	private Rect scrollZone2;
	
	private bool  setRaidLeader = false;
	private bool  setPrimaryHelper = false;
	private bool  setPrimaryTank = false;
	private bool  setLootMaster = false;
	
	private bool  setMarkerflag = false;
	
	private GUISkin raidSkin;
	
	private string setRaidLeaderText = "Raid Commander";
	private string setPrimaryHelperText = "Primary Helper";
	private string setPrimaryTankText = "Primary Tank";
	private string setLootMasterText = "Loot Master";

	public RaidPlayerSettingsWindow()
	{
		scrollZone = new Rect(Screen.width * 0.06f, Screen.height * 0.33f, Screen.width * 0.38f, Screen.height * 0.6f);
		scrollZone2 = new Rect(Screen.width * 0.56f, Screen.height * 0.33f, Screen.width * 0.38f, Screen.height * 0.6f);
		raidSkin = Resources.Load<GUISkin>("GUI/RaidSkin");
	}
	
	public void  showLeftWindow ()
	{
		Rect decorationLeftRect = new Rect(Screen.width * 0.05f, Screen.height * 0.20f, Screen.width * 0.4f, Screen.height * 0.75f);
		decorationLeft = UIPrime31.myToolkit6.addSprite("back_frame.png", decorationLeftRect, 4);
		
		ButtonInfo closeLeftRect = new ButtonInfo(Screen.width * 0.43f, Screen.height * 0.20f - Screen.width * 0.02f, Screen.width * 0.04f, Screen.width * 0.04f);
		leftClose = UIButton.create(UIPrime31.myToolkit6, "close.png", "close.png", closeLeftRect.positionX, closeLeftRect.positionY, 4);
		leftClose.setSize(closeLeftRect.sizeX, closeLeftRect.sizeY);
		leftClose.onTouchUpInside += closeLeftDelegate;
		
		GroupManager updateGroup = WorldSession.player.group;
		PartyPlayer player = RaidWindowManager.selectedPlayer;
		
		if(WorldSession.player.guid != player.guid)
		{
			if(WorldSession.player.guid == updateGroup.leaderGUID)
			{
				setRaidLeaderText = "Set Raid Commander";
				setLootMasterText = "Set Loot Master";
				setPrimaryTankText = "Set Primary Tank";
				setPrimaryHelperText = "Set Primary Helper";				
			}
		}
		
		if(player.guid == updateGroup.leaderGUID)
			setRaidLeader = true;
		if(player.flags == 1)
			setPrimaryHelper = true;
		if(player.guid == updateGroup.looterGUID)
			setLootMaster = true;
	}
	
	public void  closeLeftDelegate (UIButton sender)
	{
		if(RaidWindowManager.getState() == 3)
		{
			decorationRight.destroy();
			rightClose.destroy();
		}
		
		decorationLeft.destroy();
		leftClose.destroy();
		
		setRaidLeaderText = "Raid Commander";
		setPrimaryHelperText = "Primary Helper";
		setPrimaryTankText = "Primary Tank";
		setLootMasterText = "Loot Master";
		
		setRaidLeader = false;
		setPrimaryHelper = false;
		setPrimaryTank = false;
		setLootMaster = false;
		
		RaidWindowManager.setState(1);
		setMarkerflag = false;
	}
	
	public void  showLeftGuiWindow ()
	{
		GroupManager updateGroup = WorldSession.player.group;
		PartyPlayer player = RaidWindowManager.selectedPlayer;
		Rect playerNameRect = new Rect(Screen.width * 0.06f, Screen.height * 0.21f, Screen.width * 0.38f, Screen.height * 0.13f);
		GUI.Label(playerNameRect, RaidWindowManager.selectedPlayer.name, raidSkin.customStyles[4]);
		
		GUILayout.BeginArea(scrollZone);
		scrollPosition = GUILayout.BeginScrollView(scrollPosition);
		// Raid Leader
		if(WorldSession.player.guid == updateGroup.leaderGUID || player.guid == updateGroup.leaderGUID)
		{
			if(setRaidLeader)
				GUILayout.Button(setRaidLeaderText, raidSkin.customStyles[6]);
			else
				if(GUILayout.Button(setRaidLeaderText, raidSkin.customStyles[5]))
			{
				updateGroup.setNewLeader(player.guid);
				closeLeftDelegate(null);
			}
			
			GUILayout.Space(Screen.height * 0.03f);
		}
		// Loot Master
		if(setLootMaster)
			GUILayout.Button(setLootMasterText, raidSkin.customStyles[6]);
		else
			if(GUILayout.Button(setLootMasterText, raidSkin.customStyles[5]))
		{
			updateGroup.setNewLootMaster(player.guid);
			closeLeftDelegate(null);
		}
		// Main Assistant
		GUILayout.Space(Screen.height * 0.03f);
		if(setPrimaryHelper)
			GUILayout.Button(setPrimaryHelperText, raidSkin.customStyles[6]);
		else
			if(GUILayout.Button(setPrimaryHelperText, raidSkin.customStyles[5]))
		{
			updateGroup.setNewPlayerFlag(player.guid, (byte)GroupFlagMask.GROUP_ASSISTANT);
			closeLeftDelegate(null);
		}
		// Whisper			
		GUILayout.Space(Screen.height * 0.03f);
		if(GUILayout.Button("Whisper", raidSkin.customStyles[3]))
		{
			RaidWindowManager.closeRaidWindow();		
			WorldSession.player.interf.backMode(null);
		}
		// Check Gear
		GUILayout.Space(Screen.height * 0.03f);
		if(GUILayout.Button("Check Gear", raidSkin.customStyles[3]))
		{
			RaidWindowManager.closeRaidWindow();		
			WorldSession.player.interf.backMode(null);
		}
		// Kick
		GUILayout.Space(Screen.height * 0.03f);
		if(GUILayout.Button("Kick", raidSkin.customStyles[3]))
		{
			updateGroup.kickPlayer(player.name);
			closeLeftDelegate(null);
		}
		GUILayout.Space(Screen.height * 0.03f);
		setMarkerflag = GUILayout.Toggle(setMarkerflag, "Set Marker", raidSkin.customStyles[5]);
		GUILayout.EndScrollView();	
		GUILayout.EndArea();
		
		if(setMarkerflag && RaidWindowManager.getState() != 3)
		{
			decorationLeft.destroy();
			leftClose.destroy();
			
			RaidWindowManager.setState(3);
		}
		else if(!setMarkerflag && RaidWindowManager.getState() == 3)
		{
			closeRightDelegate(null);
		}
		
	}
	
	public void  showRightWindow ()
	{	 	
		Rect decorationRightRect = new Rect(Screen.width * 0.55f, Screen.height * 0.20f, Screen.width * 0.4f, Screen.height * 0.75f);
		decorationRight = UIPrime31.myToolkit6.addSprite("back_frame.png", decorationRightRect, 4);
		
		ButtonInfo closeRightRect = new ButtonInfo(Screen.width * 0.93f, Screen.height * 0.20f - Screen.width * 0.02f, Screen.width * 0.04f, Screen.width * 0.04f);
		rightClose = UIButton.create(UIPrime31.myToolkit6, "close.png", "close.png", closeRightRect.positionX, closeRightRect.positionY, 4);
		rightClose.setSize(closeRightRect.sizeX, closeRightRect.sizeY);
		rightClose.onTouchUpInside += closeRightDelegate;
	}
	
	public void  closeRightDelegate (UIButton sender)
	{
		setMarkerflag = false;
		RaidWindowManager.setState(4);
		
		decorationRight.destroy();
		rightClose.destroy();
	}
	
	private bool  triangleFlag = false;
	private bool  circleFlag = false;
	private bool  halfcircleFlag = false;
	private bool  pentagramFlag = false;
	private bool  gemFlag = false;
	
	public void  showRightGuiWindow ()
	{
		Rect playerNameRect = new Rect(Screen.width * 0.56f, Screen.height * 0.21f, Screen.width * 0.38f, Screen.height * 0.13f);
		GUI.Label(playerNameRect, "Set Marker", raidSkin.customStyles[4]);
		
		GUILayout.BeginArea(scrollZone2);
		scrollPosition2 = GUILayout.BeginScrollView(scrollPosition2);
		triangleFlag = GUILayout.Toggle(triangleFlag, "Triangle", raidSkin.customStyles[2]);
		GUILayout.Space(Screen.height * 0.03f);
		circleFlag = GUILayout.Toggle(circleFlag, "Circle", raidSkin.customStyles[2]);
		GUILayout.Space(Screen.height * 0.03f);
		halfcircleFlag = GUILayout.Toggle(halfcircleFlag, "Half Circle", raidSkin.customStyles[2]);
		GUILayout.Space(Screen.height * 0.03f);
		pentagramFlag = GUILayout.Toggle(pentagramFlag, "Pentagrem", raidSkin.customStyles[2]);
		GUILayout.Space(Screen.height * 0.03f);
		gemFlag = GUILayout.Toggle(gemFlag, "Gem", raidSkin.customStyles[2]);
		GUILayout.Space(Screen.height * 0.03f);
		GUILayout.Button("Nothing", raidSkin.customStyles[3]);
		GUILayout.EndScrollView();	
		GUILayout.EndArea();
	}
	
	public void  closePlayerSettingsWindow ()
	{
		if(decorationLeft != null)
		{
			decorationLeft.destroy();
			leftClose.destroy();
		}
		
		if(decorationRight != null)
		{
			decorationRight.destroy();
			rightClose.destroy();
		}
	}
}