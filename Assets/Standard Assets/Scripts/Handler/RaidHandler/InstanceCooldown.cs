﻿
public class InstanceCooldown
{
	uint instanceMapId;
	public string instanceName;
	public string instanceDifficult;
	public string instanceCooldown;
	
	public InstanceCooldown ( uint instanceMapId, string instanceName, string instanceDifficult, string instanceCooldown )
	{
		this.instanceMapId = instanceMapId;
		this.instanceName = instanceName;
		this.instanceDifficult = instanceDifficult;
		this.instanceCooldown = instanceCooldown;
	}
}