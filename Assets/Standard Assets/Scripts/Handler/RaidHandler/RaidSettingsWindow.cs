﻿using UnityEngine;
using System.Collections;

public class RaidSettingsWindow
{
	private UIAbsoluteLayout raidSettingsContainer;
	
	private UISprite frame;
	private UITextInstance text;
	
	private UIButton difficultButton;
	private UITextInstance difficultText;
	private UITextInstance difficultButtonText;
	private Difficulty difficultValue = Difficulty.RAID_DIFFICULTY_10MAN_NORMAL;
	
	private UIButton lootTypeButton;
	private UITextInstance lootTypeText;
	private UITextInstance lootTypeButtonText;
	private LootMethod lootTypeValue = LootMethod.FREE_FOR_ALL;
	
	private UIButton lootQualitiesButton;
	private UITextInstance lootQualitiesText;
	private UITextInstance lootQualitiesButtonText;
	private ItemQualities lootQualitiesValue = ItemQualities.ITEM_QUALITY_POOR;
	
	private UIButton closeButton;
	
	private UIText textStyle;
	
	public RaidSettingsWindow ()
	{
		textStyle = new UIText(UIPrime31.myToolkit1, "fontiny dark", "fontiny dark.png");		
		raidSettingsContainer = new UIAbsoluteLayout();
	}
	
	public void  showWindow ()
	{
		GroupManager updateGroup = WorldSession.player.group;
		lootTypeValue = updateGroup.lootMethod;
		difficultValue = updateGroup.raidDifficulty;
		lootQualitiesValue = updateGroup.lootThreshold;
		
		Rect frameRect = new Rect(Screen.width * 0.2f, Screen.height * 0.20f, Screen.width * 0.6f, Screen.height * 0.75f);
		frame = UIPrime31.myToolkit6.addSprite("back_frame.png", frameRect, 4);
		
		Vector2 textPosition = new Vector2(Screen.width * 0.5f, Screen.height * 0.25f);
		text = textStyle.addTextInstance("Raid Settings",textPosition.x, textPosition.y, 0.7f, -6 ,Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		
		Vector2 difficultTextPosition = new Vector2(Screen.width * 0.45f, Screen.height * 0.39f);
		difficultText = textStyle.addTextInstance("Raid Difficult", difficultTextPosition.x, difficultTextPosition.y, 0.5f, -6 ,Color.white, UITextAlignMode.Right, UITextVerticalAlignMode.Middle);
		
		ButtonInfo difficultButtonPosition = new ButtonInfo(Screen.width * 0.5f, Screen.height * 0.35f, Screen.width * 0.3f, Screen.height * 0.08f);
		difficultButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_selected.png", difficultButtonPosition.positionX, difficultButtonPosition.positionY, 1);
		difficultButton.setSize(difficultButtonPosition.sizeX, difficultButtonPosition.sizeY);
		difficultButton.onTouchUpInside += changeRaidDifficult;
		
		
		Vector2 lootTypeTextPosition = new Vector2(Screen.width * 0.45f, Screen.height * 0.49f);
		lootTypeText = textStyle.addTextInstance("Raid LootType", lootTypeTextPosition.x, lootTypeTextPosition.y, 0.5f, -6 ,Color.white, UITextAlignMode.Right, UITextVerticalAlignMode.Middle);
		
		ButtonInfo lootTypeButtonPosition = new ButtonInfo(Screen.width * 0.5f, Screen.height * 0.45f, Screen.width * 0.3f, Screen.height * 0.08f);
		lootTypeButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_selected.png", lootTypeButtonPosition.positionX, lootTypeButtonPosition.positionY, 1);
		lootTypeButton.setSize(lootTypeButtonPosition.sizeX, lootTypeButtonPosition.sizeY);
		lootTypeButton.onTouchUpInside += changeRaidLootType;
		
		
		Vector2 lootQualitiesTextPosition = new Vector2(Screen.width * 0.45f, Screen.height * 0.59f);
		lootQualitiesText = textStyle.addTextInstance("Raid LootType", lootQualitiesTextPosition.x, lootQualitiesTextPosition.y, 0.5f, -6 ,Color.white, UITextAlignMode.Right, UITextVerticalAlignMode.Middle);
		
		ButtonInfo lootQualitiesButtonPosition = new ButtonInfo(Screen.width * 0.5f, Screen.height * 0.55f, Screen.width * 0.3f, Screen.height * 0.08f);
		lootQualitiesButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_selected.png", lootQualitiesButtonPosition.positionX, lootQualitiesButtonPosition.positionY, 1);
		lootQualitiesButton.setSize(lootQualitiesButtonPosition.sizeX, lootQualitiesButtonPosition.sizeY);
		lootQualitiesButton.onTouchUpInside += changeLootQualityType;
		
		
		ButtonInfo closeButtonPosition = new ButtonInfo(Screen.width * 0.4f, Screen.height * 0.85f, Screen.width * 0.2f, Screen.height * 0.08f);
		closeButton = UIButton.create(UIPrime31.myToolkit2, "btn_close.png", "btn_close.png", closeButtonPosition.positionX, closeButtonPosition.positionY, 1);
		closeButton.setSize(closeButtonPosition.sizeX, closeButtonPosition.sizeY);
		closeButton.onTouchUpInside += closeRaidSettings;
		
		raidSettingsContainer.addChild(frame, difficultButton, lootTypeButton, lootQualitiesButton, closeButton);
		
		reDrawRaidSettingsText();
	}
	
	private void  closeRaidSettings (UIButton sender)
	{
		GroupManager updateGroup = WorldSession.player.group;
		
		updateGroup.lootMethod = lootTypeValue;
		updateGroup.raidDifficulty = difficultValue;
		updateGroup.lootThreshold = lootQualitiesValue;
		
		updateGroup.setLootType();
		updateGroup.setRaidDifficult();
		
		closeRaidSettingsWindow();
		RaidWindowManager.setState(1);
	}
	
	public void  closeRaidSettingsWindow ()
	{
		if(raidSettingsContainer._children.Count > 0)
		{
			raidSettingsContainer.removeAllChild(true);
			
			frame.destroy();
			difficultButton.destroy();
			lootTypeButton.destroy();
			lootQualitiesButton.destroy();
			closeButton.destroy();
			
			text.clear();		
			difficultText.clear();
			difficultButtonText.clear();		
			lootTypeText.clear();
			lootTypeButtonText.clear();
			lootQualitiesText.clear();
			lootQualitiesButtonText.clear();
		}
	}
	
	private void  changeRaidDifficult (UIButton sender)
	{
		difficultValue++;
		difficultValue = (difficultValue == Difficulty.RAID_DIFFICULTY_25MAN_HEROIC+1) ? Difficulty.RAID_DIFFICULTY_10MAN_NORMAL : difficultValue;
		reDrawRaidSettingsText();
	}
	
	private void  changeRaidLootType (UIButton sender)
	{
		lootTypeValue++;
		lootTypeValue = (lootTypeValue == LootMethod.NEED_BEFORE_GREED+1) ? LootMethod.FREE_FOR_ALL : lootTypeValue;
		reDrawRaidSettingsText();
	}
	
	private void  changeLootQualityType (UIButton sender)
	{
		lootQualitiesValue++;
		lootQualitiesValue = (lootQualitiesValue == ItemQualities.ITEM_QUALITY_LEGENDARY) ? ItemQualities.ITEM_QUALITY_POOR : lootQualitiesValue;
		reDrawRaidSettingsText();
	}
	
	private void  reDrawRaidSettingsText ()
	{	
		if(lootTypeButtonText != null)
		{
			lootTypeButtonText.hidden = true;
		}
		
		string lootTypeStr = string.Empty;
		
		switch(lootTypeValue)
		{
		case LootMethod.FREE_FOR_ALL:
			lootTypeStr = "Direct Loot";
			break;
		case LootMethod.ROUND_ROBIN:
			lootTypeStr = "Circular Loot";
			break;
		case LootMethod.MASTER_LOOT:
			lootTypeStr = "Loot Master";
			break;
		case LootMethod.GROUP_LOOT:
			lootTypeStr = "Modified Circular";
			break;
		case LootMethod.NEED_BEFORE_GREED:
			lootTypeStr = "Demand First";
			break;
		}
		
		Vector2 lootTypeButtonTextPosition = new Vector2(Screen.width * 0.65f, Screen.height * 0.49f);
		lootTypeButtonText = textStyle.addTextInstance(lootTypeStr, lootTypeButtonTextPosition.x, lootTypeButtonTextPosition.y, 0.45f, -6 ,Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		lootTypeButtonText.hidden = false;
		
		
		if(difficultButtonText != null)
		{
			difficultButtonText.hidden = true;
		}
		
		string difficultStr = group.getRaidDifficultName(difficultValue);
		
		Vector2 difficultButtonTextPosition = new Vector2(Screen.width * 0.65f, Screen.height * 0.39f);
		difficultButtonText = textStyle.addTextInstance(difficultStr, difficultButtonTextPosition.x, difficultButtonTextPosition.y, 0.45f, -6 ,Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		difficultButtonText.hidden = false;
		
		
		if(lootQualitiesButtonText != null)
		{
			lootQualitiesButtonText.hidden = true;
		}
		
		string qualitiesStr = ItemFlags.GetQualityName(lootQualitiesValue);
		
		Vector2 lootQualitiesButtonTextPosition = new Vector2(Screen.width * 0.65f, Screen.height * 0.59f);
		lootQualitiesButtonText = textStyle.addTextInstance(qualitiesStr, lootQualitiesButtonTextPosition.x, lootQualitiesButtonTextPosition.y, 0.45f, -6 ,Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		lootQualitiesButtonText.hidden = false;
	}
}