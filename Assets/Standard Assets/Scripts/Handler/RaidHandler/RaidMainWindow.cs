﻿using UnityEngine;
using System.Collections;

public class RaidMainWindow
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Rect backgroudRect;
	private Rect backgroundZoneRect;
	private Rect raidTextRect;
	
	private Rect changeToRaidRect;
	private Rect raidInfoRect;
	private Rect guildRect;
	private Rect friendRect;
	private Rect craftRect;
	private Rect battlegroundRect;
	private Rect quitRect;
	
	private Vector2 scrollPosition = Vector2.zero;
	private Rect scrollZone;
	private Rect fullScrollZone;
	
	private Texture2D backgroundTexture;
	private Texture2D mainTexture;
	
	private GUIStyle normalMCStyle = new GUIStyle();
	private GUIStyle normalMLStyle = new GUIStyle();
	
	public RaidMainWindow ()
	{
		backgroudRect = new Rect(0, sh * 0.1f, sw, sh * 0.9f);
		backgroundZoneRect = new Rect(0, sh * 0.13f, sw * 0.8f, sh * 0.84f);
		raidTextRect = new Rect(sw * 0.3f, sh * 0.16f, sw * 0.2f, sh * 0.06f);
		changeToRaidRect = new Rect(sw * 0.02f, sh * 0.24f, sw * 0.38f, sw * 0.054f);
		raidInfoRect = new Rect(sw * 0.42f, sh * 0.24f, sw * 0.36f, sw * 0.054f);
		guildRect = new Rect(sw * 0.815f, sh * 0.23f, sw * 0.18f, sw * 0.049f);
		friendRect = new Rect(sw * 0.815f, sh * 0.39f, sw * 0.18f, sw * 0.049f);
		craftRect = new Rect(sw * 0.815f, sh * 0.55f, sw * 0.18f, sw * 0.049f);
		battlegroundRect = new Rect(sw * 0.815f, sh * 0.55f, sw * 0.18f, sw * 0.049f);
		quitRect = new Rect(sw * 0.815f, sh * 0.81f, sw * 0.18f, sw * 0.049f);
		scrollZone = new Rect(sw * 0.03f, sh * 0.4f, sw * 0.73f, sh * 0.55f);
		fullScrollZone = new Rect(0, 0, sw * 0.56f, sh * 0.85f);

		backgroundTexture = Resources.Load<Texture2D>("GUI/InGame/basic_background");
		mainTexture = Resources.Load<Texture2D>("GUI/BigTextures/InfoScreenBackground");
		
		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		
		normalMCStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.04f, 
		                                        Color.black, true, FontStyle.Normal);
		normalMLStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.04f, 
		                                        Color.black, true, FontStyle.Normal);
	}
	
	public void  showWindow ()
	{
		GUI.DrawTexture(backgroudRect, backgroundTexture);
		GUI.DrawTexture(backgroundZoneRect, mainTexture);
		GUIAtlas.kitchenSinkSheet.DrawTexture(raidTextRect, "raid_list.png");
		
		
		GUIAtlas.kitchenSinkSheet.DrawTexture(changeToRaidRect, "Change to Raid.png");
		if(GUI.Button(changeToRaidRect, "", GUIStyle.none))
			changeToRaidDelegate();
		
		GUIAtlas.kitchenSinkSheet.DrawTexture(raidInfoRect, "Raid Info.png");
		if(GUI.Button(raidInfoRect, "", GUIStyle.none))
			raidInfoDelegate();
		
		//		GUIAtlas.kitchenSinkSheet.DrawTexture(guildRect, "guild.png");
		//		if(GUI.Button(guildRect, "", GUIStyle.none))
		//			guildDelegate();
		//		
		//		GUIAtlas.kitchenSinkSheet.DrawTexture(friendRect, "Friends.png");
		//		if(GUI.Button(friendRect, "", GUIStyle.none))
		//			friendDelegate();
		
		GUIAtlas.kitchenSinkSheet.DrawTexture(battlegroundRect, "Quit.png");
		if(GUI.Button(battlegroundRect, "", GUIStyle.none))
		{
			//			WorldSession.player.battlegroundManager.isActive = true;
			RaidWindowManager.setState(5);
			//			quitDelegate();
			//			
			//			WorldPacket pkt = new WorldPacket(OpCodes.CMSG_BATTLEFIELD_LIST, 6);
			//			uint bgtype = 32;
			//			pkt.Add(bgtype);
			//			byte aux = 1;
			//			pkt.append(aux);
			//			aux = 0;
			//			pkt.append(aux);
			//			realmSocket.outQueue.Push(pkt);
		}
		if(GUI.Button(quitRect, "", GUIStyle.none))
			quitDelegate();
		
		scrollPosition = GUI.BeginScrollView(scrollZone, scrollPosition, fullScrollZone, GUIStyle.none, GUIStyle.none);
		DrawInstanceCooldown();
		GUI.EndScrollView();
	}
	
	private void  DrawInstanceCooldown ()
	{
		int cooldownCount = RaidWindowManager.instanceCooldownList.Count;
		
		for(int index = 0; index < cooldownCount; index++)
		{
			float padding = sh * 0.1f * index;
			InstanceCooldown instanceCooldown = RaidWindowManager.instanceCooldownList[index];
			
			string name = "" + instanceCooldown.instanceName;
			string difficult = instanceCooldown.instanceDifficult;
			string time = instanceCooldown.instanceCooldown;
			
			Rect nameTextRect = new Rect(sw * 0.01f, padding, sw * 0.4f, sh * 0.04f);
			Rect difficultTextRect = new Rect(sw * 0.43f, padding, sw * 0.12f, sh * 0.04f);
			Rect timeTextRect = new Rect(sw * 0.55f, padding, sw * 0.2f, sh * 0.04f);
			
			GUI.Label(nameTextRect, name, normalMLStyle);
			GUI.Label(difficultTextRect, difficult, normalMCStyle);
			GUI.Label(timeTextRect, time, normalMCStyle);
		}
		fullScrollZone = new Rect(0, 0, sw * 0.71f, sh * 0.1f * cooldownCount);
	}
	
	private void  changeToRaidDelegate ()
	{
		GroupManager updateGroup = WorldSession.player.group;
		if(WorldSession.player.guid != updateGroup.leaderGUID)
		{
			quitDelegate();
			ErrorMessage.ShowError("You are not raid commander.");
		}
		else if(updateGroup.groupType == GroupType.GROUPTYPE_NORMAL && updateGroup.getPlayersCount() > 0)
		{
			updateGroup.changeToRaid();
		}
		else if(updateGroup.groupType == GroupType.GROUPTYPE_RAID)
		{
			quitDelegate();
			ErrorMessage.ShowError("You are already in raid.");
		}
		else
		{
			quitDelegate();
			ErrorMessage.ShowError("You can't create raid group\n with this group.");
		}
	}
	
	private void  raidInfoDelegate ()
	{
		GroupManager updateGroup = WorldSession.player.group;
		if(updateGroup.groupType == GroupType.GROUPTYPE_RAID)
			RaidWindowManager.setState(1);
		else
			ErrorMessage.ShowError("You are not in raid group.");
	}
	
//	private function guildDelegate()
//	{
//		RaidWindowManager.closeRaidWindow();
//		
//		WorldSession.player.menuSelected = 4;
//		WorldSession.player.guildUI.InitGuildAtlas();
//		WorldSession.player.guildUI.boolShowingGuild = true;
//	}
//	
//	private function friendDelegate()
//	{
//		RaidWindowManager.closeRaidWindow();
//		WorldSession.player.interf.SocialDelegate(null);
//	}

	private void  quitDelegate ()
	{
		RaidWindowManager.closeRaidWindow();		
		WorldSession.player.interf.backMode(null);
	}
}