using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemPosition
{
	public byte slot = 0;
	public byte bag = 0;
}

public class ItemManager
{
	public Dictionary<int, Bag> BaseBags = new Dictionary<int, Bag>();
	public Dictionary<int, Bag> BankBags = new Dictionary<int, Bag>();

	public List<Item> Items = new List<Item>();

	public Dictionary<int, Item> BaseBagsSlots = new Dictionary<int, Item>();
	public Dictionary<int, Item> BaseBankSlots = new Dictionary<int, Item>();
	public Dictionary<int, Item> EquippedSlots = new Dictionary<int, Item>();
	
	protected ushort TotalSlots = 16;
	protected Player playerScript;

	protected int ValuesCount = 2;
	
	public bool RangeMode = false; 


	public ItemManager(Player plr)
	{
		playerScript = plr;

		Items = new List<Item>();
		BaseBags = new Dictionary<int, Bag>();
		BankBags = new Dictionary<int, Bag>();

		BaseBagsSlots = new Dictionary<int, Item>();
		BaseBankSlots = new Dictionary<int, Item>();
		EquippedSlots = new Dictionary<int, Item>();
	}

	public void AddItem(Item newItem)
	{
		Item item = GetItemByGuid(newItem.guid);
		if(item == null)
		{
			if(newItem.bag == 0 && newItem.slot == 0)
			{
				ItemPosition itemPos = GetBagAndSlot(newItem);
				newItem.bag = itemPos.bag;
				newItem.slot = itemPos.slot;
			}
			Items.Add(newItem);
//			SetPositionForItem(newItem);
		}
	}

	ItemPosition GetBagAndSlot(Item item)
	{
		ItemPosition itemPos = new ItemPosition();
		ulong itemGuid;
		int index;

		for(index = (int)EquipmentSlots.EQUIPMENT_SLOT_START; index < (int)CurrencyTokenSlots.CURRENCYTOKEN_SLOT_END; index++)
		{
			int indexValue = (int)UpdateFields.EUnitFields.PLAYER_FIELD_INV_SLOT_HEAD + 2 * index;
			itemGuid = playerScript.GetUInt64Value(indexValue);
			if((itemGuid == item.guid) || (ByteBuffer.Reverse(itemGuid) == item.guid))
			{
				itemPos.bag = 255;
				itemPos.slot = (byte)index;
				return itemPos;
			}
		}
		
		foreach(KeyValuePair<int, Bag> bag in BaseBags)
		{
			for(index = 0; index < bag.Value.GetCapacity(); index++)
			{
				int indexValue = (int)UpdateFields.EContainerFields.CONTAINER_FIELD_SLOT_1 + 2 * index;
				itemGuid = bag.Value.GetUInt64Value(indexValue);
				
				if(itemGuid == item.guid)
				{
					item.slotType = SlotType.SLOTTYPE_PLAYER_INVENTORY;
					itemPos.bag = (byte)bag.Key;
					itemPos.slot = (byte)index;
					return itemPos;
				}
			}
		}

		foreach(KeyValuePair<int, Bag> bag in BankBags)
		{			
			if(bag.Value != null)
			{
				for(index = 0; index < bag.Value.GetCapacity(); index++)
				{
					int indexValue = (int)UpdateFields.EContainerFields.CONTAINER_FIELD_SLOT_1 + 2 * index;
					itemGuid = bag.Value.GetUInt64Value(indexValue);
					if(itemGuid == item.guid)
					{
						item.slotType = SlotType.SLOTTYPE_BANK_INVENTORY;
						itemPos.bag = (byte)bag.Value.GetBagBySlot();
						itemPos.slot = (byte)index;
						return itemPos;
					}
				}
			}
		}

		return itemPos;
	}

	public bool NeedUpdateInventory = false;
	public bool NeedUpdateEquipment = false;
	public bool NeedUpdateBanker = false;

	public bool RemoveItem(ulong guid)
	{
		bool ret = false;
		Item item = GetItemByGuid(guid);
		if(item != null)
		{
			ret = true;
			Items.Remove(item);

			switch(item.slotType)
			{
			case SlotType.SLOTTYPE_EQUIPMENT_BAGS:
				BaseBags.Remove(item.GetBagBySlot());
				playerScript.SendMessage("NeedUpdateBags");
				NeedUpdateInventory = true;
				break;
			case SlotType.SLOTTYPE_EQUIPMENT:
				UnEquipItem((EquipmentSlots)item.slot);
				EquippedSlots.Remove(item.slot);
				playerScript.SendMessage("NeedUpdateEquip");
				NeedUpdateEquipment = true;
				break;
			case SlotType.SLOTTYPE_MAIN_PLAYER_INVENTORY:
				BaseBagsSlots.Remove(item.GetInventorySlot());
				playerScript.SendMessage("NeedUpdateInventory");
				NeedUpdateInventory = true;
				break;
			case SlotType.SLOTTYPE_PLAYER_INVENTORY:
				UpdateInventoryBags();
				playerScript.SendMessage("NeedUpdateInventory");
				NeedUpdateInventory = true;
				break;
			case SlotType.SLOTTYPE_BANK_BAGS:
				BankBags.Remove(item.GetBagBySlot());
				NeedUpdateBanker = true;
				break;
			case SlotType.SLOTTYPE_MAIN_BANK_INVENTORY:
				BaseBankSlots.Remove(item.GetInventorySlot());
				NeedUpdateBanker = true;
				break;
			case SlotType.SLOTTYPE_BANK_INVENTORY:
				UpdateBankerBags();
				NeedUpdateBanker = true;
				break;
			}
		}
		return ret;
	}

	public Item GetItemByID(uint entry)
	{
		Item ret = Items.Find(element => (element.entry == entry));
		return ret;
	}

	public Item GetItemByGuid(ulong guid)
	{
		Item ret = Items.Find(element => (element.guid == guid));
		return ret;
	}

	public void SetPositionForItem(Item item)
	{
		if(item.bag == 255)
		{
			if(item.slot < (byte)EquipmentSlots.EQUIPMENT_SLOT_END)
			{
				item.slotType = SlotType.SLOTTYPE_EQUIPMENT;
				EquippedSlots[item.slot] = item;					
				EquipItem(item);
				SetEquipCooldownForItem(item);
				playerScript.SendMessage("NeedUpdateEquip");
				NeedUpdateEquipment = true;
			}
			else if(item.slot < (byte)InventorySlots.INVENTORY_SLOT_BAG_END)
			{
				item.slotType = SlotType.SLOTTYPE_EQUIPMENT_BAGS;
				Bag newBag = item as Bag;
				newBag.InitializeBag();
				BaseBags[item.GetBagBySlot()] = newBag;
				playerScript.SendMessage("NeedUpdateBags");
				NeedUpdateInventory = true;
			}
			else if(item.slot < (byte)InventoryPackSlots.INVENTORY_SLOT_ITEM_END)
			{
				item.slotType = SlotType.SLOTTYPE_MAIN_PLAYER_INVENTORY;
				BaseBagsSlots[item.GetInventorySlot()] = item;
				playerScript.SendMessage("NeedUpdateInventory");
				NeedUpdateInventory = true;
			}
			else if(item.slot < (byte)BankItemSlots.BANK_SLOT_ITEM_END)
			{
				item.slotType = SlotType.SLOTTYPE_MAIN_BANK_INVENTORY;
				BaseBankSlots[item.GetInventorySlot()] = item;
				playerScript.SendMessage("NeedUpdateBanker");
				NeedUpdateBanker = true;
			}
			else if(item.slot < (byte)BankBagSlots.BANK_SLOT_BAG_END)
			{
				item.slotType = SlotType.SLOTTYPE_BANK_BAGS;
				Bag newBag = item as Bag;
				newBag.InitializeBag();
				BankBags[item.GetBagBySlot()] = newBag;
				playerScript.SendMessage("NeedUpdateBanker");
				NeedUpdateBanker = true;
			}
		}
		else
		{
			switch(item.slotType)
			{
			case SlotType.SLOTTYPE_PLAYER_INVENTORY:
				UpdateInventoryBags();
				playerScript.SendMessage("NeedUpdateInventory");
				NeedUpdateInventory = true;
				break;
			case SlotType.SLOTTYPE_BANK_INVENTORY:
				UpdateBankerBags();
				NeedUpdateBanker = true;
				break;
			}
		}
	}

	public Item GetEquipeItem(byte slot)
	{
		Item ret = null;
		if(slot <= (byte)EquipmentSlots.EQUIPMENT_SLOT_END)
		{
			int firstEntry = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_1_ENTRYID + 2 * slot;
			uint entry = playerScript.GetUInt32Value((int)firstEntry);
			if(entry != 0)
			{
				ret = GetItemByID(entry);
				if(ret == null || ret.entry == 0)
				{
					ret = AppCache.sItems.GetItem(entry);
					if(ret == null || ret.entry == 0)
					{
						AppCache.sItems.AddRequestToQueue(entry, playerScript.guid);
					}
				}
			}
		}
		return ret;
	}

	/**
	 * Items info
	 */

	public uint GetItemsTotalCount(uint entry)
	{
		uint ret = 0;
		foreach(Item item in Items)
		{
			if(item.entry == entry && 
			   (item.slotType == SlotType.SLOTTYPE_MAIN_PLAYER_INVENTORY ||
			 	item.slotType == SlotType.SLOTTYPE_PLAYER_INVENTORY))
			{
				ret += item.GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT);
			}
		}
		return ret;
	}
	
	/// <summary>
	/// Gets the bag slot by containe item.
	/// </summary>
	/// <returns>The bag slot by containe item.</returns>
	/// <param name="item">Item.</param>
	public byte GetBagSlotByContaineItem(Item item) 
	{
		byte ret = 255;
		if(item.bag != 0)
		{
			ret = BaseBags[item.bag].slot;
		}
		return ret;
	}
	
	/// <summary>
	/// Gets the first empty slot in bag.
	/// </summary>
	/// <returns>The first empty slot in bag.</returns>
	/// <param name="bagNum">Bag number.</param>
	public byte GetFirstEmptySlotInBag(int bagIndex)
	{
		byte ret = 255;

		if(bagIndex == 255)
		{
			for(int index = 0; index < (int)TotalSlots; index++)
			{
				Item item;
				BaseBagsSlots.TryGetValue(index, out item);
				if(item == null)
				{
					ret = (byte)(index + (byte)InventorySlots.INVENTORY_SLOT_BAG_END);
					break;
				}
			}
		}
		else
		{
			ret = BaseBags[bagIndex].GetFirstEmptySlot();
		}
		return ret;
	}

	public byte GetFirstEmptySlotInBankBag()
	{
		byte ret = 255;
		for(int index = 0; index < 28; index++)
		{
			Item item;
			BaseBankSlots.TryGetValue(index, out item);
			if(item == null)
			{
				ret = (byte)(index + 39);
				break;
			}
		}
		return ret;
	}

	/// <summary>
	/// Gets the first empty slot in base bags.
	/// </summary>
	/// <returns>The first empty slot in base bags.</returns>
	/// <param name="">.</param>
	public ItemPosition GetFirstEmptySlotInBaseBags()
	{
		byte slotIndex;
		ItemPosition ret = new ItemPosition();

		slotIndex = GetFirstEmptySlotInBag(255);
		if(slotIndex != 255)
		{
			ret.slot = slotIndex;
			ret.bag = 255;
			return ret;
		}
		
		for(byte bagIndex = 1; bagIndex < 5; bagIndex++)
		{
			slotIndex = GetFirstEmptySlotInBag(bagIndex);
			if(slotIndex != 255)
			{
				ret.slot = slotIndex;
				ret.bag = bagIndex;
				break;
			}
		}
		return ret;
	}

	/// <summary>
	/// Gets the first empty slot for bag.
	/// </summary>
	/// <returns>The first empty slot for bag.</returns>
	/// <param name="bagSlot">Bag slot.</param>
	public byte GetFirstEmptySlotForBag()
	{
		byte ret = 1;
		for(byte bagIndex = 1; bagIndex < 5; bagIndex++)
		{
			if(!BaseBags.ContainsKey(bagIndex))
			{
				ret = bagIndex;
				break;
			}
		}
		return ret;
	}

	/// <summary>
	/// Determines whether this instance is bag slot empty the specified bagSlot slot.
	/// </summary>
	/// <returns><c>true</c> if this instance is bag slot empty the specified bagSlot slot; otherwise, <c>false</c>.</returns>
	/// <param name="bagSlot">Bag slot.</param>
	/// <param name="slot">Slot.</param>
	public bool IsBagSlotEmpty(byte bagSlot, byte slotIndex)
	{
		bool ret = false;
		if(bagSlot == 255)
		{
			if(!BaseBagsSlots.ContainsKey((int)slotIndex))
			{
				ret = true;
			}
		}
		else
		{
			ret = BaseBags[(int)bagSlot].IsSlotEmpty(slotIndex);
		}
		return ret;
	}
	
	public ushort EmptySlots()
	{
		ushort ret = (ushort)(TotalSlots - BaseBagsSlots.Count);
		foreach(KeyValuePair<int, Bag> baseBag in BaseBags)
		{
			ret += baseBag.Value.EmptySlotsCount();
		}
		return ret;
	}
	
	public bool MoveToFirstEmptySlot(Item item)
	{
		bool ret = false;
		if(item != null)
		{
			ItemPosition emptyPosition = GetFirstEmptySlotInBaseBags();
			if(emptyPosition.slot < 255)
			{
				MoveToSlot(item, emptyPosition.slot, emptyPosition.bag);
				ret = true;
			}
		}
		return ret;
	}
	
	public bool MoveBagToFirstEmptySlot(Item item)
	{
		bool ret = false;
		if(item != null)
		{
			for(byte bagIndex = 1; bagIndex < 4; bagIndex++)
			{
				if(bagIndex == item.bag)
				{
					continue;
				}

				byte slotIndex = GetFirstEmptySlotInBag((int)bagIndex);
				if(slotIndex < 255)
				{
					MoveToSlot(item, slotIndex, bagIndex);
					ret = true;
				}
			}
		}
		return ret;
	}

	public void MoveToSlot(Item item, byte destSlot, byte destBag)
	{
		if(item == null)
		{
			return;
		}

		byte sourceBag = 255;
		byte sourceSlot = item.slot;

		if(item.isInContainer)
		{
			sourceBag = item.GetSlotByBag();
		}

		if(sourceSlot == 255)
		{
			return;
		}
		
		SendSwapItemPkt(destBag, destSlot, sourceBag, sourceSlot);
	}

	public void InstantiateWeaponAnimationHolder(ItemEntry item)
	{
		if(item.Class != (int)ItemClass.ITEM_CLASS_WEAPON)
		{
			return;
		}
		
		string playerRaceName = dbs.sCharacterRace.GetRecord((uint)playerScript.race).Name;

		string _subClass = "";
		switch(item.SubClass)
		{
		case 0:
		case 4:
			_subClass = "1HAxe";
			break;
			
		case 1:
		case 5:
			_subClass = "2HSword";
			break;
			
		case 2:
			_subClass = "Bow";
			break;
			
		case 6:
			_subClass = "Polearm";
			break;
			
		case 7:
		case 15:
		case 16:
			_subClass = "1HSword";
			break;
		
		case 8:
		case 20:
			_subClass = "2HSword";
			break;
			
		case 10:
			if(playerRaceName == "Orc")
				_subClass = "Polearm";
			else
				_subClass = "Staff";
			break;
			
		case 3:
		case 18:
			_subClass = "Crossbow";
			break;
			
		default:
			_subClass = "";
			break;
		}
		
		if(_subClass != "")
		{
			string playerGenderName = dbs.sCharacterGender.GetRecord((int)playerScript.gender).Name;
			string ResourceString = string.Format ("prefabs/chars/{0}_{1}/Animations/{2}", playerRaceName, playerGenderName, _subClass);
			GameObject _animHolder = Object.Instantiate<GameObject>(GameResources.Load<GameObject>(ResourceString));
			
			if(!_animHolder)
			{
				return;
			}

			Dictionary<string, object> paramData = new Dictionary<string, object>();
			paramData.Add("animHolder", _animHolder);
			paramData.Add("slot", (object)GetInventoryTypeWeaponSlot(item.ID));
			playerScript.gameObject.BroadcastMessage("HandleAnimationsLoading", paramData);
		}
	}

	/**
	 * Equip Items update
	 */
	public void RefreshMainHand()
	{
		Item mainHandItem = GetEquipeItem((byte)EquipmentSlots.EQUIPMENT_SLOT_MAINHAND);
		if(mainHandItem != null && mainHandItem.entry > 0)
		{
			Item itemProt = AppCache.sItems.GetItem(mainHandItem.entry);
			uint _displayID = DefaultModels.GetModel(itemProt);
			EquipmentSlots slot = (EquipmentSlots)GetInventoryTypeWeaponSlot(itemProt.entry);
			InstantiateWeaponAnimationHolder(itemProt.ToItemEntry());
			playerScript.armory.EquipWeapon(_displayID, slot);
		}
		RangeMode = false;
	}
	
	public void RefreshOffHand()
	{
		Item offHandItem = GetEquipeItem((byte)EquipmentSlots.EQUIPMENT_SLOT_OFFHAND);
		if(offHandItem != null && offHandItem.entry > 0)
		{
			Item itemProt = AppCache.sItems.GetItem(offHandItem.entry);
			uint _displayID = DefaultModels.GetModel(itemProt);
			EquipmentSlots slot = (EquipmentSlots)GetInventoryTypeWeaponSlot(itemProt.entry);
			if(itemProt.inventoryType == InventoryType.INVTYPE_SHIELD)
			{
				playerScript.armory.EquipShield(_displayID, slot);
			}
			else
			{
				InstantiateWeaponAnimationHolder(itemProt.ToItemEntry());
				playerScript.armory.EquipWeapon(_displayID, slot);
			}
		}			
		RangeMode = false;
//		MainPlayer.updateSlots=true;
	}
	
	public void DeactivateRange()
	{
		if(RangeMode)
		{
			Item rangeItem = GetEquipeItem((byte)EquipmentSlots.EQUIPMENT_SLOT_RANGED);
			if(rangeItem != null && rangeItem.entry != 0)
			{   
				playerScript.armory.UnEquipItem(playerScript, EquipmentSlots.EQUIPMENT_SLOT_RANGED);
			}
//			RefreshOffHand();
//			RefreshMainHand();
		}
//		MainPlayer.updateSlots=true;
	}
	
	public bool IsEquipedRangeWeapon()
	{
		Item rangeItem = GetEquipeItem((byte)EquipmentSlots.EQUIPMENT_SLOT_RANGED);
		bool ret = (rangeItem.entry != 0);
		return ret;
	}
	
	public bool ActivateRange()
	{
		if(!IsEquipedRangeWeapon())
		{
			return false;
		}

		if(RangeMode)
		{
			return true;
		}

		Item rangeItem = GetEquipeItem((byte)EquipmentSlots.EQUIPMENT_SLOT_RANGED);
		Item itemProt = AppCache.sItems.GetItem(rangeItem.entry);
		uint _displayID = DefaultModels.GetModel(itemProt);
		EquipmentSlots slot = (EquipmentSlots)GetInventoryTypeWeaponSlot(itemProt.entry);
		Item mainHandItem = GetEquipeItem((byte)EquipmentSlots.EQUIPMENT_SLOT_MAINHAND);
		Item offHandItem = GetEquipeItem((byte)EquipmentSlots.EQUIPMENT_SLOT_OFFHAND);
		
		if(offHandItem != null && offHandItem.entry != 0)
		{
			playerScript.armory.UnEquipItem(playerScript, EquipmentSlots.EQUIPMENT_SLOT_OFFHAND);
		}

		if(mainHandItem != null && mainHandItem.entry != 0)
		{
			playerScript.armory.UnEquipItem(playerScript, EquipmentSlots.EQUIPMENT_SLOT_MAINHAND);
		}

		InstantiateWeaponAnimationHolder(itemProt.ToItemEntry());
		playerScript.armory.EquipWeapon(_displayID, slot);	 
		RangeMode = true;
		return true;		
	}	

	public void UnEquipItem(EquipmentSlots slot)
	{
		if(!playerScript.stealth && playerScript.armory != null)
		{
			playerScript.armory.UnEquipItem(playerScript, slot);
		}
		playerScript.SendMessage("NeedUpdateEquip");
	}
	
	public void EquipItem(uint entry)
	{
		Item item = new Item(entry);
		if(item != null)
		{
			EquipItem(item);
		}
	}
	
	public void EquipItem(Item item)
	{
		if(item == null)
		{
			return;
		}

		uint _displayID = 0;
		ItemEntry _item;
		_item = AppCache.sItems.GetItemEntry(item.entry);
		if(_item.ID == 0)
		{
			_item = item.ToItemEntry();
		}
		
		byte slot = ItemManager.staticGetSlotFromInvType(_item.InventorySlot);
		// Loading Armours	
		switch(slot)
		{
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_HEAD:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_NECK:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_SHOULDERS:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_BODY:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_CHEST:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_WAIST:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_LEGS:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_FEET:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_WRISTS:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_HANDS:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_FINGER1:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_FINGER2:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_TRINKET1:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_TRINKET2:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_BACK:
		case (byte)EquipmentSlots.EQUIPMENT_SLOT_TABARD:
			_displayID = DefaultModels.GetModel(item);
			
			if(!playerScript.stealth)
			{
				playerScript.armory.UpdateArmor(_displayID , (EquipmentSlots)slot);
			}
			return;
		}
		
		//loading Shields
		if(_item.InventorySlot == (int)InventoryType.INVTYPE_SHIELD)
		{
			if(RangeMode)
			{
				DeactivateRange();
			}
			_displayID = DefaultModels.GetModel(item);
			playerScript.armory.EquipShield(_displayID, (EquipmentSlots)slot);
			if(playerScript.charMount)
			{
				playerScript.ShowHideWeapons(false);
			}
			return;			
		}
		
		if(_item.InventorySlot == (int)InventoryType.INVTYPE_RANGED ||
		   _item.InventorySlot == (int)InventoryType.INVTYPE_RELIC ||
		   _item.InventorySlot == (int)InventoryType.INVTYPE_THROWN ||
		   _item.InventorySlot == (int)InventoryType.INVTYPE_RANGEDRIGHT)
		{
			if(RangeMode)
			{
				RangeMode = false;
				ActivateRange();
			}
			if(playerScript.charMount)
			{
				playerScript.ShowHideWeapons(false);
			}
			return;
		}
		
		switch(_item.InventorySlot)
		{
		case (int)InventoryType.INVTYPE_WEAPONMAINHAND :
		case (int)InventoryType.INVTYPE_2HWEAPON:		 				 				 	
		case (int)InventoryType.INVTYPE_HOLDABLE :     
		case (int)InventoryType.INVTYPE_WEAPONOFFHAND :
		case (int)InventoryType.INVTYPE_WEAPON:
			
			if(RangeMode)
			{
				DeactivateRange();
			}

			if(_item.InventorySlot != (int)InventoryType.INVTYPE_WEAPON)
			{
				InstantiateWeaponAnimationHolder(_item);
				_displayID = DefaultModels.GetModel(item);
				playerScript.armory.EquipWeapon(_displayID, (EquipmentSlots)slot);
			}
			else
			{
				InstantiateWeaponAnimationHolder(_item);
				_displayID = DefaultModels.GetModel(item);
				playerScript.armory.EquipWeapon(_displayID, (EquipmentSlots)GetInventoryTypeWeaponSlot(_item.ID));
			}
			if(playerScript.charMount)
			{
				playerScript.ShowHideWeapons(false);
			}
			return;
			
		}
		return;
	}
	
	public static string staticGetMeshNameFromInvType(int itemType)
	{
		return  staticGetMeshNameFromSlot((EquipmentSlots)staticGetSlotFromInvType(itemType));
	}
	
	public static string staticGetMeshNameFromSlot(EquipmentSlots slot)
	{
		string returnValue = "";
		//Debug.Log("staticGetMeshNameFromSlot   "+ slot);		
		//inventory slots from 0 to 18
		switch(slot)
		{
		case EquipmentSlots.EQUIPMENT_SLOT_HEAD:
			returnValue = "Default/Head";
			break;
		case EquipmentSlots.EQUIPMENT_SLOT_NECK:
			returnValue = "Default/Neck";
			break;
		case EquipmentSlots.EQUIPMENT_SLOT_SHOULDERS:
			returnValue = "Default/Shoulders";
			break;
		case EquipmentSlots.EQUIPMENT_SLOT_CHEST:
			returnValue = "Default/Chest";
			break;
		case EquipmentSlots.EQUIPMENT_SLOT_WAIST:
			returnValue = "Default/Waist";
			break;
		case EquipmentSlots.EQUIPMENT_SLOT_LEGS:
			returnValue = "Default/Pants";
			break;
		case EquipmentSlots.EQUIPMENT_SLOT_FEET:
			returnValue = "Default/Boots";
			break;
		case EquipmentSlots.EQUIPMENT_SLOT_WRISTS:
			returnValue = "Default/Wrists";
			break;
		case EquipmentSlots.EQUIPMENT_SLOT_HANDS:
			returnValue = "Default/Gloves";
			break;
		}
		return returnValue;		
	}
	
	public byte GetInventoryTypeWeaponSlot(uint itemEntry)
	{
		int index = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_16_ENTRYID;
		uint entry = playerScript.GetUInt32Value((int)index);
		if(entry == itemEntry)
		{
			return (byte)EquipmentSlots.EQUIPMENT_SLOT_MAINHAND;
		}
		
		index = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_17_ENTRYID;
		entry = playerScript.GetUInt32Value((int)index);
		if(entry == itemEntry)
		{
			return (byte)EquipmentSlots.EQUIPMENT_SLOT_OFFHAND;
		}		
		
		index = (int)UpdateFields.EUnitFields.PLAYER_VISIBLE_ITEM_18_ENTRYID;
		entry = playerScript.GetUInt32Value((int)index);
		if(entry == itemEntry)
		{
			return (byte)EquipmentSlots.EQUIPMENT_SLOT_RANGED;
		}

		return (byte)EquipmentSlots.EQUIPMENT_SLOT_MAINHAND;		
	}
	
	public static byte staticGetSlotFromInvType(int inventoryType)
	{
		byte returnValue = 255;
		switch(inventoryType)
		{
		case (int)InventoryType.INVTYPE_RANGEDRIGHT: /// can't equip them fron what i know
		case (int)InventoryType.INVTYPE_NON_EQUIP: //-1
			returnValue = 255;
			break;
		case (int)InventoryType.INVTYPE_HEAD:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_HEAD;
			break;
		case (int)InventoryType.INVTYPE_NECK:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_NECK;
			break;
		case (int)InventoryType.INVTYPE_SHOULDERS:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_SHOULDERS;
			break;
		case (int)InventoryType.INVTYPE_BODY:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_BODY;
			break;
		case (int)InventoryType.INVTYPE_CHEST:
		case (int)InventoryType.INVTYPE_ROBE:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_CHEST;
			break;
		case (int)InventoryType.INVTYPE_WAIST:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_WAIST;
			break;
		case (int)InventoryType.INVTYPE_LEGS:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_LEGS;
			break;
		case (int)InventoryType.INVTYPE_FEET:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_FEET;
			break;
		case (int)InventoryType.INVTYPE_WRISTS:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_WRISTS;
			break;
		case (int)InventoryType.INVTYPE_HANDS:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_HANDS;
			break;
		case (int)InventoryType.INVTYPE_FINGER:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_FINGER1;// 11 or 12// default 11 ..12  just if placed there
			break;
		case (int)InventoryType.INVTYPE_TRINKET:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_TRINKET1;// 13 or 14// default 13 ..14  just if placed there
			break;
			/*case InventoryType.INVTYPE_WEAPON :
			returnValue = GetInventoryTypeWeaponSlot(player); // 16 or 17 //default 16 ..17  just if placed there
			break;*/		
		case (int)InventoryType.INVTYPE_HOLDABLE:
		case (int)InventoryType.INVTYPE_SHIELD:
		case (int)InventoryType.INVTYPE_WEAPONOFFHAND:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_OFFHAND;
			break;
		case (int)InventoryType.INVTYPE_RANGED:
		case (int)InventoryType.INVTYPE_THROWN:
		case (int)InventoryType.INVTYPE_RELIC:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_RANGED;
			break;
		case (int)InventoryType.INVTYPE_CLOAK:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_BACK;
			break;
		case (int)InventoryType.INVTYPE_2HWEAPON:
		case (int)InventoryType.INVTYPE_WEAPONMAINHAND:
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_MAINHAND;
			break;
		case (int)InventoryType.INVTYPE_QUIVER:
			// same as the bag
		case (int)InventoryType.INVTYPE_BAG :
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_END;
			break;
		case (int)InventoryType.INVTYPE_TABARD :
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_TABARD;
			break;
		case (int)InventoryType.INVTYPE_AMMO :
			returnValue = (byte)EquipmentSlots.EQUIPMENT_SLOT_END;
			break;
		}
		return returnValue;
	}
	
	public void SendSwapItemPkt(byte destBag, byte destSlot, byte sourceBag, byte sourceSlot)
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_SWAP_ITEM);
		pkt.Append(destBag);
		pkt.Append(destSlot);
		pkt.Append(sourceBag);
		pkt.Append(sourceSlot);
		RealmSocket.outQueue.Add(pkt);
	}

	public uint GetItemStackCount(uint itmEntry)
	{
		uint ret = 0;
		Item item = playerScript.itemManager.GetItemByID(itmEntry);
		if(item != null)
		{
			ret = item.GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT);
		}
		return ret;
	}
	
	public void SetEquipCooldownForItem(Item item)
	{
		ItemSpell itemSpell = item.GetOnActiveSpell();
		if(itemSpell != null)
		{
			Spell spell = SpellManager.GetSpell(itemSpell.ID);
			if(itemSpell != null && spell!= null)
			{
				Cooldown cooldown = new Cooldown(30.0f, 30.0f);
				playerScript.cooldownManager.playerCooldownList.SetCooldown(spell, cooldown);
			}
		}
	}
	
	public void SetCooldownForItems(Item activeItem, bool state)
	{
		ItemSpell activeItemSpell = activeItem.GetOnActiveSpell();
		foreach(Item item in Items)
		{
			ItemSpell itemSpell = item.GetOnActiveSpell();
			Spell spell = SpellManager.GetSpell(activeItemSpell.ID);
			if(spell != null && itemSpell != null && itemSpell.category == activeItemSpell.category)
			{
				playerScript.cooldownManager.SetCooldownState(spell, state);
			}
		}
	}

	public void DestroyItem(Item item, uint count)
	{
		//If count == 1 then we destroy one item
		//If count == 0 then we destroy all items
		if(item != null)
		{
			if(count == 1)
			{
				item.DestroyItem(1);
			}
			else
			{
				item.DestroyItem(0);
			}
		}
	}

	public void UpdateBaseBagItems()
	{
		int slotIndex;
		ulong itemGuid;
		Item tmpitem;
		
		for(int index = 0; index < (int)TotalSlots; index++)
		{
			slotIndex = (int)UpdateFields.EUnitFields.PLAYER_FIELD_PACK_SLOT_1 + 2 * index;
			itemGuid = playerScript.GetUInt64Value(slotIndex);
			if(itemGuid > 0)
			{
				tmpitem = GetItemByGuid(itemGuid);
				if(tmpitem == null)
				{
					tmpitem = GetItemByGuid(ByteBuffer.Reverse(itemGuid));
				}
				if(tmpitem != null)
				{
					tmpitem.bag = 255;
					tmpitem.slot = (byte)(index + (byte)InventorySlots.INVENTORY_SLOT_BAG_END);
					BaseBagsSlots[index] = tmpitem;
					tmpitem.isInContainer = false;
				}
				else
				{
					itemGuid = playerScript.GetUInt64ValueReverse(slotIndex);
//					if(itemGuid > 0)
//					{
//						Debug.Log("am gasit guid: " + itemGuid);
//
//					}
//					else
//					{
//						Debug.Log("nu am gasit guid: " + itemGuid);
//					}
				}
			}
		}
	}

	public void UpdateInventoryBags()
	{
		foreach(KeyValuePair<int, Bag> bag in BaseBags)
		{
			bag.Value.PopulateSlots(this);
		}
	}

	public void UpdateBankerBags()
	{
		foreach(KeyValuePair<int, Bag> bag in BankBags)
		{
			bag.Value.PopulateSlots(this);
		}
	}

	public List<Item> GetItemListByCondition(int itemClass, int subClass, int inventoryType)
	{
		List<Item> ret = Items.FindAll(element => ((int)element.classType == itemClass &&
		                                            (subClass == -1 || element.subClass == subClass) &&
		                                           (inventoryType == -1 || element.inventoryType == (InventoryType)inventoryType)));
		return ret;

	}
}
