using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AuraEffect
{
	public byte slot;
	public GameObject effect;
}

public class UpdateAuraStruct
{
	public ulong casterGuid;
	public BaseObject unit;
	public UpdateAuraStruct(ulong _casterGuid, BaseObject _unit)
	{
		casterGuid = _casterGuid;
		unit = _unit;
	}
}

public class AuraManager {

	public List<Aura> auras = new List<Aura>();
	public List<AuraEffect> auraEffectList = new List<AuraEffect>();
	public BaseObject unit; // unit that has an aura
	public GameObject mainPlayerGO;

	public AuraManager(BaseObject baseObj)
	{
		unit = baseObj;
	}

	public void UpdateAura(Aura aura)
	{
		byte index = 0;
		
		foreach(Aura au in auras)
		{
			if(au.auraSlot == aura.auraSlot)
			{
				au.auraLevel = aura.auraLevel;
				au.auraFlags = aura.auraFlags;
				au.auraCharges = aura.auraCharges;
				au.casterGuid = aura.casterGuid;
				au.auraMaxDuration = aura.auraMaxDuration;
				au.auraDuration = aura.auraDuration;
				au.unit = aura.unit;
				
				au.spellID = aura.spellID;
				au.Apply();
				
				if(au.spellID == 0) 
				{
					auras.RemoveAt(index);
				}
				if(mainPlayerGO != null)
				{
					mainPlayerGO.BroadcastMessage("UpdateAllAuras", new UpdateAuraStruct(au.casterGuid, unit),
					                            SendMessageOptions.RequireReceiver);
				}

				UpdateAuraEffect(aura);
				return;
			}
			index++;
		}			
		
		auras.Add(aura);
		UpdateAura(aura);
	}
	
	public void UpdateAura(Aura aura, Unit obj)
	{
		int effectId;
		SpellVisual scriptSpellVisual;
		GameObject effectObject;
		SpellEntry spellEntry;
		byte index = 0;
		if(aura.unit == null)
		{
			aura.unit = obj;
		}
		
		foreach(Aura au in auras)
		{
			if(au.auraSlot == aura.auraSlot)
			{
				au.auraLevel = aura.auraLevel;
				au.auraFlags = aura.auraFlags;
				au.auraCharges = aura.auraCharges;
				au.casterGuid = aura.casterGuid;
				au.auraMaxDuration = aura.auraMaxDuration;
				au.auraDuration = aura.auraDuration;
				au.unit = aura.unit;

				if(aura.spellID == 0)
				{
					scriptSpellVisual = FxEffectManager.GetSpellVisual(au.spellID);
					if(scriptSpellVisual)
					{
						effectId = scriptSpellVisual.effect[1];
						FxEffectManager.DestroyAndRemoveEffect(effectId, obj.guid);
					}
				}
				else if(au.spellID == 0 && aura.spellID != 0)
				{	
					if(!CheckFxOptions(obj.guid))
					{
						scriptSpellVisual = FxEffectManager.GetSpellVisual(aura.spellID);
						if(scriptSpellVisual)
						{
							effectId = scriptSpellVisual.effect[1];
							if(effectId > 0)
							{
								BaseObject baseObj = obj;
								spellEntry = dbs.sSpells.GetRecord(aura.spellID);
								FxEffectManager.CastEffect(spellEntry.speed, aura.auraMaxDuration / 1000f, obj.gender,
								                           effectId, baseObj, baseObj);
							}
						}
					}
				}
				
				au.spellID = aura.spellID;
				au.Apply();
				
				if(au.spellID == 0)
				{		
					auras.RemoveAt(index);
				}

				if(mainPlayerGO != null)
				{
					mainPlayerGO.BroadcastMessage("UpdateAllAuras", new UpdateAuraStruct(au.casterGuid, unit),
					                            SendMessageOptions.RequireReceiver);
				}

				UpdateAuraEffect(aura);
				return;
			}
			index++;
		}			
		
		auras.Add(aura);
		if(aura.spellID != 0 && !(aura.auraMaxDuration == 0 && aura.auraDuration == 0))
		{
			if(!CheckFxOptions(obj.guid))
			{
				scriptSpellVisual = FxEffectManager.GetSpellVisual(aura.spellID);
				if(scriptSpellVisual)
				{
					effectId = scriptSpellVisual.effect[1];
					if(effectId > 0)
					{
						BaseObject baseObj = obj;
						spellEntry = dbs.sSpells.GetRecord(aura.spellID);
						FxEffectManager.CastEffect(spellEntry.speed, aura.auraMaxDuration/1000f, obj.gender,
						                           effectId, baseObj, baseObj);
					}
				}
			}
		}
		
		UpdateAura(aura);
	}
	
	public bool CheckFxOptions(ulong guid)
	{
		bool ret = false;
		if(guid == unit.guid && PlayerPrefs.GetInt("ShowPlayerFx", 1) == 0)
		{
			ret = true;
		}
		else if(guid != unit.guid && PlayerPrefs.GetInt("ShowOtherPlayerFx", 1) == 0)
		{
			ret = true;
		}
		return ret;
	}
	
	public void UpdateAuraEffect(Aura aura)
	{
		foreach(AuraEffect auraEffect in auraEffectList)
		{
			if(aura.auraSlot == auraEffect.slot)
			{
				if(auraEffect.effect != null)
				{
					UnityEngine.Object.Destroy(auraEffect.effect);
				}

				if(aura.spellID == 0)
				{
					auraEffectList.Remove(auraEffect);
				}
				else
				{
					auraEffect.slot = aura.auraSlot;
				}
				return;
			}			
		}
		
		//New aura
		if(aura.spellID != 0)
		{
			AuraEffect effect;
			effect = new AuraEffect();
			effect.slot = aura.auraSlot;
			auraEffectList.Add(effect);	
		}
	}
	
	public void RemoveAura(int id)
	{
		auras[id].Remove();
	}
}
