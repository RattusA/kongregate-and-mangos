﻿
public class PartyPlayer
{
	public ulong guid;
	public uint mask;
	public GroupMemberFlags status;
	public uint HP;
	public uint maxHP;
	public Powers powerType;
	public float MP;
	public float maxMP;
	public uint level;
	public ushort zoneID;
	public ushort positionX;
	public ushort positionY;
	public ulong auramask;
	public UISprite[] auraBuff = new UISprite[32];
	public Classes playerClass;
	public string name;
	public byte groupID;
	public byte flags;
	public byte checkFlag = 255;
}