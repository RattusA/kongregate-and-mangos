using System.Collections.Generic;

public class needManager
{
	public class ItemName
	{
		public uint entry;
		public string name;
	}
	
	ulong guid;
	List<ulong> guids = new List<ulong>();
	List<LootItem> needItems = new List<LootItem>();
	List<uint> countDowns = new List<uint>();
	List<ItemName> itemNames = new List<ItemName>();
	Item item = new Item();
	MainPlayer mainPlayer;
	LootItem inWorkItem;
	bool  finish;
	uint entry;
	public uint tempEntry;
	Item tempItem;
	
	public needManager ( MainPlayer plr )
	{
		mainPlayer = plr;
	}
	
//	void  newArrival ( LootItem loot ,  uint count ,   ulong guid )
//	{
//		//AppCache.sItems.SendQueryItem(loot.itemId);
//		needItems.Add(loot);
//		countDowns.Add(count);
//		guids.Add(guid);
//		if (needItems.Count==1){
//			finish=true;
//			manageNeed();
//		}
//		Debug.Log("Am adaugat need iteme"+needItems.Count+"Si countDowns:"+countDowns.Count);
//	}
	
//	void  manageNeed ()
//	{
//		if(countDowns.Count != 0)
//		{
//			int length = countDowns.Count;
//			while (length > 0)
//			{
//				int i = length - 1;
//				int aux = countDowns[i];
//				aux -= (Time.deltaTime) * 1000;
//				countDowns[i]= aux;
//				
//				if(aux < 0)
//				{
//					countDowns.RemoveAt(i);
//					needItems.RemoveAt(i);
//					guids.RemoveAt(i);
//					if(i==0)
//					{
//						finish=true;
//						item=null;
//						Button.refreshNeed = true;
//						entry = 0;
//					}
//				}
//				length-=1;
//			}
//			//Debug.Log(finish+ " " +needItems.Count );
//			if(needItems.Count!=0 && finish)
//			{
//				Debug.Log(needItems.Count);
//				inWorkItem = (needItems[0] as LootItem);
//				guid = guids[0];
//				entry= inWorkItem.itemId;
//				Debug.Log("Am intrat unde trebuie, v-om cere info despre itemul"+inWorkItem.itemId);
//				//AppCache.sItems.SendQueryItem(inWorkItem.itemId);
//				finish = false;
//			}
//		}
//	}
	
//	void  nextNeed ()
//	{	
//		if(countDowns.Count==0 || needItems.Count==0 || finish)
//		{
//			Button.refreshNeed = true;
//			//Button.needState = false;
//			return;
//		}
//		item=null;
//		countDowns.RemoveAt(0);
//		needItems.RemoveAt(0);
//		guids.RemoveAt(0);
//		finish=true;
//		item=null;
//		entry = 0;
//		Button.refreshNeed = true;
//		if(countDowns.Count==0 || needItems.Count==0)
//		{
//			Button.refreshNeed = true;
//			return;
//		}
//	}
	
//	void  AddName ( uint entry, string str )
//	{
//		itemName itemN = new itemName();  
//		itemN.entry=entry;
//		itemN.name=str;
//		
//		itemNames.Add(itemN);
//	}
	
	public string GetName (uint entry)
	{
		string ret = string.Empty;
		ItemName itemN = itemNames.Find(element => (element.entry == entry));
		if (itemN != null)
		{
			ret = itemN.name;
		}
		return ret;
	}
	
	public void EraseName (uint entry)
	{
		for(int i = 0; i < itemNames.Count; i++)
		{
			if(itemNames[i].entry == entry)
			{
				itemNames.RemoveAt(i);
			}
		}
	}
}