﻿
public enum LootMethod
{
	FREE_FOR_ALL      = 0,
	ROUND_ROBIN       = 1,
	MASTER_LOOT       = 2,
	GROUP_LOOT        = 3,
	NEED_BEFORE_GREED = 4
};

public enum RollVote
{
	ROLL_PASS              = 0,
	ROLL_NEED              = 1,
	ROLL_GREED             = 2,
	ROLL_DISENCHANT        = 3,
	
	// other not send by client
	MAX_ROLL_FROM_CLIENT   = 4,
	
	ROLL_NOT_EMITED_YET    = 4,                             // send to client
	ROLL_NOT_VALID         = 5                              // not send to client
};

// set what votes allowed
public enum RollVoteMask
{
	ROLL_VOTE_MASK_PASS       = 0x01,
	ROLL_VOTE_MASK_NEED       = 0x02,
	ROLL_VOTE_MASK_GREED      = 0x04,
	ROLL_VOTE_MASK_DISENCHANT = 0x08,
	
	ROLL_VOTE_MASK_ALL        = 0x0F,
};

public enum GroupMemberFlags
{
	MEMBER_STATUS_OFFLINE   = 0x0000,
	MEMBER_STATUS_ONLINE    = 0x0001,                       // Lua_UnitIsConnected
	MEMBER_STATUS_PVP       = 0x0002,                       // Lua_UnitIsPVP
	MEMBER_STATUS_DEAD      = 0x0004,                       // Lua_UnitIsDead
	MEMBER_STATUS_GHOST     = 0x0008,                       // Lua_UnitIsGhost
	MEMBER_STATUS_PVP_FFA   = 0x0010,                       // Lua_UnitIsPVPFreeForAll
	MEMBER_STATUS_UNK3      = 0x0020,                       // used in calls from Lua_GetPlayerMapPosition/Lua_GetBattlefieldFlagPosition
	MEMBER_STATUS_AFK       = 0x0040,                       // Lua_UnitIsAFK
	MEMBER_STATUS_DND       = 0x0080,                       // Lua_UnitIsDND
};

public enum GroupType                                              // group type flags?
{
	GROUPTYPE_NORMAL = 0x00,
	GROUPTYPE_BG     = 0x01,
	GROUPTYPE_RAID   = 0x02,
	GROUPTYPE_BGRAID = 0x03,//GroupType.GROUPTYPE_BG | GroupType.GROUPTYPE_RAID,       // mask
	GROUPTYPE_LFG_RESTRICTED = 0x04, // ? Script_HasLFGRestrictions() for trinity
	GROUPTYPE_LFD    = 0x08,
	GROUPTYPE_CHANGE_GROUP = 0x10, //leave/change group?, I saw this flag when leaving group and after leaving BG while in group
};

public enum GroupFlagMask
{
	GROUP_ASSISTANT      = 0x01,
	GROUP_MAIN_ASSISTANT = 0x02,
	GROUP_MAIN_TANK      = 0x04,
};

public enum GroupUpdateFlags
{
	GROUP_UPDATE_FLAG_NONE              = 0x00000000,       // nothing
	GROUP_UPDATE_FLAG_STATUS            = 0x00000001,       // uint16, flags
	GROUP_UPDATE_FLAG_CUR_HP            = 0x00000002,       // uint32
	GROUP_UPDATE_FLAG_MAX_HP            = 0x00000004,       // uint32
	GROUP_UPDATE_FLAG_POWER_TYPE        = 0x00000008,       // uint8
	GROUP_UPDATE_FLAG_CUR_POWER         = 0x00000010,       // uint16
	GROUP_UPDATE_FLAG_MAX_POWER         = 0x00000020,       // uint16
	GROUP_UPDATE_FLAG_LEVEL             = 0x00000040,       // uint16
	GROUP_UPDATE_FLAG_ZONE              = 0x00000080,       // uint16
	GROUP_UPDATE_FLAG_POSITION          = 0x00000100,       // uint16, uint16
	GROUP_UPDATE_FLAG_AURAS             = 0x00000200,       // uint64 mask, for each bit set uint32 spellid + uint8 unk
	GROUP_UPDATE_FLAG_PET_GUID          = 0x00000400,       // uint64 pet guid
	GROUP_UPDATE_FLAG_PET_NAME          = 0x00000800,       // pet name, NULL terminated string
	GROUP_UPDATE_FLAG_PET_MODEL_ID      = 0x00001000,       // uint16, model id
	GROUP_UPDATE_FLAG_PET_CUR_HP        = 0x00002000,       // uint32 pet cur health
	GROUP_UPDATE_FLAG_PET_MAX_HP        = 0x00004000,       // uint32 pet max health
	GROUP_UPDATE_FLAG_PET_POWER_TYPE    = 0x00008000,       // uint8 pet power type
	GROUP_UPDATE_FLAG_PET_CUR_POWER     = 0x00010000,       // uint16 pet cur power
	GROUP_UPDATE_FLAG_PET_MAX_POWER     = 0x00020000,       // uint16 pet max power
	GROUP_UPDATE_FLAG_PET_AURAS         = 0x00040000,       // uint64 mask, for each bit set uint32 spellid + uint8 unk, pet auras...
	GROUP_UPDATE_FLAG_VEHICLE_SEAT      = 0x00080000,       // uint32 vehicle_seat_id (index from VehicleSeat.dbc)
	GROUP_UPDATE_PET                    = 0x0007FC00,       // all pet flags
	GROUP_UPDATE_FULL                   = 0x0007FFFF,       // all known flags
};

public static class group
{
	static public string getRaidDifficultName ( Difficulty difficult )
	{
		string ret = "";
		switch(difficult)
		{
		case Difficulty.RAID_DIFFICULTY_10MAN_NORMAL:
			ret = "10-man";
			break;
		case Difficulty.RAID_DIFFICULTY_25MAN_NORMAL:
			ret = "25-man";
			break;
		case Difficulty.RAID_DIFFICULTY_10MAN_HEROIC:
			ret = "10-man legendary";
			break;
		case Difficulty.RAID_DIFFICULTY_25MAN_HEROIC:
			ret = "25-man legendary";
			break;
		}
		return ret;
	}
}
