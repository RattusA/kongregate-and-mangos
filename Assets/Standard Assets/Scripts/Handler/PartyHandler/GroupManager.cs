using UnityEngine;
using System.Collections.Generic;

public class GroupManager
{
	protected byte RAID_GROUP_COUNT = 8;
	protected byte MAX_GROUP_COUNT = 5;
	
	private Dictionary<ulong, PartyPlayer>[] groupMember;
	private int mainPlayerGroupID = 0;
	bool  doNotShowSpellIcons = false;
	
	public ulong leaderGUID;	
	public byte groupID;// in basic wow there are 8 groups
	public GroupType groupType;
	public byte groupFlags;
	public byte isBGGroup;
	public ulong voiceChat;
	public uint membersCount;
	public LootMethod lootMethod;
	public ulong looterGUID;
	public ItemQualities lootThreshold;
	public byte dungeonDifficulty;
	public Difficulty raidDifficulty;
	public Rect partyRect;
	Rect hpRect;
	Rect energyRect;
	Rect leaderRect;
	public uint buffDim;
	
	Vector2 namePosition;
	
	Texture partyFrame;
	Texture hpBar;
	Texture energyBar;
	Texture backGroundBar;
	Texture leaderMark;
	
	MainPlayer mainPlayer;	
	
	private UISprite frame;
	private UITextInstance text;
	
	private UIButton confirmBtn;
	private UITextInstance confirmBtnText;
	
	private UIButton declineBtn;
	private UITextInstance declineBtnText;
	
	private UIText textStyle = new UIText(UIPrime31.myToolkit1, "fontiny dark", "fontiny dark.png");
	
	public GroupManager ( MainPlayer player )
	{
		groupMember = new Dictionary<ulong, PartyPlayer>[RAID_GROUP_COUNT];
		buffDim = (uint)(Screen.width * 0.02225f);
		for(byte index = 0; index < RAID_GROUP_COUNT; index++)
		{
			groupMember[index] = new Dictionary<ulong, PartyPlayer>();
		}
		
		mainPlayer = player;
		addMainPlayerToGroup();
		
		partyRect= new Rect(0, Screen.width*0.1022f,Screen.width*0.1492f, Screen.width*0.0445f);
		hpRect = new Rect(Screen.width*0.0036f, Screen.width*0.1217f, Screen.width*0.139f, Screen.width*0.0062f);
		energyRect = new Rect(Screen.width*0.0036f, Screen.width*0.1326f, Screen.width*0.139f, Screen.width*0.0062f);
		leaderRect = new Rect(Screen.width*0.1376f, Screen.width*0.0978f, Screen.width*0.0234f,Screen.width*0.0218f);
		partyFrame = Resources.Load<Texture>("Party Textures/party_frame");
		hpBar = Resources.Load<Texture>("Party Textures/party_health");
		energyBar = Resources.Load<Texture>("Party Textures/party_mana");
		backGroundBar = Resources.Load<Texture>("Party Textures/party_progress_bar");
		leaderMark = Resources.Load<Texture>("Party Textures/party_leader");
		namePosition = new Vector2(Screen.width*0.0069f, Screen.width*0.1f);
	}
	
	/**
	 *	PUBLIC FUNCTIONS
	 */
	
	public void  setMainPlayerGroup ( byte grorp )
	{
		mainPlayerGroupID = (int)grorp;
	}
	
	public PartyPlayer getOnlinePlayer ( int index )
	{
		PartyPlayer ret = null;
		if(groupMember[mainPlayerGroupID].Count > 1)
		{
			ulong[] key = new ulong[groupMember[mainPlayerGroupID].Count];
			groupMember[mainPlayerGroupID].Keys.CopyTo(key,0);
			
			PartyPlayer plr = groupMember[mainPlayerGroupID][key[index]];
			if(plr.status != GroupMemberFlags.MEMBER_STATUS_OFFLINE)
				ret = plr;
		}
		return ret;
	}
	
	public void  updatePlayersStats ()
	{
		if(groupMember[mainPlayerGroupID].Count > 1)
		{
			for(byte index = 0; index < RAID_GROUP_COUNT; index++)
			{
				foreach(KeyValuePair<ulong, PartyPlayer> member in groupMember[index])
				{
					SendRequestPartyMemberStats(member.Key);
				}
			}
		}
	}
	
	public void  addPlayer ( PartyPlayer plr )
	{
		SendRequestPartyMemberStats(plr.guid);
		foreach(KeyValuePair<ulong, PartyPlayer> member in groupMember[(int)plr.groupID])
		{
			if(member.Key == plr.guid)
			{
				addPlayerToGroup(plr);
				return;
			}
		}
		addPlayerToGroup(plr);
	}
	
	public void  updatePlayer ( PartyPlayer plr )
	{	
		addPlayerToGroup(plr);
	}
	
	public void  remPlayer ( ulong guid )
	{
		for(byte index = 0; index < RAID_GROUP_COUNT; index++)
		{
			PartyPlayer plr;
			groupMember[index].TryGetValue(guid, out plr);
			
			if(plr != null)
			{
				destroyPlayerAura(plr);
				removePlayerFromGroup(plr);
				return;
			}
		}
	}
	
	public PartyPlayer GetPartyPlayer ( ulong guid )
	{
		PartyPlayer plr = null;
		for(byte index = 0; index < RAID_GROUP_COUNT; index++)
		{
			groupMember[index].TryGetValue(guid, out plr);
			if(plr != null)
			{
				break;
			}
		}
		return plr;
	}
	
	public void  clearGroupMember ()
	{
		for(byte index = 0; index < RAID_GROUP_COUNT; index++)
		{
			groupMember[index].Clear();
		}
	}
	
	public void  HideSpellIcons ( bool val )
	{
		doNotShowSpellIcons = val;
		if(doNotShowSpellIcons)
		{
			foreach(KeyValuePair<ulong, PartyPlayer> member in groupMember[mainPlayerGroupID])
			{
				destroyPlayerAura(member.Value);
			}
		}
	}
	
	public void  show ()
	{
		if(groupMember[mainPlayerGroupID].Count > 1)
		{
			Dictionary<ulong, PartyPlayer> mainPlayerGroup = new Dictionary<ulong, PartyPlayer>(groupMember[mainPlayerGroupID]);
			byte positionInGroup = 0;
			foreach(KeyValuePair<ulong, PartyPlayer> member in mainPlayerGroup)
			{
				if(member.Key != mainPlayer.guid)
				{
					DoMyWindow(member.Value, positionInGroup);
					positionInGroup++;
				}
			}
		}
	}
	
	public int getPlayersCount ()
	{
		return groupMember[mainPlayerGroupID].Count;
	}
	
	public void  addMainPlayerToGroup ()
	{
		PartyPlayer plr = new PartyPlayer();
		
		plr.guid = mainPlayer.guid;
		plr.status = GroupMemberFlags.MEMBER_STATUS_ONLINE;
		plr.level = mainPlayer.GetLevel();
		plr.playerClass = mainPlayer.playerClass;
		plr.name = mainPlayer.Name;
		plr.groupID = groupID;
		plr.flags = groupFlags;
		
		addPlayerToGroup(plr);
		setMainPlayerGroup(plr.groupID);
	}
	
#region PRIVATE FUNCTIONS
	private void  addPlayerToGroup ( PartyPlayer plr )
	{
		groupMember[(int)plr.groupID][plr.guid] = plr;
	}
	
	private void  removePlayerFromGroup ( PartyPlayer plr )
	{
		groupMember[(int)plr.groupID].Remove(plr.guid);
	}
	
	private void  destroyPlayerAura ( PartyPlayer plr )
	{
		foreach(UISprite aura in plr.auraBuff)
		{
			SinglePictureUtils.DestroySingleUISprite(aura);
		}
	}
	
	private UISprite addIconSprite ( string iconName , float xPos , float yPos , int depth )
	{
		UISprite sprite;
		UIToolkit tempUIToolkit;
		tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+iconName);
		sprite = tempUIToolkit.addSprite(iconName, xPos, yPos, depth);
		//		if(sprite == null)
		//		{
		//			sprite = UIPrime31.myToolkit7.addSprite(iconName, xPos, yPos, depth);
		//		}
		if(sprite == null)
		{
			if(tempUIToolkit)
			{
				GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
			}
			tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/noicon.png");
			sprite = tempUIToolkit.addSprite("noicon.png", xPos, yPos, depth);
		}
		sprite.setSize(buffDim, buffDim);
		
		return sprite;
	}
	
	public string getPlayerName ( ulong guid )
	{
		string ret;
		if (guid==MainPlayer.GUID)
		{
			ret = "You";
		}
		else
		{
			PartyPlayer plr = GetPartyPlayer(guid);
			if (string.IsNullOrEmpty (plr.name))
			{
				ret = "Other";
			}
			else
			{
				ret = plr.name;
			}
		}
		return ret;	
	}
	
	private void  DoMyWindow ( PartyPlayer plr , byte windowID )
	{
		Player currentPlayer = WorldSession.GetRealChar(plr.guid) as Player;
		if(!ButtonOLD.menuState)
		{
			destroyPlayerAura(plr);
			
			if(currentPlayer && !doNotShowSpellIcons)
			{
				int amountOfAuras = (currentPlayer.auraManager.auras.Count > 32) ? 32 : currentPlayer.auraManager.auras.Count;
				byte curBuff = 0;
				for(int index = 0; index < amountOfAuras; index++)
				{
					SpellEntry sp = dbs.sSpells.GetRecord((currentPlayer.auraManager.auras[index] as Aura).spellID);
					if(TraitsAura.traitList.Contains(sp.ID) || (sp.ID == 0))
					{
						continue;
					}
					
					SpellIconEntry icon  = dbs.sSpellIcons.GetRecord(sp.SpellIconID, false);
					string spellImage = icon.Icon;
					if (string.IsNullOrEmpty (spellImage))
					{
						spellImage = "noicon.png";
					}
					
					spellImage = spellImage.Substring(0, spellImage.LastIndexOf('.'));
					Texture texture = Resources.Load<Texture>("Icons/" + spellImage);
					Rect auraRect = new Rect((partyRect.x + buffDim*curBuff),
					         (partyRect.y + 2 + partyRect.height+(buffDim+partyRect.height)*windowID),
					         buffDim,
					         buffDim);
					GUI.DrawTexture(auraRect, texture);
					
					//					plr.auraBuff[curBuff] = addIconSprite(spellImage, (partyRect.x + buffDim*curBuff), (partyRect.y + 2 + partyRect.height+(buffDim+partyRect.height)*windowID), 1);
					//					if(Button.menuState)
					//						plr.auraBuff[curBuff].hidden = true;
					//				
					curBuff++;
				}
			}
			updatePlayer(plr);
		}
		
		
		GUI.DrawTexture( new Rect(partyRect.x, partyRect.y+2+ (buffDim+partyRect.height)*windowID, partyRect.width , partyRect.height), partyFrame);
		GUI.DrawTexture( new Rect(hpRect.x, hpRect.y+2+(buffDim+partyRect.height)*windowID, hpRect.width , hpRect.height), backGroundBar);
		GUI.DrawTexture( new Rect(energyRect.x, energyRect.y+2+(buffDim+partyRect.height)*windowID, energyRect.width , energyRect.height), backGroundBar);
		if(plr.status == GroupMemberFlags.MEMBER_STATUS_ONLINE)
		{
			GUI.DrawTexture( new Rect(hpRect.x, hpRect.y+2+(buffDim+partyRect.height)*windowID, hpRect.width * (plr.HP/(plr.maxHP+0.001f)), hpRect.height) , hpBar);
			GUI.DrawTexture( new Rect(energyRect.x, energyRect.y+2+(buffDim+partyRect.height)*windowID, energyRect.width * (plr.MP/(plr.maxMP+0.001f)), energyRect.height), energyBar);
		}
		
		if(leaderGUID == plr.guid)
		{
			GUI.DrawTexture( new Rect(leaderRect.x, leaderRect.y+2+(buffDim+partyRect.height)*windowID, leaderRect.width, leaderRect.height), leaderMark);
		}
		
		DrawOutline( new Rect(namePosition.x, namePosition.y + 2 +(buffDim+partyRect.height)*windowID,150,20), plr.name, GUI.color, GetColorClass(plr.playerClass), Color.white);
		
		if(GUI.Button( new Rect(partyRect.x, partyRect.y+2+(buffDim+partyRect.height)*windowID, partyRect.width , partyRect.height), "", UID.skin.customStyles[13]) )
		{
			if(PlayerPrefs.GetInt(mainPlayer.name+" partyPortraitButton") == 1)
			{
				return;
			}
			if(currentPlayer != null)
			{
				mainPlayer.sendTarget(currentPlayer);	
				if(PlayerPrefs.GetInt(mainPlayer.name+" partyPortraitButton") == 2)
				{
					mainPlayer.actionBarManager.DoAction(61, currentPlayer as BaseObject);
				}
			}
		}
	}
	
	private Color GetColorClass ( Classes temp )
	{
		Color returnValue = Color.white;
		switch(temp)
		{
		case Classes.CLASS_WARRIOR:		// 1 - fighter
			returnValue = Color.red;
			break;
		case Classes.CLASS_PALADIN:		// 2 - templar
			returnValue = Color.blue;
			break;
		case Classes.CLASS_HUNTER:		// 3 - ranger
			returnValue = Color.white;
			break;
		case Classes.CLASS_ROGUE:		// 4 - rogue
			returnValue = Color.yellow;
			break;
		case Classes.CLASS_PRIEST:		// 5 - confessor
			returnValue = Color.green;
			break;
		case Classes.CLASS_MAGE:		// 8 - mage
			returnValue = Color.magenta;
			break;
		case Classes.CLASS_WARLOCK:		// 9 - necromancer
			returnValue = Color.cyan;
			break;
		}
		return returnValue;
	}
	
	void  DrawOutline ( Rect position , string text , Color guiColor, Color outColor, Color inColor)
	{
		Color backUpColor= guiColor;
		
		GUI.color = outColor;
		position.x--;
		GUI.Label(position, text);
		position.x +=2;
		GUI.Label(position, text);
		position.x--;
		position.y--;
		GUI.Label(position, text);
		position.y +=2;
		GUI.Label(position, text);
		position.y--;
		
		GUI.color = inColor;
		GUI.Label(position, text);
		
		GUI.color = backUpColor;
	}
#endregion
	
//	bool isLeader ()
//	{
//		if (leaderGUID==MainPlayer.GUID)
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
	
	public string getPlayerName ( byte group, byte position )
	{	
		string ret = "Empty";
		
		if(groupMember[group].Count == 0)
		{
			return ret;
		}
		
		ulong[] key = new ulong[groupMember[group].Count];
		groupMember[group].Keys.CopyTo(key,0);
		
		if(position < groupMember[group].Count)
		{	
			ret = groupMember[group][key[position]].name;
		}
		
		return ret;
	}

	public PartyPlayer getPlayerFromGroup ( byte group , byte position )
	{		
		if(groupMember[group].Count != 0 && position < groupMember[group].Count)
		{	
			ulong[] key = new ulong[groupMember[group].Count];
			groupMember[group].Keys.CopyTo(key,0);
			return groupMember[group][key[position]];
		}
		else
		{
			return null;
		}
	}
	
//	public string getPlayerLvl ( byte group , byte position )
//	{	
//		string ret = "00";
//		
//		if(groupMember[group].Count == 0)
//		{
//			return ret;
//		}
//		
//		ulong[] key = new ulong[groupMember[group].Count];
//		groupMember[group].Keys.CopyTo(key,0);
//		
//		if(position < groupMember[group].Count)
//		{	
//			ret = " " + groupMember[group][key[position]].level.ToString();
//		}
//		
//		return ret;
//	}
	
	public void  setNewLeader ( ulong playerGuid )
	{
		if(WorldSession.player.guid != leaderGUID)
			return;
		
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_GROUP_SET_LEADER, 8);
		pkt.Append(ByteBuffer.Reverse(playerGuid));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  setNewLootMaster ( ulong playerGuid )
	{
		if(WorldSession.player.guid != leaderGUID)
			return;
		
		looterGUID = playerGuid;
		setLootType();
	}
	
	public void  setNewPlayerFlag ( ulong playerGuid , byte flag )
	{
		if(WorldSession.player.guid != leaderGUID)
			return;
		
		PartyPlayer player = GetPartyPlayer(playerGuid);
		if(player.flags == flag)
			flag = 0;
		
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_GROUP_ASSISTANT_LEADER, 9);
		pkt.Append(ByteBuffer.Reverse(playerGuid));
		pkt.Append(flag);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  setReadyCheck ( ulong playerGuid , byte flag )
	{
		PartyPlayer player = GetPartyPlayer(playerGuid);
		player.checkFlag = flag;
	}
	
	public void  kickPlayer ( string playerName )
	{
		if(WorldSession.player.guid != leaderGUID || RaidWindowManager.selectedPlayer.flags/2 == 1)
			return;
		
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_GROUP_UNINVITE, 8);
		pkt.Append(playerName);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  getNewLeader ( string playerName )
	{
		for(byte index = 0; index < RAID_GROUP_COUNT; index++)
		{
			int count = groupMember[index].Count;
			ulong[] key = new ulong[count];
			groupMember[index].Keys.CopyTo(key,0);	
			for(int position = 0; position < count; position++)			
				if(groupMember[index][key[position]].name == playerName)
					leaderGUID = key[position];
		}
	}
	
	public void  swapPosition ( string playerName , byte group )
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_GROUP_CHANGE_SUB_GROUP, 9); 
		pkt.Append(playerName);
		pkt.Append(group);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  changeToRaid ()
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_GROUP_RAID_CONVERT);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  setLootType ()
	{
		uint ltMethod = (uint)lootMethod;
		uint ltThreshold = (uint)lootThreshold;
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_LOOT_METHOD, 16);
		pkt.Append(ByteBuffer.Reverse(ltMethod));
		pkt.Append(ByteBuffer.Reverse(looterGUID));
		pkt.Append(ByteBuffer.Reverse(ltThreshold));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  setRaidDifficult ()
	{
		uint rdDifficult = (uint)raidDifficulty;
		WorldPacket pkt = new WorldPacket(OpCodes.MSG_SET_RAID_DIFFICULTY, 4);
		pkt.Append(ByteBuffer.Reverse(rdDifficult));
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  prepareConfirmWindow ()
	{
		Rect frameRect = new Rect(Screen.width * 0.3f, Screen.height * 0.10f, Screen.width * 0.4f, Screen.height * 0.3f);
		frame = UIPrime31.myToolkit6.addSprite("back_frame.png", frameRect, -5);
		
		Vector2 textPosition = new Vector2(Screen.width * 0.5f, Screen.height * 0.18f);
		text = textStyle.addTextInstance("Prepare Confirm", textPosition.x, textPosition.y, 0.5f, -8 ,Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		
		ButtonInfo confirmBtnPosition = new ButtonInfo(Screen.width * 0.33f, Screen.height * 0.25f, Screen.width * 0.15f, Screen.height * 0.08f);
		confirmBtn = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_selected.png", confirmBtnPosition.positionX, confirmBtnPosition.positionY, -6);
		confirmBtn.setSize(confirmBtnPosition.sizeX, confirmBtnPosition.sizeY);
		confirmBtn.onTouchUpInside += confirmDeleagte;
		
		Vector2 confirmBtnTextPosition = new Vector2(Screen.width * 0.4f, Screen.height * 0.29f);
		confirmBtnText = textStyle.addTextInstance("Confirm", confirmBtnTextPosition.x, confirmBtnTextPosition.y, 0.45f, -8 ,Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		
		ButtonInfo declineBtnPosition = new ButtonInfo(Screen.width * 0.52f, Screen.height * 0.25f, Screen.width * 0.15f, Screen.height * 0.08f);
		declineBtn = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_selected.png", declineBtnPosition.positionX, declineBtnPosition.positionY, -6);
		declineBtn.setSize(declineBtnPosition.sizeX, declineBtnPosition.sizeY);
		declineBtn.onTouchUpInside += declineDelegate;
		
		Vector2 declineBtnTextPosition = new Vector2(Screen.width * 0.6f, Screen.height * 0.29f);
		declineBtnText = textStyle.addTextInstance("Decline", declineBtnTextPosition.x, declineBtnTextPosition.y, 0.45f, -8 ,Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
	}
	
	void  confirmDeleagte (UIButton sender)
	{
		byte state = 1;
		WorldPacket pkt = new WorldPacket(OpCodes.MSG_RAID_READY_CHECK, 1);
		pkt.Append(state);
		RealmSocket.outQueue.Add(pkt);
		
		closeWindow();
	}
	
	void  declineDelegate (UIButton sender)
	{
		byte state = 0;
		WorldPacket pkt = new WorldPacket(OpCodes.MSG_RAID_READY_CHECK, 1);
		pkt.Append(state);
		RealmSocket.outQueue.Add(pkt);
		
		closeWindow();
	}
	
	void  closeWindow ()
	{
		frame.destroy();
		confirmBtn.destroy();
		declineBtn.destroy();
		
		text.clear();
		confirmBtnText.clear();
		declineBtnText.clear();
	}
	
	void  SendRequestPartyMemberStats ( ulong memberGuid )
	{
		WorldSession.player.timeLeftToUpdateGroup = WorldSession.player.updateTime;
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_REQUEST_PARTY_MEMBER_STATS);
		pkt.Append(ByteBuffer.Reverse(memberGuid));
		RealmSocket.outQueue.Add(pkt);
	}
}