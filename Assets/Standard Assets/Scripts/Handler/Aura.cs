using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Modifier
{
	public AuraType auraName;
	public int amount;
	public int miscvalue;
	public int periodictime; 
}

public class Aura {

	public delegate void ApplyAuraEffect();

	public ApplyAuraEffect[] ApplyEffect;
	public byte auraSlot;
	public uint spellID;
	public byte auraLevel;
	public AuraFlags auraFlags;
	public byte auraCharges;
	public ulong casterGuid = 0;
	public uint auraMaxDuration;
	public float auraDuration;

	public Unit unit;
	public Modifier modifier;
	
	public Aura(uint spellId, Unit unit)
	{
		SpellEntry spell = dbs.sSpells.GetRecord(spellId);
		byte eff = 0;
		byte damage = 0;

		this.unit = unit;
		if(spell.ID == 0)
		{
			if(spellId == 8326)//gost aura. spell.dbs dosen't have it
			{
				this.spellID = 8326;
				SetModifier(AuraType.SPELL_AURA_GHOST, 0, 0, 0);
			}
			else
			{
				this.spellID = 0;
				SetModifier(AuraType.SPELL_AURA_NONE , 0, 0, 0);
			}
		}
		else
		{
			this.spellID = spellId;
			SetModifier(GetAuraType(spell.EffectApplyAuraName[eff]), damage, 
			            spell.EffectAmplitude[eff], spell.EffectMiscValue[eff]);
		}

		InitFunctions();
	}
	
	public AuraType GetAuraType(int _type)
	{
		AuraType returnValue = AuraType.SPELL_AURA_NONE;
		switch(_type)
		{
		case 4:
			returnValue = AuraType.SPELL_AURA_DUMMY;
			break; 
		case 5:
			returnValue = AuraType.SPELL_AURA_MOD_CONFUSE;
			break; 
		case 78:
			returnValue = AuraType.SPELL_AURA_MOUNTED;
			break; 
		case 95:
			returnValue = AuraType.SPELL_AURA_GHOST;
			break; 
		case 16:
			returnValue = AuraType.SPELL_AURA_MOD_STEALTH;
			break; 
		case 18:
			returnValue = AuraType.SPELL_AURA_MOD_INVISIBILITY;
			break;
		case 36:
			returnValue = AuraType.SPELL_AURA_MOD_SHAPESHIFT;
			break; 
		case 56:
			returnValue = AuraType.SPELL_AURA_TRANSFORM;
			break;
		}
		return returnValue;
	}
	
	void InitFunctions()
	{	
		ApplyEffect = new ApplyAuraEffect[(uint)AuraType.TOTAL_AURAS];
		for(uint i = 0 ; i < (uint)AuraType.TOTAL_AURAS; ++i)
		{
			ApplyEffect[i] = NoEffect;
		}

		ApplyEffect[(uint)AuraType.SPELL_AURA_NONE] = NoEffect;
		ApplyEffect[(uint)AuraType.SPELL_AURA_MOUNTED] = Mount;
		ApplyEffect[(uint)AuraType.SPELL_AURA_GHOST] = GhostMod;
		//ApplyEffect[(uint)AuraType.SPELL_AURA_MOD_STEALTH] = SetShapeShift;
		ApplyEffect[(uint)AuraType.SPELL_AURA_MOD_SHAPESHIFT] = SetShapeShift;
		ApplyEffect[(uint)AuraType.SPELL_AURA_MOD_CONFUSE] = SetShapeShift;
	}
	
	
	public void Apply()
	{
		(ApplyEffect[(uint)modifier.auraName])();
	} 
	
	public void SetModifier(AuraType ar, int dmg, int periodic, int misc)
	{
		modifier = new Modifier();
		modifier.auraName = ar;
		modifier.amount = dmg;
		modifier.miscvalue = misc;
		modifier.periodictime = periodic;
	}
	
	public int GetDuration()
	{
		int duration = 0;
		if(auraDuration > 59)
		{
			duration = Convert.ToInt32(auraDuration / 60);	//we display the remainging aura time in minutes
		}
		return duration;
	}
	
	public void Remove()
	{
		if(spellID == 8326)
		{
			Debug.Log("Remove ghost aura! ");
		}
		else
		{
			WorldPacket pkt = new WorldPacket(OpCodes.CMSG_CANCEL_AURA, 4);
			pkt.Add(spellID);
			RealmSocket.outQueue.Add(pkt);
		}
	}
	

	AuraFlags flag = AuraFlags.AFLAG_DURATION;

	public void Update()
	{
		if((auraFlags & flag) == 0)
		{
			return;
		}

		if(auraDuration < 0.0001f)
		{
			Remove();	
		}

		auraDuration -= Time.deltaTime;
	}

	void NoEffect()
	{
		//Debug.Log("Effect on dummy!");
	}	
	
	void GhostMod()
	{
//		if(spellID > 0)
//		{
//			if(casterGuid == unit.GUID)
//			{
//				unit.UpdateStats();
//				unit.m_deathState = DeathState.DEAD;
//				unit.interf.reviveWindow.hideGhost = false;			
//			}
//		}
	}
	
	void Mount()
	{
		if((unit as Player) != null)
		{
			if(spellID == 0)
			{
				(unit as Player).mountManager.Unmount();
//				(unit as Player).charMount = false;
			}
			else
			{
				(unit as Player).mountManager.Mount("" + spellID);
//				(unit as Player).charMount = true;
			}
		}
	}
	
	void SetShapeShift()
	{
		if(spellID == 0)
		{
			if(unit.stealth)
			{
				unit.stealth = false;
				unit.TransformParent.BroadcastMessage("DestroyStealth", SendMessageOptions.DontRequireReceiver);
			}

			if(unit.shapeshift)
			{
				unit.shapeshift = false;
				unit.TransformParent.BroadcastMessage("DestroyShapeShift", SendMessageOptions.DontRequireReceiver);
			}
		}
		else
		{
			GameObject go = null;
			if(spellID == 1784)
			{
				if(unit.stealth)
				{
					return;
				}

				unit.stealth = true;
				LoadAuraEffect("prefabs/SpellEffects/AuraEffects/1");
			}
			else if(spellID == 118 || spellID == 12824 || spellID == 12825 || spellID == 12826)
			{
				if(unit.shapeshift)
				{
					return;
				}

				unit.shapeshift = true;
				LoadAuraEffect("prefabs/SpellEffects/AuraEffects/2");
			}
		}
	}

	private void LoadAuraEffect(string path)
	{
		AssetBundleManager.SpellEffectCallBackStruct callBackStruct = new AssetBundleManager.SpellEffectCallBackStruct();
		callBackStruct.speed = 0;
		callBackStruct.delayTime = 0;
		callBackStruct.gender = Gender.GENDER_NONE;
		callBackStruct.effectNameId = -1;
		callBackStruct.linkedObject = null;
		callBackStruct.targetObject = null;
		callBackStruct.goalPosition = Vector3.zero;
		callBackStruct.resultObject = null;
		callBackStruct.assetPath = path;
		WorldSession.GetBundleLoader().LoadSpellEffectBundle(ApplyAuraEffectBundle, callBackStruct);
	}

	private void ApplyAuraEffectBundle(AssetBundleManager.SpellEffectCallBackStruct callBackStruct)
	{
		string bundleName = callBackStruct.assetPath;
		string assetBundleName = string.Format ("Resources/{0}_prefab.unity3d", bundleName);
		assetBundleName = assetBundleName.ToLower();
		UnityEngine.Object bundle = null;
		AssetBundleManager.instance.currentLoadedBundles.TryGetValue(assetBundleName, out bundle);
		UnityEngine.Object resourceToInstantiate = null;
		if(bundle == null)
		{
			#if UNITY_EDITOR
			Debug.LogError(string.Format ("cannot load {0}", assetBundleName));
			#endif
		}
		else
		{
			resourceToInstantiate = bundle as UnityEngine.Object;
		}
		if(resourceToInstantiate == null)
		{
			resourceToInstantiate = Resources.Load<UnityEngine.Object>(bundleName);
		}
		
		if(resourceToInstantiate != null)
		{
			GameObject go = UnityEngine.Object.Instantiate (resourceToInstantiate, unit.TransformParent.position, unit.TransformParent.rotation) as GameObject;
			if(go != null)
			{
				go.transform.parent = unit.TransformParent;
				go.BroadcastMessage("SetAnimHolder",
				                    unit.TransformParent.Find(unit.name).GetComponent<Animation>(),
				                    SendMessageOptions.DontRequireReceiver);
				go.BroadcastMessage("PlayMirroredMovementAnimation", 
				                    (unit.aHandler as PlayerAnimationHandler).currentMovementAnimation, 
				                    SendMessageOptions.DontRequireReceiver);				
			}
		}
	}
}