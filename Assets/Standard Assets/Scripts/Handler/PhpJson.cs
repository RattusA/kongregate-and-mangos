﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class PhpJson {

	public static string[] result;
	public static int noOfBag;

	private static string _result;
	
	public static IEnumerator phpj(string url, string[] paramName, string[] param)
	{	
		WWWForm postData = new WWWForm();
		for(int j = 0; j < paramName.Length; j++)
		{
			postData.AddField(paramName[j], param[j]);
		}
		
		WWW download = new WWW(url, postData);
		while(!download.isDone)
		{
			yield return download;
		}

		while(!string.IsNullOrEmpty(download.error) && download.error.ToString().Contains("Resolving host timed out"))
		{
			download = new WWW(url,postData);
			while(!download.isDone)
			{
				yield return download;
			}
		}

		Debug.Log("phpj: Reqest finished");
		string wwwData; 
		if(!string.IsNullOrEmpty(download.error))
		{
			wwwData = "Error! Could not connect.\n"+download.error.ToString();
			Debug.Log("phpj: "+wwwData);
			yield return null;;
		}
		else
		{
			wwwData = download.text;
		}
		result = Regex.Split(wwwData,":");
	}   
	
	
//	public static void phpj(string url, string[] paramName, string[] param, Action callBackFunction)
//	{
//		WWWForm postData = new WWWForm();
//		for(int j = 0; j < paramName.Length; j++)
//		{
//			postData.AddField(paramName[j], param[j]);
//		}
//		
//		WWW download = new WWW(url, postData);
//		while(!download.isDone)
//		{
//			yield return download;
//		}
//
//		while(download.error && download.error.ToString().Contains("Resolving host timed out"))
//		{
//			download = new WWW(url, postData);
//			while(!download.isDone)
//			{
//				yield return download;
//			}
//		}
//		
//		string wwwData; 
//		if(download.error)
//		{
//			wwwData = "Error! Could not connect.";
//			return;
//		}
//		else
//		{
//			wwwData = download.text;
//		}
//		
//		result = Regex.Split(wwwData,":");
//		callBackFunction();
//	}
	
	public static List<int> unlockSlots()
	{
		List<int> resp = new List<int>();
		resp.Add(1);
		int i = 0;
		string str;
		
		if(result == null || result.Length == 0)
		{
			return resp;
		}
		
		if(result[result.Length - 2].Contains("slot_list"))
		{
			str = result[result.Length - 1];
			while(str[i].Equals("]"))
			{
				if(str[i] < 48 || str[i] > 57)
				{
					i++;
				}
				else
				{
					resp.Add(int.Parse(str.Substring(i, 1)));
					i++;
				}
			}
		}
		noOfBag = resp.Count;	
		return resp;
	}

	public static bool isPortalLocked()
	{		
		if(result.Length < 2)
		{
			return true;
		}

		if(result[result.Length - 2].Contains("is_unlocked")) //success : 1
		{
			if(result[result.Length - 1].Contains("false"))	
			{
				return true;
			}
			
			if(result[result.Length - 1].Contains("true"))	
			{
				return false;
			}		
		}
		return true;//success : 0
	}

	public static IEnumerator phpjversion(string url)
	{
		WWW download = new WWW(url);
		while(!download.isDone)
		{
			yield return download;
		}

		while(!string.IsNullOrEmpty(download.error) && download.error.ToString().Contains("Resolving host timed out"))
		{
			download = new WWW(url);
			while(!download.isDone)
			{
				yield return download;
			}
		}

		string wwwData; 
		if(!string.IsNullOrEmpty(download.error))
		{
			wwwData = "Error! Could not connect.";
			yield return null;
		}
		else
		{
			wwwData = download.text;
		}
		_result = wwwData;
	}

	public static string showVersion()
	{
		return _result;
	}
}
