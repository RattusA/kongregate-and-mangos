using UnityEngine;

public enum TutorialStages
{
	TUTORIAL_JOYSTICK	= 0,
	TUTORIAL_OPEN_CHAT	= 1,
	TUTORIAL_CLOSE_CHAT= 2,
//	uint TUTORIAL_JUMP		= 3,
	TUTORIAL_SELECT_DUMMY = 3,
	TUTORIAL_ATTACK_DUMMY = 4,
	TUTORIAL_ENDED		= 5
}

public class TutorialHandler : MonoBehaviour
{
//	public class offsets
//	{
//		public float x;
//		public float y;
//		public float sx;
//		public float sy;
//		public float bx;
//		public float by;
//		public float skx;
//		public float sky;
//	}
	
//	public class yn
//	{
//		public float x;
//		public float y;
//		public float xscale;
//		public float yscale;
//	}
	
	public class TipTexts
	{
		public string title;
		public string content;
	}

//	Texture2D tipTexture;
//	Texture2D quitTexture;
	
//	string[] textures = new string[14];
//	offsets[] offs = new offsets[17];
//	yn[] yesNoButtons = new yn[2];
	
	//---------------
	private static TutorialHandler instance;
	[SerializeField]
	Texture2D bkgTexture;
	Rect bkgTextureRect;
	public bool  isTutorialNoEnded = true;
	bool  showHint;
	bool  showAbortTutorial;
	public TutorialStages tutorialStep = TutorialStages.TUTORIAL_JOYSTICK;
	MainPlayer mainPlayer = WorldSession.player;
	TipTexts[] tipText = new TipTexts[(int)TutorialStages.TUTORIAL_ENDED+1];
	float buttonHeight;
	float textFontSize;
	GUIStyle titleStyle;
	GUIStyle contentStyle;
	Rect titleRect;
	Rect contentRect;

//	[SerializeField]
//	string[] texGlowingStick = new string[16];
	[SerializeField]
	Texture2D[] animJoy = new Texture2D[16];
	float timer = 0f;
	float timeBetweenImages = 0.17f;
	int _currentAnimatedImage = 0;
	
	string accountName = "";

	public static TutorialHandler Instance
	{
		get
		{
			return instance;
		}
	}
	
	void  Awake ()
	{
		instance = this;
//		bkgTexture = Resources.Load<Texture2D>("InGameTips/tutorial_background");
		InitBackgroundTextureRect();
//		PrepareGlowingStickTextures();
		accountName = PlayerPrefs.GetString("AccountStorage");
		
		//	accountName = accountName.Replace("@", "");
		//	accountName = accountName.Replace("!", "");
		//	accountName = accountName.Replace(".", "");
		//	accountName = accountName.Replace(",", "");
		//	accountName = accountName.Replace("?", "");
		
		//	tutorialStep = PlayerPrefs.GetInt(accountName+"_TutorialStage", 0);
		showHint = true;
		InitText();
		
		titleStyle = new GUIStyle();
		titleStyle.fontSize = (int)textFontSize;
		titleStyle.richText = true;
		titleStyle.alignment = TextAnchor.MiddleCenter;
		
		contentStyle = new GUIStyle();
		contentStyle.fontSize = (int)(textFontSize*0.8f);
		contentStyle.richText = true;
		contentStyle.alignment = TextAnchor.UpperCenter;
		
		titleRect = new Rect(bkgTextureRect.x, bkgTextureRect.y + bkgTextureRect.width*0.037f, bkgTextureRect.width, textFontSize*1.3f);
		contentRect = new Rect(bkgTextureRect.x, bkgTextureRect.y + bkgTextureRect.width*0.037f+textFontSize*1.5f, bkgTextureRect.width, bkgTextureRect.height-bkgTextureRect.width*0.037f-textFontSize*1.5f);
	}
	
	public void  setTutorialStep ( TutorialStages value )
	{
		tutorialStep = value;	
	}
	
	void  InitBackgroundTextureRect ()
	{
		float bkgTextureRatio = bkgTexture.width / bkgTexture.height;
		float defaulHeightRatio = 1024 * 0.9f / bkgTexture.width * bkgTexture.height / 768;
		float bkgRectHeight = Screen.height * defaulHeightRatio;
		float bkgRectWidth = bkgRectHeight * bkgTextureRatio;
		float bkgRectX = (Screen.width - bkgRectWidth)/2;
		float bkgRectY = (Screen.height - bkgRectHeight)/2;
		bkgTextureRect = new Rect(bkgRectX, bkgRectY, bkgRectWidth, bkgRectHeight);
		buttonHeight = bkgTextureRect.width*0.08f;
		textFontSize = bkgTextureRect.width*0.07f;
	}
	
//	void  PrepareGlowingStickTextures ()
//	{
//		//textures for the glowing joystick
//		texGlowingStick[0] = "InGameTips/GlowingJoystick/joystick1";
//		texGlowingStick[1] = "InGameTips/GlowingJoystick/joystick2";
//		texGlowingStick[2] = "InGameTips/GlowingJoystick/joystick3";
//		texGlowingStick[3] = "InGameTips/GlowingJoystick/joystick4";
//		texGlowingStick[4] = "InGameTips/GlowingJoystick/joystick5";
//		texGlowingStick[5] = "InGameTips/GlowingJoystick/joystick6";
//		texGlowingStick[6] = "InGameTips/GlowingJoystick/joystick7";	
//		texGlowingStick[7] = "InGameTips/GlowingJoystick/joystick8";
//		texGlowingStick[8] = "InGameTips/GlowingJoystick/joystick9";
//		texGlowingStick[9] = "InGameTips/GlowingJoystick/joystick10";
//		texGlowingStick[10] = "InGameTips/GlowingJoystick/joystick11";
//		texGlowingStick[11] = "InGameTips/GlowingJoystick/joystick12";
//		texGlowingStick[12] = "InGameTips/GlowingJoystick/joystick13";
//		texGlowingStick[13] = "InGameTips/GlowingJoystick/joystick14";
//		texGlowingStick[14] = "InGameTips/GlowingJoystick/joystick15";
//		texGlowingStick[15] = "InGameTips/GlowingJoystick/joystick16";
//		loadGlowJoy();
//	}
	
	void  InitText ()
	{
		int stage = (int)TutorialStages.TUTORIAL_JOYSTICK;
		tipText[stage] = new TipTexts();
		tipText[stage].title = "MOVE";
		tipText[stage].content = "Use the virtual joystick\nto move your character";
		stage = (int)TutorialStages.TUTORIAL_SELECT_DUMMY;
		tipText[stage] = new TipTexts();
		tipText[stage].title = "PICK TARGET";
		tipText[stage].content = "Tap the Skeleton\n(Battle Dummy)";
		stage = (int)TutorialStages.TUTORIAL_ATTACK_DUMMY;
		tipText[stage] = new TipTexts();
		tipText[stage].title = "REGULAR ATTACK";
		tipText[stage].content = "Tap the weapon icon\nto attack";
		stage = (int)TutorialStages.TUTORIAL_OPEN_CHAT;
		tipText[stage] = new TipTexts();
		tipText[stage].title = "OPEN CHAT CHANNEL";
		tipText[stage].content = "Touch the chat button to\nuse a chat channel";
		stage = (int)TutorialStages.TUTORIAL_CLOSE_CHAT;
		tipText[stage] = new TipTexts();
		tipText[stage].title = "CLOSE CHAT CHANNEL";
		tipText[stage].content = "Touch the chat button again to\n close a chat channel";
		stage = (int)TutorialStages.TUTORIAL_ENDED;
		tipText[stage] = new TipTexts();
		tipText[stage].title = "END TUTORIAL";
		tipText[stage].content = "If you end tutorial you'll never\nbe able to do it again\n\nDo you want to end tutorial?";
	}
	
//	void  loadGlowJoy ()
//	{
//		for (int i = 0; i<16; i++)
//		{
//			animJoy[i] = Resources.Load<Texture2D>(texGlowingStick[i]);
//		}
//	}
	
//	void  deleteGlowJoy ()
//	{
//		for (int i = 0; i<16; i++)
//		{
//			animJoy[i] = null;
//		}
//	}
	
	void  advanceTutorial ()
	{
		//	if (tutorialStep == 0)
		//	{
		//		tutorialStep = 2;
		//		deleteGlowJoy();
		//	}
		//	else
		//	{
		//		while(stepsDone[tutorialStep])
		//		{
		//			tutorialStep++;
		//		}
		//	}
		
	}
	
	// Send stage to server (not used yet)
	void  SendTutorialFlag ( TutorialStages flag )
	{
		WorldPacket packet;		
		packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_TUTORIAL_FLAG);
		packet.Append(ByteBuffer.Reverse((int)flag));
		RealmSocket.outQueue.Add(packet);		
		Debug.Log("Tutorial flag sent to server..");
	}
	
	// Repeat tutorial
	void  ResetTutorialFlag ()
	{
		//	tutorialStep = 0;
		//	PlayerPrefs.SetInt(accountName+"_TutorialStage", tutorialStep);
		WorldPacket packet;
		packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_TUTORIAL_RESET);		
		RealmSocket.outQueue.Add(packet);
		Debug.Log("Tutorial flag reset.. ");
	}
	
	// Abbort tutorial
	void  ClearTutorialFlag ()
	{
		isTutorialNoEnded = false;
		//	PlayerPrefs.SetInt(accountName+"_TutorialStage", 0xFFFFFFFF);
		WorldPacket packet;
		packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_TUTORIAL_CLEAR);		
		RealmSocket.outQueue.Add(packet);		
		Debug.Log("Tutorial flag has been cleared.");
	}
	
	public void  CompleteTutorialStage ()
	{
		if(tutorialStep <= TutorialStages.TUTORIAL_ENDED)
		{
			if(tutorialStep == TutorialStages.TUTORIAL_ATTACK_DUMMY)
			{
				mainPlayer.actionBarPanel.bottomPanelFlag = true;
			}
			SendTutorialFlag(tutorialStep);
			tutorialStep++;
			//		PlayerPrefs.SetInt(accountName+"_TutorialStage", tutorialStep);
			showHint = true;
			Debug.Log("Tutorial stage completed.");
		}
	}
	
	void  _test ()
	{
		if (showHint)
		{
			//this condition is wrong
			GUI.depth = -1;
			if(	!((tutorialStep == TutorialStages.TUTORIAL_ATTACK_DUMMY) || (tutorialStep == TutorialStages.TUTORIAL_SELECT_DUMMY)) )
			{
				GUI.DrawTexture(bkgTextureRect, bkgTexture, ScaleMode.StretchToFill);
			}
			
			GUI.Label(titleRect, "<color=" + mainPlayer.mainTextColor + ">" + tipText[(int)tutorialStep].title + "</color>", titleStyle);
			
			GUI.Label(contentRect, "<color=" + mainPlayer.mainTextColor + ">" + tipText[(int)tutorialStep].content + "</color>", contentStyle);
			
			Rect skipButtonRect = new Rect(bkgTextureRect.x + bkgTextureRect.width*0.0325f, bkgTextureRect.y + bkgTextureRect.height - (bkgTextureRect.width*0.0325f + buttonHeight), bkgTextureRect.width*0.275f, buttonHeight);
			if (GUI.Button(skipButtonRect, "", UID.skin.GetStyle("TutorialSkipButton")))
			{
				showAbortTutorial = true;		
			}
			Rect closeTipButtonRect = new Rect(bkgTextureRect.x + bkgTextureRect.width*(1-0.0225f-0.06f), bkgTextureRect.y + bkgTextureRect.width*0.0325f, buttonHeight, buttonHeight);
			if (GUI.Button(closeTipButtonRect, "", UID.skin.GetStyle("TutorialCloseButton")))
			{
				showHint = false;		
			}
			
			GUI.depth = 0;
		}
		if (tutorialStep == TutorialStages.TUTORIAL_JOYSTICK)
		{				
			GUI.DrawTexture( new Rect(Screen.width*0.06f, Screen.height - Screen.width*0.18f, Screen.width*0.12f, Screen.width*0.12f), animJoy[_currentAnimatedImage], ScaleMode.StretchToFill);
		}		
		if (showAbortTutorial)
		{
			showHint = false;
			GUI.depth = -1;
			GUI.DrawTexture(bkgTextureRect, bkgTexture, ScaleMode.StretchToFill);
			
			GUI.Label(titleRect, "<color=" + mainPlayer.mainTextColor + ">" + tipText[(int)TutorialStages.TUTORIAL_ENDED].title + "</color>", titleStyle);
			
			GUI.Label(contentRect, "<color=" + mainPlayer.mainTextColor + ">" + tipText[(int)TutorialStages.TUTORIAL_ENDED].content + "</color>", contentStyle);
			
			Rect yesButtonRect = new Rect(bkgTextureRect.x + bkgTextureRect.width*(0.25f-0.26f/2), bkgTextureRect.y + bkgTextureRect.height - (bkgTextureRect.width*0.0325f + buttonHeight), bkgTextureRect.width*0.26f, buttonHeight);
			if (GUI.Button(yesButtonRect, "", UID.skin.GetStyle("TutorialSkipYesButton")))
			{					
				EndTutorial();
			}
			Rect noButtonRect = new Rect(bkgTextureRect.x + bkgTextureRect.width*(0.75f-0.26f/2), bkgTextureRect.y + bkgTextureRect.height - (bkgTextureRect.width*0.0325f + buttonHeight), bkgTextureRect.width*0.26f, buttonHeight);
			if (GUI.Button(noButtonRect, "", UID.skin.GetStyle("TutorialSkipNoButton")))
			{					
				showHint = true;
				showAbortTutorial = false;			
			}
			GUI.depth = 0;
		}
	}
	
	void OnGUI ()
	{	
		if(isTutorialNoEnded && 
		   (tutorialStep < TutorialStages.TUTORIAL_ENDED) && 
		   WorldSession.player && 
		   WorldSession.player.interf && 
		   !WorldSession.interfaceManager.hideInterface)
		{
			StageController();
		}
	}
	
	void  Update ()
	{
		if (isTutorialNoEnded && tutorialStep == TutorialStages.TUTORIAL_JOYSTICK)
		{
			timer += Time.deltaTime;	
			if(timer >= timeBetweenImages)
			{
				_currentAnimatedImage ++;
				timer = 0;
				if(_currentAnimatedImage >= 16)
				{
					_currentAnimatedImage = 0;
				}
			}
		}
	}
	
	void  StageController ()
	{
		switch(tutorialStep)
		{
		case TutorialStages.TUTORIAL_ATTACK_DUMMY:
			ShowAttackDummyTutorial();
			break;
		case TutorialStages.TUTORIAL_SELECT_DUMMY:
			ShowSelectDummyTutorial();
			break;
		case TutorialStages.TUTORIAL_CLOSE_CHAT:
			ShowCloseChatTutorial();
			break;
		case TutorialStages.TUTORIAL_OPEN_CHAT:
			ShowOpenChatTutorial();
			break;
		case TutorialStages.TUTORIAL_JOYSTICK:
			ShowJoystickTutorial();
			break;
		case TutorialStages.TUTORIAL_ENDED:
			EndTutorial();
			break;
		default:
			tutorialStep = TutorialStages.TUTORIAL_JOYSTICK;
			break;
		}
	}
	
	void  ShowJoystickTutorial ()
	{
//		MainPlayer player = WorldSession.player;
		//	player.interf.setHideInterface(true);
		//	player.interf.chatState = false;
		//	player.interf.HideBottomHotBar(true);
		//	player.spellManagerStatus = 32;
		_test();
	}
	
	void  ShowOpenChatTutorial ()
	{
		MainPlayer player = WorldSession.player;
		player.actionBarPanel.bottomPanelFlag = true;
		//	player.interf.setHideInterface(true);
		//	player.interf.chatState = false;
		//	player.spellManagerStatus = 2+32;
		_test();
	}
	
	void  ShowCloseChatTutorial ()
	{
		MainPlayer player = WorldSession.player;
		player.actionBarPanel.bottomPanelFlag = true;
		//	player.interf.setHideInterface(true);
		//	player.interf.chatState = true;
		//	player.spellManagerStatus = 2+32;
		_test();
	}
	
	void  ShowSelectDummyTutorial ()
	{
//		MainPlayer player = WorldSession.player;
		//	player.interf.setHideInterface(true);
		//	player.interf.chatState = false;
		//	player.interf.HideBottomHotBar(false);
		//	player.spellManagerStatus = 2+32;
		_test();
	}
	
	void  ShowAttackDummyTutorial ()
	{
//		MainPlayer player = WorldSession.player;
		//	player.interf.setHideInterface(true);
		//	player.interf.chatState = false;
		//	player.interf.HideBottomHotBar(false);
		//	player.spellManagerStatus = 2+32;
		mainPlayer.actionBarPanel.bottomPanelFlag = false;
		mainPlayer.actionBarPanel.rightBotFlag = false;
		mainPlayer.actionBarPanel.rightTopFlag = false;
		mainPlayer.actionBarPanel.rightExtFlag = false;
		_test();
	}
	
	void  EndTutorial ()
	{
//		MainPlayer player = WorldSession.player;
		//	player.interf.setHideInterface(false);
		//	player.interf.HideBottomHotBar(false);
		//	player.spellManagerStatus = 2+32;
		showAbortTutorial = false;
		tutorialStep = TutorialStages.TUTORIAL_ENDED;
		CompleteTutorialStage();
		ClearTutorialFlag();
	}
	
	public bool isNeedBlockMovement ()
	{
		bool  ret = false;
		if(isTutorialNoEnded && showAbortTutorial)
			ret = true;
		return ret;
	}
}
