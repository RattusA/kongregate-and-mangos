using UnityEngine;
using System;
using System.Collections.Generic;

public class CharacterCreation : AssambleCharacter
{
	public class Customization
	{
		public FACTION charFaction;
		public Races charRace;
		public Gender charGender;
		public Classes charClass;	
		//		byte charSkin;
		public int charFace;
		public int charHairStyle;
		public int charHairColor;
		public int charFacialHair;
		public int charSkinColor;
	}
	public SelectCharacterWindow loginInterface;
	
	public Customization newCust = null;
	//	bool  updateCharacter = false;
	int MAX_FACE_STYLES;
	int MAX_HAIR_STYLES;
	int MAX_HAIR_COLORS;
	int MAX_FACIALHAIR_STYLES;
	int MAX_SKIN_COLORS;	
	
	List<CustomFaceEntry> customFace = new List<CustomFaceEntry>();
	List<CustomHairStyleEntry> customHairStyle = new List<CustomHairStyleEntry>();
	List<CustomFacialHairEntry> customFacialHair = new List<CustomFacialHairEntry>();// this will also store tattoos
	List<CustomHairColorEntry> customHairColor = new List<CustomHairColorEntry>();
	List<CustomSkinColorEntry> customSkinColor = new List<CustomSkinColorEntry>();
	
	public CharacterCreation ():base()
	{
		newCust = new Customization();
		ResetPlayerEnum();
	}
	
	public CharacterCreation ( SelectCharacterWindow loginInterface ):base()
	{
		this.loginInterface = loginInterface;
		
		newCust = new Customization();
		ResetPlayerEnum();
	}
	
	public override void  Destroy ()
	{
		base.Destroy();
		ClearCustomizationArrays();	
		loginInterface = null;	
	}
	
	void  ClearCustomizationArrays ()
	{
		customFace.Clear();
		customFace.TrimExcess();
		customHairStyle.Clear();
		customHairStyle.TrimExcess();
		customFacialHair.Clear();
		customFacialHair.TrimExcess();
		customHairColor.Clear();
		customHairColor.TrimExcess();
		customSkinColor.Clear();
		customSkinColor.TrimExcess();
	}
	
	
	public void  InitMaxStylesAndColors ()
	{
		ClearCustomizationArrays();
		
		int mask = ((int)newCust.charRace<<12) + ((int)newCust.charGender<<10);//get the character mask using race and gender
		
		//Debug.Log("**************************  new mask: "+ mask);
		
		int i;
		for(i = 0; i<dbs.sCustomFace.Records; i++)
		{
			var aux= dbs.sCustomFace.GetRecordFromIndex(i);
			if((aux.ID - mask) < 256 && (aux.ID - mask) >= 0){
				customFace.Add(aux);
			}
		}
		
		//Debug.Log("************************ "+ dbs.sCustomHairStyle.Records+"  Hair styles ");
		for(i = 0; i<dbs.sCustomHairStyle.Records; i++)
		{
			var aux2= dbs.sCustomHairStyle.GetRecordFromIndex(i);
			if((aux2.ID - mask) < 256 && (aux2.ID - mask) >= 0){
				customHairStyle.Add(aux2);
				//Debug.Log("********************Found Hair Style: "+aux2.ID);
			}
		}		
		
		//Debug.Log("************************ "+ dbs.sCustomFacialHair.Records+" Facial Hair styles ");
		if(playerEnum.gender == Gender.GENDER_FEMALE)
		{
			var vec= customFace[0].Tattos.Split (";"[0]);//get the tattos of the first face mesh
			for(i=0; i<vec.Length; i++)
			{
				if(vec[i].Length  < 1)
					continue;
				Debug.Log(vec[i]);
				
				try
				{
					var aux10= dbs.sCustomFacialHair.GetRecord(System.Int32.Parse(vec[i] as string));				
					if((aux10.ID - mask) < 256 && (aux10.ID - mask) >= 0)
					{							
						customFacialHair.Add(aux10);
					}
				}
				catch( Exception e )
				{
					#if UNITY_EDITOR
					Debug.LogWarning("Unknown facial hair id = "+vec[i]);
					#endif	
				}
			}
		}
		else{			
			for(i = 0; i<dbs.sCustomFacialHair.Records; i++)
			{
				var aux3= dbs.sCustomFacialHair.GetRecordFromIndex(i);
				if((aux3.ID - mask) < 256 && (aux3.ID - mask) >= 0){
					
					customFacialHair.Add(aux3);
				}
			}
		}
		
		//Debug.Log("************************ "+ dbs.sCustomHairColor.Records+"  Hair colors ");
		for(i = 0; i<dbs.sCustomHairColor.Records; i++)
		{
			var aux4= dbs.sCustomHairColor.GetRecordFromIndex(i);
			if((aux4.ID - mask) < 256 && (aux4.ID - mask) >= 0){
				customHairColor.Add(aux4);
				//Debug.Log("********************Found Hair Colors: "+aux4.ID);
			}
		}		
		
		//Debug.Log("************************ "+ dbs.sCustomHairColor.Records+"  Skin colors ");
		for(i = 0; i<dbs.sCustomSkinColor.Records; i++)
		{
			var aux5= dbs.sCustomSkinColor.GetRecordFromIndex(i);
			if((aux5.ID - mask) < 256 && (aux5.ID - mask) >= 0){
				customSkinColor.Add(aux5);
				//Debug.Log("********************Found Skin Colors: "+aux5.ID);
			}
		}			
		
		MAX_FACE_STYLES = customFace.Count;
		MAX_HAIR_STYLES = customHairStyle.Count;
		MAX_HAIR_COLORS = customHairColor.Count;
		MAX_FACIALHAIR_STYLES = customFacialHair.Count == 0 ? 0 : customFacialHair.Count+1; 
		MAX_SKIN_COLORS = customSkinColor.Count;
		
		Debug.Log("************************* Found "+MAX_FACE_STYLES+" Faces, "+ MAX_HAIR_STYLES+" Hair styles, "+ MAX_HAIR_COLORS+" Hair colors, "+ MAX_FACIALHAIR_STYLES+" Facial har styles, "+ MAX_SKIN_COLORS+ " Skin colors");
	}
	
	
	public void  ResetPlayerEnum ()
	{
		newCust.charRace 	= playerEnum.race 	= Races.RACE_ORC;
		newCust.charGender	= playerEnum.gender	= Gender.GENDER_MALE;
		newCust.charClass	= playerEnum.classType	= Classes.CLASS_PRIEST;
		
		newCust.charSkinColor = playerEnum.skinColor = 0;
		newCust.charFace = playerEnum.face = 0;
		newCust.charHairStyle = playerEnum.hairStyle = 0;
		newCust.charHairColor = playerEnum.hairColor = 0;
		newCust.charFacialHair = playerEnum.facialHair = 0;	
		newCust.charSkinColor = playerEnum.skinColor = 0;
	}
	
	public void  UpdateCharacter ()
	{
		
		if(playerEnum.skinColor != newCust.charSkinColor){
			if(newCust.charSkinColor >= MAX_SKIN_COLORS){
				newCust.charSkinColor = 0;
			}			
			playerEnum.skinColor = newCust.charSkinColor;
			UpdateSkinColor();
			if(playerEnum.face == newCust.charFace)
			{
				UpdateFace();
			}
		}
		
		if(playerEnum.race != newCust.charRace)
		{
			//playerEnum.faction  =	newCust.charFaction;
			playerEnum.race = newCust.charRace;
			Assemble();			 
		}
		
		if(playerEnum.gender != newCust.charGender)
		{
			playerEnum.gender = newCust.charGender;			
			Assemble();
		}
		
		if(playerEnum.classType != newCust.charClass){
			playerEnum.classType = newCust.charClass;
			//UpdateItems();
		}
		
		if(playerEnum.face != newCust.charFace){
			if(newCust.charFace >= MAX_FACE_STYLES){
				newCust.charFace = 0;
			}
			
			playerEnum.face = newCust.charFace;
			UpdateFace();
			
			if(playerEnum.gender == Gender.GENDER_FEMALE)
			{
				customFacialHair.Clear();
				customFacialHair.TrimExcess();
				var vec= customFace[playerEnum.face].Tattos.Split (";"[0]);//get the tattos of the first face mesh
				
				for(int i = 0; i<vec.Length; i++)	
				{
					if(vec[i].Length  < 1)
						continue;
					try
					{
						var aux= dbs.sCustomFacialHair.GetRecord(System.Int32.Parse(vec[i]));				
						if((aux.ID - mask) < 256 && (aux.ID - mask) >= 0)
						{							
							customFacialHair.Add(aux);
							Debug.Log((aux as CustomFacialHairEntry).ModelMesh);
						}
					}
					catch( Exception e )
					{
						#if UNITY_EDITOR
						Debug.LogWarning("Unknown facial hair id = "+vec[i]);
						#endif	
					}
				}
				
				MAX_FACIALHAIR_STYLES = customFacialHair.Count;
				Debug.Log(" //////////////// "+MAX_FACIALHAIR_STYLES);
				if(MAX_FACIALHAIR_STYLES > 0){
					newCust.charFacialHair =  customFacialHair[0].ID-mask;		
					playerEnum.facialHair =  customFacialHair[0].ID-mask;
					Debug.Log(" \\\\\\\\\\\\ "+MAX_FACIALHAIR_STYLES);
					UpdateFacialHair();				
				}
			}
		}	
		
		if(playerEnum.hairStyle != newCust.charHairStyle){
			if(newCust.charHairStyle >= MAX_HAIR_STYLES){
				newCust.charHairStyle = 0;
			}	
			
			playerEnum.hairStyle = newCust.charHairStyle;	
			UpdateHairStyle();
		}
		
		if(playerEnum.hairColor != newCust.charHairColor){
			if(newCust.charHairColor >= MAX_HAIR_COLORS){
				newCust.charHairColor = 0;
			}			
			playerEnum.hairColor = newCust.charHairColor;
			UpdateHairColor();
		}			
		
		if(playerEnum.facialHair != newCust.charFacialHair){
			if(playerEnum.gender == Gender.GENDER_FEMALE)
			{
				if(MAX_FACIALHAIR_STYLES>0){
					if((mask + newCust.charFacialHair >= (customFacialHair[0].ID + MAX_FACIALHAIR_STYLES)))
					{
						newCust.charFacialHair = customFacialHair[0].ID - mask;
					}
				}
				else{
					newCust.charFacialHair = 0;
				}
			}
			else //male
			{
				if(newCust.charFacialHair >=  MAX_FACIALHAIR_STYLES){
					newCust.charFacialHair = 0;
				}	
			}	
			playerEnum.facialHair = newCust.charFacialHair;
			
			UpdateFacialHair();
		}
		
		//Debug.Log("Race: "+playerEnum.race+"  Gender: "+playerEnum.gender+"  Class: "+playerEnum.classType+" Face: "+ playerEnum.face +"  FaacialHair: "+ playerEnum.facialHair+" Skin Color "+ playerEnum.skinColor +"  Hairstyle: "+ playerEnum.hairStyle+" Hair Color "+ playerEnum.hairColor);		
	}
	
	public void  RandomCustomizations ()
	{
		Debug.Log("************************* Random customization ");
		newCust.charFace = UnityEngine.Random.Range(0, MAX_FACE_STYLES);
		if(playerEnum.gender == Gender.GENDER_FEMALE)
		{
			customFacialHair.Clear();
			customFacialHair.TrimExcess();
			var vec= customFace[newCust.charFace].Tattos.Split (";"[0]);//get the tattos of the first face mesh
			
			for(int i = 0; i<vec.Length; i++)
			{
				if(vec[i].Length  < 1)
					continue;
				
				try
				{
					var aux= dbs.sCustomFacialHair.GetRecord(System.Int32.Parse(vec[i]));				
					if((aux.ID - mask) < 256 && (aux.ID - mask) >= 0)
					{							
						customFacialHair.Add(aux);
					}
				}
				catch( Exception e )
				{
					#if UNITY_EDITOR
					Debug.LogWarning("Unknown facial hair id = "+vec[i]);
					#endif	
				}
				MAX_FACIALHAIR_STYLES = customFacialHair.Count;
				if(customFacialHair.Count > 0)
				{
					int ran = UnityEngine.Random.Range(0, MAX_FACIALHAIR_STYLES);
					newCust.charFacialHair =  customFacialHair[0].ID - mask + ran;
				}
			}
		}
		else{
			newCust.charFacialHair	= UnityEngine.Random.Range(0, MAX_FACIALHAIR_STYLES);	
		}
		newCust.charHairStyle	= UnityEngine.Random.Range(0, MAX_HAIR_STYLES);
		newCust.charHairColor	= UnityEngine.Random.Range(0, MAX_HAIR_COLORS);
		newCust.charSkinColor	= UnityEngine.Random.Range(0, MAX_SKIN_COLORS);	
		
		UpdateCharacter();
	}	
	
	void Assemble()//UpdateItems, UpdateSkin(), ..........
	{
		InitMaxStylesAndColors();
		base.Assemble();
	}
}