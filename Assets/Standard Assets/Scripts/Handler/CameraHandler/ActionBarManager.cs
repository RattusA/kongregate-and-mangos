﻿using UnityEngine;
using System.Collections.Generic;

public class ActionBarManager
{
	protected uint PLAYER_SLOT_COUNT = 62;
	protected uint PET_SLOT_COUNT = 4;
	
	public Dictionary<int, ActionBarSlot> actionBarSlots = new Dictionary<int, ActionBarSlot>();
	public Dictionary<int, ActionBarSlot> petActionBarSlots = new Dictionary<int, ActionBarSlot>();
	
	public void  SetObjectToSlot ( int resivePosition, ActionButtonType resiveType, uint resiveID )
	{
		if(resivePosition < 0 || resivePosition >= PLAYER_SLOT_COUNT)
		{
			Debug.LogWarning("Slot position incorrect!");
			return;
		}
		
		switch (resiveType)
		{
		case ActionButtonType.ACTION_BUTTON_MACRO:
			actionBarSlots[resivePosition] = new ActionBarSlot(resiveType, resiveID);
			break;
		case ActionButtonType.ACTION_BUTTON_ITEM:
			if(CheckExistingID(resiveType, resiveID))
				actionBarSlots[resivePosition] = new ActionBarSlot(resiveType, resiveID);
			break;
		case ActionButtonType.ACTION_BUTTON_SPELL:
			if(CheckExistingID(resiveType, resiveID))
				actionBarSlots[resivePosition] = new ActionBarSlot(resiveType, resiveID);
			break;
		}
	}
	
	private bool CheckExistingID ( ActionButtonType resiveType ,   uint resiveID )
	{
		bool  result = false;
		switch (resiveType)
		{
		case ActionButtonType.ACTION_BUTTON_SPELL:
			SpellEntry spell = dbs.sSpells.GetRecord(resiveID);
			
			if(spell == null)
				Debug.LogWarning("No such spell in database!");
			else
				result = true;
			break;
		case ActionButtonType.ACTION_BUTTON_ITEM:
			ItemEntry item = AppCache.sItems.GetItemEntry(resiveID);
			
			if(item == null)
				Debug.LogWarning("No such item in database!");
			else
				result = true;
			break;
		default:
			result = false;
			break;
		}
		return result;
	}
	
	public void  DoAction ( uint resivePosition, BaseObject target )
	{
		DoAction ( (int)resivePosition, target );
	}

	public void  DoAction ( int resivePosition, BaseObject target )
	{
		if(actionBarSlots.ContainsKey(resivePosition))
			actionBarSlots[resivePosition].DoAction(target);
	}
	
	public void  DoPetAction ( int resivePosition ,   BaseObject target )
	{
		if(petActionBarSlots.ContainsKey(resivePosition))
			petActionBarSlots[resivePosition].DoAction(target);
	}
	
	public ActionBarSlot GetSlotObject ( int resivePosition )
	{
		ActionBarSlot ret;
		actionBarSlots.TryGetValue(resivePosition, out ret);
		
		return ret;
	}
	
	public ActionBarSlot GetSlotPetObject ( uint resivePosition )
	{
		return GetSlotPetObject((int)resivePosition);
	}

	public ActionBarSlot GetSlotPetObject ( int resivePosition )
	{
		ActionBarSlot ret;
		petActionBarSlots.TryGetValue(resivePosition, out ret);
		
		return ret;
	}
	
	public void  ClearPetSpellBook ()
	{
		petActionBarSlots.Clear();
	}
	
	public void  SetPetSpellToSlot ( uint resivePosition ,   ActionButtonType resiveType ,   uint resiveID )
	{
		if(resivePosition > PET_SLOT_COUNT)
		{
			Debug.LogWarning("Slot position incorrect!");
			return;
		}
		
		petActionBarSlots[(int)resivePosition] = new ActionBarSlot(resiveType, resiveID);
	}
	
	public void  EquipPetSpell ( ulong petGuid ,   ActionButtonType type ,   uint position ,   uint spellID )
	{
		if(position > PET_SLOT_COUNT)
		{
			Debug.LogWarning("Slot position incorrect!");
			return;
		}
		
		bool  isSpellOnPanel = false;
		uint swapPosition = 0;
		
		for(int index = 0; index < PET_SLOT_COUNT; index++)
		{
			ActionBarSlot actionSlot = GetSlotPetObject(index);
			if((actionSlot != null) && (actionSlot.objectID == spellID))
			{
				isSpellOnPanel = true;
				swapPosition = (uint)index;
				break;
			}
		}
		
		if(isSpellOnPanel)
		{
			ActionBarSlot swapSlot = GetSlotPetObject(position);
			SendPacket.SetPetActionButton(petGuid, position, spellID, swapPosition, swapSlot.objectID);
			
			SetPetSpellToSlot(swapPosition, type, swapSlot.objectID);
			SetPetSpellToSlot(position, type, spellID);
		}
		else
		{
			SendPacket.SetPetActionButton(petGuid, position, spellID);
			SetPetSpellToSlot(position, type, spellID);
		}
	}
	
	public void  Replace ( uint fromSpellID ,   uint toSpellID )
	{
		// TODO: Добавить функцию замены спелов в слотах
		Debug.LogError("Replace function not implemented!");
	}
}