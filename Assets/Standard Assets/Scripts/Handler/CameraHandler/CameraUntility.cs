﻿using UnityEngine;

/// <summary>
/// Camera utilities.
/// </summary>
public class CameraUntility : MonoBehaviour
{
//	[SerializeField]
//	private bool removeFog = false;

#if UNITY_WEBPLAYER
	private bool revertFogState = true;
#endif

	void OnPreRender()
	{
#if UNITY_WEBPLAYER
		revertFogState = RenderSettings.fog;
		RenderSettings.fog = enabled;
#endif
	}

	void OnPostRender()
	{
#if UNITY_WEBPLAYER
		RenderSettings.fog = revertFogState;
#endif
	}
}
