using UnityEngine;
using System;
using System.Collections;

public class Swipe : MonoBehaviour
{
	float radius = 0;
	int minRadius = 0;
	int maxRadius = 6;
	float a = 0;//radian alpha
	float b = 0;//radian beta
	float lastA = 0;
	float lastB = 0;
	static float cameraAngularLimit = 0.1f;
	int touchNumber = -1;
	GameObject parent;
	Collider worldCollider = null;
	Rect touchRect;
	Rect touchRect1;
	Rect touchRect2;
	bool  ret;
	bool  touchOk;
	BaseObject target;
	BaseObject tar;
	bool  isInFirstPerson = false;
	int layerMaskMainPlayer;
	int layerMaskAreaTrigger;
	int usedJapaneseCamera;
	
	//cam collisiion related
	Ray ray ;
	RaycastHit hit;
	int layerm;

	//tutorial flag and char level
	public static WorldPacket tutorialPacketInSwipe;
	
	public static PlayerEnum playerEnum;
	
	MainPlayer playerScript;

	Texture2D touchTexture;
	Effects eff;
	
	Vector3 addPos;
	
	Vector2 startPosition = new Vector2(0,0);
	Vector2 dPos;
	Vector2 startingPosition = Vector2.zero; 
	
	
	float pitchDistance = 0;
	Vector2 lastTouchPosition = Vector2.zero;
	public static int touchId1 = -1;
	Vector2 lastTouchPosition2 = Vector2.zero;
	public static int touchId2 = -1;
	float _previousCamZoomMaxLimit= 4.0f;

	// AoE functions
	private bool  usedAoE = false;
	public float oldRadius = 0;
	private Spell spell;
	
	private bool revertFogState = false;
	public bool  Fog = true;
	public float Far;

	public static bool setTutorial ( WorldPacket pkt )
	{
		bool  ret = false;
		if (pkt != null)
		{
			uint tutorialMask;
			tutorialMask = pkt.ReadReversed32bit();
			
			if (tutorialMask != 0xFFFFFFFF)
			{
				WorldSession.player.tutorialInstance.setTutorialStep((TutorialStages)tutorialMask);
				ret = true;
			}
			else
			{
				WorldSession.player.tutorialInstance.setTutorialStep(TutorialStages.TUTORIAL_ENDED);
			}
		}
		else
		{
			Debug.Log("Tutorial flag packet is NULL.");
			ret = true;
		}
		return ret;
	}
	
	void  Awake ()
	{
		RenderSettings.fog = false;
		#if UNITY_ANDROID && !UNITY_EDITOR || UNITY_IPHONE
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		// == deprecated == iPhone.screenCanDarken = Global.sleepMode;//disable sleepMode
		#endif
		
		//Debug.Log("************************************ignore layers "+layerMask); 
		layerMaskMainPlayer = 1<<LayerMask.NameToLayer("main_character");
		layerMaskAreaTrigger = 1<<LayerMask.NameToLayer("AreaTrigger");
		layerMaskMainPlayer += layerMaskAreaTrigger;
		layerMaskMainPlayer = ~layerMaskMainPlayer;	//setting it to all the other layers except main_character so we can ignore collision with mainchar
		layerMaskAreaTrigger = ~layerMaskAreaTrigger;
	}
	
	void  Start ()
	{		
		ChangeLayerMask();
		LoadZone loadZoneManager = WorldSession.GetLoadZoneManager();
		loadZoneManager.load();
		Camera camera = GetComponent<Camera>();
		camera.nearClipPlane = 0.1f;
		if(Application.loadedLevelName == "InDoorScene")
		{
			camera.farClipPlane = 450;
		}
		Far = camera.farClipPlane;
		RenderSettings.fogEndDistance = Far;
		
		//Radu: Added this because for this particular dungeon 571 ( tombs )
		//the camera far Clip plane was too small.
		if( loadZoneManager.zone == "571" && Application.loadedLevelName != "OutDoorScene" )
			camera.farClipPlane = 1000;
		else
			camera.farClipPlane = 450;
		
		//setup different far clip for some layers
		var distances= new float[32];
		distances[13] = camera.farClipPlane/5;
		distances[12] = 30;
		camera.layerCullDistances = distances;
	}
	
	GameObject SpawnMainChar ()
	{
		GameObject player = Player.LoadPlayerModel(playerEnum.race, playerEnum.gender);
		
		var allChildren= player.gameObject.GetComponentsInChildren<Transform>();
		foreach(Transform child in allChildren) 
			child.gameObject.layer = 8;
		
		GameObject cam = Instantiate<GameObject>(GameResources.Load<GameObject>("prefabs/inventoryCamera"));
		
		CapsuleCollider col = player.GetComponent<CapsuleCollider>();
		float addY = col.height;

		cam.transform.parent = player.transform;
		cam.transform.localPosition = new Vector3(-0.02f,addY*0.5f,2);
		cam.transform.LookAt(player.transform.position+new Vector3(0,addY*0.5f,0));
		
		//player.AddComponent<collisionControl>();
		Transform pivot = player.transform.Find("obj");
		Destroy(pivot.GetComponent<OtherPlayer>());
		//======================================//
		Portrait portraitScript;
		portraitScript = pivot.gameObject.AddComponent<Portrait>();
		portraitScript.CameraPrefab = Resources.Load<GameObject>("prefabs/portrait/Portrait");
		portraitScript.SelfTexture = Resources.Load<RenderTexture>("prefabs/portrait/selfTexture");
		portraitScript.TargetTexture = Resources.Load<RenderTexture>("prefabs/portrait/targetTexture");
		//======================================//
		playerScript = pivot.gameObject.AddComponent<MainPlayer>();
		//======================
		portraitScript.MainPlayerRefference = playerScript;
		playerScript.PortraitReff = portraitScript;
		//======================
		playerScript.InitName(playerEnum.name);
		playerScript.race = playerEnum.race;
		playerScript.gender = playerEnum.gender;
		playerScript.playerClass = playerEnum.classType;
		playerScript.skinColor = (byte)playerEnum.skinColor;
		playerScript.face = (byte)playerEnum.face;
		playerScript.hairStyle = (byte)playerEnum.hairStyle;
		playerScript.hairColor = (byte)playerEnum.hairColor;
		playerScript.facialHair = (byte)playerEnum.facialHair;		
		
		//Debug.Log("************** Update player customizations: race:"+ playerScript.race+" gender: "+ playerScript.gender + " skin: "+playerScript.skinColor+" face: "+playerScript.face+" hairStyle: "+playerScript.hairStyle+" hairColor: "+playerScript.hairColor+" facialHair "+playerScript.facialHair);
		playerScript.armory = new AssambleCharacter(playerScript as Player);
		
		WorldSession.player = playerScript;
		
		player.name = "player";
		
		// tutorial handler
		GameObject tutorialObj = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("prefabs/Tutorial"));
		tutorialObj.transform.SetParent( playerScript.transform );
		tutorialObj.transform.localPosition = Vector3.zero;
		playerScript.tutorialInstance = tutorialObj.GetComponent<TutorialHandler>();
		playerScript.tutorialInstance.isTutorialNoEnded = setTutorial(tutorialPacketInSwipe);
		tutorialPacketInSwipe = null;
		return player;
	}	
	
	public void StartSpawnMainChar ()
	{		
		Vector3 lPos = new Vector3(0,2.3f,4.2f);//transform.localPosition;
		radius = lPos.magnitude;
		lastA = a = Mathf.Atan(Mathf.Sqrt(lPos.x*lPos.x + lPos.z*lPos.z)/lPos.y);
		lastB = b = Mathf.Atan(lPos.z/lPos.x);
		parent = GameObject.Find("player");
		if(parent != null)
		{
			playerScript = parent.GetComponentInChildren<MainPlayer>();
			WorldPacket wp = new WorldPacket(OpCodes.MSG_MOVE_WORLDPORT_ACK,0);
			RealmSocket.outQueue.Add(wp);
			playerScript.mainCamTransform = transform;
		}
		else
		{		
			parent = SpawnMainChar();
		}
		
		addPos = new Vector3(radius*Mathf.Sin(a)*Mathf.Cos(b),radius*Mathf.Cos(a),radius*Mathf.Sin(a)*Mathf.Sin(b));
		
		transform.position = parent.transform.position + addPos;
		
		transform.LookAt(parent.transform.position + new Vector3(0,1.5f,0));
		
		//setting var for WorldReference in MainPlayer (Lod system)
		GameObject world = GameObject.Find("world");
		if(world)
		{
			playerScript.WorldReference = world;
			eff = world.GetComponentInChildren<Effects>();
		}
		//worldCollider = GameObject.Find("world/mesh").GetComponent<Collider>();
		ray = new Ray(transform.position, parent.transform.position);
		
		touchRect = new Rect(0, Screen.height*0.55f, Screen.width*1/4, Screen.height*0.45f);
		touchRect1 = new Rect(Screen.width/4, 0, Screen.width*3/4, Screen.height);
		touchRect2 = new Rect(0, 0, Screen.width*1/4, Screen.height*0.55f);
		touchTexture = new Texture2D(1, 1);
		touchTexture.SetPixel(0, 0, new Color(0, 0, 0, 0.3f));
		touchTexture.Apply();
		
//		yield return new WaitForSeconds (3);//harcodedt
		//following line esets tutorial flag at login
		//ResetTutorialFlag();
	}
	
	
	//## following function is used to reset tutorial flag
	//## must be uncommented in order to be able to use ResetTutorialFlag() line above
	//
	//void  ResetTutorialFlag ()
	//{
	//		WorldPacket pktt;
	//		pktt = new WorldPacket();
	//		pktt.SetOpcode(OpCodes.CMSG_TUTORIAL_RESET);	
	//		RealmSocket.outQueue.Add(pktt);
	//		
	//		Debug.Log("Tutorial flag reset.");
	//}
	
	
	void chekZone()///check the camera position for different effects
	{
		if(!eff)
			return;
		
		foreach(Effects.Effect ef in eff.Areas)
		{
			Vector3 pos = transform.position;
			if((pos.y < ef.top) && (pos.y > ef.bottom))
			{
				//print(ef.rect);
				if(ef.rect.Contains(new Vector2(pos.x,pos.z)))
				{				
					//RenderSettings.fogDensity = 0.5f;
					RenderSettings.fog = true;
					RenderSettings.fogMode = FogMode.Exponential;
					RenderSettings.fogColor = ef.color;  
					return;
				}
			}
		}
		//	RenderSettings.fog = false;
	}
	
	void  Update ()
	{	
		if(!parent)
			return;
		
		chekZone();	
		
		ray.origin = parent.transform.position + new Vector3(0,1.5f,0);
		ray.direction= parent.transform.position + addPos - ray.origin;
		
		if (Physics.Raycast(ray, out hit, radius + 5f, layerm))
		{		
			transform.position = hit.point-ray.direction*(hit.distance*0.7f);
		}
		else
		{
			transform.position = parent.transform.position + addPos;	
		}
		
		transform.LookAt(parent.transform.position + new Vector3(0,1.5f,0));
		
		// if no rotation no need for new calculations
		if((a == lastA && b == lastB) || ButtonOLD.menuState || ButtonOLD.chatState || ButtonOLD.lootState || WorldSession.interfaceManager.hideInterface 
		   || (WorldSession.player.tutorialInstance && WorldSession.player.tutorialInstance.isNeedBlockMovement()))
			return;
		addPos = new Vector3(radius*Mathf.Sin(a)*Mathf.Cos(b),radius*Mathf.Cos(a),radius*Mathf.Sin(a)*Mathf.Sin(b));
		
		lastA = a;
		lastB = b;
	}
	
	bool pitch ()
	{
		foreach(Touch touch in Input.touches)
		{
			if( ButtonOLD.chatState && !playerScript.chatOpend && !playerScript.meniuOpend ) //(MainPlayer.chatOpend && !MainPlayer.meniuOpend) || (
			{
				playerScript.MoveScrollByTouch(touch, new Rect(1.5f*playerScript.auraWidth, Screen.width*0.0935f, Screen.width - 3*playerScript.auraWidth, Screen.height*3/4 - Screen.width*0.0935f ) );
			}
			
			//if( (touchId1 == touch.fingerId || touchId2 == touch.fingerId) && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled || touchRect.Contains(new Vector2(touch.position.x * 2, Screen.height) - touch.position)) )
			if( (touchId1 == touch.fingerId || touchId2 == touch.fingerId) && (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled ) )
			{
				if(touchId1 == touch.fingerId)
				{
					lastTouchPosition = Vector2.zero;
					touchId1 = -1; 
					print("touch ended");
					return false;
				}
				else
				{
					lastTouchPosition2 = Vector2.zero;
					touchId2= -1;
					print("touch ended");
					return false;
				}
			}
			
			
			if ( (touch.fingerId != joyStick.fingerID) && (touchRect1.Contains(new Vector2(touch.position.x * 2, Screen.height) - touch.position) || touchRect2.Contains(new Vector2(touch.position.x * 2, Screen.height) - touch.position)) )
				// first condition is meant to avoid "registering" joystick touch as "pitch" (for zooming) touch
			{
				if(touch.phase == TouchPhase.Began)
				{
					//print("new touch");
					if(lastTouchPosition==Vector2.zero)
					{
						lastTouchPosition = touch.position;
						touchId1 = touch.fingerId;
						if(lastTouchPosition2!=Vector2.zero)
						{
							print("new distance first touch");
							pitchDistance = (lastTouchPosition - lastTouchPosition2).magnitude;
						}
					}
					else
					{
						print("new distance second touch");
						pitchDistance = (lastTouchPosition - touch.position).magnitude;
						lastTouchPosition2 = touch.position;
						touchId2 = touch.fingerId;
						return true;
					}
					continue;
				}
				if( (Input.touchCount < 3 && joyStick.fingerID != -1) || (Input.touchCount < 2 && joyStick.fingerID == -1) )
					// limits minimum number of touches required for pitch (zoom), depending on the existence of a joystick touch
					//   this hopefully removed the zooming bug "swipe move zoom on accident"
					return false;
				else
				{
					if(lastTouchPosition==Vector2.zero)//if we don't have the first touch position no need for zoom
						return false;
					if(lastTouchPosition2==Vector2.zero)//if we don't have the second touch position find it later
						continue;
					if(touch.phase == TouchPhase.Moved && (touchId1 == touch.fingerId || touchId2 == touch.fingerId))
					{	
						if(touchId1 == touch.fingerId)
							lastTouchPosition = touch.position;
						else 
							lastTouchPosition2 = touch.position;
						float currentDistance = (lastTouchPosition - lastTouchPosition2).magnitude;
						radius -= (currentDistance-pitchDistance)*0.05f;
						if (radius < 2.3f)
						{
							isInFirstPerson = true;
							Camera.main.cullingMask &= ~(1<<LayerMask.NameToLayer("main_character")); 
						}
						if(radius > 2.3f)
						{
							isInFirstPerson = false;
							Camera.main.cullingMask |= 1<<LayerMask.NameToLayer("main_character"); 
						}
						if(radius<minRadius)
							radius = minRadius;
						else if(radius>maxRadius)
							radius = maxRadius;
						addPos = new Vector3(radius*Mathf.Sin(a)*Mathf.Cos(b),radius*Mathf.Cos(a),radius*Mathf.Sin(a)*Mathf.Sin(b));
						// I think addPos is the variable that pitch() affects for controlling the zoom 
						pitchDistance = currentDistance;	
						return true;
					}
				}
			}
		}
		return false;
	}
	
	void  LateUpdate ()
	{
		if(!parent)
			return;
		
		ret = pitch();
		if(ret)
		{		
			return;
		}
		touchOk = true;
		
		Camera camera = GetComponent<Camera>();
		float ScrollSpeed = PlayerPrefs.GetFloat("CameraSpeedOption");
		
		ScrollSpeed = 0.1f + 0.9f * ScrollSpeed;
		
		if (playerScript && playerScript.interf)
		{
			//checking to see if the click is outside of interface
			if(new Rect(Screen.width * 0.3f, Screen.height * 0.83f, Screen.width * 0.7f, Screen.height * 0.17f).Contains(new Vector3(Input.mousePosition.x,Screen.height-Input.mousePosition.y,Input.mousePosition.z)) && playerScript.actionBarPanel.bottomPanelFlag)
				touchOk=false;
			if(new Rect(Screen.width * 0.922f, Screen.height * 0.36f, Screen.width * 0.0793f, Screen.height * 0.55f).Contains(new Vector3(Input.mousePosition.x,Screen.height-Input.mousePosition.y,Input.mousePosition.z)) && playerScript.actionBarPanel.rightBotFlag)
				touchOk=false;		
			if(new Rect(Screen.width * 0.922f, Screen.height * 0.11f, Screen.width * 0.0793f, Screen.height * 0.55f).Contains(new Vector3(Input.mousePosition.x,Screen.height-Input.mousePosition.y,Input.mousePosition.z)) && playerScript.actionBarPanel.rightTopFlag)
				touchOk=false;
			if(new Rect(Screen.width * 0.849f, Screen.height * 0.35f, Screen.width * 0.0793f, Screen.height * 0.55f).Contains(new Vector3(Input.mousePosition.x,Screen.height-Input.mousePosition.y,Input.mousePosition.z)) && playerScript.actionBarPanel.rightExtFlag)
				touchOk=false;		
			for(var i=0; i<playerScript.group.getPlayersCount(); i++)
			{
				if(new Rect(playerScript.group.partyRect.x, playerScript.group.partyRect.y+2+ (playerScript.group.buffDim+playerScript.group.partyRect.height)*i, playerScript.group.partyRect.width , playerScript.group.partyRect.height).Contains(new Vector3(Input.mousePosition.x,Screen.height-Input.mousePosition.y,Input.mousePosition.z)))
					touchOk=false;
			}
		}
		if(!(ButtonOLD.menuState || ButtonOLD.chatState || ButtonOLD.lootState || ButtonOLD.summonState || WorldSession.interfaceManager.hideInterface
		     || (WorldSession.player.tutorialInstance && WorldSession.player.tutorialInstance.isNeedBlockMovement())))
		{
			if(!(playerScript.showQuest == false && playerScript.meniuOpend == false)) 
				// disable swipe in case of various menus
				return;
			if(touchOk && isUsedSelectingAoEPosition())
			{
				AoEMouseInput();
				UpdateCameraRotation();
			}
			if(Input.touchCount > 0 && ButtonOLD.lootState == false && ButtonOLD.chatState == false && touchOk)
			{
				foreach(Touch touch in Input.touches)	
				{	
					if(touchNumber != -1)
					{
						if(touch.phase == TouchPhase.Ended)
						{
							startPosition = new Vector2(0,0);
							touchNumber=-1;
							startingPosition -= touch.position;
							if(startingPosition.sqrMagnitude<625)
							{
								Ray ray = camera.ScreenPointToRay(new Vector3(touch.position.x,touch.position.y,0));
								RaycastHit hit;
								if (isInFirstPerson)
								{
									if(Physics.Raycast(ray, out hit, camera.farClipPlane, layerMaskMainPlayer))
									{		
										tar = hit.transform.gameObject.GetComponentInChildren<BaseObject>(); 
										if(tar)
										{
											playerScript.sendTarget(tar);
											//Debug.Log("first person: " + isInFirstPerson + " | layerMask: " + layerMaskMainPlayer);
										}
										
									}
								}
								else
								{ 
									//Debug.Log("first person: " + isInFirstPerson + " | layerMask: " + layerMaskAreaTrigger);
									if(Physics.Raycast(ray, out hit, camera.farClipPlane, layerMaskAreaTrigger))
									{					
										tar = hit.transform.gameObject.GetComponentInChildren<BaseObject>(); 
										if(tar)
										{
											playerScript.sendTarget(tar);
										}								
									}
								}
							}
							startingPosition = Vector2.zero;
						}
					}
					if(!(touchRect1.Contains(new Vector2(touch.position.x * 2, Screen.height) - touch.position) || touchRect2.Contains(new Vector2(touch.position.x * 2, Screen.height) - touch.position)))
						continue;
					if(touch.phase == TouchPhase.Began)
					{
						if(touchNumber==-1)
						{
							startingPosition = startPosition = touch.position;
							touchNumber = touch.fingerId;
						}
						continue;
					}
					else if(startPosition == new Vector2(0,0))/// starting position was to low
						continue;
					if(touch.phase == TouchPhase.Moved)
					{
						if(touchNumber == touch.fingerId)
						{
							dPos = startPosition - touch.position;				
							a -= (dPos.y * Mathf.PI/180) * ScrollSpeed;
							/// setting y camera limit
							if(a>Mathf.PI-cameraAngularLimit*10)
								a= Mathf.PI-cameraAngularLimit*10;
							else if(a< cameraAngularLimit)
								a = cameraAngularLimit;
							// end camera y limit
							
							b += (dPos.x * Mathf.PI/180) * ScrollSpeed; 
							startPosition = touch.position;
						}
					}
				}
			}
			if(Application.platform != RuntimePlatform.IPhonePlayer && Application.platform != RuntimePlatform.Android)
			{
				if(Input.GetKey ("a"))
				{
					b += 3 * Time.deltaTime;
				}
				else if(Input.GetKey ("d"))
				{
					b -= 3 * Time.deltaTime;
				}			
				
				if(Input.GetMouseButtonDown(0) && touchOk)
				{
					startPosition = Input.mousePosition;
					startingPosition = Input.mousePosition;
				}
				if(Input.GetMouseButton(0) && !playerScript.moveStick.isTouched() && touchOk)
				{
					dPos = startPosition - (Vector2)Input.mousePosition;			
					a -= (dPos.y * Mathf.PI/180) * ScrollSpeed;
					/// setting y camera limit
					if(a>Mathf.PI-cameraAngularLimit*10)
						a= Mathf.PI-cameraAngularLimit*10;
					else if(a< cameraAngularLimit)
						a = cameraAngularLimit;
					// end camera y limit
					
					b += (dPos.x * Mathf.PI/180) * ScrollSpeed; 
					startPosition = Input.mousePosition;
				}
				if(Input.GetMouseButtonUp(0) && ButtonOLD.lootState==false && ButtonOLD.needState == false && ButtonOLD.chatState == false && touchOk)
				{
					startingPosition -= (Vector2)Input.mousePosition;
					if(startingPosition.sqrMagnitude<10000)//625)// 25 px
					{
						Ray ray1 = camera.ScreenPointToRay(new Vector3(Input.mousePosition.x,Input.mousePosition.y,0));
						RaycastHit hit1 = new RaycastHit();
						if (isInFirstPerson)
						{
							if(Physics.Raycast(ray1, out hit1, camera.farClipPlane, layerMaskMainPlayer))
							{		
								//Debug.Log(hit1.transform.name);			
								tar = hit1.transform.gameObject.GetComponentInChildren<BaseObject>(); 
								if(tar)
								{
									playerScript.sendTarget(tar);
									//Debug.Log("first person: " + isInFirstPerson + " | layerMask: " + layerMask);
								}
								
							}
						}
						else
						{ 
							//Debug.Log("first person: " + isInFirstPerson + " | layerMask: " + layerMask);
							if(Physics.Raycast(ray1, out hit1, camera.farClipPlane, layerMaskAreaTrigger))
							{			
								//Debug.Log("we hit: " + hit1.transform.name);		
								tar = hit1.transform.gameObject.GetComponentInChildren<BaseObject>(); 
								if(tar)
								{
									playerScript.sendTarget(tar);
								}
								
							}
						}
						
					}
					startingPosition = Vector2.zero;
				}
			}
		}
		UpdateCameraRotation();
	}
	
	void  UpdateCameraRotation ()
	{
		if(playerScript.showQuest == false && playerScript.meniuOpend == false )
			if(Application.platform!=RuntimePlatform.IPhonePlayer && Application.platform != RuntimePlatform.Android)
		{
			if(Input.GetAxis("Mouse ScrollWheel") > 0)
			{
				if(Input.GetAxis("Mouse ScrollWheel") > 0)
				{
					radius+=0.05f;
					if(radius>maxRadius)
						radius = maxRadius;
					addPos = new Vector3(radius*Mathf.Sin(a)*Mathf.Cos(b),radius*Mathf.Cos(a),radius*Mathf.Sin(a)*Mathf.Sin(b));
					if(radius > 2.3f)
					{
						isInFirstPerson = false;
						Camera.main.cullingMask |= 1<<LayerMask.NameToLayer("main_character"); 
					}
				}	
				if(Input.GetAxis("Mouse ScrollWheel") < 0)
				{
					radius-=0.05f;
					if(radius<minRadius)
						radius = minRadius;
					addPos = new Vector3(radius*Mathf.Sin(a)*Mathf.Cos(b),radius*Mathf.Cos(a),radius*Mathf.Sin(a)*Mathf.Sin(b));
					if(radius <= 2.3f)
					{
						isInFirstPerson = true;
						Camera.main.cullingMask &= ~(1<<LayerMask.NameToLayer("main_character")); 
					}
					
				}
			}
		}
	}
	
#region AoE functions
	public void  EnableSelectingAoEPosition ()
	{
		usedAoE = true;
	}
	
	public void  DisableSelectingAoEPosition ()
	{
		usedAoE = false;
	}
	
	public bool isUsedSelectingAoEPosition ()
	{
		return usedAoE;
	}
	
	public void  AoESpell ( Spell castingSpell )
	{
		spell = castingSpell;
	}
	
	void  CreateAoEMarker ( Vector3 pos )
	{
		GameObject marker;
		marker = Resources.Load<GameObject>("prefabs/AoEMarker");
		if(marker)
		{
			marker = GameObject.Instantiate(marker);
			marker.name = "AoEMarker";
			marker.transform.position = pos;
		}
	}
	
	void  AoEMouseInput ()
	{
		RaycastHit hit;
		Ray ray;
		if(isUsedSelectingAoEPosition())
		{
			GameObject marker = GameObject.Find("AoEMarker");
			if(Input.GetMouseButtonDown(0))
			{
				if(!marker)
				{
					// Create marker
					ray = Camera.main.ScreenPointToRay (Input.mousePosition);
					if (Physics.Raycast (ray, out hit, 100, layerMaskMainPlayer))
					{
						oldRadius = radius;
						radius = maxRadius;
						gameObject.transform.position = playerScript.TransformParent.position + (Vector3.up * 8f);
						CreateAoEMarker(hit.point);
					}
				}
				else
				{
					// Start spell
					playerScript.playerSpellManager.CastSpell(spell, 0, marker.transform.position);
					radius = oldRadius;
					marker.SendMessage("DisableMarker");
					DisableSelectingAoEPosition();
				}
			}
			if(Input.GetMouseButtonUp(0))
			{
				marker.SendMessage("StartTimer");
			}
			if(Input.GetMouseButton(0) && marker)
			{
				ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				if (Physics.Raycast (ray, out hit, 100, layerMaskMainPlayer))
				{
					marker.SendMessage("MoveToPosition", hit.point);
				}
			}
		}
	}
	
	void  AoETouchInput ()
	{
		//	bool  hasTouch = false;
		//	Vector3 aoeTouchPosition;
		//	if(isUsedSelectingAoEPosition())
		//	{
		//		if(touch.phase != TouchPhase.Ended)
		//		{
		//			hasTouch = true;
		//			aoeTouchPosition = new Vector3(touch.position.x, touch.position.y, 0);
		//		}
		//		if(touch.phase != TouchPhase.Ended)
		//		{
		//			hasTouch = true;
		//			aoeTouchPosition = new Vector3(touch.position.x, touch.position.y, 0);
		//		}
		//		if(touch.phase != TouchPhase.Ended)
		//		{
		//			hasTouch = true;
		//			aoeTouchPosition = new Vector3(touch.position.x, touch.position.y, 0);
		//		}
		//	}
	}
#endregion AoE functions
	
	public void  ChangeLayerMask ()
	{
		usedJapaneseCamera = PlayerPrefs.GetInt("JapaneseCamera", 0);
		layerm = (usedJapaneseCamera != 0) ? (-2147432682):(-2147420393); //default -2147420393;
	}
	
	public void  MountCamera ( Player plr )
	{
		if(plr.guid == MainPlayer.GUID)
		{
			_previousCamZoomMaxLimit = radius;
			radius = 10;
			a = 0.68f;
		}
	}
	
	public void  UnmountCamera ( Player plr )
	{
		try
		{
			if(plr.guid == MainPlayer.GUID)
			{
				radius = _previousCamZoomMaxLimit;
				a = 1.00f;
			}
		}
		catch(Exception exception)
		{
			Debug.Log("exception in setting of camera: " + exception);
		}
	}

	void OnPreRender()
	{
		revertFogState = RenderSettings.fog;
		RenderSettings.fog = Fog;
	}
	
	void OnPostRender()
	{
		RenderSettings.fog = revertFogState;
	}
}
