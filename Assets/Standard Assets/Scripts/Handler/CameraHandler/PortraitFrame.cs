using UnityEngine;
using System.Collections.Generic;

public class PortraitFrame
{
	private MainPlayer mainPlayer;
	private UIText text;
	
	private UIButton UserPortraitFrame;
	private UIButton UserLife;
	private UIButton UserEnergy;
	private UIButton UserActiveLife;
	private UIButton UserActiveEnergy;
	
	UITextInstance UserName;
	UITextInstance UserLifeText;
	UITextInstance UserEnergyText;
	
	UIAbsoluteLayout PlayerContainer;
	
	private UIButton PetPortraitFrame;
	private UIButton PetLife;
	private UIButton PetEnergy;
	private UIButton PetActiveLife;
	private UIButton PetActiveEnergy;
	
	UITextInstance PetName;
	
	UIAbsoluteLayout PetContainer;
	
	private UIButton TargetPortraitFrame;
	private UIButton TargetLife;
	private UIButton TargetEnergy;
	private UIButton TargetActiveLife;
	private UIButton TargetActiveEnergy;
	
	UITextInstance TargetName;
	UITextInstance TargetLifeText;
	UITextInstance TargetEnergyText;
	
	UIAbsoluteLayout TargetContainer;
	
	int numberOfAura;
	int numberOfAuraTarget;
	
	uint AuraButtonDim;
	uint AuraButtonSpacing;
	
	// Portrait frame size
	private uint PortraitWidth;
	private uint PortraitHeight;
	
	// Life and Energy frame size
	private uint LifeAndEnergyWidth;
	private uint LifeAndEnergyHeight;
	
	// Pet Portrait frame size
	private uint PetPortraitWidth;
	private uint PetPortraitHeight;
	
	// Pet  Life and Energy frame size
	private uint PetLifeAndEnergyWidth;
	private uint PetLifeAndEnergyHeight;
	
	private float percent;
	
	private List<PortraitAuraIcon> playerAuraList = new List<PortraitAuraIcon>();
	private List<PortraitAuraIcon> targetAuraList = new List<PortraitAuraIcon>();
	
	//////////////////////////////////////////////////////////////////////////
	// Functions
	//////////////////////////////////////////////////////////////////////////
	public PortraitFrame ()
	{
		if(!WorldSession.player)
			return;
		mainPlayer = WorldSession.player;
		
		PlayerContainer = new UIAbsoluteLayout();
		PetContainer = new UIAbsoluteLayout();
		TargetContainer = new UIAbsoluteLayout();
		
		text = new UIText(UIPrime31.myToolkit1, "fontiny simple", "fontiny simple.png");
		text.alignMode = UITextAlignMode.Center;
		
		// Set Portrait frame size
		PortraitWidth = (uint)(Screen.width * 0.26f);
		PortraitHeight = (uint)(PortraitWidth * 0.33f);
		
		LifeAndEnergyWidth = (uint)(Screen.width * 0.165f);
		LifeAndEnergyHeight = (uint)(Screen.height * 0.023f);
		
		// Set Pet Portrait frame size
		PetPortraitWidth = (uint)(Screen.width * 0.1492f);
		PetPortraitHeight = (uint)(Screen.width * 0.038f);
		
		PetLifeAndEnergyWidth = (uint)(Screen.width * 0.13f);
		PetLifeAndEnergyHeight = (uint)(Screen.width * 0.0062f);
		
		AuraButtonDim = PortraitHeight / 2;
		AuraButtonSpacing = AuraButtonDim / 15;
		
		if(WorldSession.interfaceManager.hideInterface == false)
		{
			InitUserPortrait();
			InitUserPet();
			InitTargetPortrait();
			InitAuras();
		}
	}
	
	void  InitUserPortrait ()
	{
		float UserFramePositionX = 0; 
		float UserFramePositionY = 0;
		
		float UserLifeAndEnergyPositionX = PortraitWidth * 0.33f;
		float UserLifeFramePositionY = PortraitHeight * 0.3f;
		float UserEnergyFramePositionY = PortraitHeight * 0.5f;
		
		
		UserPortraitFrame = UIButton.create(UIPrime31.myToolkit4, "portrait_frame.png", "portrait_frame.png", 0, 0);
		UserPortraitFrame.setSize(PortraitWidth, PortraitHeight);
		UserPortraitFrame.position = new Vector3(UserFramePositionX, -UserFramePositionY, 0);
		UserPortraitFrame.onTouchUpInside += mainPlayer.interf.PortraitAction;
		
		UserLife = UIButton.create(UIPrime31.myToolkit4, "portrait_progress_bar.png", "portrait_progress_bar.png", 0, 0);
		UserLife.setSize(LifeAndEnergyWidth, LifeAndEnergyHeight);
		UserLife.position = new Vector3(UserLifeAndEnergyPositionX, -UserLifeFramePositionY, 0);
		
		UserEnergy = UIButton.create(UIPrime31.myToolkit4, "portrait_progress_bar.png", "portrait_progress_bar.png", 0, 0);
		UserEnergy.setSize(LifeAndEnergyWidth, LifeAndEnergyHeight);
		UserEnergy.position = new Vector3(UserLifeAndEnergyPositionX, -UserEnergyFramePositionY, 0);
		
		UserActiveLife = UIButton.create(UIPrime31.myToolkit4, "portrait_health.png", "portrait_health.png", 0, 0);
		UserActiveLife.setSize(LifeAndEnergyWidth, LifeAndEnergyHeight);
		UserActiveLife.position = new Vector3(UserLifeAndEnergyPositionX, -UserLifeFramePositionY, 0);
		
		UserActiveEnergy = UIButton.create(UIPrime31.myToolkit4, "portrait_mana.png", "portrait_mana.png", 0, 0);
		UserActiveEnergy.setSize(LifeAndEnergyWidth, LifeAndEnergyHeight);
		UserActiveEnergy.position = new Vector3(UserLifeAndEnergyPositionX, -UserEnergyFramePositionY, 0);
		
		PlayerContainer.addChild(UserPortraitFrame);
		PlayerContainer.addChild(UserLife);
		PlayerContainer.addChild(UserEnergy);
		PlayerContainer.addChild(UserActiveLife);
		PlayerContainer.addChild(UserActiveEnergy);
		PlayerContainer.hidden = false;
		
		
		float UserNamePositionX = UserLifeAndEnergyPositionX + LifeAndEnergyWidth / 2;
		float UserNamePositionY = UserLifeFramePositionY - LifeAndEnergyHeight;
		float UserLifeAndEnergyTextPositionX = UserLifeAndEnergyPositionX + LifeAndEnergyWidth / 2;
		float UserLifeTextPositionY = UserLifeFramePositionY + LifeAndEnergyHeight / 2;
		float UserEnergyTextPositionY = UserEnergyFramePositionY + LifeAndEnergyHeight / 2;
		
		float SizeMod = 0.32f * dbs._sizeMod;
		
		
		UserName = text.addTextInstance(mainPlayer.name, UserNamePositionX, UserNamePositionY, SizeMod, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		if(mainPlayer.m_deathState == DeathState.DEAD)
			UserLifeText = text.addTextInstance("0" + "/" + mainPlayer.MaxHP, UserLifeAndEnergyTextPositionX, UserLifeTextPositionY, SizeMod, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		else		
			UserLifeText = text.addTextInstance(mainPlayer.HP + "/" + mainPlayer.MaxHP, UserLifeAndEnergyTextPositionX, UserLifeTextPositionY, SizeMod, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		UserEnergyText = text.addTextInstance(mainPlayer.Power + "/" + mainPlayer.MaxPower, UserLifeAndEnergyTextPositionX, UserEnergyTextPositionY, SizeMod, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		UserName.hidden = false;
		UserLifeText.hidden = false;
		UserEnergyText.hidden = false;
	}
	
	void  InitUserPet ()
	{
		float PetFramePositionX = Screen.width * 0.085f; 
		float PetFramePositionY = Screen.width * 0.065f;
		
		float PetLifeAndEnergyPositionX = Screen.width * 0.093f;
		float PetLifeFramePositionY = Screen.width * 0.083f;
		float PetEnergyFramePositionY = Screen.width * 0.093f;
		
		
		PetPortraitFrame = UIButton.create(UIPrime31.myToolkit4, "party_frame.png", "party_frame.png", 0, 0);
		PetPortraitFrame.setSize(PetPortraitWidth, PetPortraitHeight);
		PetPortraitFrame.position = new Vector3(PetFramePositionX, -PetFramePositionY, 0);
		
		PetLife = UIButton.create(UIPrime31.myToolkit4, "party_progress_bar.png", "party_progress_bar.png", 0, 0);
		PetLife.setSize(PetLifeAndEnergyWidth, PetLifeAndEnergyHeight);
		PetLife.position = new Vector3(PetLifeAndEnergyPositionX, -PetLifeFramePositionY, 0);
		
		PetEnergy = UIButton.create(UIPrime31.myToolkit4, "party_progress_bar.png", "party_progress_bar.png", 0, 0);
		PetEnergy.setSize(PetLifeAndEnergyWidth, PetLifeAndEnergyHeight);
		PetEnergy.position = new Vector3(PetLifeAndEnergyPositionX, -PetEnergyFramePositionY, 0);
		
		PetActiveLife = UIButton.create(UIPrime31.myToolkit4, "party_health.png", "party_health.png", 0, 0);
		PetActiveLife.setSize(PetLifeAndEnergyWidth, PetLifeAndEnergyHeight);
		PetActiveLife.position = new Vector3(PetLifeAndEnergyPositionX, -PetLifeFramePositionY, 0);
		
		PetActiveEnergy = UIButton.create(UIPrime31.myToolkit4, "party_mana.png", "party_mana.png", 0, 0);
		PetActiveEnergy.setSize(PetLifeAndEnergyWidth, PetLifeAndEnergyHeight);
		PetActiveEnergy.position = new Vector3(PetLifeAndEnergyPositionX, -PetEnergyFramePositionY, 0);
		
		PetContainer.addChild(PetPortraitFrame);
		PetContainer.addChild(PetLife);
		PetContainer.addChild(PetEnergy);
		PetContainer.addChild(PetActiveLife);
		PetContainer.addChild(PetActiveEnergy);
		PetContainer.hidden = true;
		
		
		float PetNamePositionX = PetLifeAndEnergyPositionX + PetLifeAndEnergyWidth / 2;
		float PetNamePositionY = PetLifeFramePositionY - PetLifeAndEnergyHeight;
		
		float SizeMod = 0.23f * dbs._sizeMod;
		
		PetName = text.addTextInstance("", PetNamePositionX, PetNamePositionY, SizeMod, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		PetName.hidden = true;
		
	}
	
	void  InitTargetPortrait ()
	{		
		float TargetFramePositionX = Screen.width * 0.45f; 
		float TargetFramePositionY = 0;
		
		float TargetLifeAndEnergyPositionX = TargetFramePositionX + PortraitWidth * 0.33f;
		float TargetLifeFramePositionY = PortraitHeight * 0.3f;
		float TargetEnergyFramePositionY = PortraitHeight * 0.5f;
		
		
		TargetPortraitFrame = UIButton.create(UIPrime31.myToolkit4, "portrait_frame.png", "portrait_frame.png", 0, 0);
		TargetPortraitFrame.setSize(PortraitWidth, PortraitHeight);
		TargetPortraitFrame.position = new Vector3(TargetFramePositionX, -TargetFramePositionY, 0);
		TargetPortraitFrame.onTouchUpInside += mainPlayer.interf.PortraitFightAction;
		
		TargetLife = UIButton.create(UIPrime31.myToolkit4, "portrait_progress_bar.png", "portrait_progress_bar.png", 0, 0);
		TargetLife.setSize(LifeAndEnergyWidth, LifeAndEnergyHeight);
		TargetLife.position = new Vector3(TargetLifeAndEnergyPositionX, -TargetLifeFramePositionY, 0);
		
		TargetEnergy = UIButton.create(UIPrime31.myToolkit4, "portrait_progress_bar.png", "portrait_progress_bar.png", 0, 0);
		TargetEnergy.setSize(LifeAndEnergyWidth, LifeAndEnergyHeight);
		TargetEnergy.position = new Vector3(TargetLifeAndEnergyPositionX, -TargetEnergyFramePositionY, 0);
		
		TargetActiveLife = UIButton.create(UIPrime31.myToolkit4, "portrait_health.png", "portrait_health.png", 0, 0);
		TargetActiveLife.setSize(LifeAndEnergyWidth,LifeAndEnergyHeight);
		TargetActiveLife.position= new Vector3(TargetLifeAndEnergyPositionX, -TargetLifeFramePositionY, 0);
		
		TargetActiveEnergy = UIButton.create(UIPrime31.myToolkit4, "portrait_mana.png", "portrait_mana.png", 0, 0);
		TargetActiveEnergy.setSize(LifeAndEnergyWidth, LifeAndEnergyHeight);
		TargetActiveEnergy.position = new Vector3(TargetLifeAndEnergyPositionX, -TargetEnergyFramePositionY, 0);
		
		TargetContainer.addChild(TargetPortraitFrame);
		TargetContainer.addChild(TargetLife);
		TargetContainer.addChild(TargetEnergy);
		TargetContainer.addChild(TargetActiveLife);
		TargetContainer.addChild(TargetActiveEnergy);
		TargetContainer.hidden = true;
		
		
		float TargetNamePositionX = TargetLifeAndEnergyPositionX + LifeAndEnergyWidth / 2;
		float TargetNamePositionY = TargetLifeFramePositionY - LifeAndEnergyHeight;
		float TargetLifeAndEnergyTextPositionX = TargetLifeAndEnergyPositionX + LifeAndEnergyWidth / 2;
		float TargetLifeTextPositionY = TargetLifeFramePositionY + LifeAndEnergyHeight / 2;
		float TargetEnergyTextPositionY = TargetEnergyFramePositionY + LifeAndEnergyHeight / 2;
		
		float SizeMod = 0.32f * dbs._sizeMod;
		
		
		TargetName = text.addTextInstance("", TargetNamePositionX, TargetNamePositionY, SizeMod, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		TargetLifeText = text.addTextInstance("", TargetLifeAndEnergyTextPositionX, TargetLifeTextPositionY, SizeMod, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		TargetEnergyText = text.addTextInstance("", TargetLifeAndEnergyTextPositionX, TargetEnergyTextPositionY, SizeMod, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		TargetName.hidden = true;
		TargetLifeText.hidden = true;
		TargetEnergyText.hidden = true;
	}
	
	void  InitAuras ()
	{
		UpdateUserAuras();
		
		if(mainPlayer.target)
		{
			UpdateTargetAuras();
		}
	}
	
	public void  HideUserPortrait ( bool state )
	{
		PlayerContainer.hidden = state;
		
		UserName.hidden = state;
		UserLifeText.hidden = state;
		UserEnergyText.hidden = state;
	}
	
	public void  HidePetPortrait ( bool state )
	{
		PetContainer.hidden = state;
		
		PetName.hidden = state;
	}
	
	public void  HideTargetPortrait ( bool state )
	{
		TargetContainer.hidden = state;
		
		TargetName.hidden = state;
		TargetLifeText.hidden = state;
		TargetEnergyText.hidden = state;
	}
	
	public void  SetUserHp ( int Hp ,   int MaxHp )
	{
		percent = Hp / (MaxHp + 0.0001f);
		UserActiveLife.setSize(LifeAndEnergyWidth * percent, LifeAndEnergyHeight);
		
		UserLifeText.clear();
		if(mainPlayer.m_deathState == DeathState.DEAD)
			UserLifeText.text = "0" + "/" + MaxHp;
		else
			UserLifeText.text = Hp + "/" + MaxHp;
	}
	
	public void  SetUserEnergy ( int Energy ,   int MaxEnergy )
	{
		percent = Energy / (MaxEnergy + 0.0001f);
		UserActiveEnergy.setSize(LifeAndEnergyWidth * percent, LifeAndEnergyHeight);
		
		UserEnergyText.clear();
		UserEnergyText.text = Energy + "/" + MaxEnergy;
	}
	
	public void  SetPetHp ( int Hp ,   int MaxHp )
	{
		percent = Hp / (MaxHp + 0.0001f);
		PetActiveLife.setSize(PetLifeAndEnergyWidth * percent, PetLifeAndEnergyHeight);
	}
	
	public void  SetPetEnergy ( int Energy ,   int MaxEnergy )
	{
		percent = Energy / (MaxEnergy + 0.0001f);
		PetActiveEnergy.setSize(PetLifeAndEnergyWidth * percent, PetLifeAndEnergyHeight);
	}
	
	public void  SetTargetName ( string name )
	{
		TargetName.clear();
		TargetName.text = name;
	}
	
	public void  SetPetName ( string name )
	{
		PetName.clear();
		PetName.text = name;
	}
	
	public void  SetTargetHp ( int Hp ,   int MaxHp )
	{
		percent = Hp / (MaxHp + 0.0001f);
		TargetActiveLife.setSize(LifeAndEnergyWidth * percent, LifeAndEnergyHeight);
		
		TargetLifeText.clear();
		TargetLifeText.text = Hp + "/" + MaxHp;
	}
	
	public void  SetTargetEnergy ( int Energy ,   int MaxEnergy )
	{
		percent = Energy / (MaxEnergy + 0.0001f);
		TargetActiveEnergy.setSize(LifeAndEnergyWidth * percent, LifeAndEnergyHeight);
		
		TargetEnergyText.clear();
		TargetEnergyText.text = Energy + "/" + MaxEnergy;
	}
	
	public void  UpdateUserAuras ()
	{
		string spellImage;
		SpellEntry sp;
		SpellIconEntry icon;
		
		playerAuraList.Clear();
		
		numberOfAura = mainPlayer.auraManager.auras.Count;
		
		if(numberOfAura > 32)
		{
			numberOfAura = 32;
		}
		
		byte auraSlotNumber = 0;
		for(int i = 0; i < numberOfAura; i++)
		{
			if(TraitsAura.traitList.Contains((mainPlayer.auraManager.auras[i] as Aura).spellID))
			{
				if((mainPlayer.auraManager.auras[i] as Aura).auraDuration <= 0)
				{
					continue;
				}
			}
			sp = dbs.sSpells.GetRecord((mainPlayer.auraManager.auras[i] as Aura).spellID);
			icon  = dbs.sSpellIcons.GetRecord(sp.SpellIconID, false);
			spellImage = icon.Icon;
			
			if (string.IsNullOrEmpty (spellImage))
			{
				spellImage = "noicon.png";
				continue;
			}
			
			spellImage = spellImage.Substring(0, spellImage.LastIndexOf('.'));
			
			Texture2D auraTexture = Resources.Load<Texture2D>("Icons/" + spellImage);
			float auraIconPosX = PortraitWidth + AuraButtonSpacing * (auraSlotNumber % 4) + AuraButtonDim * (auraSlotNumber % 4);
			float auraIconPosY = AuraButtonSpacing * (auraSlotNumber / 4) + AuraButtonDim * (auraSlotNumber / 4);
			float auraIconSize = AuraButtonDim * 0.8f;
			
			PortraitAuraIcon auraIcon = new PortraitAuraIcon(i, auraTexture, new Rect(auraIconPosX, auraIconPosY, auraIconSize, auraIconSize));
			playerAuraList.Add(auraIcon);
			
			auraSlotNumber++;						
		}
	}
	
	public void  DrawPlayerAuras ()
	{
		for(byte index = 0; index < playerAuraList.Count; index++)
		{
			if(GUI.Button(playerAuraList[index].position, playerAuraList[index].image, GUIStyle.none))
			{
				mainPlayer.auraManager.RemoveAura(playerAuraList[index].index);
			}
		}
	}
	
	public void  UpdateTargetAuras ()
	{
		string spellImage;
		SpellEntry sp;
		SpellIconEntry icon;
		
		targetAuraList.Clear();
		
		if(!mainPlayer.target)
		{
			return;
		}
		
		numberOfAuraTarget = (mainPlayer.target as Unit).auraManager.auras.Count;
		if(numberOfAuraTarget > 32)
		{
			numberOfAuraTarget = 32;
		}
		
		byte auraSlotNumber = 0;
		for(int i = 0; i < numberOfAuraTarget; i++)
		{	
			if(TraitsAura.traitList.Contains(((mainPlayer.target as Unit).auraManager.auras[i] as Aura).spellID))
			{
				if(((mainPlayer.target as Unit).auraManager.auras[i] as Aura).auraDuration <= 0)
				{
					continue;
				}
			}
			
			sp  = dbs.sSpells.GetRecord(((mainPlayer.target as Unit).auraManager.auras[i] as Aura).spellID);
			icon = dbs.sSpellIcons.GetRecord(sp.SpellIconID, false);
			spellImage = icon.Icon;
			
			if (string.IsNullOrEmpty (spellImage))
			{
				spellImage = "noicon.png";
				continue;
			}
			
			spellImage = spellImage.Substring(0, spellImage.LastIndexOf('.'));
			
			Texture2D auraTexture = Resources.Load<Texture2D>("Icons/" + spellImage);
			float auraIconPosX = Screen.width * 0.5401f + AuraButtonDim / 2 * auraSlotNumber;
			float auraIconPosY = PortraitHeight * 0.8f;
			float auraIconSize = AuraButtonDim / 2;
			
			PortraitAuraIcon auraIcon = new PortraitAuraIcon(i, auraTexture, new Rect(auraIconPosX, auraIconPosY, auraIconSize, auraIconSize));
			targetAuraList.Add(auraIcon);
			
			auraSlotNumber++;
		}
	}
	
	public void  DrawTargetAuras ()
	{
		for(byte index = 0; index < targetAuraList.Count; index++)
		{
			GUI.DrawTexture(targetAuraList[index].position, targetAuraList[index].image);
		}
	}
}