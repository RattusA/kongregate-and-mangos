using UnityEngine;
using System.Collections.Generic;

public class targetTab : MonoBehaviour
{
	// THIS SCRIPT contains functionality for two "default spells" : Closest Target (nearest enemy) and Next Target (character tabbing)
	static public float resetInterval = 1;
	static public float sphereRadius = 40;
	
	static private List<BaseObject> charList = new List<BaseObject>();
	static private int index;
	static private float timert = 0;
	static private Vector3 playerPosition; // used to decide remaking charList
	
	static public byte option = 2; 
	// 1 for default ( best !), 2 for forcing enemy select, 3 for forcing friendly/ally select
	// affecting both closestTarget and nearestEnemy functionalities
	
	static public void closestTarget()//nearestEnemy() - > by default will cause selection of closest aggressive or neutral player / mob
	{
		BaseObject toreturn = null;
		
		//GameObject mainScript= GameObject.Find("player"); // "player" is the game main character, the avatar gameobject, YOU ! 
		MainPlayer mainplayer;
		if (WorldSession.player)
		{
			mainplayer =  WorldSession.player;
		}
		else return; // terminates if script attached to avatar was not found in worldsession
		float mindist = 200; // presumably in game meters	
		float dist;
		
		byte n; 
		// enemy player (n==1), then NPC mob (2), friendly player (3) and friendly NPC (4)
		
		// find enemy closest to the main player :
		foreach(BaseObject enemy in WorldSession.realChars.Values) 
		{
			
			//NPC NPC= enemy.GetComponentInChildren<NPC>();  // store the NPC script to a local variable
			// oddly enough, NPC script belongs to a Unity GameObject while BaseObject seem to be something class related within scripts
			// yet this works :D
			
			if (enemy is Unit) // avoiding ojects like apples, chests; objects would cause an IOexception with the following code
			{
				n = decide(enemy, mainplayer); // will set value of n
				
				Unit enemy1 = (Unit)enemy;
				
				if ( enemy1.m_deathState == DeathState.ALIVE && (  ( (option == 1 || option == 2) && (n == 2 || n == 1 || n == 6) )   ||  ( option == 3 &&  (n == 4 || n == 3) ) ) ) 
				{
					dist = Vector3.Distance(enemy.TransformParent.position, mainplayer.TransformParent.position);
					
					if (dist < mindist && dist < 20)
					{
						toreturn = enemy;
						mindist = dist;
					}
				}
			}
		}
		
		if (toreturn)
		{
			//MainPlayer mainScript= mainplayer.GetComponentInChildren<MainPlayer>();
			mainplayer.sendTarget(toreturn); 
			// this is where the target is set, calling a function within the active MainPlayer script attached to the player gameobject
			
			if ( ! mainplayer.isMoving ) // no sense in reorienting avatar while you are moving
			{
				Quaternion rotation;
				Vector3 positionA = new Vector3(toreturn.transform.position.x, 0, toreturn.transform.position.z);
				Vector3 positionB = new Vector3(mainplayer.TransformParent.position.x, 0, mainplayer.TransformParent.position.z);
				Vector3 distanceb = positionA - positionB;
				//if(Vector3.Dot(Vector3.Cross(mainplayer.transformParent.forward,distanceb),Vector3.up) < 0.0f)
				//angl = -angl;
				rotation =  Quaternion.LookRotation(distanceb);
				Rigidbody rigidbody = mainplayer.TransformParent.GetComponent<Rigidbody>();
				rigidbody.rotation = rotation;
				mainplayer.BuildPkt(OpCodes.MSG_MOVE_SET_FACING, rotation);
			}
			//Debug.Log("FactionTemplate  " + toreturn.GetUInt32Value(UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE) + "  ");
		}
	}
	
	
	static public void nextTarget() //objectTab() - > will cause cyclic selection of game Units
	{
		MainPlayer mainplayer;
		if (WorldSession.player)
		{
			mainplayer =  WorldSession.player;
		}
		else return; // terminates if script attached to avatar was not found in worldsession
		
		BaseObject toreturn = null; // at the end we combine the arrays, index, and return this for targeting
		
		//float movedDistance = Vector3.Distance( playerPosition, ( GameObject.Find("player") ).transform.position );
		float movedDistance = Vector3.Distance( playerPosition, mainplayer.TransformParent.position );
		
		if (timert < Time.time || movedDistance > 4) 
			// this recreates the list if more than resetInterval sec passed or the main player moved more than 4 game meters
		{
			// charList is remade during this function call
			timert = Time.time + resetInterval; // resetInterval seconds reset
			
			makeList(mainplayer);	// calls function to (re)make the list
			
			if (charList.Count > 0)
				toreturn = charList[index];
		}
		
		else 
			
		{
			timert = Time.time + resetInterval; // just resets the timer without remaking the list
			// list was not remade
			index++; // points to the next target
			if (index >= charList.Count) // end of list
			{
				index = 0; // cycles back
			}
			
			byte f = 0;	
			while(f == 0)
			{
				if(charList.Count > 0 && charList[index]) // hopefully this makes sure the objects exists
				{
					toreturn = charList[index];
					if(Vector3.Distance( toreturn.TransformParent.position, mainplayer.TransformParent.position ) < sphereRadius )	
					{
						toreturn = charList[index];
						f = 1;
					}
					else
					{
						charList.RemoveAt(index);
						// must not increment index, no index++
						if(index >= charList.Count )
						{
							makeList(mainplayer);
							f = 1;
							if (charList.Count > 0)
							{
								toreturn = charList[index];
							}
						}	
					}
				}
				else 
				{
					index++;
					if(index >= charList.Count)
					{
						makeList(mainplayer);
						if(charList.Count > 0)
						{
							toreturn = charList[index];
						}
						f = 1;
					}
				}
			}
		}
		
		if (toreturn) // the following orients the avatar to the target and sets target
		{
			//FIXME_VAR_TYPE mainScript= (GameObject.Find("player")).GetComponentInChildren<MainPlayer>();
			mainplayer.sendTarget(toreturn); 
			// this is where the target is set, calling a function within the active MainPlayer script attached to the player gameobject
			
			if(!mainplayer.isMoving) // no sense in reorienting your avatar while moving
			{
				Vector3 positionA = new Vector3(toreturn.transform.position.x, 0, toreturn.transform.position.z);
				Vector3 positionB = new Vector3(mainplayer.TransformParent.position.x, 0, mainplayer.TransformParent.position.z);
				Vector3 distanceb = positionA - positionB;
				float angl = Vector3.Angle(mainplayer.TransformParent.forward, distanceb);
				
				mainplayer.TransformParent.Rotate(0, angl, 0);
			}
		}
	}
	
	
	static protected byte decide( BaseObject target, MainPlayer mainplayer) // identifies relation between object and main player
	{
		uint rel = target.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE); //get creature/player faction id
		byte j;
		
		//int playerFaction = ( (GameObject.Find("player")).GetComponentInChildren<MainPlayer>() ).GetUInt32Value(UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE);
		uint playerFaction = mainplayer.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_FACTIONTEMPLATE);
		
		uint[] playerFactionID = new uint[3];
		uint[] friendlyFactionID = new uint[1];
		uint[] neutralFactionID = new uint[3];
//		uint[] agresiveFactionID = new uint[9]; // anyway, at this stage, all others are presumed aggressive
		if ( playerFaction == 1 || playerFaction == 4 || playerFaction == 3 ) // Alliance FACTIONTEMPLATE IDs are 1,4,3
		{
			playerFactionID = new uint[3]{ 1, 4, 3 }; // human, elf, dwarf
			friendlyFactionID = new uint[2]{ 35, 2007 }; // unknown if all questgivers / somesuch are all 35 
			neutralFactionID = new uint[3]{ 0, 25, 32 }; // surely not complete
//			agresiveFactionID = new uint[9]{ 73, 413, 18, 14, 514, 44, 37, 63, 18 }; // and many /any other..
		}
		else
		{
			playerFactionID = new uint[3]{ 1610, 2, 116 }; // dark elf, orc, blooddrak
			friendlyFactionID = new uint[2]{ 35, 2007 }; // unknown if all questgivers / somesuch are all 35 
			neutralFactionID = new uint[3]{ 0, 25, 32 }; // surely not complete
//			agresiveFactionID = new uint[9]{ 73, 413, 18, 14, 514, 44, 37, 63, 18 }; // and many /any other..
		}
		
		
		
		for(j = 0;j<friendlyFactionID.Length;j++)
		{
			if( (target is NPC) && !(target is Player) && friendlyFactionID[j] == rel) //npc, friendly
			{
				return 4;
			}
		}
		
		for(j = 0;j<playerFactionID.Length;j++)
		{
			if( (target is Player) && playerFactionID[j] == rel) // player, friendly
			{
				return 3;
			}
		}
		
		///////// just for option 2 (forced enemy select):
		if (option ==2)	
		{
			for(j = 0;j<neutralFactionID.Length;j++)
			{
				if( (target is NPC) && !(target is Player) && (neutralFactionID[j] == rel) ) //neutral mob
				{
					return 6; // "mob", yet neutral faction; this just prevents returning 2 or 1, only for option 2
				}
			}
		}
		// just for option 2 //////
		
		if( (target is NPC) && !(target is Player) ) // mob
		{
			return 2; 
		}		

		if(target is Player) // player enemy
		{
			return 1; 
		}	
		
		return 0; // if identification failed, presumed object and nothing happens
	}
	
	//static protected function find (  Array lista ,   BaseObject targetul  ) : byte // finds if object is in list
	//{
	//	foreach(var dude in lista) 
	//	{
	//		if (dude == targetul)
	//		 return 1;
	//	}
	//	
	//	return 0;
	//}
	
	static protected List<BaseObject> order_byAngle ( List<BaseObject> lista, MainPlayer mainplayer )
	{
		Vector3 distanceb;
		float angl;
		
		//GameObject mainplayer= GameObject.Find("player"); 
		//Transform playerObjT = (GameObject.Find("player").GetComponentInChildren<MainPlayer>()).transformParent;
		Transform playerObjT = mainplayer.TransformParent;
		
		List<float> angleList = new List<float>(); // saves angles in a list so as to order by them
		foreach(BaseObject obj in lista)
		{
			Vector3 positionA = new Vector3(obj.TransformParent.position.x, 0, obj.TransformParent.position.z);
			Vector3 positionB = new Vector3(playerObjT.position.x, 0, playerObjT.position.z);
			distanceb = positionA - positionB;
			angl = Vector3.Angle(playerObjT.forward, distanceb); // angle judged to player
			
			if (Vector3.Dot(Vector3.Cross(playerObjT.forward, distanceb), Vector3.up) < 0.0f)
				angleList.Add( angl );
			else
				angleList.Add( 360 - angl );
		}
		
		List<BaseObject> tempList = new List<BaseObject>(); // will return this array
		float min_angle = 181;
		int i = 0; //index in original list
		int index_i = i;
		
		while (angleList.Count > 0)
		{
			min_angle = 181;
			i = 0;
			index_i = i;
			foreach(float angle in angleList)
			{		
				if (min_angle < angle)
				{
					min_angle = angle;
					index_i = i;
				}
				i++; // goes through angleList
			}
			angleList.RemoveAt(index_i);
			tempList.Add(lista[index_i]);
			lista.RemoveAt(index_i);
		}
		
		lista.Clear(); // though should already be empty
		return tempList;
	}
	
	static protected void  makeList ( MainPlayer mainplayer  )
	{
		// enemy player (1), NPC mob (2), friendly player (3) and friendly NPC (4) :
		List<BaseObject> tempList1 = new List<BaseObject>();
		List<BaseObject> tempList2 = new List<BaseObject>();
		List<BaseObject> tempList3 = new List<BaseObject>();
		List<BaseObject> tempList4 = new List<BaseObject>(); 
		//Array charList = new Array(); // we compose the above 4 lists after they are arranged
		// this EXISTS as static Global for this script, don't define here !
		// index is also Global
		
		//GameObject mainplayer= GameObject.Find("player"); // "player" is the game main character, the avatar gameobject, YOU ! 
		
		charList.Clear(); // before (re)making it, we clear the list
		
		float dist;
		
		for (byte i = 1; i <= 4; i++) // enemy player (1), then NPC mob (2), friendly player (3) and friendly NPC (4)
		{
			byte n;
			foreach(BaseObject obj in WorldSession.realChars.Values) 
				// we will populate the charList with objects that meet criteria
			{
				if (obj is Unit)
				{
					Unit enemy1 = (Unit)obj; 
					n = decide(obj, mainplayer);
					if (enemy1.m_deathState == DeathState.ALIVE && n == i && ( option == 1 || ( option == 2 && (n == 2 || n == 1) ) || ( option == 3 &&  (n == 4 || n == 3) ) ) )					
					{
						dist = Vector3.Distance(obj.TransformParent.position, mainplayer.TransformParent.position);
						
						if (dist < sphereRadius) // distance for this is bigger than for nearestEnemy()
						{
							switch(i)
							{
							case 1:
								tempList1.Add(obj);
								break;
							case 2:
								tempList2.Add(obj);
								break;
							case 3:
								tempList3.Add(obj);
								break;
							case 4:
								tempList4.Add(obj);
								break;
							}
						}
					}
				}
			}
		}
		
		tempList1 = order_byAngle(tempList1, mainplayer);
		tempList2 = order_byAngle(tempList2, mainplayer);
		tempList3 = order_byAngle(tempList3, mainplayer);
		tempList4 = order_byAngle(tempList4, mainplayer);
		
		charList.AddRange(tempList1);
		charList.AddRange(tempList2);
		charList.AddRange(tempList3);
		charList.AddRange(tempList4); //now charList is ready
		//charList = tempList1.Concat(tempList2, tempList3, tempList4); // now charList is ready
		index = 0; // index is set for start of the list
		playerPosition = mainplayer.TransformParent.position;
	}}
