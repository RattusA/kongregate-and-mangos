﻿using UnityEngine;

public class Effects:MonoBehaviour
{
	public class Effect
	{
		public Rect rect;
		public Color color;
		public float top;
		public float bottom;
	}
	public Effect[] Areas;
}