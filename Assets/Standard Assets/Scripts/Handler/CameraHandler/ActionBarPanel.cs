using UnityEngine;
using System.Collections.Generic;

public class ActionBarPanel
{
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	protected TexturePacker HUDToolkit;
	
	private Rect bottomPanelRect;
	private Rect rightBotPanelRect;
	private Rect rightTopPanelRect;
	private Rect rightExtPanelRect;
	
	private Rect btnLeftRect;
	private Rect btnLeftSmallRect;
	private Rect btnRightRect;
	private Rect btnRightSmallRect;
	private Rect btnLeftExtRect;
	
	private Rect xpBarRect;
	private Rect xpLineRect;			
	
	private Rect switchButtonRect;
	private Rect attackButtonRect;
	private Rect chatButtonRect;
	private Rect chatIconRect;
	
	// Controll flags
	public bool  bottomPanelFlag = true;
	public bool  rightBotFlag = false;
	public bool  rightTopFlag = false;
	public bool  rightExtFlag = false;
	
	private GameObject miniMapCam;
	private MiniMap miniMapScript;
	
	protected uint ACTION_BUTTONS_COUNT = 15;
	
	protected uint BOTTOM_BUTTON_INDEX = 0;
	protected uint RIGHT_BUTTON_INDEX = 6;
	protected uint RIGHT_EXTENDED_BUTTON_INDEX = 11;
	
	private GUITouchableTexture attackBtn;
	
	private Dictionary<uint, GUITouchableTexture> actionSlotsList ;
	private Rect[] actionBtnRect;
	public uint switchMode = 0;
	private bool  showPanelFlag = true;
	
	private float questArrowBeginningTime = 0;
	private float deltaMove;
	private Rect questArrowRect;
	/***************/
	/* CONSTRUCTOR */
	/***************/
	
	public ActionBarPanel ()
	{
		bottomPanelRect = new Rect(sw * 0.3f, sh * 0.83f, sw * 0.7f, sh * 0.17f);
		rightBotPanelRect = new Rect(sw * 0.905f, sh * 0.74f, sw * 0.095f, sh * 0.55f);
		rightTopPanelRect = new Rect(sw * 0.905f, sh * 0.11f, sw * 0.095f, sh * 0.55f);
		rightExtPanelRect = new Rect(sw * 0.832f, sh * 0.35f, sw * 0.095f, sh * 0.70f);
		btnLeftRect = new Rect(sw * 0.266f, sh * 0.884f, sw * 0.047f, sh * 0.116f);
		btnLeftSmallRect = new Rect(sw * 0.851f, sh * 0.913f, sw * 0.047f, sh * 0.058f);
		btnRightRect = new Rect(sw * 0.925f, sh * 0.71f, sw * 0.0793f, sh * 0.075f);
		btnRightSmallRect = new Rect(sw * 0.95f, sh * 0.77f, sw * 0.0425f, sh * 0.078f);
		btnLeftExtRect = new Rect(sw * 0.9f, sh * 0.5f, sw * 0.018f, sh * 0.043f);
		xpBarRect = new Rect(sw * 0.3683f, sh * 0.885f, sw * 0.3f, sh * 0.0072f);
		xpLineRect = new Rect(sw * 0.3683f, sh * 0.885f, sw * 0.5f, sh * 0.0072f);			
		switchButtonRect = new Rect(sw * 0.94f, sh * 0.78f, sw * 0.055f, sw * 0.055f);
		attackButtonRect = new Rect(sw * 0.925f, sh - sw * 0.075f, sw * 0.08f, sw * 0.08f);
		chatButtonRect = new Rect(sw * 0.34f, sh * 0.9f, sw * 0.065f, sw * 0.065f);
		chatIconRect = new Rect(sw * 0.3525f, sh * 0.915f, sw * 0.035f, sw * 0.035f);
		actionSlotsList = new Dictionary<uint, GUITouchableTexture>();
		actionBtnRect = new Rect[ACTION_BUTTONS_COUNT];
		deltaMove = sh*0.02f;

		FindMinimap();
		LoadHUDData();
		InitDefaultActionIcons();
	}
	
	private void  FindMinimap ()
	{		
		miniMapCam = GameObject.Find("MinimapCamera");
		if(miniMapCam)
		{
			miniMapScript = miniMapCam.GetComponent<MiniMap>();
		}
	}
	
	
	/**********************************************/
	/* Load all Resources and init default values */
	/**********************************************/
	
	public void  LoadHUDData ()
	{
		HUDToolkit = new TexturePacker("HUDToolkit");
		
		attackBtn = new GUITouchableTexture();
		attackBtn.CreateButton(attackButtonRect, "GUI/InGame/AttackImage", 0);
		
		if(!miniMapCam)
		{
			FindMinimap();
		}
	}
	
	private void  InitDefaultActionIcons ()
	{
		InitBottonSlotsPosition();
		InitRightSlotsPosition();
		InitRightExtSlotsPosition();
		
		for(uint index = BOTTOM_BUTTON_INDEX; index < ACTION_BUTTONS_COUNT; index++)
		{
			GUITouchableTexture textureButton = new GUITouchableTexture();
			textureButton.CreateButton(actionBtnRect[index], "Icons/tab_solo", 0);
			actionSlotsList[index] = textureButton;
		}
	}
	
	private void  InitBottonSlotsPosition ()
	{
		for(uint index = BOTTOM_BUTTON_INDEX; index < RIGHT_BUTTON_INDEX; index++)
		{
			float xPos = sw * 0.43f + (sw * 0.08f * index);
			actionBtnRect[index] = new Rect(xPos, sh * 0.9f, sw * 0.061f, sw * 0.061f);
		}
	}
	
	private void  InitRightSlotsPosition ()
	{
		for(uint index = RIGHT_BUTTON_INDEX; index < RIGHT_EXTENDED_BUTTON_INDEX; index++)
		{
			uint newIndex = index - RIGHT_BUTTON_INDEX;
			float yPos = sh * 0.155f + (sh * 0.12f * newIndex);
			if(index > (RIGHT_BUTTON_INDEX + 1))
			{
				yPos += sw * 0.01f;
			}
			
			actionBtnRect[index] = new Rect(sw * 0.935f, yPos, sw * 0.061f, sw * 0.061f);
		}
	}
	
	private void  InitRightExtSlotsPosition ()
	{
		for(uint index = RIGHT_EXTENDED_BUTTON_INDEX; index < ACTION_BUTTONS_COUNT; index++)
		{
			uint newIndex = index - RIGHT_EXTENDED_BUTTON_INDEX;
			float yPos = sh * 0.39f + (sh * 0.12f * newIndex);
			actionBtnRect[index] = new Rect(sw * 0.86f, yPos, sw * 0.061f, sw * 0.061f);
		}
	}
	
	public void  RefreshSlots ()
	{
		MainPlayer mainPlayer = WorldSession.player;
		if(HUDToolkit == null)
		{
			LoadHUDData();
		}
		else
		{
			ClearActionSlotList();
		}
		
		showPanelFlag = PlayerPrefs.GetInt("showWoodenBackGround", 1) > 0 ? true : false;
		
		for(uint index = BOTTOM_BUTTON_INDEX; index < ACTION_BUTTONS_COUNT; index++)
		{
			ActionBarSlot actionSlotBar;
			uint slotNumber = index + ACTION_BUTTONS_COUNT * switchMode;
			
			if(mainPlayer.pet && index >= RIGHT_EXTENDED_BUTTON_INDEX && switchMode == 0)
			{
				actionSlotBar = mainPlayer.actionBarManager.GetSlotPetObject(index - RIGHT_EXTENDED_BUTTON_INDEX);
			}
			else
			{
				actionSlotBar = mainPlayer.actionBarManager.GetSlotObject((int)slotNumber);
			}
			
			GUITouchableTexture textureButton = new GUITouchableTexture();
			if((actionSlotBar == null) || (actionSlotBar.objectID == 0 && actionSlotBar.slotType == 0))
			{
				textureButton.CreateButton(actionBtnRect[index], "Icons/tab_solo", 0);
				actionSlotsList[index] = textureButton;
			}
			else
			{
				string slotImage = mainPlayer.interf.RefreshIcon((int)(index + ACTION_BUTTONS_COUNT * switchMode));
				slotImage = slotImage.Substring(0, slotImage.LastIndexOf('.'));
				
				textureButton.CreateButton(actionBtnRect[index], "Icons/" + slotImage, actionSlotBar.objectID);
				actionSlotsList[index] = textureButton;
			}
		}
	}
	
	
	/************************/
	/* Unload all Resources */
	/************************/
	
	private void  DestroyHUDData ()
	{
		ClearActionSlotList();
		
		HUDToolkit.Destroy();
		HUDToolkit = null;
		
		attackBtn.DestroyTexture();
		attackBtn = null;
	}
	
	private void  ClearActionSlotList ()
	{
		if(actionSlotsList.Count == 0)
		{
			return;	
		}
		
		for(uint index = BOTTOM_BUTTON_INDEX; index < ACTION_BUTTONS_COUNT; index++)
		{
			actionSlotsList[index].DestroyTexture();
			actionSlotsList[index] = null;
		}
		actionSlotsList.Clear();
	}
	
	private bool CheckResourcesState ()
	{
		if(WorldSession.player.interf == null)
		{
			return true;
		}
		
		if(WorldSession.interfaceManager.hideInterface || ButtonOLD.menuState)
		{
			if(HUDToolkit != null)
			{
				DestroyHUDData();
			}
			return true;
		}
		else
		{
			if(HUDToolkit == null || actionSlotsList.Count == 0)
			{
				RefreshSlots();
			}
			return false;
		}
	}
	
	/*********************/
	/* Draw Action Panel */
	/*********************/
	
	public void  DrawSpellHUD ()
	{
		if(CheckResourcesState())
		{
			return;
		}
		
		// Right panel
		if(rightBotFlag)
		{
			if(rightTopFlag)
			{
				if(showPanelFlag)
				{
					HUDToolkit.DrawTexture(rightTopPanelRect, "RighrWoodPanel.png");
				}
			}
			
			if(showPanelFlag)
			{
				HUDToolkit.DrawTexture(rightBotPanelRect, "RighrWoodPanel.png");
			}
			
			HUDToolkit.DrawTexture(btnRightRect, "RightWoodButton.png");
			if(GUI.Button(btnRightRect, "", GUIStyle.none))
				ChangeRightPanelState();
			
			if(rightExtFlag && !ButtonOLD.chatState)
			{
				if(showPanelFlag)
				{
					HUDToolkit.DrawTexture(rightExtPanelRect, "RightExtWoodPanel.png");
				}
				
				ShowRightExtPanelIcons();
			}
			
			ShowRightPanelIcons();
			
			if(!ButtonOLD.chatState)
			{
				HUDToolkit.DrawTexture(btnLeftExtRect, "RightExtWoodButton.png");
				if(GUI.Button(btnLeftExtRect, "", GUIStyle.none))
					ChangeRightExtPanelState();
			}
			
			HUDToolkit.DrawTexture(switchButtonRect, "SwitchButton.png");
			if(GUI.Button(switchButtonRect, "", GUIStyle.none))
				SwitchPanelState();
		}
		else
		{
			if(bottomPanelFlag)
			{
				if(showPanelFlag)
				{
					HUDToolkit.DrawTexture(rightBotPanelRect, "RighrWoodPanel.png");
				}
				
				HUDToolkit.DrawTexture(btnRightRect, "RightWoodButton.png");
				if(GUI.Button(btnRightRect, "", GUIStyle.none))
					ChangeRightPanelState();
				
				HUDToolkit.DrawTexture(switchButtonRect, "SwitchButton.png");
				if(GUI.Button(switchButtonRect, "", GUIStyle.none))
					SwitchPanelState();
			}
			else
			{
				HUDToolkit.DrawTexture(btnRightSmallRect, "RightSmallWoodButton.png");
				if(GUI.Button(btnRightSmallRect, "", GUIStyle.none))
					ChangeRightPanelState();
			}
		}
		
		if(bottomPanelFlag)
		{
			if(showPanelFlag)
			{
				HUDToolkit.DrawTexture(bottomPanelRect, "BottomWoodPanel.png");
			}
			
			HUDToolkit.DrawTexture(btnLeftRect, "LeftWoodButton.png");
			if(GUI.Button(btnLeftRect, "", GUIStyle.none))
			{
				ChangeBottomPanelState();
			}
			
			float xpBarWidth = sw * 0.5f * WorldSession.player.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_XP) / 
				(0.001f + WorldSession.player.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_NEXT_LEVEL_XP));
			xpBarRect.width = xpBarWidth;
			
			ShowBottomPanelIcons();
			HUDToolkit.DrawTexture(xpLineRect, "XpBorder.png");
			HUDToolkit.DrawTexture(xpBarRect, "XpBar.png");
		}
		else
		{
			HUDToolkit.DrawTexture(btnLeftSmallRect, "LeftSmallWoodButton.png");
			if(GUI.Button(btnLeftSmallRect, "", GUIStyle.none))
				ChangeBottomPanelState();
		}
		
		ShowaAttackButton();
		DrawSpellCooldown();
	}
	
	public void  HideTopRightPanel ()
	{
		if(rightTopFlag)
		{
			rightTopFlag = false;
			btnRightRect = new Rect(sw * 0.925f, sh * 0.33f, sw * 0.0793f, sh * 0.075f);
		}
	}
	
	private void  ChangeBottomPanelState ()
	{
		bottomPanelFlag = !bottomPanelFlag;
		if(bottomPanelFlag && !rightBotFlag)
		{
			btnRightRect = new Rect(sw * 0.925f, sh * 0.71f, sw * 0.0793f, sh * 0.075f);
			rightBotPanelRect = new Rect(sw * 0.905f, sh * 0.74f, sw * 0.095f, sh * 0.55f);
		}
	}
	
	private void  ChangeRightPanelState ()
	{
		if(!rightBotFlag)
		{
			btnRightRect = new Rect(sw * 0.925f, sh * 0.33f, sw * 0.0793f, sh * 0.075f);
			rightBotPanelRect = new Rect(sw * 0.905f, sh * 0.36f, sw * 0.095f, sh * 0.55f);
			rightBotFlag = true;
		}
		else
		{
			if(!rightTopFlag)
			{
				if(miniMapScript == null)
				{
					FindMinimap();
				}
				if(miniMapScript)
				{
					miniMapScript.HideMiniMap();
				}
				
				btnRightRect = new Rect(sw * 0.925f, sh * 0.08f, sw * 0.0793f, sh * 0.075f);
				rightTopFlag = true;
			}
			else
			{
				rightBotFlag = false;
				rightTopFlag = false;
				btnRightRect = new Rect(sw * 0.925f, sh * 0.71f, sw * 0.0793f, sh * 0.075f);
				rightBotPanelRect = new Rect(sw * 0.905f, sh * 0.74f, sw * 0.095f, sh * 0.55f);
			}
		}
	}
	
	private void  ChangeRightExtPanelState ()
	{
		rightExtFlag = !rightExtFlag;
		if(rightExtFlag)
			btnLeftExtRect = new Rect(sw * 0.83f, sh * 0.5f, sw * 0.018f, sh * 0.043f);
		else
			btnLeftExtRect = new Rect(sw * 0.9f, sh * 0.5f, sw * 0.018f, sh * 0.043f);
	}
	
	/**
	 *	Init 
	 */
	private void  ShowBottomPanelIcons ()
	{
		for(uint index = BOTTOM_BUTTON_INDEX; index < RIGHT_BUTTON_INDEX; index++)
		{
			actionSlotsList[index].DrawTouchebleTexture();
			if(actionSlotsList[index].GetAction())
			{
				Action(index);
			}
		}
		
		HUDToolkit.DrawTexture(chatButtonRect, "ChatButton.png");
		if(GUI.Button(chatButtonRect, "", GUIStyle.none))
			StartChat();
		
		DrawChatIcon();
		
		//		DrawChatQuestArrow();
	}
	
	private void  DrawChatIcon ()
	{
		if(ButtonOLD.chatState)
		{
			HUDToolkit.DrawTexture(chatIconRect, "minus.png");
		}
		else
		{
			HUDToolkit.DrawTexture(chatIconRect, "plus.png");
		}
	}
	
	public void  DrawChatQuestArrow ()
	{
		if((WorldSession.player.tutorialInstance.tutorialStep != TutorialStages.TUTORIAL_OPEN_CHAT) 
		   && (WorldSession.player.tutorialInstance.tutorialStep != TutorialStages.TUTORIAL_CLOSE_CHAT))
		{
			return;
		}
		questArrowBeginningTime = (questArrowBeginningTime == 0) ? Time.realtimeSinceStartup : questArrowBeginningTime;
		float deltaMove = sh*0.04f;
		float deltaTime = (Time.realtimeSinceStartup - questArrowBeginningTime)%0.5f; // 0.5f of seconds
		questArrowRect = new Rect(((chatButtonRect.x+(chatButtonRect.width*0.5f))-((sw*0.08f)*0.5f)), 
		                          (chatButtonRect.y-(sh*0.09f)-deltaMove+(deltaMove*deltaTime)), 
		                          (sw*0.08f), 
		                          (sh*0.1f));
		//		HUDToolkit.DrawTexture(questArrowRect, "ArrowDown128.png");
		GUI.Window(0, questArrowRect, QuestArrow, "", GUIStyle.none);
		GUI.BringWindowToBack(-1);
	}
	
	private void  QuestArrow (int windowID)
	{
		HUDToolkit.DrawTexture(new Rect(0, 0, questArrowRect.width, questArrowRect.height), "ArrowDown128.png");
	}
	
	private void  ShowRightPanelIcons ()
	{
		for(uint index = RIGHT_BUTTON_INDEX; index < RIGHT_EXTENDED_BUTTON_INDEX; index++)
		{
			if(index <= 7 && !rightTopFlag)
			{
				continue;
			}
			
			actionSlotsList[index].DrawTouchebleTexture();
			if(actionSlotsList[index].GetAction())
			{
				Action(index);
			}
		}
	}
	
	private void  ShowRightExtPanelIcons ()
	{
		for(uint index = RIGHT_EXTENDED_BUTTON_INDEX; index < ACTION_BUTTONS_COUNT; index++)
		{
			actionSlotsList[index].DrawTouchebleTexture();
			if(actionSlotsList[index].GetAction())
			{
				Action(index);
			}
		}
	}
	
	private void  ShowaAttackButton ()
	{
		attackBtn.DrawTouchebleTexture();
		if(attackBtn.GetAction())
		{
			WorldSession.player._Attack();
		}
	}
	
	private void  DrawSpellCooldown ()
	{
		MainPlayer mainPlayer = WorldSession.player;
		if(WorldSession.player.cooldownManager != null)
		{
			for(uint index = BOTTOM_BUTTON_INDEX; index < ACTION_BUTTONS_COUNT; index++)
			{
				if(index < 6 && !bottomPanelFlag)
					continue; 
				else if(index >= 6 && index < 8 && !rightTopFlag)
					continue;
				else if(index >= 8 && index < 11 && !rightBotFlag)
					continue;
				else if(index >= RIGHT_EXTENDED_BUTTON_INDEX && (!rightExtFlag || ButtonOLD.chatState))
					continue;
				
				ActionBarSlot actionSlot;
				
				if(index >= 11 && mainPlayer.pet)
				{ 
					actionSlot = mainPlayer.actionBarManager.GetSlotPetObject(index - RIGHT_EXTENDED_BUTTON_INDEX);
				}
				else
				{
					actionSlot = mainPlayer.actionBarManager.GetSlotObject((int)(index + ACTION_BUTTONS_COUNT * switchMode));
				}
				
				if(actionSlot != null)
				{
					Spell spell = null;
					switch(actionSlot.slotType)
					{
					case ActionButtonType.ACTION_BUTTON_SPELL:
						spell = SpellManager.GetSpell(actionSlot.objectID);
						break;
					case ActionButtonType.ACTION_BUTTON_MACRO:
						spell = null;
						break;
					case ActionButtonType.ACTION_BUTTON_ITEM:
						spell = null;
						Item item = mainPlayer.itemManager.GetItemByID(actionSlot.objectID);
						if(item != null)
						{
							ItemSpell itemSpell = item.GetOnActiveSpell();
							if(itemSpell != null)
							{
								uint itemSpellID = item.GetOnActiveSpell().ID;
								spell = SpellManager.GetSpell(itemSpellID);
							}
						}	
						break;
					}
					
					if(spell != null)
					{
						Cooldown cooldown = mainPlayer.cooldownManager.GetCooldown(spell);
						if(cooldown != null)
						{
							float ratio = cooldown.currentTime / cooldown.maxTime;
							actionSlotsList[index].SetSpellCooldown(ratio);
							continue;
						}
						else
						{
							actionSlotsList[index].SetSpellCooldown(0);
						}
					}
				}
			}
		}
	}
	
	public void  StartChat ()
	{
		ButtonOLD.chatState =!ButtonOLD.chatState;
		
		if(!ButtonOLD.chatState && WorldSession.player.chatOpend)
		{
			GUI.FocusControl ("");
			WorldSession.player.chatOpend = false;
		}
		
		if(ButtonOLD.chatState && miniMapScript)
			miniMapScript.HideMiniMap();
		
		///for tutorial
		if (WorldSession.player.tutorialInstance)
			if ((WorldSession.player.tutorialInstance.tutorialStep == TutorialStages.TUTORIAL_OPEN_CHAT && 
			     ButtonOLD.chatState) || 
			    (WorldSession.player.tutorialInstance.tutorialStep == TutorialStages.TUTORIAL_CLOSE_CHAT && 
			 !ButtonOLD.chatState)) 			
				WorldSession.player.tutorialInstance.CompleteTutorialStage();	
	}	
	
	public void  Action ( uint slotNumber )
	{
		MainPlayer mainPlayer = WorldSession.player;
		if(mainPlayer.chatOpend)
		{
			return;
		}
		
		uint slotPos = slotNumber + ACTION_BUTTONS_COUNT * switchMode;
		
		if(mainPlayer.pet != null && slotNumber >= RIGHT_EXTENDED_BUTTON_INDEX && switchMode == 0)
		{
			mainPlayer.actionBarManager.DoPetAction((int)(slotNumber - RIGHT_EXTENDED_BUTTON_INDEX), mainPlayer.target);
		}
		else
		{
			mainPlayer.actionBarManager.DoAction(slotPos, mainPlayer.target);
		}
	}
	
	private void  SwitchPanelState ()
	{
		switchMode++;
		if(switchMode >= 4)
			switchMode = 0;
		
		ButtonOLD.switchMode = (int)switchMode;
		RefreshSlots();
	}
}