﻿
public class ButtonInfo
{
	public int positionX;
	public int positionY;
	public int sizeX;
	public int sizeY;
	
	public ButtonInfo (float posX, float posY, float szX, float szY)
	{
		positionX = (int)posX;
		positionY = (int)posY;
		sizeX = (int)szX;
		sizeY = (int)szY;
	}

	public ButtonInfo (int posX, int posY, int szX, int szY)
	{
		positionX = posX;
		positionY = posY;
		sizeX = szX;
		sizeY = szY;
	}
}