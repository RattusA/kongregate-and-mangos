using UnityEngine;
using System.Collections;

public class PetFeeding
{
	private MainPlayer mainPlayer;
	private UIText buttonStyle;
	private UIText warningText;
	
	private string petInfoTxt = "";
	private string petName = "";
	
	private bool  petFeedingFlag;
	
	private UIAbsoluteLayout petInfoContainer;
	
	private UIButton decorationBackground;
	private UIButton petInfoBackground;
	private UIButton feedButton;
	private UIButton closeButton;
	
	private UITextInstance feedButtonText;
	private UITextInstance closeButtonText;
	private UITextInstance petInfoText;
	
	public PetFeeding ( MainPlayer player  )
	{
		mainPlayer = player;
		buttonStyle = mainPlayer.interf.text1;
		
		petInfoContainer = new UIAbsoluteLayout();
		
		petFeedingFlag = false;
	}
	
	public void  Init ()
	{
		if(mainPlayer.pet == null)
		{
			ErrorMessage.ShowError("Pet not selected!");
			
			ShowPlayerInventory();
			return;
		}
		else if(petFeedingFlag == true)
		{
			return;
		}
		else
		{			
			GetPetInfo();
		}
		
		ShowWindow();
	}
	
	private void  ShowWindow ()
	{
		petFeedingFlag = true;
		
		int positionX = 0;
		int positionY = 0;
		float width = 0;
		float height = 0;
		
		// Общий фон
		positionX = (int)(Screen.width * 0.05f);
		positionY = (int)(Screen.height * 0.2f);
		width = Screen.width * 0.4f;
		height = Screen.height * 0.6f;
		
		decorationBackground = UIButton.create(UIPrime31.myToolkit4, "popup_background.png", "popup_background.png", positionX, positionY, 2);
		decorationBackground.setSize(width, height);
		
		// Фон для текста
		positionX = (int)(decorationBackground.position.x + decorationBackground.width * 0.1f);
		positionY = (int)(-decorationBackground.position.y + decorationBackground.height * 0.05f);
		width = decorationBackground.width * 0.8f;
		height = decorationBackground.height * 0.7f;
		
		petInfoBackground = UIButton.create(UIPrime31.myToolkit6, "paper_frame.png", "paper_frame.png", positionX, positionY, 1);
		petInfoBackground.setSize(width, height);
		
		// Кнопка кормления		
		positionX = (int)(decorationBackground.position.x + decorationBackground.width * 0.15f);
		positionY = (int)(-decorationBackground.position.y + decorationBackground.height * 0.85f);
		width = decorationBackground.width * 0.25f;
		height = decorationBackground.height * 0.1f;
		
		feedButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_selected.png", positionX, positionY, 1);
		feedButton.setSize(width, height);
		feedButton.onTouchDown += FeedDelegate;
		
		// Кнопка закрытия окна
		positionX = (int)(decorationBackground.position.x + decorationBackground.width * 0.60f);
		positionY = (int)(-decorationBackground.position.y + decorationBackground.height * 0.85f);
		width = decorationBackground.width * 0.25f;
		height = decorationBackground.height * 0.1f;
		
		closeButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_selected.png", positionX, positionY, 1);
		closeButton.setSize(width, height);
		closeButton.onTouchDown += CloseDelegate;
		
		petInfoContainer.addChild(decorationBackground, petInfoBackground, feedButton, closeButton);
		
		
		// Надпись на кнопке кормления
		positionX = (int)(feedButton.position.x + feedButton.width / 2f);
		positionY = (int)(-feedButton.position.y + feedButton.height / 2f);
		
		feedButtonText = buttonStyle.addTextInstance("Feed", positionX, positionY, 0.4f , -6 , Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		// Надпись на кнопке закрытия окна
		positionX = (int)(closeButton.position.x + closeButton.width / 2f);
		positionY = (int)(-closeButton.position.y + closeButton.height / 2f);
		
		closeButtonText = buttonStyle.addTextInstance("Close", positionX, positionY, 0.4f , -6 , Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		positionX = (int)(petInfoBackground.position.x + petInfoBackground.width * 0.05f);
		positionY = (int)(-petInfoBackground.position.y + petInfoBackground.height * 0.05f);
		
		petInfoText = buttonStyle.addTextInstance(petInfoTxt, positionX, positionY, 0.3f , -6 , Color.white, UITextAlignMode.Left, UITextVerticalAlignMode.Top);
	}
	
	private void  CloseDelegate (UIButton sender)
	{
		petInfoContainer.removeAllChild(true);
		
		decorationBackground.destroy();
		petInfoBackground.destroy();
		
		feedButton.destroy();
		closeButton.destroy();
		
		feedButtonText.clear();
		closeButtonText.clear();
		petInfoText.clear();
		
		ShowPlayerInventory();
		
		petFeedingFlag = false;
	}
	
	private void  ShowPlayerInventory ()
	{
		mainPlayer.interf.resetInventoryStatsMode(2);		
		mainPlayer.interf.inventoryManager.switchInventoryStatsMode(null);
		mainPlayer.interf.inventoryManager.inventoryContainer.hidden = false;
	}
	
	private void  GetPetInfo ()
	{
		string petId = (mainPlayer.petFamily).ToString();
		petName = (string)DefaultTabel.petFamilyName[petId];
		
		petInfoTxt = "";
		
		petInfoTxt += "Pet Name : " + mainPlayer.pet.Name + "\n";
		petInfoTxt += "Family : " + petName + "\n";
		petInfoTxt += "Level : " + mainPlayer.petLvl + "\n";
		petInfoTxt += "Happines : " + mainPlayer.petHappines + "\n";
		petInfoTxt += "Food : ";
		
		IDictionary dictionary = (IDictionary)DefaultTabel.petFoodName[petName];
		string food = "";
		
		var dictkey= dictionary.Keys;
		
		foreach(string foodNumber in dictkey)
		{			
			food = (string)dictionary[foodNumber];			
			petInfoTxt += food;
		}		
	}
	
	private void  FeedDelegate (UIButton sender)
	{
		if(mainPlayer.itemSelected == null)
		{
			ErrorMessage.ShowError("Food item not selected!");
			return;
		}
		
		uint spellID = 6991;
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_CAST_SPELL);
		byte aux8 = 0;

		pkt.Append(aux8);
		pkt.Append(ByteBuffer.Reverse(spellID));
		pkt.Append(aux8);
		
		if(mainPlayer.itemSelected != null)
		{
			SpellCastTargets targets= new SpellCastTargets();	
			pkt.Append(targets.WriteItem(mainPlayer.itemSelected.guid));
		}
		
		RealmSocket.outQueue.Add(pkt);
		
		CloseDelegate(sender);
	}
	
	public void  Destroy ()
	{
		if(petInfoContainer._children.Count > 0)
		{
			CloseDelegate(null);
		}
	}
}