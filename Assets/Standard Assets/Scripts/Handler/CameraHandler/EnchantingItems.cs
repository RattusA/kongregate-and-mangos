using UnityEngine;

public class EnchantingItems
{
	private MainPlayer mainPlayer;
	private ButtonOLD button;
	
	private UIToolkit mixToolkit2;
	private UIText text1;
	private UIText text2;
	private UIText text3;
	
	private Item enchantItem;
	private Item enchantReagent;
	
	private UIAbsoluteLayout useItemContainer;
	
	private UISprite useItemBackground;
	private UISprite decorationTextureRound;
	private UISprite decorationTextureLine;
	
	private UIButton enchantButton;
	private UIButton cancelButton;
	
	private UITextInstance enchantButtonText;
	private UITextInstance cancelButtonText;
	private UITextInstance useItemQuestionText;
	
	private UIAbsoluteLayout warningContainer;
	
	private UISprite warningBackground;
	private UISprite warningTextureRound;
	private UISprite warningTextureLine;
	
	private UIButton closeButton;
	
	private UITextInstance closeButtonText;
	private UITextInstance warningText;
	
	public EnchantingItems ( MainPlayer player  )
	{
		mainPlayer = player;
		button = mainPlayer.interf;
		
		mixToolkit2 = UIPrime31.myToolkit3;
		text1 = button.text1;
		text2 = button.text2;
		text3 = button.text3;
		
		useItemContainer = new UIAbsoluteLayout();
		warningContainer = new UIAbsoluteLayout();
	}
	
	public void  Init ()
	{
		string errorText;
		
		if(mainPlayer.itemSelected == null)
		{
			errorText = "Re-forge ingredien\nnot selected!";
			ShowWarningWindow(errorText);
			
			return;
		}
		
		if(mainPlayer.itemSelected.classType != 0 && (mainPlayer.itemSelected.subClass != 6 || mainPlayer.itemSelected.subClass != 8))
		{
			errorText = "Selected item is not\na re-forge ingredient!";
			ShowWarningWindow(errorText);
			
			return;
		}
		
		uint level = mainPlayer.GetLevel();
		
		if(mainPlayer.itemSelected.reqLvl > level)
		{
			errorText = "This re-forge ingredient\nrequires higher\ncharacter level!";
			ShowWarningWindow(errorText);
			
			return;
		}
		
		enchantReagent = mainPlayer.itemSelected;
		mainPlayer.itemSelected = null;
		
		ShowWindow("Select slot to re-forge", 1);
	}			
	
	// mode = 1 - forge window
	// mode = 2 - is have enchant warning 
	private void  ShowWindow ( string message ,   byte mode  )
	{
		int useItemBackgroundPositionX = Screen.width / 2;
		int useItemBackgroundPositionY = Screen.height / 4;
		int useItemBackgroundSizeX = Screen.width / 2;
		int useItemBackgroundSizeY = Screen.height / 3;
		
		useItemBackground = UIPrime31.myToolkit4.addSprite("popup_background.png", useItemBackgroundPositionX, useItemBackgroundPositionY, -3);
		useItemBackground.setSize(useItemBackgroundSizeX, useItemBackgroundSizeY);
		
		
		int decorationTextureRoundPositionX = useItemBackgroundPositionX + useItemBackgroundSizeX / 30;
		int decorationTextureRoundPositionY = useItemBackgroundPositionY + useItemBackgroundSizeY / 5;
		float decorationTextureRoundSizeX = useItemBackgroundSizeY * 0.4f;
		float decorationTextureRoundSizeY = useItemBackgroundSizeY * 0.4f;
		
		decorationTextureRound = mixToolkit2.addSprite("decoration_02.png", decorationTextureRoundPositionX, decorationTextureRoundPositionY, -4);
		decorationTextureRound.setSize(decorationTextureRoundSizeX, decorationTextureRoundSizeY);
		
		
		int decorationTextureLinePositionX = (int)(decorationTextureRoundPositionX + decorationTextureRoundSizeX * 0.8f);
		int decorationTextureLinePositionY = (int)(decorationTextureRoundPositionY + decorationTextureRoundSizeY * 0.95f);
		float decorationTextureLineSizeX = useItemBackgroundSizeX * 0.8f;
		float decorationTextureLineSizeY = useItemBackgroundSizeY * 0.1f;
		
		decorationTextureLine = mixToolkit2.addSprite("decoration_01.png", decorationTextureLinePositionX, decorationTextureLinePositionY, -4);
		decorationTextureLine.setSize(decorationTextureLineSizeX, decorationTextureLineSizeY);
		
		// Buttons coordinates
		int enchantButtonPositionX = (int)(useItemBackgroundPositionX + useItemBackgroundSizeX * 0.15f);
		int enchantButtonPositionY = (int)(useItemBackgroundPositionY + useItemBackgroundSizeY * 0.68f);
		
		int cancelButtonPositionX = (int)(useItemBackgroundPositionX + useItemBackgroundSizeX * 0.58f);
		int cancelButtonPositionY = (int)(useItemBackgroundPositionY + useItemBackgroundSizeY * 0.68f);
		
		int ButtonSizeX = useItemBackgroundSizeX / 3;
		int ButtonSizeY = useItemBackgroundSizeY / 5;
		
		// Text coordinates
		
		int enchantButtonTextPositionX = enchantButtonPositionX + ButtonSizeX / 2;
		int enchantButtonTextPositionY = enchantButtonPositionY + ButtonSizeY / 2;
		
		int cancelButtonTextPositionX = cancelButtonPositionX + ButtonSizeX / 2;
		int cancelButtonTextPositionY = cancelButtonPositionY + ButtonSizeY / 2;
		
		float useItemQuestionTextPositionX = decorationTextureRoundPositionX + decorationTextureRoundSizeX * 1.2f;
		float useItemQuestionTextPositionY = decorationTextureRoundPositionY + decorationTextureRoundSizeY / 12f;
		
		// Create Buttons
		enchantButton = UIButton.create(mixToolkit2, "3_selected.png", "3_selected.png", enchantButtonPositionX, enchantButtonPositionY, -4);
		enchantButton.setSize(ButtonSizeX, ButtonSizeY); 
		enchantButton.centerize();
		
		string leftButtonText = "";
		switch(mode)
		{
		case 1:
			enchantButton.onTouchDown += EnchantItemDelegate;
			leftButtonText = "Re-forge";
			break;
		case 2:
			enchantButton.onTouchDown += RealyEnchantItemDelegate;
			leftButtonText = "Yes";
			break;
		}
		
		cancelButton = UIButton.create(mixToolkit2, "3_selected.png", "3_selected.png", cancelButtonPositionX, cancelButtonPositionY, -4);
		cancelButton.setSize(ButtonSizeX, ButtonSizeY); 
		cancelButton.centerize();
		cancelButton.onTouchDown += CancelDelegate;
		
		// Create Text for Buttons
		enchantButtonText = text1.addTextInstance(leftButtonText, enchantButtonTextPositionX, enchantButtonTextPositionY, 0.4f , -6 , Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		cancelButtonText = text1.addTextInstance("Cancel", cancelButtonTextPositionX, cancelButtonTextPositionY, 0.4f ,-6 ,Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		useItemQuestionText = text3.addTextInstance(message, useItemQuestionTextPositionX, useItemQuestionTextPositionY, 0.4f , -6, Color.white, UITextAlignMode.Left, UITextVerticalAlignMode.Top);
		
		useItemContainer.addChild(useItemBackground, decorationTextureRound, decorationTextureLine, enchantButton, cancelButton);
	}	
	
	private void  RealyEnchantItemDelegate (UIButton sender)
	{
		CancelDelegate(sender);
		SendInfo();
	}
	
	private void  EnchantItemDelegate (UIButton sender)
	{
		enchantItem = mainPlayer.itemSelected;
		
		CancelDelegate(sender);
		
		string errorText;
		
		if(enchantItem == null)
		{
			errorText = "Incorrect slot for chosen\nre-forge ingredient!";
			ShowWarningWindow(errorText);
		}
		else
		{
			if(enchantItem.HasAnyEnchant())
			{
				errorText = "This item is already Re-forged.\nDo you want to overwrite \nprevious Re-forge?";
				ShowWindow(errorText, 2);
			}
			else
				SendInfo();
		}
	}
	
	void  Error ()
	{		
		string errorText;
		
		errorText = "Incorrect slot for chosen\nre-forge ingredient!";
		ShowWarningWindow(errorText);
	}
	
	private void  CancelDelegate (UIButton sender)
	{
		useItemContainer.removeAllChild(true);
		
		useItemBackground.destroy();
		decorationTextureRound.destroy();
		decorationTextureLine.destroy();
		
		enchantButton.destroy();
		cancelButton.destroy();
		
		enchantButtonText.clear();
		cancelButtonText.clear();
		useItemQuestionText.clear();
	}
	
	private void  ShowWarningWindow ( string warningMessage  )
	{
		int warningBackgroundPositionX = Screen.width / 2;
		int warningBackgroundPositionY = Screen.height / 4;
		int warningBackgroundSizeX = Screen.width / 2;
		int warningBackgroundSizeY = Screen.height / 3;
		
		warningBackground = UIPrime31.myToolkit4.addSprite("popup_background.png", warningBackgroundPositionX, warningBackgroundPositionY, -3);
		warningBackground.setSize(warningBackgroundSizeX, warningBackgroundSizeY);
		
		
		int warningTextureRoundPositionX = warningBackgroundPositionX + warningBackgroundSizeX / 30;
		int warningTextureRoundPositionY = warningBackgroundPositionY + warningBackgroundSizeY / 5;
		float warningTextureRoundSizeX = warningBackgroundSizeY * 0.4f;
		float warningTextureRoundSizeY = warningBackgroundSizeY * 0.4f;
		
		warningTextureRound = mixToolkit2.addSprite("decoration_02.png", warningTextureRoundPositionX, warningTextureRoundPositionY, -4);
		warningTextureRound.setSize(warningTextureRoundSizeX, warningTextureRoundSizeY);
		
		
		int warningTextureLinePositionX = (int)(warningTextureRoundPositionX + warningTextureRoundSizeX * 0.8f);
		int warningTextureLinePositionY = (int)(warningTextureRoundPositionY + warningTextureRoundSizeY * 0.95f);
		float warningTextureLineSizeX = warningBackgroundSizeX * 0.8f;
		float warningTextureLineSizeY = warningBackgroundSizeY * 0.1f;
		
		warningTextureLine = mixToolkit2.addSprite("decoration_01.png", warningTextureLinePositionX, warningTextureLinePositionY, -4);
		warningTextureLine.setSize(warningTextureLineSizeX, warningTextureLineSizeY);
		
		
		// Buttons coordinates
		int closeButtonPositionX = (int)(warningBackgroundPositionX + warningBackgroundSizeX * 0.34f);
		int closeButtonPositionY = (int)(warningBackgroundPositionY + warningBackgroundSizeY * 0.68f);
		int ButtonSizeX = warningBackgroundSizeX / 3;
		int ButtonSizeY = warningBackgroundSizeY / 5;
		
		
		// Text coordinates		
		int closeButtonTextPositionX = closeButtonPositionX + ButtonSizeX / 2;
		int closeButtonTextPositionY = closeButtonPositionY + ButtonSizeY / 2;
		
		float warningTextPositionX = warningTextureRoundPositionX + warningBackgroundSizeX * 0.55f;
		float warningTextPositionY = warningTextureRoundPositionY - warningTextureRoundSizeY / 4;
		
		
		// Create Buttons
		closeButton = UIButton.create(mixToolkit2, "3_selected.png", "3_selected.png", closeButtonPositionX, closeButtonPositionY, -4);
		closeButton.setSize(ButtonSizeX, ButtonSizeY); 
		closeButton.centerize();
		closeButton.onTouchDown += CloseWarninglDelegate;
		
		// Create Text for Buttons
		closeButtonText = text1.addTextInstance("Close", closeButtonTextPositionX, closeButtonTextPositionY, 0.4f, -6, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		warningText = text3.addTextInstance(warningMessage, warningTextPositionX, warningTextPositionY, 0.4f, -6, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
		warningContainer.addChild(warningBackground, warningTextureRound, warningTextureLine, closeButton);
	}
	
	private void  CloseWarninglDelegate (UIButton sender)
	{
		warningContainer.removeAllChild(true);
		
		warningBackground.destroy();
		warningTextureRound.destroy();
		warningTextureLine.destroy();
		
		closeButton.destroy();
		
		closeButtonText.clear();
		warningText.clear();
	}
	
	public void  Destroy ()
	{
		if(useItemContainer._children.Count > 0)
		{
			CancelDelegate(null);
		}
		
		if(warningContainer._children.Count > 0)
		{
			CloseWarninglDelegate(null);
		}
	}
	
	private void  SendInfo ()
	{
		byte bag = enchantReagent.bag;
		byte slot = enchantReagent.slot;
		
		byte cost_count = 0;
		uint glyphIndex = 0;
		uint spellID = 0;
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_USE_ITEM);
		byte aux = 255;
		if(bag == 255)
		{
			pkt.Append(aux);
			aux = slot;
			pkt.Append(aux);
		}
		else
		{	
			enchantReagent.GetSlotByBag();
			aux = enchantReagent.GetSlotByBag();;
			pkt.Append(aux);
			aux = slot;
			pkt.Append(aux);
		}	
		
		pkt.Append(cost_count);
		pkt.Append(ByteBuffer.Reverse(spellID));
		pkt.Append(ByteBuffer.Reverse(enchantReagent.guid));
		pkt.Append(ByteBuffer.Reverse(glyphIndex));
		pkt.Append((cost_count));
		
		SpellCastTargets targets= new SpellCastTargets();	
		pkt.Append(targets.WriteItem(enchantItem.guid));
		
		RealmSocket.outQueue.Add(pkt);
	}
}