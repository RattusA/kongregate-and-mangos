using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ButtonOLD : MonoBehaviour
{
	public static GameObject _UI = GameObject.Find("UI");
	
	public enum SpellButtonCategory
	{
		DEFAULT		=	0,
		CLASS		=	1,
		ITEM		=	2,
		MOUNTS		=	3
	}

	public UIToolkit buttonToolkit;
	public UIToolkit textToolkit;
	//public static UIToolkit mixToolkit;	//mix1
	static public UIToolkit mixToolkit3;    //mix3
	static public UIToolkit mixToolkit6;	//mix6
	//public static UIToolkit mixToolkit7;	//mix7
	private MusicManager MUSICHOLDER;
	////READ ME IF YOU HAVE INTERFACE ERROR!!!
	/*One: mixToolkit (aka firstToolkit) previously stored all the buttons for interface. When Alin remade the interface he added a secondary texture pack (mix2) only for 
interface buttons so there still might be some remnats through the code which still use the old path for interface button (firstToolkit). If that is the issue then
find the class and function needed and change with myToolkit3 and it should work
Two: Never forget. When in doubt, google it.
*/
	bool  hideShit = false;
	bool  npcMode = false;
	GUISkin newskin;
	Rect SmallerStatsRect;
	Vector2 scrollViewItemVector;
	int MinChatBoxSizeCounter = 1; //-Sebek
	static public bool  targetState=false;
	static public bool  diveState=false;
	static public bool  menuState  = false;
	static bool  gridTargetState = false;
	static public bool  chatState  = false;
	static public bool  editMacrosState  = false;
	static bool  traitInfoFlag = false;
	
	static public bool  lootState = false;
	static public bool  needState = false;
	
	public List<uint> listRequestedLootItems = new List<uint>();
	
	
	static public bool  inventoryState=false;
	static public bool  questState=false;
	static bool  spellState  = false;
	static public bool  optionsState = false;
	static public bool  partyState = false;
	static public bool  mithrilShopState = false;
	static public bool  wip1State = false;
	static public bool  wip2State  = false;
	static public bool  isNeedRefreshInventory =false;
	static public bool  isNeedRefreshBags =false;
	static bool  isNeedRefreshEquip =false;
	//static bool  refreshSlotMenu = false;
	static public bool  refreshAuraM = false;
	static public bool  refreshAura = false;
	static bool  refreshLoot = false;
	static public bool  refreshNeed = false;
	static bool  refreshInterf = false;
	bool  destroyLoot = false; 
	bool  destroyNeed = false;
	
	bool  lootStateOld = false;
	bool  needStateOld = false;
	bool  inventoryStateOld = false;
	bool  questStateOld =  false;
	bool  diveStateOld = false;
	bool  menuStateOld = false;
	bool  spellStateOld = false;
	bool  chatStateOld = false;
	bool  targetStateOld = false;
	bool  optionsStateOld  = false;
	bool  partyStateOld = false;
	bool  wip1StateOld = false;
	//SEBEK
	UIAbsoluteLayout spellDescriptionContainer;
	UIAbsoluteLayout spellEquipPopupContainer;
	UIButton spellDescriptionBkg;
	UISprite descriptionPopupSpellIcon;
	
	UIButton equipButton;
	UIButton cancelSpellDescrButton;
	UITextInstance descriptionPopupSpellText;
	UITextInstance levelPopupSpellText;
	bool  spellDescriptionActive = false;
	bool  macrosDescriptionActive = false;
	
	NonUIElements.TextBox spellDescriptionText;
	NonUIElements.TextBox macrosDescriptionText;
	UIButton spellEquipBkg1;
	UITextInstance equipSpellDescriptionText;
	UITextInstance cancelSpellDescriptionText;
	
	// MACROS EQUIP WINDOW
	UIButton equipMacrosButton;
	UIButton editMacrosButton;
	UIButton cancelMacrosButton;
	
	UITextInstance editMacrosDescriptionText;
	UITextInstance equipMacrosDescriptionText;
	UITextInstance cancelMacrosDescriptionText;
	
	// MACROS EDIT WINDOW
	UISprite macrosEditWindowBackGround;
	UISprite macrosEditWindowSprite;
	
	UIButton saveEditMacrosButton;
	UIButton cancelEditMacrosButton;
	
	UITextInstance saveEditMacrosDescriptionText;
	UITextInstance cancelEditMacrosDescriptionText;
	
	
	//UIButton spellEquipCover;
	//UITextInstance spellEquipText;
	UITextInstance spellEquipName;
	UIButton[] spellEquipSlot;
	//UIButton[] spellEquipSlotLocked;
	//spellEquipSlotLocked = new UIButton[3];
	bool  initSpellEquipPopupActive = false;
	UIButton spellEquipClose; 
	int currentLayer = 0; 
	Spell selectedSpell;
	bool  isPetSpell = false;
	UIStateButton triangleArrowButtonThinghy;//UIButton;
	bool  initEquipSpellConfirmationActive;
	bool  initEquipMacrosConfirmationActive;
	UIButton equipSpellConfPanel;
	UIButton equipConfSpellYESbutton;
	UIButton equipConfSpellNObutton;
	UIButton equipConfSpellCLOSE;
	UITextInstance equipConfSpellYESbuttonText;
	UITextInstance equipConfSpellNObuttonText;
	public UIText text1;
	public UIText text2;
	public UIText text3;
	UIScrollableVerticalLayout UIScrollableVerticalLayout;
	UIButton[] spellCastPortrait;
	UITextInstance[] spellCastPortraitText;
	UISprite portraitSpellEquipBarsHealth1;
	UISprite portraitSpellEquipBarsMana1;
	UISprite portraitSpellEquipBarsHealth2;
	UISprite portraitSpellEquipBarsMana2;
	
	//----------------------------------- SUPER SLIDER POSITIONER - AlinR Tm -----------------
	//float dynamicX;
	//float dynamicY;
	//float dynamicXsize;
	//float dynamicYsize;
	//UIButton targetButton;
	//targetButton = UIButton.create( mixToolkit3, "portrait_health.png", "portrait_health.png", 1, 1, -0.5f);
	//targetButton.setSize(Screen.width*0.13f, Screen.height*0.03f); 
	//function OnGUI()
	//{
	//dynamicX=GUI.HorizontalSlider ( new Rect(Screen.width*0.05f, Screen.height*0.2f,Screen.width*0.3f, Screen.height*0.05f),dynamicX, 0, 1);
	//dynamicY=GUI.HorizontalSlider ( new Rect(Screen.width*0.05f, Screen.height*0.25f,Screen.width*0.3f, Screen.height*0.05f),dynamicY, 0, 1);
	//dynamicXsize=GUI.HorizontalSlider ( new Rect(Screen.width*0.45f, Screen.height*0.2f,Screen.width*0.3f, Screen.height*0.05f),dynamicXsize, 0, 0.5f);
	//dynamicYsize=GUI.HorizontalSlider ( new Rect(Screen.width*0.45f, Screen.height*0.25f,Screen.width*0.3f, Screen.height*0.05f),dynamicYsize, 0, 0.5f);
	//GUI.Label( new Rect(Screen.width*0.1f, Screen.height*0.12f, Screen.width*0.2f, Screen.height*0.2f), "X : "+dynamicX + "    Y : "+dynamicY);
	//GUI.Label( new Rect(Screen.width*0.35f, Screen.height*0.12f, Screen.width*0.2f, Screen.height*0.2f), "Xsize : "+dynamicXsize + "    Ysize : "+dynamicYsize);
	//
	//targetButton.position = new Vector3(Screen.width * dynamicX, -Screen.height * dynamicY,-0.5f);
	//targetButton.setSize(Screen.width*dynamicXsize, Screen.height*dynamicYsize); 
	//}
	//----------------------------------- End of SUPER SLIDER POSITIONER - AlinR Tm -----------------
	
	
	UIAbsoluteLayout traitsContainer;
	UIAbsoluteLayout spellsAndTraitsContainer;
	UIAbsoluteLayout popupContainer;
	UIButton spellsAndTraitsBackground;
	UIButton traitsOrnaments;
	
	UIButton popupWindow;
	
	UIButton[] spellorTraitButton;
	UITextInstance[] spellorTraitText;
	
	bool  traitsActive = false;
	static bool  spellAndTraitState = false;
	static bool  spellAndTraitStateOld = false;
	
	//========End of Traits=======
	float scrh = Screen.height;
	static public string descText;
	short auxItemCounter = 0;
	short auxGeneralCounter = 0;
	static public int switchMode  ;
	static bool  fightMode;
	UIButton rollLootButton;
	public UIZoomButton shopButtonNew;
	public UIButton backButton;
	public UIZoomButton inventoryButton;
	public UIZoomButton questButton;
	public UIZoomButton spellButton;
	public UIZoomButton guildButton;
	public UIZoomButton socialButton;
	public UIZoomButton optionsButton;
	public UIZoomButton logoutButton;
	UIZoomButton ShowDMGAllPlayers;
	//UIZoomButton reportBugButton;
	UIButton helpButton;
	public UIAbsoluteLayout mainMenuContainer;
	//UIZoomButton ShowDMGAllPlayersButton;
	UIButton ItemShopButton;
	
	UIButton mithrilShopButtonAmazon;
	UIZoomButton mithrilShopButton;
	UIZoomButton BuyMithrilButton;
	UITextInstance BuyMithrilButtonText;
	UIButton unstuckButton;
	UIButton leaderboardButton;
	
	//UIButton BuyMithrillButton;
	//UIZoomButton BuyWebMithrillButton;
	UISprite menuBackGround;
	UISprite decorationTexture5;
	UIButton GetFreeMithrillButton;
	//UIButton destroyButton2;
	//UIButton tapFreeMithril;
	UISprite lootGrid;
	UISprite needGreedGrid;
	UISprite needGreedDecoration;
	UISprite lootDecoration;
	UIButton closeLootXButton;
	UIButton closeNeedXButton;
	UIButton needButton;
	UIButton greedButton;
	UIButton passButton;
	UIButton takeThisLoot;
	UIButton takeAllLoot;
	UIButton cancelLoot;
	UIButton selectedItem;
	UITextInstance[] itemTexts;
	UIAbsoluteLayout lootContainer;
	UIAbsoluteLayout needContainer;
	UIAbsoluteLayout lootItemsContainer;
	UIAbsoluteLayout needItemsContainer;
	
	///////////////////////ALERT WINDOW//////////
	UIAbsoluteLayout alertContainer;
	UISprite alertBackground;
	UISprite alertDecoration;
	UIButton closeAlertButton;
	UITextInstance UIalert;
	/////////////////////////////////////////////
	
	/////// QUEST LOG ///////////
	UIAbsoluteLayout questContainer;
	UISprite lastChar;
	UISprite lastChar2;
	UIButton questBackground;
	UISprite topTexture;
	UISprite botTexture;
	UISprite questTitleCase;
	UISprite questSelected;
	UISprite questNotSelected;
	UISprite decorationTexture8;
	UIToggleButton[] questTitleButton = new UIToggleButton[25];
	UIButton questDeleteButton;
	UIButton questShareButton;
	UITextInstance questShareText;
	UITextInstance questDeleteText;
	
	// Set quest arrow button
	UIButton buttonSetArrow;
	UITextInstance textSetArrow;
	
	UITextInstance[] questTitleButtonText = new UITextInstance[25];
	public quest qst;
	UIScrollableVerticalLayout scrollableQuestButtons;
	NonUIElements.TextBox questObjectives;
	NonUIElements.TextBox questDetails;
	//////////////////////////////
	
	/////////QUEST GIVER NPC INTERFACE///////// 
	ulong[] param;
	NonUIElements.TextBox npcQuestObjectives;
	NonUIElements.TextBox npcQuestDetails;
	UIButton[] rewardItems;
	UITextInstance[] rewardItemsText; 
	UIButton[] requiredItems;
	UITextInstance[] requiredItemsText;
	List<quest> quests;
	public QuestStatus qstState = QuestStatus.QUEST_STATUS_INCOMPLETE;
	QuestGiver questGiver;
	public NPC npc;
	UIScrollableVerticalLayout scrollableQuests;
	UIButton questNPCBackground;
	UISprite topQuestTexture;
	UISprite botQuestTexture;
	UISprite decorationTexture9;
	UISprite questCase;
	UIButton questExitButton;
	UIButton getQuestButton;
	UIButton continueQuestButton;
	public UIButton completeQuestButton;
	UITextInstance questExitText;
	UITextInstance getQuestText;
	UITextInstance continueQuestText;
	public UITextInstance completeQuestText;
	UIToggleButton[] questButtonNPC ;
	UITextInstance[] questButtonNPCText;
	///////////////////////////////////////////
	
	public UIButton[] woodenBarBackGround;
	public int woodenBarBackGroundNumber;
	
	
	UIButton Ghost;
	UIZoomButton[] spell;
	int noSpell;
	UIButton items;
	UIButton classSpells;
	//UIZoomButton classSpells
	UIButton defaultSpells;
	//UIAbsoluteLayout _classSpell;
	UIButton spellLeftBackGround;
	UIButton spellSmallBackGround;
	UIButton spellRightBackGround;
	//UIToggleButton fogButton;
	UIButton arrowUp;
	UIButton arrowDown;
	UIButton Mounts;
	UIButton SpellBuyMithril;
	
	
	//OptionsMenu
	UISprite optionBackGround;
	UISprite decorationTexture;
	UISprite decorationTexture2;
	UISprite scrollableTopMargin;
	UISprite scrollableBotMargin;
	//UIButton scrollUpButton;
	UIButton selfPortraitButton;
	UIButton targetPortraitButton;
	UIButton partyPortraitButton;
	UISprite popupBackground;
	UIButton button1; 
	UIButton button2;
	UIButton button3;
	UIButton closeButton;
	bool  popupState = false;
	
	UISprite LogOutBackGround;
	UISprite decorationTexture3;
	UISprite decorationTexture4;
	UISprite counterCase;
	UIButton cancelLogOutButton;
	UIButton cancelLogOutButtonX;
	
	
	
	UIButton reviveButton;
	
	public UIHorizontalLayout bottomActionBar;
	public UIVerticalLayout rightActionBar;
	public UIVerticalLayout rightExtendedActionBar;

	public UIHorizontalLayout rowMainMenu;
	UIHorizontalLayout secondRowMainMenu;
	UIHorizontalLayout thirdRowMainMenu;
	
	//UIButton menuButton;
	
	MainPlayer mainPlayer;
	UIButton[] slot;
	//UIButton[] slotMenu;
	//UIZoomButton portraitSpell;
	//UIZoomButton portraitFightSpell;
	UIText text;
	UIText title;

	UIScrollableVerticalLayout scrollable;
	
	public InventoryManagerOLD inventoryManager = new InventoryManagerOLD();
	
	List<UIToolkit> lootUIToolkitList;
	List<UIToolkit> rollLootUIToolkitList;
	UIButton lootButton;
	UISprite itemDecoration;
	
	float timeCounter = 1.0f; 
	int counter = 20;
	int revCounter = 10;
	
	// these are the tutorial tips
	
	private UITextInstance spellButtonText;
	private UITextInstance traitsButtonText;
	private UITextInstance glyphsButtonText;
	private UITextInstance petTraitButtontText;
	
	UIButton[] tip;
	private UITextInstance[] bagSlotStack;
	private UITextInstance defaultText;
	private UITextInstance classText;
	private UITextInstance itemText;
	private UITextInstance MountsText;
	//private UITextInstance fogText;
	//private UITextInstance soundText;
	private UITextInstance gameOptionsTitle;
	private UITextInstance viewText;
	private UITextInstance lodText;
	private UITextInstance hdText;
	private UITextInstance allDmgText;
	private UITextInstance shadowsText;
	private UITextInstance musicButtonText;
	private UITextInstance musicVolumeText;
	private UITextInstance soundFXText;
	private UITextInstance fxVolumeText;
	private UITextInstance selfPortraitText;
	private UITextInstance targetPortraitText;
	private UITextInstance partyPortraitText; 
	private UITextInstance selfPortraitButtonText;
	private UITextInstance targetPortraitButtonText;
	private UITextInstance partyPortraitButtonText;
	private UITextInstance woodenBackGroundShow;
	private UITextInstance vibroText;
	private UITextInstance appCacheText;
	private UITextInstance showFXButtonText;
	private UITextInstance otherFXButtonText;
	private UITextInstance scrollSpeedText;
	private UITextInstance questArrowToggleText;
	private UITextInstance japaneseCamText;
	
	private UITextInstance button1Text;
	private UITextInstance button2Text;
	private UITextInstance button3Text;
	
	private UITextInstance MinChatBoxSizeText;	//-Sebek
	
	private UITextInstance pleaseWaitText;
	private UITextInstance loggingOutText;
	private UITextInstance counterText;
	private UITextInstance cancelLogOutText;
	private UITextInstance backText ;
	private UITextInstance inventoryText ;
	private UITextInstance questText ;
	private UITextInstance spellText ;
	private UITextInstance wip1Text ;
	private UITextInstance SocialButtonText ;
	private UITextInstance optionsText ;
	private UITextInstance logoutText ;
	//private UITextInstance reportBugButtonText;
	//private UITextInstance helpButtonText;
	//private UITextInstance mithrilShopText;
	//private UITextInstance mithrilShopTextAmazon;
	//private UITextInstance FreeMithrilButtonText;
	//private UIZoomButton FreeMithrilButton;
	//private UITextInstance SupportButtonText;
	//private UIZoomButton SupportButton;
	//private UITextInstance ItemShopButtonText;
	//private UITextInstance diveText ;
	private UITextInstance reviveText ;
	private UITextInstance menuText ;
	private UITextInstance chatText;
	private UITextInstance switchText;
	private UITextInstance switchMenuText;
	//private UITextInstance[] slotText;
	private UITextInstance[] SpellText;
	
	//UITextInstance[] slotMenuText;
	UITextInstance portraitSpellText;
	UITextInstance portraitSpellFightText;
	public int HP;
	public int MaxHP;
	public int Power;
	public int MaxPower;
	int OldHP;
	int OldMaxHP;
	int OldPower;
	int OldMaxPower;
	
	bool  PetPortrait = false;
	
	int PetHP;
	int PetMaxHP;
	int PetPower;
	int PetMaxPower;
	int PetOldHP;
	int PetOldMaxHP;
	int PetOldPower;
	int PetOldMaxPower;
	
	float hpPc;
	float pwPc;
	
	public ulong targetGuid;
	public string targetName;
	public int targetHP;
	public int targetMaxHP;
	public int targetPower;
	public int targetMaxPower;
	float targethpPc=0;
	float targetpwPc=0;
	ulong targetOldGuid;
	string targetOldName;
	int targetOldHP;
	int targetOldMaxHP;
	int targetOldPower;
	int targetOldMaxPower;
	
	uint SpellBackGroundSmallWidth;
	uint SpellBackGroundSmallHeight;
	uint SpellBackGroundRightWidth;
	uint SpellBackGroundRightHeight;
	uint SpellBackGroundLeftWidth;
	uint SpellBackGroundLeftHeight;
	uint optionBackGroundWidth;
	uint optionBackGroundHeight;
	uint LogOutBackGroundWidth;
	uint LogOutBackGroundHeight;
	uint reviveWidth;
	uint reviveHeight;
	uint diveWidth;
	uint diveHeight;
	uint slotWidth;
	uint slotHeight;
	uint jumpWidth;
	uint jumpHeight;
	uint menuWidth;
	uint menuHeight;
	uint chatWidth;
	uint chatHeight;
	uint switchWidth;
	uint switchHeight;
	uint newSwitchHeight;
	uint newSwitchWidth;
	int menuButtonWidth;
	int menuButtonHeight;
	Camera cam;
	public NonUIElements.ReviveWindow reviveWindow = null; 
	
	public string jumpImage;
	public string selectEnemyImage;
	public string objectTabImage;
	public string tradeImage;
	public string battleImage;
	public string inspectImage;
	public string invitePartyImage;
	public string leavePartyImage;
	
	string AssaultImage;
	string EscortImage;
	string FollowImage;
	string HaltImage;
	string HostileImage;
	string InactiveImage;
	
	string[] filenames;
	string[] highlightedFilenames;
	int OffsetLootWindowWidth;
	int OffsetLootWindowHeight;
	int OffsetNeedWindowWidth;
	int OffsetNeedWindowHeight;
	int LootWindowWidth;
	int LootWindowHeight;
	int NeedWindowWidth;
	int NeedWindowHeight;
	
	Touch myTouch;
	/* 
#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
	UIFakeTouch myTouch;
#elif UNITY_EDITOR
	UIFakeTouch myTouch;
#else
	Touch myTouch;
#endif
*/
	uint optionsCount = 21;

	UIProgressBar progressBar;
	
	/////////////PARTY INTERFACE//////////////////////////
	UIAbsoluteLayout partyContainer;
	UISprite partyBackground;
	UISprite partyDecoration;
	UIButton partyAccept;
	UIButton partyDecline;
	UIButton partyCloseButton;
	UITextInstance partyNameText;
	UITextInstance partyQuestion;
	//////////////////////////////////////////////////////////
	/*
*	Summon
*/
	public string sumonnerGUIDName = "";
	private string summonZoneName = "";
	public float sumonningTime = 0.0f;
	
	public float startTime;
	int remainingTime;
	public bool  isTimerStart;
	
	private UIButton acceptSummonButton;
	private UIButton cancelSummonButton;
	private UIButton closeSummonButton;
	private UIButton summonPopUpWindow;
	private UITextInstance summonText;
	private float summonFontSize = UID.TEXT_SIZE * 0.1f;
	
	private UIButton closeSummonPopUpButton;
	
	private bool  agree = true;
	private bool  disagree = false;
	
	/*
* 	Pop Up window of party members
*/
	
	private UIButton partyMemberPopUpWndBackground;
	private UISprite partyMemberPopUpWndDecor;
	private UIButton bottomCloseBtn;
	private UIButton topCloseBtn;
	private UITextInstance partyMemberWndTitle;
	private UIButton[] buttonBackground;
	private UISprite paperBackground;
	private float titleFontSize = UID.TEXT_SIZE * 0.1f;
	private UITextInstance[] partyMemberLabel;
	
	static public bool  summonState  = false;
	
	public PortraitFrame PortraitsFrame;
	public TraitsWindow traitsWindow;
	
	int textSize = 10;
	
	protected int ACTION_BUTTONS_COUNT = 15;
	
	protected int BOTTOM_BUTTON_INDEX = 0;
	protected int RIGHT_BUTTON_INDEX = 6;
	protected int RIGHT_EXTENDED_BUTTON_INDEX = 11;
	
	protected int SELF_PORTRAIT_BUTTON_INDEX = 60;
	protected int TARGET_PORTRAIT_BUTTON_INDEX = 61;

	ItemEntry itm;
	ItemDisplayInfoEntry display;
	SpellEntry sp;
	
	void Awake()
	{
		text1 = new UIText (UIPrime31.myToolkit1, "fontiny dark","fontiny dark.png");
		text2 = new UIText (UIPrime31.myToolkit1, "fontiny title","fontiny title.png");
		text3 = new UIText (UIPrime31.myToolkit1, "fontiny simple","fontiny simple.png");
		spellCastPortrait = new UIButton[2];
		spellCastPortraitText = new UITextInstance[2];
		param = new ulong[3];
		questButtonNPC = new UIToggleButton[10];
		questButtonNPCText = new UITextInstance[10];
		lootUIToolkitList = new List<UIToolkit>();
		rollLootUIToolkitList = new List<UIToolkit>();
		spellDescriptionContainer = new UIAbsoluteLayout();
		spellEquipPopupContainer = new UIAbsoluteLayout();
	}
	
	static public void  SetRefreshEquip ( bool val )
	{
		isNeedRefreshEquip = val;
		if(isNeedRefreshEquip && WorldSession.player)
		{
			WorldSession.player.RequestGearRating();
			WorldSession.player.UpdateStealth();
		}
	}
	void  Start ()
	{
		//init of UI vars
		_UI = GameObject.Find("UI");
		buttonToolkit = _UI.transform.Find("UIToolkitButton").GetComponent<UIToolkit>();
		textToolkit = _UI.transform.Find("UIToolkitText").GetComponent<UIToolkit>();
		//mixToolkit = _UI.transform.Find("UIToolkitInterface").GetComponent<UIToolkit>();
		mixToolkit3 = _UI.transform.Find("UIToolkitInterface3").GetComponent<UIToolkit>();
		mixToolkit6 = _UI.transform.Find("UIToolkitInterface6").GetComponent<UIToolkit>();
		//mixToolkit7 = _UI.transform.Find("UIToolkitInterface7").GetComponent<UIToolkit>();
		//init sound/music
		MUSICHOLDER = GameObject.Find("MusicManager(Clone)").GetComponent<MusicManager>();
		
		newskin = (dbs._sizeMod == 2)? Resources.Load<GUISkin>("GUI/Skins_2x") : Resources.Load<GUISkin>("GUI/Skins");
		
		traitsWindow = new TraitsWindow();
		
		WorldSession.interfaceManager.hideInterface = false;//a fix for HUD blockage at reloging
		
		alertContainer = new UIAbsoluteLayout();
		partyContainer = new UIAbsoluteLayout();
		questContainer = new UIAbsoluteLayout();
		
		secondRowMainMenu = new UIHorizontalLayout(5);
		secondRowMainMenu.position = new Vector3(Screen.width * 0.14f,-Screen.height*4/10,0);
		//row position
		thirdRowMainMenu = new UIHorizontalLayout(5);
		thirdRowMainMenu.position = new Vector3(Screen.width * 0.14f, -Screen.height*5/10,0);
		if(Screen.width<512)
			textSize = 6;
		else 
			if(Screen.width<1024)
				textSize = 8;
		//		fourthRowMainMenu = new UIHorizontalLayout(5);
		//		fourthRowMainMenu.position = new Vector3(Screen.width * 0.14f + ((Screen.width-21)/8), -Screen.height*3/10,0);
		
		reviveWindow = new NonUIElements.ReviveWindow(ReleaseSpirit, ReclaimCorpse);
		
		targetOldGuid=1;
		targetState =false;
		diveState=false;
		menuState = false;
		chatState = false;
		lootState = false;
		needState = false;
		
		//summonState
		summonState = false;
		
		setInventoryState(false);
		questState =false;
		spellState = false;
		spellAndTraitState = false;
		optionsState  = false;
		partyState = false;
		mithrilShopState = false;
		wip1State  = false;
		wip2State  = false;
		isNeedRefreshInventory = false;
		isNeedRefreshBags = false;
		SetRefreshEquip(false);
		refreshLoot = false;
		refreshNeed = false;
		refreshInterf = false;
		destroyLoot = false;
		destroyNeed = false;
		switchMode  = 0;
		fightMode = false;
		
		inventoryStateOld  = inventoryState;
		questStateOld =  questState;
		diveStateOld  = diveState;
		menuStateOld  = menuState;
		spellStateOld  = spellState;
		chatStateOld  = chatState;
		targetStateOld = targetState;
		optionsStateOld  = optionsState;
		wip1StateOld = wip1State;
		lootStateOld = lootState;
		needStateOld = needState;
		
		jumpImage = "Web bound.png";
		selectEnemyImage = "skill_rogue2.png";
		objectTabImage = "tabard.png";
		tradeImage = "money.png";
		battleImage = "Dual Wield 1.png";
		inspectImage = "young_wolfdog_greyeye.png";
		invitePartyImage = "party_invite.png";
		leavePartyImage = "party_leave.png";
		
		AssaultImage = "Assault.png";
		EscortImage = "Escort.png";
		FollowImage = "Follow.png";
		HaltImage = "Halt.png";
		HostileImage = "Hostile.png";
		InactiveImage = "Inactive.png";
		
		
		mainPlayer = GameObject.Find("player").GetComponentInChildren<MainPlayer>();
		
		
		GameObject obj = GameObject.FindWithTag("MainCamera");
		cam = obj.GetComponent<Camera>();
		
		cam.layerCullDistances[12] = cam.farClipPlane * 0.2f;//medium objects
		cam.layerCullDistances[13] = cam.farClipPlane * 0.5f;//small objects

		spell = new UIZoomButton[48];
		//SpellText = new UITextInstance[48];
		noSpell = 0;
		
		SpellBackGroundRightWidth = (uint)(Screen.width*3/4);
		SpellBackGroundRightHeight = (uint)(Screen.height*3/4*0.95f);
		SpellBackGroundLeftWidth = (uint)(Screen.width/4);
		SpellBackGroundLeftHeight = (uint)(Screen.height*3/4*0.95f);
		SpellBackGroundSmallWidth = (uint)(SpellBackGroundRightWidth*9/10);
		SpellBackGroundSmallHeight = (uint)(SpellBackGroundRightHeight*9/10);
		
		optionBackGroundWidth = (uint)(Screen.width*3/4);
		optionBackGroundHeight = (uint)(Screen.height*3/4);
		
		LogOutBackGroundWidth = (uint)(Screen.width/2);
		LogOutBackGroundHeight = (uint)(Screen.height/2); 
		
		menuButtonWidth = (Screen.width-40)/8;
		menuButtonHeight = Screen.height/10;
		
		reviveWidth = (uint)(Screen.width/5);
		reviveHeight = (uint)(Screen.height/10);
		
		jumpWidth = (uint)(Screen.width*0.09f);
		jumpHeight = (uint)(Screen.width*0.09f);
		
		slotWidth = 0;//Screen.width*0.061f;
		slotHeight = 0;//Screen.height*0.098f;
		
		menuWidth = (uint)(Screen.width/10);
		menuHeight = (uint)(Screen.width/22);
		
		chatWidth = (uint)(Screen.width/10);
		chatHeight = (uint)(Screen.width/22);
		
		switchWidth = (uint)(Screen.width/14);
		switchHeight = (uint)(Screen.width/22);
		
		newSwitchWidth = (uint)(Screen.width/18);
		newSwitchHeight = (uint)(Screen.width/18);
		
		
		OffsetLootWindowWidth = (int)(Screen.width/2.5f);
		OffsetLootWindowHeight = Screen.height/8;
		LootWindowWidth = Screen.width/2;
		LootWindowHeight = (int)(Screen.height/1.6f);
		
		OffsetNeedWindowWidth = Screen.width/20;
		OffsetNeedWindowHeight = Screen.height/8;
		NeedWindowWidth = (int)(Screen.width/2.5f-Screen.width/10);
		NeedWindowHeight = (int)(Screen.height/1.6f);
		
		HP = (int)mainPlayer.HP;
		MaxHP = (int)mainPlayer.MaxHP;
		Power = (int)mainPlayer.Power;
		MaxPower = (int)mainPlayer.MaxPower;
		hpPc = HP/(MaxHP+0.001f);
		pwPc = Power/(MaxPower+0.001f);
		
		
		text = new UIText( textToolkit, "prototype", "prototype.png" );
		text.alignMode = UITextAlignMode.Center;
		
		rowMainMenu= new UIHorizontalLayout(5);
		
		bottomActionBar = new UIHorizontalLayout(Screen.width / 50);
		rightActionBar = new UIVerticalLayout(Screen.width / 60);
		rightExtendedActionBar = new UIVerticalLayout(Screen.width / 60);	
		/*
			cast bar
		*/
		progressBar = UIProgressBar.create(UIPrime31.myToolkit3,"progressBar.png", "progressBarBorder.png",5,3,0,0,false );
		progressBar.resizeTextureOnChange = true;
		progressBar.hidden = !menuState && !chatState && !mainPlayer.goCast;
		progressBar.value = 0.0f;
		progressBar.position = new Vector3(Screen.width*0.44f, -Screen.height*0.660f, 0);
		
		
		/**********
		*********
		provizoriu
		********/						
		
		
		/**********
		*********
		MenuButtons
		********/
		
		InitMainMenuButtons();
		
		/***********
		***Traits***
		***********/
		spellsAndTraitsContainer = new UIAbsoluteLayout();
		traitsContainer = new UIAbsoluteLayout();
		popupContainer = new UIAbsoluteLayout();
		
		//spells and Traits background
		spellsAndTraitsBackground = UIButton.create(UIPrime31.myToolkit3, "basic_background.png", "basic_background.png", 0, 0);
		spellsAndTraitsBackground.setSize(Screen.width*1.01f, Screen.height * 0.93f);
		spellsAndTraitsBackground.position = new Vector3(0, -Screen.height * 0.07f, 5);
		spellsAndTraitsBackground.centerize();
		
		
		////////////////////////////////////////////////////////////////////////////////
		
		//spells and traits buttons
		spellorTraitButton = new UIButton[4];							//SPELLS button
		spellorTraitButton[0] = UIButton.create(UIPrime31.myToolkit3, "button1.png", "button1.png", 0, 0);
		spellorTraitButton[0].setSize(Screen.width * 0.25f, Screen.height * 0.12f);
		spellorTraitButton[0].position = new Vector3(Screen.width * 0.27f, -Screen.height * 0.12f, 3);
		spellorTraitButton[0].onTouchDown += SpellsDelegate;
		
		spellButtonText = text2.addTextInstance("SPELLS", Screen.width * 0.38f, Screen.height * 0.185f, UID.TEXT_SIZE * 0.9f, -6, Color.white,UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		spellButtonText.hidden = !spellState;
		//GLYPHS button
		spellorTraitButton[3] = UIButton.create(UIPrime31.myToolkit3, "button1.png", "button1.png", 0, 0);
		spellorTraitButton[3].setSize(Screen.width * 0.25f, Screen.height * 0.12f);
		spellorTraitButton[3].position = new Vector3(Screen.width * 0.27f, -Screen.height * 0.87f, 3);
		spellorTraitButton[3].onTouchDown += GlyphsDelegate;
		
		glyphsButtonText = text2.addTextInstance("APLHS", Screen.width * 0.38f, Screen.height * 0.935f, UID.TEXT_SIZE * 0.9f, -6, Color.white,UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		glyphsButtonText.hidden = !spellState;
		//Traits button
		spellorTraitButton[1] = UIButton.create(UIPrime31.myToolkit3, "button1.png", "button1.png", 0, 0);
		spellorTraitButton[1].setSize(Screen.width * 0.25f, Screen.height * 0.12f);
		spellorTraitButton[1].position = new Vector3(Screen.width * 0.54f, -Screen.height * 0.12f, 3);
		spellorTraitButton[1].onTouchDown += traitsMode;
		spellorTraitButton[1].onTouchDown += TraitsDelegate;
		
		traitsButtonText = text2.addTextInstance("TRAITS", Screen.width * 0.655f, Screen.height * 0.185f, UID.TEXT_SIZE * 0.9f, -6, Color.white,UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		traitsButtonText.hidden = !spellState;
		
		if(mainPlayer.playerClass == Classes.CLASS_HUNTER)
		{		
			spellorTraitButton[2] = UIButton.create(UIPrime31.myToolkit3, "button1.png", "button1.png", 0, 0);
			spellorTraitButton[2].setSize(Screen.width * 0.25f, Screen.height * 0.12f);
			spellorTraitButton[2].position = new Vector3(Screen.width * 0.54f, -Screen.height * 0.87f, 3);
			spellorTraitButton[2].onTouchDown += traitsMode;
			spellorTraitButton[2].onTouchDown += TraitsPetDelegate;
			
			petTraitButtontText = text2.addTextInstance("PET TRAITS", Screen.width * 0.655f, Screen.height * 0.935f, UID.TEXT_SIZE * 0.9f, -6, Color.white,UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
			petTraitButtontText.hidden = !spellState;
			
			spellsAndTraitsContainer.addChild(spellsAndTraitsBackground, spellorTraitButton[0], spellorTraitButton[1], spellorTraitButton[2], spellorTraitButton[3]);
			spellsAndTraitsContainer.hidden = !spellAndTraitState;
		}
		else
		{
			petTraitButtontText = text2.addTextInstance("", 0, 0, UID.TEXT_SIZE * 0.9f, -6, Color.white,UITextAlignMode.Center, UITextVerticalAlignMode.Middle);			
			petTraitButtontText.hidden = !spellState;
			
			spellorTraitButton[2] = null;
			
			spellsAndTraitsContainer.addChild(spellsAndTraitsBackground, spellorTraitButton[0], spellorTraitButton[1], spellorTraitButton[3]);
			spellsAndTraitsContainer.hidden = !spellAndTraitState;
		}
		
		
		//************spells sebek************
		
		InitSpellAndItemEquipWindows();
		InitMacrosEquipWindows();
		InitMacrosEditWindow();
		
		equipSpellConfPanel = UIButton.create(UIPrime31.myToolkit3, "panel.png", "panel.png", 0, 0);
		equipSpellConfPanel.setSize (Screen.width * 0.7f, Screen.height * 0.3f);
		equipSpellConfPanel.position = new Vector3(Screen.width * 0.08f, -Screen.height * 0.23f,-0.3f);
		equipSpellConfPanel.hidden = true;
		
		
		
		equipConfSpellYESbutton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0);
		equipConfSpellYESbutton.setSize (Screen.width * 0.15f, Screen.height * 0.07f);
		equipConfSpellYESbutton.position = new Vector3(Screen.width * 0.21f	, -Screen.height * 0.43f, -0.31f);
		equipConfSpellYESbutton.hidden = true;
		//equipConfSpellYESbutton.info = 0;
		equipConfSpellYESbutton.onTouchDown += EquipConfSpellYESbuttonDelegate;
		
		equipConfSpellNObutton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0);
		equipConfSpellNObutton.setSize (Screen.width * 0.15f, Screen.height * 0.07f);
		equipConfSpellNObutton.position = new Vector3(Screen.width * 0.51f	, -Screen.height * 0.43f, -0.31f);
		equipConfSpellNObutton.hidden = true; 
		equipConfSpellNObutton.info = 2;
		equipConfSpellNObutton.onTouchDown += equipSpellCLOSEdelegate;
		
		equipConfSpellCLOSE = UIButton.create(UIPrime31.myToolkit3, "close_button.png", "close_button.png", 0, 0);
		equipConfSpellCLOSE.setSize (Screen.width * 0.05f, Screen.height * 0.075f);
		equipConfSpellCLOSE.position = new Vector3(Screen.width * 0.75f	, -Screen.height * 0.2f,-0.31f);
		equipConfSpellCLOSE.hidden = true; 
		equipConfSpellCLOSE.info = 2;
		equipConfSpellCLOSE.onTouchDown += equipSpellCLOSEdelegate;
		
		equipConfSpellYESbuttonText = text1.addTextInstance("YES",Screen.width * 0.247f	, Screen.height * 0.47f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
		equipConfSpellYESbuttonText.hidden=true;
		
		equipConfSpellNObuttonText = text1.addTextInstance("NO",Screen.width * 0.555f	, Screen.height * 0.47f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
		equipConfSpellNObuttonText.hidden=true;
		
		spellEquipClose = UIButton.create(UIPrime31.myToolkit3, "close_button.png","close_button.png",0,0);
		spellEquipClose.setSize(Screen.width * 0.05f, Screen.height * 0.075f);
		spellEquipClose.position = new Vector3(Screen.width * 0.81f, -Screen.height * 0.15f, -0.3f);
		spellEquipBkg1 = UIButton.create(UIPrime31.myToolkit4,"spell_equip_screen.png","spell_equip_screen.png", 0, 0);
		spellEquipBkg1.setSize(Screen.width*1.001f, Screen.height * 0.875f);
		spellEquipBkg1.position = new Vector3(0, -Screen.height * 0.11f, -0.2f);
		
		spellCastPortrait[0] = UIButton.create(mixToolkit3, "portrait_frame.png", "portrait_frame.png", 0, 0);
		spellCastPortrait[0].setSize(Screen.width * 0.3f, Screen.height * 0.14f);
		spellCastPortrait[0].position = new Vector3(Screen.width * 0.05f, -Screen.height * 0.65f,-0.22f);
		spellCastPortrait[0].hidden = true;
		spellCastPortrait[0].onTouchDown += initEquipSpellConfirmation;
		spellCastPortrait[0].info = SELF_PORTRAIT_BUTTON_INDEX;
		
		spellCastPortrait[1] = UIButton.create(mixToolkit3, "portrait_frame.png", "portrait_frame.png", 0, 0);  //OMFG
		spellCastPortrait[1].setSize(Screen.width * 0.3f, Screen.height * 0.14f);
		spellCastPortrait[1].position = new Vector3(Screen.width * 0.45f, -Screen.height * 0.65f,-0.22f);
		spellCastPortrait[1].hidden = true;
		spellCastPortrait[1].onTouchDown += initEquipSpellConfirmation;
		spellCastPortrait[1].info = TARGET_PORTRAIT_BUTTON_INDEX;
		
		portraitSpellEquipBarsHealth1 = mixToolkit3.addSprite("portrait_health.png", Screen.width * 0.151f, -Screen.height * 0.686f, -0.22f);
		portraitSpellEquipBarsHealth1.setSize(Screen.width * 0.183f, Screen.height * 0.029f);
		portraitSpellEquipBarsHealth1.position = new Vector3(Screen.width * 0.151f, -Screen.height * 0.686f, -0.22f);
		portraitSpellEquipBarsHealth1.hidden = true;
		
		portraitSpellEquipBarsMana1 = mixToolkit3.addSprite("portrait_mana.png",  Screen.width * 0.151f, -Screen.height* 0.717f, -0.22f);
		portraitSpellEquipBarsMana1.setSize(Screen.width * 0.183f, Screen.height * 0.029f);
		portraitSpellEquipBarsMana1.position = new Vector3(Screen.width * 0.151f, -Screen.height* 0.717f, -0.22f);
		portraitSpellEquipBarsMana1.hidden = true;
		
		portraitSpellEquipBarsHealth2 = mixToolkit3.addSprite("portrait_health.png",  Screen.width * 0.551f, -Screen.height * 0.686f, -0.22f);
		portraitSpellEquipBarsHealth2.setSize(Screen.width * 0.183f, Screen.height * 0.029f);
		portraitSpellEquipBarsHealth2.position = new Vector3(Screen.width * 0.551f, -Screen.height * 0.686f, -0.222f);
		portraitSpellEquipBarsHealth2.hidden = true;
		
		portraitSpellEquipBarsMana2 = mixToolkit3.addSprite("portrait_mana.png",  Screen.width * 0.551f, -Screen.height * 0.717f, -0.22f);
		portraitSpellEquipBarsMana2.setSize(Screen.width * 0.183f, Screen.height * 0.029f);
		portraitSpellEquipBarsMana2.position = new Vector3(Screen.width * 0.551f, -Screen.height * 0.717f, -0.22f);
		portraitSpellEquipBarsMana2.hidden = true;
		
		
		
		spellCastPortraitText[0] = text2.addTextInstance(" PLAYER NAME ",Screen.width * 0.163f	, Screen.height * 0.674f,UID.TEXT_SIZE*0.5f,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
		spellCastPortraitText[0].hidden=true;
		spellCastPortraitText[1] = text2.addTextInstance(" TARGET NAME ",Screen.width * 0.558f	, Screen.height * 0.674f,UID.TEXT_SIZE*0.5f,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
		spellCastPortraitText[1].hidden=true;
		spellCastPortraitText[0].Position (Screen.width * 0.5f, -Screen.height * 0.5f);
		
		string[] trianglestate = {"switch_button.png","switch_button.png","switch_button.png","switch_button.png"};
		
		triangleArrowButtonThinghy =  UIStateButton.create(UIPrime31.myToolkit3,trianglestate,trianglestate,0,0);
		triangleArrowButtonThinghy.setSize (Screen.width * 0.05f, Screen.height * 0.075f);
		triangleArrowButtonThinghy.position = new Vector3(Screen.width * 0.385f, -Screen.height * 0.5f,-0.21f);
		triangleArrowButtonThinghy.index=switchMode;
		triangleArrowButtonThinghy.info=1;
		triangleArrowButtonThinghy.onStateChange += startSwitch;	
		//		spellEquipText = text1.addTextInstance(" TOUCH A SPOT TO EQUIP A SPELL!",Screen.width * 0.05f, Screen.height * 0.4f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
		spellEquipName = text2.addTextInstance(" ",Screen.width * 0.08f	, Screen.height * 0.18f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
		spellEquipClose.onTouchDown += spellEquipCloseDelegate;
		spellEquipBkg1.hidden = true;
		//		spellEquipCover.hidden = true;
		spellEquipClose.hidden = true;			
		triangleArrowButtonThinghy.hidden = true;	
		//		spellEquipText.hidden=true;
		spellEquipName.hidden=true; 
		
		//		int iconNumber = dbs.sSpells.GetRecord(sender.Obj._entry).SpellIconID;
		//		string iconName = dbs.sSpellIcons.GetRecord(iconNumber).Icon
		spellEquipSlot = new UIButton[25]; //15
		
		equipConfSpellYESbuttonText = text1.addTextInstance("YES",Screen.width * 0.247f	, Screen.height * 0.47f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
		equipConfSpellNObuttonText = text1.addTextInstance("NO",Screen.width * 0.555f	, Screen.height * 0.47f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
		equipConfSpellYESbuttonText.hidden=true;
		equipConfSpellNObuttonText.hidden=true;
		
		//*******************************************************
		traitsContainer.hidden = true;
		
		//Debug.Log("UID TEXT SIZE: " + UID.TEXT_SIZE);
		//*************************************************End of Traits*****************************************
		
		/*********
		**********
		*********
		Inventory
		********/
		
		inventoryManager.Init();
		
		/*********
		**********
		*********
		Quest
		********/
		
		
		/*********
		**********
		*********
		Spells
		********/
		
		arrowUp = UIButton.create( UIPrime31.myToolkit3,"arrow_up.png", "arrow_up.png", 0, 0);
		arrowUp.setSize(Screen.width * 0.05f, Screen.height * 0.06f);
		arrowUp.position = new Vector3(Screen.width * 0.8188f, -Screen.height * 0.163f,3);
		arrowUp.hidden=!spellState;
		arrowUp.onTouchDown += ArrowUpDelegate;
		
		arrowDown = UIButton.create( UIPrime31.myToolkit3,"arrow_down.png", "arrow_down.png", 0, 0);
		arrowDown.setSize(Screen.width * 0.05f, Screen.height * 0.06f);
		arrowDown.position = new Vector3(Screen.width * 0.8188f, -Screen.height * 0.857f,3);
		arrowDown.hidden=!spellState;
		arrowDown.onTouchDown += ArrowDownDelegate;
		
		backg = UIButton.create( UIPrime31.myToolkit3,"main_window_spells.png","main_window_spells.png", 0, 0);
		backg.setSize(Screen.width * 0.7f, Screen.height * 0.79f);
		backg.position = new Vector3( Screen.width * 0.27f, -Screen.height * 0.15f,3.1f);
		backg.hidden=!spellState; 
		
		
		items = UIButton.create( UIPrime31.myToolkit3,"Items.png", "Items.png", 0, 0);
		items.centerize();
		items.setSize(Screen.width*0.25f,Screen.width*0.1f);
		items.position = new Vector3(Screen.width * 0.125f, -Screen.height * 0.55f,0);
		items.hidden=!spellState;
		items.onTouchDown += itemSpell;
		
		classSpells = UIButton.create( UIPrime31.myToolkit3,"class.png", "class.png", 0, 0);
		classSpells.centerize();
		classSpells.setSize(Screen.width/4,Screen.width/10);
		classSpells.position = new Vector3(Screen.width * 0.125f, -Screen.height * 0.40f,0);
		classSpells.hidden=!spellState;
		classSpells.onTouchDown += classSpell;
		
		defaultSpells = UIButton.create( UIPrime31.myToolkit3,"general.png", "general.png", 0, 0);
		defaultSpells.centerize();
		defaultSpells.setSize(Screen.width/4,Screen.width/10);
		defaultSpells.position = new Vector3(Screen.width * 0.125f, -Screen.height * 0.25f,0.01f);
		defaultSpells.hidden=!spellState;
		defaultSpells.onTouchDown += defaultSpell;
		
		Mounts = UIButton.create( UIPrime31.myToolkit3,"Mounts_pets.png", "Mounts_pets.png", 0, 0);
		Mounts.centerize();
		Mounts.setSize(Screen.width*0.25f,Screen.width*0.1f);
		Mounts.position = new Vector3(Screen.width * 0.125f, -Screen.height * 0.70f,0);
		Mounts.hidden=!spellState;
		Mounts.onTouchDown += MountsDelegate;
		
		if(BuildConfig.hasFreeMithrilButton)
		{
			SpellBuyMithril = UIButton.create( UIPrime31.myToolkit3,"gfm.png", "gfm.png", 0, 0);
			SpellBuyMithril.centerize();
			SpellBuyMithril.setSize(Screen.width*0.3f,Screen.width*0.1f);
			SpellBuyMithril.position = new Vector3(Screen.width * 0.15f, -Screen.height * 0.85f,0);
			SpellBuyMithril.hidden=!spellState;
			SpellBuyMithril.onTouchDown += FreeMithrilDelegate;
		}
		
		spellLeftBackGround = UIButton.create( UIPrime31.interfMix,"toggle_inactive_normal.png", "toggle_inactive_normal.png", 0, 0);
		spellLeftBackGround.color = new Color(0.5f,0.5f,0.5f,0.5f);
		spellLeftBackGround.setSize(SpellBackGroundLeftWidth,SpellBackGroundLeftHeight);
		spellLeftBackGround.position = new Vector3(0,-1.5f*menuHeight,5);
		spellLeftBackGround.hidden=!spellState;
		
		
		spellRightBackGround = UIButton.create( UIPrime31.interfMix,"toggle_inactive_normal.png", "toggle_inactive_normal.png", 0, 0);
		spellRightBackGround.color = new Color(0.5f,0.5f,0.5f,0.5f);
		spellRightBackGround.setSize(SpellBackGroundRightWidth,SpellBackGroundRightHeight);
		spellRightBackGround.position = new Vector3(SpellBackGroundLeftWidth,-1.5f*menuHeight,5);
		spellRightBackGround.hidden=!spellState;
		
		spellSmallBackGround = UIButton.create( UIPrime31.interfMix,"toggle_inactive_normal.png", "toggle_inactive_normal.png", 0, 0);
		spellSmallBackGround.centerize();
		spellSmallBackGround.color = new Color(0,0,0,0.7f);
		spellSmallBackGround.setSize(SpellBackGroundSmallWidth,SpellBackGroundSmallHeight);
		spellSmallBackGround.position = new Vector3(SpellBackGroundLeftWidth+SpellBackGroundRightWidth/2,-1.5f*menuHeight-SpellBackGroundRightHeight/2,5); 
		spellSmallBackGround.hidden=!spellState;
		
		
		defaultText=text2.addTextInstance("GENERAL",defaultSpells.position.x * 1.2f,-defaultSpells.position.y,UID.TEXT_SIZE * 0.9f,-6,Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);//("GENERAL",defaultSpells.position.x,-defaultSpells.position.y,0.4ff,-6,Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
		defaultText.hidden=!spellState;
		
		
		classText=text2.addTextInstance("CLASS",classSpells.position.x * 1.2f,-classSpells.position.y,UID.TEXT_SIZE * 0.9f,-1,Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
		classText.hidden=!spellState;
		
		
		itemText=text2.addTextInstance("ITEMS",items.position.x * 1.2f,-items.position.y,UID.TEXT_SIZE * 0.9f,-1,Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
		itemText.hidden=!spellState;
		
		MountsText=text2.addTextInstance("BEINGS",Mounts.position.x * 1.2f,-Mounts.position.y,UID.TEXT_SIZE * 0.9f,-1,Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
		MountsText.hidden=!spellState;
		
		InitLogOut();	
		InitUserInterface();
	}
	
	void  InitMainMenuButtons ()
	{
		float mainMenuButtonWidth = menuButtonWidth * 0.8f;
		
		// Init the Shop Button on the Main Menu
		float shopButtonWidth = 0.0f;
		shopButtonNew = UIZoomButton.create(UIPrime31.myToolkit3, "shop.png", "shop.png", 0, 0, -3);
		shopButtonNew.centerize();
		if(BuildConfig.hasItemShopButton)
		{
			shopButtonWidth = menuButtonWidth * 1.6f;
			shopButtonNew.setSize(shopButtonWidth, menuButtonHeight);
			shopButtonNew.onTouchUpInside += ItemShopDelegate;
		}
		else
		{
			#if UNITY_ANDROID
			if(BuildConfig.platformName == "amazon")
			{
				shopButtonNew.setSize(shopButtonWidth, menuButtonHeight);
				shopButtonNew.onTouchUpInside += BuyMithrilIapAmazonDelegate;
			}
			else
			{
				shopButtonNew.setSize(shopButtonWidth, menuButtonHeight);
				shopButtonNew.onTouchUpInside += BuyMithrilIapDelegate;
			}
			#elif UNITY_IPHONE
			//Do buy mithril ios
			shopButtonNew.setSize(shopButtonWidth, menuButtonHeight);
			shopButtonNew.onTouchUpInside += BuyMithrilIapDelegate;
			#else
			//Do nothing
			shopButtonNew.setSize(0, 0);
			#endif
		}
		
		// Init Back Button to exit from menu
		backButton = UIButton.create(UIPrime31.myToolkit3, "back.png", "back.png", 0, 0, -3);
		backButton.centerize();
		backButton.setSize(mainMenuButtonWidth, menuButtonHeight);
		backButton.onTouchUpInside += backMode;
		
		// Init Inventory Button to open players inventory
		inventoryButton = UIZoomButton.create(UIPrime31.myToolkit3, "inventory.png", "inventory.png", 0, 0, -3);
		inventoryButton.centerize();
		inventoryButton.setSize(mainMenuButtonWidth, menuButtonHeight);
		inventoryButton.onTouchUpInside += inventoryMode;
		
		// Init Quest Button to open players quest list
		questButton = UIZoomButton.create(UIPrime31.myToolkit3, "map.png", "map.png", 0, 0, -3);
		questButton.centerize();
		questButton.setSize(mainMenuButtonWidth, menuButtonHeight);
		questButton.onTouchUpInside += questMode;
		
		// Init Spell Button to open players spell book
		spellButton = UIZoomButton.create(UIPrime31.myToolkit3, "guest.png", "guest.png", 0, 0, -3);
		spellButton.centerize();
		spellButton.setSize(mainMenuButtonWidth, menuButtonHeight);
		spellButton.onTouchDown += spellMode;
		spellButton.onTouchUpInside += defaultSpell;
		
		// Init Guild Button to show players guild info
		guildButton = UIZoomButton.create(UIPrime31.myToolkit3, "guild.png", "guild.png", 0, 0, -3);
		guildButton.centerize();
		guildButton.setSize(mainMenuButtonWidth, menuButtonHeight);
		guildButton.onTouchUpInside += AnimViewMode;
		
		// Init Social Button to show players friend info
		socialButton = UIZoomButton.create(UIPrime31.myToolkit3, "society.png", "society.png", 0, 0, -3);
		socialButton.centerize();
		socialButton.setSize(mainMenuButtonWidth, menuButtonHeight);
		socialButton.onTouchUpInside += SocialDelegate;
		
		// Init options Button to show Game Options
		optionsButton = UIZoomButton.create(UIPrime31.myToolkit3, "options.png", "options.png", 0, 0, -3);
		optionsButton.centerize();
		optionsButton.setSize(mainMenuButtonWidth, menuButtonHeight);
		optionsButton.onTouchUpInside += optionsMode;
		
		// Init LogOut Button to Exit fromm Game
		logoutButton = UIZoomButton.create(UIPrime31.myToolkit3, "exit.png", "exit.png", 0, 0, -3);
		logoutButton.centerize();
		logoutButton.setSize(mainMenuButtonWidth, menuButtonHeight);
		logoutButton.onTouchUpInside +=LogoutDelegate;
		//logoutButton.hidden = !menuState;
		
		mainMenuContainer = new UIAbsoluteLayout();
		
		InitMainMenu();
		
		woodenBarBackGroundNumber = Screen.width / 256 +1;
		woodenBarBackGround = new UIButton[woodenBarBackGroundNumber];
		
		for (int j=0; j<woodenBarBackGroundNumber; j++)
		{
			woodenBarBackGround[j] = UIButton.create(UIPrime31.myToolkit3, "wooden_bar.png", "wooden_bar.png", 0, 0,-1);
			woodenBarBackGround[j].setSize(256, Screen.height*0.11f);
			woodenBarBackGround[j].position=new Vector3(256 * j, 0, -2);
			woodenBarBackGround[j].hidden=!menuState;
		}
		
		rowMainMenu.addChild(shopButtonNew);
		rowMainMenu.addChild(backButton);
		rowMainMenu.addChild(inventoryButton);
		rowMainMenu.addChild(questButton);
		rowMainMenu.addChild(spellButton);
		rowMainMenu.addChild(guildButton);
		rowMainMenu.addChild(socialButton);
		rowMainMenu.addChild(optionsButton);
		rowMainMenu.addChild(logoutButton);
		rowMainMenu.hidden = !menuState;
	}
	
	void  InitLogOut ()
	{
		float LogOutBackGroundWidth = Screen.width / 4.0f;
		float LogOutBackGroundHeight = Screen.height / 4.0f;
		float LogOutBackGroundSizeWidth = Screen.width / 2.0f;
		float LogOutBackGroundSizeHeight = Screen.height / 2.0f;
		
		LogOutBackGround = UIPrime31.myToolkit4.addSprite("popup_background.png", LogOutBackGroundWidth, LogOutBackGroundHeight, -5);
		LogOutBackGround.setSize(LogOutBackGroundSizeWidth, LogOutBackGroundSizeHeight);
		LogOutBackGround.hidden = !mainPlayer.LoggingOut;
		
		cancelLogOutButtonX = UIButton.create(UIPrime31.myToolkit3, "close_button.png", "close_button.png", 0, 0, -6);
		cancelLogOutButtonX.setSize(LogOutBackGround.height/8, LogOutBackGround.height/8);
		cancelLogOutButtonX.position = new Vector3(LogOutBackGround.position.x + LogOutBackGround.width - cancelLogOutButtonX.width/1.5f, LogOutBackGround.position.y + cancelLogOutButtonX.height/3, -6 );
		cancelLogOutButtonX.onTouchDown += CancelLogoutDelegate;
		cancelLogOutButtonX.hidden = !mainPlayer.LoggingOut;					
		
		cancelLogOutButton = UIButton.create(UIPrime31.myToolkit4, "3_selected.png", "3_unselected.png", 0, 0, -6);
		cancelLogOutButton.setSize(LogOutBackGround.width / 1.8f, LogOutBackGround.height / 7);
		cancelLogOutButton.centerize();
		cancelLogOutButton.position = new Vector3(LogOutBackGround.position.x + LogOutBackGround.width/2, LogOutBackGround.position.y - LogOutBackGround.height + cancelLogOutButton.height, -6);
		cancelLogOutButton.onTouchDown += CancelLogoutDelegate;
		cancelLogOutButton.hidden = !mainPlayer.LoggingOut;
		
		counterCase = UIPrime31.myToolkit3.addSprite("tab_solo.png", 0, 0, -6 );
		counterCase.setSize(LogOutBackGround.width / 5, LogOutBackGround.height / 5);
		counterCase.centerize();
		counterCase.position = new Vector3(LogOutBackGround.position.x + LogOutBackGround.width/2, LogOutBackGround.position.y - counterCase.height*1.8f, -6 );
		counterCase.hidden = !mainPlayer.LoggingOut;
		
		decorationTexture3 = UIPrime31.myToolkit3.addSprite( "decoration_02.png", LogOutBackGround.position.x + LogOutBackGround.width/20, -LogOutBackGround.position.y + LogOutBackGround.height/2, -6);
		decorationTexture3.setSize(LogOutBackGround.height * 0.25f,LogOutBackGround.height * 0.25f );
		decorationTexture3.hidden = !mainPlayer.LoggingOut;
		
		decorationTexture4 = UIPrime31.myToolkit3.addSprite( "decoration_01.png", decorationTexture3.position.x + decorationTexture3.width*0.8f, -decorationTexture3.position.y + decorationTexture3.height*0.8f, -6);
		decorationTexture4.setSize(LogOutBackGround.width*0.7f,LogOutBackGround.height * 0.05f );
		decorationTexture4.hidden = !mainPlayer.LoggingOut;
		
		cancelLogOutText=text1.addTextInstance("Cancel Logout", cancelLogOutButton.position.x, -cancelLogOutButton.position.y, 0.5f, -6, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		cancelLogOutText.hidden = !mainPlayer.LoggingOut;
		
		loggingOutText = text3.addTextInstance("Logging Out.", LogOutBackGround.position.x + LogOutBackGround.width/2, -LogOutBackGround.position.y + LogOutBackGround.height/7, 0.7f, - 6, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		loggingOutText.hidden = !mainPlayer.LoggingOut;
		
		pleaseWaitText = text3.addTextInstance("Please wait!", LogOutBackGround.position.x + LogOutBackGround.width/2, -counterCase.position.y + counterCase.height*1.2f, 0.7f, - 6, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		pleaseWaitText.hidden = !mainPlayer.LoggingOut;
		
		counterText=text1.addTextInstance(counter.ToString(), counterCase.position.x - counterCase.height/12, -counterCase.position.y + counterCase.height/20, 1f, -7, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		counterText.hidden = !mainPlayer.LoggingOut;
	}
	
	void  InitUserInterface ()
	{
		int i;
		
		PortraitsFrame = new PortraitFrame();
		
		//addTextInstance
		
		filenames = new string[]{"SWITCH_REFRESH.png", "SWITCH_REFRESH.png", "SWITCH_REFRESH.png", "SWITCH_REFRESH.png"};
		
		highlightedFilenames = new string[]{"SWITCH_REFRESH.png", "SWITCH_REFRESH.png", "SWITCH_REFRESH.png", "SWITCH_REFRESH.png"};
		
		slot = new UIButton[ACTION_BUTTONS_COUNT];
		
		for(i = 0; i < ACTION_BUTTONS_COUNT; i++)
		{
			slot[i] = UIButton.create(UIPrime31.myToolkit3, "tab_solo.png", "tab_solo.png", 0, 0);
			slot[i].setSize(slotWidth,slotHeight);
			slot[i].info =i ;
			//slot[i].onTouchDown += newColor;
			slot[i].hidden = menuState;
			
			if(i < RIGHT_BUTTON_INDEX)
				bottomActionBar.addChild(slot[i]);
			else if(i < RIGHT_EXTENDED_BUTTON_INDEX)
				rightActionBar.addChild(slot[i]);
			else
				rightExtendedActionBar.addChild(slot[i]);
		}

		rightActionBar.position = new Vector3(Screen.width-rightActionBar.width-Screen.width*0.008f, -Screen.height * 0.165f, 0);
		rightActionBar.hidden = menuState;
		
		rightExtendedActionBar.position = new Vector3(Screen.width -rightActionBar.width - rightExtendedActionBar.width - Screen.width * 0.0175f, -Screen.height * 0.38f, 0);
		rightExtendedActionBar.hidden = menuState;
		
		bottomActionBar.position = new Vector3(Screen.width * 0.43f, -Screen.height * 0.9f, 0);
		
		secondRowMainMenu.hidden = menuState;
		thirdRowMainMenu.hidden = menuState;
		
		mainPlayer.need.tempEntry = 0;
	}
	
	void  InitSpellAndItemEquipWindows ()
	{
		UIText TextOnButton;
		TextOnButton = new UIText(UIPrime31.myToolkit1, "fontiny dark","fontiny dark.png");
		
		Color TextColor = new Color(0.62f, 0.41f, 0.31f, 1);
		float EquipButtonWidth = Screen.width * 0.2f;
		float EquipButtonHeight = Screen.height * 0.0875f;
		
		float spellDescriptionBkgWidth = Screen.width * 0.85f;
		float spellDescriptionBkgHeight = Screen.height * 0.79f;

		spellDescriptionBkg = UIButton.create(UIPrime31.myToolkit3,"main.png","main.png", 0, 0);
		spellDescriptionBkg.setSize(spellDescriptionBkgWidth, spellDescriptionBkgHeight);
		spellDescriptionBkg.position = new Vector3(Screen.width * 0.0568f, -Screen.height * 0.128f, -0.1f);
		spellDescriptionBkg.hidden = true;

		cancelSpellDescrButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0);
		cancelSpellDescrButton.setSize(EquipButtonWidth, EquipButtonHeight);
		cancelSpellDescrButton.position = new Vector3(Screen.width * 0.605f, -Screen.height * 0.805f, -2);
		cancelSpellDescrButton.onTouchDown += cancelSpellDescriptionPopupDelegate;
		cancelSpellDescrButton.hidden = true;
		
		equipButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0);
		equipButton.setSize(EquipButtonWidth, EquipButtonHeight);
		equipButton.position = new Vector3(Screen.width * 0.185f, -Screen.height * 0.805f, -2);
		equipButton.onTouchDown += equipButtonDelegate;
		equipButton.hidden = true;
		
		equipSpellDescriptionText = TextOnButton.addTextInstance("EQUIP",equipButton.position.x*1.51f,-equipButton.position.y*1.06f,UID.TEXT_SIZE,-2, TextColor,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
		cancelSpellDescriptionText = TextOnButton.addTextInstance("CANCEL",cancelSpellDescrButton.position.x*1.165f,-cancelSpellDescrButton.position.y*1.06f,UID.TEXT_SIZE,-2, TextColor,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
		
		equipSpellDescriptionText.hidden = true;				
		cancelSpellDescriptionText.hidden = true;
	}
	
	void  InitMacrosEquipWindows ()
	{
		UIText TextOnButton;
		TextOnButton = new UIText(UIPrime31.myToolkit1, "fontiny dark","fontiny dark.png");
		
		Color TextColor = new Color(0.62f, 0.41f, 0.31f, 1);
		float EquipButtonWidth = Screen.width * 0.2f;
		float EquipButtonHeight = Screen.height * 0.0875f;
		
		Vector3 editMacrosButtonPos = new Vector3(Screen.width * 0.119f, -Screen.height * 0.805f, -2);
		Vector3 equipMacrosButtonPos = new Vector3(Screen.width * 0.381f, -Screen.height * 0.805f, -2);
		Vector3 cancelMacrosButtonPos = new Vector3(Screen.width * 0.643f, -Screen.height * 0.805f, -2);
		
		editMacrosButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0);
		editMacrosButton.setSize(EquipButtonWidth, EquipButtonHeight);
		editMacrosButton.position = editMacrosButtonPos;
		editMacrosButton.onTouchDown += editMacrosButtonDelegate;
		editMacrosButton.hidden = true;
		
		equipMacrosButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0);
		equipMacrosButton.setSize(EquipButtonWidth, EquipButtonHeight);
		equipMacrosButton.position = equipMacrosButtonPos;
		equipMacrosButton.onTouchDown += equipMacrosButtonDelegate;
		equipMacrosButton.hidden = true;
		
		cancelMacrosButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0);
		cancelMacrosButton.setSize(EquipButtonWidth, EquipButtonHeight);
		cancelMacrosButton.position = cancelMacrosButtonPos;
		cancelMacrosButton.onTouchDown += cancelMacrosButtonDelegate;
		cancelMacrosButton.hidden = true;
		
		
		// Text of buttons position = Button.position + 50% of Button.Width
		// Width = Button.position.X + 50% of Button.Width
		// Height = Button.position.Y + 50% of Button.Height
		editMacrosDescriptionText = TextOnButton.addTextInstance("EDIT", Screen.width * 0.219f, Screen.height * 0.853f, UID.TEXT_SIZE, -2, TextColor, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		equipMacrosDescriptionText = TextOnButton.addTextInstance("EQUIP", Screen.width * 0.481f, Screen.height * 0.853f, UID.TEXT_SIZE, -2, TextColor, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		cancelMacrosDescriptionText = TextOnButton.addTextInstance("CANCEL", Screen.width * 0.743f, Screen.height * 0.853f, UID.TEXT_SIZE, -2, TextColor, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		editMacrosDescriptionText.hidden = true;
		equipMacrosDescriptionText.hidden = true;				
		cancelMacrosDescriptionText.hidden = true;
	}
	
	void  InitMacrosEditWindow ()
	{
		UIText TextOnButton;
		TextOnButton = new UIText(UIPrime31.myToolkit1, "fontiny dark","fontiny dark.png");
		
		Color TextColor = new Color(0.62f, 0.41f, 0.31f, 1);
		float EquipButtonWidth = Screen.width * 0.2f;
		float EquipButtonHeight = Screen.height * 0.0875f;
		
		Vector3 saveEditMacrosButtonPos = new Vector3(Screen.width * 0.233f, -Screen.height * 0.805f, 1);
		Vector3 cancelEditMacrosButtonPos = new Vector3(Screen.width * 0.566f, -Screen.height * 0.805f, 1);
		
		
		// Init Sprite elements
		macrosEditWindowBackGround = UIPrime31.myToolkit4.addSprite( "popup_background.png", 0, 0, 2);
		macrosEditWindowBackGround.setSize (Screen.width*0.9f, Screen.height*0.9f);
		macrosEditWindowBackGround.position = new Vector3 (Screen.width*0.05f, -Screen.height * 0.05f, 2);
		macrosEditWindowBackGround.hidden = true;
		
		macrosEditWindowSprite = UIPrime31.myToolkit4.addSprite("deco_buy_mithrill.png",0,0, 1);
		macrosEditWindowSprite.setSize(Screen.width*0.86f, Screen.height*0.125f);
		macrosEditWindowSprite.position = new Vector3(Screen.width*0.067f, -Screen.height*0.0975f, 1);
		macrosEditWindowSprite.hidden = true;
		
		saveEditMacrosButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0);
		saveEditMacrosButton.setSize(EquipButtonWidth, EquipButtonHeight);
		saveEditMacrosButton.position = saveEditMacrosButtonPos;
		saveEditMacrosButton.onTouchDown += saveEditMacrosButtonDelegate;
		saveEditMacrosButton.hidden = true;
		
		cancelEditMacrosButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0);
		cancelEditMacrosButton.setSize(EquipButtonWidth, EquipButtonHeight);
		cancelEditMacrosButton.position = cancelEditMacrosButtonPos;
		cancelEditMacrosButton.onTouchDown += cancelEditMacrosButtonDelegate;
		cancelEditMacrosButton.hidden = true;	
		
		
		// Init Text on the buttons
		saveEditMacrosDescriptionText = TextOnButton.addTextInstance("SAVE", Screen.width * 0.333f, Screen.height * 0.853f, UID.TEXT_SIZE, -2, TextColor, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		cancelEditMacrosDescriptionText = TextOnButton.addTextInstance("CLOSE", Screen.width * 0.666f, Screen.height * 0.853f, UID.TEXT_SIZE, -2, TextColor, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		saveEditMacrosDescriptionText.hidden = true;
		cancelEditMacrosDescriptionText.hidden = true;
		
	}
	
	public string RefreshIcon ( int slot )
	{
		string slotImage = "";
		ActionBarSlot selectedSlot;
		int slotNumber = slot % ACTION_BUTTONS_COUNT;
		if(mainPlayer.pet && switchMode == 0 &&
		   slotNumber >= RIGHT_EXTENDED_BUTTON_INDEX && slotNumber <= ACTION_BUTTONS_COUNT)
		{
			selectedSlot = mainPlayer.actionBarManager.GetSlotPetObject(slotNumber - RIGHT_EXTENDED_BUTTON_INDEX);
		}
		else
		{
			selectedSlot = mainPlayer.actionBarManager.GetSlotObject(slot);
		}
		
		if(selectedSlot != null)
		{	
			switch(selectedSlot.slotType)
			{
			case ActionButtonType.ACTION_BUTTON_MACRO:
				switch(selectedSlot.objectID)
				{
				case 0:		// "Jump"
					slotImage = jumpImage;
					break;
				case 1:		// "Closest Target"
					slotImage = selectEnemyImage;
					break;
				case 2:		// "Next Target"
					slotImage = objectTabImage;
					break;
				case 3:		// "Trade"
					slotImage = tradeImage;
					break;
				case 4:		// "Battle"
					slotImage = battleImage;
					break;
				case 5:		// "Examine"
					slotImage = inspectImage;
					break;
				case 6:		// "Invite to party"
					slotImage = invitePartyImage;
					break;
				case 7:		// "Leave party"
					slotImage = leavePartyImage;
					break;
				case 8:		// "Macro 1"
					slotImage = "Marco1.png";
					break;
				case 9:		// "Macro 2"
					slotImage = "Marco2.png";
					break;
				case 10:	// "Macro 3"
					slotImage = "Marco3.png";
					break;
				case 11:	// "Macro 4"
					slotImage = "Marco4.png";
					break;
				case 12:	// "Macro 5"
					slotImage = "Marco5.png";
					break;
				case 13:	// "Macro 6"
					slotImage = "Marco6.png";
					break;
				case 14:	// "Macro 7"
					slotImage = "Marco7.png";
					break;
				case 15:	// "Macro 8"
					slotImage = "Marco8.png";
					break;
				case 16:	// "Macro 9"
					slotImage = "Marco9.png";
					break;
				case 17:	// "Macro 10"
					slotImage = "Marco10.png";
					break;
				case 18:	// "Macro 11"
					slotImage = "Marco11.png";
					break;
				case 19:	// "Macro 12"
					slotImage = "Marco12.png";
					break;
				case 20:	// "Macro 13"
					slotImage = "Marco13.png";
					break;
				case 21:	// "Macro 14"
					slotImage = "Marco14.png";
					break;
				case 22:	// "Macro 15"
					slotImage = "Marco15.png";
					break;
					
				case 23:	// "Assault"
					if(mainPlayer.playerClass == Classes.CLASS_WARLOCK || mainPlayer.playerClass == Classes.CLASS_HUNTER)
					{			
						slotImage = AssaultImage;
					}
					break;
				case 24:	// "Escort"
					if(mainPlayer.playerClass == Classes.CLASS_WARLOCK || mainPlayer.playerClass == Classes.CLASS_HUNTER)
					{			
						slotImage = EscortImage;
					}
					break;
				case 25:	// "Follow"
					if(mainPlayer.playerClass == Classes.CLASS_WARLOCK || mainPlayer.playerClass == Classes.CLASS_HUNTER)
					{			
						slotImage = FollowImage;
					}
					break;
				case 26:	// "Halt"
					if(mainPlayer.playerClass == Classes.CLASS_WARLOCK || mainPlayer.playerClass == Classes.CLASS_HUNTER)
					{			
						slotImage = HaltImage;
					}
					break;
				case 27:	// "Hostile"
					if(mainPlayer.playerClass == Classes.CLASS_WARLOCK || mainPlayer.playerClass == Classes.CLASS_HUNTER)
					{			
						slotImage = HostileImage;
					}
					break;
				case 28:	// "Inactive"
					if(mainPlayer.playerClass == Classes.CLASS_WARLOCK || mainPlayer.playerClass == Classes.CLASS_HUNTER)
					{			
						slotImage = InactiveImage;
					}
					break;
				}
				break;
				
			case ActionButtonType.ACTION_BUTTON_SPELL:
				slotImage = DefaultIcons.GetSpellIcon(selectedSlot.objectID);
				break;
				
			case ActionButtonType.ACTION_BUTTON_ITEM:
				slotImage = DefaultIcons.GetItemIcon(selectedSlot.objectID);
				break;
			}
		}
		
		if(slotImage == null || slotImage == "")
		{
			slotImage = "noicon.png";
		}
		
		return slotImage;
	}
	
	void  Update ()
	{
		if(!mainPlayer.gSocialUI.isHidden && mainPlayer.menuSelected != 101)
		{
			mainPlayer.gSocialUI.HideSocialUI();
		}
		Vector3 tmpPosition;
		if(questState)
		{	
			//yield return new WaitForSeconds(0.1f);
			for (int k= 0; k < 25; k++) 
				if(questTitleButton[k] != null)
			{
				tmpPosition = new Vector3(questTitleButton[k].position.x,
				                          questTitleButton[k].position.y + questTitleButton[k].height*0.1f,
				                          questTitleButtonText[k].position.z);
				questTitleButtonText[k].position = tmpPosition;
				questTitleButtonText[k].hidden = questTitleButton[k].hidden;
				
				if (questObjectives!=null)
				{	
					if(questTitleButton[k].selected)
					{	
						if (questObjectives.m_textInstances.position.y != questTitleButton[k].position.y)
						{
							tmpPosition = new Vector3(questObjectives.m_textInstances.position.x,
							                          questTitleButton[k].position.y,
							                          questObjectives.m_textInstances.position.z);
							questObjectives.m_textInstances.position = tmpPosition;
						}
						questObjectives.m_textInstances.hidden = false;
						lastChar = questObjectives.m_textInstances.textSprites[questObjectives.m_textInstances.textSprites.Count-1];
						if(questDetails.m_textInstances.position.y != lastChar.position.y - lastChar.height)
						{
							tmpPosition = new Vector3(questDetails.m_textInstances.position.x,
							                          lastChar.position.y - lastChar.height,
							                          questDetails.m_textInstances.position.z);
							questDetails.m_textInstances.position = tmpPosition;
						}
						questDetails.m_textInstances.hidden = false;
					}
				}
			}
		}
		
		if (questButtonNPC[0] != null)
		{	
			for(int k = 0; k < quests.Count; k++)
			{
				if(questButtonNPC[k]!=null)
				{
					tmpPosition = new Vector3(questButtonNPC[k].position.x,
					                          questButtonNPC[k].position.y + questButtonNPC[k].height*0.1f,
					                          questButtonNPCText[k].position.z);
					questButtonNPCText[k].position = tmpPosition;
					questButtonNPCText[k].hidden = questButtonNPC[k].hidden; 
					
					if (npcQuestObjectives!=null)
					{	
						if(questButtonNPC[k].selected)
						{	
							if (npcQuestObjectives.m_textInstances.position.y!= questButtonNPC[k].position.y)
							{
								tmpPosition = new Vector3(npcQuestObjectives.m_textInstances.position.x,
								                          questButtonNPC[k].position.y,
								                          npcQuestObjectives.m_textInstances.position.z);
								npcQuestObjectives.m_textInstances.position = tmpPosition;
							}
							npcQuestObjectives.m_textInstances.hidden = false;
							lastChar = npcQuestObjectives.m_textInstances.textSprites[npcQuestObjectives.m_textInstances.textSprites.Count-1];
							
							if(npcQuestDetails.m_textInstances.position.y != lastChar.position.y - lastChar.height)
							{
								tmpPosition = new Vector3(npcQuestDetails.m_textInstances.position.x,
								                          lastChar.position.y - lastChar.height,
								                          npcQuestDetails.m_textInstances.position.z);
								npcQuestDetails.m_textInstances.position = tmpPosition;
							}
							npcQuestDetails.m_textInstances.hidden = false;
							lastChar2 = npcQuestDetails.m_textInstances.textSprites[npcQuestDetails.m_textInstances.textSprites.Count-1];
							
							if(qst.getRewChoiceItemCount>0)
								if(rewardItems!=null)
									for(int _k= 0 ; _k < qst.getRewChoiceItemCount ; _k++)
								{
									if(_k==0)
									{
										if(rewardItems[0]!=null)
										{
											tmpPosition = new Vector3(questButtonNPC[k].position.x,
											                          lastChar2.position.y - rewardItems[_k].height,
											                          rewardItems[_k].position.z);
											rewardItems[_k].position = tmpPosition;
											if(rewardItemsText[_k]!=null)
												rewardItemsText[_k].position = rewardItems[_k].position;
										}
									}
									else
										if(rewardItems[_k]!=null)
									{
										tmpPosition = new Vector3(rewardItems[_k-1].position.x,
										                          rewardItems[_k-1].position.y - rewardItems[_k].height*1.1f,
										                          rewardItems[_k].position.z);
										rewardItems[_k].position = tmpPosition;
										if(rewardItemsText[_k]!=null)
											rewardItemsText[_k].position = rewardItems[_k].position;
									} 
								}
							
							if(qst.getReqItemsCount > 0 && requiredItems != null)
							{
								for(int _k = 0 ; _k < qst.getReqItemsCount ; _k++)
								{
									if(_k==0)
									{
										if(requiredItems[0]!=null)
										{	
											tmpPosition = new Vector3(questButtonNPC[k].position.x,
											                          lastChar2.position.y - requiredItems[_k].height,
											                          requiredItems[_k].position.z);
											requiredItems[_k].position = tmpPosition;
											if(requiredItemsText[_k]!=null)
											{
												requiredItemsText[_k].position = requiredItems[_k].position;
											}
										}
									}
									else if(requiredItems[_k]!=null)
									{
										tmpPosition = new Vector3(requiredItems[_k-1].position.x,
										                          requiredItems[_k-1].position.y - requiredItems[_k].height*1.1f,
										                          requiredItems[_k].position.z);
										requiredItems[_k].position = tmpPosition;
										if(requiredItemsText[_k]!=null)
										{
											requiredItemsText[_k].position = requiredItems[_k].position;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		if(mainPlayer.LoggingOut)
		{
			counterText.clear();
			counterText.text = mainPlayer.logOutTimer.ToString();
		}
		
		inventoryManager.UpdateClickStatus();
		
		if(targetState && (targetOldGuid != targetGuid) && !menuState)
		{
			targetOldGuid = targetGuid;
			
			if(targetOldName != targetName)
			{
				targetOldName=targetName;
				mainPlayer.PortraitReff.comboPoints=0;
				
				if(targetName.Length <= 16)
					PortraitsFrame.SetTargetName(targetName);
				else
					PortraitsFrame.SetTargetName(targetName.Substring(0,15) + "..");
			}
		}
		
		if (targetState && !menuState)
		{
			if((targetOldHP != targetHP) || (targetOldMaxHP != targetMaxHP))
			{
				targetOldHP = targetHP;
				targetOldMaxHP = targetMaxHP;
				PortraitsFrame.SetTargetHp(targetHP, targetMaxHP);
			}
			if((targetOldPower != targetPower) || (targetOldMaxPower != targetMaxPower))
			{
				targetOldPower = targetPower;
				targetOldMaxPower = targetMaxPower;
				PortraitsFrame.SetTargetEnergy(targetPower, targetMaxPower);
			}
		}				
		
		if(!menuState)
		{ // if we are out of menu state
			inventoryManager.UpdateItemToDestroy();
			
			if((OldHP != HP) || (OldMaxHP != MaxHP))
			{	
				OldHP = HP;
				OldMaxHP = MaxHP;
				if(PortraitsFrame != null)
				{
					PortraitsFrame.SetUserHp(HP, MaxHP);
				}
			}		
			if((OldPower != Power) || (OldMaxPower != MaxPower))
			{
				OldPower = Power;
				OldMaxPower = MaxPower;
				if(PortraitsFrame != null)
				{
					PortraitsFrame.SetUserEnergy(Power, MaxPower);
				}
			}
		}
		
		if(!menuState)
		{
			if(!PetPortrait && mainPlayer.pet != null)
			{
				PortraitsFrame.SetPetName(mainPlayer.pet.name);
				PortraitsFrame.HidePetPortrait(PetPortrait);
				PetPortrait = true;
			}
			else if(PetPortrait && mainPlayer.pet == null)
			{
				PortraitsFrame.HidePetPortrait(PetPortrait);
				PetPortrait = false;
			}
		}
		
		if(!menuState && PetPortrait)
		{
			PetHP = mainPlayer.pet.HP;
			PetMaxHP = mainPlayer.pet.MaxHP;
			PetPower = mainPlayer.pet.Power;
			PetMaxPower = mainPlayer.pet.MaxPower;
			
			if((PetOldHP != PetHP) || (PetOldMaxHP != PetMaxHP))
			{	
				PetOldHP = PetHP;
				PetOldMaxHP = PetMaxHP;
				PortraitsFrame.SetPetHp(PetHP, PetMaxHP);
			}		
			if((PetOldPower != PetPower) || (PetOldMaxPower != PetMaxPower))
			{	
				PetOldPower = PetPower;
				PetOldMaxPower = PetMaxPower;
				PortraitsFrame.SetPetEnergy(PetPower, PetMaxPower);
			}
		}
		
		if (changeInterface())
			stateManager();
		
		
		//	AICI se seteaza pozitia inventarului cu adevarat
		if(inventoryState)
		{
			if(isNeedRefreshBags)
			{
				inventoryManager.RefreshBags();
			}
			
			if(isNeedRefreshInventory)
			{
				inventoryManager.RefreshInventory();
			}
			
			//Aici se seteaza echipamentu
			
			if(isNeedRefreshEquip)
			{
				inventoryManager.RefreshEquip();
			}
		}
		
		if(!menuState)
		{
			if(refreshAuraM)
			{
				refreshAuraM = false;
				if(PortraitsFrame != null)
				{
					PortraitsFrame.UpdateUserAuras();
				}
			}
		}
		
		
		if(!menuState && targetState)
		{
			if(refreshAura)
			{
				refreshAura = false;
				if(PortraitsFrame != null)
				{
					PortraitsFrame.UpdateTargetAuras();
				}
			}
		}	
	}
	
	public void  stateManager ()
	{
		traitsContainer.hidden = (!traitsActive || !spellAndTraitState);
		spellsAndTraitsContainer.hidden = !spellAndTraitState;
		
		//		progressBar.hidden = menuState || chatState || lootState || !mainPlayer.goCast;
		inventoryManager.inventoryContainer.hidden=!inventoryState;
		inventoryManager.inventoryBagContainer.hidden=!inventoryState;
		inventoryManager.enchantingItems.Destroy();
		inventoryManager.petFeeding.Destroy();
		
		for(int i=0;i<noSpell;i++)
		{
			spell[i].hidden=!spellState;
			//SpellText[i].hidden=!spellState;
		}
		
		spellButtonText.hidden = !spellAndTraitState;
		traitsButtonText.hidden = !spellAndTraitState;
		glyphsButtonText.hidden = !spellState;
		petTraitButtontText.hidden = !spellState;
		
		defaultText.hidden=!spellState;
		classText.hidden=!spellState;
		itemText.hidden=!spellState;
		MountsText.hidden=!spellState;
		items.hidden=!spellState;
		classSpells.hidden=!spellState;
		defaultSpells.hidden=!spellState;
		Mounts.hidden=!spellState; 
		
		if (SpellBuyMithril != null)
		{
			SpellBuyMithril.hidden=!spellState;
		}
		spellLeftBackGround.hidden=!spellState;
		spellSmallBackGround.hidden=!spellState;
		spellRightBackGround.hidden=!spellState;
		
		counterText.hidden = !mainPlayer.LoggingOut;
		pleaseWaitText.hidden = !mainPlayer.LoggingOut;
		loggingOutText.hidden = !mainPlayer.LoggingOut;
		cancelLogOutText.hidden = !mainPlayer.LoggingOut;
		LogOutBackGround.hidden = !mainPlayer.LoggingOut;
		cancelLogOutButton.hidden = !mainPlayer.LoggingOut;
		cancelLogOutButtonX.hidden = !mainPlayer.LoggingOut;
		counterCase.hidden = !mainPlayer.LoggingOut;
		decorationTexture3.hidden = !mainPlayer.LoggingOut;
		decorationTexture4.hidden = !mainPlayer.LoggingOut;
		
		if(!npcMode)
			mainMenuContainer.hidden = !menuState;
		
		if(!editMacrosState)
		{
			rowMainMenu.hidden = !menuState;
			for (int j= 0; j < woodenBarBackGroundNumber; j++) 
				woodenBarBackGround[j].hidden =! menuState;
		}
		
		//	logoutButton.hidden = !((!questState && !inventoryState && !spellState) && menuState);
		//	reportBugButton.hidden = !((!questState && !inventoryState && !spellState) && menuState);
		
		//logoutText.hidden = logoutButton.hidden;
		
		// USER INTERFACE
		PortraitsFrame.HideUserPortrait(menuState || WorldSession.interfaceManager.hideInterface);
		PortraitsFrame.HideTargetPortrait(!targetState || menuState || WorldSession.interfaceManager.hideInterface);
		
		if(PetPortrait)
		{
			PortraitsFrame.HidePetPortrait(menuState);
		}
		
		if((mainPlayer.spellManagerStatus&32)!=32)
			mainPlayer.spellManagerStatus += 32;
		
		reviveWindow.hidden = menuState || chatState || mainPlayer.ShowReviveDialogue;
		
		if(defaultSpellBook != null)
			defaultSpellBook.hidden = !spellState;
		if(classSpellBook != null)
			classSpellBook.hidden = !spellState;
		if(itemSpellBook != null)
			itemSpellBook.hidden = !spellState;
		if(mountsSpellBook != null)
			mountsSpellBook.hidden = !spellState;
		
		if(backg != null)
			backg.hidden=!spellState; 
		if(spellInfo != null)
			spellInfo.hidden=!spellState;
		if(arrowUp != null)
			arrowUp.hidden=!spellState;
		if(arrowDown != null)
			arrowDown.hidden=!spellState;
		
		//	spellInfoDescription.text=" ";
		
		if(mainPlayer.LoggingOut || mainPlayer.menuSelected != -1)
			mainMenuContainer.hidden = true;
	}
	
	bool changeInterface ()
	{
		if ((targetStateOld != targetState) || (menuStateOld != menuState) ||
		    (spellAndTraitStateOld != spellAndTraitState) || (chatStateOld != chatState) || (diveStateOld != diveState) ||
		    (optionsStateOld != optionsState) || (partyStateOld != partyState) || (questStateOld != questState) ||
		    (inventoryStateOld != inventoryState) || (lootStateOld != lootState) || (needStateOld != needState))
		{
			needStateOld=needState;
			lootStateOld=lootState;
			inventoryStateOld=inventoryState;
			partyStateOld=partyState;
			questStateOld=questState;
			optionsStateOld=optionsState;
			targetStateOld=targetState;
			diveStateOld=diveState;
			menuStateOld=menuState;
			spellAndTraitStateOld=spellAndTraitState;
			chatStateOld=chatState;
			return true;
		}
		else 
			return false;
	}
	
	public void  ReleaseSpirit ()
	{
		Debug.Log("Releasse Spirit?");
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_REPOP_REQUEST,3);
		byte aux8 = 0;
		pkt.Append(aux8);
		RealmSocket.outQueue.Add(pkt);	
		
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.MSG_CORPSE_QUERY);
		RealmSocket.outQueue.Add(pkt);  	
	}	
	
	public void  ReclaimCorpse ()
	{
		Debug.Log("Revive!");
		WorldPacket pkt;
		pkt  = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_RECLAIM_CORPSE);
		pkt.Append(ByteBuffer.Reverse(MainPlayer.GUID));
		RealmSocket.outQueue.Add(pkt);  
		
	}
	
	public void  startMenu ()
	{
		UIButton random = null;
		startMenu(random);
		mainPlayer.questManager.getPlayerListQuests();
	}
	public void  startMenu ( UIButton sender )
	{
		
		WorldSession.interfaceManager.SetWindowState(InterfaceWindowState.PLAYER_MENU_WINDOW_STATE);
		mainPlayer.blockTargetChange = true;
		
		if ( mainPlayer.canShowMenu )
		{
			menuState = true;
			mainPlayer.mailHandler.mailUI.UniversalExitMail();
			mainPlayer.MoveStop();
			mainPlayer.MoveStopStrafe();
			mainPlayer.MoveStopTurn();
			mainPlayer.RefreshInventory(-1,-1);
			resetInventoryStatsMode(0);
			stateManager();
		}
	}
	
	public void  startSwitch ( UIButton sender,   int state )
	{
		if(WorldSession.interfaceManager.hideInterface == false)
		{
			WorldSession.player.actionBarPanel.RefreshSlots();
		}
		switchMode = state;
		WorldSession.player.actionBarPanel.switchMode = (uint)switchMode;
		//Debug.Log("state:"+state);
		if(sender.info > 0)
		{
			DrawSpellButtons(true);
		}
		/*	for(int i =0;i<8;i++)
			{
			
			if(!mainPlayer._equipSlots[i+14*switchMode].isEmpty){
				slotText[i].clear();
			slotMenuText[i].clear();
			slotText[i].text=mainPlayer._equipSlots[i+14*switchMode].name;
			slotMenuText[i].text=mainPlayer._equipSlots[i+14*switchMode].name;
			}
			else{
				slotText[i].clear();
			slotMenuText[i].clear();
			slotText[i].text="A"+((i+1)+(switchMode*14));
			slotMenuText[i].text="A"+((i+1)+(switchMode*14));
			}
			slotText[i].hidden= menuState;
			slotMenuText[i].hidden= !spellState;
			
			}*/
	}
	
	public void  backMode ( UIButton sender )
	{
		WorldSession.interfaceManager.SetWindowState(InterfaceWindowState.ACTION_BAR_PANEL_STATE);
		inventoryManager.HideClassStats();
		mainPlayer.actionBarPanel.RefreshSlots();
		setInventoryState(false);
		
		setInventoryState(false);
		questState = false;
		SpellState(false);
		traitsWindow.destroy();
		RaidWindowManager.closeRaidWindow();
		spellAndTraitState = false;
		optionsState = false;
		partyState = false;
		mithrilShopState = false;
		wip1State = false;
		wip2State = false;
		mainPlayer.menuSelected = 0;
		MainPlayer.waitingToEquip = false;
		//		mainPlayer.toggleLeft.hidden = false;
		//		mainPlayer.toggleRight.hidden = false;
		menuState = false;
		WorldSession.interfaceManager.hideInterface = false;
		mainPlayer.actionBarPanel.RefreshSlots();
		
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		
		InitQuestInterf(false);
		
		bool  state = Convert.ToBoolean(PlayerPrefs.GetInt("UseQuestArrow", 1));
		if(WorldSession.player)
		{
			WorldSession.player.questManager.questArrowController.setUseQuestArrow(state);
		}
		
		ExitMenu();
	}
	
	public void  inventoryMode ( UIButton sender )
	{
		WorldSession.interfaceManager.playerMenuMng.SetWindowState(PlayerMenuWindowState.INVENTORY_WINDOW_STATE);
		MainPlayer.updateSlots=true;
		isNeedRefreshBags=true;
		mainPlayer.inventoryCameraReff.gameObject.SetActive(true);
		//		setInventoryState(true);
		questState =false;
		SpellState(false);
		traitsWindow.destroy();
		RaidWindowManager.closeRaidWindow();
		spellAndTraitState = false;
		optionsState = false;
		partyState = false;
		mithrilShopState = false;
		wip1State = false;
		wip2State = false;
		mainPlayer.menuSelected=1;
		mainPlayer.session._firstVisit = 1;
		WorldSession.RequestMithrilCoinsCount();
		mainMenuContainer.hidden = true;
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		InitQuestInterf(false);
		resetInventoryStatsMode(0);
	}
	public void  questMode ( UIButton sender )
	{
		WorldSession.interfaceManager.playerMenuMng.SetWindowState(PlayerMenuWindowState.QUEST_WINDOW_STATE);
		inventoryManager.HideClassStats();
		setInventoryState(false);
		questState =true;
		SpellState(false);
		traitsWindow.destroy();
		RaidWindowManager.closeRaidWindow();
		spellAndTraitState = false;
		optionsState = false;
		partyState = false;
		mithrilShopState = false;
		wip1State = false;
		wip2State = false;
		mainPlayer.menuSelected=2;
		MainPlayer.waitingToEquip = false;
		mainMenuContainer.hidden = true;
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		InitQuestInterf(true);
	}
	
	public void  traitsMode ( UIButton sender )
	{
		WorldSession.interfaceManager.playerMenuMng.SetWindowState(PlayerMenuWindowState.SPELL_WINDOW_STATE);
		inventoryManager.HideClassStats();
		//refreshSlotMenu=true;
		setInventoryState(false);
		questState =false;
		SpellState(false);
		traitsWindow.destroy();
		RaidWindowManager.closeRaidWindow();
		traitsActive = true;
		spellAndTraitState = true;
		optionsState = false;
		partyState = false;
		mithrilShopState = false;
		wip1State = false;
		wip2State = false;
		mainPlayer.menuSelected=11;
		MainPlayer.waitingToEquip = false;
		
		spellorTraitButton[3].hidden = true;
		if(spellorTraitButton[2] != null)
		{
			spellorTraitButton[2].hidden = true;
		}
		
	}
	public void  spellMode ( UIButton sender )
	{
		WorldSession.interfaceManager.playerMenuMng.SetWindowState(PlayerMenuWindowState.SPELL_WINDOW_STATE);
		inventoryManager.HideClassStats();
		//refreshSlotMenu=true;
		setInventoryState(false);
		questState =false;
		SpellState(true);
		traitsWindow.destroy();
		RaidWindowManager.closeRaidWindow();
		spellAndTraitState = true;
		optionsState = false;
		partyState = false;
		mithrilShopState = false;
		wip1State = false;
		wip2State = false;
		mainPlayer.menuSelected=3;
		MainPlayer.waitingToEquip = false;
		mainMenuContainer.hidden = true;
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		
		spellorTraitButton[3].hidden = false;
		if(spellorTraitButton[2] != null && spellorTraitButton[2].hidden == true)
		{
			spellorTraitButton[2].hidden = false;
		}
		
		InitQuestInterf(false);
	}
	
	public void  optionsMode ( UIButton sender )
	{
		WorldSession.interfaceManager.playerMenuMng.SetWindowState(PlayerMenuWindowState.OPTION_WINDOW_STATE);
		inventoryManager.HideClassStats();
		setInventoryState(false);
		questState =false;
		SpellState(false);
		traitsWindow.destroy();
		RaidWindowManager.closeRaidWindow();
		spellAndTraitState = false;
		optionsState = true;
		partyState = false;
		mithrilShopState = false;
		wip1State = false;
		wip2State = false;
		mainPlayer.menuSelected=7;
		MainPlayer.waitingToEquip = false;
		mainMenuContainer.hidden = true;
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		InitQuestInterf(false);
	}
	
	void  InitMainMenu ()
	{	
		mainMenuContainer.removeAllChild(true);
		
		menuBackGround = UIPrime31.myToolkit4.addSprite( "main_menu_beckground.png", -2, Screen.height * 0.08f, 5);
		menuBackGround.setSize(Screen.width * 1.002f,Screen.height * 0.92f);
		menuBackGround.hidden=!menuState;
		
		decorationTexture5 = UIPrime31.myToolkit3.addSprite( "decoration_texture2.png", 0, Screen.height * 0.112f, -3);
		decorationTexture5.setSize(Screen.width,Screen.height * 0.03f );
		decorationTexture5.hidden=!menuState;
		
		
		
		ItemShopButton = UIButton.create( UIPrime31.myToolkit4, "button_ITEM SHOP_idle.png", "button_ITEM SHOP_selected.png", Screen.width*0.08f, Screen.height*0.18f, -1);
		ItemShopButton.setSize(Screen.width*0.4f,Screen.height*0.2f);
		ItemShopButton.onTouchUpInside += ItemShopDelegate;
		ItemShopButton.hidden = !menuState;
		
		GetFreeMithrillButton = UIButton.create( UIPrime31.myToolkit4, "button_GET FREE MITHRIL_idle.png", "button_GET FREE MITHRIL_selected.png", Screen.width*0.08f, - ItemShopButton.position.y + ItemShopButton.height*1.3f, -1);
		GetFreeMithrillButton.setSize(Screen.width*0.4f,Screen.height*0.2f);
		GetFreeMithrillButton.onTouchUpInside += FreeMithrilDelegate;
		GetFreeMithrillButton.hidden = !menuState;
		
		leaderboardButton = UIButton.create( UIPrime31.myToolkit4,"button_LEADERBOARD_Normal.png","button_LEADERBOARD_down.png", Screen.width*0.08f, - GetFreeMithrillButton.position.y + GetFreeMithrillButton.height*1.3f, -1);
		leaderboardButton.setSize(Screen.width*0.4f,Screen.height*0.2f);
		leaderboardButton.onTouchUpInside +=LeaderboardButtonDelegate;
		leaderboardButton.hidden = !menuState;
		
		
		
		unstuckButton = UIButton.create( UIPrime31.myToolkit4,"button_UNSTUCK_idle.png","button_UNSTUCK_selected.png", Screen.width*0.52f, -ItemShopButton.position.y * 1.07f, -1 );
		unstuckButton.setSize(Screen.width*0.4f,Screen.height*0.18f);
		unstuckButton.onTouchUpInside +=unstuckButtonDelegate;
		unstuckButton.hidden = !menuState;
		
		helpButton = UIButton.create( UIPrime31.myToolkit4,"button_HELP_idle.png","button_HELP_selected.png", Screen.width*0.52f, -GetFreeMithrillButton.position.y * 1.03f, -1);
		helpButton.setSize(Screen.width*0.4f,Screen.height*0.18f);
		helpButton.onTouchUpInside +=HelpButtonDelegate;
		helpButton.hidden = !menuState;
		
		mithrilShopButtonAmazon = UIButton.create( UIPrime31.myToolkit4, "button_BUY MITHRIL_idle.png", "button_BUY MITHRIL_selected.png", Screen.width*0.52f, -leaderboardButton.position.y, -1);// "cbDown.png", "cbDown.png", Screen.width * 0.7f, Screen.height * 0.55f);
		mithrilShopButtonAmazon.setSize(Screen.width*0.4f,Screen.height*0.2f);
		if(BuildConfig.platformName == "amazon")
		{
			mithrilShopButtonAmazon.onTouchUpInside += BuyMithrilIapAmazonDelegate;
		}
		else
		{
			mithrilShopButtonAmazon.onTouchUpInside += BuyMithrilIapDelegate;
		}
		mithrilShopButtonAmazon.hidden = !menuState;
		
		
		
		mainMenuContainer.addChild(menuBackGround, decorationTexture5, unstuckButton, leaderboardButton);
		if(BuildConfig.hasHelpButton)
		{
			mainMenuContainer.addChild(helpButton);
		}
		if(BuildConfig.hasPurchaseMithrilButton)
		{
			mainMenuContainer.addChild(mithrilShopButtonAmazon);
		}
		if(BuildConfig.hasFreeMithrilButton)
		{
			mainMenuContainer.addChild(GetFreeMithrillButton);
		}
		if(BuildConfig.hasItemShopButton)
		{
			mainMenuContainer.addChild(ItemShopButton);
		}
		mainMenuContainer.hidden = !menuState;
	}
	
	/////////////////////////////////////////MENU BUTTONS/////////////////////////////////////
	
	void  QuestArrowToggleChange ( UIToggleButton sender,   bool state )
	{
		PlayerPrefs.SetInt("UseQuestArrow", ((state) ? 1 : 0));
		if(WorldSession.player)
		{
			WorldSession.player.questManager.questArrowController.setUseQuestArrow(state);
		}
	}
	
	void  JapaneseCameraSwitch ( UIToggleButton sender,   bool state )
	{
		PlayerPrefs.SetInt("JapaneseCamera",(state) ? 1 : 0);
		WorldSession.player.swipeScript.ChangeLayerMask();
	}
	
	/////////////////////////////////////////END OF MENU BUTTONS/////////////////////////////////////
	public void  SocialDelegate ( UIButton sender )
	{
		WorldSession.interfaceManager.playerMenuMng.SetWindowState(PlayerMenuWindowState.SOCIAL_WINDOW_STATE);
		mainPlayer.SocialMgr.SendContactList();
		optionsState = false;
		setInventoryState(false);
		wip1State = false;
		questState =false;
		SpellState(false);
		traitsWindow.destroy();
		RaidWindowManager.closeRaidWindow();
		spellAndTraitState = false;
		mainPlayer.menuSelected = 101;
		
		//ShowDMGAllPlayersButton.hidden = true;
		//mithrilShopTextAmazon.hidden = true;
		mainMenuContainer.hidden = true;
		inventoryManager.HideClassStats();
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		InitQuestInterf(false);
		
	}
	
	public void  DisableMenuButtons ( bool choise )
	{
		shopButtonNew.disabled = choise;
		backButton.disabled = choise;
		inventoryButton.disabled = choise;
		questButton.disabled = choise;
		spellButton.disabled = choise;
		guildButton.disabled = choise;
		socialButton.disabled = choise;
		optionsButton.disabled = choise;
		logoutButton.disabled = choise;
	}
	
	void  DestroyPopupWindow ()
	{
		ButtonOLD.summonState = false;	
		
		if(summonText != null)
		{
			summonText.clear();
		}
		popupContainer.removeAllChild(true);
	}
	
	void  SpellsDelegate ( UIButton sender )
	{
		WorldSession.interfaceManager.playerMenuMng.SetWindowState(PlayerMenuWindowState.SPELL_WINDOW_STATE);
		RaidWindowManager.closeRaidWindow();
		SpellState(true);
		mainPlayer.menuSelected = 3;
		traitsWindow.destroy();
		
		stateManager();
		defaultSpell(null);
		
		spellorTraitButton[3].hidden = false;
		if(spellorTraitButton[2] != null && spellorTraitButton[2].hidden == true)
		{
			spellorTraitButton[2].hidden = false;
		}
		//Debug.Log("clicked spellsdelegate");
		
	}
	
	void  GlyphsDelegate ( UIButton sender )
	{
		SpellState(false);
		mainPlayer.menuSelected = 11;
		stateManager();
		spellorTraitButton[3].hidden = true;
		if(spellorTraitButton[2] != null)
		{
			spellorTraitButton[2].hidden = true;
		}
		WorldSession.interfaceManager.playerMenuMng.SpellBookMng.SetWindowState(SpellBookWindowState.GLYPHS_WINDOW_STATE);
	}
	
	void  TraitsDelegate ( UIButton sender )
	{
		SpellState(false);
		
		mainPlayer.menuSelected = 11;
		stateManager();
		
		traitsWindow.destroy();
		TraitsManager.setActiveTraitList(1);
		traitsWindow.initTraitsWindow();
	}
	
	void  TraitsPetDelegate ( UIButton sender )
	{			
		if(mainPlayer.pet == null)
		{
			return;
		}
		
		SpellState(false);
		mainPlayer.menuSelected = 11;
		stateManager();

		TraitsManager.setActiveTraitList(0);
		traitsWindow.petFamily = GetActualTallentTree();
		traitsWindow.initPetTraitsWindow();
	}
	
	public int GetActualTallentTree ()
	{
		int category = 0;
		switch(mainPlayer.petFamily)
		{
		case 1:
		case 2:
		case 7:
		case 11:
		case 12:
		case 25:
		case 37:
		case 39:
		case 44:
		case 45:
		case 46:
			category = 0;
			break;
		case 4:
		case 5:
		case 6:
		case 8:
		case 9:
		case 20:
		case 21:
		case 32:
		case 42:
		case 43:
			category = 1;
			break;
		default:
			category = 2;
			break;
		}
		return category;
	}
	
	//**************************End of traits Delegates***************************/
	
	
	
	//************************************TURN OFF MENU BUTTONS********************/
	//************************************TURN OFF MENU BUTTONS********************/
	
	void  TurnOffMenuButtons ()
	{
		mainPlayer.interf.optionsButton.disabled = true;
		mainPlayer.interf.socialButton.disabled = true;
		mainPlayer.interf.guildButton.disabled = true;
		mainPlayer.interf.spellButton.disabled = true;
		mainPlayer.interf.questButton.disabled = true;
		mainPlayer.interf.inventoryButton.disabled = true;
		mainPlayer.interf.backButton.disabled = true;
		mainPlayer.interf.shopButtonNew.disabled = true;
		mainPlayer.interf.logoutButton.disabled = true;
	}
	
	void  TurnOnMenuButtons ()
	{
		mainPlayer.interf.optionsButton.disabled = false;
		mainPlayer.interf.socialButton.disabled = false;
		mainPlayer.interf.guildButton.disabled = false;
		mainPlayer.interf.spellButton.disabled = false;
		mainPlayer.interf.questButton.disabled = false;
		mainPlayer.interf.inventoryButton.disabled = false;
		mainPlayer.interf.backButton.disabled = false;
		mainPlayer.interf.shopButtonNew.disabled = false;
		mainPlayer.interf.logoutButton.disabled = false;
	}
	
	//************************************TURN OFF MENU BUTTONS********************/
	//************************************TURN OFF MENU BUTTONS********************/
	
	
	//*************************Spells - Description PopUp Window - SEBEK **************************************/
	
	void  DestroySpellButtons ()
	{
		spellCastPortrait[0].hidden = true;  //needs to be set to false to work
		spellCastPortrait[1].hidden = true;
		spellCastPortraitText[0].hidden=true;
		spellCastPortraitText[1].hidden=true;
		portraitSpellEquipBarsHealth1.hidden=true;
		portraitSpellEquipBarsMana1.hidden=true;
		portraitSpellEquipBarsHealth2.hidden=true;
		portraitSpellEquipBarsMana2.hidden=true;
		for (int i= 0; i < ACTION_BUTTONS_COUNT; i++)
		{
			ClearSpellEquipSlot(spellEquipSlot[i]);
		}
	}
	
	void  DrawSpellButtons ( bool active )
	{
		int sw = Screen.width;
		int sh = Screen.height;
		
		string tempIconName;
		bool  isButtonDisabel;
		
		for (int index = BOTTOM_BUTTON_INDEX; index < ACTION_BUTTONS_COUNT; index++)
		{
			ClearSpellEquipSlot(spellEquipSlot[index]);
			
			isButtonDisabel = false;
			if(mainPlayer.pet
			   && ((isPetSpell && !(switchMode == 0 && index >= RIGHT_EXTENDED_BUTTON_INDEX))
			    || (!isPetSpell && (switchMode == 0 && index >= RIGHT_EXTENDED_BUTTON_INDEX))))
			{
				tempIconName = "lock_button.png";
				isButtonDisabel = true;
			}
			else
			{
				tempIconName = RefreshIcon(index + ACTION_BUTTONS_COUNT * switchMode);
				if(tempIconName == "noicon.png") 
				{ 
					tempIconName = "tab_solo.png";
				}
			}
			UIToolkit spellEquipSlotUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+tempIconName);
			spellEquipSlot[index] = UIButton.create(spellEquipSlotUIToolkit, tempIconName, tempIconName, 0, 0, 100);
			spellEquipSlot[index].setSize(Screen.height * 0.095f, Screen.height * 0.095f);
			spellEquipSlot[index].info = index;
			spellEquipSlot[index].onTouchDown += initEquipSpellConfirmation;
			spellEquipSlot[index].disabled = isButtonDisabel;
			
			float buttonSpacing;
			
			if(index < RIGHT_BUTTON_INDEX)
			{
				buttonSpacing = sw * 0.1f * (index);
				spellEquipSlot[index].position = new Vector3(sw * 0.23f + buttonSpacing, -sh * 0.86f, -0.21f);
			}
			else if (index < RIGHT_EXTENDED_BUTTON_INDEX)
			{
				buttonSpacing = sw * 0.08f * (index - RIGHT_BUTTON_INDEX);
				spellEquipSlot[index].position = new Vector3(sw * 0.91f, -sh * 0.15f - buttonSpacing, -0.21f);
			}
			else
			{
				buttonSpacing = sw * 0.08f * (index - RIGHT_EXTENDED_BUTTON_INDEX + 1);
				spellEquipSlot[index].position = new Vector3(sw * 0.83f, -sh * 0.15f - buttonSpacing, -0.22f);
			}
		}
	}	
	
	void  initSpellDescriptionPopup ( UICell sender )
	{			
		
		if(!spellDescriptionActive && !initEquipSpellConfirmationActive) 
		{
			HideSpellBook(true);
			classText.hidden = true;
			defaultText.hidden = true;
			itemText.hidden = true;
			MountsText.hidden = true;
			
			spellButtonText.hidden = true;
			traitsButtonText.hidden = true;
			glyphsButtonText.hidden = true;
			petTraitButtontText.hidden = true;
			
			items.disabled=true;
			classSpells.disabled=true;
			defaultSpells.disabled=true;
			Mounts.disabled=true; 
			if(SpellBuyMithril != null)
			{
				SpellBuyMithril.disabled = true;
			}
			
			spellorTraitButton[0].disabled = true;
			spellorTraitButton[1].disabled = true;
			spellorTraitButton[3].disabled = true;
			
			if(spellorTraitButton[2] != null)
			{
				spellorTraitButton[2].disabled = true;
			}
			
			//Debug.Log("Spell id:"+sender.Obj._entry);
			equipConfSpellYESbutton.oInfo = sender.info;
			spellDescriptionActive = true; 
			//spellDescriptionContainer.removeAllChild(true);
			string iconName = DefaultIcons.GetSpellIcon(sender.info);
			
			SinglePictureUtils.DestroySingleUISprite(descriptionPopupSpellIcon);
			descriptionPopupSpellIcon = addIconSprite(iconName, 0, 0, -1); 
			descriptionPopupSpellIcon.setSize(Screen.width * 0.067f, Screen.height * 0.115f);
			descriptionPopupSpellIcon.position=new Vector3(Screen.width * 0.0933f, -Screen.height * 0.17f,-0.1f);
			descriptionPopupSpellIcon.hidden = true;
			
			//			spellDescriptionText = new NonUIElements.TextBox(" ",new Vector3(Screen.width * 0.09f, Screen.height * 0.33f, -0.2f), new Vector2(Screen.width * 0.2f, 1) * UID.TEXT_SIZE,5,Color.black, UID.TEXT_SIZE*0.7f,"fontiny dark");
			//			spellDescriptionText.size = new Vector2(Screen.width * 0.0225f*5/4,0);
			
			
			spellDescriptionBkg.hidden = false;
			descriptionPopupSpellIcon.hidden = false;
			cancelSpellDescrButton.hidden = false;
			
			equipButton.hidden = false;
			equipSpellDescriptionText.hidden=false;				
			cancelSpellDescriptionText.hidden=false;
			
			
			SpellEntry sp = dbs.sSpells.GetRecord(mainPlayer.playerSpellManager.playerSpellBook[(uint)sender.info].ID);
			selectedSpell = mainPlayer.playerSpellManager.GetSpellByID((uint)sender.info);
			isPetSpell = false;
			
			
			spellDescriptionText = new NonUIElements.TextBox(" ",new Vector3(Screen.width * 0.09f, Screen.height * 0.33f, -0.2f), new Vector2(Screen.width * 0.2f, 1) * UID.TEXT_SIZE,5,Color.black, UID.TEXT_SIZE*0.7f,"fontiny dark");
			spellDescriptionText.size = new Vector2(Screen.width * 0.8f, 0);
			//Debug.Log(sp.Description.Strings[1]);
			spellDescriptionText.text = ("\n" + sp.Description.Strings[1]);
			
			descriptionPopupSpellText = text2.addTextInstance(" ",Screen.width * 0.18f, Screen.height * 0.24f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
			descriptionPopupSpellText.text = (sp.SpellName.Strings[0] );
			
			levelPopupSpellText = text2.addTextInstance(" ",Screen.width * 0.72f, Screen.height * 0.24f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
			levelPopupSpellText.text = (sp.Rank.Strings[0]);
			
			
		}
		
	}	
	
	void  initPetSpellDescriptionPopup ( UICell sender )
	{		
		if(!spellDescriptionActive && !initEquipSpellConfirmationActive) 
		{
			HideSpellBook(true);
			classText.hidden = true;
			defaultText.hidden = true;
			itemText.hidden = true;
			MountsText.hidden = true;
			
			spellButtonText.hidden = true;
			traitsButtonText.hidden = true;
			glyphsButtonText.hidden = true;
			petTraitButtontText.hidden = true;
			
			if(SpellBuyMithril != null)
			{
				SpellBuyMithril.disabled = true;
			}
			
			mountsSpellBook.scrollStop = true;
			
			items.disabled=true;
			classSpells.disabled=true;
			defaultSpells.disabled=true;
			Mounts.disabled=true; 
			
			
			spellorTraitButton[0].disabled = true;
			spellorTraitButton[1].disabled = true;
			spellorTraitButton[3].disabled = true;
			
			if(spellorTraitButton[2] != null)
			{
				spellorTraitButton[2].disabled = true;
			}
			
			equipConfSpellYESbutton.oInfo = sender.info;
			spellDescriptionActive = true; 
			string iconName = DefaultIcons.GetSpellIcon(sender.info);
			
			SinglePictureUtils.DestroySingleUISprite(descriptionPopupSpellIcon);
			descriptionPopupSpellIcon = addIconSprite(iconName, 0, 0, 0); 
			descriptionPopupSpellIcon.setSize(Screen.width * 0.067f, Screen.height * 0.115f);
			descriptionPopupSpellIcon.position=new Vector3(Screen.width * 0.0933f, -Screen.height * 0.17f,-0.1f);
			descriptionPopupSpellIcon.hidden = true;
			
			
			spellDescriptionBkg.hidden = false;
			descriptionPopupSpellIcon.hidden = false;
			cancelSpellDescrButton.hidden = false;
			
			equipButton.hidden = false;
			equipSpellDescriptionText.hidden=false;				
			cancelSpellDescriptionText.hidden=false;
			
			
			SpellEntry sp = dbs.sSpells.GetRecord(sender.info);
			selectedSpell = mainPlayer.petSpellManager.GetPetSpellByID((uint)sender.info);
			isPetSpell = true;
			
			spellDescriptionText = new NonUIElements.TextBox(" ",new Vector3(Screen.width * 0.09f, Screen.height * 0.33f, -0.2f), new Vector2(Screen.width * 0.2f, 1) * UID.TEXT_SIZE,5,Color.black, UID.TEXT_SIZE*0.7f,"fontiny dark");
			spellDescriptionText.size = new Vector2(Screen.width * 0.8f, 0);
			spellDescriptionText.text = ("\n" + sp.Description.Strings[1]);
			
			descriptionPopupSpellText = text2.addTextInstance(" ",Screen.width * 0.18f, Screen.height * 0.24f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
			descriptionPopupSpellText.text = (sp.SpellName.Strings[0]);
			
			levelPopupSpellText = text2.addTextInstance(" ",Screen.width * 0.72f, Screen.height * 0.24f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
			levelPopupSpellText.text = (sp.Rank.Strings[0]);
		}
	}
	
	void  initItemDescriptionPopup ( UIButton sender )
	{			
		
		if(!spellDescriptionActive && !initEquipSpellConfirmationActive) 
		{
			HideSpellBook(true);
			classText.hidden = true;
			
			defaultText.hidden = true;
			
			itemText.hidden = true;
			
			MountsText.hidden = true; 
			
			spellButtonText.hidden = true;
			traitsButtonText.hidden = true;
			glyphsButtonText.hidden = true;
			petTraitButtontText.hidden = true;
			
			items.disabled=true;
			classSpells.disabled=true;
			defaultSpells.disabled=true;
			Mounts.disabled=true; 
			if(SpellBuyMithril != null)
			{
				SpellBuyMithril.disabled = true;
			}
			
			spellorTraitButton[0].disabled = true;
			spellorTraitButton[1].disabled = true;
			spellorTraitButton[3].disabled = true;
			
			if(spellorTraitButton[2] != null)
			{
				spellorTraitButton[2].disabled = true;
			}
			
			if (mainPlayer.spellCategory == ActionButtonType.ACTION_BUTTON_ITEM)
				equipConfSpellYESbutton.oInfo=sender.oInfo; 
			if (mainPlayer.spellCategory == ActionButtonType.ACTION_BUTTON_MACRO)
				equipConfSpellYESbutton.oInfo=sender.oInfo;
			spellDescriptionActive = true; 
			string spellImage = "noicon.png";
			string itemDescription = "";
			//			Debug.Log("spell category: " + mainPlayer.spellCategory);
			if (mainPlayer.spellCategory == ActionButtonType.ACTION_BUTTON_MACRO)
			{
				
				switch (sender.oInfo)
				{
				case 0:
					spellImage = jumpImage;
					break;
					
				case 1:
					spellImage = selectEnemyImage;
					break;
				case 2:
					spellImage = objectTabImage;
					break;
				case 3:
					spellImage = tradeImage;
					break;					
				case 4:
					spellImage = battleImage;
					break;
				case 5:
					spellImage = inspectImage;
					break;
				case 6:
					spellImage = invitePartyImage;
					break;
				case 7:
					spellImage = leavePartyImage;
					break;
				case 23:
					spellImage = AssaultImage;
					break;					
				case 24:
					spellImage = EscortImage;
					break;
				case 25:
					spellImage = FollowImage;
					break;
				case 26:
					spellImage = HaltImage;
					break;
				case 27:
					spellImage = HostileImage;
					break;
				case 28:
					spellImage = InactiveImage;
					break;
				}
			}
			else 
			{
				spellImage = DefaultIcons.GetItemIcon((uint)sender.oInfo);
				Debug.Log("spellimg: " + spellImage);
				
				Item item = mainPlayer.itemManager.GetItemByID((uint)sender.oInfo);
				itemDescription = item.description;
				if(itemDescription == "\0")
				{
					itemDescription = item.GetItemStats();
				}
			}
			
			
			//Debug.Log("spell img: " + spellImage);
			SinglePictureUtils.DestroySingleUISprite(descriptionPopupSpellIcon);
			descriptionPopupSpellIcon = addIconSprite(spellImage, 0, 0, 0);
			descriptionPopupSpellIcon.setSize(Screen.width * 0.067f, Screen.height * 0.115f);
			descriptionPopupSpellIcon.position=new Vector3(Screen.width * 0.0933f, -Screen.height * 0.17f,-0.1f);
			descriptionPopupSpellIcon.hidden = true;
			
			spellDescriptionText = new NonUIElements.TextBox(itemDescription, new Vector3(Screen.width * 0.09f, Screen.height * 0.33f, -0.2f), new Vector2(Screen.width * 0.2f, 1) * UID.TEXT_SIZE,5,Color.black, UID.TEXT_SIZE*0.7f,"fontiny dark");
			spellDescriptionText.size = new Vector2(Screen.width * 0.8f, Screen.height * 0.5f);
			
			
			spellDescriptionBkg.hidden = false;
			descriptionPopupSpellIcon.hidden = false;
			cancelSpellDescrButton.hidden = false;
			
			equipButton.hidden = false;
			equipSpellDescriptionText.hidden=false;				
			cancelSpellDescriptionText.hidden=false;
			
			
			if (mainPlayer.spellCategory == ActionButtonType.ACTION_BUTTON_MACRO)
			{
				
				switch (sender.oInfo) //aici
				{				
				case 0:
					descText = "Jump";
					break;
				case 1:
					descText = "Closest Target";
					break;
				case 2:
					descText = "Next Target";
					break;
				case 3:
					descText = "Trade";
					break;
				case 4: 
					descText = "Battle";
					break;
				case 5: 
					descText = "Examine";
					break;
				case 6: 
					descText = "Invite to party";
					break;
				case 7: 
					descText = "Leave party";
					break;
				case 23:
					descText = "Assault";
					break;
				case 24: 
					descText = "Escort";
					break;
				case 25: 
					descText = "Follow";
					break;
				case 26: 
					descText = "Halt";
					break;
				case 27: 
					descText = "Hostile";
					break;
				case 28: 
					descText = "Inactive";
					break;
				case 29: 
					descText = "Send Beast Away";
					break;
				case 30: 
					descText = "Hail Beast";
					break;
				case 31: 
					descText = "Resurrect Beast";
					break;
				case 32: 
					descText = "Curb Beast";
					break;
				case 33: 
					descText = "Call Boarded Beast";
					break;
				}
			}
			else 
				descText = AppCache.sItems.GetItemEntry(sender.oInfo).Name;
			
			//			string descTextdesc;
			//			descTextdesc = AppCache.sItems.GetItemEntry(sender.info).Name;
			descriptionPopupSpellText = text2.addTextInstance(" ",Screen.width * 0.18f, Screen.height * 0.24f,UID.TEXT_SIZE,-1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
			descriptionPopupSpellText.text = ( descText );
			
			//			spellDescriptionText.text = ("\n" + descTextdesc);
			
		}
		
	}
	
	void  initMacrosDescriptionPopup ( UIButton sender )
	{			
		if(!macrosDescriptionActive && !initEquipMacrosConfirmationActive) 
		{
			HideSpellBook(true);
			classText.hidden = true;
			defaultText.hidden = true;
			itemText.hidden = true;
			MountsText.hidden = true; 
			
			spellButtonText.hidden = true;
			traitsButtonText.hidden = true;
			glyphsButtonText.hidden = true;
			petTraitButtontText.hidden = true;
			
			items.disabled=true;
			classSpells.disabled=true;
			defaultSpells.disabled=true;
			Mounts.disabled=true;
			if(SpellBuyMithril != null)
			{
				SpellBuyMithril.disabled = true;
			}
			
			spellorTraitButton[0].disabled = true;
			spellorTraitButton[1].disabled = true;
			spellorTraitButton[3].disabled = true;
			
			if(spellorTraitButton[2] != null)
			{
				spellorTraitButton[2].disabled = true;
			}
			
			if (mainPlayer.spellCategory == ActionButtonType.ACTION_BUTTON_ITEM)
				equipConfSpellYESbutton.oInfo = sender.oInfo; 
			if (mainPlayer.spellCategory == ActionButtonType.ACTION_BUTTON_MACRO)
				equipConfSpellYESbutton.oInfo = sender.oInfo;
			
			spellDescriptionActive = true; 
			
			string spellImage;
			
			spellImage = "Marco"+(sender.oInfo - NUMBER_OF_DEFAULT_ACTION_BUTTONS + 1)+".png";
			SinglePictureUtils.DestroySingleUISprite(descriptionPopupSpellIcon);
			descriptionPopupSpellIcon = addIconSprite(spellImage, 0, 0, 0); 
			descriptionPopupSpellIcon.setSize(Screen.width * 0.067f, Screen.height * 0.115f);
			descriptionPopupSpellIcon.position = new Vector3(Screen.width * 0.0933f, -Screen.height * 0.17f, -0.1f);
			descriptionPopupSpellIcon.hidden = true;			
			
			spellDescriptionBkg.hidden = false;
			descriptionPopupSpellIcon.hidden = false;
			
			equipMacrosButton.hidden = false;
			editMacrosButton.hidden = false;
			cancelMacrosButton.hidden = false;
			
			editMacrosDescriptionText.hidden = false;
			equipMacrosDescriptionText.hidden = false;				
			cancelMacrosDescriptionText.hidden = false;
			
			descText = "Macro " + (sender.oInfo - NUMBER_OF_DEFAULT_ACTION_BUTTONS + 1);
			
			macrosDescriptionText = new NonUIElements.TextBox(" ",new Vector3(Screen.width * 0.09f, Screen.height * 0.33f, -0.2f), new Vector2(Screen.width * 0.2f, 1) * UID.TEXT_SIZE,5,Color.black, UID.TEXT_SIZE*0.7f,"fontiny dark");
			macrosDescriptionText.size = new Vector2(Screen.width * 0.8f, Screen.height * 0.4f);
			
			string macrosInfo = PlayerPrefs.GetString(mainPlayer.name + " " + descText);
			
			if(macrosInfo == "" || macrosInfo ==  null)
				macrosDescriptionText.text = "";
			else
				macrosDescriptionText.text = macrosInfo;
			
			descriptionPopupSpellText = text2.addTextInstance(" ",Screen.width * 0.18f, Screen.height * 0.24f,UID.TEXT_SIZE, -1,Color.white,UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
			descriptionPopupSpellText.text = ( descText );
		}
	}
	
	void ArrowUpDelegate(UIButton sender) //TODO
	{ 
		//	_classSpell.position.y -= UID.CELL_SIZE;
	}

	void  ArrowDownDelegate (UIButton sender)
	{
		//	_classSpell.position.y += UID.CELL_SIZE;	
	}		
	
	void  equipButtonDelegate (UIButton sender)
	{
		StartCoroutine(equipSpell());
	}
	
	IEnumerator  equipSpell ()
	{
		yield return new WaitForSeconds(0.1f);
		DestroySpellDescriptionPopup();
		initSpellEquipPopup();
	}
	
	void  DestroySpellDescriptionPopup ()
	{	
		equipSpellDescriptionText.hidden = true;
		cancelSpellDescriptionText.hidden = true;
		
		if(spellDescriptionText != null)
		{
			spellDescriptionText.clear();
		}		
		
		if(descriptionPopupSpellText != null)
		{
			descriptionPopupSpellText.clear();
		}		
		
		if(levelPopupSpellText != null)
		{
			levelPopupSpellText.clear();
		}
		
		descriptionPopupSpellIcon.hidden = true;
		
		spellDescriptionActive = false; 
		
		cancelSpellDescrButton.hidden = true;
		equipButton.hidden = true;
		
		spellDescriptionBkg.hidden = true;
	}
	
	void  cancelSpellDescriptionPopupDelegate (UIButton sender)
	{
		DestroySpellDescriptionPopup();
		DestroyMacrosDescriptionPopup();
		
		classText.hidden = false;
		defaultText.hidden = false;
		itemText.hidden = false;
		MountsText.hidden = false; 
		items.disabled=false;
		classSpells.disabled=false;
		defaultSpells.disabled=false;
		
		spellButtonText.hidden = false;
		traitsButtonText.hidden = false;
		glyphsButtonText.hidden = false;
		petTraitButtontText.hidden = false;
		
		Mounts.disabled=false;
		
		if(SpellBuyMithril != null)
		{
			SpellBuyMithril.disabled = false;
		}
		
		TurnOnMenuButtons();
		HideSpellBook(false);
		
		spellorTraitButton[0].disabled = false;
		spellorTraitButton[1].disabled = false;
		spellorTraitButton[3].disabled = false;
		
		if(spellorTraitButton[2] != null)
		{
			spellorTraitButton[2].disabled = false;
		}
	}
	
	void  initSpellEquipPopup ()
	{
		classText.hidden = true;
		defaultText.hidden = true; 
		
		spellButtonText.hidden = true;
		traitsButtonText.hidden = true;
		glyphsButtonText.hidden = true;
		petTraitButtontText.hidden = true;
		
		itemText.hidden = true;
		MountsText.hidden = true;
		
		if(!initEquipSpellConfirmationActive)
		{
			initSpellEquipPopupActive = true;
			
			spellEquipBkg1.hidden = false;
			
			spellCastPortrait[0].hidden = true; // must be set to false to work
			spellCastPortrait[1].hidden = true;
			spellCastPortraitText[0].hidden=true;
			spellCastPortraitText[1].hidden=true;
			portraitSpellEquipBarsHealth1.hidden=true;
			portraitSpellEquipBarsMana1.hidden=true;
			portraitSpellEquipBarsMana2.hidden=true;
			//			spellEquipCover.hidden = false;
			spellEquipClose.hidden = false;			
			triangleArrowButtonThinghy.hidden = false;
			if(isPetSpell)
			{
				triangleArrowButtonThinghy.disabled = true;
				WorldSession.player.actionBarPanel.switchMode = 0;
				switchMode = 0;
			}
			else
			{
				triangleArrowButtonThinghy.hidden = false;
				triangleArrowButtonThinghy.disabled = false;
			}
			
			//			spellEquipText.hidden=false;
			spellEquipName.hidden=false;
			spellCastPortrait[0].hidden = false;
			spellCastPortrait[1].hidden = false;
			portraitSpellEquipBarsHealth1.hidden = false;
			portraitSpellEquipBarsMana1.hidden = false;
			portraitSpellEquipBarsHealth2.hidden = false;
			portraitSpellEquipBarsMana2.hidden = false;
			spellCastPortraitText[0].hidden = false;
			spellCastPortraitText[1].hidden = false;
			string n;
			switch (mainPlayer.spellCategory)
			{
			case ActionButtonType.ACTION_BUTTON_MACRO: 
				switch (auxGeneralCounter)
				{
				case 0:
					n = "Attack";
					break;
				case 1:
					n = "Closest Target";
					break;
				case 2:
					n = "Next Target";
					break;
				case 3:
					n = "Trade";
					break;
				case 4: 
					n = "Battle";
					break;
				case 5: 
					n = "Examine";
					break;
				case 6: 
					n = "Invite\nto party";
					break;
				case 7: 
					n = "Leave\n party";
					break;
				default:
					n = "Macro";
					break;
				}
				spellEquipName.text = ("EQUIP : " + n);
				break;
				
			case ActionButtonType.ACTION_BUTTON_ITEM:
				spellEquipName.text = ("EQUIP : " + descText);
				break;
				
			default: 
				spellEquipName.text = ("EQUIP : " + selectedSpell.SpellName);
				break;
			} 
			DrawSpellButtons(true);
		}
	}
	
	void  DestroySpellEquipPopup ()
	{	
		spellEquipBkg1.hidden = true;
		/*	spellEquipSlotLocked[0].hidden = true;
		spellEquipSlotLocked[1].hidden = true;
		spellEquipSlotLocked[2].hidden = true;*/
		spellCastPortrait[0].hidden = true;
		spellCastPortrait[1].hidden = true;
		spellCastPortraitText[0].hidden=true;
		spellCastPortraitText[1].hidden=true;
		portraitSpellEquipBarsHealth1.hidden=true;
		portraitSpellEquipBarsMana1.hidden=true;
		portraitSpellEquipBarsHealth2.hidden=true;
		portraitSpellEquipBarsMana2.hidden=true;
		//		spellEquipCover.hidden = true;
		//		spellEquipText.hidden = true; 
		spellEquipName.hidden=true;
		initSpellEquipPopupActive=false;
		spellEquipClose.hidden = true;
		equipSpellDescriptionText.hidden=true;
		cancelSpellDescriptionText.hidden=true;
		descriptionPopupSpellIcon.hidden = true; 
		triangleArrowButtonThinghy.hidden = true;
		for(int i=0;i<14;i++) 
		{
			if (spellEquipSlot[i] != null)
				spellEquipSlot[i].hidden = true;
		}
	}
	
	void  spellEquipCloseDelegate (UIButton sender)
	{
		for (int i= 0; i < ACTION_BUTTONS_COUNT; i++)
		{
			ClearSpellEquipSlot(spellEquipSlot[i]);
		}
		
		classText.hidden = false;
		defaultText.hidden = false;
		itemText.hidden = false; 
		MountsText.hidden = false; 
		spellDescriptionActive = false;
		items.disabled=false;
		classSpells.disabled=false;
		defaultSpells.disabled=false;
		
		spellButtonText.hidden = false;
		traitsButtonText.hidden = false;
		glyphsButtonText.hidden = false;
		petTraitButtontText.hidden = false;
		
		//SpellBuyMithril.disabled = false;
		Mounts.disabled=false; 
		TurnOnMenuButtons();  
		DestroySpellEquipPopup();
		
		//		menuButton.onTouchUpInside += startMenu;
		
		spellorTraitButton[0].disabled = false;
		spellorTraitButton[1].disabled = false;
		spellorTraitButton[3].disabled = false;
		
		if(spellorTraitButton[2] != null)
		{
			spellorTraitButton[2].disabled = false;
		}
	}
	
	// MACROS buttons descriptions
	
	void  DestroyMacrosDescriptionPopup ()
	{
		editMacrosDescriptionText.hidden = true;
		equipMacrosDescriptionText.hidden = true;
		cancelMacrosDescriptionText.hidden = true;
		
		if(macrosDescriptionText != null)
			macrosDescriptionText.clear();
		
		descriptionPopupSpellText.clear();
		descriptionPopupSpellIcon.hidden = true;
		
		macrosDescriptionActive = false;
		
		editMacrosButton.hidden = true;
		equipMacrosButton.hidden = true;
		cancelMacrosButton.hidden = true;
		
		spellDescriptionBkg.hidden = true;
	}
	
	void  ShowMacrosEditWindow ()
	{
		macrosEditWindowBackGround.hidden = false;
		macrosEditWindowSprite.hidden = false;
		saveEditMacrosButton.hidden = false;
		cancelEditMacrosButton.hidden = false;
		
		saveEditMacrosDescriptionText.hidden = false;
		cancelEditMacrosDescriptionText.hidden = false;
	}
	
	void  HideMacrosEditWindow ()
	{
		macrosEditWindowBackGround.hidden = true;
		macrosEditWindowSprite.hidden = true;
		saveEditMacrosButton.hidden = true;
		cancelEditMacrosButton.hidden = true;
		
		saveEditMacrosDescriptionText.hidden = true;
		cancelEditMacrosDescriptionText.hidden = true;
	}
	
	void  editMacrosButtonDelegate ( UIButton sender )
	{
		string macrosInfo = PlayerPrefs.GetString(mainPlayer.name + " " + descText);
		
		if (string.IsNullOrEmpty (macrosInfo))
		{
			mainPlayer.macrosText = "";
		}
		else
		{
			mainPlayer.macrosText = macrosInfo;
		}
		
		cancelMacrosButtonDelegate(null);
		DontBugMe();
		
		inventoryManager.HideClassStats();
		
		for (int j= 0; j < woodenBarBackGroundNumber; j++)
			woodenBarBackGround[j].hidden=true;
		
		if(!editMacrosState)
		{
			setInventoryState(false);
			questState = false;
			SpellState(false);
			traitsWindow.destroy();
			spellAndTraitState = false;
			optionsState = false;
			partyState = false;
			wip1State = false;
			wip2State = false;
			
			editMacrosState = true;
		}
		
		rowMainMenu.hidden = true;
		mainMenuContainer.hidden = true;
		
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		InitQuestInterf(false);
		
		ShowMacrosEditWindow();
	}
	
	void  equipMacrosButtonDelegate (UIButton sender)
	{
		StartCoroutine(equipMacros());
	}
	
	IEnumerator  equipMacros ()
	{
		yield return new WaitForSeconds(0.1f);
		DestroyMacrosDescriptionPopup();
		initSpellEquipPopup();
	}
	
	void  cancelMacrosButtonDelegate (UIButton sender)
	{
		cancelSpellDescriptionPopupDelegate(null);
	}
	
	void  cancelEditMacrosButtonDelegate (UIButton sender)
	{		
		editMacrosState = false;
		
		HideMacrosEditWindow();
		mainMenuContainer.hidden = false;
		
		for (int j= 0; j < woodenBarBackGroundNumber; j++)
		{
			woodenBarBackGround[j].hidden = false;
		}
		
		rowMainMenu.hidden = false;
		spellMode(null);
		defaultSpell(null);
		
		mainPlayer.macrosText = "";
	}
	
	void  saveEditMacrosButtonDelegate (UIButton sender)
	{
		string scriptText = mainPlayer.macrosText;
		
		scriptText = CheakMacros(scriptText);
		
		PlayerPrefs.SetString(mainPlayer.name + " " + descText, scriptText);
		
		cancelEditMacrosButtonDelegate(null);
	}
	
	string CheakMacros ( string macrosText )
	{
		string msg = macrosText;
		int pos = 0;
		bool  noZeroLines = true;
		
		msg += "\n";
		
		while(noZeroLines)
		{
			if(msg.Contains("\n\n"))
			{
				pos = msg.IndexOf("\n\n", 0);
				msg = msg.Remove(pos, 1);
			}
			else
				noZeroLines = false;
		}
		
		return msg;
	}
	
	//*************************End of Spells - Equip Spell PopUp Window - Sebek **************************************/
	
	
	
	
	
	//*************************Spells - Equip Spell Confirmation Window - Sebek **************************************/
	
	void  initEquipSpellConfirmation ( UIButton sender )
	{
		initEquipSpellConfirmationActive = true; 
		//Debug.Log("sender.info"+sender.info + "   sender.oInfo:"+sender.oInfo);
		equipConfSpellYESbutton.info=sender.info;
		classText.hidden = true;
		defaultText.hidden = true;
		itemText.hidden = true;
		
		spellButtonText.hidden = true;
		traitsButtonText.hidden = true;
		glyphsButtonText.hidden = true;
		petTraitButtontText.hidden = true;
		
		TurnOffMenuButtons();
		
		for(int i= 0; i < ACTION_BUTTONS_COUNT; i++)
		{
			if (spellEquipSlot[i] != null)
				spellEquipSlot[i].hidden = false;
		}
		spellEquipClose.hidden = true;
		
		initSpellEquipPopupActive = false;
		equipSpellConfPanel.hidden = false;
		equipConfSpellYESbutton.hidden = false;
		equipConfSpellNObutton.hidden = false;
		equipConfSpellCLOSE.hidden = false;
		
		equipConfSpellYESbuttonText.hidden=false;
		equipConfSpellNObuttonText.hidden=false;
		
	}
	
	void  DestroyEquipSpellConfirmation ()
	{
		equipSpellConfPanel.hidden = true;
		equipConfSpellYESbutton.hidden = true;
		equipConfSpellNObutton.hidden = true;
		equipConfSpellCLOSE.hidden = true;
		equipConfSpellYESbuttonText.hidden = true;
		equipConfSpellNObuttonText.hidden = true;
		initEquipSpellConfirmationActive = false;
	}
	
	void  equipSpellCLOSEdelegate ( UIButton sender )
	{   
		if (sender.info != 2)
			DestroySpellButtons();
		
		//spellEquipName.hidden=false;
		DestroyEquipSpellConfirmation();
		initSpellEquipPopupActive = true;
		spellEquipClose.hidden = false;
		DontBugMe();
	}
	
	void  EquipConfSpellYESbuttonDelegate ( UIButton sender )
	{ 
		DestroySpellButtons();
		ActionButtonType type = mainPlayer.spellCategory;
		int position;
		if(sender.info < SELF_PORTRAIT_BUTTON_INDEX)
		{
			position = sender.info + switchMode * ACTION_BUTTONS_COUNT;
		}
		else
		{
			position = sender.info;
		}
		
		if(isPetSpell)
		{			
			position -= RIGHT_EXTENDED_BUTTON_INDEX;
			mainPlayer.actionBarManager.EquipPetSpell(mainPlayer.petGuid, type, (uint)position, (uint)sender.oInfo);
		}
		else
		{
			mainPlayer.actionBarManager.SetObjectToSlot(position, type, (uint)sender.oInfo);
			SendPacket.SetActionButton((byte)position, (uint)sender.oInfo, (uint)type);
		}
		
		equipSpellCLOSEdelegate(sender);
		TurnOnMenuButtons();
		DestroySpellEquipPopup();
		cancelSpellDescriptionPopupDelegate(null);
		//Debug.Log("spell category: " + mainPlayer.spellCategory);
		
		StartCoroutine(DontBugMe());
		//yield return new WaitForSeconds(0.2f);
	}
	
	
	IEnumerator DontBugMe()	//solves an issue about UIButtons clicking one through another, ask Toba about it
	{
		yield return new WaitForSeconds(0.2f);
	}
	
	//*************************End of Spells - Equip Spell Confirmation Window - Sebek **************************************/
	
	public void  ItemShopDelegate ( UIButton sender )
	{
		Application.OpenURL("https://mithril.worldofmidgard.com/worldofmidgard/pages/direct_sales.php?email=" + LoginWindow.accountLogin + "&password=" + LoginWindow.passHash + "&disable_sale=1");
	}
	
	void  FreeMithrilDelegate ( UIButton sender )
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		//trialpay
		TrialpayIntegration.openOfferwall("fd745515f05c3b668cd77edecd619634", LoginWindow.accountLogin);
		#elif UNITY_IPHONE
		//			TapjoyPlugin.ShowOffers();
		#endif
		/*setInventoryState(false);
		questState =false;
		SpellState(false);
		spellAndTraitState = false;
		optionsState = false;
		partyState = false;
		wip1State = false;
		wip2State = true;
		mainPlayer.menuSelected=5;
		MainPlayer.waitingToEquip = false;
		mainMenuContainer.hidden = true;*/
	}
	
	void  SupportDelegate ( UIButton sender )
	{
		Application.OpenURL("http://worldofmidgard.com/womforum/viewforum.php?f=32");
	}
	
	public void  BuyMithrilIapDelegate ( UIButton sender )
	{	
		inventoryManager.HideClassStats();
		if(!rowMainMenu.hidden)
		{
			for (int j= 0; j < woodenBarBackGroundNumber; j++)
			{
				woodenBarBackGround[j].hidden=true;
			}
			rowMainMenu.hidden = true;
		}
		if(!mithrilShopState)
		{	
			setInventoryState(false);
			questState = false;
			SpellState(false);
			traitsWindow.destroy();
			spellAndTraitState = false;
			optionsState = false;
			partyState = false;
			wip1State = false;
			wip2State = false;
			mithrilShopState = true;
			MainPlayer.waitingToEquip = false;		
			mainPlayer.purchaseManager.Init();		
		}
		mainMenuContainer.hidden = true;
		mainPlayer.menuSelected=8;
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		InitQuestInterf(false);
	}
	
	public void  BuyMithrilIapAmazonDelegate ( UIButton sender )
	{	
		inventoryManager.HideClassStats();
		if(!rowMainMenu.hidden)
		{
			for (int j= 0; j < woodenBarBackGroundNumber; j++)
			{
				woodenBarBackGround[j].hidden=true;
			}
			rowMainMenu.hidden = true;
		}
		if(!mithrilShopState)
		{
			setInventoryState(false);
			questState = false;
			SpellState(false);
			traitsWindow.destroy();
			spellAndTraitState = false;
			optionsState = false;
			partyState = false;
			wip1State = false;
			wip2State = false;
			mithrilShopState = true;
			MainPlayer.waitingToEquip = false;	
			mainPlayer.purchaseManager.Init();		
		}
		mainMenuContainer.hidden = true;
		mainPlayer.menuSelected=9;
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		InitQuestInterf(false);
	}
	
	void  BuyMithrilWebDelegate ( UIButton sender )
	{
		Application.OpenURL("https://mithril.worldofmidgard.com/worldofmidgard/pages/direct_sales.php?email=" + LoginWindow.accountLogin + "&password=" + LoginWindow.passHash + "&disable_sale=1");
	}
	
	void  HelpButtonDelegate ( UIButton sender )
	{
		Application.OpenURL("http://worldofmidgard.com/womforum/viewforum.php?f=32");
	}
	
	void  LeaderboardButtonDelegate ( UIButton sender )
	{
		Application.OpenURL("http://leaderboards.worldofmidgard.com/");
	}
	
	void  unstuckButtonDelegate ( UIButton sender )
	{
		string _url = "https://mithril.worldofmidgard.com/worldofmidgard/pages/unstack_character_login.php";
		Application.OpenURL (_url);
	}
	
	void  reportBugDelegate ( UIButton sender )
	{
		Application.OpenURL("http://worldofmidgard.com/womforum/viewforum.php?f=33");
	}
	
	public void  LogoutDelegate ( UIButton sender )
	{	
		WorldSession.interfaceManager.SetWindowState(InterfaceWindowState.EMPTY_WINDOW_STATE);
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		InitQuestInterf(false);
		
		inventoryManager.HideClassStats();
		WorldPacket logOutPacket = new WorldPacket(OpCodes.CMSG_LOGOUT_REQUEST,0);
		RealmSocket.outQueue.Add(logOutPacket);
	}
	
	void  CancelLogoutDelegate ( UIButton sender )
	{
		WorldPacket cancelLogOutPacket = new WorldPacket(OpCodes.CMSG_LOGOUT_CANCEL,0);
		RealmSocket.outQueue.Add(cancelLogOutPacket);
	}
	
	public void  AnimViewMode ( UIButton sender )
	{
		setInventoryState(false);
		inventoryManager.HideClassStats();
		questState =false;
		SpellState(false);
		traitsWindow.destroy();
		spellAndTraitState = false;
		optionsState = false;
		wip1State = true;
		wip2State = false;
		
		if(WorldSession.player.raidFlag)
		{
			RaidWindowManager.closeRaidWindow();
		}
		
		if(WorldSession.player.menuSelected == 4)
		{
			WorldSession.player.menuSelected = 7;
		}
		
		WorldSession.interfaceManager.playerMenuMng.SetWindowState(PlayerMenuWindowState.EXTRA_WINDOW_STATE);
		MainPlayer.waitingToEquip = false;
		mainMenuContainer.hidden = true;
		//		RaidWindowManager.show();
		
		//mithrilShopTextAmazon.hidden = true;
		mainMenuContainer.hidden = true;
		if(npcMode)
		{
			QuestExitDelegate(questExitButton);
		}
		
		mainPlayer.session.SendRaidRequest();
		InitQuestInterf(false);
		
	}
	
	UIScrollableVerticalLayout defaultSpellBook;
	UIScrollableVerticalLayout classSpellBook;
	UIScrollableVerticalLayout itemSpellBook;
	UIScrollableVerticalLayout mountsSpellBook;
	
	public void  itemSpell (UIButton sender)
	{
		mainPlayer.spellCategory = ActionButtonType.ACTION_BUTTON_ITEM;
		
		ClearSpellBook();
		
		itemSpellBook = new UIScrollableVerticalLayout(17); //WTF
		itemSpellBook.position = new Vector3((Screen.width * 0.295f), (-Screen.height * 0.27f), 0 );
		
		uint itemBookColumns = (uint)((backg.width * 0.95f) / (1.6f * UID.CELL_SIZE));	
		
		itemSpellBook.setSize((backg.width * 0.95f), (backg.height * 0.7f));
		itemSpellBook.cellSize = UID.CELL_SIZE;
		itemSpellBook.setGridLayout(itemBookColumns);
		
		Item cheakItem;
		string iconName;
		for(int i = 0; i < mainPlayer.itemManager.Items.Count; i++)
		{
			iconName = "noicon.png";
			cheakItem = (mainPlayer.itemManager.Items[i] as Item);
			if(cheakItem.entry != 0 && CheckItemSlot(cheakItem))
			{
				iconName = DefaultIcons.GetItemIcon(cheakItem);
				UIToolkit tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+iconName);
				UICell itemIcon = UICell.create(tempUIToolkit, new OBJ(OBJ.CELLTYPE.CELLTYPE_ITEM, cheakItem.entry), iconName);
				itemIcon.setSize(UID.CELL_SIZE, UID.CELL_SIZE);
				itemIcon.info = i;
				itemIcon.oInfo = (int)cheakItem.entry;
				itemIcon.onTouchDown += initItemDescriptionPopup;
				
				itemSpellBook.addChild(itemIcon);
				itemSpellBook.hidden = !spellState;
			}
		}
	}
	
	private bool CheckItemSlot ( Item item )
	{
		bool  ret = false;
		bool  checkClassType = false;
		bool  checkPlayerBag = false;
		
		if(item.classType == ItemClass.ITEM_CLASS_CONSUMABLE || item.classType == ItemClass.ITEM_CLASS_MISC || item.inventoryType != 0)
		{
			checkClassType = true;
		}
		
		if(item.bag == 255 || item.bag < 5)
		{	
			checkPlayerBag = true;
		}
		else if(item.bag == 0 && (item.slot < 20 || (item.slot > 23 && item.slot < 40)) && item.slot != 0)
		{	
			checkPlayerBag = true;
		}
		
		if(checkClassType && checkPlayerBag)
		{
			ret = true;
		}
		
		return ret;
	}
	
	public void  classSpell (UIButton sender)
	{
		mainPlayer.spellCategory = ActionButtonType.ACTION_BUTTON_SPELL;
		
		ClearSpellBook();
		
		classSpellBook = new UIScrollableVerticalLayout(17); //WTF
		classSpellBook.position = new Vector3((Screen.width * 0.295f), (-Screen.height * 0.27f), 0 );
		
		uint spellBookColumns = (uint)((backg.width * 0.95f) / (1.6f * UID.CELL_SIZE));	
		
		classSpellBook.setSize((backg.width * 0.95f), (backg.height * 0.7f));
		classSpellBook.cellSize = UID.CELL_SIZE;
		classSpellBook.setGridLayout(spellBookColumns); // Set a count of colums in spellbook
		
		long spellAttrUnk7 = (long)SpellAttributes.SPELL_ATTR_UNK7;
		long spellAttrTradeSpell = (long)SpellAttributes.SPELL_ATTR_TRADESPELL;
		foreach(KeyValuePair<uint, Spell> spellBookPair in mainPlayer.playerSpellManager.playerSpellBook)
		{	
			Spell spell = spellBookPair.Value;
			string iconName = "noicon.png";
			
			// Do not create mount spells and hidden for the client side spells.
			if(CheckIfMount(spellBookPair.Key)
			   || ((spell.Attributes & spellAttrUnk7) > 0)
			   || ((spell.Attributes & spellAttrTradeSpell) > 0)
			   || ((spell.Targets & 1026) > 0)
			   || ((spell.Targets & 16384) > 0))
			{
				continue;
			}
			iconName = DefaultIcons.GetSpellIcon(spellBookPair.Key);
			UIToolkit tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+iconName);
			UICell spellIcon = UICell.create(tempUIToolkit, new OBJ(OBJ.CELLTYPE.CELLTYPE_SPELL, spellBookPair.Key), iconName);
			spellIcon.info = (int)spellBookPair.Key;
			spellIcon.onTouchDown += initSpellDescriptionPopup;
			spellIcon.setSize(UID.CELL_SIZE, UID.CELL_SIZE);
			classSpellBook.addChild(spellIcon);
			classSpellBook.hidden=!spellState;
		}
	}
	
	UICell spellInfo;
	UIButton backg;
	
	void  MountsDelegate (UIButton sender)
	{
		mainPlayer.spellCategory = ActionButtonType.ACTION_BUTTON_SPELL;
		
		ClearSpellBook();
		
		mountsSpellBook = new UIScrollableVerticalLayout(17); //WTF
		mountsSpellBook.position = new Vector3(Screen.width * 0.295f, -Screen.height * 0.27f, 0 );
		
		uint SpellBookRows = (uint)((backg.height * 0.7f) / (1.6f * UID.CELL_SIZE));
		uint SpellBookColumns = (uint)((backg.width * 0.95f) / (1.6f * UID.CELL_SIZE));
		
		mountsSpellBook.setSize(backg.width * 0.95f, backg.height * 0.7f);
		mountsSpellBook.cellSize = UID.CELL_SIZE;
		mountsSpellBook.setGridLayout(SpellBookColumns);
		
		DrawPetsSpell();
		
		foreach(KeyValuePair<uint, Spell> spell in mainPlayer.playerSpellManager.playerSpellBook)
		{	
			string iconName = "noicon.png";
			if(CheckIfMount(spell.Key))
			{
				iconName = DefaultIcons.GetSpellIcon(spell.Key);
				UIToolkit tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+iconName);
				UICell spellIcon = UICell.create(tempUIToolkit, new OBJ(OBJ.CELLTYPE.CELLTYPE_MOUNT, spell.Key), iconName);
				spellIcon.info = (int)spell.Key;
				spellIcon.onTouchDown += initSpellDescriptionPopup;
				spellIcon.setSize(UID.CELL_SIZE, UID.CELL_SIZE);
				mountsSpellBook.addChild(spellIcon);
				mountsSpellBook.hidden=!spellState;
			}
		}
	}
	
	void  DrawPetsSpell ()
	{
		if(mainPlayer.pet == null)
		{
			return;
		}
		
		for(int index = 0; index < mainPlayer.petSpellManager.GetPetSpellCount(); index++)
		{
			Spell spell = mainPlayer.petSpellManager.GetPetSpellByIndex(index);
			string iconName = DefaultIcons.GetSpellIcon(spell.ID);
			UIToolkit tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+iconName);
			UICell spellIcon = UICell.create(tempUIToolkit, new OBJ(OBJ.CELLTYPE.CELLTYPE_SPELL, spell.ID), iconName);
			spellIcon.info = (int)spell.ID;
			spellIcon.onTouchDown += initPetSpellDescriptionPopup;
			spellIcon.setSize(UID.CELL_SIZE, UID.CELL_SIZE);
			mountsSpellBook.addChild(spellIcon);
			mountsSpellBook.hidden = !spellState;
		}
	}
	
	public void  SpellState ( bool flag )
	{
		ClearSpellBook();
		spellState = flag;
	}
	
	public void  ClearSpellBook ()
	{
		// SpellCathegory = 0;
		ClearSpellBookForTab(defaultSpellBook);
		
		// SpellCathegory = 1;
		ClearSpellBookForTab(classSpellBook);
		
		// SpellCathegory = 3;
		ClearSpellBookForTab(itemSpellBook);
		
		// SpellCathegory = 2;
		if(itemSpellBook != null)
		{
			itemSpellBook.removeAllChild(true);
			itemSpellBook.setSize(0,0);
		}
		
		
		// SpellCathegory = 3;
		ClearSpellBookForTab(mountsSpellBook);
	}
	
	void  ClearSpellBookForTab ( UIScrollableVerticalLayout spellBookTab )
	{
		if(spellBookTab != null)
		{
			while(spellBookTab._children.Count > 0)
			{
				UISprite temp = spellBookTab._children[0];
				UIToolkit tempUIToolkit = temp.manager;
				spellBookTab.removeChild(temp, false);
				temp.destroy();
				if(tempUIToolkit)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
			spellBookTab.setSize(0, 0);
		}
	}
	
	void  HideSpellBook ( bool state )
	{
		if(defaultSpellBook != null)
		{
			defaultSpellBook.hidden = state;
		}
		
		// SpellCathegory = 1;
		if(classSpellBook != null)
		{
			classSpellBook.hidden = state;
		}
		
		// SpellCathegory = 2;
		if(itemSpellBook != null)
		{
			itemSpellBook.hidden = state;
		}
		
		// SpellCathegory = 3;
		if(mountsSpellBook != null)
		{
			mountsSpellBook.hidden = state;
		}
	}
	
	int NUMBER_OF_DEFAULT_ACTION_BUTTONS = 8;
	int NUMBER_OF_DEFAULT_MACROS_BUTTONS = 15;	
	int NUMBER_OF_DEFAULT_PETS_BUTTONS = 6;
	
	bool CheckIfMount ( uint entry )
	{
		if(dbs.sSpells.GetRecord(entry).EffectApplyAuraName[0] == (int)AuraType.SPELL_AURA_MOUNTED)
			return true;
		return false;
	}
	
	public void  defaultSpell (UIButton sender)
	{	
		int i;
		
		string defaultSpells = null;
		string defaultSpellsTexture = "noicon.png";
		
		stateManager();
		
		mainPlayer.spellCategory = ActionButtonType.ACTION_BUTTON_MACRO;
		
		ClearSpellBook();
		
		defaultSpellBook = new UIScrollableVerticalLayout(17); //WTF
		defaultSpellBook.position = new Vector3(Screen.width * 0.295f, -Screen.height * 0.27f, 0 );
		
		uint defaultBookColumns = (uint)((backg.width * 0.95f) / (1.6f * UID.CELL_SIZE));
		
		defaultSpellBook.setSize(backg.width * 0.95f, backg.height * 0.7f);
		defaultSpellBook.cellSize = UID.CELL_SIZE;
		
		defaultSpellBook.setGridLayout(defaultBookColumns);
		
		//		defaultSpellBook.cellSize = UID.CELL_SIZE;
		
		UICell DefaultIcon;
		int currentSpellNumber = 0;
		UIToolkit tempUIToolkit;
		//create the default spell buttons
		for(i = currentSpellNumber; i < NUMBER_OF_DEFAULT_ACTION_BUTTONS; i++)
		{			
			switch(i)
			{
			case 0:
				defaultSpells = "Jump";
				defaultSpellsTexture = jumpImage;
				break;
			case 1:
				defaultSpells = "Closest\nTarget";
				defaultSpellsTexture = selectEnemyImage;
				break;
			case 2:
				defaultSpells = "Next\nTarget";
				defaultSpellsTexture = objectTabImage;
				break;
			case 3:
				defaultSpells = "Trade";
				defaultSpellsTexture = tradeImage;
				break;
			case 4: 
				defaultSpells = "Battle";
				defaultSpellsTexture = battleImage;
				break;
			case 5: 
				defaultSpells = "Examine";
				defaultSpellsTexture = inspectImage;
				break;
			case 6: 
				defaultSpells = "Invite\nto party";
				defaultSpellsTexture = invitePartyImage;
				break;
			case 7: 
				defaultSpells = "Leave\n party";
				defaultSpellsTexture = leavePartyImage;
				break;
			default:
				defaultSpellsTexture = "noicon.png";
				break;
			}
			
			tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+defaultSpellsTexture);
			DefaultIcon = UICell.create(tempUIToolkit, new OBJ(OBJ.CELLTYPE.CELLTYPE_EMPTY, 1), defaultSpellsTexture);
			DefaultIcon.setSize(UID.CELL_SIZE, UID.CELL_SIZE);
			DefaultIcon.hidden = !spellState;
			DefaultIcon.oInfo = i;
			DefaultIcon.onTouchDown += initItemDescriptionPopup;
			
			defaultSpellBook.addChild(DefaultIcon);
			defaultSpellBook.hidden=!spellState;
			
		}
		
		currentSpellNumber = NUMBER_OF_DEFAULT_ACTION_BUTTONS;
		
		for(i = currentSpellNumber; i < (currentSpellNumber + NUMBER_OF_DEFAULT_MACROS_BUTTONS); i++)
		{			
			defaultSpells = "Macro " + (i - currentSpellNumber + 1);
			
			defaultSpellsTexture = "Marco"+(i - currentSpellNumber + 1)+".png";
			tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+defaultSpellsTexture);
			DefaultIcon = UICell.create(tempUIToolkit, new OBJ(OBJ.CELLTYPE.CELLTYPE_EMPTY, 1), defaultSpellsTexture);
			DefaultIcon.setSize(UID.CELL_SIZE, UID.CELL_SIZE);
			DefaultIcon.hidden = !spellState;
			DefaultIcon.oInfo = i;
			DefaultIcon.onTouchDown += initMacrosDescriptionPopup;
			
			defaultSpellBook.addChild(DefaultIcon);
			defaultSpellBook.hidden=!spellState;
			
		}
		
		int id;
		
		currentSpellNumber = NUMBER_OF_DEFAULT_ACTION_BUTTONS + NUMBER_OF_DEFAULT_MACROS_BUTTONS;
		
		if((mainPlayer.playerClass == Classes.CLASS_WARLOCK || mainPlayer.playerClass == Classes.CLASS_HUNTER) && mainPlayer.pet != null)
		{
			for(i = currentSpellNumber; i < currentSpellNumber + NUMBER_OF_DEFAULT_PETS_BUTTONS; i++)
			{	
				id = i - currentSpellNumber;		
				
				switch(id)
				{
				case 0:
					defaultSpells = "Assault";
					defaultSpellsTexture = AssaultImage;
					break;
				case 1:
					defaultSpells = "Escort";
					defaultSpellsTexture = EscortImage;
					break;
				case 2:
					defaultSpells = "Follow";
					defaultSpellsTexture = FollowImage;
					break;
				case 3:
					defaultSpells = "Halt";
					defaultSpellsTexture = HaltImage;
					break;
				case 4: 
					defaultSpells = "Hostile";
					defaultSpellsTexture = HostileImage;
					break;
				case 5: 
					defaultSpells = "Inactive";
					defaultSpellsTexture = InactiveImage;
					break;
				}
				
				tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+defaultSpellsTexture);
				DefaultIcon = UICell.create(tempUIToolkit, new OBJ(OBJ.CELLTYPE.CELLTYPE_EMPTY, 1), defaultSpellsTexture);
				DefaultIcon.setSize(UID.CELL_SIZE, UID.CELL_SIZE);
				DefaultIcon.hidden = !spellState;
				DefaultIcon.oInfo = i;
				DefaultIcon.onTouchDown += initItemDescriptionPopup;
				
				defaultSpellBook.addChild(DefaultIcon);
				defaultSpellBook.hidden=!spellState;
			}
		}		
	}
	
	public void  startDrag ( UIButton sender )
	{
		Debug.Log("starting drag...");
	}
	
	public void  resetInventoryStatsMode ( int zoomValue )
	{
		inventoryManager.inventoryStatsMode = zoomValue;
	}
	
	public void MinChatBoxSizeSwitchPlus( UIButton sender,   int state )			//	This right here will is the button code for the minimezed chatbox size, it is between 1 and 6 rows- Sebek
	{	
		mainPlayer.MinChatBoxSize=state + 1;		
		Debug.Log("Chat Size:"+mainPlayer.MinChatBoxSize);	
		//auxStoreChatBoxSize = mainPlayer.MinChatBoxSize; //-Sebek
		//PlayerPrefs.SetInt(mainPlayer.name+" ChatSize",auxStoreChatBoxSize);
		PlayerPrefs.SetInt(mainPlayer.name+" ChatSize", mainPlayer.MinChatBoxSize);
	}		
	
	public void  PortraitFightAction ( UIButton sender )
	{
		int val = PlayerPrefs.GetInt(mainPlayer.name+" targetPortraitButton");
		switch(val)
		{
		case (0):
			Debug.Log("nothing target");
			break;
			
		case (1):
			Debug.Log("self-cast target");
			mainPlayer.actionBarManager.DoAction(TARGET_PORTRAIT_BUTTON_INDEX, mainPlayer as BaseObject);
			break;
			
		case (2):
			Debug.Log("cast target");
			mainPlayer.actionBarManager.DoAction(TARGET_PORTRAIT_BUTTON_INDEX, mainPlayer.target);
			break;
			
		default:
			Debug.Log("unsuported option."+PlayerPrefs.GetInt(mainPlayer.name+" targetPortraitButton"));
			break;
		}
	}
	public void  PortraitAction ( UIButton sender )
	{
		int val = PlayerPrefs.GetInt(mainPlayer.name+" selfPortraitButton");
		switch(val)
		{
		case (0):
			Debug.Log("nothing portrait");
			break; //do nothing option
			
		case (1):
			Debug.Log("self-cast portrait");
			mainPlayer.actionBarManager.DoAction(SELF_PORTRAIT_BUTTON_INDEX, mainPlayer as BaseObject);
			break;
			
		default:
			Debug.Log("unsuported option."+PlayerPrefs.GetInt(mainPlayer.name+" selfPortraitButton"));
			break;
		}
		
		
	}
	
	public void  TemplateAlertWindow ( string message )
	{
		DestroyAlert();
		
		alertBackground = UIPrime31.myToolkit4.addSprite("popup_background.png", Screen.width*0.2f, Screen.height*0.3f, -5);
		alertBackground.setSize(Screen.width*0.55f, Screen.height*0.45f);
		
		alertDecoration = UIPrime31.questMix.addSprite("ornament4.png", alertBackground.position.x + alertBackground.width*0.03f, -alertBackground.position.y + alertBackground.height*0.22f, -6);
		alertDecoration.setSize(alertBackground.width*0.9f, alertBackground.height*0.33f);
		
		closeAlertButton = UIButton.create(UIPrime31.myToolkit3, "button_OK.png", "button_OK.png", alertBackground.position.x + alertBackground.width*0.3f, -alertBackground.position.y + alertBackground.height*0.72f, -6);
		closeAlertButton.setSize(alertBackground.width*0.4f, alertBackground.height*0.16f);
		closeAlertButton.onTouchUpInside += CloseAlertDelegate;
		alertContainer.addChild(closeAlertButton);
		
		closeAlertButton = UIButton.create( UIPrime31.myToolkit4, "close_button.png", "close_button.png", 0, 0, 0);
		closeAlertButton.setSize(alertBackground.width*0.1f, alertBackground.width*0.1f);
		closeAlertButton.position = new Vector3( alertBackground.position.x + alertBackground.width - closeAlertButton.width/1.5f, alertBackground.position.y + closeAlertButton.height/5, -6);
		closeAlertButton.onTouchUpInside += CloseAlertDelegate;
		
		alertContainer.addChild(alertBackground, alertDecoration, closeAlertButton);
		
		if (message.Length > 20)
		{	
			int tempNum;
			tempNum = message.IndexOf(" ", 19);
			if(tempNum!=-1)
			{
				UIalert = text3.addTextInstance(message.Substring(0, tempNum)+"\n"+message.Substring(tempNum+1, message.Length-tempNum-1),
				                                alertBackground.position.x + alertBackground.width * 0.6f,
				                                -alertBackground.position.y + alertBackground.height*0.25f,
				                                UID.TEXT_SIZE*0.7f,
				                                -6,
				                                Color.white,
				                                UITextAlignMode.Center,
				                                UITextVerticalAlignMode.Middle);
			}
			else
			{
				UIalert = text3.addTextInstance(message,
				                                alertBackground.position.x + alertBackground.width * 0.6f,
				                                -alertBackground.position.y + alertBackground.height*0.25f,
				                                UID.TEXT_SIZE*0.7f,
				                                -6,
				                                Color.white,
				                                UITextAlignMode.Center,
				                                UITextVerticalAlignMode.Middle);
			}
		}
		else
		{
			UIalert = text3.addTextInstance(message,
			                                alertBackground.position.x + alertBackground.width * 0.6f,
			                                -alertBackground.position.y + alertBackground.height*0.25f,
			                                UID.TEXT_SIZE*0.7f,
			                                -6,
			                                Color.white,
			                                UITextAlignMode.Center,
			                                UITextVerticalAlignMode.Middle);
		}
	}
	
	public void  CloseAlertDelegate ( UIButton sender )
	{
		DestroyAlert();
	}
	
	public void  DestroyAlert ()
	{
		alertContainer.removeAllChild(true);
		if(UIalert != null)
		{
			UIalert.clear();
			UIalert = null;
		}
	}
	
	public void  TapFreeMithrilDelegate ( UIButton sender )
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		//			CallJavaCode.ShowOffers();
		#endif
		Debug.Log ("buttonu merge");
	}
	
	
	public void  InitQuestButtons ()
	{	
		uint questId;
		QuestManager questManager = WorldSession.player.questManager;
		byte questCount = questManager.getQuestCount();
		for(byte questSlot = 0; questSlot < questCount; questSlot++)
		{
			questId = questManager.getQuestBySlotId(questSlot);
			if(questId > 0)
			{
				qst = questManager.getQuest(questId);
				if(qst != null && !string.IsNullOrEmpty(qst.title))
				{
					// 					Debug.Log("///////////////////////////////////ID : "+questId+ "   questSlot: "+questSlot+ qst.title+"end");
					questTitleButton[questSlot] = UIToggleButton.create(UIPrime31.questMix, "wooden_button.png", "wooden_button.png", "wooden_button.png", 0, 0, 0);
					questTitleButton[questSlot].setSize(questTitleCase.width * 0.92f, questTitleCase.height * 0.15f);
					questTitleButton[questSlot].centerize();
					questTitleButton[questSlot].hidden = false;
					questTitleButton[questSlot].info = (int)questId;
					questTitleButton[questSlot].onToggle += QuestTitleButtonToggle;
					scrollableQuestButtons.addChild(questTitleButton[questSlot]);
					
					questTitleButtonText[questSlot] = text2.addTextInstance(qst.title, 0, 0, UID.TEXT_SIZE*0.8f, -1, Color.white, UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
				}
			}
		}
		if(questCount > 0)
		{
			questId = questManager.getQuestBySlotId((byte)(questCount - 1));
			if(questManager.markedQuest == 0)
			{
				questManager.setMarkedQuest(questId);
			}
		}
		else
		{
			questManager.setMarkedQuest(0);
		}
	}
	void  ButtonSetArrowDelegate ( UIButton sender )
	{
		WorldSession.player.questManager.setMarkedQuest((uint)sender.info);
	}
	
	void  CreateSetArrowButton ()
	{
		buttonSetArrow = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0, 0 );
		buttonSetArrow.setSize(Screen.width * 0.26f, questTitleCase.height * 0.12f);
		buttonSetArrow.centerize();
		buttonSetArrow.position = new Vector3(Screen.width*0.86f, -Screen.height * 0.25f, 0);;
		QuestManager questManager = WorldSession.player.questManager;
		buttonSetArrow.info = (int)questManager.markedQuest;
		buttonSetArrow.onTouchDown += ButtonSetArrowDelegate;
		buttonSetArrow.hidden = true;
		questContainer.addChild(buttonSetArrow);
		
		textSetArrow = text1.addTextInstance("Set Arrow", 0, 0, UID.TEXT_SIZE*0.9f, -1, Color.white, UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
		textSetArrow.position = buttonSetArrow.position;			
		textSetArrow.hidden = true;
	}
	
	public void  QuestTitleButtonToggle ( UIToggleButton sender,   bool val )
	{	
		for (int _i=0; _i<25; _i++)
		{
			if (questTitleButton[_i] != null)
			{	
				if(sender == questTitleButton[_i])
				{
					_i++;
				}
				if (questTitleButton[_i] != null && questTitleButton[_i].selected)
				{
					questTitleButton[_i].selected = false;
					scrollableQuestButtons.childToMove = null;
					scrollableQuestButtons._contentSize.y -= scrollableQuestButtons.spaceToAdd;
					scrollableQuestButtons.calculateMinMaxInsets();
					scrollableQuestButtons.doLayoutChildren();
					questObjectives.destroy();
					questDetails.destroy();
					buttonSetArrow.hidden = true;
					textSetArrow.hidden = true;
				}
			}
		}
		
		if (val) 
		{	
			scrollableQuestButtons.scrollTo((int)(sender.position.y - FirstButton().position.y), true);
			ShowQuestDetails(sender);
			questDeleteButton.info = sender.info;
			questShareButton.info = sender.info;
			lastChar2 = questDetails.m_textInstances.textSprites[questDetails.m_textInstances.textSprites.Count-1];
			scrollableQuestButtons.spaceToAdd = (questObjectives.m_textInstances.position.y - questObjectives.m_textInstances.textSprites[questObjectives.m_textInstances.textSprites.Count-1].position.y) + (questDetails.m_textInstances.position.y - lastChar2.position.y);
			scrollableQuestButtons.childToMove = sender;
			scrollableQuestButtons._contentSize.y += scrollableQuestButtons.spaceToAdd;
			scrollableQuestButtons.calculateMinMaxInsets();
			scrollableQuestButtons.doLayoutChildren();
			buttonSetArrow.info = sender.info;
			buttonSetArrow.hidden = false;
			textSetArrow.hidden = false;
		}
		else if (questObjectives != null)
		{	
			scrollableQuestButtons.scrollTo(0,true);
			scrollableQuestButtons._contentSize.y -= scrollableQuestButtons.spaceToAdd;
			scrollableQuestButtons.calculateMinMaxInsets();
			questObjectives.destroy();
			questDetails.destroy();
			scrollableQuestButtons.childToMove = null;
			scrollableQuestButtons.doLayoutChildren();
			buttonSetArrow.hidden = true;
			textSetArrow.hidden = true;
		}
	}
	
	public void  QuestDeleteDelegate ( UIButton sender )
	{	
		int _questButtonToDel = 0;
		for (int _i=0; _i<25; _i++)
		{
			if (questTitleButton[_i] != null)
			{
				if(questTitleButton[_i].info==sender.info)
				{
					_questButtonToDel = _i;
				}
				if(questTitleButton[_i].selected)
				{
					questTitleButton[_i].selected = false;
					questObjectives.destroy();
					questDetails.destroy();
				}
			}
		}
		scrollableQuestButtons.scrollTo(0,true);
		scrollableQuestButtons.childToMove = null;
		scrollableQuestButtons.doLayoutChildren();
		scrollableQuestButtons.removeChild(questTitleButton[_questButtonToDel], true);
		scrollableQuestButtons._contentSize.y -= scrollableQuestButtons.spaceToAdd;
		scrollableQuestButtons.calculateMinMaxInsets();
		scrollableQuestButtons.doLayoutChildren();
		questTitleButton[_questButtonToDel].destroy();
		questTitleButton[_questButtonToDel] = null;
		questTitleButtonText[_questButtonToDel].clear();
		questTitleButtonText[_questButtonToDel] = null;
		mainPlayer.questManager.removeQuest((uint)sender.info);
		
	}
	
	public void  InitQuestInterf ( bool choise )
	{
		if(choise)
		{
			if(scrollableQuestButtons != null)
				DestroyQuestInterf();
			
			questBackground = UIButton.create( UIPrime31.myToolkit3, "basic_background.png", "basic_background.png", 0, Screen.height * 0.08f, 5);
			questBackground.setSize(Screen.width * 1.002f, Screen.height * 0.92f);
			questBackground.hidden = false;
			
			topTexture = UIPrime31.myToolkit3.addSprite( "backgroundframe_top.png", 0, 0, -2);
			topTexture.setSize(Screen.width * 1.002f, Screen.height*0.14f);
			topTexture.hidden = false;
			
			botTexture = UIPrime31.myToolkit3.addSprite( "backgroundframe_bot.png", 0, Screen.height * 0.97f, -2);
			botTexture.setSize(Screen.width * 1.002f, Screen.height * 0.05f);
			botTexture.hidden = false;
			
			decorationTexture8 = UIPrime31.myToolkit3.addSprite( "decoration_texture2.png", 0, Screen.height * 0.112f, -4);
			decorationTexture8.setSize(Screen.width,Screen.height * 0.03f );
			decorationTexture8.hidden = false;
			
			questTitleCase = UIPrime31.questMix.addSprite("background_case.png", Screen.width * 0.02f, Screen.height * 0.14f, 4);
			questTitleCase.setSize(Screen.width * 0.7f, Screen.height * 0.83f);
			questTitleCase.hidden = false;
			
			//-------------Quest selecting as current
			questSelected = mixToolkit3.addSprite("box.png", Screen.width * 0.02f, Screen.height * 0.14f, 5);
			questSelected.setSize(Screen.width * 0.7f, Screen.height * 0.83f);
			questSelected.hidden = false;
			
			questNotSelected = mixToolkit3.addSprite("box_checked.png", Screen.width * 0.02f, Screen.height * 0.14f, 5);
			questNotSelected.setSize(Screen.width * 0.7f, Screen.height * 0.83f);
			questNotSelected.hidden = false;
			//-------------
			
			questShareButton = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0, 0 );
			questShareButton.setSize(Screen.width * 0.26f, questTitleCase.height * 0.12f);
			questShareButton.centerize();
			questShareButton.position = new Vector3(Screen.width*0.86f, -Screen.height * 0.45f, 0);
			questShareButton.hidden = false;
			
			questDeleteButton = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0, 0 );
			questDeleteButton.setSize(Screen.width * 0.26f, questTitleCase.height * 0.12f);
			questDeleteButton.centerize();
			questDeleteButton.position = new Vector3(Screen.width*0.86f, -Screen.height * 0.65f, 0);
			questDeleteButton.onTouchDown += QuestDeleteDelegate;
			questDeleteButton.hidden = false;
			
			CreateSetArrowButton();
			
			questContainer.addChild(questBackground, topTexture, botTexture, decorationTexture8, questTitleCase, questShareButton, questDeleteButton, questSelected, questNotSelected);
			
			
			questShareText = text1.addTextInstance("SHARE", 0, 0, UID.TEXT_SIZE*0.9f, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
			questShareText.position = questShareButton.position;
			questShareText.hidden = false;
			
			questDeleteText = text1.addTextInstance("DELETE", 0, 0, UID.TEXT_SIZE*0.9f, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
			questDeleteText.position = questDeleteButton.position;
			questDeleteText.hidden = false;
			
			scrollableQuestButtons  = new UIScrollableVerticalLayout( Screen.height/40);
			scrollableQuestButtons.position = new Vector3( Screen.width * 0.02f, -Screen.height * 0.20f/*0.14f*/, 0 );
			scrollableQuestButtons.setSize(questTitleCase.width, questTitleCase.height * /*0.88f*/0.74f);
			scrollableQuestButtons.edgeInsets = new UIEdgeInsets(0, (int)(scrollableQuestButtons.width/2f + questTitleCase.height*0.04f), 0, 0);
			scrollableQuestButtons.marginModify = true;
			
			InitQuestButtons();
		}
		else if(scrollableQuestButtons != null)
			DestroyQuestInterf();
	}
	
	public void  DestroyQuestInterf ()
	{
		questContainer.removeAllChild(true);
		
		if(questShareText != null)
		{
			questShareText.clear();
			questShareText = null;
		}
		
		if(questDeleteText != null)
		{
			questDeleteText.clear();
			questDeleteText = null;
		}
		
		textSetArrow.clear();
		textSetArrow = null;
		
		if(scrollableQuestButtons!=null)
		{
			scrollableQuestButtons.removeAllChild(true);
			scrollableQuestButtons.setSize(0, 0);
			scrollableQuestButtons = null;
		}
		
		for(int i= 0 ; i < 25 ; i++)
		{ 
			if(questTitleButtonText[i] != null)
			{
				questTitleButtonText[i].clear();
				questTitleButtonText[i] = null;
			}
			
			if(questTitleButton[i] != null)
			{
				questTitleButton[i].destroy();
				questTitleButton[i] = null;
			}
		}
		
		if (questObjectives != null)
		{
			questObjectives.destroy();
		}
		if(questDetails != null)
		{
			questDetails.destroy();
		}
	}
	
	public void  ShowQuestDetails ( UIToggleButton theQuestButton )
	{	
		uint questID = (uint)theQuestButton.info;
		qst = mainPlayer.questManager.getQuest((uint)theQuestButton.info);
		
		//Show Quest Objectives
		if(questObjectives != null)
			questObjectives.destroy();
		if(questDetails != null)
			questDetails.destroy();
		questObjectives = new NonUIElements.TextBox("", new Vector3(questTitleCase.position.x + questTitleCase.width * 0.03f, 0, -1), new Vector2(0, 0), 5,Color.white, UID.TEXT_SIZE*0.7f,"fontiny title");
		questObjectives.size = new Vector2(Screen.width * 0.65f/*0.018f*/, Screen.height * 0.5f);
		questObjectives.text = ("\n"+ qst.objectives);
		
		ushort[] req;
		ushort i = 0;
		req= mainPlayer.questManager.mobsLeft(questID);
		i=req[1];
		req[1]=req[0];
		req[0]=i;
		
		for(i=0;i<4;i++)
		{
			if(qst.reqCreatureOrGold[i] > 0)
			{
				questObjectives.text += ("\n" + req[i] + " / " + qst.reqCreatureOrGoldCount[i] + (mainPlayer.questManager.questState(questID) == QuestStatus.QUEST_STATUS_COMPLETE ? "  Completed!" : "")); 
			}
		}
		
		for(i=0;i<6;i++)
		{
			if(qst.reqItemId[i] > 0)
			{
				uint itmCount =  mainPlayer.itemManager.GetItemsTotalCount(qst.reqItemId[i]);
				questObjectives.text += ("\n" + itmCount + " / " + qst.reqItemCount[i] + (mainPlayer.questManager.questState(questID) == QuestStatus.QUEST_STATUS_COMPLETE ? "  Completed!" : ""));
				questObjectives.m_textInstances.hidden = true;
				
			}
		}
		
		questDetails = new NonUIElements.TextBox("", new Vector3(questTitleCase.position.x + questTitleCase.width * 0.03f, 0, -1), new Vector2(0, 0), 5,Color.white, UID.TEXT_SIZE*0.7f,"fontiny dark");
		questDetails.size = new Vector2(Screen.width * 0.65f/*0.018f*/, Screen.height * 0.5f);
		questDetails.text = ("\n\n"+ qst.details);
		questDetails.m_textInstances.hidden = true;
	}
	
	UIToggleButton FirstButton ()
	{
		UIToggleButton ret = null;
		for(int i= 0; i<24 ; i++)
		{
			if(questTitleButton[i]!=null)
			{
				ret = questTitleButton[i];
				break;
			}
		}
		return ret;
	}
	
	public void  ShowQuestListInterface ( List<quest> questsArray )
	{	
		quests = new List<quest>(questsArray);

		HidePlayerInterface(true);
		//HideBottomHotBar(true);
		questNPCBackground = UIButton.create( UIPrime31.myToolkit3, "basic_background.png", "basic_background.png", 0, Screen.height * 0.08f, 5);
		questNPCBackground.setSize(Screen.width * 1.002f, Screen.height * 0.92f);
		
		topQuestTexture = UIPrime31.myToolkit3.addSprite( "backgroundframe_top.png", 0, 0, -2);
		topQuestTexture.setSize(Screen.width * 1.002f, Screen.height*0.14f);
		
		botQuestTexture = UIPrime31.myToolkit3.addSprite( "backgroundframe_bot.png", 0, Screen.height * 0.97f, -2);
		botQuestTexture.setSize(Screen.width * 1.002f, Screen.height * 0.05f);
		
		decorationTexture9 = UIPrime31.myToolkit3.addSprite( "decoration_texture2.png", 0, Screen.height * 0.112f, -4);
		decorationTexture9.setSize(Screen.width,Screen.height * 0.03f );
		
		questCase = UIPrime31.questMix.addSprite("background_case.png", Screen.width * 0.02f, Screen.height * 0.14f, 4);
		questCase.setSize(Screen.width * 0.7f, Screen.height * 0.83f);
		
		questExitButton = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0, 0 );
		questExitButton.setSize(Screen.width * 0.26f, questCase.height * 0.12f);
		questExitButton.centerize();
		questExitButton.position = new Vector3(Screen.width*0.86f, -Screen.height * 0.65f, 0);
		questExitButton.onTouchDown += backMode;
		
		questExitText = text1.addTextInstance("EXIT", 0, 0, UID.TEXT_SIZE*0.9f, -1, Color.white, UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
		questExitText.position = questExitButton.position;
		
		scrollableQuests  = new UIScrollableVerticalLayout( Screen.height/40);
		scrollableQuests.position = new Vector3( Screen.width * 0.02f, -Screen.height * 0.14f - questCase.height * 0.06f, 0 );
		scrollableQuests.setSize(questCase.width, questCase.height * 0.88f);
		scrollableQuests.edgeInsets = new UIEdgeInsets(0, (int)(scrollableQuests.width/2 + questCase.height*0.04f), 0, 0);
		scrollableQuests.marginModify = true;
		
		InitNPCQuestButtons();
	}
	
	void  QuestExitDelegate ( UIButton sender )
	{
		if(mainPlayer.questRewardItemInfoWindow != null && mainPlayer.questRewardItemInfoWindow.active)
		{
			mainPlayer.questRewardItemInfoWindow.CloseItemInfoWindow();
		}
		DestroyNPCQuestInterface();
		HidePlayerInterface(false);
		ExitMenu();
	}
	
	void  DestroyNPCQuestInterface ()
	{	
		questNPCBackground.destroy();
		topQuestTexture.destroy();
		botQuestTexture.destroy();
		decorationTexture9.destroy();
		questCase.destroy();
		questExitButton.destroy();
		questExitText.clear();
		questExitText = null;
		TryToDestroy();
		if(scrollableQuests!=null)
		{
			scrollableQuests.removeAllChild(true);
			scrollableQuests.setSize(0, 0);
			scrollableQuests = null;
		}
		for(byte i = 0 ; i < quests.Count ; i++)
		{
			if(questButtonNPCText[i]!=null)
			{
				questButtonNPCText[i].clear();
				questButtonNPCText[i] = null;
			}
			
			if(questButtonNPC[i]!=null)
			{
				questButtonNPC[i].destroy();
				questButtonNPC[i]=null;
			}
		}
		
		if (npcQuestObjectives != null)
		{
			npcQuestObjectives.destroy();
		}
		if(npcQuestDetails != null)
		{
			npcQuestDetails.destroy();
		}
	}
	
	void TryToDestroy() //removes getQuest, Continue or Complete buttons if they exist
	{
		if (getQuestButton!=null)
		{
			getQuestButton.destroy();
			getQuestButton = null;
		}	
		if (getQuestText!=null)
		{
			getQuestText.clear();
			getQuestText = null;
		}
		
		if (continueQuestButton!=null)
		{
			continueQuestButton.destroy();
			continueQuestButton = null;
		}	
		if (continueQuestText!=null)
		{
			continueQuestText.clear();
			continueQuestText = null;
		}
		
		if (completeQuestButton!=null)
		{
			completeQuestButton.destroy();
			completeQuestButton = null;
		}	
		if (completeQuestText!=null)
		{
			completeQuestText.clear();
			completeQuestText = null;
		}
		int i;
		if(rewardItems!=null && rewardItems[0]!=null)
		{
			for(i= 0; i<qst.getRewChoiceItemCount; i++)
			{
				rewardItems[i].destroy();
				rewardItems[i] = null;
				if(rewardItemsText[i]!=null)
				{
					rewardItemsText[i].clear();
					rewardItemsText[i] = null;
				}
			}
		}
		if(requiredItems!=null && requiredItems[0]!=null)
		{
			for(i = 0; i<qst.getReqItemsCount; i++)
			{
				requiredItems[i].destroy();
				requiredItems[i] = null;
				if(requiredItemsText[i]!=null)
				{
					requiredItemsText[i].clear();
					requiredItemsText[i] = null;
				}
			}
		}
	}
	
	void  ExitMenu ()
	{	
		if(menuState) return;
		WorldSession.interfaceManager.hideInterface = false;
		stateManager();
		mainPlayer.blockTargetChange = false;
	}
	
	void  InitNPCQuestButtons ()
	{
		for(int i = 0 ; i<quests.Count ; i++)
		{
			qst = quests[i];
			if(!string.IsNullOrEmpty(qst.title))
			{
				questButtonNPC[i] = UIToggleButton.create(UIPrime31.questMix, "wooden_button.png", "wooden_button.png", "wooden_button.png", 0, 0, 0);
				questButtonNPC[i].setSize(questCase.width * 0.92f, questCase.height * 0.15f);
				questButtonNPC[i].centerize();
				questButtonNPC[i].hidden = false;
				questButtonNPC[i].info = i;
				questButtonNPC[i].onToggle += QuestButtonNPCToggle;
				scrollableQuests.addChild(questButtonNPC[i]);
				
				questButtonNPCText[i] = text2.addTextInstance(qst.title, 0, 0, UID.TEXT_SIZE*0.8f, -1, Color.white, UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
				QuestButtonNPCToggle(questButtonNPC[i], true);
				QuestButtonNPCToggle(questButtonNPC[i], false);
			}
		}
	}
	
	void  QuestButtonNPCToggle ( UIToggleButton sender,   bool flag )
	{	
		TryToDestroy();
		for (int _i=0; _i<quests.Count; _i++)
		{
			if (questButtonNPC[_i] != null)
			{	
				if(sender == questButtonNPC[_i])
				{
					_i++;
				}
				if (questButtonNPC[_i] != null && questButtonNPC[_i].selected)
				{	
					questButtonNPC[_i].selected = false;
					scrollableQuests.childToMove = null;
					scrollableQuests._contentSize.y -= scrollableQuests.spaceToAdd;
					scrollableQuests.calculateMinMaxInsets();
					scrollableQuests.doLayoutChildren();
					npcQuestObjectives.destroy();
					npcQuestDetails.destroy();
				}
			}
		}
		
		if (flag) 
		{	
			scrollableQuests.scrollTo(sender.position.y - questButtonNPC[0].position.y, true);
			QuestSelected(sender.info);
			ShowNPCQuestDetails(sender.info);
			lastChar2 = npcQuestDetails.m_textInstances.textSprites[npcQuestDetails.m_textInstances.textSprites.Count-1];
			scrollableQuests.spaceToAdd = (npcQuestObjectives.m_textInstances.position.y - npcQuestObjectives.m_textInstances.textSprites[npcQuestObjectives.m_textInstances.textSprites.Count-1].position.y) + (npcQuestDetails.m_textInstances.position.y - lastChar2.position.y);
			scrollableQuests.childToMove = sender;
			
			if(requiredItems!=null)
				if(requiredItems[0]!=null)
					scrollableQuests.spaceToAdd += requiredItems[0].height * qst.getReqItemsCount * 1.1f;
			
			if(rewardItems!=null)
				if(rewardItems[0]!=null)
					scrollableQuests.spaceToAdd += rewardItems[0].height * qst.getRewChoiceItemCount * 1.1f;
			
			scrollableQuests._contentSize.y += scrollableQuests.spaceToAdd;
			scrollableQuests.calculateMinMaxInsets();
			scrollableQuests.doLayoutChildren();
		}
		
		else if (npcQuestObjectives != null)
		{	
			scrollableQuests.scrollTo(0,true);
			scrollableQuests._contentSize.y -= scrollableQuests.spaceToAdd;
			scrollableQuests.calculateMinMaxInsets();
			npcQuestObjectives.destroy();
			npcQuestDetails.destroy();
			scrollableQuests.childToMove = null;
			scrollableQuests.doLayoutChildren();
		}		
	}
	
	void  QuestSelected ( int selectedQuestPosition )
	{	
		qst = quests[selectedQuestPosition];
		qstState = WorldSession.player.questManager.questState(qst.questId);
		
		Debug.Log("QUEST STATE: "+qstState);
		Debug.Log("CASE: "+npc.questGiver.completeQuestStages);
		
		param[0] = npc.guid;
		param[1] = qst.questId;
		param[2] = 0;
		
		//		//hiding tutorial tip
		//		if (WorldSession.player.tutorialInstance) 
		//		{
		//			if (!WorldSession.player.tutHandler.stepsDone[6]) 
		//				WorldSession.player.tutHandler.stepsDone[6] = true;
		//							
		//			if (WorldSession.player.tutHandler.tutorialStep == 6) 
		//				WorldSession.player.tutHandler.showHint = false;						
		//		}
		
		WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_QUERY_QUEST,param);
		WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_COMPLETE_QUEST,param);	
		WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_REQUEST_REWARD,param);
		
		if (qstState == QuestStatus.QUEST_STATUS_INCOMPLETE)
		{
			getQuestButton = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0, 0 );
			getQuestButton.setSize(Screen.width * 0.26f, questCase.height * 0.12f);
			getQuestButton.centerize();
			getQuestButton.position = questExitButton.position + new Vector3(0, getQuestButton.height*1.5f, 0);
			getQuestButton.info = selectedQuestPosition;
			getQuestButton.onTouchDown += GetQuestDelegate;
			
			getQuestText = text1.addTextInstance("ACCEPT", 0, 0, UID.TEXT_SIZE*0.9f, -1, Color.white, UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
			getQuestText.position = getQuestButton.position;
		}
		
		if(qstState == QuestStatus.QUEST_STATUS_COMPLETE)
		{	
			switch(npc.questGiver.completeQuestStages)
			{
			case 0: 
				continueQuestButton = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0, 0 );
				continueQuestButton.setSize(Screen.width * 0.26f, questCase.height * 0.12f);
				continueQuestButton.centerize();
				continueQuestButton.position = questExitButton.position + new Vector3(0, continueQuestButton.height*1.5f, 0);
				continueQuestButton.info = selectedQuestPosition;
				continueQuestButton.onTouchDown += ContinueQuestDelegate;
				
				continueQuestText = text1.addTextInstance("CONTINUE", 0, 0, UID.TEXT_SIZE*0.9f, -1, Color.white, UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
				continueQuestText.position = continueQuestButton.position;			
				break;
			case 2: 
				completeQuestButton = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0, 0 );
				completeQuestButton.setSize(Screen.width * 0.26f, questCase.height * 0.12f);
				completeQuestButton.centerize();
				completeQuestButton.position = questExitButton.position + new Vector3(0, completeQuestButton.height*1.5f, 0);
				completeQuestButton.onTouchDown += CompleteQuestDelegate;
				//						completeQuestButton.info = -1;
				//						completeQuestButton.hidden = true;
				
				completeQuestText = text1.addTextInstance("COMPLETE", 0, 0, UID.TEXT_SIZE*0.9f, -1, Color.white, UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
				completeQuestText.position = completeQuestButton.position;
				//						completeQuestText.hidden = true;
				break;
			}	
		}
	}
	
	void  GetQuestDelegate ( UIButton sender )
	{	
		qst = quests[sender.info];
		
		param[0] = npc.guid;
		param[1] = qst.questId;
		param[2] = 0;
		
		WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_ACCEPT_QUEST,param);
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_QUESTGIVER_STATUS_MULTIPLE_QUERY);
		RealmSocket.outQueue.Add(pkt);
		
		//advance tutorial code
		//		if (WorldSession.player.tutorialInstance) 
		//			if (WorldSession.player.tutHandler.tutorialStep == 8)
		//				WorldSession.player.tutHandler.showHint = true;						
		//				
		//		if (WorldSession.player.tutorialInstance)
		//		{
		//			if (!WorldSession.player.tutHandler.stepsDone[6])
		//				WorldSession.player.tutHandler.stepsDone[6] = true;
		//	
		//			if (WorldSession.player.tutHandler.tutorialStep == 6)
		//				WorldSession.player.tutHandler.advanceTutorial();						
		//		}
		backMode(sender);
		
		//-------------added for quest arrow
		WorldSession.player.questManager.setMarkedQuest(qst.questId);
	}
	
	void  ContinueQuestDelegate ( UIButton sender )
	{	
		//		param[0] = npc.guid; // npcGuid
		//		param[1] = qst.questId;
		//		param[2] = 0;
		//		WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_REQUEST_REWARD,param);
		TryToDestroy();
		QuestButtonNPCToggle(questButtonNPC[sender.info], false);
		QuestButtonNPCToggle(questButtonNPC[sender.info], true);
	}
	
	void  CompleteQuestDelegate ( UIButton sender )
	{
		if(completeQuestButton.info!=-1)
		{	
			param[0] = npc.guid; // npcGuid
			param[1] = qst.questId; //quest ID
			//param[2] = 0;
			param[2] = (ulong)sender.info;
			WorldSession.player.session.handleOpcode(OpCodes.CMSG_QUESTGIVER_CHOOSE_REWARD,param);	
			
			WorldPacket pkt2 = new WorldPacket();
			pkt2.SetOpcode(OpCodes.CMSG_QUESTGIVER_STATUS_MULTIPLE_QUERY);
			RealmSocket.outQueue.Add(pkt2);
			
			//			if (WorldSession.player.tutorialInstance)
			//				if (WorldSession.player.tutHandler.tutorialStep == 5)
			//					WorldSession.player.tutHandler.showHint = false;	
			backMode(sender);	
		}
	}
	
	void  RewardItemsDelegate ( UIButton sender )
	{
		mainPlayer.questRewardItemInfoWindow.CreateItemInfoWindow(sender.info);
		DisableNPCQuestWindow(true);
	}
	
	public void  DisableNPCQuestWindow ( bool state )
	{
		int i = 0;
		if(qst.getRewChoiceItemCount > 0)
		{
			for(i= 0 ; i<qst.getRewChoiceItemCount ; i++)
			{
				rewardItems[i].disabled = state;
			}
		}
		
		if(quests.Count > 0)
		{
			for(;i<quests.Count ; i++)
			{
				questButtonNPC[i].disabled = state;
			}
		}
	}
	
	void  ShowNPCQuestDetails ( int selectedQuest )
	{	
//		Exception err = null;
		try
		{
			qst.details = qst.details.Replace("CHAR_CLASS", Global.GetClassAsString( WorldSession.player.playerClass ) );
		}
		catch(Exception err)
		{
			Debug.LogError("Exception: qst.details, \"CHAR_CLASS\", " + err.Message);
		}
		try
		{
			qst.details = qst.details.Replace("CHAR_NAME", WorldSession.player.name );
		}
		catch(Exception err)
		{
			Debug.LogError("Exception: qst.details, \"CHAR_NAMES\", " + err.Message);
		}
		try
		{
			qst.details = qst.details.Replace("NICKNAME", WorldSession.player.name );
		}
		catch(Exception err)
		{
			Debug.LogError("Exception: qst.details, \"NICKNAME\", " + err.Message);
		}
		
		try
		{
			qst.progress = qst.progress.Replace("CHAR_CLASS", Global.GetClassAsString( WorldSession.player.playerClass ) );
		}
		catch(Exception err)
		{
			Debug.LogError("Exception: qst.progress, \"CHAR_CLASS\", " + err.Message);
		}
		try
		{
			qst.progress = qst.progress.Replace("CHAR_NAME", WorldSession.player.name );
		}
		catch(Exception err)
		{
			Debug.LogError("Exception: qst.progress, \"CHAR_NAME\", " + err.Message);
			
		}
		try
		{
			qst.progress = qst.progress.Replace("NICKNAME", WorldSession.player.name );
		}
		catch(Exception err)
		{
			Debug.LogError("Exception: qst.progress, \"NICKNAME\", " + err.Message);
		}
		
		if(npcQuestObjectives != null) npcQuestObjectives.destroy();
		if(npcQuestDetails != null) npcQuestDetails.destroy();
		
		if (qstState == QuestStatus.QUEST_STATUS_INCOMPLETE)//NEW QUEST
		{	
			npcQuestObjectives = new NonUIElements.TextBox("", new Vector3(questCase.position.x + questCase.width * 0.03f, 0, -1), new Vector2(0, 0), 5,Color.white, UID.TEXT_SIZE*0.7f,"fontiny title");
			npcQuestObjectives.size = new Vector2(Screen.width * 0.65f, Screen.height * 0.5f);
			npcQuestObjectives.text = ("\nOBJECTIVES: \n"+qst.objectives);  
			//npcQuestObjectives.m_textInstances.hidden = true;
			
			npcQuestDetails = new NonUIElements.TextBox("", new Vector3(questCase.position.x + questCase.width * 0.03f, 0, -1), new Vector2(0, 0), 5,Color.white, UID.TEXT_SIZE*0.7f,"fontiny dark");
			npcQuestDetails.size = new Vector2(Screen.width * 0.65f, Screen.height * 0.5f);
			npcQuestDetails.text = ("\n\n"+qst.details);
			//npcQuestDetails.m_textInstances.hidden = true;
			
			if(qst.offerRewText!=null)
				npcQuestDetails.text += ("\n\n" + qst.offerRewText + "\nREWARD:");
			
			else if(qst.xpValue>0 || qst.getRewOrReqMoney>0)
			{
				npcQuestDetails.text += ("\n\nREWARD:");
			}
			
			if(qst.xpValue>0)
				npcQuestDetails.text += "\nXP: "+qst.xpValue; 
			
			if(qst.getRewOrReqMoney>0)
				npcQuestDetails.text += "\nGold: "+qst.getRewOrReqMoney;
			
			if (qst.getRewChoiceItemCount > 0)
			{
				if(qst.offerRewText==null)
					npcQuestDetails.text += "\nREWARD:";
				InitRewardItems(qst);
			} 
		}
		
		else if(qstState == QuestStatus.QUEST_STATUS_COMPLETE && npc.questGiver.completeQuestStages == 0)//CONTINUE QUEST
		{
			npcQuestObjectives = new NonUIElements.TextBox("", new Vector3(questCase.position.x + questCase.width * 0.03f, 0, -1), new Vector2(0, 0), 5,Color.white, UID.TEXT_SIZE*0.7f,"fontiny title");
			npcQuestObjectives.size = new Vector2(Screen.width * 0.65f, Screen.height * 0.5f);
			npcQuestObjectives.text = ("\nOBJECTIVES: \n"+qst.objectives);
			
			npcQuestDetails = new NonUIElements.TextBox("", new Vector3(questCase.position.x + questCase.width * 0.03f, 0, -1), new Vector2(0, 0), 5,Color.white, UID.TEXT_SIZE*0.7f,"fontiny dark");
			npcQuestDetails.size = new Vector2(Screen.width * 0.65f, Screen.height * 0.5f);
			npcQuestDetails.text = ("\nPROGRESS: \n"+qst.progress);
			npcQuestDetails.text += "\nREWARD: ";
			
			//if(qst.getRewChoiceItemCount > 0)
			//	InitRewardItems(qst); 
			
			if(qst.XPAmount>0)
				npcQuestDetails.text += "\nXP: "+qst.XPAmount; 
			
			if(qst.moneyAmaunt>0)
				npcQuestDetails.text += "\nGold: "+qst.moneyAmaunt;
			
			
		} 
		
		else if (qstState == QuestStatus.QUEST_STATUS_COMPLETE && npc.questGiver.completeQuestStages == 2)
		{
			npcQuestObjectives = new NonUIElements.TextBox("", new Vector3(questCase.position.x + questCase.width * 0.03f, 0, -1), new Vector2(0, 0), 5,Color.white, UID.TEXT_SIZE*0.7f,"fontiny title");
			npcQuestObjectives.size = new Vector2(Screen.width * 0.65f, Screen.height * 0.5f);
			npcQuestObjectives.text = ("\n\n"+ qst.offerRewText);  
			//npcQuestObjectives.m_textInstances.hidden = true;
			
			npcQuestDetails = new NonUIElements.TextBox("", new Vector3(questCase.position.x + questCase.width * 0.03f, 0, -1), new Vector2(0, 0), 5,Color.white, UID.TEXT_SIZE*0.7f,"fontiny dark");
			npcQuestDetails.size = new Vector2(Screen.width * 0.65f, Screen.height * 0.5f);
			npcQuestDetails.text = ("\n\nREWARD: ");
			
			if(qst.getRewChoiceItemCount > 0)
				InitRewardItems(qst); 
			
			if(qst.XPAmount>0)
				npcQuestDetails.text += "\n\nXP: "+qst.XPAmount; 
			
			if(qst.moneyAmaunt>0)
				npcQuestDetails.text += "\n\nGold: "+qst.moneyAmaunt;
		}
		
		else 
		{
			npcQuestObjectives = new NonUIElements.TextBox("", new Vector3(questCase.position.x + questCase.width * 0.03f, 0, -1), new Vector2(0, 0), 5,Color.white, UID.TEXT_SIZE*0.7f,"fontiny title");
			npcQuestObjectives.size = new Vector2(Screen.width * 0.65f, Screen.height * 0.5f);
			npcQuestObjectives.text = ("\nOBJECTIVES: \n"+ qst.objectives);  
			//npcQuestObjectives.m_textInstances.hidden = true;
			
			npcQuestDetails = new NonUIElements.TextBox("", new Vector3(questCase.position.x + questCase.width * 0.03f, 0, -1), new Vector2(0, 0), 5,Color.white, UID.TEXT_SIZE*0.7f,"fontiny dark");
			npcQuestDetails.size = new Vector2(Screen.width * 0.65f, Screen.height * 0.5f);
			npcQuestDetails.text = ("\n\nPROGRESS: \n\n"+ qst.progress);
			//npcQuestDetails.m_textInstances.hidden = true;
			
			//if(qst.getReqItemsCount>0) 
			//	InitRequiredItems(qst);
		}
		
	}
	
	void  InitRewardItems ( quest selectedQuest )
	{
		Debug.Log("TREBUIE SA ALEGEM UN REWARD ITEM");
		rewardItems = new UIButton[qst.getRewChoiceItemCount];
		rewardItemsText = new UITextInstance[qst.getRewChoiceItemCount];
		npc.questGiver.prepareRewQuestItems(selectedQuest);
		
		for(int i= 0 ; i<qst.getRewChoiceItemCount ; i++)
		{
			rewardItems[i] = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0, 0 );
			rewardItems[i].setSize(questCase.width * 0.9f, questCase.height * 0.1f);
			rewardItems[i].centerize();
			rewardItems[i].info = i;
			rewardItems[i].onTouchDown += RewardItemsDelegate;
			//			rewardItems[i].disabled = true;
			
			rewardItemsText[i] = text1.addTextInstance(npc.questGiver.itemsName2[i], 0, 0, UID.TEXT_SIZE*0.8f, -2, Color.white, UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
			rewardItemsText[i].position = rewardItems[i].position;
		}
		
		if(qst.getRewChoiceItemCount > 0 && qstState == QuestStatus.QUEST_STATUS_COMPLETE)
		{
			completeQuestText.hidden = true;
			completeQuestButton.hidden = true;
			completeQuestButton.info = -1;
		}
	}
	
	void  InitRequiredItems ( quest selectedQuest )
	{		
		requiredItems = new UIButton[qst.getReqItemsCount];
		requiredItemsText = new UITextInstance[qst.getReqItemsCount];
		npc.questGiver.prepareReqQuestItems(selectedQuest);
		
		for(int i= 0 ; i<qst.getReqItemsCount ; i++)
		{
			requiredItems[i] = UIButton.create( UIPrime31.myToolkit3, "3_selected.png", "3_unselected.png", 0, 0, 0 );
			requiredItems[i].setSize(questCase.width * 0.9f, questCase.height * 0.1f);
			requiredItems[i].centerize();
			requiredItems[i].info = i;
			requiredItems[i].disabled = true;
			
			requiredItemsText[i] = text1.addTextInstance(npc.questGiver.itemsReqItems[i], 0, 0, UID.TEXT_SIZE*0.8f, -2, Color.white, UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
			requiredItemsText[i].position = requiredItems[i].position;
		}
	}
	
	void  HidePlayerInterface ( bool val )
	{	
		if (val)
		{	
			npcMode = true;
			startMenu();
			//mainMenuContainer.hidden = true;
		}
		
		else
		{	
			npcMode = false;
			//HideBottomHotBar(true);
		}	
	}
	
	
	public void  SummonPopUpWindow ( UIButton sender )
	{
		ButtonOLD.summonState = true;
		Color colorTitle;
		colorTitle = new Color(0.906f, 0.702f, 0.267f, 1.000f);
		
		isTimerStart = true;
		float koefWidth = Screen.width * 0.5f / 1015;
		float koefHeight = Screen.height * 0.5f / 396;
		
		float scr = Screen.width;
		popupContainer.removeAllChild(true);
		
		if(partyMemberWndTitle != null)
		{
			partyMemberWndTitle.clear();
			partyMemberWndTitle = null;
		}
		
		if(summonText != null)
		{
			summonText.clear();
		}
		
		summonPopUpWindow = UIButton.create(mixToolkit6, "window.png", "window.png", 0, 0);
		summonPopUpWindow.setSize(Screen.width * 0.74f, Screen.height * 0.5f);
		summonPopUpWindow.position = new Vector3(Screen.width * 0.12f, -Screen.height * 0.24f, 2);	
		
		//AcceptSummonButton
		acceptSummonButton = UIButton.create(mixToolkit6, "acc.png", "acc.png", 0, 0);
		acceptSummonButton.setSize(koefWidth * 328, koefHeight * 65);
		acceptSummonButton.position = new Vector3(Screen.width * 0.21f, -Screen.height * 0.624f, 1);
		acceptSummonButton.hidden = false;
		acceptSummonButton.onTouchDown += AcceptSummoning;		
		
		//cancelSummonButton
		cancelSummonButton = UIButton.create(mixToolkit6, "cancel.png", "cancel.png", 0, 0);
		cancelSummonButton.setSize(koefWidth * 328, koefHeight * 65);
		cancelSummonButton.position = new Vector3(Screen.width * 0.61f, -Screen.height * 0.624f, 1);
		cancelSummonButton.hidden = false;
		cancelSummonButton.onTouchDown += CancelSummoning;
		
		//closeSummonButton
		closeSummonPopUpButton = UIButton.create(mixToolkit6, "close.png", "close.png", 0, 0);
		closeSummonPopUpButton.setSize(Screen.width * 0.82f / 1015 * 56, Screen.height * 0.5f / 396 * 56);
		closeSummonPopUpButton.position = new Vector3(Screen.width * 0.83f, -Screen.height * 0.210f, 1);
		closeSummonPopUpButton.hidden = false;
		closeSummonPopUpButton.onTouchDown += CloseSummoningWindow;
		
		summonFontSize = scr / 960f * 0.60f;
		summonText = text3.addTextInstance("You are summoned to" + '\n' + summonZoneName + " by " + sumonnerGUIDName + '\n' + "Summon spell lasts only 2min."/* + "(last: " + remainingTime + "sec)"*/, Screen.width * 0.5f, Screen.height * 0.27f, summonFontSize, -1, colorTitle, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
		popupContainer.hidden = true;
		popupContainer.addChild(summonPopUpWindow, acceptSummonButton, cancelSummonButton, closeSummonPopUpButton);
	}
	
	public void  setSummonZoneName ( string value )
	{
		summonZoneName = value;
	}
	
	void  AcceptSummoning ( UIButton sender )
	{
		print("Accept");
		SummonResponse(mainPlayer.guid, agree);
		DestroyPopupWindow();
	}
	
	void  CancelSummoning ( UIButton sender )
	{
		print("Cancel");
		isTimerStart = false;
		remainingTime = 0;
		SummonResponse(mainPlayer.guid, disagree);
		DestroyPopupWindow();
	}
	
	void  CloseSummoningWindow ( UIButton sender )
	{
		print("Close");
		isTimerStart = false;
		remainingTime = 0;
		SummonResponse(mainPlayer.guid, disagree);
		DestroyPopupWindow();
	}
	
	void  SummonResponse ( ulong playerId,   bool response )
	{
		WorldPacket pkt = new WorldPacket();   
		pkt.SetOpcode(OpCodes.CMSG_SUMMON_RESPONSE);
		pkt.Append( ByteBuffer.Reverse(playerId));
		pkt.Append(response);
		RealmSocket.outQueue.Add(pkt);
		isTimerStart = false;
		remainingTime = 0;
	}
	
	//Party member Wnd
	
	private ulong summonStoneGuig;
	private UIButton[] partyMemberButton;
	private List<PartyPlayer> partyMemberStatus = new List<PartyPlayer>();
	
	
	
	public void  PartyMemberWnd ( UIButton sender )
	{
		ButtonOLD.summonState = true;
		partyMemberStatus.Clear();
		float buttonBackgroundPosX = 1.135f;
		float partyMemberLabelPosX = 1.02f;
		float buttonBackgroundPosY = -1.55f;
		float coef = 6.0f;
		float partyMemberLabelFontSize;
		float partyMemberLabelPosCoef = 0.02f;
		float fontCoef = 0.28f;
		Color color;			
		Color titleColor;
		color = new Color(0.243f, 0.118f, 0.059f, 1.000f);
		titleColor = new Color(0.906f, 0.702f, 0.267f, 1.000f);		
		
		float koefWidth = Screen.width * 0.7f / 899;
		float koefHeight = Screen.height * 0.7f / 645;
		
		float scr = Screen.width;
		popupContainer.removeAllChild(true);
		
		if(partyMemberWndTitle != null)
		{
			partyMemberWndTitle.clear();
			partyMemberWndTitle = null;
		}
		
		partyMemberLabel = null;
		
		//background
		partyMemberPopUpWndBackground = UIButton.create(mixToolkit6, "back_frame.png", "back_frame.png", 0, 0);
		partyMemberPopUpWndBackground.setSize(Screen.width * 0.47f, Screen.height * 0.57f);
		partyMemberPopUpWndBackground.position = new Vector3(Screen.width * 0.3f, -Screen.height * 0.20f, 2);	
		
		//top close btn
		topCloseBtn = UIButton.create(mixToolkit6, "close.png", "close.png", 0, 0);
		topCloseBtn.setSize(Screen.width * 0.52f / 899 * 61, Screen.height * 0.7f / 645 * 57);
		topCloseBtn.position = new Vector3(Screen.width * 0.75f, -Screen.height * 0.170f, 1);
		topCloseBtn.hidden = false;
		topCloseBtn.onTouchDown += ClosePartyMemberWnd;	
		
		//bottom close btn
		bottomCloseBtn = UIButton.create(mixToolkit6, "Close_butt.png", "Close_butt.png", 0, 0);
		bottomCloseBtn.setSize(koefWidth * 200, koefHeight * 50);
		bottomCloseBtn.position = new Vector3(Screen.width * 0.60f, -Screen.height * 0.68f, 1);
		bottomCloseBtn.hidden = false;
		bottomCloseBtn.onTouchDown += ClosePartyMemberWnd;
		
		//title
		titleFontSize = scr / 960f * 0.60f;
		partyMemberWndTitle = text3.addTextInstance("Summon character", Screen.width * 0.54f, Screen.height * 0.23f, titleFontSize, -1, titleColor, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
		//decor texture
		partyMemberPopUpWndDecor = mixToolkit6.addSprite( "decor_elements.png", partyMemberPopUpWndBackground.position.x * 1.13f, -partyMemberPopUpWndBackground.position.y * 1.5f, -4 );
		partyMemberPopUpWndDecor.setSize(Screen.width * 0.40f, Screen.height * 0.35f );
		
		//paper texture
		paperBackground = mixToolkit6.addSprite( "paper_frame.png", partyMemberPopUpWndBackground.position.x * 1.1f, -partyMemberPopUpWndBackground.position.y * 2.0f, 2 );
		paperBackground.setSize(Screen.width * 0.25f, Screen.height * 0.27f );
		
		int playerCount = mainPlayer.group.getPlayersCount();
		
		for(byte j = 0; j < playerCount; j++)
		{
			PartyPlayer plr = mainPlayer.group.getOnlinePlayer(j);
			if(plr != null)
			{
				partyMemberStatus.Add(plr);
			}
		}
		if(mainPlayer)
		{
			buttonBackground = new UIButton[playerCount];
			partyMemberLabel = new UITextInstance[playerCount];
		}
		
		if(partyMemberStatus != null)
		{
			for(int i = 0; i < partyMemberStatus.Count; i++)
			{				
				buttonBackground[i] = UIButton.create(mixToolkit6, "hilight_line.png", "hilight_line.png", 0, 0);
				buttonBackground[i].setSize(Screen.width * 0.23f, koefHeight * 43);
				buttonBackground[i].position = new Vector3(partyMemberPopUpWndBackground.position.x * buttonBackgroundPosX, paperBackground.height * buttonBackgroundPosY - i * (buttonBackground[i].height + coef), -5);
				buttonBackground[i].info = i;
				buttonBackground[i].hidden = false;
				buttonBackground[i].onTouchDown += SelectPartyMember;
				
				partyMemberLabelFontSize = scr / 960f * fontCoef;
				partyMemberLabel[i] = text3.addTextInstance(" ", buttonBackground[i].position.x * partyMemberLabelPosX, -buttonBackground[i].position.y + buttonBackground[i].height * partyMemberLabelPosCoef, partyMemberLabelFontSize, -6,/*Color.black*/color, UITextAlignMode.Left, UITextVerticalAlignMode.Top);	
				partyMemberLabel[i].text = ((partyMemberStatus[i] as PartyPlayer).name);
			}			
		}		
		
		//end of function	
		popupContainer.hidden = true;
		
		popupContainer.addChild(partyMemberPopUpWndBackground, topCloseBtn, bottomCloseBtn, partyMemberPopUpWndDecor,paperBackground);
		
		for(int k = 0; k < buttonBackground.Length; k++)
		{
			popupContainer.addChild(buttonBackground[k]);
		}
	}
	
	void  SelectPartyMember ( UIButton sender )
	{		
		ButtonOLD.summonState = true;
		if(partyMemberStatus[sender.info] != null )
		{
			WorldPacket pkt = new WorldPacket();
			pkt.SetOpcode(OpCodes.CMSG_SET_SELECTION);
			pkt.Append(ByteBuffer.Reverse(partyMemberStatus[sender.info].guid));
			RealmSocket.outQueue.Add(pkt);
			
			pkt = new WorldPacket();
			pkt.SetOpcode(OpCodes.CMSG_GAMEOBJ_USE);
			pkt.Append(ByteBuffer.Reverse(summonStoneGuig));
			RealmSocket.outQueue.Add(pkt);
			ClosePartyMemberWnd(null);	
		}
	}
	
	private void  ClosePartyMemberWnd (UIButton sender)
	{
		Debug.Log("close papry member window");
		StartCoroutine(DestroyPartyWnd());
	}
	
	
	
	public void  setSummonStoneGuid ( ulong value )
	{
		summonStoneGuig = value;
	}
	
	IEnumerator  DestroyPartyWnd ()
	{
		float timeToClose = 0.3f;
		yield return new WaitForSeconds(timeToClose);
		ButtonOLD.summonState = false;
		if(partyMemberWndTitle != null)
		{
			partyMemberWndTitle.clear();
			partyMemberWndTitle = null;
		}
		if(partyMemberLabel != null)
		{
			for(int k = 0; k < partyMemberLabel.Length; k++)
			{
				if(partyMemberLabel[k] != null)
				{
					partyMemberLabel[k].clear();	
				}
			}
		}
		popupContainer.removeAllChild(true);
	}
	
	public void  InviteToParty ( string name )
	{
		DestroyPartyInvitation();
		
		partyBackground = UIPrime31.myToolkit4.addSprite("popup_background.png", Screen.width*0.2f, Screen.height*0.22f, 5);
		partyBackground.setSize(Screen.width*0.6f, Screen.height*0.45f);
		
		partyDecoration = UIPrime31.questMix.addSprite("party_decoration.png", partyBackground.position.x + partyBackground.width*0.05f, -partyBackground.position.y + partyBackground.height*0.1f, 4);
		partyDecoration.setSize(partyBackground.width*0.9f, partyBackground.height*0.55f);
		
		partyAccept = UIButton.create(UIPrime31.questMix, "yes.png", "yes.png", partyBackground.position.x + partyBackground.width*0.2f, -partyBackground.position.y + partyBackground.height*0.75f, 4);
		partyAccept.setSize(partyBackground.width*0.22f, partyBackground.height*0.16f);
		partyAccept.onTouchDown += PartyAcceptDelegate;
		
		partyDecline = UIButton.create(UIPrime31.questMix, "no.png", "no.png", partyBackground.position.x + partyBackground.width*0.58f, -partyBackground.position.y + partyBackground.height*0.75f, 4);
		partyDecline.setSize(partyBackground.width*0.22f, partyBackground.height*0.16f);
		partyDecline.onTouchDown += PartyDeclineDelegate;
		
		partyCloseButton = UIButton.create( UIPrime31.myToolkit4, "close_button.png", "close_button.png", 0, 0, 0);
		partyCloseButton.setSize(partyBackground.height*0.2f, partyBackground.height*0.2f);
		partyCloseButton.position = new Vector3( partyBackground.position.x + partyBackground.width - partyCloseButton.width/1.5f, partyBackground.position.y + partyCloseButton.height/3, 4);
		partyCloseButton.onTouchDown += PartyDeclineDelegate;
		
		partyContainer.addChild(partyBackground, partyDecoration, partyAccept, partyDecline, partyCloseButton);
		
		partyNameText = text3.addTextInstance(name + " has invited\nyou to join his party!", partyBackground.position.x + partyBackground.width * 0.55f, -partyBackground.position.y + partyBackground.height*0.08f, UID.TEXT_SIZE*0.8f, 4, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
		partyQuestion = text3.addTextInstance("Do you want to join?", partyBackground.position.x + partyBackground.width * 0.6f, -partyBackground.position.y + partyBackground.height*0.52f, UID.TEXT_SIZE*0.8f, 4, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
	}
	
	void  PartyAcceptDelegate ( UIButton sender )
	{
		mainPlayer.session.handleOpcode(OpCodes.CMSG_GROUP_ACCEPT, null);
		DestroyPartyInvitation();
	}
	
	void  PartyDeclineDelegate ( UIButton sender )
	{
		mainPlayer.session.handleOpcode(OpCodes.CMSG_GROUP_DECLINE, null);
		DestroyPartyInvitation();
	}
	
	void  DestroyPartyInvitation ()
	{
		partyContainer.removeAllChild(true);
		
		if(partyNameText!=null)
		{
			partyNameText.clear();
			partyNameText = null;
		}
		
		if(partyQuestion!=null)
		{
			partyQuestion.clear();
			partyQuestion = null;
		}
	}
	
	void  FreezeLootForRoll ( bool val )
	{
		foreach(UISprite child in lootItemsContainer._children)
		{
			if(child.GetType() == typeof(UIButton))
			{	
				(child as UIButton).disabled = val;
			}
		}
	}
	
	void  OnDestroy ()
	{
		#if UNITY_EDITOR
		Debug.LogWarning("Interface destroyed!");
		#endif
	}
	
	float SetDefaultCameraSpeed ()
	{
		float returnValue = 0.0f;
		int width = Screen.width;
		int height = Screen.height;
		float dpi;
		float diag = 0.0f;
		
		dpi = Screen.dpi;
		if (dpi < 0.01f)
		{
			returnValue = 0.5f;
		}
		else
		{
			diag = Mathf.Sqrt(Mathf.Pow(width, 2) + Mathf.Pow(height, 2));
			diag = diag / dpi;
			
			if(diag < 3.5f)
			{
				returnValue = 0.1f;
			}
			else if(diag < 5.5f)
			{
				returnValue = 0.4f;
			}
			else if(diag < 11.0f)
			{
				returnValue = 0.8f;
			}
			else
			{
				returnValue = 1;
			}
		}
		return returnValue;
	}
	
	UISprite addIconSprite ( string iconName,   int xPos,   int yPos,   int depth )
	{
		UISprite sprite;
		UIToolkit tempUIToolkit;
		tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+iconName);
		sprite = tempUIToolkit.addSprite(iconName, xPos, yPos, depth);
		if(sprite == null)
		{
			SinglePictureUtils.DestroySingleUISprite(sprite);
			//			sprite = mixToolkit7.addSprite(iconName, xPos, yPos, depth);
		}
		if(sprite == null)
		{
			tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/noicon.png");
			sprite = tempUIToolkit.addSprite("noicon.png", xPos, yPos, depth);
		}
		return sprite;
	}
	
	void  ClearSpellEquipSlot ( UIButton slotButton )
	{
		if (slotButton != null) 
		{
			UIToolkit tempUIToolkit = slotButton.manager;
			slotButton.destroy();
			slotButton = null; 
			if(tempUIToolkit)
			{
				GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
			}
		}
	}
	
	public void  setInventoryState ( bool val )
	{
		inventoryState = val;
		//		if(!inventoryState)
		//		{
		inventoryManager.ClearInventory();
		//		}
	}
	
}