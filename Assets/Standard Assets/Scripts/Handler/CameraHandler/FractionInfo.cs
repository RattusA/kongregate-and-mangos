﻿
public class FractionInfo
{
	public int entry;			// entry key in DBC file
	public int id;				// faction in database
	public int standing;		// standing in database
	public int flags;			// flags in database
	public string name;
	
	public FractionInfo ()
	{
		entry = 0;
		id = 0;
		flags = 0;
		standing = 0;
		name = "";
	}
}