using UnityEngine;

public class ActionBarSlot
{
	private MainPlayer mainPlayer = GameObject.Find("player").GetComponentInChildren<MainPlayer>();
	public uint objectID = 0;
	public ActionButtonType slotType = ActionButtonType.ACTION_BUTTON_SPELL;
	
	public ActionBarSlot ( ActionButtonType resiveType, uint resiveID )
	{
		objectID = resiveID;
		slotType = resiveType;
	}
	
	public void  DoAction ( BaseObject target )
	{
		if(mainPlayer.m_deathState != DeathState.ALIVE)
		{
			Debug.Log("No Spell on action bar");
			return;
		}
		
		if(mainPlayer.m_deathState == DeathState.DEAD)
		{
			Debug.Log("Player is Dead");
			return;
		}
		
		switch(slotType)
		{
		case ActionButtonType.ACTION_BUTTON_SPELL:
			if(mainPlayer.getIsCasting() == objectID)
				return;
			
			Spell spell = mainPlayer.playerSpellManager.GetSpellByID(objectID);	
			
			if(spell == null)
			{
				spell = mainPlayer.petSpellManager.GetPetSpellByID(objectID);
				if(mainPlayer.petSpellManager.IsPetSpell(objectID))
				{
					if((target != null) || (target == null && spell.IsHaveTargetEffect(TargetsFlags.TARGET_MASTER)))
					{
						if(target == null)
						{
							target = mainPlayer;
						}
						Debug.LogWarning("TODO!");
						mainPlayer.petSpellManager.DoPetCommand(objectID, (uint)PetActionFlag.ACTION_PET_ACTIVE_SPELL, target.guid); //0x81 = 129
					}
					return;
				}
				
				Debug.Log("Player don't know this spell");
				return;
			}
			
			if(spell.ID == 13262)
			{
				mainPlayer.UpdateCraftInfo();
				WorldSession.interfaceManager.DrawDisassembleWindow(DisassembleWindowType.ENCHANTINGE_WINDOW_TYPE);
				return;
			}
			
			if(spell.ID == 31252)
			{
				mainPlayer.UpdateCraftInfo();
				WorldSession.interfaceManager.DrawDisassembleWindow(DisassembleWindowType.JEWELCRAFTING_WINDOW_TYPE);
				return;
			}
			
			if(spell.ID == 51005)
			{
				mainPlayer.UpdateCraftInfo();
				WorldSession.interfaceManager.DrawDisassembleWindow(DisassembleWindowType.INSCRIPTION_WINDOW_TYPE);
				return;
			}
			
			// AoE
			Swipe swipeScript = Camera.main.GetComponent<Swipe>();
			if(swipeScript.isUsedSelectingAoEPosition())
			{
				swipeScript.DisableSelectingAoEPosition();
				return;
			}
			
			uint dstFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_DEST_LOCATION;
			if ((spell.Targets & dstFlag) != 0)
			{
				swipeScript.EnableSelectingAoEPosition();
				swipeScript.AoESpell(spell);
				return;
			}
			//End AoE
			
			// if spell is mount then unmount
			if(mainPlayer.charMount)
			{
				mainPlayer.pkt = new WorldPacket();
				mainPlayer.pkt.SetOpcode(OpCodes.CMSG_CANCEL_MOUNT_AURA);
				RealmSocket.outQueue.Add(mainPlayer.pkt);
			}
			
			if(spell.EffectApplyAuraName[0] == (int)AuraType.SPELL_AURA_MOUNTED)
			{					
				mainPlayer.playerSpellManager.CastSpell(spell, mainPlayer.guid, mainPlayer.TransformParent.position);
				return;
			}
			
			if((target == mainPlayer || target == null) && spell.ID == 54646)
			{
				return;
			}
			
			if(target != null)
			{
				mainPlayer.playerSpellManager.CastSpell(spell, target.guid, target.TransformParent.position);
				//Debug.Log("Face spell");
				
				//tutorial code
				//					if (WorldSession.player.tutorialInstance)
				//					{
				//						if (mainPlayer.tutHandler.tutorialStep == 10 && (target as Unit).Name == "Battle Dummy")
				//						{
				//							mainPlayer.tutHandler.spell = true;						
				//						}
				//						if (!WorldSession.player.tutHandler.stepsDone[10] && WorldSession.player.tutHandler.spell)
				//						{
				//							WorldSession.player.tutHandler.stepsDone[10] = true;
				//						}			
				//						if (WorldSession.player.tutHandler.tutorialStep == 10 && (WorldSession.player.target as Unit).Name == "Battle Dummy" && WorldSession.player.tutHandler.spell)
				//						{
				//							WorldSession.player.tutHandler.advanceTutorial();						
				//						}
				//					}
				
				//end of tutorial code
			}		
			else
			{
				mainPlayer.playerSpellManager.CastSpell(spell, mainPlayer.guid, mainPlayer.TransformParent.position);
			}
			break;
			
		case ActionButtonType.ACTION_BUTTON_MACRO:
			switch (objectID)
			{
			case 0:
				if(mainPlayer.CanJump)
				{
					mainPlayer.StartJump();
				}
				mainPlayer.CanJump = false;
				break;
				
			case 1:
				targetTab.closestTarget();
				break;
				
			case 2:
				targetTab.nextTarget();
				break;
				
			case 3: // Make a trade request
				if(target != null && target.IsPlayer())
				{
					if((Player.GetFactionByRace((Races)mainPlayer.race) == Player.GetFactionByRace((Races)(target as OtherPlayer).race)) && 
					   !(mainPlayer.HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_IN_COMBAT)))
					{
						Debug.Log("TARGET'S GUID IS : " + target.guid);
						WorldPacket pkt = new WorldPacket();
						pkt.SetOpcode(OpCodes.CMSG_INITIATE_TRADE);
						pkt.Append(ByteBuffer.Reverse(target.guid));
						RealmSocket.outQueue.Add(pkt);
						
						mainPlayer.trade.RequestTrade(mainPlayer.target);
					}
				}
				break;
				
			case 4:	// Make a battle request
				if(target != null && target.IsPlayer())
				{
					if((Player.GetFactionByRace((Races)mainPlayer.race) == Player.GetFactionByRace((Races)(target as Player).race)) && 
					   !(mainPlayer.HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_IN_COMBAT)))
					{
						mainPlayer.duel.Request(mainPlayer, (Player)target);
					}
				}
				break;
				
			case 5:	// Inspect other player
				if(target != null && target.IsPlayer())
				{
					if((Player.GetFactionByRace((Races)mainPlayer.race) == Player.GetFactionByRace((Races)(target as OtherPlayer).race)) && 
					   !(mainPlayer.HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_IN_COMBAT)))
					{
						mainPlayer.InspectMgr.InspectTarget(target);
					}
				}
				break;
				
			case 6: // Invite to party
				if(target != null && target.IsPlayer())
				{
					if (!(mainPlayer.HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_IN_COMBAT)))
					{
						mainPlayer.SendMessage2("/invite "+(target as OtherPlayer).Name);
					}
				}
				break;
				
			case 7: // Leave party
				if(target != null && target.IsPlayer())
				{
					if (!(mainPlayer.HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_IN_COMBAT)))
					{
						mainPlayer.SendMessage2("/leave");
					}
				}
				break;
				
			case 23: 
				if(target != null && mainPlayer.pet != null)
				{
					mainPlayer.DoPetCommand( 2, 0x07, target.guid);
				}
				break;
				
			case 24: 
				if(mainPlayer.pet != null)
				{
					mainPlayer.DoPetCommand( 1, 0x06, mainPlayer.pet.guid);
				}
				break;
				
			case 25: 
				if(mainPlayer.pet != null)
				{
					mainPlayer.DoPetCommand( 1, 0x07, mainPlayer.pet.guid);
				}
				break;	
				
			case 26: 
				if(mainPlayer.pet != null)
				{
					mainPlayer.DoPetCommand( 0, 0x07, mainPlayer.pet.guid);
				}
				break;
				
			case 27: 
				if(mainPlayer.pet != null)
				{
					mainPlayer.DoPetCommand( 2, 0x06, mainPlayer.pet.guid);
				}
				break;
				
			case 28: 
				if(mainPlayer.pet != null)
				{
					mainPlayer.DoPetCommand( 0, 0x06, mainPlayer.pet.guid);
				}
				break;
				
			default:
				mainPlayer.UseMacros(objectID);
				break;
			}
			break;
			
		case ActionButtonType.ACTION_BUTTON_ITEM:
			if(mainPlayer.charMount)
			{
				return;
			}
			
			Item item = mainPlayer.itemManager.GetItemByID(objectID);
			if(item != null)
			{
				Debug.Log("ITEM DATA: " + item.entry);
				if(item.inventoryType == 0)
				{
					item.UseItem();
				}
				else
				{
					ItemSpell itemSpellID = item.GetOnActiveSpell(objectID);
					Spell itemSpell = null;
					if(itemSpellID != null)
					{
						itemSpell = SpellManager.CreateOrRetrive(itemSpellID.ID);
					}
					
					if(!item.IsEquiped())
					{
						item.EquipItem();
						if(itemSpell != null)
						{
							Cooldown cooldown = new Cooldown(30.0f, 30.0f);
							mainPlayer.cooldownManager.playerCooldownList.SetCooldown(itemSpell, cooldown);
						}
					}
					else
					{
						if(itemSpell != null)
						{
							item.UseItem();
						}
					}
				}
			}
			break;
		}
	}
	
	/**
  	 * @return Returns the spell of this slot
  	 */
	public Spell GetSpell ()
	{
		Spell spell = null;
		uint itemSpellID = 0;
		
		switch(slotType)
		{
		case ActionButtonType.ACTION_BUTTON_SPELL:
			spell = mainPlayer.playerSpellManager.GetSpellByID(objectID);
			break;
			
		case ActionButtonType.ACTION_BUTTON_ITEM:
			itemSpellID = mainPlayer.itemManager.GetItemByID(objectID).GetOnActiveSpell().ID;
			if(itemSpellID != 0)
				spell = SpellManager.GetSpell(itemSpellID);
			break;
			
		default:
			//Debug.LogWarning("Slot type " + slotType + " not supported.");
			break;
		}
		
		return spell;
	}
}
