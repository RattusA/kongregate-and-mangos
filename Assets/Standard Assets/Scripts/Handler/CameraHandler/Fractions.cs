﻿using UnityEngine;
using System;
using System.Collections.Generic;

public enum FactionFlags
{
	FACTION_FLAG_VISIBLE 			= 0x01, // makes visible in client (set or can be set at interaction with target of this faction)
	FACTION_FLAG_AT_WAR 			= 0x02, // enable AtWar-button in client. player controlled (except opposition team always war state), Flag only set on initial creation
	FACTION_FLAG_HIDDEN 			= 0x04, // hidden faction from reputation pane in client (player can gain reputation, but this update not sent to client)
	FACTION_FLAG_INVISIBLE_FORCED 	= 0x08, // always overwrite FACTION_FLAG_VISIBLE and hide faction in rep.list, used for hide opposite team factions
	FACTION_FLAG_PEACE_FORCED 		= 0x10, // always overwrite FACTION_FLAG_AT_WAR, used for prevent war with own team factions
	FACTION_FLAG_INACTIVE 			= 0x20, // player controlled, state stored in characters.data ( CMSG_SET_FACTION_INACTIVE )
	FACTION_FLAG_RIVAL 				= 0x40, // flag for the two competing outland factions
	FACTION_FLAG_TEAM_REPUTATION 	= 0x80  // faction has own reputation standing despite teaming up sub-factions; spillover from subfactions will go this instead of other subfactions
};

static public class Fractions
{
	static Dictionary<int, FractionInfo> fractionState = new Dictionary<int, FractionInfo>();
	static int[] listedFractions = new int[]{59, 92, 93, 270, 529, 609, 910};
	
	static public FractionInfo GetFactionInfo ( int factionId  )
	{
		FractionInfo factionInf = new FractionInfo();
		fractionState.TryGetValue(factionId, out factionInf);
		return factionInf;
	}
	
	static public void  InitializationReputationData ( WorldPacket pkt )
	{
		int i = 0;		
		try
		{
			FractionInfo fraction;					
			uint packetId = pkt.ReadReversed32bit();
			
			fractionState.Clear();
			
			for(i = 0; i < packetId; i++)
			{
				fraction = new FractionInfo();	
				fraction.entry = (int)DefaultTabel.fractionEntry[i.ToString()];
				fraction.name = (string)DefaultTabel.fractionName[fraction.entry.ToString()];
				fraction.id = i;
				fraction.flags = (int)pkt.Read();
				fraction.standing = pkt.ReadInt();
				fractionState[fraction.entry] = fraction;
			}
		}
		catch( Exception e  )
		{
			Debug.Log("Exeption on initialize fraction on number - " + i);
		}
	}
	
	static public void  SetFractionReputation ( WorldPacket pkt )
	{		
		float packetId = pkt.ReadFloat(); // a 0 value
		
		int isRankWasChanged = pkt.Read();
		int ranksCount = pkt.ReadInt(); // a 1 value
		int reputationListId = pkt.ReadInt();
		int newReputationValue = pkt.ReadInt();
		int factionId = (int)DefaultTabel.fractionEntry[reputationListId.ToString()];
		fractionState[factionId].standing = newReputationValue;
	}
	
	static string checkReputationText ( int repValue )
	{
		string reputationText = "";
		
		if(repValue >= 42000)
		{
			reputationText = "Worshiped";
		}
		else if(repValue >= 21000)
		{
			reputationText = "Esteemed";
		}
		else if(repValue >= 9000)
		{
			reputationText = "Respected";
		}
		else if(repValue >= 3000)
		{
			reputationText = "Hospitable";
		}
		else if(repValue >= 0)
		{
			reputationText = "Unconcerned";
		}
		else if(repValue >= -3000)
		{
			reputationText = "Aggressive";
		}
		else if(repValue >= -6000)
		{
			reputationText = "Hateful";
		}
		else
		{
			reputationText = "Loathed";
		}
		
		return reputationText;
	}
	
	static string checkReputationValue ( int repValue )
	{
		string reputationMaxValue = "";
		
		if(repValue >= 42000) 				// Exalted
		{
			reputationMaxValue = actualReputationValue(repValue, 42000) + "/2000";
		}
		else if(repValue >= 21000) 			// Revered
		{
			reputationMaxValue = actualReputationValue(repValue, 21000) + "/42000";
		}
		else if(repValue >= 9000)			// Honored
		{
			reputationMaxValue = actualReputationValue(repValue, 6000) + "/24000";
		}
		else if(repValue >= 3000)			// Friendly
		{
			reputationMaxValue = actualReputationValue(repValue, 3000) + "/12000";
		}
		else if(repValue >= 0)				// Neutral
		{
			reputationMaxValue = (repValue * 2) + "/6000";
		}
		else if(repValue >= -3000)			// Unfriendly
		{
			reputationMaxValue = actualReputationValue(repValue, -3000) + "/6000";
		}
		else if(repValue >= -6000)			// Hostile
		{
			reputationMaxValue = actualReputationValue(repValue, -6000) + "/6000";
		}
		else								// Hated
		{
			reputationMaxValue = actualReputationValue(repValue, -42000) + "/72000";;
		}
		
		return reputationMaxValue;
	}
	
	static string actualReputationValue ( int repValue, int mod )
	{
		int newValue = Mathf.Abs(repValue - mod) * 2;
		return newValue.ToString();
	}
	
	static public string GetReputationInfo ()
	{		
		string reputationText = "";
		
		for(int index = 0; index < listedFractions.Length; index++)
		{
			FractionInfo factionInf = new FractionInfo();
			fractionState.TryGetValue(listedFractions[index], out factionInf);
			if(factionInf != null)
			{
				if((factionInf.flags&1) == 1)
				{
					reputationText += factionInf.name + " " + checkReputationText(factionInf.standing) + ": ";
					reputationText += checkReputationValue(factionInf.standing) + "\n";
				}
			}
		}
		return reputationText;
	}
}