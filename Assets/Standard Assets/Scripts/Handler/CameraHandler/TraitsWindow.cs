﻿using UnityEngine;

public class TraitsWindow
{	
	private UIText text1;
	private UIText text2;
	
	// Общие переменные
	private UIAbsoluteLayout traitsWindowContainer;
	private UIAbsoluteLayout traitsButtonsContainer;
	
	private UIButton traitsOrnaments;
	private UITextInstance unspendTraitsPointsText;
	private UITextInstance spendTraitsPointsText;	
	
	public UIButton[] traitsButton = new UIButton[16];
	private UIButton[] traitRank = new UIButton[16];
	
	public string[] traitsTabName = {"3_selected.png", "3_unselected.png", "3_unselected.png"};
	private string[] traitsTabText = {"Not Found", "Not Found", "Not Found"};
	private string traitsPetTabText = "";
	
	// Переменные для окна игрока
	private UIButton[] traitsTab = new UIButton[3];
	
	private UITextInstance[] traitsText = new UITextInstance[3];
	private UIButton nextPageButton;
	private UIButton prevPageButton;
	
	private string traitsPetName;
	public int petFamily = 0;
	public byte page = 1;
	
	// Дополнительные переменные	
	private string descriptionText = "";
	
	private float txtSize = UID.TEXT_SIZE * 1.35f;
	private int activeTab = 0;
	
	private UIAbsoluteLayout traitsInfoWindow;
	private UIButton popupWindow;
	private UIButton investButton;
	private UIButton2 cancelInvestButton;
	private UIButton ornamentPopup;
	private UIButton2 investSpellButton;
	private UIButton closeXPopup;
	
	private UITextInstance investRankText;
	private UITextInstance spellNameText;
	
	private bool  popupFlag = false;
	private ScrollZone TextZone = new ScrollZone();

	public TraitsWindow ()
	{		
		text1 = new UIText(UIPrime31.myToolkit1, "fontiny dark", "fontiny dark.png");
		text2 = new UIText(UIPrime31.myToolkit1, "fontiny title", "fontiny title.png");
		
		traitsWindowContainer = new UIAbsoluteLayout();
		traitsButtonsContainer  = new UIAbsoluteLayout();
		traitsInfoWindow = new UIAbsoluteLayout();
		
		activeTab = 0;
	}
	
	public void  initTraitsWindow ()
	{		
		ButtonInfo traitsOrnamentsInfo = new ButtonInfo(0, Screen.height * 0.12f, Screen.width, Screen.height * 0.88f);
		traitsOrnaments = UIButton.create(UIPrime31.myToolkit3, "traits_background.png", "traits_background.png", traitsOrnamentsInfo.positionX, traitsOrnamentsInfo.positionY, 4);
		traitsOrnaments.setSize(traitsOrnamentsInfo.sizeX, traitsOrnamentsInfo.sizeY);
		traitsOrnaments.centerize();
		
		ButtonInfo[] traitsTabInfo = new ButtonInfo[3];
		traitsTabInfo[0] = new ButtonInfo(Screen.width * 0.065f, Screen.height * 0.23f, Screen.width * 0.26f, Screen.height * 0.085f);
		traitsTabInfo[1] = new ButtonInfo(Screen.width * 0.370f, Screen.height * 0.23f, Screen.width * 0.26f, Screen.height * 0.085f);
		traitsTabInfo[2] = new ButtonInfo(Screen.width * 0.675f, Screen.height * 0.23f, Screen.width * 0.26f, Screen.height * 0.085f);
		
		traitsTab[0] = UIButton.create(UIPrime31.myToolkit3, traitsTabName[0], traitsTabName[0], traitsTabInfo[0].positionX, traitsTabInfo[0].positionY, 1);
		traitsTab[0].setSize(traitsTabInfo[0].sizeX, traitsTabInfo[0].sizeY);
		traitsTab[0].onTouchDown += traitsTabDelegate;
		traitsTab[0].oInfo = TraitsManager.GetTabID(traitsTab[0].info);
		traitsTab[0].info = 0;
		
		traitsTab[1] = UIButton.create(UIPrime31.myToolkit3, traitsTabName[1], traitsTabName[1], traitsTabInfo[1].positionX, traitsTabInfo[1].positionY, 1);
		traitsTab[1].setSize(traitsTabInfo[1].sizeX, traitsTabInfo[1].sizeY);
		traitsTab[1].onTouchDown += traitsTabDelegate;
		traitsTab[1].oInfo = TraitsManager.GetTabID(traitsTab[1].info);
		traitsTab[1].info = 1;
		
		traitsTab[2] = UIButton.create(UIPrime31.myToolkit3, traitsTabName[2], traitsTabName[2], traitsTabInfo[2].positionX, traitsTabInfo[2].positionY, 1);
		traitsTab[2].setSize(traitsTabInfo[2].sizeX, traitsTabInfo[2].sizeY);
		traitsTab[2].onTouchDown += traitsTabDelegate;
		traitsTab[2].oInfo = TraitsManager.GetTabID(traitsTab[2].info);
		traitsTab[2].info = 2;
		
		traitsWindowContainer.addChild(traitsOrnaments, traitsTab[0], traitsTab[1], traitsTab[2]);
		
		showTraitsButtons();
	}
	
	private void  showTraitsButtons ()
	{
		TraitsManager.initTraitsInfoByTab(activeTab, page);
		
		ButtonInfo nextPageButtonInfo = new ButtonInfo(Screen.width * 0.9f, Screen.height * 0.6f, Screen.width * 0.06f, Screen.height * 0.09f);
		nextPageButton = UIButton.create(UIPrime31.interfMix, "right_arrow.png", "right_arrow.png", nextPageButtonInfo.positionX, nextPageButtonInfo.positionY, 3);
		nextPageButton.setSize(nextPageButtonInfo.sizeX, nextPageButtonInfo.sizeY);
		nextPageButton.onTouchUpInside += nextPageDelegate;
		
		ButtonInfo prevPageButtonInfo = new ButtonInfo(Screen.width * 0.04f, Screen.height * 0.6f, Screen.width * 0.06f, Screen.height * 0.09f);
		prevPageButton = UIButton.create(UIPrime31.interfMix, "left_arrow.png", "left_arrow.png", prevPageButtonInfo.positionX, prevPageButtonInfo.positionY, 3);
		prevPageButton.setSize(prevPageButtonInfo.sizeX, prevPageButtonInfo.sizeY);
		prevPageButton.onTouchUpInside += prevPageDelegate;
		
		switch(page)
		{
		case 1:
			prevPageButton.hidden = true;
			nextPageButton.hidden = false;
			break;
		case 2:
			prevPageButton.hidden = false;
			nextPageButton.hidden = true;
			break;
		}
		
		traitsButtonsContainer.addChild(nextPageButton, prevPageButton);
		
		float[] traitsTabNamePositionX = new float[3];
		float[] traitsTabNamePositionY = new float[3];
		
		traitsTabNamePositionX[0] = Screen.width * 0.195f;
		traitsTabNamePositionY[0] = Screen.height * 0.24f;
		traitsTabNamePositionX[1] = Screen.width * 0.500f;
		traitsTabNamePositionY[1] = Screen.height * 0.24f;
		traitsTabNamePositionX[2] = Screen.width * 0.805f;
		traitsTabNamePositionY[2] = Screen.height * 0.24f;
		
		txtSize = Screen.height / 640f * 0.48f;
		
		traitsTabText[0] = TraitsManager.GetTabName(0);
		traitsTabText[1] = TraitsManager.GetTabName(1);
		traitsTabText[2] = TraitsManager.GetTabName(2);
		
		txtSize = Screen.height / 640f * 0.5f;
		
		traitsText[0] = text2.addTextInstance(traitsTabText[0], traitsTabNamePositionX[0], traitsTabNamePositionY[0], txtSize, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		traitsText[1] = text2.addTextInstance(traitsTabText[1], traitsTabNamePositionX[1], traitsTabNamePositionY[1], txtSize, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		traitsText[2] = text2.addTextInstance(traitsTabText[2], traitsTabNamePositionX[2], traitsTabNamePositionY[2], txtSize, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
		float unspendTraitsPointsPositionX = Screen.width * 0.946f;
		float unspendTraitsPointsPositionY = Screen.height * 0.125f;
		float spendTraitsPointsPositionX = Screen.width * 0.054f;
		float spendTraitsPointsPositionY = Screen.height * 0.125f;
		
		unspendTraitsPointsText = text1.addTextInstance(TraitsManager.unspendTraitsInTab.ToString(), unspendTraitsPointsPositionX, unspendTraitsPointsPositionY, txtSize, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		spendTraitsPointsText = text1.addTextInstance(TraitsManager.spendTraitsInTab.ToString(), spendTraitsPointsPositionX, spendTraitsPointsPositionY, txtSize, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
		// Добавляем иконки для талантов		
		float buttonSpacing = 0;
		float buttonHeight = 0;
		
		for (byte index = 0; index < 16; ++index)
		{
			if ((index % 4 == 0) && (index > 1))
			{
				buttonSpacing = 0;
				buttonHeight += Screen.height * 0.15f;
			}
			
			if(TraitsManager.traitsButtonInfo[index] != null)
			{
				ButtonInfo traitsButtonInfo = new ButtonInfo(Screen.width * 0.13f + buttonSpacing,
				                                             Screen.height * 0.38f + buttonHeight,
				                                             Screen.width * 0.075f,
				                                             Screen.height * 0.1f);
				UIToolkit tempUIToolkit;
				if(TraitsManager.traitsButtonIcon[index].Equals("lock_button.png"))
				{
					tempUIToolkit = SinglePictureUtils.CreateUIToolkit("GUI/InGame/"+TraitsManager.traitsButtonIcon[index]);
				}
				else
				{
					tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+TraitsManager.traitsButtonIcon[index]);
				}
				traitsButton[index] = UIButton.create(tempUIToolkit,
				                                      TraitsManager.traitsButtonIcon[index],
				                                      TraitsManager.traitsButtonIcon[index],
				                                      traitsButtonInfo.positionX,
				                                      traitsButtonInfo.positionY,
				                                      3);
				traitsButton[index].setSize(traitsButtonInfo.sizeX, traitsButtonInfo.sizeY);
				traitsButton[index].info = index;
				traitsButton[index].onTouchDown += traitsButtonDelegate;
				
				traitsButtonsContainer.addChild(traitsButton[index]);
			}
			
			buttonSpacing += Screen.width * 0.22f;
		}
		
		initTraitsRank();
	}
	
	public void  initPetTraitsWindow ()
	{		
		ButtonInfo traitsOrnamentsInfo = new ButtonInfo(0, Screen.height * 0.12f, Screen.width, Screen.height * 0.88f);
		traitsOrnaments = UIButton.create(UIPrime31.myToolkit3, "traits_background.png", "traits_background.png", traitsOrnamentsInfo.positionX, traitsOrnamentsInfo.positionY, 4);
		traitsOrnaments.setSize(traitsOrnamentsInfo.sizeX, traitsOrnamentsInfo.sizeY);
		traitsOrnaments.centerize();
		
		ButtonInfo traitsTabInfo = new ButtonInfo(Screen.width * 0.370f, Screen.height * 0.23f, Screen.width * 0.26f, Screen.height * 0.085f);
		
		traitsTab[1] = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_selected.png", traitsTabInfo.positionX, traitsTabInfo.positionY, 1);
		traitsTab[1].setSize(traitsTabInfo.sizeX, traitsTabInfo.sizeY);
		
		traitsWindowContainer.addChild(traitsOrnaments, traitsTab[1]);
		
		showPetTraitsButtons();
	}
	
	private void  showPetTraitsButtons ()
	{
		TraitsManager.initTraitsInfoByTab(petFamily, page);
		
		ButtonInfo nextPageButtonInfo = new ButtonInfo(Screen.width * 0.9f, Screen.height * 0.6f, Screen.width * 0.06f, Screen.height * 0.09f);
		nextPageButton = UIButton.create(UIPrime31.interfMix, "right_arrow.png", "right_arrow.png", nextPageButtonInfo.positionX, nextPageButtonInfo.positionY, 3);
		nextPageButton.setSize(nextPageButtonInfo.sizeX, nextPageButtonInfo.sizeY);
		nextPageButton.onTouchUpInside += nextPageDelegate;
		
		ButtonInfo prevPageButtonInfo = new ButtonInfo(Screen.width * 0.04f, Screen.height * 0.6f, Screen.width * 0.06f, Screen.height * 0.09f);
		prevPageButton = UIButton.create(UIPrime31.interfMix, "left_arrow.png", "left_arrow.png", prevPageButtonInfo.positionX, prevPageButtonInfo.positionY, 3);
		prevPageButton.setSize(prevPageButtonInfo.sizeX, prevPageButtonInfo.sizeY);
		prevPageButton.onTouchUpInside += prevPageDelegate;
		
		//switch(page)
		//{
		//	case 1:
		prevPageButton.hidden = true;
		//		nextPageButton.hidden = false;
		//		break;
		//	case 2:
		//		prevPageButton.hidden = false;
		nextPageButton.hidden = true;
		//		break;
		//}
		
		traitsButtonsContainer.addChild(nextPageButton, prevPageButton);
		
		float traitsTabNamePositionX = Screen.width * 0.500f;
		float traitsTabNamePositionY = Screen.height * 0.24f;
		
		txtSize = Screen.height / 640f * 0.48f;
		
		traitsTabText[1] = TraitsManager.GetTabName(petFamily);
		
		txtSize = Screen.height / 640f * 0.5f;
		
		traitsText[1] = text2.addTextInstance(traitsTabText[1], traitsTabNamePositionX, traitsTabNamePositionY, txtSize, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
		float unspendTraitsPointsPositionX = Screen.width * 0.946f;
		float unspendTraitsPointsPositionY = Screen.height * 0.125f;
		float spendTraitsPointsPositionX = Screen.width * 0.054f;
		float spendTraitsPointsPositionY = Screen.height * 0.125f;
		
		unspendTraitsPointsText = text1.addTextInstance(TraitsManager.unspendTraitsInTab.ToString(), unspendTraitsPointsPositionX, unspendTraitsPointsPositionY, txtSize, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		spendTraitsPointsText = text1.addTextInstance(TraitsManager.spendTraitsInTab.ToString(), spendTraitsPointsPositionX, spendTraitsPointsPositionY, txtSize, -1, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
		// Добавляем иконки для талантов		
		float buttonSpacing = 0;
		float buttonHeight = 0;
		
		for (byte index = 0; index < 16; ++index)
		{
			if ((index % 4 == 0) && (index > 1))
			{
				buttonSpacing = 0;
				buttonHeight += Screen.height * 0.15f;
			}
			
			if(TraitsManager.traitsButtonInfo[index] != null)
			{
				ButtonInfo traitsButtonInfo = new ButtonInfo(Screen.width * 0.13f + buttonSpacing,
				                                             Screen.height * 0.38f + buttonHeight,
				                                             Screen.width * 0.075f,
				                                             Screen.height * 0.1f);
				UIToolkit tempUIToolkit;
				if(TraitsManager.traitsButtonIcon[index].Equals("lock_button.png"))
				{
					tempUIToolkit = SinglePictureUtils.CreateUIToolkit("GUI/InGame/"+TraitsManager.traitsButtonIcon[index]);
				}
				else
				{
					tempUIToolkit = SinglePictureUtils.CreateUIToolkit(TraitsManager.traitsButtonIcon[index]);
				}
				traitsButton[index] = UIButton.create(tempUIToolkit,
				                                      TraitsManager.traitsButtonIcon[index],
				                                      TraitsManager.traitsButtonIcon[index],
				                                      traitsButtonInfo.positionX,
				                                      traitsButtonInfo.positionY,
				                                      3);
				traitsButton[index].setSize(traitsButtonInfo.sizeX, traitsButtonInfo.sizeY);
				traitsButton[index].info = index;
				traitsButton[index].onTouchDown += traitsButtonDelegate;
				
				traitsButtonsContainer.addChild(traitsButton[index]);
			}
			
			buttonSpacing += Screen.width * 0.22f;
		}
		
		initTraitsRank();
	}
	
	private void  nextPageDelegate (UIButton sender)
	{
		page++;
		checkTraitsInfoWindow();
		clearTraitsButtons();
		showTraitsButtons();
	}
	
	private void  prevPageDelegate (UIButton sender)
	{
		page--;
		checkTraitsInfoWindow();
		clearTraitsButtons();
		showTraitsButtons();
	}
	
	private void  clearTraitsButtons ()
	{
		for(byte index = 0; index < 16; index++)
		{
			SinglePictureUtils.DestroySingleUISprite(traitsButton[index] as UISprite);
		}
		traitsButtonsContainer.removeAllChild(true);
		
		nextPageButton.destroy();
		prevPageButton.destroy();
		
		traitsTabText = new string[]{"Not Found", "Not Found", "Not Found"};
		
		if(traitsText[0] != null)
		{
			traitsText[0].clear();
		}
		if(traitsText[1] != null)
		{
			traitsText[1].clear();
		}
		if(traitsText[2] != null)
		{
			traitsText[2].clear();
		}
		
		unspendTraitsPointsText.clear();
		spendTraitsPointsText.clear();
	}
	
	private void  initTraitsRank ()
	{
		for (int index = 0; index < 16; index++)
		{
			if (!TraitsManager.traitsButtonIcon[index].Equals("lock_button.png") && TraitsManager.traitsButtonInfo[index].rank > 0)
			{
				string tName;
				int traitLvl = TraitsManager.traitsButtonInfo[index].rank;
				if(traitLvl > 0)
				{
					tName = traitLvl.ToString() + ".png";
					traitRank[index] = UIButton.create(UIPrime31.myToolkit3, tName, tName, 0, 0);
					traitRank[index].setSize(Screen.width * 0.03f, Screen.height * 0.045f);
					traitRank[index].position = new Vector3((traitsButton[index].position.x + Screen.width * 0.06f),
					                                    (traitsButton[index].position.y + Screen.height * 0.023f),
					                                    2.8f);
					traitsButtonsContainer.addChild(traitRank[index]);
				}
			}
		}
	}
	
	private void  traitsTabDelegate ( UIButton sender )
	{
		checkTraitsInfoWindow();
		destroyTraitsManager();
		clearTraitsButtons();
		
		traitsTabName[activeTab] = "3_unselected.png";
		traitsTabName[sender.info] = "3_selected.png";
		activeTab = sender.info;
		
		initTraitsWindow();
	}
	
	private void  traitsButtonDelegate ( UIButton sender )
	{
		TraitsManager.prepareTallentInfo(sender.info);
		initPopupWindow(sender.info);
	}
	
	public void  destroyTraitsManager ()
	{
		traitsWindowContainer.removeAllChild(true);
		
		traitsOrnaments.destroy();
		for (int i = 0; i < 3; i++)
		{
			if(traitsTab[i] != null)
			{
				traitsTab[i].destroy();
			}
		}
		clearTraitsButtons();
	}
	
	// Окно обучения талантов
	private void  initPopupWindow ( int index )
	{
		if(popupFlag)
		{
			return;
		}
		else
		{
			popupFlag = true;
		}
		
		// Фон
		float popupWindowPositionX = Screen.width * 0.05f;
		float popupWindowPositionY  = -Screen.height * 0.30f;
		float popupWindowSizeX  = Screen.width * 0.90f;
		float popupWindowSizeY  = Screen.height * 0.7f;
		
		popupWindow = UIButton.create(UIPrime31.myToolkit3, "main.png", "main.png", 0, 0);
		popupWindow.setSize(popupWindowSizeX, popupWindowSizeY);
		popupWindow.position = new Vector3(popupWindowPositionX, popupWindowPositionY, 2);
		
		// Кнопка изучения таланта
		float investButtonPositionX = Screen.width * 0.19f;
		float investButtonPositionY = -Screen.height * 0.891f;
		float investButtonSizeX = Screen.width * 0.2f;
		float investButtonSizeY = Screen.height * 0.0875f;
		
		investButton = UIButton.create(UIPrime31.myToolkit3, "investbutton.png", "investbutton.png", 0, 0);
		investButton.setSize(investButtonSizeX, investButtonSizeY);
		investButton.position = new Vector3(investButtonPositionX, investButtonPositionY, 1);
		investButton.hidden = false;
		investButton.info = index;
		investButton.onTouchDown += investButtonDelegate;
		
		// Кнопка отмены изучения таланта
		float cancelInvestButtonPositionX = Screen.width * 0.61f;
		float cancelInvestButtonPositionY = -Screen.height * 0.891f;
		float cancelInvestButtonSizeX = Screen.width * 0.2f;
		float cancelInvestButtonSizeY = Screen.height * 0.0875f;
		
		cancelInvestButton = UIButton2.create(UIPrime31.myToolkit3, "canceltbutton.png", "canceltbutton.png", 0, 0);
		cancelInvestButton.setSize(cancelInvestButtonSizeX, cancelInvestButtonSizeY);
		cancelInvestButton.position = new Vector3(cancelInvestButtonPositionX, cancelInvestButtonPositionY, 1);
		cancelInvestButton.hidden = false;
		cancelInvestButton.onTouchDown += cancelInvestDelegate;
		
		
		float ornamentPopupButtonPositionX = Screen.width * 0.45f;
		float ornamentPopupButtonPositionY = -Screen.height * 0.891f;
		float ornamentPopupButtonSizeX = Screen.width * 0.1f;
		float ornamentPopupButtonSizeY = Screen.height * 0.08f;
		
		ornamentPopup = UIButton.create(UIPrime31.myToolkit3, "central_points number.png", "central_points number.png", 0, 0);
		ornamentPopup.setSize(ornamentPopupButtonSizeX, ornamentPopupButtonSizeY);
		ornamentPopup.position = new Vector3(ornamentPopupButtonPositionX, ornamentPopupButtonPositionY, 1);
		ornamentPopup.hidden = false;
		
		
		float investSpellButtonPositionX = Screen.width * 0.087f;
		float investSpellButtonPositionY = -Screen.height * 0.3363f;
		float investSpellButtonSizeX = Screen.width * 0.072f;
		float investSpellButtonSizeY = Screen.height * 0.1f;
		
		UIToolkit tempUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+TraitsManager.investSpellIcon);
		investSpellButton = UIButton2.create(tempUIToolkit, TraitsManager.investSpellIcon, TraitsManager.investSpellIcon, 0, 0);
		investSpellButton.setSize(investSpellButtonSizeX, investSpellButtonSizeY);
		investSpellButton.position = new Vector3(investSpellButtonPositionX, investSpellButtonPositionY, 1);
		investSpellButton.hidden = false;
		
		
		float closePopupPositionX = Screen.width * 0.92f;
		float closePopupPositionY = -Screen.height * 0.269f;
		float closePopupSizeX = Screen.width * 0.05f;
		float closePopupSizeY = Screen.height * 0.075f;
		
		closeXPopup = UIButton.create(UIPrime31.myToolkit3, "close_button.png", "close_button.png", 0, 0);
		closeXPopup.setSize(closePopupSizeX, closePopupSizeY);
		closeXPopup.position = new Vector3(closePopupPositionX, closePopupPositionY, 1);
		closeXPopup.hidden = false;
		closeXPopup.onTouchDown += cancelInvestDelegate;		
		
		
		float rectWidth = Screen.width * 0.85f;
		float rectHeigth = Screen.height * 0.4f;
		Rect statsRect = new Rect(Screen.width * 0.074f, Screen.height * 0.475f, rectWidth, rectHeigth);

		TextZone.SetFontSizeByRatio(1.5f);
		TextZone.InitSkinInfo(WorldSession.player.newskin.GetStyle("DescriptionAndStats"));
		TextZone.CreateScrollZoneWithText(statsRect, TraitsManager.descriptionText, "#000000ff");
		
		txtSize = Screen.width / 960f * 0.5f;
		investRankText = text1.addTextInstance(TraitsManager.traitsRank, Screen.width * 0.5f, Screen.height * 0.9f, txtSize, -1,Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
		txtSize = Screen.width / 960f * 0.7f;
		spellNameText = text2.addTextInstance(TraitsManager.spellNameInfo, Screen.width * 0.20f, Screen.height * 0.33f, txtSize, -1, Color.white, UITextAlignMode.Left, UITextVerticalAlignMode.Top);
		
		traitsInfoWindow.addChild(popupWindow, investButton, investSpellButton, cancelInvestButton, ornamentPopup, closeXPopup);
		traitsInfoWindow.hidden = false;
	}	
	
	private void  cancelInvestDelegate (UIButton sender)
	{
		TextZone.RemoveScrollZone();
		SinglePictureUtils.DestroySingleUISprite(investSpellButton as UISprite);
		traitsInfoWindow.removeAllChild(true);
		
		popupWindow.destroy();
		investButton.destroy();
		cancelInvestButton.destroy();
		ornamentPopup.destroy();
		closeXPopup.destroy();
		
		investRankText.clear();
		spellNameText.clear();
		
		popupFlag = false;
	}
	
	private void  investButtonDelegate ( UIButton sender )
	{
		cancelInvestDelegate(sender);
		
		if(TraitsManager.canLearnTalent == string.Empty)
		{
			destroyTraitsManager();
			clearTraitsButtons();
			
			WorldPacket pkt;
			
			TalentButton selectedTalent = TraitsManager.traitsButtonInfo[sender.info];
			TraitsManager.needUpdateTraitWindow = true;
			
			if(TraitsManager.getActionTraitIndex() != 0)
			{
				pkt = new WorldPacket(OpCodes.CMSG_LEARN_TALENT, 8);
				pkt.Append(ByteBuffer.Reverse(selectedTalent.talentID));
				pkt.Append(ByteBuffer.Reverse(selectedTalent.rank));
				RealmSocket.outQueue.Add(pkt);
			}
			else
			{
				pkt = new WorldPacket(OpCodes.CMSG_PET_LEARN_TALENT, 16);
				pkt.Append(ByteBuffer.Reverse(WorldSession.player.petGuid));
				pkt.Append(ByteBuffer.Reverse(selectedTalent.talentID));
				pkt.Append(ByteBuffer.Reverse(selectedTalent.rank));
				RealmSocket.outQueue.Add(pkt);
			}
		}
		else
		{
			ErrorMessage.ShowError(TraitsManager.canLearnTalent);
		}		
	}
	
	public void  destroy ()
	{
		checkTraitsInfoWindow();
		
		if(traitsWindowContainer._children.Count > 0)
		{
			destroyTraitsManager();
			clearTraitsButtons();	
		}
	}
	
	private void  checkTraitsInfoWindow ()
	{
		if(traitsInfoWindow._children.Count > 0)
		{
			cancelInvestDelegate(null);
		}
	}
	
	/*
	void  UnlearnPetTalent ( System.UInt64 guid )
	{
		WorldPacket pkt = new WorldPacket (OpCodes.CMSG_PET_LEARN_TALENT, 8);
		pkt.Append(ByteBuffer.Reverse(guid));
		RealmSocket.outQueue.Add(pkt);
	}
	*/
}