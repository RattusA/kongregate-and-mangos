using UnityEngine;
using System.Collections;

public class Portrait: MonoBehaviour
{
	public RenderTexture SelfTexture;
	public RenderTexture TargetTexture;
	
	private Material SelfTextureMaterial;
	private Material TargetTextureMaterial;
	
	private Texture NPCTexture;
	private Texture stealthTexture;
	
	Texture DefaultTexture;
	
	public GameObject CameraPrefab;
	
	public MainPlayer MainPlayerRefference;
	
	public static bool  AreWeInMenu = false;
	private bool  isNPC;
	
	Transform selfCamera;
	Transform targetCamera;
	
	//rects
	private Rect selfRect;
	Rect selfRectClass;
	private Rect targetRect;
	private Rect targetRectClass;
	
	//target Transform refference
	private Transform Target;
	public int comboPoints;
	
	//if self target
	private bool  isNotSelf;
	
	
	//Object[] Textures;
	
	
	GameObject TargetClone;
	GameObject SelfClone;
	Texture comboPointsTexture;
	Texture CombatTexture;
	//================== timers ==================//
	
	int selfTimer;
	int targetTimer;
	
	float StartTimer;
	
	//=================class/faction textures==============//
	private Texture[] ClassTextures;
	//private Texture _targetFactionTexture;
	private Texture _targetClassTexture;
	//private Texture _selfFactionTexture;
	private Texture _selfClassTexture;
	
	//=========================== END OF VARIABLES AREA =============================//
	
	void  Start ()
	{
		selfRect = new Rect(Screen.width*0.0035f, Screen.width*0.0081f, Screen.width*0.0752f, Screen.width*0.0738f);
		selfRectClass = new Rect(/*Screen.width*0.0062f*/0, 0 , Screen.width*0.039f, Screen.width*0.0434f);
		targetRect = new Rect(Screen.width * 0.4572f, Screen.width*0.0081f, Screen.width*0.0752f, Screen.width* 0.07815f);
		targetRectClass = new Rect(Screen.width * 0.4427f, 0 , Screen.width * 0.039f, Screen.width * 0.0434f);
		comboPointsTexture = Resources.Load<Texture>("Portrait/dot");
		CombatTexture = Resources.Load<Texture>("Portrait/CombatIcon");
		ClassTextures = Resources.LoadAll<Texture>("Portrait/ClassIcons");
		SelfTextureMaterial = Resources.Load<Material>("prefabs/portrait/SelfTextureMaterial");
		TargetTextureMaterial = Resources.Load<Material>("prefabs/portrait/TargetTextureMaterial");
		stealthTexture = Resources.Load<Texture>("Portrait/Icons/stealth");
		
		//clearing RenderTextures
		SelfTexture.Release();
		TargetTexture.Release();
		
		
		InstantiateSelfCamera();
		InstantiateTargetCamera();
		
		selfTimer = 0;
		targetTimer = 0;
		StartTimer = 3;
		isNotSelf = false;
		
		//==============DO OTHER STUFF=============//
		//print("done instantiation");
		
		AreWeInMenu = false;
		isNPC = true;
		
		
		//====================================//
		//Textures = Resources.LoadAll("Portrait/Icons", Texture);
		DefaultTexture = Resources.Load<Texture>("Portrait/DefaultTexture");
	}
	
	void  Update ()
	{
		
		if(!selfCamera)
		{
			InstantiateSelfCamera();
			UpdateSelfSnapShot();
		}
		
		if(!targetCamera)
		{
			InstantiateTargetCamera();
			MainPlayerRefference.ReleaseTarget();
			UpdateTargetSnapShot(null);
		}
		
		
		FirstTimeSelfSnapShot();
		//=======
		//int level;
		//level = MainPlayerRefference.level;
		//print("level" + level.ToString());
		
		if(selfTimer > 0)
		{
			selfTimer --;
		}
		
		if(selfTimer == 0)
		{
			selfCamera.GetComponent<Camera>().gameObject.SetActive(false);
			Destroy(SelfClone);
			selfTimer --;
		}
		
		if(targetTimer > 0)
		{
			targetTimer --;
		}
		
		if(targetTimer == 0)
		{
			targetCamera.GetComponent<Camera>().gameObject.SetActive(false);
			Destroy(TargetClone);
			targetTimer --;
		}
	}
	
	void  OnGUI ()
	{	
		
		if(LoadSceneManager.LoadingStatus == LoadSceneManager.LOADING.LOADING_INPROGRESS)
		{
			return;
		}
		
		if(AreWeInMenu)
		{
			return;
		}
		//drawing self portrait
		if(Event.current.type == EventType.Repaint)
		{
			Graphics.DrawTexture(selfRect, SelfTexture, SelfTextureMaterial);
		}
		//drawing self class texture
		//	try{
		
		if(_selfClassTexture != null)
		{
			GUI.DrawTexture(selfRectClass, _selfClassTexture, ScaleMode.StretchToFill, true);
		}
		
		/*		}
	catch( Exception e )
	{
    Debug.Log("PORTRAIT EXCEPTION: " + e);
}*/
		
		//drawing faction texture
		
		//GUI.DrawTexture(selfRect, _selfFactionTexture, ScaleMode.StretchToFill, true);
		
		
		if (MainPlayerRefference.HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_IN_COMBAT))
		{
			GUI.DrawTexture(selfRect, CombatTexture, ScaleMode.StretchToFill);				
		}
		
		
		if(Target)
		{
			if(!isNPC)
			{
				if(isNotSelf)
				{	
					//drawing target player portrait	
					if(MainPlayerRefference.target != null && (MainPlayerRefference.target.stealth || MainPlayerRefference.target.shapeshift))
					{
						Graphics.DrawTexture(targetRect, stealthTexture, TargetTextureMaterial);
					}
					else if(Event.current.type == EventType.Repaint)
					{	
						Graphics.DrawTexture(targetRect, TargetTexture, TargetTextureMaterial);
					}
					//drawing target class texture
					if(_targetClassTexture)
					{
						GUI.DrawTexture(targetRectClass, _targetClassTexture, ScaleMode.ScaleToFit, true, 0);
					}
					//drawing target faction texture
					//GUI.DrawTexture(targetRect, _targetFactionTexture, ScaleMode.StretchToFill, true, 0);
				}
				else
				{	
					//drawing target player portrait
					if(MainPlayerRefference.target.stealth || MainPlayerRefference.target.shapeshift)
					{
						Graphics.DrawTexture(targetRect, SelfTexture, TargetTextureMaterial);
					}
					else if(Event.current.type == EventType.Repaint)
					{	
						Graphics.DrawTexture(targetRect, TargetTexture, TargetTextureMaterial);
					}
					//drawing target class texture
					GUI.DrawTexture(targetRectClass, _selfClassTexture, ScaleMode.ScaleToFit, true, 0);
					//drawing target faction texture
					//GUI.DrawTexture(targetRect, _selfFactionTexture, ScaleMode.StretchToFill, true, 0);				
				}
			}
			else
			{	
				if(NPCTexture)
					if(Event.current.type == EventType.Repaint)
				{
					Graphics.DrawTexture(targetRect, NPCTexture, TargetTextureMaterial);
				}
			}
		}
		if(comboPoints > 0)
		{
			//MonoBehaviour.print("classType: " + MainPlayerRefference.classtype);
			if(WorldSession.player.playerClass == Classes.CLASS_ROGUE)
			{
				for(int i = 0; i < comboPoints; i++)
				{
					GUI.DrawTexture( new Rect(Screen.width * 0.45f + (Screen.width / 64) * i + 1, Screen.width / 10 , Screen.width / 64, Screen.width / 64), comboPointsTexture);
				}
			}
			//else{
			//	MonoBehaviour.print("target dodged/parried a blow...");
			//}
		}
	}
	
	void  GetSelfFactionAndClass ()
	{
		//MonoBehaviour.print("SELF_CLASS: " + MainPlayerRefference._class);
		
		//setting the class texture refference for main player
		switch(MainPlayerRefference.playerClass)
		{
		case Classes.CLASS_WARRIOR:
			_selfClassTexture = ClassTextures[0];
			break;
		case Classes.CLASS_PRIEST:
			_selfClassTexture = ClassTextures[1];
			break;
		case Classes.CLASS_MAGE:
			_selfClassTexture = ClassTextures[2];
			break;
		case Classes.CLASS_ROGUE:
			_selfClassTexture = ClassTextures[3];
			break;
		case Classes.CLASS_PALADIN:
			_selfClassTexture = ClassTextures[4];
			break;
		case Classes.CLASS_WARLOCK:
			_selfClassTexture = ClassTextures[7];
			break;
		case Classes.CLASS_HUNTER:
			_selfClassTexture = ClassTextures[8];
			break;
		default:
			_selfClassTexture = ClassTextures[0];
			break;
		}
		
		//setting the faction texture refference for main player
		/*if(MainPlayerRefference.race == Races.RACE_HUMAN || MainPlayerRefference.race == Races.RACE_NIGHTELF || MainPlayerRefference.race == Races.RACE_DWARF){
		_selfFactionTexture = ClassTextures[5];	
	}
	else{
		_selfFactionTexture = ClassTextures[6];
	}	*/
	}
	
	void  GetTargetFactionAndClass ( Player _otherPlayerRefference )
	{
		//MonoBehaviour.print("CLASS: " + _otherPlayerRefference.classtype);
		
		switch(_otherPlayerRefference.playerClass)
		{
		case Classes.CLASS_WARRIOR:
			_targetClassTexture = ClassTextures[0];
			break;
		case Classes.CLASS_PRIEST:
			_targetClassTexture = ClassTextures[1];
			break;
		case Classes.CLASS_MAGE:
			_targetClassTexture = ClassTextures[2];
			break;
		case Classes.CLASS_ROGUE:
			_targetClassTexture = ClassTextures[3];
			break;
		case Classes.CLASS_PALADIN:
			_targetClassTexture = ClassTextures[4];
			break;
		case Classes.CLASS_WARLOCK:
			_targetClassTexture = ClassTextures[7];
			break;
		case Classes.CLASS_HUNTER:
			_targetClassTexture = ClassTextures[8];
			break;
		}
		
		//setting the faction texture refference for other player
		/*	if(_otherPlayerRefference.race == Races.RACE_HUMAN || _otherPlayerRefference.race == Races.RACE_NIGHTELF || _otherPlayerRefference.race == Races.RACE_DWARF){
		_targetFactionTexture = ClassTextures[5];	
	}
	else{
		_targetFactionTexture = ClassTextures[6];
	}	*/
	}
	
	void  FirstTimeSelfSnapShot ()
	{
		if(StartTimer == -100)
		{
			return;
		}
		else{
			if(StartTimer > 0)
			{
				StartTimer -= Time.deltaTime;
				//print("decreasing Timer...");
			}
			else
			{
				//	print("initializing selfSnapshot @ start...");
				GetSelfFactionAndClass();
				UpdateSelfSnapShot();
				StartTimer = -100;
			}	
		}
	}
	
	void  SetNPCTexture ( string name )
	{
		NPCTexture = null;
		NPCTexture = Resources.Load<Texture>("Portrait/Icons/" + name);
		
		if(NPCTexture == null)
		{
			NPCTexture = DefaultTexture;
		}
	}
	
	void  InstantiateSelfCamera ()
	{
		//for self camera instantiation
		selfCamera = (GameObject.Instantiate(CameraPrefab, new Vector3(0, -1000, 10), Quaternion.identity) as GameObject).transform;
		Camera camera = selfCamera.GetComponent<Camera>();
		camera.targetTexture = SelfTexture;
		selfCamera.eulerAngles += new Vector3(0, 180, 0);
		camera.gameObject.SetActive(false);
	}
	
	void  InstantiateTargetCamera ()
	{
		//for target camera instantiation
		targetCamera = (GameObject.Instantiate(CameraPrefab, new Vector3(0, -1100, 10), Quaternion.identity) as GameObject).transform;
		Camera camera = targetCamera.GetComponent<Camera>();
		camera.targetTexture = TargetTexture;
		targetCamera.eulerAngles += new Vector3(0, 180, 0);
		camera.gameObject.SetActive(false);
	}

	// unused
//	Transform LookAtHead ( Transform root )
//	{
//		Transform head = null;
//		
//		foreach(Transform go in root.GetComponentsInChildren<Transform>())
//		{
//			if(head)
//			{
//				continue;
//			}
//			
//			if(go.name == "Bip01 Head")
//			{
//				head = go;
//			}
//		}
//		
//		return head;
//	}
	
	void  InstantiateTargetPortraitObjects ( bool self ,    Player _player )
	{
		//print("instantiating gameobjects for portrait camera...");
		//	if(_player.stealth || _player.shapeshift)
		//	{
		//		if(self)
		//		{
		//			TargetTexture = SelfTexture;
		//			return;
		//		}
		//		else
		//		{
		//			//try and parse here from Texture2d to RenderTexture, doing it tomorow
		//			TargetTexture = Resources.Load("Portrait/Icons/stealth", RenderTexture) as RenderTexture;
		//		}
		//		Debug.Log("target is in stealth mode or shapeshifted " + _player.name);
		//	}
		TargetTexture.Release();
		//TargetTexture.DiscardContents();
		
		
		Destroy(TargetClone);
		
		//	Debug.Log("<size of collider stuff> size: " + Target.parent.collider.bounds.size + " center: " + Target.parent.collider.bounds.center);
		Animation animation;
		
		var collider= Target.parent.GetComponent<Collider>();
		if(self)
		{
			TargetClone = (GameObject)Object.Instantiate(gameObject, new Vector3(0, -1100, 0), Quaternion.identity);
			//set layer
			SetLayerAndUpdateOffScreen(TargetClone);
			
			Destroy(TargetClone.GetComponent<Portrait>());
			Destroy(TargetClone.GetComponent<MainPlayer>());
			animation = TargetClone.GetComponent<Animation>();
			animation.Play("idle");
			
			targetCamera.LookAt(TargetClone.transform.position + new Vector3(0, collider.bounds.size.y * 0.9f, 0));
			/* trolololo
		if(!MainPlayerRefference.mountManager.isMounted){
			targetCamera.LookAt(TargetClone.transform.position + new Vector3(0, collider.bounds.size.y * 0.9f, 0));
		}
		else{
			targetCamera.LookAt(LookAtHead(TargetClone.transform));
		}
		*/
		}
		
		else{
			TargetClone = (GameObject)GameObject.Instantiate(Target.gameObject, new Vector3(0, -1100, 0), Quaternion.identity);
			//set layer
			SetLayerAndUpdateOffScreen(TargetClone);
			
			TargetClone.GetComponent<OtherPlayer>().mountManager.currentMount = null;
			Destroy(TargetClone.GetComponent<OtherPlayer>());
			animation = TargetClone.GetComponent<Animation>();
			animation.Play("idle");
			
			targetCamera.LookAt(TargetClone.transform.position + new Vector3(0, collider.bounds.size.y * 0.9f, 0));
		}
		
		//print("done initializing gameobjects for portrait camera...");
	}
	
	public void  UpdateTargetSnapShot ( Transform target )
	{
		Target = target;
		if(!Target)
		{
			//Debug.Log("we have no target...");
			return;
		}

		MainPlayer _mainPlayer = Target.GetComponent<MainPlayer>();
		OtherPlayer _otherPlayer = Target.GetComponent<OtherPlayer>();
		
		Camera camera = targetCamera.GetComponent<Camera>();
		if(_mainPlayer)
		{
			camera.gameObject.SetActive(true);
			InstantiateTargetPortraitObjects(true, _mainPlayer as Player);
			isNotSelf = false;
			targetTimer = 10;
			isNPC = false;
		}
		else if(_otherPlayer)
		{		
			camera.gameObject.SetActive(true);
			InstantiateTargetPortraitObjects(false, _otherPlayer as Player);
			isNotSelf = true;
			GetTargetFactionAndClass(Target.gameObject.GetComponent<OtherPlayer>());
			targetTimer = 10;
			isNPC = false;
		}
		else
		{
			string targetName = Target.parent.name.ToString();
			SetNPCTexture(targetName.Substring(0,targetName.Length-7));
			isNPC = true;			
		}
	}
	
	public void  UpdateSelfSnapShot ()
	{
		//	print("updating target portrait snapshot...");
		if(MainPlayerRefference.stealth || MainPlayerRefference.shapeshift)
		{
			Debug.Log("we are shapeshifted or in stealth mode, so we do not update portrait");
			return;
		}
		SelfTexture.Release();
		//SelfTexture.DiscardContents();

		Destroy(SelfClone);
		
		SelfClone = Object.Instantiate(gameObject, new Vector3(0, -1000, 0), Quaternion.identity) as GameObject;
		//set layer
		SetLayerAndUpdateOffScreen(SelfClone);
		
		Destroy(SelfClone.GetComponent<Portrait>());
		Destroy(SelfClone.GetComponent<MainPlayer>());
		Animation animation = SelfClone.GetComponent<Animation>();
		animation.Play("idle");
		
		var collider= transform.parent.GetComponent<Collider>();
		selfCamera.LookAt(SelfClone.transform.position + new Vector3(0, collider.bounds.size.y * 0.9f, 0));
		//selfCamera.ortographicSize = collider.bounds.size.	
		
		Camera camera = selfCamera.GetComponent<Camera>();
		camera.gameObject.SetActive(true);
		
		selfTimer = 10;
	}
	
	void  SetLayerAndUpdateOffScreen ( GameObject target )
	{
		foreach(SkinnedMeshRenderer go in target.GetComponentsInChildren<SkinnedMeshRenderer>())
		{
			go.gameObject.layer = 31;
			go.updateWhenOffscreen = true;
		}
	}
}