﻿using UnityEngine;

static public class ErrorMessage
{	
	static private UIText buttonStyle;
	static private UIText textStyle;
	
	static private bool  errorMessageFlag;
	
	static private UIAbsoluteLayout errorMessageContainer;
	
	static private UIButton errorMessageBackground;
	static private UIButton errorMessageTextureRound;
	static private UIButton errorMessageTextureLine;
	
	static private UIButton closeButton;
	
	static private UITextInstance closeButtonText;
	static private UITextInstance errorText;
	
	static public void  ShowError ( string errorMessageText  )
	{
		if(errorMessageFlag == true)
		{
			return;
		}
		
		errorMessageFlag = true;
		errorMessageContainer = new UIAbsoluteLayout();

		int positionX = 0;
		int positionY = 0;
		float width = 0;
		float height = 0;
		
		positionX = (int)(Screen.width * 0.15f);
		positionY = (int)(Screen.height / 4f);
		width = Screen.width * 0.7f;
		height = Screen.height / 3f;
		
		errorMessageBackground = UIButton.create(UIPrime31.myToolkit4, "popup_background.png", "popup_background.png", positionX, positionY, -3);
		errorMessageBackground.setSize(width, height);
		
		positionX = (int)(errorMessageBackground.position.x + errorMessageBackground.width / 30f);
		positionY = (int)(-errorMessageBackground.position.y + errorMessageBackground.height / 5f);
		width = errorMessageBackground.height * 0.4f;
		height = errorMessageBackground.height * 0.4f;
		
		errorMessageTextureRound = UIButton.create(UIPrime31.myToolkit3, "decoration_02.png", "decoration_02.png", positionX, positionY, -4);
		errorMessageTextureRound.setSize(width, height);
		
		
		positionX = (int)(errorMessageTextureRound.position.x + errorMessageTextureRound.width * 0.8f);
		positionY = (int)(-errorMessageTextureRound.position.y + errorMessageTextureRound.height * 0.95f);
		width = errorMessageBackground.width * 0.8f;
		height = errorMessageBackground.height * 0.1f;
		
		errorMessageTextureLine = UIButton.create(UIPrime31.myToolkit3, "decoration_01.png", "decoration_01.png", positionX, positionY, -4);
		errorMessageTextureLine.setSize(width, height);
		
		
		positionX = (int)(errorMessageBackground.position.x + errorMessageBackground.width * 0.34f);
		positionY = (int)(-errorMessageBackground.position.y + errorMessageBackground.height * 0.68f);
		width = errorMessageBackground.width / 3f;
		height = errorMessageBackground.height / 5f;
		
		closeButton = UIButton.create(UIPrime31.myToolkit3, "3_selected.png", "3_selected.png", positionX, positionY, -4);
		closeButton.setSize(width, height); 
		closeButton.centerize();
		closeButton.onTouchDown += CloseDelegate;
		
		buttonStyle = new UIText(UIPrime31.myToolkit1, "fontiny dark", "fontiny dark.png");
		textStyle = new UIText(UIPrime31.myToolkit1, "fontiny simple","fontiny simple.png");
		
		positionX = (int)closeButton.position.x/* + closeButton.width / 2*/;
		positionY = (int)-closeButton.position.y/* + closeButton.height / 2*/;
		
		closeButtonText = buttonStyle.addTextInstance("Close", positionX, positionY, 0.4f, -6 ,Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
		positionX = (int)(errorMessageTextureRound.position.x + errorMessageBackground.width * 0.55f);
		positionY = (int)(-errorMessageTextureRound.position.y - errorMessageTextureRound.height / 4f);
		
		errorText = textStyle.addTextInstance(errorMessageText, positionX, positionY, 0.4f, -6, Color.white, UITextAlignMode.Center, UITextVerticalAlignMode.Top);
		
		errorMessageContainer.addChild(errorMessageBackground, errorMessageTextureRound, errorMessageTextureLine, closeButton);
	}
	
	static private void  CloseDelegate (UIButton sender)
	{
		errorMessageContainer.removeAllChild(true);
		
		errorMessageBackground.destroy();
		errorMessageTextureRound.destroy();
		errorMessageTextureLine.destroy();
		
		closeButton.destroy();
		
		closeButtonText.clear();
		errorText.clear();
		
		buttonStyle = null;
		textStyle = null;
		errorMessageFlag = false;
	}
}