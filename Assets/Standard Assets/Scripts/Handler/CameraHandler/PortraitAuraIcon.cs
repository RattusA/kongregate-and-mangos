﻿using UnityEngine;

public class PortraitAuraIcon
{
	public int index;
	public Texture2D image;
	public Rect position;
	
	public PortraitAuraIcon ( int newIndex ,   Texture2D newIname ,   Rect newPosition )
	{
		index = newIndex;
		image = newIname;
		position = newPosition;
	}
}