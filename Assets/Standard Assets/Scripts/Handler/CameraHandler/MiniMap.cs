using UnityEngine;

public class MiniMap : MonoBehaviour
{
	public float Size = 0;
	public float MinSize = 20;
	public float MaxSize = 50;
	//big map stuff
	public float MAXSIZECAMERA = 200;
	public float MINSIZECAMERA = 30;
	
	public RenderTexture MinimapRenderTexture;
	public Material MinimapRenderMaterial;
	
	//private vars
	private float _minimapZoomValue = 10;
	private Texture _image;
	private Texture _minus;
	private Texture _plus;
	private Texture _close;
	private Texture _map;
	private Texture _xButton;
	private Transform _arrowPlane;
	private MainPlayer _player;
	
	//rects
	private Rect _closeRect;// = new Rect(Screen.width - _size, 0, _size, _size);
	private Rect _plusRect;
	private Rect _minusRect;
	private Rect _mapRect;
	private Rect _clockRect;
	private Rect _bigClockRect;
	private Rect _cameraRect;
	private Rect _imageRect;
	
	//public Vector3 BigMapRotation = Vector3.zero;
	private bool  _bigClockEnabled = false;
	private bool  _bigMapEnabled = false;
	private Camera _bigMapCamera = null;
	private GameObject _mapPlane;
	//private Vector3 _previousMapRotation = Vector3.zero;
	private Vector3 _tempMousePosition = Vector3.zero;
	private float _difference = 0;
	private int _direction = 0;
	private GameObject UIgo;
	private UISprite bigClockBackGround;
	private UISprite bigClockDeco; 
	
	private bool  revertFogState;
	
	void  Awake ()
	{
		ButtonOLD._UI = GameObject.Find("UI");
		//loading all the gui resources + renderTexture for minimap, etc
		LoadMapGUIResources();
		MinimapRenderTexture.DiscardContents();
		//doin the rest of the init
		Camera camera = GetComponent<Camera>();
		camera.targetTexture = MinimapRenderTexture;
		//camera.orthographicSize = MinSize;
//		camera.enabled = false;
	}
	void  Start ()
	{
		_arrowPlane = transform.Find("arrow");
		_mapPlane = GameObject.Find("MinimapTexture");
		
		Size = 220 * Screen.height / 600; //600 was the height i made the minimap, so it's relative to that screen size i guess
		//setting up of rects
		float _size = 17 * Size / 100;
		_closeRect = new Rect(Screen.width - _size*1.4f, 0, _size*1.4f, _size*1.4f);
		_plusRect = new Rect(Screen.width - Size*0.8f, Size - _size*1.8f, _size, _size);
		_minusRect = new Rect(Screen.width - _size*1.2f, Size - _size*1.8f, _size, _size);
		_mapRect = new Rect(Screen.width - Size*0.8f, 0, _size*1.2f, _size*1.2f);
		_imageRect = new Rect(Screen.width*0.8033f, 0, Screen.width*0.199f, Screen.width*0.199f);
		_cameraRect = new Rect(Screen.width*0.8277f, Screen.width*0.0234f, Screen.width*0.1645f, Screen.width*0.1528f);
		_clockRect = new Rect(Screen.width*0.8413f, Screen.width*0.1838f, Screen.width*0.0652f, Screen.width*0.0342f);
		_bigClockRect = new Rect(Screen.width*0.5214f, Screen.width*0.1747f, Screen.width*0.259f, Screen.width*0.1576f);
		
		bigClockBackGround = UIPrime31.myToolkit4.addSprite("popup_background.png", (int)_bigClockRect.x, (int)_bigClockRect.y);
		bigClockBackGround.setSize(_bigClockRect.width,_bigClockRect.height);
		bigClockBackGround.hidden=true;
		
		bigClockDeco = UIPrime31.myToolkit4.addSprite("upper_deco.png",
		                                              (int)(_bigClockRect.x+_bigClockRect.width*0.03f),
		                                              (int)(_bigClockRect.y+_bigClockRect.height*0.485f));
		bigClockDeco.setSize(_bigClockRect.width*0.9f, _bigClockRect.height*0.1f);
		bigClockDeco.hidden=true;
		
		
		
		
		
		MaxSize = Mathf.Abs(_mapPlane.transform.localScale.x * _mapPlane.transform.parent.localScale.x);
		MinSize = MaxSize/3;
		Camera camera = GetComponent<Camera>();
		camera.orthographicSize = MinSize;
		_arrowPlane.localScale = new Vector3(camera.orthographicSize/30, camera.orthographicSize/30 , camera.orthographicSize/30);
		_minimapZoomValue = MinSize/5;
		
		//Enabled = main player refference bla bla
//		_player = WorldSession.player;
//		camera.enabled = false;
		
		//for big map, texture and a rect to draw on screen, part of that big texture
		//previousMapRotation = _mapPlane.transform.eulerAngles;
		MAXSIZECAMERA = Mathf.Abs(_mapPlane.transform.localScale.x * 3 * _mapPlane.transform.parent.localScale.x);
		MINSIZECAMERA = Mathf.Abs(_mapPlane.transform.localScale.x * _mapPlane.transform.parent.localScale.x);
		UIgo = ButtonOLD._UI;
	}
	
	
	void  Update ()
	{
		CenterMinimapOnPlayer();
	}
	
	void  OnGUI ()
	{
		if(_player != null)
		{
			if(LoadSceneManager.LoadingStatus == LoadSceneManager.LOADING.LOADING_INPROGRESS)
			{
				if( _player.actionBarPanel.rightBotFlag)
				{
					_player.actionBarPanel.HideTopRightPanel();
					UnHideMiniMap();
				}
				return;
			}
			
			if(ButtonOLD.menuState)
			{
				return;
			}
			
			if(_bigMapEnabled)
			{
				if(_bigClockEnabled)
				{
					_bigClockEnabled=false;
					bigClockBackGround.hidden=true;
					bigClockDeco.hidden=true;
				}
				ShowBigMap();
			}
			else
			{
				if(!WorldSession.interfaceManager.hideInterface)
				{
					DrawMinimapOnScreen();
				}
			}
		}
	}
	
	void  LoadMapGUIResources ()
	{
		_image = Resources.Load<Texture>("GUI/Maps/busola");
		_minus = Resources.Load<Texture>("GUI/Maps/minus");
		_plus = Resources.Load<Texture>("GUI/Maps/plus");
		_close = Resources.Load<Texture>("GUI/Maps/close");
		_map = Resources.Load<Texture>("GUI/Maps/map_circle_menu");
		_xButton = Resources.Load<Texture>("GUI/Maps/close_button");
		MinimapRenderTexture = Resources.Load<RenderTexture>("prefabs/MiniMap/MinimapRenderTexture");
		MinimapRenderMaterial = Resources.Load<Material>("prefabs/MiniMap/MinimapRenderMaterial");
	}
	
	void  HideMiniMapDelegate ()
	{
		Camera camera = GetComponent<Camera>();
		camera.enabled = !camera.enabled;
		_player.ShowMinimap = !_player.ShowMinimap;
		_player.compassMiniMap.SetActive(true);
	}
	
	public void  HideMiniMap ()
	{
		Camera camera = GetComponent<Camera>();
		camera.enabled = false;
		_player.ShowMinimap = false;
		_player.compassMiniMap.SetActive(true);
	}
	
	void  UnHideMiniMap ()
	{
		Camera camera = GetComponent<Camera>();
		camera.enabled = true;
		_player.ShowMinimap = true;
		_player.compassMiniMap.SetActive(false);
	}
	
	void  DrawMinimapOnScreen ()
	{
		if(_player.ShowMinimap)
		{	
			//drawing of texture
			if(Event.current.type == EventType.Repaint){
				Graphics.DrawTexture(_cameraRect, MinimapRenderTexture, MinimapRenderMaterial);
			}
			
			DrawMiniMapHelper();
			
			//the drawing of overlay textures
			GUI.DrawTexture(_imageRect, _image, ScaleMode.ScaleToFit);
			
			//zoom in / zoom out functionality buttons
			
			Camera camera = GetComponent<Camera>();
			GUI.DrawTexture(_plusRect, _minus, ScaleMode.ScaleToFit);
			if(GUI.Button(_plusRect, "", new GUIStyle()) && camera.orthographicSize < MaxSize)
			{
				Debug.Log("X = "  + _arrowPlane.localScale.x.ToString() + "Y = " + _arrowPlane.localScale.y.ToString() + "Z = "  + _arrowPlane.localScale.z.ToString());
				camera.orthographicSize += _minimapZoomValue;
				_arrowPlane.localScale = new Vector3(camera.orthographicSize/30, camera.orthographicSize/30 , camera.orthographicSize/30);
			}
			
			GUI.DrawTexture(_minusRect, _plus, ScaleMode.ScaleToFit);
			if(GUI.Button(_minusRect, "", new GUIStyle()) && camera.orthographicSize > MinSize)
			{
				Debug.Log("X = "  + _arrowPlane.localScale.x.ToString() + "Y = " + _arrowPlane.localScale.y.ToString() + "Z = "  + _arrowPlane.localScale.z.ToString());
				camera.orthographicSize -= _minimapZoomValue;
				_arrowPlane.localScale = new Vector3(camera.orthographicSize/30, camera.orthographicSize/30 , camera.orthographicSize/30);
			}
			
			if(GUI.Button( new Rect(_cameraRect.x+_cameraRect.width*0.25f, _cameraRect.y+_cameraRect.height*0.25f, _cameraRect.width*0.5f,_cameraRect.height*0.5f), "", new GUIStyle()) && _player.canShowMenu)
			{
				WorldSession.interfaceManager.hideInterface = true;
				InstantiateBigMap(!_bigMapEnabled);
			}
			
			GUI.DrawTexture(_mapRect, _close, ScaleMode.ScaleToFit);
			if(GUI.Button(_mapRect, "", new GUIStyle()))
			{
				//HideMiniMapDelegate();
				HideMiniMap();
			}
			
			GUI.DrawTexture(_closeRect, _map, ScaleMode.ScaleToFit);
			if(GUI.Button(_closeRect, "", new GUIStyle()))
			{
				bigClockBackGround.hidden=true;
				bigClockDeco.hidden=true;
				_bigClockEnabled=false;
				_player.interf.startMenu(null);
			}
			
			if(GUI.Button(_clockRect, _player.serverTime, _player.newskin.GetStyle("newChat (34)")))
			{
				_bigClockEnabled =! _bigClockEnabled;
			}
			
			if(_bigClockEnabled)
			{			
				GUI.Label( new Rect(_bigClockRect.x+_bigClockRect.width*0.05f, _bigClockRect.y+_bigClockRect.height*0.08f, _bigClockRect.width,_bigClockRect.height*0.21f),"Local Time:",_player.newskin.label);
				GUI.Label( new Rect(_bigClockRect.x+_bigClockRect.width*0.4f, _bigClockRect.y+_bigClockRect.height*0.28f, _bigClockRect.width,_bigClockRect.height*0.21f),_player.localTime,_player.newskin.label);
				GUI.Label( new Rect(_bigClockRect.x+_bigClockRect.width*0.05f, _bigClockRect.y+_bigClockRect.height*0.58f, _bigClockRect.width,_bigClockRect.height*0.21f),"Server Time:",_player.newskin.label);
				GUI.Label( new Rect(_bigClockRect.x+_bigClockRect.width*0.4f, _bigClockRect.y+_bigClockRect.height*0.78f, _bigClockRect.width,_bigClockRect.height*0.21f),_player.serverTime,_player.newskin.label);
				
				if(GUI.Button( new Rect(_bigClockRect.x+_bigClockRect.width*0.95f, _bigClockRect.y, 34 * Screen.height / 600*1.2f, 34 * Screen.height / 600*1.2f), _xButton, new GUIStyle()))
				{
					_bigClockEnabled = false;
				}
			}
			//if(GUI.Button(_bigClockRect,"",_player.newskin.GetStyle("newChat (34)")))
			//{}
			if(_bigClockEnabled == false)
			{
				bigClockBackGround.hidden=true;
				bigClockDeco.hidden=true;
			}
			if(_bigClockEnabled == true)
			{
				bigClockBackGround.hidden=false;
				bigClockDeco.hidden=false;
			} 
			
		}
		else
		{
			GUI.DrawTexture(_closeRect, _close, ScaleMode.ScaleToFit);
			if(GUI.Button(_closeRect, "", new GUIStyle()))
			{
				UnHideMiniMap();
				_player.actionBarPanel.HideTopRightPanel();
				//			//camera.enabled = !camera.enabled;
				//			//_player.ShowMinimap = !_player.ShowMinimap;
				//			if((_player.spellManagerStatus&8)==8)
				//			{
				//				if((_player.spellManagerStatus&4)==4)
				//				{
				//					_player.spellManagerStatus -= 4;
				//				}
				//			}
				//			_player.DrawActionBars();
			}
		}
		
	}
	
	void  CenterMinimapOnPlayer ()
	{
		if(!_player)
		{
			Debug.Log("no player refference...");
			_player = WorldSession.player;
		}
		else
		{
			if(!_player.ShowMinimap)
			{
				return;
			}	
			//moving the camera for minimap where the player is and rotating the arrow
			//try{
			transform.position = _player.transform.parent.position + new Vector3(0, 500, 0);
			_arrowPlane.eulerAngles = new Vector3(0, _player.transform.parent.eulerAngles.y, 180);
			//	}
			//	catch( Exception exception )
			//	{
			//	    Debug.Log("Exception: " + exception);
			//	}
		}
	}
	
	void  InstantiateBigMap ( bool val )
	{
		_bigMapEnabled = ! _bigMapEnabled;
		//Debug.Log("inst cam: " + val);
		//_arrowPlane.gameObject.SetActive(!val);
		if(val)
		{
			//Debug.Log("instantiate camera..");
			GameObject obj = new GameObject("BigMapCamera");
			_bigMapCamera = obj.AddComponent<Camera>(); //GameObject.Instantiate(Resources.Load<Camera>("prefabs/MiniMap/BigMapCamera"));
			_bigMapCamera.transform.eulerAngles = new Vector3(90.0f, 0, 0);
			_bigMapCamera.transform.position = new Vector3(_player.TransformParent.position.x, 500, _player.TransformParent.position.z);
			_bigMapCamera.orthographic = true;
			_bigMapCamera.orthographicSize = MINSIZECAMERA;
			_bigMapCamera.clearFlags = CameraClearFlags.SolidColor;
			_bigMapCamera.cullingMask = 1 << LayerMask.NameToLayer("MiniMap");
			_bigMapCamera.backgroundColor = new Color(100.0f/256.0f, 100.0f/256.0f, 100.0f/256.0f, 0.0f);

			//deactivating shit
			Camera.main.gameObject.GetComponent<Swipe>().enabled = false;
			UIgo.SetActive(false);
			_player.PortraitReff.enabled = false;
			_player.meniuOpend = true;
		}
		else
		{
			//Debug.Log("trying to destroy camera....");
			//_mapPlane.transform.eulerAngles = _previousMapRotation;
			//we re-enable shit
			Camera.main.gameObject.GetComponent<Swipe>().enabled = true;
			UIgo.SetActive(true);
			_player.PortraitReff.enabled = true;
			_player.meniuOpend = false;
			
			if(_bigMapCamera){
				//Debug.Log("we DESTROY!!!");
				GameObject.Destroy(_bigMapCamera.gameObject);
			}
		}
		RenderSettings.fog = !_bigMapEnabled;
	}
	
	void  ShowBigMap ()
	{
		//Debug.Log("showing big map...");
		GUI.DrawTexture(_closeRect, _close, ScaleMode.StretchToFill);
		if(GUI.Button(_closeRect, "", new GUIStyle()))
		{
			WorldSession.interfaceManager.hideInterface = false;
			InstantiateBigMap(!_bigMapEnabled);
		}
		
		DrawMapPOIHelper();
		DrawDeadCorpsMarker();
		
		//for win and mouse input coord
		#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR
		//moving of map
		if(Input.GetMouseButtonDown(0))
	{
			_tempMousePosition = Input.mousePosition;
		}
		if(Input.GetMouseButton(0))
	{
			MoveBigMap(Input.mousePosition - _tempMousePosition);
			_tempMousePosition = Input.mousePosition;
		}
		//zooming in-out of map
		if(Event.current.type == EventType.ScrollWheel){
			//Debug.Log("se da de scroll wheel... " + Event.current.delta);
			Zoom(Event.current.delta.y);
		}
		#elif UNITY_ANDROID || UNITY_IPHONE
		//Debug.Log("IPHONE!!!");
		if(Input.touchCount == 1){
			MoveBigMap(Input.touches[0].deltaPosition);
		}
		if(Input.touchCount == 2){
			if(Input.touches[1].phase == TouchPhase.Began){
				_difference = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
			}
			Zoom(_difference - Vector2.Distance(Input.touches[0].position, Input.touches[1].position));
			_difference = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);        
		}
		#endif
	}
	
	void  MoveBigMap ( Vector2 _deltaPosition )
	{
		if(_deltaPosition.x != 0){
			MoveX(-_deltaPosition.x, false);
		}
		if(_deltaPosition.y != 0){
			MoveY(-_deltaPosition.y, false);
		}
	}
	
	void  MoveX ( float val ,   bool zoom )
	{
		//Debug.Log("x val: " + val);
		//raycasting
		if(!zoom){
			val *= 1;//(_bigMapCamera.orthographicSize / MAXSIZECAMERA) * Screen.height / Screen.width;
		}
		Vector3 _distance = (val < 0)? new Vector3(-(_bigMapCamera.orthographicSize * Screen.width/Screen.height - val), 0, 0) : 
			new Vector3(_bigMapCamera.orthographicSize * Screen.width/Screen.height + val, 0, 0);
		//Debug.Log("distance: " + _distance);
		//Debug.DrawRay(_bigMapCamera.transform.position + _distance, -Vector3.up, Color.green, 10000);
		if(Physics.Raycast(_bigMapCamera.transform.position + _distance, -Vector3.up, 1000, 1 << 17))
	{
			_bigMapCamera.transform.position += new Vector3(val, 0, 0);
			//Debug.Log("we move the camera on X with: " + val);
		}
	}
	
	void  MoveY ( float val ,   bool zoom )
	{
		//Debug.Log("y val: " + val);
		//raycasting
		if(!zoom){
			val *= Screen.width / Screen.height;//(_bigMapCamera.orthographicSize / MAXSIZECAMERA) * Screen.height / Screen.width;
		}
		Vector3 _distance = (val < 0)? new Vector3(0, 0, -(_bigMapCamera.orthographicSize - val)) : 
			new Vector3(0, 0, _bigMapCamera.orthographicSize + val);
		//Debug.Log("distance: " + _distance);
		//Debug.DrawRay(_bigMapCamera.transform.position + _distance, -Vector3.up, Color.green, 10000);
		if(Physics.Raycast(_bigMapCamera.transform.position + _distance, -Vector3.up, 1000, 1 << 17))
	{
			_bigMapCamera.transform.position += new Vector3(0, 0, val);
			//Debug.Log("we move the camera on Y with: " + val);
		}
	}
	
	void  Zoom ( float val )
	{
		//calculate the value of the increase/decrease of the camera size (zoom in or out)
		if(_bigMapCamera.orthographicSize + val > MAXSIZECAMERA){
			val = MAXSIZECAMERA - _bigMapCamera.orthographicSize;
		}
		if(_bigMapCamera.orthographicSize + val < MINSIZECAMERA){
			val = MINSIZECAMERA - _bigMapCamera.orthographicSize;
		}
		//move the camera if we are to zoom out too much
		_direction = 0;
		if(val > 0){
			//Debug.Log("we zoom out...");
			if(!Physics.Raycast(_bigMapCamera.transform.position - new Vector3(-(_bigMapCamera.orthographicSize + val)* Screen.width/Screen.height,0 , -(_bigMapCamera.orthographicSize + val)), -Vector3.up, 1000, 1 << 17))
	{
				_direction = 1;
			}
			if(!Physics.Raycast(_bigMapCamera.transform.position - new Vector3(-(_bigMapCamera.orthographicSize + val)* Screen.width/Screen.height,0 , _bigMapCamera.orthographicSize + val), -Vector3.up, 1000, 1 << 17))
	{
				_direction = (_direction == 1)? 5 : 2;
			}
			if(!Physics.Raycast(_bigMapCamera.transform.position - new Vector3((_bigMapCamera.orthographicSize + val)* Screen.width/Screen.height,0 , _bigMapCamera.orthographicSize + val), -Vector3.up, 1000, 1 << 17))
	{
				_direction = (_direction == 2)? 6 : (_direction == 5)? 2 : (_direction == 1)? 4 : 3;
			}
			if(!Physics.Raycast(_bigMapCamera.transform.position - new Vector3((_bigMapCamera.orthographicSize + val)* Screen.width/Screen.height,0 , -(_bigMapCamera.orthographicSize + val)), -Vector3.up, 1000, 1 << 17))
	{
				_direction = (_direction == 3)? 7 : (_direction == 1)? 8 : (_direction == 5)? 1 : (_direction == 6)? 3 : (_direction == 2)? 0 : 4;
			}
			//Debug.Log("ZOOOM: " + _direction + "\n");
			
			switch(_direction){
			case 1:
				MoveX(-val * Screen.width/Screen.height, true);
				MoveY(-val, true);
				break;
				
			case 2:
				MoveX(-val * Screen.width/Screen.height, true);
				MoveY(val, true);
				break;
				
			case 3:
				MoveX(val * Screen.width/Screen.height, true);
				MoveY(val, true);
				break;
				
			case 4:
				MoveX(val * Screen.width/Screen.height, true);
				MoveY(-val, true);
				break;
				
			case 5:
				MoveX(-val * Screen.width/Screen.height, true);
				//MoveY(val);
				break;
				
			case 6:
				//MoveX(val * Screen.width/Screen.height);
				MoveY(val, true);
				break;
				
			case 7:
				MoveX(val * Screen.width/Screen.height, true);
				//MoveY(val);
				break;
				
			case 8:
				//MoveX(val * Screen.width/Screen.height);
				MoveY(-val, true);
				break;
				
			default:
				//    			Debug.Log("we won't move the camera... we are centered.");
				break;
			}
		}
		_bigMapCamera.orthographicSize += val;
	}
	
	void  DrawMiniMapHelper ()
	{
		int helperSize = 18;
		MainPlayer player = _player;
		Camera miniMapCam = gameObject.GetComponentInChildren<Camera>();
		Vector3 viewPos;
		for(int i= 0; i < player.questGiverList.Count; i++)
		{
			viewPos = miniMapCam.WorldToViewportPoint(player.questGiverList[i].transform.position);
			player.questGiverList[i].helperStyle.fontSize = helperSize;
			GUI.Label(new Rect(Screen.width*(0.8277f + 0.1645f*viewPos.x) - helperSize/2, Screen.width*(0.0234f + 0.1528f*(1 - viewPos.y)) - helperSize/2, helperSize, helperSize), 
			          player.questGiverList[i].helperText, 
			          player.questGiverList[i].helperStyle);
		}
	}
	
	//----------------
	//-1 - talk with NPC (yellow "?")
	//4 - collect items (yellow "O")
	//3 - kill mobs (red "O")
	//5 - kill mobs and collect loot (blue "O")
	//6 - find and escort NPC (white "O")
	void  DrawMapPOIHelper ()
	{
		if(!WorldSession.player)
			return;
		QuestManager questManager = WorldSession.player.questManager;
		int helperSize = 18;
		int helperCircleSize = 36;
		Camera mapCam = GameObject.Find("BigMapCamera").GetComponentInChildren<Camera>();
		//Debug.Log("QuestPoiManager.poiPointList.count = "+QuestPoiManager.poiPointList.Count);
		Vector3 viewPos;
		
		//---Style for number
		GUIStyle numberStyle = new GUIStyle();
		numberStyle.alignment = TextAnchor.MiddleCenter;
		numberStyle.fontStyle = FontStyle.Bold;
		numberStyle.normal.textColor = Color.black;
		numberStyle.fontSize = 16;
		
		GUIStyle helperStyle = new GUIStyle();
		helperStyle.alignment = TextAnchor.MiddleCenter;
		helperStyle.fontStyle = FontStyle.Bold;
		helperStyle.normal.textColor = Color.yellow;
		string helperText = "";
		
		for(byte questIndex = 0; questIndex < questManager.getQuestCount(); questIndex++)
		{
			uint poiQuestId = questManager.getQuestBySlotId(questIndex);
			PoiPointManager questPoi = QuestPoiManager.getQuestPoiPoints(poiQuestId);
			foreach(uint poiPointId in questPoi.poiStages.Keys)
			{
				QuestStage poiStage = questPoi.getQuestStage(poiPointId);
				switch(poiStage.objectiveIndex)
				{
				case -1:
					helperStyle.normal.textColor = Color.yellow;
					helperStyle.fontSize = helperSize;
					helperText = "?";
					break;
				case 4:
					helperStyle.normal.textColor = Color.yellow;
					helperStyle.fontSize = helperCircleSize;
					helperText = "O";
					break;
				case 3:
					helperStyle.normal.textColor = Color.red;
					helperStyle.fontSize = helperCircleSize;
					helperText = "O";
					break;
				case 5:
					helperStyle.normal.textColor = Color.blue;
					helperStyle.fontSize = helperCircleSize;
					helperText = "O";
					break;
				case 6:
					helperStyle.normal.textColor = Color.white ;
					helperStyle.fontSize = helperCircleSize;
					helperText = "O";
					break;
				default:
					break;
				}
				for(int k= 0; k < poiStage.poiPositionsList.Count; k++)
				{
					viewPos = mapCam.WorldToViewportPoint(poiStage.poiPositionsList[k]);
					//Debug.Log("x = "+viewPos.x+" y = "+viewPos.y+" z = "+viewPos.z);
					GUI.Label(new Rect(Screen.width*viewPos.x - helperStyle.fontSize/2, Screen.height*(1-viewPos.y) - helperStyle.fontSize/2, helperStyle.fontSize, helperStyle.fontSize), 
					          helperText, helperStyle);
					if(poiStage.objectiveIndex != -1)
					{
						GUI.Label(new Rect(Screen.width*viewPos.x - helperCircleSize/2, Screen.height*(1-viewPos.y) - helperSize/2, helperCircleSize, helperSize), 
						          (questIndex+1).ToString(), numberStyle);
					}
				}
			}
		}
		
	}
	
	void  DrawDeadCorpsMarker ()
	{
		MainPlayer player = _player;
		if(player.m_deathState == DeathState.ALIVE)
		{
			return;
		}
		int helperSize = 30;
		Camera mapCam = GameObject.Find("BigMapCamera").GetComponentInChildren<Camera>();
		Vector3 viewPos;
		GUIStyle corpsMarkerStyle = new GUIStyle();
		corpsMarkerStyle.alignment = TextAnchor.MiddleCenter;
		corpsMarkerStyle.fontStyle = FontStyle.Bold;
		corpsMarkerStyle.normal.textColor = Color.red;
		corpsMarkerStyle.fontSize = helperSize;
		viewPos = mapCam.WorldToViewportPoint(player.lastDeadPosition);
		GUI.Label(new Rect(Screen.width*viewPos.x - helperSize/2, Screen.height*(1-viewPos.y) - helperSize/2, helperSize, helperSize), "X", corpsMarkerStyle);
	}
	
	void  OnPreRender ()
	{
		revertFogState = RenderSettings.fog;
		RenderSettings.fog = false;
	}
	
	void  OnPostRender ()
	{
		RenderSettings.fog = revertFogState;
	}
	
}