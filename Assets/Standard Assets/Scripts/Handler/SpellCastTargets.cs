﻿using UnityEngine;
using System.Collections;

// SpellEntry::Targets
public enum SpellCastTargetFlags
{
	TARGET_FLAG_SELF            = 0x00000000,
	TARGET_FLAG_UNUSED1         = 0x00000001,               // not used in any spells as of 3.0.3 (can be set dynamically)
	TARGET_FLAG_UNIT            = 0x00000002,               // pguid
	TARGET_FLAG_UNUSED2         = 0x00000004,               // not used in any spells as of 3.0.3 (can be set dynamically)
	TARGET_FLAG_UNUSED3         = 0x00000008,               // not used in any spells as of 3.0.3 (can be set dynamically)
	TARGET_FLAG_ITEM            = 0x00000010,               // pguid
	TARGET_FLAG_SOURCE_LOCATION = 0x00000020,               // pguid + 3 float
	TARGET_FLAG_DEST_LOCATION   = 0x00000040,               // pguid + 3 float
	TARGET_FLAG_OBJECT_UNK      = 0x00000080,               // used in 7 spells only
	TARGET_FLAG_UNIT_UNK        = 0x00000100,               // looks like self target (480 spells)
	TARGET_FLAG_PVP_CORPSE      = 0x00000200,               // pguid
	TARGET_FLAG_UNIT_CORPSE     = 0x00000400,               // 10 spells (gathering professions)
	TARGET_FLAG_OBJECT          = 0x00000800,               // pguid, 2 spells
	TARGET_FLAG_TRADE_ITEM      = 0x00001000,               // pguid, 0 spells
	TARGET_FLAG_STRING          = 0x00002000,               // string, 0 spells
	TARGET_FLAG_UNK1            = 0x00004000,               // 199 spells, opening object/lock
	TARGET_FLAG_CORPSE          = 0x00008000,               // pguid, resurrection spells
	TARGET_FLAG_UNK2            = 0x00010000,               // pguid, not used in any spells as of 3.0.3 (can be set dynamically)
	TARGET_FLAG_GLYPH           = 0x00020000,               // used in glyph spells
	TARGET_FLAG_UNK3            = 0x00040000,               //
	TARGET_FLAG_UNK4            = 0x00080000                // uint32, loop { vec3, guid -> if guid == 0 break }
};


public class SpellCastTargets {
	
	public _GameObject itemTarget;
	public _GameObject GoTarget;
	
	public ulong unitTargetGuid;
	public ulong GoTargetGuid;
	public ulong CorpseTargetGuid;
	public ulong itemTargetGuid;
	public ulong srcTransportGuid;
	public ulong destTransportGuid;
	
	public uint itemTargetEntry;
	
	
	public float srcX;
	public float srcY;
	public float srcZ;
	public Vector3 srcCoordinats;
	
	public float destX;
	public float destY;
	public float destZ;
	public Vector3 dstCoordinats;
	
	public string strTarget;
	public uint targetMask;
	
	public uint intTargetFlags;
	public uint selfFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_SELF;
	public uint unitFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_UNIT;	// pguid
	public uint itemFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_ITEM;	// pguid
	public uint srcFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_SOURCE_LOCATION;	// pguid + 3 float
	public uint dstFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_DEST_LOCATION;	// pguid + 3 float
	public uint objUnkFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_OBJECT_UNK;	// used in 7 spells only
	public uint unitUnkFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_UNIT_UNK;	// looks like self target (480 spells)
	public uint pvpCorpseFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_PVP_CORPSE;	// pguid
	public uint unitCorpseFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_UNIT_CORPSE;	// 10 spells (gathering professions)
	public uint objFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_OBJECT;	// pguid, 2 spells
	public uint tradeItemFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_TRADE_ITEM;	// pguid, 0 spells
	public uint stringFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_STRING;	// string, 0 spells
	public uint unk1Flag = (uint)SpellCastTargetFlags.TARGET_FLAG_UNK1;	// 199 spells, opening object/lock
	public uint corpseFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_CORPSE;	// pguid, resurrection spells
	public uint unk2Flag = (uint)SpellCastTargetFlags.TARGET_FLAG_UNK2;	// pguid, not used in any spells as of 3.0.3 (can be set dynamically)
	public uint glyphFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_GLYPH;	// used in glyph spells
	public uint unk3Flag = (uint)SpellCastTargetFlags.TARGET_FLAG_UNK3;	//
	public uint unk4Flag = (uint)SpellCastTargetFlags.TARGET_FLAG_UNK4;	// uint32, loop { vec3, guid -> if guid == 0 break }
	
	
	public SpellCastTargets()
	{
		itemTarget = null;
		GoTarget = null;
		
		unitTargetGuid = 0;
		GoTargetGuid = 0;
		CorpseTargetGuid = 0;
		itemTargetGuid = 0;
		itemTargetEntry = 0;
		
		srcX = 0;
		srcY = 0;
		srcZ = 0;
		srcCoordinats = Vector3.zero;
		
		destX = 0;
		destY = 0;
		destZ = 0;
		dstCoordinats = Vector3.zero;
		
		strTarget = "";
		targetMask = 0;
		intTargetFlags = 0;
	}
	
	public ByteBuffer Write(ulong targetGUID)
	{
		Spell spell = new Spell();
		return Write(spell, (uint)spell.Targets, targetGUID);
	}
	
	public ByteBuffer Write(Spell spell, ulong targetGUID)
	{
		return Write(spell, (uint)spell.Targets, targetGUID);
	}
	
	public ByteBuffer Write(Spell spell, uint targetFlags, ulong targetGUID)
	{
		return Write(spell, targetFlags, targetGUID, Vector3.zero);
	}
	
	public ByteBuffer Write(Spell spell, uint targetFlags, ulong targetGUID, Vector3 pos)
	{
		ByteBuffer bb = new ByteBuffer();
		
		if((targetFlags & (pvpCorpseFlag | corpseFlag)) > 0)
		{
			targetMask = ByteBuffer.Reverse(unitFlag);
		}
		else if((targetFlags & unk1Flag) > 0)
		{
			targetFlags = targetFlags & (~unk1Flag) | objFlag;
			targetMask = ByteBuffer.Reverse(targetFlags);
		}
		else
		{
			targetMask = ByteBuffer.Reverse(targetFlags);
		}
		bb.Append(targetMask);
		targetMask = targetFlags;
		if((targetMask & (unitFlag | unk2Flag)) > 0)
		{
			bb.AppendPackGUID(targetGUID);
		}
		if((targetMask & objFlag) > 0)
		{
			bb.AppendPackGUID(targetGUID);
		}
		if((targetMask & (itemFlag | tradeItemFlag)) > 0)
		{
			bb.AppendPackGUID(targetGUID);
		}
		if((targetMask & (pvpCorpseFlag | corpseFlag)) > 0)
		{
			bb.AppendPackGUID(targetGUID);
		}
		if((targetMask & srcFlag) > 0)
		{
			bb.AppendPackGUID(targetGUID);
			bb.Append(pos.x);
			bb.Append(pos.z);
			bb.Append(pos.y);
		}
		if((targetMask & dstFlag) > 0)
		{
			bb.AppendPackGUID(targetGUID);
			bb.Append(pos.x);
			bb.Append(pos.z);
			bb.Append(pos.y);
		}
		if((targetMask & stringFlag) > 0)
		{
			//TODO add string of targets
		}
		return bb;
	}
	
	public void Read(ByteBuffer data)
	{
		targetMask = (uint)data.ReadReversed32bit();
		if((targetMask & (unitFlag | pvpCorpseFlag | objFlag | corpseFlag | unk2Flag)) > 0)
		{
			if((targetMask & unitFlag) > 0)
			{
				unitTargetGuid = data.GetPacketGuid();
			}
			else if((targetMask & objFlag) > 0)
			{
				GoTargetGuid = data.GetPacketGuid();
			}
			else if((targetMask & (pvpCorpseFlag | corpseFlag)) > 0)
			{
				CorpseTargetGuid = data.GetPacketGuid();
			}
		}
		
		if((targetMask & (itemFlag | tradeItemFlag)) > 0)
		{
			itemTargetGuid = data.GetPacketGuid();
		}
		
		if((targetMask & srcFlag) > 0)
		{
			srcTransportGuid = data.GetPacketGuid();
			srcX = data.ReadFloat();
			srcZ = data.ReadFloat();
			srcY = data.ReadFloat();
			srcCoordinats = new Vector3(srcX, srcY, srcZ);
		}
		
		if((targetMask & dstFlag) > 0)
		{
			destTransportGuid = data.GetPacketGuid();
			destX = data.ReadFloat();
			destZ = data.ReadFloat();
			destY = data.ReadFloat();
			dstCoordinats = new Vector3(destX, destY, destZ);
		}
		
		if((targetMask & stringFlag) > 0)
		{
			strTarget = data.ReadString();
		}
	}
	
	public ByteBuffer WriteItem(ulong targetGUID)
	{
		ByteBuffer bb = new ByteBuffer();
		
		uint auxFlag = (uint)SpellCastTargetFlags.TARGET_FLAG_ITEM;
		targetMask |= auxFlag;
		
		bb.Append(ByteBuffer.Reverse(targetMask));
		bb.AppendPackGUID(targetGUID);
		
		return bb;
	}
}
