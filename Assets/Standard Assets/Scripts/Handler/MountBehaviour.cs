﻿using UnityEngine;
using System;

public class MountBehaviour : MonoBehaviour {

	public Transform riderPosition;
	public Player playerRefference;
	public Transform charCenter;

	Transform _saddle;	
	
	public void Awake()
	{
		_saddle = GetSaddle();
	}

	public void Start()
	{
		playerRefference.GetComponent<Animation>().Play("mount_idle");
		Camera.main.SendMessage("MountCamera", playerRefference);

		//we hide the weapons
		playerRefference.ShowHideWeapons(false);
	}
	
	
	public void Update()
	{
		transform.position = riderPosition.position;
		transform.eulerAngles = riderPosition.eulerAngles;
		
		//for player
		charCenter.position = _saddle.position;
		charCenter.eulerAngles = _saddle.eulerAngles;
	}
	
	
	
	public void Destroy()
	{
//		charCenter.localPosition = Vector3.zero;
//		charCenter.localRotation = Quaternion.identity;
//		
//		charCenter.animation.Play("idle");
//		Camera.main.SendMessage("UnmountCamera", playerRefference);
//
//		playerRefference.ShowHideWeapons(true);
		UnityEngine.Object.Destroy(gameObject);
	}
	
	
	public Transform GetSaddle()
	{
		Transform ret = null;
		if(_saddle)
		{
			ret = _saddle;
		}
		
		foreach(Transform go in transform.GetComponentsInChildren<Transform>())
		{
			if(go.tag == "mountSaddle")
			{
				ret = go;
				break;
			}
		}
		return ret;
	}

}
