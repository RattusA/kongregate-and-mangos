﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerSpellManager {

	public Dictionary<uint, Spell> playerSpellBook = new Dictionary<uint, Spell>();
	public Player player;

	public uint spellsDelayed = 0;


	public PlayerSpellManager(Player plr)
	{
		player = plr;
	}
	
	public void AddSpell(Spell spell)
	{
		playerSpellBook[spell.ID] = spell;
	}
	
	public bool RemoveSpell(uint spellID)
	{
		return playerSpellBook.Remove(spellID);
	}
	
	public bool HasSpell(uint spellID)
	{
		return playerSpellBook.ContainsKey(spellID);
	}
	
	public Spell GetSpellByID(uint spellID)
	{
		Spell ret;
		playerSpellBook.TryGetValue(spellID, out ret);
		return ret;
	}
	
	public void CastSpell(uint sp, ulong targetGUID, Vector3 pos)
	{
		Spell spell = GetSpellByID(sp);
		if(spell != null)
		{
			CastSpell(spell, targetGUID, pos);
		}
		else
		{
			Debug.Log("********SPELL " + sp + " not found! ");
		}
	}
	
	public void CastSpell(Spell sp, ulong targetGUID, Vector3 pos)
	{
		if(sp == null || spellsDelayed > 0)
		{
			return;
		}

		byte aux8 = 0;
		uint targetMask = (uint)sp.Targets;

		SpellCastTargets targets = new SpellCastTargets();
		if(targetGUID == 0)
		{
			targetGUID = player.guid;
		}
		if(targetMask == targets.selfFlag)
		{
			bool positiveSpell = sp.IsPositiveSpell();
			if(targetGUID == player.guid || !sp.IsNeedTarget())
			{
				if(sp.IsNeedTarget() && !positiveSpell)
				{
					return;
				}
			}
			else 
			{
				targetMask = targets.unitFlag;
				bool isTargetFrendly = false;
				if(player.target != null)
				{
					if(player.target.IsCreature()
					   && ((player.target as Unit).enemyStatus == 0 || player.target.guid == player.petGuid))
					{
						isTargetFrendly = true;
					}
					else if(player.target.IsPlayer())
					{
						bool isTargetFromSameFaction = (player.target as Player).SameFaction(player as Player);
						bool isTargetFromPlayerPaty = player.IsTargetFromPlayerPaty(player.target.guid);
						bool isTargetFromEnemyDuelTeam = player.IsFromEnemyDuelTeam(player.target as Player);
						if((isTargetFromSameFaction || isTargetFromPlayerPaty) && !isTargetFromEnemyDuelTeam)
						{
							isTargetFrendly = true;
						}
					}
				}
				if(positiveSpell && (sp.ID != 7266))
				{
					if(!isTargetFrendly)
					{
						targetMask = targets.selfFlag;
					}
				}
				else if(!positiveSpell && isTargetFrendly)
				{
					return;
				}
			}	
		}
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_CAST_SPELL);
		pkt.Append(aux8);
		pkt.Add(sp.ID);
		pkt.Append(aux8);//unk_flags
		pkt.Append(targets.Write(sp, targetMask, targetGUID, pos));
		RealmSocket.outQueue.Add(pkt);
		player.BroadcastMessage("SetCastingSpell", sp.ID);
	}
	
	public bool IsNeedCastAttackAfterSpell(Spell sp)
	{
		bool ret = false;
		if((sp.Attributes & ((int)SpellAttributes.SPELL_ATTR_ON_NEXT_SWING_1 | (int)SpellAttributes.SPELL_ATTR_ON_NEXT_SWING_2)) > 0)
		{
			ret = true;
		}
		return ret;
	}
	
	public Dictionary<uint, Spell> GetSpellList()
	{
		return playerSpellBook;
	}
	
};
