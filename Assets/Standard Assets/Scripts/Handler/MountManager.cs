using UnityEngine;
using System.Collections.Generic;

public class Mount
{
	public uint mountID;
	
	//mount Info
	public string name = "obj";
	public string location = "";
	public byte level = 1;		
	public uint ridingPoints;	//riding points needed for riding this mount
	public byte faction = 0;	/*(0 = anyFaction)*/			//wich faction he belongs;
	public byte bonus1;
	public byte bonus2; 
	public byte movementType = 0/*0 = ground; 1 = fly*/;
	public uint costGold = 1;
	public uint costHonorPoints = 0;
	public byte scale = 5;

	public Mount(string _name, uint _mountID, byte _movementType)
	{
		name = _name;
		mountID = _mountID;
		movementType = _movementType;
	}
};

public class MountManager {

	public List<Mount> mounts = new List<Mount>();		//store a list of mounts
	public Player rider;								//here is the owner of the mounts
	public GameObject charCenter;						//store the player center  (Bip01)
	
	public GameObject currentMount;
	public CapsuleCollider mountPlayerCollider;
	
	public MountManager(Player owner)
	{
		rider = owner;
	}
	
	public void Init()
	{
		//this mettode gets only the first level of childrens
		foreach(Transform child in rider.transform)		//gets the owner Center
		{
			if(child.gameObject.tag == "charCenter")
			{
				charCenter = child.gameObject;
				break;
			}
		}

		mountPlayerCollider = (rider.TransformParent.gameObject).GetComponent<CapsuleCollider>();
		mounts.Clear();

		//=============================================
		mounts.Add(new Mount("308", 470, 1));		// Dragon Lava
		mounts.Add(new Mount("306", 471, 0));		// Rhino
		mounts.Add(new Mount("284", 458, 0));		// Cocatrice
		mounts.Add(new Mount("18051", 35711, 0));	// Wolf Mount
		mounts.Add(new Mount("20629", 35018, 0));	// Tiger Mount
		mounts.Add(new Mount("22717", 42776, 0));	// Boar Mount
		mounts.Add(new Mount("34488", 58983, 0));	// Rhino Mount
		mounts.Add(new Mount("34488", 58997, 0));	// Feral Rhino Mount (+60% speed)
		mounts.Add(new Mount("34488", 58999, 0));	// Feral Rhino Mount (+100% speed)
		mounts.Add(new Mount("34488", 64992, 0));	// Feral Rhino Mount (+60% speed)
		mounts.Add(new Mount("34488", 64993, 0));	// Feral Rhino Mount (+100% speed)
		mounts.Add(new Mount("36474", 35022, 0));	// Horse Mount
		mounts.Add(new Mount("43520", 64977, 1));	// Frost Dragon
		mounts.Add(new Mount("45134", 64658, 1));	// Green Dragon
		mounts.Add(new Mount("57548", 35020, 1));	// Red Dragon
		mounts.Add(new Mount("58059", 10969, 0));	// Horse Mount
		mounts.Add(new Mount("59498", 17463, 0));	// Horse Mount 
		mounts.Add(new Mount("59499", 43899, 0));	// Horse Mount
	}
	
	public bool IsMounted()
	{
		unchecked
		{
			return rider.HasFlag(UpdateFields.EUnitFields.UNIT_FIELD_FLAGS, (uint)UnitFlags.UNIT_FLAG_MOUNT);
		}
	}
	
	public Mount GetMount(string name)
	{
		List<string> mountName = new List<string>(name.Split("(Clone)"[0]));
		Mount mount = mounts.Find(element => (element.name == mountName[0]));
		return mount;
	}
	
	public void Mount(string mountName)
	{
		Mount mount = mounts.Find(element => (element.mountID.ToString() == mountName));
		if(mount != null)
		{
			//check if we have already a mount instance(the unmount wasn't properly done)
			if(currentMount)
			{
				Unmount();
			}			

			LoadMount( mount.name, mount.mountID );
//			currentMount = Object.Instantiate<GameObject> (Resources.Load<GameObject> ("prefabs/mounts/" + mount.name));//instantiate the mount
//			if(currentMount)
//			{
//				rider.charMount = true;
//				MountBehaviour mountBehaviour = currentMount.GetComponent<MountBehaviour>();
//				mountBehaviour.riderPosition = rider.TransformParent;
//				mountBehaviour.playerRefference = rider;
//				mountBehaviour.charCenter = charCenter.transform.parent;
//				
//				List<Transform> allChildrens = new List<Transform>(currentMount.GetComponentsInChildren<Transform>());
//				foreach(Transform mountSaddle in allChildrens)
//				{
//					if(mountSaddle.gameObject.tag == "mountSaddle")
//					{
//						CorectionFields corect = CorectionDBC.GetCorectionPrototype((uint)CorectionTypeEnum.CORECTION_MOUNT, 
//						                                                            mount.mountID, (uint)rider.race, (uint)rider.gender);
//						if(corect != null)
//						{
//							mountSaddle.localPosition =	corect.pos;		 	// mounts[i].saddlePositionCorection[corection];
//							mountSaddle.localEulerAngles = corect.rot;		// mounts[i].saddleRotationCorection[corection];
//						}
//						rider.GetComponent<Animation>().Play("mount_idle");						// stops the idle animation and starts the mount_idle animation
//						rider.scale = mount.scale;								// creature scale become player scale(just for visual efect)
//						break;
//					}
//				}
//				rider.ShowHideWeapons(false);
//			}
		}
		else
		{
			Debug.Log("mount not found!");
		}
	}
	
	public void Unmount()
	{
		if(rider && currentMount)
		{
			currentMount.GetComponent<MountBehaviour>().Destroy();
		}

		rider.transform.localPosition = Vector3.zero;
		rider.transform.localRotation = Quaternion.identity;
		
		rider.transform.GetComponent<Animation>().Play("idle");
		Camera.main.SendMessage("UnmountCamera", rider);
		
		rider.charMount = false;
		rider.ShowHideWeapons(true);
	}

	void LoadMount (string mountName, uint mountID)
	{
		AssetBundleManager.MobCallBackStruct callBackStruct = new AssetBundleManager.MobCallBackStruct();
		callBackStruct.guid = (ulong)mountID;
		callBackStruct.scale = 1;
		callBackStruct.dummy = null;
		callBackStruct.assetPath = string.Format ("prefabs/mounts/{0}", mountName);
		WorldSession.GetBundleLoader().LoadNPCBundle(ApplyMountBundle, callBackStruct);		
	}
	
	void ApplyMountBundle ( AssetBundleManager.MobCallBackStruct callBackStruct )
	{
		string bundleName = callBackStruct.assetPath;
		string assetBundleName = string.Format ("Resources/{0}_prefab.unity3d", bundleName);
		assetBundleName = assetBundleName.ToLower();
		Object bundle;
		AssetBundleManager.instance.currentLoadedBundles.TryGetValue(assetBundleName, out bundle);
		GameObject resourceToInstantiate = null;
		if(bundle == null)
		{
			#if UNITY_EDITOR
			Debug.LogError("cannot load "+assetBundleName);
			#endif
		}
		else
		{
			resourceToInstantiate = bundle as GameObject;
		}
		if(resourceToInstantiate == null)
		{
			resourceToInstantiate = GameResources.Load<GameObject>(bundleName);
		}
		if(resourceToInstantiate != null)
		{
			currentMount = Object.Instantiate<GameObject>(resourceToInstantiate);
			if(currentMount != null)
			{
				Mount mount = mounts.Find(element => (element.mountID.ToString() == callBackStruct.guid.ToString()));
				rider.charMount = true;
				MountBehaviour mountBehaviour = currentMount.GetComponent<MountBehaviour>();
				mountBehaviour.riderPosition = rider.TransformParent;
				mountBehaviour.playerRefference = rider;
				mountBehaviour.charCenter = charCenter.transform.parent;
				
				List<Transform> allChildrens = new List<Transform>(currentMount.GetComponentsInChildren<Transform>());
				foreach(Transform mountSaddle in allChildrens)
				{
					if(mountSaddle.gameObject.tag == "mountSaddle")
					{
						CorectionFields corect = CorectionDBC.GetCorectionPrototype((uint)CorectionTypeEnum.CORECTION_MOUNT, 
						                                                            mount.mountID, (uint)rider.race, (uint)rider.gender);
						if(corect != null)
						{
							mountSaddle.localPosition =	corect.pos;		 	// mounts[i].saddlePositionCorection[corection];
							mountSaddle.localEulerAngles = corect.rot;		// mounts[i].saddleRotationCorection[corection];
						}
						rider.GetComponent<Animation>().Play("mount_idle");	// stops the idle animation and starts the mount_idle animation
						rider.scale = mount.scale;							// creature scale become player scale(just for visual efect)
						break;
					}
				}
				rider.ShowHideWeapons(false);
			}
		}
	}
}