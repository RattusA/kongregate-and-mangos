﻿using UnityEngine;
using System.Collections;

public class Spell : SpellEntry {
	
	protected int MAX_EFFECT_INDEX = 3;
	public SpellCastTargets targets = null;
	
	public Spell(uint spellID)
	{
		if(spellID == 0)
		{
			return;
		}

		SpellEntry spellEntry = dbs.sSpells.GetRecord(spellID);
		if(spellEntry == null || spellEntry.ID == 0)
		{
			spellEntry = new SpellEntry();
			spellEntry.ID = spellID;
		}
		CopyFromSpellEntru(spellEntry);
	}

	public Spell(SpellEntry spellEnrty)
	{
		if(spellEnrty != null && spellEnrty.ID > 0)
		{
			CopyFromSpellEntru(spellEnrty);
		}
	}

	public Spell()
	{
		CopyFromSpellEntru(new SpellEntry());
	}

	public void CopyFromSpellEntru(SpellEntry spellEnrty)
	{
		ID = spellEnrty.ID;
		Category = spellEnrty.Category;
		Dispel = spellEnrty.Dispel;
		Mechanic = spellEnrty.Mechanic;
		Attributes = spellEnrty.Attributes;
		AttributesEx = spellEnrty.AttributesEx;
		AttributesEx2 = spellEnrty.AttributesEx2;
		AttributesEx3 = spellEnrty.AttributesEx3;
		AttributesEx4 = spellEnrty.AttributesEx4;
		AttributesEx5 = spellEnrty.AttributesEx5;
		AttributesEx6 = spellEnrty.AttributesEx6;
		AttributesEx7 = spellEnrty.AttributesEx7;
		Stances = spellEnrty.Stances;
		unk_320_2 = spellEnrty.unk_320_2;
		StancesNot = spellEnrty.StancesNot;
		unk_320_3 = spellEnrty.unk_320_3;
		Targets = spellEnrty.Targets;
		TargetCreatureType = spellEnrty.TargetCreatureType;
		RequiresSpellFocus = spellEnrty.RequiresSpellFocus;
		FacingCasterFlags = spellEnrty.FacingCasterFlags;
		CasterAuraState = spellEnrty.CasterAuraState;
		TargetAuraState = spellEnrty.TargetAuraState;
		CasterAuraStateNot = spellEnrty.CasterAuraStateNot;
		TargetAuraStateNot = spellEnrty.TargetAuraStateNot;
		casterAuraSpell = spellEnrty.casterAuraSpell;
		targetAuraSpell = spellEnrty.targetAuraSpell;
		excludeCasterAuraSpell = spellEnrty.excludeCasterAuraSpell;
		excludeTargetAuraSpell = spellEnrty.excludeTargetAuraSpell;
		CastingTimeIndex = spellEnrty.CastingTimeIndex;
		RecoveryTime = spellEnrty.RecoveryTime;
		CategoryRecoveryTime = spellEnrty.CategoryRecoveryTime;
		InterruptFlags = spellEnrty.InterruptFlags;
		AuraInterruptFlags = spellEnrty.AuraInterruptFlags;
		ChannelInterruptFlags = spellEnrty.ChannelInterruptFlags;
		procFlags = spellEnrty.procFlags;
		procChance = spellEnrty.procChance;
		procCharges = spellEnrty.procCharges;
		maxLevel = spellEnrty.maxLevel;
		baseLevel = spellEnrty.baseLevel;
		spellLevel = spellEnrty.spellLevel;
		DurationIndex = spellEnrty.DurationIndex;
		powerType = spellEnrty.powerType;
		manaCost = spellEnrty.manaCost;
		manaCostPerlevel = spellEnrty.manaCostPerlevel;
		manaPerSecond = spellEnrty.manaPerSecond;
		manaPerSecondPerLevel = spellEnrty.manaPerSecondPerLevel;
		rangeIndex = spellEnrty.rangeIndex;
		speed = spellEnrty.speed;
		modalNextSpell = spellEnrty.modalNextSpell;
		StackAmount = spellEnrty.StackAmount;
		Totem = spellEnrty.Totem;
		Reagent = spellEnrty.Reagent;
		ReagentCount = spellEnrty.ReagentCount;
		EquippedItemClass = spellEnrty.EquippedItemClass;
		EquippedItemSubClassMask = spellEnrty.EquippedItemSubClassMask;
		EquippedItemInventoryTypeMask = spellEnrty.EquippedItemInventoryTypeMask;
		Effect = spellEnrty.Effect;
		EffectDieSides = spellEnrty.EffectDieSides;
		EffectRealPointsPerLeve = spellEnrty.EffectRealPointsPerLeve;
		EffectBasePoints = spellEnrty.EffectBasePoints;
		EffectMechanic = spellEnrty.EffectMechanic;
		EffectImplicitTargetA = spellEnrty.EffectImplicitTargetA;
		EffectImplicitTargetB = spellEnrty.EffectImplicitTargetB;
		EffectRadiusIndex = spellEnrty.EffectRadiusIndex;
		EffectApplyAuraName = spellEnrty.EffectApplyAuraName;
		EffectAmplitude = spellEnrty.EffectAmplitude;
		EffectMultipleValue = spellEnrty.EffectMultipleValue;
		EffectChainTarget = spellEnrty.EffectChainTarget;
		EffectItemType = spellEnrty.EffectItemType;
		EffectMiscValue = spellEnrty.EffectMiscValue;
		EffectMiscValueB = spellEnrty.EffectMiscValueB;
		EffectTriggerSpell = spellEnrty.EffectTriggerSpell;
		EffectPointsPerComboPoint = spellEnrty.EffectPointsPerComboPoint;
		EffectSpellClassMaskA = spellEnrty.EffectSpellClassMaskA;
		EffectSpellClassMaskB = spellEnrty.EffectSpellClassMaskB;
		EffectSpellClassMaskC = spellEnrty.EffectSpellClassMaskC;
		SpellVisual = spellEnrty.SpellVisual;
		SpellIconID = spellEnrty.SpellIconID;
		activeIconID = spellEnrty.activeIconID;
		spellPriority = spellEnrty.spellPriority;
		SpellName = spellEnrty.SpellName;
		Rank = spellEnrty.Rank;
		Description = spellEnrty.Description;
		ToolTip = spellEnrty.ToolTip;
		ManaCostPercentage = spellEnrty.ManaCostPercentage;
		StartRecoveryCategory = spellEnrty.StartRecoveryCategory;
		StartRecoveryTime = spellEnrty.StartRecoveryTime;
		MaxTargetLevel = spellEnrty.MaxTargetLevel;
		SpellFamilyName = spellEnrty.SpellFamilyName;
		SpellFamilyFlags = spellEnrty.SpellFamilyFlags;
		SpellFamilyFlags2 = spellEnrty.SpellFamilyFlags2;
		MaxAffectedTargets = spellEnrty.MaxAffectedTargets;
		DmgClass = spellEnrty.DmgClass;
		PreventionType = spellEnrty.PreventionType;
		StanceBarOrder = spellEnrty.StanceBarOrder;
		DmgMultiplier = spellEnrty.DmgMultiplier;
		MinFactionId = spellEnrty.MinFactionId;
		MinReputation = spellEnrty.MinReputation;
		RequiredAuraVision = spellEnrty.RequiredAuraVision;
		TotemCategory = spellEnrty.TotemCategory;
		AreaGroupId = spellEnrty.AreaGroupId;
		SchoolMask = spellEnrty.SchoolMask;
		runeCostID = spellEnrty.runeCostID;
		spellMissileID = spellEnrty.spellMissileID;
		PowerDisplayId = spellEnrty.PowerDisplayId;
		unk_320_4 = spellEnrty.unk_320_4;
		spellDescriptionVariableID = spellEnrty.spellDescriptionVariableID;
		SpellDifficultyId = spellEnrty.SpellDifficultyId;
	}
	
	public int CalculateSimpleValue(int eff)
	{
		return EffectBasePoints[eff] + 1;
	}
	
	public bool HasAttribute(SpellAttributes attribute)
	{
		return (Attributes & (int)attribute) != 0;
	}
	
	public bool HasAttributeEx(SpellAttributesEx attribute)
	{
		return (AttributesEx & (int)attribute) != 0;
	}
	
	public bool HasAttributeEx2(SpellAttributesEx2 attribute)
	{
		return (AttributesEx2 & (int)attribute) != 0;
	}
	
	public bool HasAttributeEx3(SpellAttributesEx3 attribute)
	{
		return (AttributesEx3 & (int)attribute) != 0;
	}
	
	public bool HasAttributeEx4(SpellAttributesEx4 attribute)
	{
		return (AttributesEx4 & (int)attribute) != 0;
	}
	
	public bool HasAttributeEx5(SpellAttributesEx5 attribute)
	{
		return (AttributesEx5 & (int)attribute) != 0;
	}
	
	public bool HasAttributeEx6(SpellAttributesEx6 attribute)
	{
		return (AttributesEx6 & (int)attribute) != 0;
	}
	
	public bool HasAttributeEx7(SpellAttributesEx7 attribute)
	{
		return (AttributesEx7 & (int)attribute) != 0;
	}
	
	public bool IsPositiveSpell()
	{
		// spells with at least one negative effect are considered negative
		// some self-applied spells have negative effects but in self casting case negative check ignored.
		for (int index = 0; index < MAX_EFFECT_INDEX; ++index)
		{
			if(Effect[index] != 0 && IsPositiveEffect(this, index) == false)
			{
				return false;
			}
		}
		return true;
	}
	
	public bool IsPositiveEffect(Spell spellproto, int effIndex)
	{
		switch((__SpellEffects)spellproto.Effect[effIndex])
		{
		case __SpellEffects.SPELL_EFFECT_DUMMY:
			// some explicitly required dummy effect sets
			switch (spellproto.ID)
			{
			case 28441:								 // AB Effect 000
				return false;
			case 49634:								 // Sergeant's Flare
			case 54530:								 // Opening
			case 62105:								 // To'kini's Blowgun
				return true;
			default:
				break;
			}
			break;
			// always positive effects (check before target checks that provided non-positive result in some case for positive effects)
		case __SpellEffects.SPELL_EFFECT_HEAL:
		case __SpellEffects.SPELL_EFFECT_LEARN_SPELL:
		case __SpellEffects.SPELL_EFFECT_SKILL_STEP:
		case __SpellEffects.SPELL_EFFECT_HEAL_PCT:
		case __SpellEffects.SPELL_EFFECT_ENERGIZE_PCT:
		case __SpellEffects.SPELL_EFFECT_QUEST_COMPLETE:
			return true;
			
			// non-positive aura use
		case __SpellEffects.SPELL_EFFECT_APPLY_AURA:
		case __SpellEffects.SPELL_EFFECT_APPLY_AREA_AURA_FRIEND:
			switch ((AuraType)spellproto.EffectApplyAuraName[effIndex])
			{
			case AuraType.SPELL_AURA_DUMMY:
				// dummy aura can be positive or negative dependent from casted spell
				switch (spellproto.ID)
				{
				case 13139:						 // net-o-matic special effect
				case 23445:						 // evil twin
				case 35679:						 // Protectorate Demolitionist
				case 38637:						 // Nether Exhaustion (red)
				case 38638:						 // Nether Exhaustion (green)
				case 38639:						 // Nether Exhaustion (blue)
				case 11196:						 // Recently Bandaged
				case 44689:						 // Relay Race Accept Hidden Debuff - DND
				case 58600:						 // Restricted Flight Area
					return false;
					// some spells have unclear target modes for selection, so just make effect positive
				case 27184:
				case 27190:
				case 27191:
				case 27201:
				case 27202:
				case 27203:
				case 47669:
					return true;
				default:
					break;
				}
				break;
			case AuraType.SPELL_AURA_MOD_DAMAGE_DONE:			// dependent from base point sign (negative -> negative)
			case AuraType.SPELL_AURA_MOD_RESISTANCE:
			case AuraType.SPELL_AURA_MOD_STAT:
			case AuraType.SPELL_AURA_MOD_SKILL:
			case AuraType.SPELL_AURA_MOD_DODGE_PERCENT:
			case AuraType.SPELL_AURA_MOD_HEALING_PCT:
			case AuraType.SPELL_AURA_MOD_HEALING_DONE:
				if(spellproto.CalculateSimpleValue(effIndex) < 0)
				{
					return false;
				}
				break;
			case AuraType.SPELL_AURA_MOD_DAMAGE_TAKEN:			// dependent from bas point sign (positive -> negative)
				if(spellproto.CalculateSimpleValue(effIndex) < 0)
				{
					return true;
				}
				// let check by target modes (for Amplify Magic cases/etc)
				break;
			case AuraType.SPELL_AURA_MOD_SPELL_CRIT_CHANCE:
			case AuraType.SPELL_AURA_MOD_INCREASE_HEALTH_PERCENT:
			case AuraType.SPELL_AURA_MOD_DAMAGE_PERCENT_DONE:
				if(spellproto.CalculateSimpleValue(effIndex) > 0)
				{
					return true;						// some expected positive spells have SPELL_ATTR_EX_NEGATIVE or unclear target modes
				}
				break;
			case AuraType.SPELL_AURA_ADD_TARGET_TRIGGER:
				return true;
			case AuraType.SPELL_AURA_PERIODIC_TRIGGER_SPELL:
				if (spellproto.ID != spellproto.EffectTriggerSpell[effIndex])
				{
					uint spellTriggeredId = (uint)spellproto.EffectTriggerSpell[effIndex];
					Spell spellTriggeredProto = SpellManager.CreateOrRetrive(spellTriggeredId);
					
					if(spellTriggeredProto != null)
					{
						// non-positive targets of main spell return early
						for (int index = 0; index < MAX_EFFECT_INDEX; ++index)
						{
							// if non-positive trigger cast targeted to positive target this main cast is non-positive
							// this will place this spell auras as debuffs
							if (spellTriggeredProto.Effect[index] != 0 &&
							    IsPositiveTarget((TargetsFlags)spellTriggeredProto.EffectImplicitTargetA[index], 
							                 (TargetsFlags)spellTriggeredProto.EffectImplicitTargetB[index]) &&
							    !IsPositiveEffect(spellTriggeredProto, index))
							{
								return false;
							}
						}
					}
				}
				break;
			case AuraType.SPELL_AURA_PROC_TRIGGER_SPELL:
				// many positive auras have negative triggered spells at damage for example and this not make it negative (it can be canceled for example)
				break;
			case AuraType.SPELL_AURA_MOD_STUN:					// have positive and negative spells, we can't sort its correctly at this moment.
				if (effIndex == (int)SpellEffectIndex.EFFECT_INDEX_0 &&
				    spellproto.Effect[(int)SpellEffectIndex.EFFECT_INDEX_1] == 0 &&
				    spellproto.Effect[(int)SpellEffectIndex.EFFECT_INDEX_2] == 0)
					return false;						// but all single stun aura spells is negative
				
				// Petrification
				if (spellproto.ID == 17624)
					return false;
				break;
			case AuraType.SPELL_AURA_MOD_PACIFY_SILENCE:
				switch (spellproto.ID)
				{
				case 24740:						 // Wisp Costume
				case 47585:						 // Dispersion
					return true;
				default:
					break;
				}
				return false;
			case AuraType.SPELL_AURA_MOD_ROOT:
			case AuraType.SPELL_AURA_MOD_FEAR:
			case AuraType.SPELL_AURA_MOD_SILENCE:
			case AuraType.SPELL_AURA_GHOST:
			case AuraType.SPELL_AURA_PERIODIC_LEECH:
			case AuraType.SPELL_AURA_MOD_STALKED:
			case AuraType.SPELL_AURA_PERIODIC_DAMAGE_PERCENT:
				return false;
			case AuraType.SPELL_AURA_PERIODIC_DAMAGE:			// used in positive spells also.
				// part of negative spell if casted at self (prevent cancel)
				if((TargetsFlags)spellproto.EffectImplicitTargetA[effIndex] == TargetsFlags.TARGET_SELF ||
				   (TargetsFlags)spellproto.EffectImplicitTargetA[effIndex] == TargetsFlags.TARGET_SELF2)
				{
					return false;
				}
				break;
			case AuraType.SPELL_AURA_MOD_DECREASE_SPEED:		 // used in positive spells also
				// part of positive spell if casted at self
				if(((TargetsFlags)spellproto.EffectImplicitTargetA[effIndex] == TargetsFlags.TARGET_SELF ||
				    (TargetsFlags)spellproto.EffectImplicitTargetA[effIndex] == TargetsFlags.TARGET_SELF2) &&
				   (SpellFamily)spellproto.SpellFamilyName == SpellFamily.SPELLFAMILY_GENERIC)
				{
					return false;
				}
				// but not this if this first effect (don't found better check)
				if(spellproto.HasAttribute(SpellAttributes.SPELL_ATTR_UNK26) && 
				   (SpellEffectIndex)effIndex == SpellEffectIndex.EFFECT_INDEX_0)
				{
					return false;
				}
				break;
			case AuraType.SPELL_AURA_TRANSFORM:
				// some spells negative
				switch (spellproto.ID)
				{
				case 36897:						 // Transporter Malfunction (race mutation to horde)
				case 36899:						 // Transporter Malfunction (race mutation to alliance)
					return false;
				}
				break;
			case AuraType.SPELL_AURA_MOD_SCALE:
				// some spells negative
				switch(spellproto.ID)
				{
				case 802:							// Mutate Bug, wrongly negative by target modes
					return true;
				case 36900:						 // Soul Split: Evil!
				case 36901:						 // Soul Split: Good
				case 36893:						 // Transporter Malfunction (decrease size case)
				case 36895:						 // Transporter Malfunction (increase size case)
					return false;
				}
				break;
			case AuraType.SPELL_AURA_MECHANIC_IMMUNITY:
				// non-positive immunities
				switch ((Mechanics)spellproto.EffectMiscValue[effIndex])
				{
				case Mechanics.MECHANIC_BANDAGE:
				case Mechanics.MECHANIC_SHIELD:
				case Mechanics.MECHANIC_MOUNT:
				case Mechanics.MECHANIC_INVULNERABILITY:
					return false;
				default:
					break;
				}
				break;
			case AuraType.SPELL_AURA_ADD_FLAT_MODIFIER:			// mods
			case AuraType.SPELL_AURA_ADD_PCT_MODIFIER:
				// non-positive mods
				switch ((SpellModOp)spellproto.EffectMiscValue[effIndex])
				{
				case SpellModOp.SPELLMOD_COST:					// dependent from bas point sign (negative -> positive)
					if(spellproto.CalculateSimpleValue(effIndex) > 0)
					{
						return false;
					}
					break;
				default:
					break;
				}
				break;
			case AuraType.SPELL_AURA_FORCE_REACTION:
				switch (spellproto.ID)
				{
				case 42792:						 // Recently Dropped Flag (prevent cancel)
				case 46221:						 // Animal Blood
					return false;
				default:
					break;
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		
		// non-positive targets
		if(!IsPositiveTarget((TargetsFlags)spellproto.EffectImplicitTargetA[effIndex], 
		                     (TargetsFlags)spellproto.EffectImplicitTargetB[effIndex]))
		{
			return false;
		}
		// AttributesEx check
		if(spellproto.HasAttributeEx(SpellAttributesEx.SPELL_ATTR_EX_NEGATIVE))
		{
			return false;
		}
		// ok, positive
		return true;
	}
	
	public bool IsPositiveTarget(TargetsFlags targetA, TargetsFlags targetB)
	{
		switch(targetA)
		{
			// non-positive targets
		case TargetsFlags.TARGET_CHAIN_DAMAGE:
		case TargetsFlags.TARGET_ALL_ENEMY_IN_AREA:
		case TargetsFlags.TARGET_ALL_ENEMY_IN_AREA_INSTANT:
		case TargetsFlags.TARGET_IN_FRONT_OF_CASTER:
		case TargetsFlags.TARGET_ALL_ENEMY_IN_AREA_CHANNELED:
		case TargetsFlags.TARGET_CURRENT_ENEMY_COORDINATES:
		case TargetsFlags.TARGET_SINGLE_ENEMY:
		case TargetsFlags.TARGET_IN_FRONT_OF_CASTER_30:
			return false;
			// positive or dependent
		case TargetsFlags.TARGET_CASTER_COORDINATES:
			return (targetB == TargetsFlags.TARGET_ALL_PARTY || targetB == TargetsFlags.TARGET_ALL_FRIENDLY_UNITS_AROUND_CASTER);
		default:
			break;
		}
		if(targetB != 0)
		{
			return IsPositiveTarget(targetB, 0);
		}
		return true;
	}
	
	public bool IsExplicitPositiveTarget(TargetsFlags targetA)
	{
		// positive targets that in target selection code expect target in m_targers, so not that auto-select target by spell data by m_caster and etc
		switch (targetA)
		{
		case TargetsFlags.TARGET_SINGLE_FRIEND:
		case TargetsFlags.TARGET_SINGLE_PARTY:
		case TargetsFlags.TARGET_CHAIN_HEAL:
		case TargetsFlags.TARGET_SINGLE_FRIEND_2:
		case TargetsFlags.TARGET_AREAEFFECT_PARTY_AND_CLASS:
			return true;
		default:
			break;
		}
		return false;
	}
	
	public bool IsExplicitNegativeTarget(TargetsFlags targetA)
	{
		// non-positive targets that in target selection code expect target in m_targers, so not that auto-select target by spell data by m_caster and etc
		switch(targetA)
		{
		case TargetsFlags.TARGET_CHAIN_DAMAGE:
		case TargetsFlags.TARGET_CURRENT_ENEMY_COORDINATES:
		case TargetsFlags.TARGET_SINGLE_ENEMY:
			return true;
		default:
			break;
		}
		return false;
	}
	
	public bool IsNeedTarget()
	{
		bool ret = false;
		int index;
		for(index = 0; index < MAX_EFFECT_INDEX; index++)
		{
			TargetsFlags targetA = (TargetsFlags)EffectImplicitTargetA[index];
//			TargetsFlags targetB = (TargetsFlags)EffectImplicitTargetB[index];
			switch(targetA)
			{
			case TargetsFlags.TARGET_ALL_ENEMY_IN_AREA:
			case TargetsFlags.TARGET_ALL_ENEMY_IN_AREA_INSTANT:
			case TargetsFlags.TARGET_IN_FRONT_OF_CASTER:
			case TargetsFlags.TARGET_ALL_ENEMY_IN_AREA_CHANNELED:
			case TargetsFlags.TARGET_IN_FRONT_OF_CASTER_30:
			case TargetsFlags.TARGET_CASTER_COORDINATES:
				ret = true;
				break;
			default:
				break;
			}
			
			if(ret)
			{
				break;
			}
		}
		
		for(index = 0; (index < MAX_EFFECT_INDEX) && (ret); index++)
		{
			if(Effect[index] == (int)__SpellEffects.SPELL_EFFECT_TRIGGER_SPELL)
			{
				SpellEntry spellEntry = dbs.sSpells.GetRecord(EffectTriggerSpell[index]);
				if(spellEntry.ID == 0)
				{
					Debug.LogError("Missed spell with ID "+EffectTriggerSpell[index]);
					continue;
				}

				Spell triggerSpell = new Spell(spellEntry);
				if(!triggerSpell.IsPositiveSpell())
				{
					ret = false;
				}
			}
		}
		return !ret;
	}

	public bool IsHaveTargetEffect(TargetsFlags targetEffect)
	{
		bool ret = false;
		int index;
		for(index = 0; index < MAX_EFFECT_INDEX; index++)
		{
			TargetsFlags targetA = (TargetsFlags)EffectImplicitTargetA[index];
			TargetsFlags targetB = (TargetsFlags)EffectImplicitTargetB[index];
			if(targetA == targetEffect)
			{
				ret = true;
				break;
			}
			if(targetB == targetEffect)
			{
				ret = true;
				break;
			}
		}
		return ret;
	}
};
