﻿using UnityEngine;

public class FishingFloatController : MonoBehaviour
{
	[SerializeField]
	private _GameObject go;
	private RopeScript rope;
	private Vector3 position;
	
	void  OnDestroy ()
	{
		if(rope == null)
		{
			ulong caserGuid = go.GetUInt64Value((int)UpdateFields.EDynamicObjectFields.DYNAMICOBJECT_CASTER);
			BaseObject player = WorldSession.GetRealChar(caserGuid);
			if(player != null)
			{
				rope = player.gameObject.GetComponentInChildren<RopeScript>();
			}
		}
		if(rope != null)
		{
			rope.DestroyRope();
		}
	}

	/// <summary>
	/// Builds the rope.
	/// </summary>
	/// <param name="player">Player.</param>
	public void BuildRope ( BaseObject player )
	{
		if(rope == null)
		{
			ulong casterGuid = go.GetUInt64Value((int)UpdateFields.EDynamicObjectFields.DYNAMICOBJECT_CASTER);
			if(player.guid == casterGuid)
			{
				rope = player.gameObject.GetComponentInChildren<RopeScript>();
				if(rope != null)
				{
					rope.target = transform;
					rope.BuildRope();
				}
			}
		}
	}
}
