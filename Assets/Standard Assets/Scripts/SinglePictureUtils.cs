﻿using System;
using UnityEngine;
using System.Collections;

public class SinglePictureUtils
{
	static public UIToolkit CreateUIToolkit(Texture2D texture, string textureName)
	{
		GameObject objectUIToolkit = new GameObject(texture.name);
		objectUIToolkit.layer = 14; // UILayer
		objectUIToolkit.transform.parent = UIPrime31.instance.transform;
		UIToolkit toolkitScript = objectUIToolkit.AddComponent<UIToolkit>();

		toolkitScript.material = new Material(Shader.Find("UIToolkit/No Alpha"));
		toolkitScript.material.mainTexture = texture;
		
		toolkitScript.GetInfoForSingleTexture(texture, textureName);
		return toolkitScript;
	}
	
	static public UIToolkit CreateUIToolkit(string textureName)
	{
		string textureNameForLoad = textureName.Substring(0, textureName.LastIndexOf('.'));
		int lastSlashPosition = textureName.LastIndexOf('/')+1;
		textureName = textureName.Substring(lastSlashPosition, textureName.Length - lastSlashPosition);
		Texture2D spriteTexture = Resources.Load<Texture2D>(textureNameForLoad);
		if(!spriteTexture)
		{
			spriteTexture = Resources.Load<Texture2D>("Icons/noicon");
		}
		return CreateUIToolkit(spriteTexture, textureName);
	}

	static public void DestroySingleUISprite(UISprite sprite)
	{
		if (sprite != null) 
		{
			UIToolkit tempUIToolkit = sprite.manager;
			sprite.destroy();
			sprite = null; 
			if(tempUIToolkit)
			{
				GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
			}
		}
	}

	static public UIToolkit CreateUIToolkitForAtlas(Texture2D texture)
	{
		GameObject objectUIToolkit = new GameObject(texture.name);
		objectUIToolkit.layer = 14; // UILayer
		objectUIToolkit.transform.parent = UIPrime31.instance.transform;
		UIToolkit toolkitScript = objectUIToolkit.AddComponent<UIToolkit>();
		
		toolkitScript.material = new Material(Shader.Find("UIToolkit/No Alpha"));
		toolkitScript.material.mainTexture = texture;

		toolkitScript.texturePackerConfigName = texture.name;
		toolkitScript.loadTextureAndPrepareForUse();

		toolkitScript.SetMeshRenderer(toolkitScript.material);
		return toolkitScript;
	}
}
