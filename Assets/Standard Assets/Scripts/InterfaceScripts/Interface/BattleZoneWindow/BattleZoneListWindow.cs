using UnityEngine;
using System.Collections;

public class PvPInfo {

	public bool showPvPWindow = false;
	
	public int honorToday = 0;
	public int honorYesterday = 0;
	public int honorTotal = 0;
	
	public int killsToday = 0;
	public int killsYesterday = 0;
	public int killsTotal = 0;
}

public class BattleZoneListWindow : MonoBehaviour {

	[SerializeField]
	private GUISkin _skin;
	[SerializeField]	
	public BattleZoneWindow parent;

	public PvPInfo pvpInfo = new PvPInfo();
	public string joinButtonText;
	public string joinGroupButtonText;
	
	public void Awake()
	{
		_skin.button.fontSize = (int)(Screen.height * 0.065f);
		_skin.label.fontSize = (int)(Screen.height * 0.05f);
		_skin.GetStyle("DarkButton").fontSize = (int)(Screen.height * 0.065f);
		_skin.GetStyle("LightButton").fontSize = (int)(Screen.height * 0.065f);
		_skin.GetStyle("CategoryLabel").fontSize = (int)(Screen.height * 0.065f);
		_skin.GetStyle("CategoryLabel").contentOffset = new Vector2(0, (int)(Screen.height * 0.007f));
	}

	public void OnEnable()
	{
		parent.battleZoneManager.GetBattleZoneList();
	}
	
	public void DrawGUI()
	{
		float SW = Screen.width;
		float SH = Screen.height;
		GUI.skin = _skin;
		GUI.Box(new Rect(SW * 0.01f, SH * 0.12f, SW * 0.98f, SH * 0.87f), "", _skin.GetStyle("PvPJoinWindowBox"));
		
		GUI.Label(new Rect(SW * 0.035f, SH * 0.14f, SW * 0.7f, SH * 0.12f), "Battles",
		          _skin.GetStyle("CategoryLabel"));
		
		if(GUI.Button(new Rect(SW * 0.75f, SH * 0.2f, SW * 0.25f, SH * 0.1f), "PvP", _skin.GetStyle("LightButton")))
		{
			pvpInfo.showPvPWindow = true;
		}
		
		if(GUI.Button(new Rect(SW * 0.735f, SH * 0.55f, SW * 0.25f, SH * 0.1f), joinButtonText, _skin.GetStyle("DarkButton")))
		{
			if(joinGroupButtonText == "Leave")
			{
				return;
			}

			if(joinButtonText == "Leave")
			{
				joinButtonText = "Enlist";
				parent.battleZoneManager.LeaveBattleZoneQuerry();
			}
			else
			{
				joinButtonText = "Leave";
				parent.battleZoneManager.AddToWaitList(0, 2, 0, 0);
			}
		}

		if(GUI.Button(new Rect(SW * 0.735f, SH * 0.7f, SW * 0.25f, SH * 0.1f), joinGroupButtonText, _skin.GetStyle("DarkButton")))
		{
			if(joinButtonText == "Leave")
			{
				return;
			}

			if(joinGroupButtonText == "Leave")
			{
				joinGroupButtonText = "Premade";
				parent.battleZoneManager.LeaveBattleZoneQuerry();
			}
			else
			{
				joinGroupButtonText = "Leave";
				parent.battleZoneManager.AddToWaitList(0, 2, 0, 1);
			}
		}

		DrawBattleZoneList();

		if(pvpInfo.showPvPWindow)
		{
			GUI.ModalWindow(1, new Rect(0, 0, SW, SH), DrawPvPWindow, "");
		}
//		GUI.Button(new Rect(SW * 0.75f, SH * 0.87f, SW * 0.25f, SH * 0.1f), "Quit");
	}

	private void DrawBattleZoneList()
	{
		float SW = Screen.width;
		float SH = Screen.height;

		GUI.Button(new Rect(SW * 0.035f, SH * 0.27f, SW * 0.65f, SH * 0.08f), "The Strongholds");
	}

	private void DrawPvPWindow(int index)
	{
		float SW = Screen.width;
		float SH = Screen.height;
		GUI.Box(new Rect(SW * 0.15f, SH * 0.25f, SW * 0.7f, SH * 0.5f), "", _skin.GetStyle("PvPWindow"));

		GUI.Label(new Rect(SW * 0.35f, SH * 0.27f, SW * 0.15f, SH * 0.06f), "Prestige:");
		GUI.Label(new Rect(SW * 0.5f, SH * 0.27f, SW * 0.15f, SH * 0.06f), pvpInfo.honorTotal.ToString());

		GUI.Label(new Rect(SW * 0.32f, SH * 0.35f, SW * 0.1f, SH * 0.06f), "Today");
		GUI.Label(new Rect(SW * 0.47f, SH * 0.35f, SW * 0.15f, SH * 0.06f), "Yesterday");
		GUI.Label(new Rect(SW * 0.67f, SH * 0.35f, SW * 0.1f, SH * 0.06f), "Total");

		GUI.Label(new Rect(SW * 0.15f, SH * 0.45f, SW * 0.15f, SH * 0.06f), "PVP Kills");
		GUI.Label(new Rect(SW * 0.15f, SH * 0.55f, SW * 0.15f, SH * 0.06f), "Prestige");

		// Kills Values
		GUI.Label(new Rect(SW * 0.32f, SH * 0.45f, SW * 0.1f, SH * 0.06f), pvpInfo.killsToday.ToString());
		GUI.Label(new Rect(SW * 0.47f, SH * 0.45f, SW * 0.15f, SH * 0.06f), pvpInfo.killsYesterday.ToString());
		GUI.Label(new Rect(SW * 0.67f, SH * 0.45f, SW * 0.1f, SH * 0.06f), pvpInfo.honorTotal.ToString());

		// Honor Values
		GUI.Label(new Rect(SW * 0.32f, SH * 0.55f, SW * 0.1f, SH * 0.06f), pvpInfo.honorToday.ToString());
		GUI.Label(new Rect(SW * 0.47f, SH * 0.55f, SW * 0.15f, SH * 0.06f), pvpInfo.honorYesterday.ToString());
		GUI.Label(new Rect(SW * 0.67f, SH * 0.55f, SW * 0.1f, SH * 0.06f), pvpInfo.honorTotal.ToString());


		if(GUI.Button(new Rect(SW * 0.4f, SH * 0.64f, SW * 0.2f, SH * 0.09f), "Close", _skin.GetStyle("LightButton")))
		{
			pvpInfo.showPvPWindow = false;
		}
	}

}
