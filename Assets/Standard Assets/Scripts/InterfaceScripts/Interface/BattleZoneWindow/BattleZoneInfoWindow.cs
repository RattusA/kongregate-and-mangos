using UnityEngine;
using System.Collections;

public class BattleZoneInfoWindow : MonoBehaviour {

	[SerializeField]
	private GUISkin _skin;
	
	public BattleZoneWindow parentWnd;

	private Vector2 _scrollPosition = Vector2.zero;

	public void Awake()
	{
		_skin.GetStyle("LightButton").fontSize = (int)(Screen.height * 0.065f);
	}

	public void OnEnable()
	{
		WorldPacket pkt = new WorldPacket(OpCodes.MSG_PVP_LOG_DATA);
		RealmSocket.outQueue.Add(pkt);
	}

	public void DrawGUI()
	{
		float SW = Screen.width;
		float SH = Screen.height;
		GUI.skin = _skin;
		GUI.Box(new Rect(SW * 0.01f, SH * 0.12f, SW * 0.74f, SH * 0.87f), "");

		GUI.Button(new Rect(SW * 0.75f, SH * 0.2f, SW * 0.25f, SH * 0.1f), "All", _skin.GetStyle("LightButton"));
		GUI.Button(new Rect(SW * 0.75f, SH * 0.4f, SW * 0.25f, SH * 0.1f), "Alliance", _skin.GetStyle("LightButton"));
		GUI.Button(new Rect(SW * 0.75f, SH * 0.6f, SW * 0.25f, SH * 0.1f), "Fury", _skin.GetStyle("LightButton"));

		if(GUI.Button(new Rect(SW * 0.75f, SH * 0.8f, SW * 0.25f, SH * 0.1f), "Leave", _skin.GetStyle("LightButton")))
		{
			parentWnd.parent.parent.parentMng.messageWnd.InitTwoButtonWindow("Do you want leave BattleZone?", "Yes", "No");
			parentWnd.parent.parent.parentMng.messageWnd.onOkButton += ConfirmLeavBattleZone;
		}

		GUILayout.BeginArea(new Rect(SW * 0.03f, SH * 0.14f, SW * 0.7f, SH * 0.1f));
		_scrollPosition.x = GUILayout.HorizontalScrollbar(_scrollPosition.x, 50, 0, SW);
		GUILayout.EndArea();
	}

	public void ConfirmLeavBattleZone(bool state)
	{
		parentWnd.battleZoneManager.LeaveBattleZoneQuerry();
		parentWnd.parent.parent.parentMng.SetWindowState(InterfaceWindowState.ACTION_BAR_PANEL_STATE);
	}
}
