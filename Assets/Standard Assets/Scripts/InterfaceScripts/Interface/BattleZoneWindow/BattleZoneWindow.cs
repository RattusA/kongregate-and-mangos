﻿using UnityEngine;
using System.Collections;

public enum BattleZoneWindowState {

	BATTLEZONE_LIST_WINDOW_STATE,
	BATTLEZONE_INFO_WINDOW_STATE,
};

public class BattleZoneWindow : MonoBehaviour {

	[SerializeField]
	public BattleZoneListWindow battleZoneListWindow;
	[SerializeField]
	public BattleZoneInfoWindow battleZoneInfoWindow;
	[SerializeField]
	public BattleZoneManager battleZoneManager;
	[SerializeField]
	public ExtraMenuWindow parent;
	
	public BattleZoneWindowState windowState = BattleZoneWindowState.BATTLEZONE_LIST_WINDOW_STATE;

	private void OnEnable()
	{
		SetWindowState(windowState);
	}

	private void OnDisable()
	{
		switch(windowState)
		{
		case BattleZoneWindowState.BATTLEZONE_LIST_WINDOW_STATE:
			battleZoneListWindow.gameObject.SetActive(false);
			break;
		case BattleZoneWindowState.BATTLEZONE_INFO_WINDOW_STATE:
			battleZoneInfoWindow.gameObject.SetActive(false);
			break;
		}
	}
	
	public void DrawGUI()
	{
		switch(windowState)
		{
		case BattleZoneWindowState.BATTLEZONE_LIST_WINDOW_STATE:
			battleZoneListWindow.DrawGUI();
			break;
		case BattleZoneWindowState.BATTLEZONE_INFO_WINDOW_STATE:
			battleZoneInfoWindow.DrawGUI();
			break;
		}
	}

	public void SetWindowState(BattleZoneWindowState newState)
	{
		OnDisable();
		
		switch (newState)
		{
		case BattleZoneWindowState.BATTLEZONE_LIST_WINDOW_STATE:
			battleZoneListWindow.gameObject.SetActive(true);
			break;
		case BattleZoneWindowState.BATTLEZONE_INFO_WINDOW_STATE:
			battleZoneInfoWindow.gameObject.SetActive(true);
			break;
		}
		windowState = newState;
	}
}
