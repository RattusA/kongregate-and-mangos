﻿using UnityEngine;
using System.Collections;

public class GossipWindow : MonoBehaviour
{
	private GUISkin skin;
	public NPCWindowManager parent;
	public GossipManager mng;

	protected Rect gossipWindowRect;
	protected Rect closeButtonRect;
	protected Rect leftDecorationRect;
	protected Rect rightDecorationRect;

	protected Vector2 scrollPosition;
	protected Rect scrollZone;

	public void Awake()
	{
		float SW = Screen.width;
		float SH = Screen.height;

		skin = Resources.Load<GUISkin> ("GUI/GossipSkin");
		skin.label.fontSize = (int)(SH * 0.045f);
		skin.button.fontSize = (int)(SH * 0.045f);

		gossipWindowRect = new Rect(SW * 0.1f, SH * 0.05f, SW * 0.8f, SH * 0.93f);
		closeButtonRect = new Rect(SW * 0.4f, SH * 0.88f, SW * 0.2f, SH * 0.08f);
		leftDecorationRect = new Rect(SW * 0.12f, SH * 0.88f, SW * 0.28f, SH * 0.07f);
		rightDecorationRect = new Rect(SW * 0.6f, SH * 0.88f, SW * 0.28f, SH * 0.07f);

		scrollPosition = Vector2.zero;
		scrollZone = new Rect(SW * 0.1125f, SH * 0.12f, SW * 0.775f, SH * 0.7f);
	}

	public void OnDestroy()
	{
		parent.gossipWnd = null;
		parent = null;

		Resources.UnloadAsset(skin);
		skin = null;
		mng = null;
	}

	public void DrawGUI()
	{
		GUI.Box(gossipWindowRect, "", skin.box);
		if(GUI.Button(closeButtonRect, "", skin.GetStyle("CloseButton")))
		{
			CloseGossipMenu();
		}

		GUI.Label(leftDecorationRect, "", skin.GetStyle("Decoration"));
		GUI.Label(rightDecorationRect, "", skin.GetStyle("Decoration"));

		GUILayout.BeginArea(scrollZone);
		scrollPosition = GUILayout.BeginScrollView(scrollPosition);
		GUILayout.Label(WordProcessing.ReplaceDefinedWords(mng.GetGossipInfoText()), skin.label);
		GUILayout.Space(20);
		DrawMenuElements();
		GUILayout.EndScrollView();	
		GUILayout.EndArea();
	}

	private void DrawMenuElements()
	{
		for(int index = 0; index < mng.menuCount; index++)
		{
			if(GUILayout.Button(mng.GetMenuItemText(index), skin.button, GUILayout.Height((int)(Screen.height * 0.09f))))
			{
				GossipDelegat(index);
			}
		}
	}

	private void GossipDelegat(int index)
	{
		mng.SendGossipSelectedOption(index);
		CloseGossipMenu();
	}
		
	private void CloseGossipMenu()
	{
		parent.SetWindowState(NpcWindowState.NONE);
		InterfaceManager.Instance.SetWindowState(InterfaceWindowState.ACTION_BAR_PANEL_STATE);
		Defines.showInterf = true;

		Destroy(this.gameObject);
	}
}