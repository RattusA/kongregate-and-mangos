using UnityEngine;
using System.Collections;

public class BankerWindow : MonoBehaviour {


	public GUISkin Skin;
	public NPCWindowManager Parent;
	public BankerManager Mng;
	public InventoryWindowBase InventoryWnd;

	private const int BagsCount = 8;
	private int OpenedBagsCount = 0;

	private float SW;
	private float SH;

	public void Awake()
	{
		SW = Screen.width;
		SH = Screen.height;

		Skin.button.fontSize = (int)(SH * 0.05f);
		Skin.label.fontSize = (int)(SH * 0.06f);
	}

	public void SetBankerGuid(ulong guid)
	{
		Mng = new BankerManager();
		Mng.PlayerScript = InterfaceManager.Instance.PlayerScript;
		Mng.BankerGuid = guid;
		Mng.InitBanker();
	}

	public void GetOpenedBagsCount()
	{
		ByteBuffer bb = new ByteBuffer();
		bb.Append(Mng.PlayerScript.GetUInt32Value((int)(ushort)UpdateFields.EUnitFields.PLAYER_BYTES_2));
		bb.Read();
		OpenedBagsCount = (int)bb.Read();
	}

	public void DrawGUI()
	{
		InventoryWnd.DrawGUI();

		GUI.skin = Skin;
		GUI.Box(new Rect(SW * 0.25f - SH * 0.3125f, SH * 0.225f, SH * 0.675f, SH * 0.75f), "");

		for(int bagIndex = 0; bagIndex < BagsCount; bagIndex++)
		{
			ItemButton bagButton;
			Mng.InventoryBags.TryGetValue(bagIndex, out bagButton);
			if(bagButton != null)
			{
				DrawBankBagButton(bagButton.position, bagIndex);
			}
		}
		
		for(int slotIndex = 0; slotIndex < Mng.BagSlotCount; slotIndex++)
		{
			ItemButton itemButton;
			Mng.BagSlots.TryGetValue(slotIndex, out itemButton);
			if(itemButton != null && itemButton.texture != null)
			{
				GUI.DrawTexture(itemButton.position, itemButton.texture);
				if(GUI.Button(itemButton.position, "", GUIStyle.none))
				{
					ItemEvent(itemButton);
				}
			}
		}

		for(int slotIndex = Mng.BagSlotCount; slotIndex < 28; slotIndex++)
		{
			ItemButton itemButton;
			Mng.ClosedSlots.TryGetValue(slotIndex, out itemButton);
			if(itemButton != null && itemButton.texture != null)
			{
				GUI.DrawTexture(itemButton.position, itemButton.texture);
			}
		}

		GUI.Box(new Rect(SW * 0.75f - SH * 0.285f, SH * 0.82f, SH * 0.62f, SH * 0.14f), "",
		        Skin.GetStyle("Buttons Background"));

		if(GUI.Button(new Rect(SW * 0.75f - SH * 0.26f, SH * 0.85f, SH * 0.275f, SH * 0.08f), "Buy Slot"))
		{
			Mng.GetBankSlotCost();
		}

		if(GUI.Button(new Rect(SW * 0.75f + SH * 0.035f, SH * 0.85f, SH * 0.275f, SH * 0.08f), "Close"))
		{
			Parent.CloseNPCDialog();
			Destroy(this.gameObject);
		}

		switch(InventoryWnd.modalInfoWindowState)
		{
		case ModalInfoWindow.None:
			if(IsDragBegin)
			{
				Rect dragRect = DragItem.position;
				dragRect.center = new Vector2(Input.mousePosition.x, SH - Input.mousePosition.y);
				GUI.DrawTexture(dragRect, DragItem.texture);
			}
			break;
		}
	}

	public void ItemEvent(ItemButton itemButton)
	{
		InventoryWnd.descriptionWnd = new DescriptionWindow();
		InventoryWnd.descriptionWnd.IconTecture = itemButton.texture;
		InventoryWnd.descriptionWnd.TitleText = itemButton.item.name;
		InventoryWnd.descriptionWnd.DescriptionTextValue = itemButton.item.GetItemBasicStatsOutName();
		
		InfoWindowButton closeButton = InventoryWnd.descriptionWnd.ButtonManager.AddFindButton(InfoWindowButtonType.Close);
		closeButton.ButtonName = "Close";
		closeButton.onButtonUp += InventoryWnd.CloseDescriptionWindow;

		InfoWindowButton deleteButton = InventoryWnd.descriptionWnd.ButtonManager.AddFindButton(InfoWindowButtonType.Delete);
		deleteButton.item = itemButton.item;
		deleteButton.ButtonName = "Delete";
		deleteButton.onButtonUpWithSender += InventoryWnd.DestroyItemWindow;

		InventoryWnd.modalInfoWindowState = ModalInfoWindow.Description;
	}

	private void DrawBankBagButton(Rect position, int index)
	{
		ItemButton itemButton;
		Mng.InventoryBags.TryGetValue(index, out itemButton);
		if(index == 0)
		{
			if(GUI.Button(position, "", Skin.GetStyle("Bag Icon")))
			{
				Mng.ShowBagSlots(index);
			}
		}
		else
		{
			if(index > OpenedBagsCount)
			{
				GUI.Label(position, "", Skin.GetStyle("Blocked Bag Icon"));
			}
			else
			{
				if(itemButton.item == null || itemButton.item.guid == 0 || itemButton.item.entry == 0)
				{
					GUI.Label(position, "", Skin.GetStyle("Empty Bag Icon"));
				}
				else
				{
					if(GUI.Button(position, itemButton.texture, GUIStyle.none))
					{
						Mng.ShowBagSlots(index);
					}
				}
			}
		}
	}

	public void FixedUpdate()
	{
		if(Mng.PlayerScript.itemManager.NeedUpdateBanker)
		{
			Mng.InitBanker();
			Mng.PlayerScript.itemManager.NeedUpdateBanker = false;
		}

		GetOpenedBagsCount();
	}

	private ItemButton DragItem;
	private bool IsDragBegin = false;
	
	public void LateUpdate()
	{
		Vector2 touchPosition = new Vector2(Input.mousePosition.x, SH - Input.mousePosition.y);
		if(Input.GetMouseButtonDown(0))
		{
			ItemButton itemButton = InventoryWnd.mng.IsTouchInSlotsContainer(touchPosition);
			if(itemButton != null && itemButton.texture != null)
			{
				DragItem = itemButton;
				IsDragBegin = true;
				return;
			}
			
			itemButton = InventoryWnd.mng.IsTouchInBagContainer(touchPosition);
			if(itemButton != null)
			{
				if(itemButton.item != null && itemButton.item.entry != 0 && (itemButton.item as Bag).IsEmpty())
				{
					DragItem = itemButton;
					IsDragBegin = true;
				}
				return;
			}
			
			itemButton = Mng.IsTouchInSlotsContainer(touchPosition);
			if(itemButton != null && itemButton.texture != null)
			{
				DragItem = itemButton;
				IsDragBegin = true;
				return;
			}
			
			itemButton = Mng.IsTouchInBagContainer(touchPosition);
			if(itemButton != null)
			{
				if(itemButton.item != null && itemButton.item.entry != 0 && (itemButton.item as Bag).IsEmpty())
				{
					DragItem = itemButton;
					IsDragBegin = true;
				}
				return;
			}
		}
		
		if(IsDragBegin)
		{
			if(Input.GetMouseButtonUp(0))
			{
				IsDragBegin = false;
				
				if(DragItem.position.Contains(touchPosition))
				{
					return;
				}
				
				ItemButton itemButton = InventoryWnd.mng.IsTouchInSlotsContainer(touchPosition);
				if(itemButton != null)
				{
					byte bag = (byte)itemButton.item.GetSlotByBag();
					byte slot = itemButton.item.slot;
					Mng.PlayerScript.itemManager.MoveToSlot(DragItem.item, slot, bag);
					return;
				}
				
				itemButton = InventoryWnd.mng.IsTouchInBagContainer(touchPosition);
				if(itemButton != null)
				{
		
					if(DragItem.item.classType == ItemClass.ITEM_CLASS_CONTAINER)
					{
						if(DragItem.item.classType == ItemClass.ITEM_CLASS_CONTAINER && 
						   itemButton.item.bag == 255 && itemButton.item.slot != 255 && 
						   (itemButton.item.entry == 0 || (itemButton.item as Bag).IsEmpty()))
						{
							byte bag = itemButton.item.bag;
							byte slot = itemButton.item.slot;
							Mng.PlayerScript.itemManager.MoveToSlot(DragItem.item, slot, bag);
						}
						else
						{
							byte bag = itemButton.item.slot;
							byte slot = Mng.PlayerScript.itemManager.GetFirstEmptySlotInBag(itemButton.item.GetBagBySlot());
							Mng.PlayerScript.itemManager.MoveToSlot(DragItem.item, slot, bag);
						}
					}
					else
					{
						byte bag = itemButton.item.slot;
						byte slot = Mng.PlayerScript.itemManager.GetFirstEmptySlotInBag(itemButton.item.GetBagBySlot());
						Mng.PlayerScript.itemManager.MoveToSlot(DragItem.item, slot, bag);
					}
					return;
				}

				itemButton = Mng.IsTouchInSlotsContainer(touchPosition);
				if(itemButton != null)
				{
					byte bag = (byte)itemButton.item.GetSlotByBag();
					byte slot = itemButton.item.slot;
					Mng.PlayerScript.itemManager.MoveToSlot(DragItem.item, slot, bag);
					return;
				}
				
				itemButton = Mng.IsTouchInBagContainer(touchPosition);
				if(itemButton != null)
				{
					if(DragItem.item.classType == ItemClass.ITEM_CLASS_CONTAINER && 
					   itemButton.item.bag == 255 && itemButton.item.slot != 255 && 
					   (itemButton.item.entry == 0 || (itemButton.item as Bag).IsEmpty()))
					{
						byte bag = 255;
						byte slot = itemButton.item.slot;
						Mng.PlayerScript.itemManager.MoveToSlot(DragItem.item, slot, bag);
					}
					else
					{
						byte bag = 255;
						byte slot = Mng.PlayerScript.itemManager.GetFirstEmptySlotInBankBag();
						if((itemButton.item as Bag) != null)
						{
							bag = (itemButton.item as Bag).slot;
							slot = (itemButton.item as Bag).GetFirstEmptySlot();
						}
						Mng.PlayerScript.itemManager.MoveToSlot(DragItem.item, slot, bag);
					}
					return;
				}
			}
		}
	}
}