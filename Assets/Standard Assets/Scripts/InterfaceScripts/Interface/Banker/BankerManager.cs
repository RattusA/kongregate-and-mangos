﻿using UnityEngine;
using System.Collections.Generic;

public class BankerManager {

	public Dictionary<int, ItemButton> InventoryBags = new Dictionary<int, ItemButton>();
	public Dictionary<int, ItemButton> BagSlots = new Dictionary<int, ItemButton>();
	public Dictionary<int, ItemButton> ClosedSlots = new Dictionary<int, ItemButton>();
	public List<ulong> BlockedItems = new List<ulong>();
	public int BagSlotCount = 28;
	public byte currentBag = 255;
	
	public Player PlayerScript;
	public ulong BankerGuid;
	
	public void InitBanker()
	{
		int firstBagID = (int)BankBagSlots.BANK_SLOT_BAG_START;
		int lastBagID = (int)BankBagSlots.BANK_SLOT_BAG_END;
		
		InventoryBags.Clear();
		ItemButton bagButton = new ItemButton();
		bagButton.item = new Item();
		bagButton.item.bag = 255;
		bagButton.item.slot = 255;
		bagButton.position = new Rect(Screen.width * 0.25f - Screen.height * 0.1375f, Screen.height * 0.02f, 
		                              Screen.height * 0.105f, Screen.height * 0.10f);
		InventoryBags[0] = bagButton;
		
		for(int bagSlotIndex = firstBagID; bagSlotIndex < lastBagID; bagSlotIndex++)
		{
			bagButton = new ItemButton();
			int index = bagSlotIndex - (int)BankBagSlots.BANK_SLOT_BAG_START + 1;
			int indexValue = (int)UpdateFields.EUnitFields.PLAYER_FIELD_INV_SLOT_HEAD + bagSlotIndex * 2;
			ulong bagGUID = PlayerScript.GetUInt64Value(indexValue);
			if(bagGUID > 0)
			{
				Item bagItem = PlayerScript.itemManager.GetItemByGuid(bagGUID);
				if(bagItem != null)
				{
					bagButton.item = bagItem;
					
					string iconName = DefaultIcons.GetItemIcon(bagItem);
					iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
					bagButton.texture = (Texture)Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
				}
			}
			else
			{
				bagButton.item = new Item();
				bagButton.item.bag = 255;
				bagButton.item.slot = (byte)bagSlotIndex;
			}

			float xPos;
			if(index < 3)
			{
				xPos = Screen.width * 0.25f - Screen.height * 0.1375f + (Screen.height * 0.11f * index);
				bagButton.position = new Rect(xPos, Screen.height * 0.02f,
				                              Screen.height * 0.105f, Screen.height * 0.10f);
			}
			else
			{
				xPos = Screen.width * 0.25f - Screen.height * 0.2475f + (Screen.height * 0.11f * (index - 3));
				bagButton.position = new Rect(xPos, Screen.height * 0.125f,
				                              Screen.height * 0.105f, Screen.height * 0.10f);
			}
			
			InventoryBags[index] = bagButton;
		}
		ShowBagSlots(currentBag);
	}
	
	public void ShowBagSlots(int bagIndex)
	{
		if(bagIndex == 0 || bagIndex == 255)
		{
			BagSlots.Clear();
			BagSlotCount = 28;
			currentBag = 255;
			for(int slotIndex = 0; slotIndex < BagSlotCount; slotIndex++)
			{
				int indexValue = (int)UpdateFields.EUnitFields.PLAYER_FIELD_BANK_SLOT_1 + slotIndex * 2;
				ulong itemGUID = PlayerScript.GetUInt64Value(indexValue);
				ItemButton itemButton = new ItemButton();
				if(itemGUID > 0)
				{
					Item item = PlayerScript.itemManager.GetItemByGuid(itemGUID);
					if(item != null)
					{
						itemButton.item = item;
						
						string iconName = DefaultIcons.GetItemIcon(item);
						iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
						itemButton.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
					}
					else
					{
						itemButton.item = new Item();
						itemButton.item.bag = currentBag;
						itemButton.item.slot = (byte)(slotIndex + 39);
					}
				}
				else
				{
					itemButton.item = new Item();
					itemButton.item.bag = currentBag;
					itemButton.item.slot = (byte)(slotIndex + 39);
				}

				itemButton.item.slotType = SlotType.SLOTTYPE_MAIN_BANK_INVENTORY;
				itemButton.position = GetItemPosition(slotIndex);
				BagSlots[slotIndex] = itemButton;
			}
		}
		else
		{
			Bag selectedBag;
			PlayerScript.itemManager.BankBags.TryGetValue(bagIndex, out selectedBag);
			if(selectedBag == null)
			{
				return;
			}
			
			currentBag = (byte)bagIndex;
			BagSlots.Clear();
			BagSlotCount = selectedBag.GetCapacity();
			for(int slotIndex = 0; slotIndex < BagSlotCount; slotIndex++)
			{
				Item item = selectedBag.GetItemFromSlot(slotIndex);
				ItemButton itemButton = new ItemButton();
				
				if(item != null)
				{
					itemButton.item = item;
					
					string iconName = DefaultIcons.GetItemIcon(item);
					iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
					itemButton.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
				}
				else
				{
					itemButton.item = new Item();
					itemButton.item.bag = currentBag;
					itemButton.item.slot = (byte)slotIndex;
				}

				itemButton.item.slotType = SlotType.SLOTTYPE_BANK_INVENTORY;
				itemButton.position = GetItemPosition(slotIndex);
				BagSlots[slotIndex] = itemButton;
			}
		}

		Texture blockedTexture = Resources.Load<Texture> ("Icons/fundal");

		for(int slotIndex = BagSlotCount; slotIndex < 28; slotIndex++)
		{
			ItemButton itemButton = new ItemButton();

			itemButton.texture = blockedTexture;
			itemButton.position = GetItemPosition(slotIndex);
			ClosedSlots[slotIndex] = itemButton;
		}
	}
	
	private Rect GetItemPosition(int index)
	{
		float xPos = (Screen.width * 0.25f - Screen.height * 0.26775f) + (index % 5) * Screen.height * 0.1175f;
		xPos += Screen.height * 0.1175f * (index / 25);
		float yPos = Screen.height * 0.2675f + (index / 5) * Screen.height * 0.11225f;
		
		return new Rect(xPos, yPos, Screen.height * 0.1175f, Screen.height * 0.11225f);
	}
	
	public ItemButton IsTouchInSlotsContainer(Vector2 touch)
	{
		foreach(KeyValuePair<int, ItemButton> itemButton in BagSlots)
		{
			if(itemButton.Value != null && itemButton.Value.position.Contains(touch))
			{
				return itemButton.Value;
			}
		}
		return null;
	}
	
	public ItemButton IsTouchInBagContainer(Vector2 touch)
	{
		foreach(KeyValuePair<int, ItemButton> itemButton in InventoryBags)
		{
			if(itemButton.Value != null && itemButton.Value.position.Contains(touch))
			{
				return itemButton.Value;
			}
		}
		return null;
	}

	public void GetBankSlotCost()
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_CHARACTER_BANK_NEXT_SLOT_COST);
		RealmSocket.outQueue.Add(pkt);
	}

	public void SendBuyBankSlot(bool state)
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_BUY_BANK_SLOT);
		pkt.Add(BankerGuid);
		RealmSocket.outQueue.Add(pkt);
	}
}
