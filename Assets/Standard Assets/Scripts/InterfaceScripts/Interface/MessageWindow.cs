﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum MessageWindowType {

	ONE_BUTTON_WINDOW,
	TWO_BUTTON_WINDOW,
	THREE_BUTTON_WINDOW,
};

public class MessageWindow : MonoBehaviour {

	[SerializeField]
	private GUISkin _skin;

	protected byte FIRST_FUNCTION = 0;
	protected byte SECOND_FUNCTION = 1;
	protected byte THIRD_FUNCTION = 2;

	private List<string> _functionList = new List<string>();
	private MessageWindowType _wndType = MessageWindowType.ONE_BUTTON_WINDOW;

	private string _messageText = "test test test test test test test test test test test test test test test test test";
	private string _yesButtonText = "Yes";
	private string _noButtonText = "No";

	public event Action<bool> onOkButton;
	public event Action<bool> onCancelButton;

	public void Awake()
	{
		_skin.button.fontSize = (int)(Screen.height * 0.055f);
		_skin.label.fontSize = (int)(Screen.height * 0.065f);
	}

	public void OnDisable()
	{
		onOkButton = null;
		onCancelButton = null;
	}

	public void InitOneButtonWindow(string val)
	{
		onOkButton = null;

		_wndType = MessageWindowType.ONE_BUTTON_WINDOW;
		_messageText = val;
	}

	public void InitTwoButtonWindow(string messageText, string yesButtonText, string noButtonText)
	{
		onOkButton = null;
		onCancelButton = null;

		gameObject.SetActive(true);
		_wndType = MessageWindowType.TWO_BUTTON_WINDOW;
		_messageText = messageText;

		_yesButtonText = yesButtonText;
		_noButtonText = noButtonText;
	}

	public void DrawGUI()
	{
		GUI.skin = _skin;

		switch(_wndType)
		{
		case MessageWindowType.ONE_BUTTON_WINDOW:
			GUI.ModalWindow(1, new Rect(0, 0, Screen.width, Screen.height), DrawOneButtonMessageWindow, "");
			break;
		case MessageWindowType.TWO_BUTTON_WINDOW:
			GUI.ModalWindow(2, new Rect(0, 0, Screen.width, Screen.height), DrawTwoButtonMessageWindow, "");
			break;
		}
	}

	private void DrawOneButtonMessageWindow(int index)
	{
		float SW = Screen.width;
		float SH = Screen.height;

		GUI.Box(new Rect(SW * 0.25f, SH * 0.325f, SW * 0.5f, SH * 0.35f), "");
		GUI.Label(new Rect(SW * 0.26f, SH * 0.565f, SW * 0.14f, SH * 0.09f), "", _skin.GetStyle("LeftDecoration"));
		GUI.Label(new Rect(SW * 0.6f, SH * 0.565f, SW * 0.14f, SH * 0.09f), "", _skin.GetStyle("RightDecoration"));

		if(GUI.Button(new Rect(SW * 0.4f, SH * 0.585f, SW * 0.2f, SH * 0.07f), "Close"))
		{
			this.gameObject.SetActive(false);
		}
		
		GUI.Label(new Rect(SW * 0.27f, SH * 0.37f, SW * 0.46f, SH * 0.17f), _messageText);
	}

	private void DrawTwoButtonMessageWindow(int index)
	{
		float SW = Screen.width;
		float SH = Screen.height;
		
		GUI.Box(new Rect(SW * 0.2f, SH * 0.30f, SW * 0.6f, SH * 0.4f), "");
		
		if(GUI.Button(new Rect(SW * 0.25f, SH * 0.575f, SW * 0.2f, SH * 0.08f), _yesButtonText))
		{
			if(onOkButton != null)
			{
				onOkButton(true);
			}
			this.gameObject.SetActive(false);
		}

		if(GUI.Button(new Rect(SW * 0.55f, SH * 0.575f, SW * 0.2f, SH * 0.08f), _noButtonText))
		{
			if(onCancelButton != null)
			{
				onCancelButton(false);
			}
			this.gameObject.SetActive(false);
		}
		
		GUI.Label(new Rect(SW * 0.22f, SH * 0.32f, SW * 0.56f, SH * 0.22f), _messageText);
	}
}
