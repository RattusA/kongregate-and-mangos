﻿using UnityEngine;
using System.Collections;

public class BeastLoreWindow : MonoBehaviour {

	[SerializeField]
	private GUISkin _skin;

	private string _beastInfoText = "";

	public void Awake()
	{
		_skin.button.fontSize = (int)(Screen.height * 0.055f);
		_skin.label.fontSize = (int)(Screen.height * 0.045f);
	}

	public void OnDisable()
	{
		_beastInfoText = "";
	}

	public void ShowBeastInfoWindow(string beastInfoText)
	{
		_beastInfoText = beastInfoText;
		gameObject.SetActive(true);
	}

	public void DrawGUI()
	{
		GUI.skin = _skin;
		float SW = Screen.width;
		float SH = Screen.height;

		GUI.Box(new Rect(SW * 0.25f, SH * 0.25f, SW * 0.5f, SH * 0.5f), "");
		GUI.Label(new Rect(SW * 0.275f, SH * 0.275f, SW * 0.45f, SH * 0.38f), _beastInfoText);

		if(GUI.Button(new Rect(SW * 0.4f, SH * 0.66f, SW * 0.2f, SH * 0.07f), "Close"))
		{
			gameObject.SetActive(false);
		}
	}
}
