﻿using UnityEngine;
using System.Collections;

public class MainMenuTopPanel : MonoBehaviour
{
	public void OnShopButtonClick()
	{
		Debug.Log("Shop");
	}

	public void OnBackButtonClick()
	{
		Debug.Log("Back");
	}

	public void OnInventoryButtonClick()
	{
		Debug.Log("Inventory");
	}

	public void OnQuestButtonClick()
	{
		Debug.Log("Quest");
	}

	public void OnSpellBookButtonClick()
	{
		Debug.Log("Spell Book");
	}

	public void OnExtraButtonClick()
	{
		Debug.Log("Extra");
	}

	public void OnSocialpButtonClick()
	{
		Debug.Log("Social");
	}

	public void OnOptionsButtonClick()
	{
		Debug.Log("Options");
	}

	public void OnExitButtonClick()
	{
		Debug.Log("Exit");
	}
}
