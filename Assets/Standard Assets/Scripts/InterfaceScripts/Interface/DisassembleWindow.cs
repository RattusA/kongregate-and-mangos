using UnityEngine;
using System.Collections;

public enum DisassembleWindowType {

	ENCHANTINGE_WINDOW_TYPE,
	JEWELCRAFTING_WINDOW_TYPE,
	INSCRIPTION_WINDOW_TYPE
}

public class DisassembleWindow : MonoBehaviour {

	public GUISkin Skin;
	public DisassembleManager Mng;
	public InventoryWindowBase InventoryWnd;

	private float SW;
	private float SH;

	public void Awake()
	{
		SW = Screen.width;
		SH = Screen.height;
		int padding = (int)(SH * 0.025f);
		
		Skin.label.fontSize = (int)(Screen.height * 0.07f);
		Skin.button.fontSize = (int)(Screen.height * 0.05f);
		
		Skin.GetStyle("Description").fontSize = (int)(Screen.height * 0.04f);
		Skin.GetStyle("Description").padding = new RectOffset(padding, padding, padding, padding);
		Skin.GetStyle("ItemInfo").fontSize = (int)(Screen.height * 0.04f);
	}

	public void SetWindowType(DisassembleWindowType windowType)
	{
		Mng = new DisassembleManager();
		Mng.PlayerScript = InterfaceManager.Instance.PlayerScript;
		Mng.InitDataByWindowType(windowType);
	}

	public void DrawGUI()
	{
		GUI.skin = Skin;
		InventoryWnd.DrawGUI();

		GUI.Box(new Rect(SW * 0.01f, SH * 0.15f, SW * 0.53f, SH * 0.7f), "");
		GUI.Label(new Rect(SW * 0.02f, SH * 0.17f, SW * 0.46f, SH * 0.1f), Mng.WindowName);

		GUI.Box(new Rect(SW * 0.03f, SH * 0.27f, SW * 0.49f, SH * 0.22f), Mng.DescriptionText, Skin.GetStyle("Description"));

		if(Mng.ReagentItem.texture != null)
		{
			GUI.DrawTexture(Mng.ReagentItem.position, Mng.ReagentItem.texture);
		}
		GUI.Label(new Rect(SW * 0.04f, SH * 0.5f, SH * 0.175f, SH * 0.175f), "", Skin.GetStyle("IconFrame"));

		if(!string.IsNullOrEmpty(Mng.ItemName))
		{
			GUI.Label(new Rect(SW * 0.04f + SH * 0.2f, SH * 0.5f, SW * 0.47f - SH * 0.2f, SH * 0.175f), Mng.ItemName,
			          Skin.GetStyle("ItemInfo"));
		}

		if(GUI.Button(new Rect(SW * 0.04f, SH * 0.74f, SW * 0.22f, SH * 0.08f), "Disassemble"))
		{
			if(Mng.Disassemble())
			{
				GameObject.Destroy(this.gameObject);
				InterfaceManager.Instance.hideInterface = false;
			}
		}

		if(GUI.Button(new Rect(SW * 0.29f, SH * 0.74f, SW * 0.22f, SH * 0.08f), "Close"))
		{
			GameObject.Destroy(this.gameObject);
			InterfaceManager.Instance.hideInterface = false;
		}

		if(IsDragBegin)
		{
			Rect dragRect = DragItem.position;
			dragRect.center = new Vector2(Input.mousePosition.x, SH - Input.mousePosition.y);
			GUI.DrawTexture(dragRect, DragItem.texture);
		}
	}

	private ItemButton DragItem;
	private bool IsDragBegin = false;
	
	public void LateUpdate()
	{
		ItemButton itemButton;
		Vector2 touchPosition = new Vector2(Input.mousePosition.x, SH - Input.mousePosition.y);
		if(Input.GetMouseButtonDown(0))
		{
			itemButton = InventoryWnd.mng.IsTouchInSlotsContainer(touchPosition);
			if(itemButton != null && itemButton.texture != null && 
			   !InventoryWnd.mng.blockedItems.Contains(itemButton.item.guid))
			{
				DragItem = itemButton;
				IsDragBegin = true;
				return;
			}

			itemButton = Mng.IsTouchInReagentContainer(touchPosition);
			if(itemButton != null)
			{
				DragItem = Mng.ReagentItem;
				IsDragBegin = true;
				return;
			}
		}
		
		if(IsDragBegin)
		{
			if(Input.GetMouseButtonUp(0))
			{
				IsDragBegin = false;
				if(DragItem.position.Contains(touchPosition))
				{
					return;
				}
				
				itemButton = InventoryWnd.mng.IsTouchInSlotsContainer(touchPosition);
				if(itemButton != null)
				{
					if(itemButton.item != null && InventoryWnd.mng.blockedItems.Contains(itemButton.item.guid))
					{
						InventoryWnd.mng.blockedItems.Clear();
						Mng.ReagentItem.texture = null;
						Mng.ReagentItem.item = null;
					}
					return;
				}

				itemButton = Mng.IsTouchInReagentContainer(touchPosition);
				if(itemButton != null)
				{
					if(Mng.InsertItemToSlot(DragItem.item))
					{
						Mng.InsertItemToSlot(DragItem.item);
						InventoryWnd.mng.blockedItems.Clear();
						InventoryWnd.mng.blockedItems.Add(DragItem.item.guid);
					}
					return;
				}
			}
		}
	}
}
