using UnityEngine;
using System.Collections;

public class CraftProfessionListWindow : MonoBehaviour
{
	public CraftInterfaceController parent;

	private bool _uhowUnlearnWindow = false;
	private uint _unlearnSkillID = 0;
	private string _craftName = "";

	private float _dpi;
	private const float _DEFAULT_DPI = 72;

	private Rect _modalWindowRect;

	private Vector2 _scrollPosition;
	private Rect _scrollZone;
	private Rect _fullScrollZone;

	private Rect _topDecorationRect;
	private Rect _mainCraftLabelRect;

	private Rect _unlearnWindowRect;
	private Rect _unlearnYesButtonRect;
	private Rect _unlearnNoButtonRect;
	private Rect _unlearnMessageLabelRect;

	public void Awake()
	{
		_dpi = (Screen.dpi > 0) ? Screen.dpi : _DEFAULT_DPI;
	}

	public void Start()
	{
		float SW = Screen.width;
		float SH = Screen.height;

		parent.skin.button.fontSize = (int)(SH * 0.055f);
		parent.skin.GetStyle("ProfessionListTitleText").fontSize = (int)(SH * 0.065f);
		parent.skin.GetStyle("ProfessionListCraftNameText").fontSize = (int)(SH * 0.06f);
		parent.skin.GetStyle("ProfessionListCraftValueText").fontSize = (int)(SH * 0.06f);
		parent.skin.GetStyle("ProfessionListUnlearnMessageText").fontSize = (int)(SH * 0.05f);

		_modalWindowRect = new Rect(0, 0, SW, SH);

		_scrollPosition = Vector2.zero;
		_scrollZone = new Rect(SW * 0.01f, SH * 0.24f, SW * 0.98f, SH * 0.76f);
		_fullScrollZone = new Rect(0, 0, SW * 0.98f, SH * 0.76f);

		_topDecorationRect = new Rect(SW * 0.01f, SH * 0.105f, SW * 0.99f, SH * 0.12f);
		_mainCraftLabelRect = new Rect(SW * 0.09f, SH * 0.135f, SW * 0.82f, SH * 0.1f);

		_unlearnWindowRect = new Rect(SW * 0.25f, SH * 0.35f, SW * 0.5f, SH * 0.3f);
		_unlearnYesButtonRect = new Rect(SW * 0.3f, SH * 0.56f, SW * 0.15f, SH * 0.07f);
		_unlearnNoButtonRect = new Rect(SW * 0.55f, SH * 0.56f, SW * 0.15f, SH * 0.07f);
		_unlearnMessageLabelRect = new Rect(SW * 0.27f, SH * 0.37f, SW * 0.46f, SH * 0.17f);
	}

	public void OnDisable()
	{
		parent.professionListWindow = null;
		parent = null;
	}

	public void DrawGUI()
	{
		GUI.Label(_topDecorationRect, "", parent.skin.GetStyle("ProfessionListTopDecoration"));
		GUI.Label(_mainCraftLabelRect, "MAIN CRAFTS", parent.skin.GetStyle("ProfessionListTitleText"));

		_scrollPosition = GUI.BeginScrollView(_scrollZone, _scrollPosition, _fullScrollZone, GUIStyle.none, GUIStyle.none);
			DrawProfessionInfo();
		GUI.EndScrollView();

		if(_uhowUnlearnWindow)
		{
			GUI.ModalWindow(1, _modalWindowRect, ShowUnlearnRequest, "");
		}
	}

	private void DrawProfessionInfo()
	{
		float SW = Screen.width;
		float SH = Screen.height;

		int pos = 0;
		float padding = 0;

		if(InterfaceManager.Instance.PlayerScript.craftManager.mainCraftProfession.Count == 0)
		{
			Rect noCraftLabel = new Rect(SW * 0.1f, 0, SW * 0.45f, SH * 0.08f);
			GUI.Label(noCraftLabel, "No main crafts.", parent.skin.GetStyle("ProfessionListCraftNameText"));
			pos++;
		}
		else
		{
			foreach(CraftProfession craft in InterfaceManager.Instance.PlayerScript.craftManager.mainCraftProfession)
			{
				if(craft == null)
				{
					continue;
				}
				padding = SH * 0.12f * pos;

				Rect unlearnCraftButtonRect = new Rect(0, padding, SW * 0.07f, SW * 0.07f);
				Rect craftNameLabelRect = new Rect(SW * 0.1f, padding + SH * 0.005f, SW * 0.45f, SH * 0.08f);
				Rect craftSkillValueLabelRect = new Rect(SW * 0.5f, padding + SH * 0.005f, SW * 0.2f, SH * 0.08f);

				if(GUI.Button(unlearnCraftButtonRect, "", parent.skin.GetStyle("ProfessionListUnlearnButton")))
				{
					_craftName = craft.professionName;
					_unlearnSkillID = craft.skillID;
					_uhowUnlearnWindow = true;
				}

				GUI.Label(craftNameLabelRect, craft.professionName, parent.skin.GetStyle("ProfessionListCraftNameText"));
				GUI.Label(craftSkillValueLabelRect, craft.skillValue, parent.skin.GetStyle("ProfessionListCraftValueText"));
				
				if(craft.spellList.Count > 0)
				{
					Rect craftNowButtonRect = new Rect(SW * 0.68f, padding + SH * 0.01f, SW * 0.28f, SH * 0.085f);
					if(GUI.Button(craftNowButtonRect, "", parent.skin.GetStyle("ProfessionListCraftNowButton")))
					{
						InterfaceManager.Instance.PlayerScript.craftManager.selectedProfession = craft;
						parent.SetWindowState(CraftMenuWindowState.CRAFT_WINDOW_STATE);
					}
				}
				pos++;
			}
		}

		if(InterfaceManager.Instance.PlayerScript.craftManager.secondaryCraftProfession.Count > 0)
		{
			padding = SH * 0.12f * pos;

			Rect secondaryCraftDecorationRect = new Rect(0, padding, SW * 0.98f, SH * 0.05f);
			Rect secondaryCraftLabelRect = new Rect(SW * 0.08f, padding + SH * 0.05f, SW * 0.82f, SH * 0.1f);

			GUI.Label(secondaryCraftDecorationRect, "", parent.skin.GetStyle("ProfessionListMiddleDecoration"));
			GUI.Label(secondaryCraftLabelRect, "SECONDARY CRAFTS", parent.skin.GetStyle("ProfessionListTitleText"));

			pos++;

			foreach(CraftProfession craft in InterfaceManager.Instance.PlayerScript.craftManager.secondaryCraftProfession)
			{
				if(craft == null)
				{
					continue;
				}
				padding = SH * 0.12f * pos;

				Rect craftDecorationBoxRect = new Rect(0, padding, SW * 0.07f, SW * 0.07f);
				Rect craftNameLabelRect = new Rect(SW * 0.1f, padding + SH * 0.005f, SW * 0.45f, SH * 0.08f);
				Rect craftSkillValueLabelRect = new Rect(SW * 0.5f, padding + SH * 0.005f, SW * 0.2f, SH * 0.08f);

				GUI.Label(craftDecorationBoxRect, "", parent.skin.GetStyle("ProfessionListUnlearnDecoration"));
				GUI.Label(craftNameLabelRect, craft.professionName, parent.skin.GetStyle("ProfessionListCraftNameText"));
				GUI.Label(craftSkillValueLabelRect, craft.skillValue, parent.skin.GetStyle("ProfessionListCraftValueText"));
				
				if(craft.spellList.Count > 0)
				{
					Rect craftNowButtonRect = new Rect(SW * 0.68f, padding + SH * 0.01f, SW * 0.28f, SH * 0.085f);
					if(GUI.Button(craftNowButtonRect, "", parent.skin.GetStyle("ProfessionListCraftNowButton")))
					{
						InterfaceManager.Instance.PlayerScript.craftManager.selectedProfession = craft;
						parent.SetWindowState(CraftMenuWindowState.CRAFT_WINDOW_STATE);
						Destroy(this.gameObject);
						return;
					}
				}
				pos++;
			}
		}
		_fullScrollZone = new Rect(0, 0, Screen.width * 0.98f, SH * 0.12f * pos);
	}

	public void Update()
	{
		if(Input.touchCount > 0)
		{
			Touch touch = Input.touches[0];
			if(touch.phase == TouchPhase.Moved && _scrollZone.Contains(touch.position))
			{
				_scrollPosition.y += touch.deltaPosition.y * (_dpi / _DEFAULT_DPI);
			}
		}
	}
	
	private void ShowUnlearnRequest(int index)
	{
		GUI.Box(_unlearnWindowRect, "", parent.skin.GetStyle("ModalWindowBackground"));
		GUI.Label(_unlearnMessageLabelRect, "Do you want unlearn " + _craftName + " craft?", parent.skin.GetStyle("ProfessionListUnlearnMessageText"));

		if(GUI.Button(_unlearnYesButtonRect, "Yes", parent.skin.button))
		{
			InterfaceManager.Instance.PlayerScript.craftManager.UnlearnSkill(_unlearnSkillID);
			_uhowUnlearnWindow = false;
		}

		if(GUI.Button(_unlearnNoButtonRect, "No", parent.skin.button))
		{
			_uhowUnlearnWindow = false;
		}
	}
}