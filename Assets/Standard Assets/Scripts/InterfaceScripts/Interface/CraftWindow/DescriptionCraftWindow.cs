using UnityEngine;
using System.Collections;

public class DescriptionCraftWindow : MonoBehaviour {

	[SerializeField]
	private Font _font;

	protected TexturePacker HUDToolkit = null;
	protected float SW = Screen.width;
	protected float SH = Screen.height;

	private CraftRecipeListWindow _craftWindow;
	public CraftRecipeListWindow craftWindow {
		set { _craftWindow = value; }
	}

	private GUIStyle GreenTextStyle;
	private GUIStyle BrownTextStyle;

	void Awake()
	{
		GreenTextStyle = GUIFunctions.CreateGuiStyle(_font, TextAnchor.MiddleLeft, (int)(Screen.height * 0.06), 
		                                             new Color(0, 0.4f, 0, 1.0f), true, FontStyle.Bold);
		BrownTextStyle = GUIFunctions.CreateGuiStyle(_font, TextAnchor.MiddleCenter, (int)(Screen.height * 0.06), 
		                                              new Color(0.2f, 0.12f, 0, 1.0f), true, FontStyle.Bold);
	}
	 
	public void InitData(TexturePacker HUDToolkit)
	{
		SW = Screen.width;
		SH = Screen.height;
		this.HUDToolkit = HUDToolkit;
	}

	public void DrawGUI()
	{
		HUDToolkit.DrawTexture(new Rect(SW * 0.07f, SH * 0.15f, SW * 0.09f, SW * 0.09f), "IconBackground.png");
		GUI.Label(new Rect(SW * 0.17f, SH * 0.15f + SW * 0.03f, SW * 0.6f, SW * 0.03f), "Test Description Name", GreenTextStyle);
		HUDToolkit.DrawTexture(new Rect(SW * 0.02f, SH * 0.15f + SW * 0.07f, SW * 0.71f, SW * 0.065f), "Decoration.png");
		GUI.Label(new Rect(SW * 0.1f, SH * 0.16f + SW * 0.12f, SW * 0.5f, SW * 0.03f), "Description", BrownTextStyle);
	}
}
