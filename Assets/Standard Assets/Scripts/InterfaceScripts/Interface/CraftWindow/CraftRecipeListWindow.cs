using UnityEngine;
using System.Collections;

public enum CraftWindowState
{
	CRAFT_WINDOW_STATE,
	SEARCH_WINDOW_STATE,
	DESCRIPTION_WINDOW_SATE,
	SORT_WINDOW_STATE
}

public class CraftRecipeListWindow : MonoBehaviour
{
	public CraftInterfaceController parent;

	private CraftManager _craftManager;
	private DescriptionWindow DescriptionWnd;
	private ModalInfoWindow ModalInfoWindowState;

	private string _craftName = "";
	private string _searchText = string.Empty;
	private string SortText = "All Spots";
	private int SelectedIndex = -1;
	private CraftWindowState _windowState = CraftWindowState.CRAFT_WINDOW_STATE;

	private float _dpi;
	private const float _DEFAULT_DPI = 72;

	private bool _onlyWithMats = false;

	private Vector2 _scrollPosition;
	private Rect _scrollZone;
	private Rect _fullScrollZone;

	private Rect _modalWindowRect;

	private Rect _searchButtonRect;
	private Rect _dropMenuButtonRect;
	private Rect _checkBoxButtonRect;

	private Rect _descriptionWindowRect;
	private Rect _topLeftDecorationRect;
	private Rect _topRightDecorationRect;
	private Rect _professionNameLabelRect;
	private Rect _scrollUpButtonRect;
	private Rect _scrollDownButtonRect;

	private Rect _searchWindowRect;
	private Rect _searchTextBoxRect;
	private Rect _searchWindowSearchButtonRect;
	private Rect _searchWindowClearButtonRect;
	private Rect _searchWindowCloseButtonRect;

	private Rect _sortWindowRect;
	private Rect _sortWindowSelectButtonRect;

	public void Awake()
	{
		_craftManager = InterfaceManager.Instance.PlayerScript.craftManager;
		_craftName = _craftManager.selectedProfession.professionName + " Craft";

		_dpi = (Screen.dpi > 0) ? Screen.dpi : _DEFAULT_DPI;
	}

	public void Start()
	{
		float SW = Screen.width;
		float SH = Screen.height;

		parent.skin.textField.fontSize = (int)(SH * 0.065f);
		parent.skin.GetStyle("RecipeListRecipeButton").fontSize = (int)(SH * 0.055f);
		parent.skin.GetStyle("RecipeListDarkButton").fontSize = (int)(SH * 0.055f);
		parent.skin.GetStyle("RecipeListDropMenuButton").fontSize = (int)(SH * 0.055f);
		parent.skin.GetStyle("RecipeListDropMenuButton").contentOffset = new Vector2(0, (int)(-SH * 0.01f));
		parent.skin.GetStyle("RecipeListCheckBox").fontSize = (int)(SH * 0.055f);
		parent.skin.GetStyle("RecipeListCheckBox").contentOffset = new Vector2((int)(SW * 0.09f), 0);
		parent.skin.GetStyle("RecipeListCheckBox").fixedWidth = (int)(SW * 0.09f);
		parent.skin.GetStyle("RecipeListSortCheckBox").fontSize = (int)(SH * 0.06f);
		parent.skin.GetStyle("RecipeListSortCheckBox").contentOffset = new Vector2((int)(SW * 0.09f), 0);
		parent.skin.GetStyle("RecipeListSortCheckBox").fixedWidth = (int)(SW * 0.07f);
		
		_scrollPosition = Vector2.zero;
		_scrollZone = new Rect(SW * 0.05f, SH * 0.39f, SW * 0.83f, SH * 0.58f);
		_fullScrollZone = new Rect(0, 0, SW * 0.81f, SH * 0.6f);
		
		_modalWindowRect = new Rect(0, 0, SW, SH);

		_searchButtonRect = new Rect(SW * 0.01f, SH * 0.11f, SW * 0.24f, SH * 0.085f);
		_dropMenuButtonRect = new Rect(SW * 0.26f, SH * 0.11f, SW * 0.35f, SH * 0.095f);
		_checkBoxButtonRect = new Rect(SW * 0.62f, SH * 0.11f, SW * 0.38f, SH * 0.085f);

		_descriptionWindowRect = new Rect(SW * 0.01f, SH * 0.2f, SW * 0.98f, SH * 0.79f);
		_topLeftDecorationRect = new Rect(SW * 0.03f, SH * 0.23f, SW * 0.15f, SW * 0.15f);
		_topRightDecorationRect = new Rect(SW * 0.82f, SH * 0.23f, SW * 0.15f, SW * 0.15f);
		_professionNameLabelRect = new Rect(SW * 0.15f, SH * 0.29f, SW * 0.67f, SH * 0.08f);

		_scrollUpButtonRect = new Rect(SW * 0.89f, SH * 0.39f, SW * 0.05f, SW * 0.04f);
		_scrollDownButtonRect = new Rect(SW * 0.89f, SH * 0.89f, SW * 0.05f, SW * 0.04f);

		_searchWindowRect = new Rect(SW * 0.01f, SH * 0.2f, SW * 0.6f, SH * 0.31f);
		_searchTextBoxRect = new Rect(SW * 0.035f, SH * 0.25f, SW * 0.5475f, SH * 0.085f);
		_searchWindowSearchButtonRect = new Rect(SW * 0.03f, SH * 0.36f, SW * 0.23f, SH * 0.1f);
		_searchWindowClearButtonRect = new Rect(SW * 0.36f, SH * 0.36f, SW * 0.23f, SH * 0.1f);
		_searchWindowCloseButtonRect = new Rect(SW * 0.36f, SH * 0.36f, SW * 0.23f, SH * 0.1f);

		_sortWindowRect = new Rect(SW * 0.01f, SH * 0.2f, SW * 0.6f, SH * 0.6f);
		_sortWindowSelectButtonRect = new Rect(SW * 0.2f, SH * 0.7f, SW * 0.2f, SH * 0.08f);
	}

	public void OnDisable()
	{
		parent.recipeListWindow = null;
		parent = null;

		_craftManager = null;
	}

	public void DrawGUI()
	{
		DrawTopPanel();

		GUI.Box(_descriptionWindowRect, "", parent.skin.GetStyle("DescriptionPlaneBackground"));
		GUI.Label(_topLeftDecorationRect, "", parent.skin.GetStyle("RecipeListTopLeftDecoration"));
		GUI.Label(_topRightDecorationRect, "", parent.skin.GetStyle("RecipeListTopRightDecoration"));
		GUI.Label(_professionNameLabelRect, _craftName, parent.skin.GetStyle("RecipeListCraftNameText"));

		if(GUI.RepeatButton(_scrollUpButtonRect, "", parent.skin.GetStyle("RecipeListScrollUp")))
		{
			ScrollUp();
		}

		if(GUI.RepeatButton(_scrollDownButtonRect, "", parent.skin.GetStyle("RecipeListScrollDown")))
		{
			ScrollDown();
		}

		DrawCraftSpellsList();
		switch(_windowState)
		{
		case CraftWindowState.SEARCH_WINDOW_STATE:
			GUI.ModalWindow(0, _modalWindowRect, ShowSearchWindow, "");
			break;
		case CraftWindowState.SORT_WINDOW_STATE:
			GUI.ModalWindow(1, _modalWindowRect, ShowSortWindow, "");
			break;
		case CraftWindowState.DESCRIPTION_WINDOW_SATE:
			GUI.ModalWindow(2, _modalWindowRect, DescriptionWnd.DrawGUI, "");
			break;
		}
	}

	private void DrawTopPanel()
	{
		if(GUI.Button(_searchButtonRect, "Search", parent.skin.GetStyle("RecipeListDarkButton")))
		{
			_windowState = CraftWindowState.SEARCH_WINDOW_STATE;
		}
		
		if(GUI.Button(_dropMenuButtonRect, SortText, parent.skin.GetStyle("RecipeListDropMenuButton")))
		{
			_windowState = CraftWindowState.SORT_WINDOW_STATE;
		}
		
		_onlyWithMats = GUI.Toggle(_checkBoxButtonRect, _onlyWithMats, "Only With Mats", parent.skin.GetStyle("RecipeListCheckBox"));
	}

	private void ScrollUp()
	{
		_scrollPosition.y -= Screen.height * 0.0333f;
	}
	
	private void ScrollDown()
	{
		_scrollPosition.y += Screen.height * 0.0333f;
	}

	public void Update()
	{
		if(Input.touchCount > 0)
		{
			Touch touch = Input.touches[0];
			if(touch.phase == TouchPhase.Moved && _scrollZone.Contains(touch.position))
			{
				_scrollPosition.y += touch.deltaPosition.y * (_dpi / _DEFAULT_DPI);
			}
		}
	}

	private void DrawCraftSpellsList()
	{
		_scrollPosition = GUI.BeginScrollView(_scrollZone, _scrollPosition, _fullScrollZone, GUIStyle.none, GUIStyle.none);
			DrawCraftSpells();
		GUI.EndScrollView();
	}

	private void DrawCraftSpells()
	{
		float SW = Screen.width;
		float SH = Screen.height;
		
		int pos = 0;
		
		for(int index = 0; index < _craftManager.selectedProfession.spellList.Count; index++)
		{
			float padding = SH * 0.09f * pos;
			CraftSpell craftSpell = _craftManager.selectedProfession.spellList[index];
			if((!string.IsNullOrEmpty(_searchText) && !craftSpell.itemName.Contains(_searchText)) ||
			   (_onlyWithMats && craftSpell.GetMaxCraftCount() == 0))
			{
				continue;
			}

			SpellEntry spellInfo = dbs.sSpells.GetRecord(_craftManager.selectedProfession.spellList[index].spellID);
			if(spellInfo == null)
			{
				continue;
			}

			string craftSpellName = GetFormatedCraftSpellName(craftSpell); 

			Rect buttonRect = new Rect(0, padding, _fullScrollZone.width * 0.98f, SH * 0.09f);
			bool state = (SelectedIndex == pos) ? true : false;
			if(GUI.Toggle(buttonRect, state, craftSpellName, parent.skin.GetStyle("RecipeListRecipeButton")))
			{
				SelectedIndex = pos;

				if(state)
				{
					SelectedIndex = -1;
					_craftManager.selectedCraftSpell = craftSpell;
					if(craftSpell.resultItemID > 0)
					{
						ItemInfoEvent(craftSpell.resultItemID );
					}
				}
			}
			pos++;
		}
		_fullScrollZone = new Rect(0, 0, SW * 0.81f, SH * 0.09f * pos);
	}

	private string GetFormatedCraftSpellName(CraftSpell craftSpell)
	{
		string ret = craftSpell.spellName;
		if(craftSpell.GetMaxCraftCount() > 0)
		{
			ret += " [" + craftSpell.GetMaxCraftCount() + "]";
		}
		
		if(_craftManager.selectedProfession.CurrentSkillValue <= craftSpell.yellowValue)
		{
			ret = "<color=#ff00ffff>" + ret + "</color>";
		}
		else 
		{
			uint greenValue = (craftSpell.yellowValue + craftSpell.grayValue) / 2;
			if(_craftManager.selectedProfession.CurrentSkillValue <= greenValue)
			{
				ret = "<color=blue>" + ret + "</color>";
			}
			else if(_craftManager.selectedProfession.CurrentSkillValue <= craftSpell.grayValue)
			{
				ret = "<color=green>" + ret + "</color>";
			}
			else
			{
				ret = "<color=#404040ff>" + ret + "</color>";
			}
		}
		return ret;
	}

	/**
	 * Search Window 
	 */

	private void ShowSearchWindow(int windowID)
	{
		if(GUI.Button(_searchButtonRect, "Search", parent.skin.GetStyle("RecipeListDarkButton")))
		{
			_windowState = CraftWindowState.CRAFT_WINDOW_STATE;
		}

		GUI.Box(_searchWindowRect, "", parent.skin.GetStyle("RecipeListSearchWindowBackground"));

		_searchText = GUI.TextField(_searchTextBoxRect, _searchText);
		
		if(GUI.Button(_searchWindowSearchButtonRect, "Search", parent.skin.button))
		{
			_windowState = CraftWindowState.CRAFT_WINDOW_STATE;
		}

		if(!string.IsNullOrEmpty(_searchText))
		{
			if(GUI.Button(_searchWindowClearButtonRect, "Clear", parent.skin.button))
			{
				_searchText = string.Empty;
			}
		}
		else if(GUI.Button(_searchWindowCloseButtonRect, "Cancel", parent.skin.button))
		{
			_windowState = CraftWindowState.CRAFT_WINDOW_STATE;
		}
	}

	private Vector2 _sortScrollPosition = Vector2.zero;
	private Rect _sortScrollZone = new Rect(Screen.width * 0.03f, Screen.height * 0.23f, 
	                                        Screen.width * 0.45f, Screen.height * 0.56f);
	private Rect _sortFullScrollZone = new Rect(0, 0, Screen.width * 0.48f, Screen.height * 0.56f);

	/**
	 * Sort Window
	 */

	private void ShowSortWindow(int windowID)
	{
		if(GUI.Button(_dropMenuButtonRect, SortText, parent.skin.GetStyle("RecipeListDropMenuButton")))
		{
			_windowState = CraftWindowState.CRAFT_WINDOW_STATE;
		}

		GUI.Box(_sortWindowRect, "", parent.skin.GetStyle("ModalWindowBackground"));

		_sortScrollPosition = GUI.BeginScrollView(_sortScrollZone, _sortScrollPosition, _sortFullScrollZone, GUIStyle.none, GUIStyle.none);
			DrawSortScrollZone();
		GUI.EndScrollView();

		if(GUI.Button(_sortWindowSelectButtonRect, "Select", parent.skin.button))
		{
			_windowState = CraftWindowState.CRAFT_WINDOW_STATE;
		}
	}

	private void DrawSortScrollZone()
	{
		float SW = Screen.width;
		float SH = Screen.height;

		int index = 0;
		for(index = 0; index < _craftManager.selectedProfession.sortList.Count; index++)
		{
			float padding = SH * 0.09f * index;
			Rect buttonRect = new Rect(SW * 0.03f, padding, SW * 0.6f, SH * 0.09f);
			bool state = (SortText == _craftManager.selectedProfession.sortList[index]) ? true : false;
			if(GUI.Toggle(buttonRect, state, _craftManager.selectedProfession.sortList[index], parent.skin.GetStyle("RecipeListSortCheckBox")))
			{
				SortText = _craftManager.selectedProfession.sortList[index];
			}
		}

		_sortFullScrollZone = new Rect(0, 0, Screen.width * 0.48f, SH * 0.09f * index);
	}

	public void ItemInfoEvent(uint itemGuid)
	{
		DescriptionWnd = new DescriptionWindow();
		Item item = AppCache.sItems.GetItem(itemGuid);
		ItemDisplayInfoEntry displayInfo = dbs.sItemDisplayInfo.GetRecord(item.displayInfoID);
		string iconName = displayInfo.InventoryIcon.Substring(0, displayInfo.InventoryIcon.LastIndexOf('.'));
		DescriptionWnd.IconTecture = Resources.Load<Texture>( "Icons/" + iconName );
		DescriptionWnd.TitleText = item.name;
		DescriptionWnd.DescriptionTextValue = item.GetItemBasicStatsOutName();

		InfoWindowButton closeButton = DescriptionWnd.ButtonManager.AddFindButton(InfoWindowButtonType.Close);
		closeButton.ButtonName = "Close";
		closeButton.onButtonUp += ClosePopupWindow;

		InfoWindowButton makeButton = DescriptionWnd.ButtonManager.AddFindButton(InfoWindowButtonType.Make);
		makeButton.ButtonName = "Make";
		makeButton.onButtonUp += MakeItemEvent;

		_windowState = CraftWindowState.DESCRIPTION_WINDOW_SATE;
	}
	
	public void ClosePopupWindow()
	{
		_windowState = CraftWindowState.CRAFT_WINDOW_STATE;
		DescriptionWnd = null;
	}

	public void MakeItemEvent()
	{
		parent.SetWindowState(CraftMenuWindowState.CRAFT_RECIEPT_WINDOW_STATE);
		DescriptionWnd = null;
		Destroy(this.gameObject);
	}
}