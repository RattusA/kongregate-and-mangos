﻿using UnityEngine;
using System.Collections;

public enum CraftMenuWindowState
{
	NONE,
	CRAFT_LIST_WINDOW_STATE,
	CRAFT_WINDOW_STATE,
	CRAFT_RECIEPT_WINDOW_STATE
}

public class CraftInterfaceController : MonoBehaviour
{
	public GUISkin skin;
	public ExtraMenuWindow parent;

	public CraftProfessionListWindow professionListWindow;
	public CraftRecipeListWindow recipeListWindow;
	public CraftProcessWindow processWindow;
	
	private CraftMenuWindowState _windowState;

	public void Awake()
	{
		InterfaceManager.Instance.PlayerScript.UpdateCraftInfo();

		skin = Resources.Load<GUISkin> ("GUI/CraftSkin");

		SetWindowState(CraftMenuWindowState.CRAFT_LIST_WINDOW_STATE);
	}

	public void OnDestroy()
	{
		parent.craftInterfaceController = null;
		parent = null;

		Resources.UnloadAsset(skin);
		skin = null;
	}

	public void DrawGUI()
	{
		switch(_windowState)
		{
		case CraftMenuWindowState.CRAFT_LIST_WINDOW_STATE:
			if(professionListWindow != null)
			{
				professionListWindow.DrawGUI();
			}
			break;
		case CraftMenuWindowState.CRAFT_WINDOW_STATE:
			if(recipeListWindow != null)
			{
				recipeListWindow.DrawGUI();
			}
			break;
		case CraftMenuWindowState.CRAFT_RECIEPT_WINDOW_STATE:
			if(processWindow != null)
			{
				processWindow.DrawGUI();
			}
			break;
		}
	}

	public void SetWindowState(CraftMenuWindowState newState)
	{
		GameObject go;
		switch(newState)
		{
		case CraftMenuWindowState.CRAFT_LIST_WINDOW_STATE:
			go = Instantiate<GameObject>(Resources.Load<GameObject> ("Interface Prefabs/CraftProfessionListWindow"));
			if(go != null)
			{
				professionListWindow = go.GetComponent<CraftProfessionListWindow>();
				if(professionListWindow == null)
				{
					Debug.LogError("CraftProfessionListWindow script missing.");
				}
				else
				{
					professionListWindow.transform.parent = this.transform;
					professionListWindow.parent = this;
				}
			}
			else
			{
				Debug.LogError("Can't load CraftProfessionListWindow prefab.");
			}
			break;
		case CraftMenuWindowState.CRAFT_WINDOW_STATE:
			InterfaceManager.Instance.PlayerScript.craftManager.UpdateCraftReagentsCount();
			go = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/CraftRecipeListWindow"));
			if(go != null)
			{
				recipeListWindow = go.GetComponent<CraftRecipeListWindow>();
				if(recipeListWindow == null)
				{
					Debug.LogError("CraftRecipeListWindow script missing.");
				}
				else
				{
					recipeListWindow.transform.parent = this.transform;
					recipeListWindow.parent = this;
				}
			}
			else
			{
				Debug.LogError("Can't load CraftRecipeListWindow prefab.");
			}
			break;
		case CraftMenuWindowState.CRAFT_RECIEPT_WINDOW_STATE:
			go = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/CraftProcessWindow"));
			if(go != null)
			{
				processWindow = go.GetComponent<CraftProcessWindow>();
				if(processWindow == null)
				{
					Debug.LogError("CraftProcessWindow script missing.");
				}
				else
				{
					processWindow.transform.parent = this.transform;
					processWindow.parent = this;
				}
			}
			else
			{
				Debug.LogError("Can't load CraftProcessWindow prefab.");
			}
			break;
		}

		go = null;
		_windowState = newState;
	}
}