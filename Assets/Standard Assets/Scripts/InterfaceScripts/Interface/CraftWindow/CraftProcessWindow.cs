using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CraftProcessWindow : MonoBehaviour
{
	public CraftInterfaceController parent;
	public InventoryWindowBase inventory;

	private CraftManager _craftManager;

	private Texture _resultItemTexture;
	private List<Texture> _reagentTextureList;

	private ItemButton _enchantTargetItem;
	private string _enchantTargetItemName;

	private int _craftCount = 1;

	private Vector2 _scrollPosition;
	private Rect _scrollZone;
	private Rect _fullScrollZone;

	private Rect _descriptionBackgroundRect;
	private Rect _resultItemTextureRect;
	private Rect _resultItemDecorationRect;
	private Rect _resultItemNameLabelRect;

	private Rect _decorationRect;
	private Rect _reagentsLabelRect;

	private Rect _makeButtonRect;
	private Rect _incrementButtonRect;
	private Rect _decrementButtonRect;
	private Rect _craftCountLabelRect;
	private Rect _makeAllButtonRect;
	private Rect _closeButtonRect;
	
	public void Awake()
	{
		_craftManager = InterfaceManager.Instance.PlayerScript.craftManager;
		Init();
	}

	public void Start()
	{
		float SW = Screen.width;
		float SH = Screen.height;
		
		parent.skin.GetStyle("ProcessWindowReagentNameText").fontSize = (int)(SH * 0.045f);
		parent.skin.GetStyle("ProcessWindowCraftCountText").fontSize = (int)(SH * 0.06f);
		parent.skin.GetStyle("ProcessWindowRecipeLabelText").fontSize = (int)(SH * 0.07f);
		
		_scrollPosition = Vector2.zero;
		_scrollZone = new Rect(0, SH * 0.37f, SW * 0.5f, SH * 0.51f);
		_fullScrollZone = new Rect(0, 0, SW * 0.48f, SH * 0.49f);
		
		_descriptionBackgroundRect = new Rect(SW * 0.01f, SH * 0.11f, SW * 0.54f, SH * 0.79f);
		_resultItemTextureRect = new Rect(SW * 0.0325f, SH * 0.155f, SH * 0.12f, SH * 0.12f);
		_resultItemDecorationRect = new Rect(SW * 0.03f, SH * 0.15f, SH * 0.13f, SH * 0.13f);
		_resultItemNameLabelRect = new Rect(SW * 0.04f + SH * 0.13f, SH * 0.15f, SW * 0.42f, SH * 0.12f);
		
		_decorationRect = new Rect(SW * 0.03f, SH * 0.24f, SW * 0.51f, SH * 0.08f);
		_reagentsLabelRect = new Rect(SW * 0.01f, SH * 0.27f, SW * 0.54f, SW * 0.07f);
		
		_makeButtonRect = new Rect(SW * 0.01f, SH * 0.91f, SW * 0.18f, SH * 0.08f);
		_incrementButtonRect = new Rect(SW * 0.21f, SH * 0.91f, SW * 0.13f, SH * 0.08f);
		_decrementButtonRect = new Rect(SW * 0.46f, SH * 0.91f, SW * 0.13f, SH * 0.08f);
		_craftCountLabelRect = new Rect(SW * 0.27f, SH * 0.91f, SW * 0.05f, SH * 0.08f);
		_makeAllButtonRect = new Rect(SW * 0.61f, SH * 0.91f, SW * 0.18f, SH * 0.08f);
		_closeButtonRect = new Rect(SW * 0.81f, SH * 0.91f, SW * 0.18f, SH * 0.08f);
	}

	public void OnDestroy()
	{
		parent.processWindow = null;
		parent = null;

		_craftManager = null;
		_resultItemTexture = null;
	}

	public void Init()
	{
		string itemIconName = _craftManager.selectedCraftSpell.itemIcon;
		itemIconName = itemIconName.Substring(0, itemIconName.LastIndexOf('.'));
		_resultItemTexture = Resources.Load<Texture> (string.Format ("Icons/{0}", itemIconName));

		Spell craftSpell = SpellManager.GetSpell(_craftManager.selectedCraftSpell.spellID);
		if(craftSpell != null && craftSpell.Effect[0] == (int)__SpellEffects.SPELL_EFFECT_ENCHANT_ITEM)
		{
			_enchantTargetItem = new ItemButton();
			_enchantTargetItemName = "This craft request target item.";
		}

		_reagentTextureList = new List<Texture>();
		foreach(CraftReagent craftReagent in _craftManager.selectedCraftSpell.reagents)
		{
			itemIconName = craftReagent.itemIcon.Substring(0, craftReagent.itemIcon.LastIndexOf('.'));
			Texture reagentIconTexture = Resources.Load<Texture> (string.Format ("Icons/{0}", itemIconName));
			_reagentTextureList.Add(reagentIconTexture);
		}
	}

	public void DrawGUI()
	{
		GUI.Box(_descriptionBackgroundRect, "", parent.skin.GetStyle("DescriptionPlaneBackground"));
		GUI.DrawTexture(_resultItemTextureRect, _resultItemTexture);
		GUI.Label(_resultItemDecorationRect, "", parent.skin.GetStyle("ProcessWindowResultItemDecoration"));

		uint countInInventory = _craftManager.selectedCraftSpell.countInInventory;
		string resultItemName = _craftManager.selectedCraftSpell.spellName + " [" + countInInventory + "]";
		GUI.Label(_resultItemNameLabelRect, resultItemName, parent.skin.GetStyle("ProcessWindowReagentNameText"));

		GUI.Label(_decorationRect, "", parent.skin.GetStyle("ProcessWindowDescriptionDecoration"));
		GUI.Label(_reagentsLabelRect, "Reagents", parent.skin.GetStyle("ProcessWindowRecipeLabelText"));

		_scrollPosition = GUI.BeginScrollView(_scrollZone, _scrollPosition, _fullScrollZone, GUIStyle.none, GUIStyle.none);
			DrawReagentsInfo();
		GUI.EndScrollView();
		
		DrawRightPanel();
		inventory.DrawGUI();

		if(_isDragBegin)
		{
			Rect dragRect = _dragItem.position;
			dragRect.center = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
			GUI.DrawTexture(dragRect, _dragItem.texture);
		}
	}

	private void DrawReagentsInfo()
	{
		float SW = Screen.width;
		float SH = Screen.height;

		float offset = SH * 0.03f;
		if(_enchantTargetItem != null)
		{
			offset += SH * 0.14f;
			if(_enchantTargetItem.texture != null)
			{
				GUI.DrawTexture(_enchantTargetItem.position, _enchantTargetItem.texture);
			}

			Rect enchantDecorationRect = new Rect(SW * 0.03f, SH * 0.03f, SH * 0.13f, SH * 0.13f);
			_enchantTargetItem.position = enchantDecorationRect;
			Rect enchantNameLabelRect = new Rect(SW * 0.04f + SH * 0.13f, SH * 0.03f, SW * 0.42f, SH * 0.12f);
			GUI.Label(enchantDecorationRect, "", parent.skin.GetStyle("ProcessWindowResultItemDecoration"));
			GUI.Label(enchantNameLabelRect, _enchantTargetItemName, parent.skin.GetStyle("ProcessWindowReagentNameText"));
		}

		float padding = 0;
		for(int index = 0; index < _craftManager.selectedCraftSpell.reagents.Count; index++)
		{
			padding = SH * 0.14f * index + offset;

			Rect reagentTextureRect = new Rect(SW * 0.0325f, padding + (SH * 0.005f), SH * 0.12f, SH * 0.12f);
			Rect reagentDecorationRect = new Rect(SW * 0.03f, padding, SH * 0.13f, SH * 0.13f);
			Rect reagentNameLabelRect = new Rect(SW * 0.04f + SH * 0.13f, padding, SW * 0.42f, SH * 0.12f);
			
			GUI.DrawTexture(reagentTextureRect, _reagentTextureList[index]);
			GUI.Label(reagentDecorationRect, "", parent.skin.GetStyle("ProcessWindowResultItemDecoration"));
			
			CraftReagent craftReagent = _craftManager.selectedCraftSpell.reagents[index];
			string reagentName = craftReagent.itemName;
			reagentName += " [" + craftReagent.countInInventory.ToString() + "/" + craftReagent.itemsCount.ToString() + "]";
			
			GUI.Label(reagentNameLabelRect, reagentName, parent.skin.GetStyle("ProcessWindowReagentNameText"));
		}

		_fullScrollZone = new Rect(0, 0, Screen.width * 0.48f, offset + padding);
	}

	private void DrawRightPanel()
	{
		if(GUI.Button(_makeButtonRect, "Make", parent.skin.button))
		{
			if(_enchantTargetItem != null && _enchantTargetItem.item != null)
			{
				_craftManager.CraftToItem(_craftManager.selectedCraftSpell.spellID, _enchantTargetItem.item.guid);
			}
			else
			{
				_craftManager.countCraft = _craftCount;
				_craftManager.spellToCraft = _craftManager.selectedCraftSpell.spellID;
			}
		}

		if(_enchantTargetItem == null)
		{
			if(GUI.Button(_incrementButtonRect, "", parent.skin.GetStyle("ProcessWindowIncrementButton")))
			{
				if(_craftCount < _craftManager.selectedCraftSpell.GetMaxCraftCount())
				{
					_craftCount++;
				}
			}

			GUI.Label(_craftCountLabelRect, _craftCount.ToString(), parent.skin.GetStyle("ProcessWindowCraftCountText")); 
			
			if(GUI.Button(_decrementButtonRect, "", parent.skin.GetStyle("ProcessWindowDecrementButton")))
			{
				if(_craftCount > 1)
				{
					_craftCount--;
				}
			}
			
			if(GUI.Button(_makeAllButtonRect, "Make All", parent.skin.button))
			{
				_craftManager.countCraft = _craftManager.selectedCraftSpell.GetMaxCraftCount();
				_craftManager.spellToCraft = _craftManager.selectedCraftSpell.spellID;
			}

			if(_craftManager.countCraft >= 0 && _craftManager.spellToCraft != 0)
			{
				_craftCount = _craftManager.countCraft;
			}
			else if(_craftCount < 1)
			{
				_craftCount = 1;
			}
		}
		
		if(GUI.Button(_closeButtonRect, "Close", parent.skin.button))
		{
			parent.SetWindowState(CraftMenuWindowState.CRAFT_WINDOW_STATE);
			Destroy(this.gameObject);
		}
	}

	private ItemButton _dragItem;
	private bool _isDragBegin = false;

	public void LateUpdate()
	{
		ItemButton itemButton;
		Vector2 touchPosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
		if(Input.GetMouseButtonDown(0))
		{
			itemButton = inventory.mng.IsTouchInSlotsContainer(touchPosition);
			if(itemButton != null && itemButton.texture != null && 
			   !inventory.mng.blockedItems.Contains(itemButton.item.guid))
			{
				_dragItem = itemButton;
				_isDragBegin = true;
				return;
			}
			
			if(_enchantTargetItem != null && 
			   _enchantTargetItem.position.Contains(touchPosition) &&
			   _enchantTargetItem.item != null)
			{
				_dragItem = _enchantTargetItem;
				_isDragBegin = true;
				return;
			}
		}

		if(_isDragBegin)
		{
			if(Input.GetMouseButtonUp(0))
			{
				_isDragBegin = false;
				
				if(_dragItem.position.Contains(touchPosition))
				{
					return;
				}
				
				itemButton = inventory.mng.IsTouchInSlotsContainer(touchPosition);
				if(itemButton != null)
				{
					if(itemButton.item != null && inventory.mng.blockedItems.Contains(itemButton.item.guid))
					{
						inventory.mng.blockedItems.Remove(itemButton.item.guid);
						_enchantTargetItem.texture = null;
						_enchantTargetItemName = "This craft request target item.";
					}
					return;
				}

				if(_dragItem.item != null)
				{
					Rect globalEnchantTargetPosition = new Rect(_scrollZone.x + _enchantTargetItem.position.x + _scrollPosition.x,
					                                            _scrollZone.y + _enchantTargetItem.position.y + _scrollPosition.y,
					                                            _enchantTargetItem.position.width,
					                                            _enchantTargetItem.position.height);
					if(globalEnchantTargetPosition.Contains(touchPosition))
					{
						if((_dragItem.item.classType == ItemClass.ITEM_CLASS_ARMOR && 
						    _dragItem.item.inventoryType != InventoryType.INVTYPE_NON_EQUIP &&
						    _dragItem.item.inventoryType != InventoryType.INVTYPE_BODY &&
						    _dragItem.item.inventoryType != InventoryType.INVTYPE_FINGER &&
						    _dragItem.item.inventoryType != InventoryType.INVTYPE_NECK &&
						    _dragItem.item.inventoryType != InventoryType.INVTYPE_TABARD &&
						    _dragItem.item.inventoryType != InventoryType.INVTYPE_TRINKET &&
						    _dragItem.item.inventoryType != InventoryType.INVTYPE_RELIC &&
						    _dragItem.item.inventoryType != InventoryType.INVTYPE_HOLDABLE) ||
						   (_dragItem.item.classType == ItemClass.ITEM_CLASS_WEAPON &&
						 	_dragItem.item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_obsolete &&
						 	_dragItem.item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_EXOTIC &&
						 	_dragItem.item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_EXOTIC2 &&
						 	_dragItem.item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_MISC &&
						 	_dragItem.item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_THROWN &&
						 	_dragItem.item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_WAND))
						{
							inventory.mng.blockedItems.Add(_dragItem.item.guid);
							if(_enchantTargetItem.item != null)
							{
								inventory.mng.blockedItems.Remove(_enchantTargetItem.item.guid);
							}
							_enchantTargetItem.item = _dragItem.item;
							_enchantTargetItem.texture = _dragItem.texture;
							_enchantTargetItemName = _enchantTargetItem.item.name;
							return;
						}
					}
				}
			}
		}

		if(Input.touchCount > 0)
		{
			Touch touch = Input.touches[0];
			if(touch.phase == TouchPhase.Moved && _scrollZone.Contains(touch.position))
			{
				_scrollPosition.y += touch.deltaPosition.y;
			}
		}

		_craftManager.UpdateCraftReagentsCount(_craftManager.selectedCraftSpell);
	}
}