using UnityEngine;
using System.Collections;

public enum ExatraMenuWindowState
{
	EXTRA_WINDOW_STATE,
	RAID_WINDOW_STATE,
	GUILD_WINDOW_STATE,
	CRAFT_WINDOW_STATE,
	BATTLEZONE_WINDOW_STATE
}

public class ExtraMenuWindow : MonoBehaviour
{
	public GUISkin skin;
	public CraftInterfaceController craftInterfaceController;
	public BattleZoneWindow battleZoneWindow;
	public PlayerMenuManager parent;
	
	public ExatraMenuWindowState windowState = ExatraMenuWindowState.EXTRA_WINDOW_STATE;

	private Rect _backgroundPlaneRect;
	private Rect _leftDecorationRect;
	private Rect _rightDecorationRect;

	private Rect _raidButtonRect;
	private Rect _craftButtonRect;
	private Rect _battleZoneButtonRect;
	private Rect _guildButtonRect;
	private Rect _friendsButtonRect;

	public void Awake()
	{
		skin = Resources.Load<GUISkin> ("GUI/ExtraMenuSkin");

		windowState = ExatraMenuWindowState.EXTRA_WINDOW_STATE;

		float SW = Screen.width;
		float SH = Screen.height;

		skin.button.fontSize = (int)(SH * 0.055f);

		_backgroundPlaneRect = new Rect(0, SH * 0.1f, SW, SH * 0.9f);
		_leftDecorationRect = new Rect(SW * 0.025f, SH * 0.11f, SH * 0.11f, SH * 0.88f);
		_rightDecorationRect = new Rect(SW * 0.975f - SH * 0.11f, SH * 0.11f, SH * 0.11f, SH * 0.88f);

		_raidButtonRect = new Rect(SW * 0.15f, SH * 0.15f, SW * 0.3f, SH * 0.15f);
		_craftButtonRect = new Rect(SW * 0.15f, SH * 0.4f, SW * 0.3f, SH * 0.15f);
		_battleZoneButtonRect = new Rect(SW * 0.15f, SH * 0.65f, SW * 0.3f, SH * 0.15f);
		_guildButtonRect = new Rect(SW * 0.55f, SH * 0.15f, SW * 0.3f, SH * 0.15f);
		_friendsButtonRect = new Rect(SW * 0.55f, SH * 0.4f, SW * 0.3f, SH * 0.15f);
	}

	public void OnEnable()
	{
		windowState = ExatraMenuWindowState.EXTRA_WINDOW_STATE;
	}

	public void OnDisable()
	{
		switch (windowState)
		{
		case ExatraMenuWindowState.RAID_WINDOW_STATE:
			break;
		case ExatraMenuWindowState.GUILD_WINDOW_STATE:
			break;
		case ExatraMenuWindowState.CRAFT_WINDOW_STATE:
			Destroy(craftInterfaceController.gameObject);
			break;
		case ExatraMenuWindowState.BATTLEZONE_WINDOW_STATE:
			battleZoneWindow.gameObject.SetActive(false);
			break;
		}
	} 

	public void DrawGUI()
	{
		switch (windowState)
		{
		case ExatraMenuWindowState.EXTRA_WINDOW_STATE:
			DrawExtraWindow();
			break;
		case ExatraMenuWindowState.RAID_WINDOW_STATE:
			break;
		case ExatraMenuWindowState.GUILD_WINDOW_STATE:
			break;
		case ExatraMenuWindowState.CRAFT_WINDOW_STATE:
			GUI.Box(_backgroundPlaneRect, "", skin.GetStyle("BackGround"));
			if(craftInterfaceController != null)
			{
				craftInterfaceController.DrawGUI();
			}
			break;
		case ExatraMenuWindowState.BATTLEZONE_WINDOW_STATE:
			GUI.Box(_backgroundPlaneRect, "", skin.GetStyle("BackGround"));
			battleZoneWindow.DrawGUI();
			break;
		}
	}

	public void SetWindowState(ExatraMenuWindowState newState)
	{
		OnDisable();

		switch (newState)
		{
		case ExatraMenuWindowState.RAID_WINDOW_STATE:
			RealmSocket.IsRaidState = true;
			break;
		case ExatraMenuWindowState.GUILD_WINDOW_STATE:
			RealmSocket.IsGuildState = true;
			break;
		case ExatraMenuWindowState.CRAFT_WINDOW_STATE:
			GameObject go = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/CraftInterfaceController"));
			if(go != null)
			{
				craftInterfaceController = go.GetComponent<CraftInterfaceController>();
				if(craftInterfaceController == null)
				{
					Debug.LogError("CraftInterfaceController script missing.");
				}
				else
				{
					craftInterfaceController.transform.parent = this.transform;
					craftInterfaceController.parent = this;
				}
			}
			else
			{
				Debug.LogError("Can't load CraftInterfaceController prefab.");
			}
			break;
		case ExatraMenuWindowState.BATTLEZONE_WINDOW_STATE:
			battleZoneWindow.gameObject.SetActive(true);
			battleZoneWindow.parent = this;
			break;
		}
		windowState = newState;
	}

	private void DrawExtraWindow()
	{
		GUI.Box(_backgroundPlaneRect, "", skin.GetStyle("Background"));
		GUI.Label(_leftDecorationRect, "", skin.GetStyle("Decoration"));
		GUI.Label(_rightDecorationRect, "", skin.GetStyle("Decoration"));

		// Left Column
		if(GUI.Button(_raidButtonRect, "RAID", skin.button))
		{
			SetWindowState(ExatraMenuWindowState.RAID_WINDOW_STATE);
		}

		if(GUI.Button(_craftButtonRect, "CRAFT", skin.button))
		{
			SetWindowState(ExatraMenuWindowState.CRAFT_WINDOW_STATE);
		}

		if(GUI.Button(_battleZoneButtonRect, "BATTLE ZONE", skin.button))
		{
			SetWindowState(ExatraMenuWindowState.BATTLEZONE_WINDOW_STATE);
		}


		// Right Column
		if(GUI.Button(_guildButtonRect, "GUILD", skin.button))
		{
			SetWindowState(ExatraMenuWindowState.GUILD_WINDOW_STATE);
		}

		if(GUI.Button(_friendsButtonRect, "FRIENDS", skin.button))
		{
			RealmSocket.IsFriendState = true;
			this.gameObject.SetActive(false);
		}
	}
}