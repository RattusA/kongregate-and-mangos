﻿using UnityEngine;
using System.Collections;

public class MainMenuPanel : MonoBehaviour
{
	public void OnItemShopButtonClick()
	{
		Debug.Log("ItemShop");
	}

	public void OnGetFreeMithrilButtonClick()
	{
		Debug.Log("GetFreeMithril");
	}

	public void OnLeaderboardButtonClick()
	{
		Debug.Log("Leaderboard");
	}

	public void OnUnstuckButtonClick()
	{
		Debug.Log("Unstuck");
	}

	public void OnHelpButtonClick()
	{
		Debug.Log("Help");
	}

	public void OnBuyMithrilButtonClick()
	{
		Debug.Log("BuyMithril");
	}
}