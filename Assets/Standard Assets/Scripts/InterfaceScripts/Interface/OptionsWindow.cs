﻿using UnityEngine;
using System.Collections;

enum OptionWindowState
{
	OPTION_WINDOW_NONE,
	OPTION_WINDOW_SELF_CAST_WINDOW,
	OPTION_WINDOW_TARGET_CAST_WINDOW,
	OPTION_WINDOW_PARTY_CAST_WINDOW,
	OPTION_WINDOW_CHAT_SIZE_WINDOW,
}

public class OptionsWindow : MonoBehaviour
{
	public GUISkin skin;
	public PlayerMenuManager parent;

	private OptionWindowState _windowState;
	private MusicManager _musicManager;
	private Camera _camera;

	private Vector2 _scrollPosition;
	private Rect _scrollZone;
	private Rect _fullScrollZone;

	private float _dpi;
	private const float _DEFAULT_DPI = 72;

	private Rect _backgroundPlaneRect;
	private Rect _topLeftDecorationRect;
	private Rect _topRightDecorationRect;
	private Rect _gameOptionsLabelRect;
	private Rect _topDecorationRect;
	
	public void Awake()
	{
		skin = Resources.Load<GUISkin> ("GUI/OptionsSkin");

		_musicManager = GameObject.Find("MusicManager(Clone)").GetComponent<MusicManager>();
		_camera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();

		_dpi = (Screen.dpi > 0) ? Screen.dpi : _DEFAULT_DPI;

		float SW = Screen.width;
		float SH = Screen.height;

		float constWidth = 1024;
		float constHeight = 768;

		skin.horizontalSlider.padding.left = (int)(SW * 0.01f);
		skin.horizontalSlider.padding.right = (int)(SW * 0.01f);
		skin.horizontalSliderThumb.fixedWidth = (int)(SW * 0.035f);
		
		skin.label.fontSize = (int)(SH * 0.06f);
		skin.button.fontSize = (int)(SH * 0.055f);
		skin.GetStyle("OptionsMainLabel").fontSize = (int)(SH * 0.1f);

		_backgroundPlaneRect = new Rect(0, SH * 0.1f, SW, SH * 0.9f);
		_topLeftDecorationRect = ScreenController.ConvertRect(new Rect(0, 0, constHeight * 0.35f, constHeight * 0.35f));
		_topLeftDecorationRect.x = SW * 0.02f;
		_topLeftDecorationRect.y = SH * 0.12f;
		
		_topRightDecorationRect = ScreenController.ConvertRect(new Rect(0, 0, constHeight * 0.35f, constHeight * 0.35f));
		_topRightDecorationRect.x = SW * 0.98f - _topRightDecorationRect.width;
		_topRightDecorationRect.y = SH * 0.12f;
		
		_gameOptionsLabelRect = ScreenController.ConvertRect(new Rect(0, 0, constWidth * 0.6f, 
		                                                              constWidth * 0.12f));
		_gameOptionsLabelRect.x = (SW - _gameOptionsLabelRect.width) / 2;
		_gameOptionsLabelRect.y = SH * 0.15f;
		
		_topDecorationRect = new Rect(_topLeftDecorationRect.width * 0.65f , SH * 0.26f,
		                              SW - _topLeftDecorationRect.width * 1.3f, SW * 0.05f);
		
		_scrollPosition = Vector2.zero;
		_scrollZone = new Rect(SW * 0.05f, _topDecorationRect.yMax, SW * 0.9f, SH * 0.98f - _topDecorationRect.yMax);
		_fullScrollZone = new Rect(0, 0, SW * 0.88f, SH * 2f);

		_windowState = OptionWindowState.OPTION_WINDOW_NONE;
	}
	
	void OnDestroy()
	{
		parent.optionsWindow = null;
		parent = null;

		_musicManager = null;
		_camera = null;

		Resources.UnloadAsset(skin);
		skin = null;
	}

	public void DrawGUI()
	{
		GUI.Box(_backgroundPlaneRect, "", skin.GetStyle("OptionsBackgroudPlane"));
		GUI.Label(_gameOptionsLabelRect, "GAME OPTIONS", skin.GetStyle("OptionsMainLabel"));

		_scrollPosition = GUI.BeginScrollView(_scrollZone, _scrollPosition, _fullScrollZone, GUIStyle.none, GUIStyle.none);
			DrawOptions();
		GUI.EndScrollView();

		GUI.Label(_topLeftDecorationRect, "", skin.GetStyle("OptionsTopLeftDecoration"));
		GUI.Label(_topRightDecorationRect, "", skin.GetStyle("OptionsTopRightDecoration"));
		GUI.Label(_topDecorationRect, "", skin.GetStyle("OptionsTopDecoration"));
	}

	private void DrawOptions()
	{
		// View Distance
		GUI.Label(GUIFunctions.RectWH(0, 0.02f, 0.45f, 0.1f), "Model Distance", skin.label);
		float newModelDistance = GUI.HorizontalSlider(GUIFunctions.RectWH(0.48f, 0.03f, 0.35f, 0.08f), 
		                                              OptionsManager.ViewDistance, 0, 1,
		                                              skin.horizontalSlider, skin.horizontalSliderThumb);
		if(Mathf.Abs(newModelDistance - OptionsManager.ViewDistance) > 0.001f)
		{
			_camera.farClipPlane = 300 + newModelDistance * 500;
			_camera.layerCullDistances[12] = _camera.farClipPlane * 0.2f; 
			_camera.layerCullDistances[13] = _camera.farClipPlane * 0.5f;
			RenderSettings.fogEndDistance = _camera.farClipPlane;
			OptionsManager.ViewDistance = newModelDistance;
		}

		// Camera Speed
		GUI.Label(GUIFunctions.RectWH(0, 0.14f, 0.45f, 0.1f), "Camera Speed", skin.label);
		OptionsManager.CameraSpeed = GUI.HorizontalSlider(GUIFunctions.RectWH(0.48f, 0.15f, 0.35f, 0.08f), 
		                                                  OptionsManager.CameraSpeed, 0, 1,
		                                                  skin.horizontalSlider, skin.horizontalSliderThumb);

		// All Damage Numbers
		GUI.Label(GUIFunctions.RectWH(0, 0.26f, 0.45f, 0.1f), "All Damage Numbers", skin.label);
		OptionsManager.AllDamageNumbers = GUI.Toggle(GUIFunctions.RectWH(0.48f, 0.27f, 0.12f, 0.08f), 
		                                             OptionsManager.AllDamageNumbers, "", skin.toggle);

		// Music
		GUI.Label(GUIFunctions.RectWH(0, 0.38f, 0.45f, 0.1f), "Music", skin.label);
		bool newMusicState = GUI.Toggle(GUIFunctions.RectWH(0.48f, 0.39f, 0.12f, 0.08f), OptionsManager.Music, "",
		                                skin.toggle);
		if(newMusicState != OptionsManager.Music)
		{
			OptionsManager.Music = newMusicState;
			_musicManager.EnableDisableSound(newMusicState);
		}

		// Sound FX
		GUI.Label(GUIFunctions.RectWH(0, 0.5f, 0.45f, 0.1f), "Sound FX", skin.label);
		bool newSoundFX = GUI.Toggle(GUIFunctions.RectWH(0.48f, 0.51f, 0.12f, 0.08f), OptionsManager.SoundFX, "",
		                             skin.toggle);
		if(newSoundFX != OptionsManager.SoundFX)
		{
			OptionsManager.SoundFX = newSoundFX;
			_musicManager.EnableDisableSoundFX(newSoundFX);
			_musicManager.EffectVolumeChange(OptionsManager.FxVolume);
		}

		// Music Volume
		GUI.Label(GUIFunctions.RectWH(0, 0.62f, 0.45f, 0.1f), "Music Volume", skin.label);
		float newMusicVolume = GUI.HorizontalSlider(GUIFunctions.RectWH(0.48f, 0.63f, 0.35f, 0.08f), 
		                                            OptionsManager.MusicVolume, 0, 1,
		                                            skin.horizontalSlider, skin.horizontalSliderThumb);
		if(Mathf.Abs(newMusicVolume - OptionsManager.MusicVolume) > 0.001f)
		{
			OptionsManager.MusicVolume = newMusicVolume;
			_musicManager.MusicVolumeChange(newMusicVolume);
		}

		// FX Volume
		GUI.Label(GUIFunctions.RectWH(0, 0.74f, 0.45f, 0.1f), "FX Volume", skin.label);
		float newFxVolume = GUI.HorizontalSlider(GUIFunctions.RectWH(0.48f, 0.75f, 0.35f, 0.08f), 
		                                         OptionsManager.FxVolume, 0, 1,
		                                         skin.horizontalSlider, skin.horizontalSliderThumb);
		if(Mathf.Abs(newFxVolume - OptionsManager.FxVolume) > 0.001f)
		{
			OptionsManager.FxVolume = newFxVolume;
			_musicManager.EffectVolumeChange(newFxVolume);
		}

		// Self Portrait Touch
		GUI.Label(GUIFunctions.RectWH(0, 0.86f, 0.45f, 0.1f), "Self Portrait Touch", skin.label);
		if(GUI.Button(GUIFunctions.RectWH(0.48f, 0.87f, 0.35f, 0.08f), "Select", skin.button))
		{
			_windowState = OptionWindowState.OPTION_WINDOW_SELF_CAST_WINDOW;
		}

		// Target Portrait Touch
		GUI.Label(GUIFunctions.RectWH(0, 0.98f, 0.45f, 0.1f), "Target Portrait Touch", skin.label);
		if(GUI.Button(GUIFunctions.RectWH(0.48f, 0.99f, 0.35f, 0.08f), "Select", skin.button))
		{
			_windowState = OptionWindowState.OPTION_WINDOW_TARGET_CAST_WINDOW;
		}

		// Party Portrait Touch
		GUI.Label(GUIFunctions.RectWH(0, 1.1f, 0.45f, 0.1f), "Party Portrait Touch", skin.label);
		if(GUI.Button(GUIFunctions.RectWH(0.48f, 1.11f, 0.35f, 0.08f), "Select", skin.button))
		{
			_windowState = OptionWindowState.OPTION_WINDOW_PARTY_CAST_WINDOW;
		}

		// Chat Rows
		GUI.Label(GUIFunctions.RectWH(0, 1.22f, 0.45f, 0.1f), "Chat Rows", skin.label);
		if(GUI.Button(GUIFunctions.RectWH(0.48f, 1.23f, 0.35f, 0.08f), "Select", skin.button))
		{
			_windowState = OptionWindowState.OPTION_WINDOW_CHAT_SIZE_WINDOW;
		}

		// Panel Background
		GUI.Label(GUIFunctions.RectWH(0, 1.34f, 0.45f, 0.1f), "Panel Background", skin.label);
		OptionsManager.ShowSkillsBackground = GUI.Toggle(GUIFunctions.RectWH(0.48f, 1.35f, 0.12f, 0.08f), 
		                                                 OptionsManager.ShowSkillsBackground, "", skin.toggle);

		// Use Vibration
		GUI.Label(GUIFunctions.RectWH(0, 1.46f, 0.45f, 0.1f), "Use Vibration", skin.label);
		OptionsManager.UseVibration = GUI.Toggle(GUIFunctions.RectWH(0.48f, 1.47f, 0.12f, 0.08f), 
		                                         OptionsManager.UseVibration, "", skin.toggle);

		// Show Player FX
		GUI.Label(GUIFunctions.RectWH(0, 1.58f, 0.45f, 0.1f), "Show Player FX", skin.label);
		OptionsManager.PlayerFx = GUI.Toggle(GUIFunctions.RectWH(0.48f, 1.59f, 0.12f, 0.08f),
		                                     OptionsManager.PlayerFx, "", skin.toggle);

		// Show Other Players FX
		GUI.Label(GUIFunctions.RectWH(0, 1.7f, 0.45f, 0.1f), "Show Other Players FX", skin.label);
		OptionsManager.OtherPlayerFx = GUI.Toggle(GUIFunctions.RectWH(0.48f, 1.71f, 0.12f, 0.08f), 
		                                          OptionsManager.OtherPlayerFx, "", skin.toggle);

		// Use the quest arrow?
		GUI.Label(GUIFunctions.RectWH(0, 1.82f, 0.45f, 0.1f), "Use the quest arrow?", skin.label);
		OptionsManager.ShowQuestArrow = GUI.Toggle(GUIFunctions.RectWH(0.48f, 1.83f, 0.12f, 0.08f), 
		                                           OptionsManager.ShowQuestArrow, "", skin.toggle);

		switch(_windowState)
		{
		case OptionWindowState.OPTION_WINDOW_SELF_CAST_WINDOW:
			GUI.ModalWindow(1, GUIFunctions.RectWH(0, 0, 1, 1), DrawSelfPortraitOptionWindow, "");
			break;
		case OptionWindowState.OPTION_WINDOW_TARGET_CAST_WINDOW:
			GUI.ModalWindow(2, GUIFunctions.RectWH(0, 0, 1, 1), DrawTargetPortraitOptionWindow, "");
			break;
		case OptionWindowState.OPTION_WINDOW_PARTY_CAST_WINDOW:
			GUI.ModalWindow(3, GUIFunctions.RectWH(0, 0, 1, 1), DrawPartyPortraitOptionWindow, "");
			break;
		case OptionWindowState.OPTION_WINDOW_CHAT_SIZE_WINDOW:
			GUI.ModalWindow(4, GUIFunctions.RectWH(0, 0, 1, 1), DrawChatSizeOptionWindow, "");
			break;
		}
	}

	public void Update()
	{
		if(Input.touchCount > 0)
		{
			Touch touch = Input.touches[0];
			if(touch.phase == TouchPhase.Moved && _scrollZone.Contains(touch.position))
			{
				_scrollPosition.y += touch.deltaPosition.y * (_dpi / _DEFAULT_DPI);
			}
		}
	}

	private void DrawSelfPortraitOptionWindow(int index)
	{
		GUI.Box(GUIFunctions.RectWH(0.25f, 0.325f, 0.5f, 0.35f), "", skin.box);
		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.375f, 0.46f, 0.1f), "Self Spellcast", skin.button))
		{
			OptionsManager.SelfPortraitTouch = 1;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}

		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.525f, 0.46f, 0.1f), "Do Nothing", skin.button))
		{
			OptionsManager.SelfPortraitTouch = 0;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}
	}

	private void DrawTargetPortraitOptionWindow(int index)
	{
		GUI.Box(GUIFunctions.RectWH(0.25f, 0.25f,0.5f, 0.5f), "", skin.box);
		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.3f, 0.46f, 0.1f), "Self Spellcast", skin.button))
		{
			OptionsManager.TargetPortraitTouch = 1;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}

		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.45f, 0.46f, 0.1f), "Spellcast On Target", skin.button))
		{
			OptionsManager.TargetPortraitTouch = 2;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}

		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.6f, 0.46f, 0.1f), "Do Nothing", skin.button))
		{
			OptionsManager.TargetPortraitTouch = 0;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}
	}

	private void DrawPartyPortraitOptionWindow(int index)
	{
		GUI.Box(GUIFunctions.RectWH(0.25f, 0.25f, 0.5f, 0.5f), "", skin.box);
		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.3f, 0.46f, 0.1f), "Do Nothing", skin.button))
		{
			OptionsManager.PartyPortraitTouch = 1;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}

		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.45f, 0.46f, 0.1f), "Cast Spell", skin.button))
		{
			OptionsManager.PartyPortraitTouch = 2;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}

		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.6f, 0.46f, 0.1f), "Select", skin.button))
		{
			OptionsManager.PartyPortraitTouch = 0;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}
	}

	private void DrawChatSizeOptionWindow(int index)
	{
		GUI.Box(GUIFunctions.RectWH(0.25f, 0.1f, 0.5f, 0.825f), "", skin.box);
		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.15f, 0.46f, 0.1f), "", skin.GetStyle("OptionsOneRowButton")))
		{
			OptionsManager.TextFieldSize = 1;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}
		
		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.275f, 0.46f, 0.1f), "", skin.GetStyle("OptionsTwoRowsButton")))
		{
			OptionsManager.TextFieldSize = 2;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}
		
		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.4f, 0.46f, 0.1f), "", skin.GetStyle("OptionsThreeRowsButton")))
		{
			OptionsManager.TextFieldSize = 3;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}

		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.525f, 0.46f, 0.1f), "", skin.GetStyle("OptionsFourRowsButton")))
		{
			OptionsManager.TextFieldSize = 4;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}

		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.65f, 0.46f, 0.1f), "", skin.GetStyle("OptionsFiveRowsButton")))
		{
			OptionsManager.TextFieldSize = 5;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}

		if(GUI.Button(GUIFunctions.RectWH(0.27f, 0.775f, 0.46f, 0.1f), "", skin.GetStyle("OptionsSixRowsButton")))
		{
			OptionsManager.TextFieldSize = 6;
			_windowState = OptionWindowState.OPTION_WINDOW_NONE;
		}
	}
}