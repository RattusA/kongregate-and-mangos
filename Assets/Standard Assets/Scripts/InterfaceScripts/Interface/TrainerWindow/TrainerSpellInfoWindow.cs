using UnityEngine;
using System.Collections;

public class TrainerSpellInfoWindow : MonoBehaviour
{
	public TrainerWindow parent;

	protected Texture spellIcon;
	protected TrainerSpell trainerSpell;
	protected string spellDescription;

	protected Rect windowBackgroundRect;
	protected Rect descriptionRect;

	protected Rect topDecorationRect;
	protected Rect spellIconRect;
	protected Rect spellDecorationRect;

	protected Rect byeButtonRect;
	protected Rect closeButtonRect;

	protected Rect spellNameRect;
	protected Rect spellRankRect;
	protected Rect spellDescriptionRect;

	void Awake()
	{
		float SW = Screen.width;
		float SH = Screen.height;

		windowBackgroundRect = new Rect(SW * 0.15f, SH * 0.15f, SW * 0.7f, SH * 0.7f);
		descriptionRect = new Rect(SW * 0.36f, SH * 0.25f, SW * 0.47f, SH * 0.58f);

		topDecorationRect = new Rect(SW * 0.17f, SH * 0.18f, SW * 0.66f, SH * 0.06f);
		spellIconRect = new Rect(SW * 0.175f, SH * 0.255f, SW * 0.15f, SW * 0.15f);
		spellDecorationRect = new Rect(SW * 0.17f, SH * 0.25f, SW * 0.18f, SW * 0.18f);

		byeButtonRect = new Rect(SW * 0.17f, SH * 0.65f, SW * 0.195f, SH * 0.075f);
		closeButtonRect = new Rect(SW * 0.17f, SH * 0.75f, SW * 0.195f, SH * 0.075f);

		spellNameRect = new Rect(SW * 0.25f, SH * 0.18f, SW * 0.5f, SH * 0.06f);
		spellRankRect = new Rect(SW * 0.75f, SH * 0.18f, SW * 0.1f, SH * 0.06f);
		spellDescriptionRect = new Rect(SW * 0.38f, SH * 0.27f, SW * 0.43f, SH * 0.54f);
	}

	public void OnDestroy()
	{
		parent.spellInfoWindow = null;
		parent = null;

		spellIcon = null;
		trainerSpell = null;
	}

	public void SetSpellInfo(TrainerSpell spell, Texture icon)
	{
		trainerSpell = spell;
		spellIcon = icon;

		GetSpellDescription();
	}

	public void DrawGUI()
	{
		GUI.Box(windowBackgroundRect, "", parent.skin.box);
		GUI.Box(descriptionRect, "", parent.skin.GetStyle("SpellInfoDescriptionBackground"));

		GUI.Label(topDecorationRect, "", parent.skin.GetStyle("SpellInfoTopDecoration"));
		GUI.DrawTexture(spellIconRect, spellIcon);
		GUI.Label(spellDecorationRect, "", parent.skin.GetStyle("SpellInfoSpellDecoration"));

		if(GUI.Button(byeButtonRect, "Bye", parent.skin.button))
		{
			ByeTrainerSpell();
		}

		if(GUI.Button(closeButtonRect, "Close", parent.skin.button))
		{
			Destroy(this.gameObject);
		}

		GUI.Label(spellNameRect, trainerSpell.spellName, parent.skin.GetStyle("SpellNameLabel"));
		GUI.Label(spellRankRect, trainerSpell.spellRank, parent.skin.GetStyle("SpellNameLabel"));
		GUI.Label(spellDescriptionRect, spellDescription, parent.skin.GetStyle("SpellDescriptionLabel"));
	}

	private void GetSpellDescription()
	{
		if(trainerSpell.spellName != "unknown")
		{
			string description = "";
			SpellEntry blahSpell = dbs.sSpells.GetRecord(trainerSpell.spellID);
			
			if(trainerSpell.reqSkillName != "No Skill")
			{
				description += "Required: " + trainerSpell.reqSkillName + " ";
				description += trainerSpell.reqSkillValue.ToString() + "\n";
			}

			if(trainerSpell.reqSkillValue != 0)
			{
				uint reqSkillValue = trainerSpell.reqSkillValue * 2 + 1;
				description += "Required " + reqSkillValue + " lvl of Prospecting \n";
			}

			if(blahSpell.Description.Strings[1].Length > 0)
			{
				description += "Description: " + blahSpell.Description.Strings[1] + "\n\n";
			}

			description += "Cost: " + trainerSpell.spellCost + "\n";
			description += "Required level: " + trainerSpell.reqLevel.ToString() + "\n";

			spellDescription = description;
		}
	}

	private void ByeTrainerSpell()
	{
		ulong money = InterfaceManager.Instance.PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COINAGE);
		if(trainerSpell.spellState != TrainerSpellState.SPELL_NORMAL)
		{
			InterfaceManager.Instance.DrawMessageWindow("You cannot learn this yet!");
		}
		else if((ulong)trainerSpell.spellCost > money)
		{
			InterfaceManager.Instance.DrawMessageWindow("Not enough gold to train this spell!");
		}
		else
		{
			parent.mng.LearnSpellDelegate(trainerSpell);
		}

		Destroy(this.gameObject);
	}
}
