﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrainerWindow : MonoBehaviour
{
	public GUISkin skin;
	public NPCWindowManager parent;
	public TrainerSpellInfoWindow spellInfoWindow;

	public TrainerManager mng;

	protected Rect modalWindowRect;

	protected Rect windowBackgroundRect;
	protected Rect spellContainerRect;
	protected Rect leftDecorationRect;
	protected Rect rightDecorationRect;

	protected Rect leftArrowButtonRect;
	protected Rect rightArrowButtonRect;
	protected Rect closeButtonRect;

	public void Awake()
	{
		skin = Resources.Load<GUISkin> ("GUI/TrainerSkin");
		mng = new TrainerManager();

		float SW = Screen.width;
		float SH = Screen.height;

		skin.button.fontSize = (int)(SH * 0.05f);
		skin.label.fontSize = (int)(SH * 0.04f);
		skin.GetStyle("SpellNameLabel").fontSize = (int)(SH * 0.05f);
		skin.GetStyle("SpellDescriptionLabel").fontSize = (int)(SH * 0.05f);

		modalWindowRect = new Rect(0, 0, SW, SH);

		windowBackgroundRect = new Rect(SW * 0.1f, SH * 0.1f, SW * 0.8f, SH * 0.8f);
		spellContainerRect = new Rect(SW * 0.17f, SH * 0.15f, SW * 0.66f, SH * 0.6f);
		leftDecorationRect = new Rect(SW * 0.13f, SH * 0.77f, SW * 0.15f, SH * 0.1f);
		rightDecorationRect = new Rect(SW * 0.72f, SH * 0.77f, SW * 0.15f, SH * 0.1f);

		leftArrowButtonRect = new Rect(SW * 0.115f, SH * 0.415f, SW * 0.06f, SW * 0.06f);
		rightArrowButtonRect = new Rect(SW * 0.825f, SH * 0.415f, SW * 0.06f, SW * 0.06f);
		closeButtonRect = new Rect(SW * 0.4f, SH * 0.77f, SW * 0.2f, SW * 0.06f);
	}

	public void OnDestroy()
	{
		parent.trainerWnd = null;
		parent = null;

		Resources.UnloadAsset(skin);
		skin = null;
		mng = null;
	}

	public void DrawGUI()
	{
		GUI.Box(windowBackgroundRect, "", skin.box);
		GUI.Label(spellContainerRect, "", skin.GetStyle("TrainerSlotContainer"));

		GUI.Label(leftDecorationRect, "", skin.GetStyle("TrainerWindowDecorationLeft"));
		GUI.Label(rightDecorationRect, "", skin.GetStyle("TrainerWindowDecorationRight"));
		
		if(mng.page > 0)
		{
			if(GUI.Button(leftArrowButtonRect, "", skin.GetStyle("ArrowLeftButton")))
			{
				mng.page--;
				mng.UpdatePage();
			}
		}

		if(mng.page < mng.maxPage)
		{
			if(GUI.Button(rightArrowButtonRect, "", skin.GetStyle("ArrowRightButton")))
			{
				mng.page++;
				mng.UpdatePage();
			}
		}

		if(GUI.Button(closeButtonRect, "Close", skin.button))
		{	
			parent.SetWindowState(NpcWindowState.NONE);
			InterfaceManager.Instance.SetWindowState(InterfaceWindowState.ACTION_BAR_PANEL_STATE);
			Defines.showInterf = true;

			Destroy(this.gameObject);
		}

		DrawSpell();

		if(spellInfoWindow != null)
		{
			GUI.ModalWindow(0, modalWindowRect, DrawSpellInfoWindow, "");
		}
	}

	private void DrawSpellInfoWindow(int index)
	{
		spellInfoWindow.DrawGUI();
	}

	private void DrawSpell()
	{
		if(mng.spellSlotsList.Count > 0)
		{
			foreach(TrainerSlotButton trainSpell in mng.spellSlotsList)
			{
				GUI.DrawTexture(trainSpell.position, trainSpell.spellIcon);
				if(trainSpell.trainerSpell.spellState == TrainerSpellState.SPELL_BLOCKED)
				{
					GUI.Label(trainSpell.position, "", skin.GetStyle("LockedSpell"));
				}

				if(GUI.Button(trainSpell.position, "", GUIStyle.none))
				{
					InitTrainerSpellInfoWindow(trainSpell.trainerSpell, trainSpell.spellIcon);
				}
			}
		}
	}

	public void InitTrainerSpellInfoWindow(TrainerSpell spell, Texture texture)
	{
		GameObject gameObject = (GameObject)Instantiate(new GameObject("TrainerSpellInfoWindow"));
		gameObject.transform.parent = this.transform;
		
		spellInfoWindow = gameObject.AddComponent<TrainerSpellInfoWindow>();
		spellInfoWindow.parent = this;
		spellInfoWindow.SetSpellInfo(spell, texture);
	}
}