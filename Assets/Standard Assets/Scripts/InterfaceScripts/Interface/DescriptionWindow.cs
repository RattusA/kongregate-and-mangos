﻿using UnityEngine;

public class DescriptionWindow
{
	protected GUISkin skin;
	protected float SW;
	protected float SH;
	public InfoWindowButtonManager ButtonManager;
	public Texture IconTecture;
	public string TitleText = "";
	public string TitleTextExtended = "";
	public string DescriptionText;
	private ScrollZone TextZone = new ScrollZone();

	public string DescriptionTextValue
	{
		set
		{
			DescriptionText = value;
			TextZone.SetNewText(DescriptionText);
		}
	}

	public DescriptionWindow()
	{
		skin = Resources.Load<GUISkin> ("GUI/DescriptionSkin");
		if(skin != null)
		{
			skin.button.fontSize = (int)(Screen.height * 0.04f);
			skin.GetStyle("Title text").fontSize = (int)(Screen.height * 0.05f);
			skin.GetStyle("Description Text").fontSize = (int)(Screen.height * 0.0375f);
		}

		SW = Screen.width;
		SH = Screen.height;

		Rect statsRect = new Rect(SW * 0.33f, SH * 0.22f, SW * 0.53f, SH * 0.64f);
		TextZone.InitSkinInfo(skin.GetStyle("Description Text"));
		TextZone.CreateScrollZoneWithText(statsRect, DescriptionText, WorldSession.player.mainTextColor);

		ButtonManager = new InfoWindowButtonManager();
	}

	public void OnDestroy()
	{
		Resources.UnloadAsset(skin);
		skin = null;
	}
	
	public void DrawGUI(int index)
	{
		GUI.skin = skin;

		GUI.Box(new Rect(SW * 0.1f, SH * 0.1f, SW * 0.8f, SH * 0.8f), "");
		GUI.Box(new Rect(SW * 0.31f, SH * 0.2f, SW * 0.57f, SH * 0.68f), "",
		        skin.GetStyle("Desxription Text Field"));

		GUI.Label(new Rect(SW * 0.12f, SH * 0.13f, SW * 0.76f, SH * 0.06f), "",
		          skin.GetStyle("Top Decoration"));
		GUI.DrawTexture(new Rect(SW * 0.125f, SH * 0.205f, SW * 0.17f, SW * 0.17f), IconTecture);
		GUI.Label(new Rect(SW * 0.12f, SH * 0.20f, SW * 0.18f, SW * 0.18f), "",
		          skin.GetStyle("Icon Decoration"));

		foreach(InfoWindowButton infoButton in ButtonManager.ButtonList)
		{
			int elementID = ButtonManager.ButtonList.IndexOf(infoButton);
			infoButton.position = new Vector2(SW * 0.11f, SH * 0.775f - (SH * 0.1f * elementID));
			infoButton.width = SW * 0.2f;
			infoButton.height = SH * 0.08f;
			infoButton.Draw(skin.button);
		}
		
		GUI.Label(new Rect(SW * 0.2f, SH * 0.13f, SW * 0.55f, SH * 0.06f), TitleText,
		          skin.GetStyle("Title text"));
		GUI.Label(new Rect(SW * 0.75f, SH * 0.13f, SW * 0.1f, SH * 0.06f), TitleTextExtended,
		          skin.GetStyle("Title text"));
		TextZone.DrawTraitsInfo();
	}
}
