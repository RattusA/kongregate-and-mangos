﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.Text.RegularExpressions;
//
//public enum TradeWindowState
//{
//
//	EMPTY_WINDOW_STATE,
//	TRADE_REQUEST_WINDOW_STATE,
//	TRADE_WINDOW_STATE,
//};
//
//public class TradeWindow : MonoBehaviour
//{
//	[SerializeField]
//	protected GUISkin skin;
//
//	protected string goldAmount = "0";
//
//	public PlayerDialogWindowController parent;
//	public InventoryWindowBase inventory;
////	public TradeManager manager;
//
//	public TradeWindowState WindowState = TradeWindowState.EMPTY_WINDOW_STATE;
//
//	public bool reForge = false;
//	public bool showMessage = false;
//	public bool inputGold = false;
//
//	void Awake()
//	{
//		skin.label.fontSize = (int)(Screen.height * 0.045f);
//		skin.button.fontSize = (int)(Screen.height * 0.045f);
//		skin.textField.fontSize = (int)(Screen.height * 0.045f);
//		skin.GetStyle("Re-Forge Text Left").fontSize = (int)(Screen.height * 0.04f);
//		skin.GetStyle("Re-Forge Text Right").fontSize = (int)(Screen.height * 0.04f);
//		skin.GetStyle("Trade Request Label").fontSize = (int)(Screen.height * 0.07f);
//		skin.GetStyle("Gold Info Label").fontSize = (int)(Screen.height * 0.04f);
//	}
//
//	public void OnEnable()
//	{
////		ParentMng.inventoryWindow.WindowState = InventoryWindowState.TRADE_STATE;
//	}
//
//	public void OnDisable()
//	{
//		WindowState = TradeWindowState.EMPTY_WINDOW_STATE;
//	}
//
//	public void DrawGUI()
//	{
//		GUI.skin = skin;
//
//		switch(WindowState)
//		{
//		case TradeWindowState.TRADE_REQUEST_WINDOW_STATE:
//			DrawTradeRequestWindow();
//			break;
//		case TradeWindowState.TRADE_WINDOW_STATE:
//			DrawTradeWindow();
//			DrawInventory();
//
//			if(reForge)
//			{
//				DrawReForge();
//			}
//			break;
//		}
//
//		if(showMessage)
//		{
//			GUI.ModalWindow(1, new Rect(0, 0, Screen.width, Screen.height), DrawTradeMessageWindow, "");
//		}
//
//		if(inputGold)
//		{
//			GUI.ModalWindow(1, new Rect(0, 0, Screen.width, Screen.height), DrawInsertGoldWindow, "");
//		}
//	}
//
//	public void CloseTrade()
//	{
//		if(showMessage)
//		{
//			showMessage = false;
//		}
//
//		if(inputGold)
//		{
//			inputGold = false;
//		}
//		gameObject.SetActive(false);
//	}
//
//	// Draw Trade interface for Player and OtherPlayer.
//	private void DrawTradeWindow()
//	{
//		// Player trade panel
//		GUI.Box(new Rect(Screen.width * 0.01f, Screen.height * 0.02f, Screen.width * 0.47f, Screen.height * 0.325f),
//		        "", skin.GetStyle("Trade Panel"));
//		GUI.Label(new Rect(Screen.width * 0.05f, Screen.height * 0.025f, Screen.width * 0.25f, Screen.height * 0.05f),
//		          manager.playerScript.name);
//		if(GUI.Button(new Rect(Screen.width * 0.02f, Screen.height * 0.245f, Screen.width * 0.19f, Screen.height * 0.08f),
//		              "Add Gold"))
//		{
//			inputGold = true;
//		}
//		GUI.Label(new Rect(Screen.width * 0.2125f, Screen.height * 0.27f, Screen.width * 0.25f, Screen.height * 0.06f),
//		          goldAmount, skin.GetStyle("Gold Info Label"));
//		foreach(ItemButton itemButton in manager.playerTradeSlots)
//		{
//			if(GUI.Button(itemButton.position, itemButton.texture, GUIStyle.none))
//			{
//				manager.playerTradeSlots.Remove(itemButton);
//				Debug.LogError("TODO");
//				break;
//			}
//		}
//
//		// Target trade panel
//		GUI.Box(new Rect(Screen.width * 0.01f, Screen.height * 0.655f, Screen.width * 0.47f, Screen.height * 0.325f),
//		        "", skin.GetStyle("Trade Panel"));
////		GUI.Label(new Rect(Screen.width * 0.05f, Screen.height * 0.66f, Screen.width * 0.25f, Screen.height * 0.05f),
////		          manager.playerScript.target.name);
//	}
//
//	// Draw ReForge panel if playe know ReForge craft.
//	private void DrawReForge()
//	{
//		GUI.Box(new Rect(Screen.width * 0.01f, Screen.height * 0.36f, Screen.width * 0.47f, Screen.height * 0.28f),
//		        "", skin.GetStyle("Re-Forge Panel"));
//		GUI.Label(new Rect(Screen.width * 0.02f, Screen.height * 0.375f, Screen.width * 0.09f, Screen.width * 0.09f),
//		          "", skin.GetStyle("Item Decoration"));
//		GUI.Label(new Rect(Screen.width * 0.12f, Screen.height * 0.365f, Screen.width * 0.34f, Screen.height * 0.08f),
//		          "Test Test Test Test", skin.GetStyle("Re-Forge Text Left"));
//		GUI.Label(new Rect(Screen.width * 0.03f, Screen.height * 0.555f, Screen.width * 0.34f, Screen.height * 0.08f),
//		          "Test Test Test Test", skin.GetStyle("Re-Forge Text Right"));
//		GUI.Label(new Rect(Screen.width * 0.38f, Screen.height * 0.625f - Screen.width * 0.09f, Screen.width * 0.09f, Screen.width * 0.09f),
//		          "", skin.GetStyle("Item Decoration"));
//		GUI.Button(new Rect(Screen.width * 0.135f, Screen.height * 0.455f, Screen.width * 0.22f, Screen.height * 0.09f),
//		           "Re-Forge");
//	}
//
//	// Draw Inventory window with Player items.
//	private void DrawInventory()
//	{
//		inventory.DrawGUI();
//
//		if(GUI.Button(new Rect(Screen.width * 0.75f - Screen.height * 0.23f, Screen.height * 0.82f, Screen.height * 0.225f, Screen.height * 0.08f),
//		              "Accept"))
//		{
//
//		}
//
//		if(GUI.Button(new Rect(Screen.width * 0.75f + Screen.height * 0.005f, Screen.height * 0.82f, Screen.height * 0.225f, Screen.height * 0.08f),
//		              "Cancel"))
//		{
//			manager.SendCancelTrade();
//		}
//	}
//
//	private void DrawTemplateWindow()
//	{
//		GUI.Box(new Rect(Screen.width * 0.2f, Screen.height * 0.325f, Screen.width * 0.6f, Screen.height * 0.35f),
//		        "", skin.GetStyle("Re-Forge Panel"));
//
//		GUI.Label(new Rect(Screen.width * 0.22f, Screen.height * 0.345f, Screen.width * 0.56f, Screen.height * 0.18f),
//		          "", skin.GetStyle("Trade Request Decoration"));
//	}
//
//	private void DrawTradeRequestWindow()
//	{
//		DrawTemplateWindow();
//		GUI.Label(new Rect(Screen.width * 0.36f, Screen.height * 0.375f, Screen.width * 0.42f, Screen.height * 0.2f),
//		          "Trade requested", skin.GetStyle("Trade Request Label"));
//
//		if(GUI.Button(new Rect(Screen.width * 0.235f, Screen.height * 0.55f, Screen.width * 0.22f, Screen.height * 0.085f),
//		              "Accept"))
//		{
//			manager.SendAcceptTrade();
//		}
//
//		if(GUI.Button(new Rect(Screen.width * 0.545f, Screen.height * 0.57f, Screen.width * 0.22f, Screen.height * 0.085f),
//		              "Cancel"))
//		{
//			manager.SendUnnaceptTrade();
//		}
//	}
//
//	protected void DrawTradeMessageWindow(int index)
//	{
//		DrawTemplateWindow();
//		GUI.Label(new Rect(Screen.width * 0.39f, Screen.height * 0.43f, Screen.width * 0.39f, Screen.height * 0.09f),
//		          "That item can't be traded!");
//				
//		if(GUI.Button(new Rect(Screen.width * 0.39f, Screen.height * 0.57f, Screen.width * 0.22f, Screen.height * 0.085f),
//		              "Ok"))
//		{
//			showMessage = false;
//		}
//	}
//
//	protected void DrawInsertGoldWindow(int index)
//	{
//		DrawTemplateWindow();
//		GUI.Label(new Rect(Screen.width * 0.39f, Screen.height * 0.38f, Screen.width * 0.39f, Screen.height * 0.09f),
//		          "Enter gold amount");
//		goldAmount = GUI.TextField(new Rect(Screen.width * 0.44f, Screen.height * 0.47f, Screen.width * 0.29f, Screen.height * 0.08f),
//		                           goldAmount);
//		goldAmount = Regex.Replace(goldAmount, @"[^0-9]", "");
//
//		if(GUI.Button(new Rect(Screen.width * 0.235f, Screen.height * 0.57f, Screen.width * 0.22f, Screen.height * 0.085f),
//		              "Accept"))
//		{
//			inputGold = false;
//		}
//		
//		if(GUI.Button(new Rect(Screen.width * 0.545f, Screen.height * 0.57f, Screen.width * 0.22f, Screen.height * 0.085f),
//		              "Cancel"))
//		{
//			goldAmount = "0";
//			inputGold = false;
//		}
//	}
//}
