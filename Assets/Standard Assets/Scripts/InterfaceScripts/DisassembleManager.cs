﻿using UnityEngine;
using System.Collections;

public class DisassembleManager {

	public string WindowName;
	public string DescriptionText;
	public DisassembleWindowType WindowType;

	public ItemButton ReagentItem;
	public string ItemName;
	public Player PlayerScript;

	private const uint EnchantingSpell = 13262;
	private const uint InscriptionSpell = 51005;
	private const uint JevelcraftingSpell = 31252;

	public void InitDataByWindowType(DisassembleWindowType windowType)
	{
		WindowType = windowType;
		switch(WindowType)
		{
		case DisassembleWindowType.ENCHANTINGE_WINDOW_TYPE:
			WindowName = "Scavenge";
			DescriptionText = "Disassemble rare or higher quality equipment or weapon item for re-forge components.";
			break;
		case DisassembleWindowType.INSCRIPTION_WINDOW_TYPE:
			WindowName = "Grain";
			DescriptionText = "Milling 5 units of herbs to create ink.";
			break;
		case DisassembleWindowType.JEWELCRAFTING_WINDOW_TYPE:
			WindowName = "Scooping";
			DescriptionText = "Grind 5 units of ore to find gems.";
			break;
		}

		ReagentItem = new ItemButton();
		ReagentItem.position = new Rect(Screen.width * 0.04035f, Screen.height * 0.505f,
		                                Screen.height * 0.165f, Screen.height * 0.165f);
	}

	public ItemButton IsTouchInReagentContainer(Vector2 touch)
	{
		if(ReagentItem.position.Contains(touch))
		{
			return ReagentItem;
		}
		return null;
	}

	public bool InsertItemToSlot(Item item)
	{
		switch(WindowType)
		{
		case DisassembleWindowType.ENCHANTINGE_WINDOW_TYPE:
			if(item.classType != ItemClass.ITEM_CLASS_ARMOR && item.classType != ItemClass.ITEM_CLASS_WEAPON)
			{
				return false;
			}
			else
			{
				if(CheckArmor(item) || CheckWeapon(item))
				{
					return false;
				}
				else if(item.quality == ItemQualities.ITEM_QUALITY_POOR ||
				        item.quality == ItemQualities.ITEM_QUALITY_NORMAL)
				{
					return false;
				}
			}
			break;
		case DisassembleWindowType.INSCRIPTION_WINDOW_TYPE:
			if((item.flags & (uint)ItemPrototypeFlags.ITEM_FLAG_MILLABLE) == 0)
			{
				return false;
			}
			break;
		case DisassembleWindowType.JEWELCRAFTING_WINDOW_TYPE:
			if((item.flags & (uint)ItemPrototypeFlags.ITEM_FLAG_PROSPECTABLE) == 0)
			{
				return false;
			}
			break;
		}

		string iconName = DefaultIcons.GetItemIcon(item.entry);
		iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
		ReagentItem.texture = Resources.Load<Texture> ("Icons/" + iconName);
		ReagentItem.item = item;

		ItemName = item.name + "\n";
		switch(WindowType)
		{
		case DisassembleWindowType.ENCHANTINGE_WINDOW_TYPE:
			ItemName += "Re-Forge level: " + item.reqDisenchantSkill * 2;
			break;
		case DisassembleWindowType.INSCRIPTION_WINDOW_TYPE:
			ItemName += "Scrollcrafting level: " + item.reqSkillRank * 2;
			break;
		case DisassembleWindowType.JEWELCRAFTING_WINDOW_TYPE:
			ItemName += "Gemcrafing level: " + item.reqSkillRank * 2;
			break;
		}

		return true;
	}

	private bool CheckArmor(Item item)
	{
		if(item.classType == ItemClass.ITEM_CLASS_ARMOR &&
		  (item.inventoryType == InventoryType.INVTYPE_NON_EQUIP ||
		   item.inventoryType == InventoryType.INVTYPE_BODY ||
		   item.inventoryType == InventoryType.INVTYPE_TABARD))
		{
			return true;
		}
		return false;
	}

	private bool CheckWeapon(Item item)
	{
		if(item.classType == ItemClass.ITEM_CLASS_WEAPON &&
		  (item.subClass == (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_obsolete ||
		   item.subClass == (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_EXOTIC ||
		   item.subClass == (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_EXOTIC2 ||
		   item.subClass == (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_MISC))
		{
			return true;
		}
		return false;
	}

	public bool Disassemble()
	{
		if(ReagentItem.item == null)
		{
			return false;
		}

		uint craftSkillValue = 0;
		switch(WindowType)
		{
		case DisassembleWindowType.ENCHANTINGE_WINDOW_TYPE:
			craftSkillValue = InterfaceManager.Instance.PlayerScript.craftManager.GetMainCraftLevelByID((uint)CraftSkill.SKILL_ENCHANTING);
			break;
		case DisassembleWindowType.INSCRIPTION_WINDOW_TYPE:
			craftSkillValue = InterfaceManager.Instance.PlayerScript.craftManager.GetMainCraftLevelByID((uint)CraftSkill.SKILL_INSCRIPTION);
			break;
		case DisassembleWindowType.JEWELCRAFTING_WINDOW_TYPE:
			craftSkillValue = InterfaceManager.Instance.PlayerScript.craftManager.GetMainCraftLevelByID((uint)CraftSkill.SKILL_JEWELCRAFTING);
			break;
		}

		if(craftSkillValue <= (ReagentItem.item.reqDisenchantSkill * 2))
		{
			InterfaceManager.Instance.DrawMessageWindow("Your craft level is to low.");
			return false;
		}

		switch(WindowType)
		{
		case DisassembleWindowType.ENCHANTINGE_WINDOW_TYPE:
			PlayerScript.playerSpellManager.CastSpell(EnchantingSpell, ReagentItem.item.guid,
			                                          PlayerScript.transform.position);
			break;
		case DisassembleWindowType.INSCRIPTION_WINDOW_TYPE:
			if(PlayerScript.itemManager.GetItemsTotalCount(ReagentItem.item.entry) < 5)
			{
				InterfaceManager.Instance.DrawMessageWindow("Ore count is to low.");
				return false;
			}
			PlayerScript.playerSpellManager.CastSpell(InscriptionSpell, ReagentItem.item.guid,
			                                          PlayerScript.transform.position);
			break;
		case DisassembleWindowType.JEWELCRAFTING_WINDOW_TYPE:
			if(PlayerScript.itemManager.GetItemsTotalCount(ReagentItem.item.entry) < 5)
			{
				InterfaceManager.Instance.DrawMessageWindow("Herb count is to low.");
				Debug.LogError("TODO!!!");
				return false;
			}
			PlayerScript.playerSpellManager.CastSpell(JevelcraftingSpell, ReagentItem.item.guid,
			                                          PlayerScript.transform.position);
			break;
		}
		return true;
	}
}