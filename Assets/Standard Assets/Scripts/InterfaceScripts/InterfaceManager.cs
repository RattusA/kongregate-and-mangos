﻿using UnityEngine;
using System.Collections;

public enum InterfaceWindowState {

	ACTION_BAR_PANEL_STATE,
	NPC_WINDOW_STATE,
	PLAYER_DIALOG_STATE,
	PLAYER_MENU_WINDOW_STATE,
	EMPTY_WINDOW_STATE
}

public class InterfaceManager : MonoBehaviour
{
	static public InterfaceManager Instance;

	public PlayerMenuManager playerMenuMng;
	public MessageWindow messageWnd;
	public CastBarManager castBarMng;
	public BeastLoreWindow beastLoreWindow;
	public DisassembleWindow DisassembleWnd;
	public NPCWindowManager npcWindowMng;
	public PlayerDialogWindowController playerDialogWindow;

	public bool hideInterface;
	public InterfaceWindowState wndState;

	public InventoryMessageWindow msgWindow;

	public Player PlayerScript;

	public void SetMainPlayer(Player playerScript)
	{
		PlayerScript = playerScript;
		playerMenuMng.SetMainPlayer(playerScript);
	}

	public void Awake()
	{
		DontDestroyOnLoad(this);
		Instance = this;
		wndState = InterfaceWindowState.ACTION_BAR_PANEL_STATE;
		hideInterface = false;

		//InventoryMessageWindow
		msgWindow = new InventoryMessageWindow();
	}

	private void Start()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		gameObject.AddComponent<TrialpayIntegration>();
#endif
	}

	private void SetHideInterface(bool val)
	{
		if(hideInterface != val)
		{
			Resources.UnloadUnusedAssets();
			hideInterface = val;
		}
	}

	public void DrawMessageWindow(string text)
	{
		messageWnd.gameObject.SetActive(true);
		messageWnd.InitOneButtonWindow(text);
	}

	public void SetWindowState(InterfaceWindowState newState)
	{
		switch(wndState)
		{
		case InterfaceWindowState.ACTION_BAR_PANEL_STATE:
			hideInterface = true;
			break;
		case InterfaceWindowState.PLAYER_MENU_WINDOW_STATE:
			playerMenuMng.gameObject.SetActive(false);
			break;
		}

		wndState = newState;

		switch(wndState)
		{
		case InterfaceWindowState.ACTION_BAR_PANEL_STATE:
			hideInterface = false;
			break;
		case InterfaceWindowState.NPC_WINDOW_STATE:
			break;
		case InterfaceWindowState.PLAYER_DIALOG_STATE:
			playerDialogWindow.gameObject.SetActive(true);
			break;
		case InterfaceWindowState.PLAYER_MENU_WINDOW_STATE:
			playerMenuMng.gameObject.SetActive(true);
			playerMenuMng.parentMng = this;
			break;
		}
	}

	public void OnGUI()
	{
		switch(wndState)
		{
		case InterfaceWindowState.NPC_WINDOW_STATE:
			npcWindowMng.DrawGUI();
			break;
		case InterfaceWindowState.PLAYER_DIALOG_STATE:
//			playerDialogWindow.DrawGUI();
			break;
		case InterfaceWindowState.PLAYER_MENU_WINDOW_STATE:
			playerMenuMng.DrawGUI();
			break;
		}

		GUI.depth = 1;
		if(DisassembleWnd != null && DisassembleWnd.gameObject.activeInHierarchy)
		{
			DisassembleWnd.DrawGUI();
		}
//
//		if(beastLoreWindow.gameObject.activeInHierarchy)
//		{
//			beastLoreWindow.DrawGUI();
//		}

		if(messageWnd.gameObject.activeInHierarchy)
		{
			messageWnd.DrawGUI();
		}

		if(castBarMng.gameObject.activeInHierarchy)
		{
			castBarMng.DrawGUI();
		}
		
		GUI.depth = 0;	
		if(msgWindow != null && !string.IsNullOrEmpty(msgWindow.inventoryMessage))
		{
			msgWindow.ShowInventoryMessage();
		}
	}

	public void SetInventoryMessage(string message)
	{
		msgWindow.inventoryMessage = message;
	}

	public void DrawDisassembleWindow(DisassembleWindowType windowType)
	{
		GameObject DisassembleGO = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/Disassemble Window"));
		if(DisassembleGO != null)
		{
			DisassembleGO.transform.parent = this.transform;
			DisassembleWnd = DisassembleGO.GetComponent<DisassembleWindow>();
			DisassembleWnd.SetWindowType(windowType);
			hideInterface = true;
		}
		else
		{
			Debug.LogError("Can't load Disassemble Window prefab.");
		}
	}
}
