﻿using UnityEngine;
using System;
using System.Collections.Generic;

public enum InfoWindowState {

	Inventory_PlayerInventory,
	Inventory_PlayerEquipment,
	Vendor_PlayerInventory,
	Vendor_VendorInventory,
	Banker_PlayerInventory,
	Banker_BankInventory,
	Guild_PlayerInventory,
	Guild_GuildBank,
	Blacksmith_Repair,
	Trade_PlayerInventory,
	Trade_PlayerSlots,
	Trade_TraderSlots,
	Glyphs_GlyphList,
	GLyphs_EquipedGlyph,
	Trainer_TrainerSpell,
	SpellBook_SpellOrItem,
	SpellBook_Macro
}

public enum InfoWindowButtonType {

	Close = 0,
	BuySell = 1,
	Move = 2,
	Delete = 3,
	EquipUnequip = 4,
	Use = 5,
	Open = 6,
	Gem = 7,
	ReForge = 8,
	Make = 9
}

public class InfoWindowButton {

	public InfoWindowButtonType type;
	public Item item;
	public SpellEntry spell;
	public string ButtonName = "";
	public Texture ButtonUpImage;
	public Texture ButtonDownImage;
	public event Action onButtonUp;
	public event Action<InfoWindowButton> onButtonUpWithSender;
	public event Action onButtonDown;
	public event Action<InfoWindowButton> onButtonDownWithSender;

	public Vector2 position;
	public float width;
	public float height;

	public void Draw(GUIStyle style)
	{
		if(GUI.Button(new Rect(position.x, position.y, width, height), ButtonName, style))
		{
			if(onButtonUp != null)
			{
				onButtonUp();
			}
			if(onButtonUpWithSender != null)
			{
				onButtonUpWithSender(this);
			}
			if(onButtonDown != null)
			{
				onButtonDown();
			}
			if(onButtonDownWithSender != null)
			{
				onButtonDownWithSender(this);
			}
		}
	}
}

public class InfoWindowButtonManager {

	public List<InfoWindowButton> ButtonList = new List<InfoWindowButton>();

	public void Sort()
	{
		ButtonList.Sort(delegate(InfoWindowButton left, InfoWindowButton right)
		{
			if (left == null && right == null) return 0;
			else if (left == null) return -1;
			else if (right == null) return 1;
			else return (left.type > right.type) ? 1 : 0;
		});
	}

	public InfoWindowButton AddFindButton(InfoWindowButtonType type)
	{
		InfoWindowButton ret = ButtonList.Find(element => (element.type == type));
		if(ret == null)
		{
			ret = new InfoWindowButton();
			ret.type = type;
			ButtonList.Add (ret);
		}
		return ret;
	}

	public void FillButtons(Item item, InfoWindowState parentWindowState)
	{
		// TODO fill button list for item info window
		InfoWindowButton button = AddFindButton(InfoWindowButtonType.Close);
		button.ButtonName = "Close";
		switch(parentWindowState)
		{
		case InfoWindowState.Inventory_PlayerInventory:
			button = AddFindButton(InfoWindowButtonType.Delete);
			button.ButtonName = "Delete";
			if(item.classType == ItemClass.ITEM_CLASS_ARMOR
			   || item.classType == ItemClass.ITEM_CLASS_CONTAINER
			   || item.inventoryType > (uint)InventoryType.INVTYPE_NON_EQUIP)
			{
				button = AddFindButton(InfoWindowButtonType.EquipUnequip);
				button.ButtonName = "Equip";
				button.onButtonUp += item.EquipItem;
			}
			if((item.flags & (uint)ItemPrototypeFlags.ITEM_FLAG_USABLE) > 0
			   || item.classType == ItemClass.ITEM_CLASS_RECIPE
			   || item.classType == ItemClass.ITEM_CLASS_CONSUMABLE)
			{
				if(item.isPermanentEnchant || item.isTemporaryEnchant || item.isPrismaticSlotEnchant)
				{
					button = AddFindButton(InfoWindowButtonType.ReForge);
					button.ButtonName = "Re-Forge";
				}
				else
				{
					button = AddFindButton(InfoWindowButtonType.Use);
					button.ButtonName = "Use";
					button.onButtonUp += item.UseItem;
				}
			}
			if((item.flags & (uint)ItemPrototypeFlags.ITEM_FLAG_LOOTABLE) > 0)
			{
				button = AddFindButton(InfoWindowButtonType.Open);
				button.ButtonName = "Open";
				button.onButtonUp += item.UseItem;
			}
			if(item.socket[0].color != 0 || item.HasPrismaticSlot())
			{
				button = AddFindButton(InfoWindowButtonType.Gem);
				button.ButtonName = "Gem";
			}
			break;
		case InfoWindowState.Inventory_PlayerEquipment:
			button = AddFindButton(InfoWindowButtonType.EquipUnequip);
			button.ButtonName = "Unequip";
			break;
		case InfoWindowState.Vendor_PlayerInventory:
			button = AddFindButton(InfoWindowButtonType.Delete);
			button.ButtonName = "Delete";
			if(item.classType != ItemClass.ITEM_CLASS_QUEST)
			{
				button = AddFindButton(InfoWindowButtonType.BuySell);
				button.ButtonName = "Sell";
			}
			break;
		case InfoWindowState.Vendor_VendorInventory:
			button = AddFindButton(InfoWindowButtonType.BuySell);
			button.ButtonName = "Buy";
			break;
		case InfoWindowState.Banker_PlayerInventory:
		case InfoWindowState.Banker_BankInventory:
			button = AddFindButton(InfoWindowButtonType.Delete);
			button.ButtonName = "Delete";
			button = AddFindButton(InfoWindowButtonType.Move);
			button.ButtonName = "Move";
			break;
		}
//		Sort();
	}

	public void FillButtons(SpellEntry spell, InfoWindowState parentWindowState)
	{
		// TODO fill button list for spell info window
	}
}
