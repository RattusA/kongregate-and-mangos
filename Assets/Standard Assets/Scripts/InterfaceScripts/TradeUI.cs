using UnityEngine;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public class TradeUI
{
	private TradeManager tm;
	List<Item> items;
	public List<Item> targetItems;
	UIAbsoluteLayout myContainer;
	UIAbsoluteLayout targetContainer;
	public uint targetGold;
	int targetItem;
	public byte showType;
	byte wannaBeShowType;
	
	private MessageWnd messageWnd;
	private byte i;
	private Vector2 itemDescVector;
	
	private UIAbsoluteLayout tradeContainer;
	private UIAbsoluteLayout itemContainer;
	private UIButton closeXButton;
	private UIButton closeButton;
	private UISprite tradeDecoration;
	private UISprite tradeBackground;
	private UIButton tradeAccept;
	private UIButton tradeDecline;
	private UITextInstance[] tradeTexts;
	private UITextInstance[] itemTexts;
	private UISprite targetTradePanel;
	private UISprite myTradePanel;
	private UIButton addGoldButton;
	
	private Vector3 tempItmPos;
	private UIButton selectedItem;
	
	float slotWidth;
	float itemVPos;
	float myHPos;
	float targetHPos;

	string myGold;
	uint myGoldInt;
	byte slot;
	
	public TradeUI ( TradeManager tradeMgr )
	{
		myGold = "";
		myGoldInt = 0;
		messageWnd = new MessageWnd();
		itemDescVector = new Vector2();
		tm = tradeMgr;
		items = new List<Item>();
		targetItems = new List<Item>();
		i = 0;
		showType = 0;
		wannaBeShowType = 0;
		slot = 0;
		targetGold = 0;
		targetItem = 0;
		
		slotWidth = Screen.width * 0.08f;
		itemVPos = -Screen.height * 0.16f;
		myHPos = Screen.width * 0.1f;
		targetHPos = Screen.width * 0.3f;
	}
	
	public void  Enable ()
	{
		tm.mainPlayer.blockTargetChange = true;
		WorldSession.interfaceManager.hideInterface = true;
		tm.mainPlayer.interf.stateManager();
		
		if(tradeContainer != null)
			tradeContainer.removeAllChild(true);
		else
			tradeContainer = new UIAbsoluteLayout();
		
		if(itemContainer != null)
			itemContainer.removeAllChild(true);
		else
			itemContainer = new UIAbsoluteLayout();
		
		ClearMyTradeSlots();
		
		ClearTargetTradeSlots();
		
		if(tradeTexts != null)
		{
			for(i = 0 ; i < 10 ; i++)
				if(tradeTexts[i] != null)
			{
				tradeTexts[i].clear();
				tradeTexts[i] = null;
			}
		}
		else 
			tradeTexts = new UITextInstance[10];
		
		if(itemTexts != null)
		{
			for(i = 0 ; i < 10 ; i++)
				if(itemTexts[i] != null)
			{
				itemTexts[i].clear();
				itemTexts[i] = null;
			}
		}
		else 
			itemTexts = new UITextInstance[10];	
	}
	
	public void  Disable ()
	{
		showType = 0;
		wannaBeShowType = 0;
		WorldSession.interfaceManager.hideInterface = false;
		tm.mainPlayer.interf.stateManager();
		tm.mainPlayer.blockTargetChange = false;
		closeWindow();
	}
	
	void  ReplaceItem ( Item item )
	{
		for(i = 0; i < targetItems.Count; i++)
		{
			if(targetItems[i].entry == item.entry)
			{
				//				item.stackCount = targetItems[i].stackCount;
				item.GetItemBasicStats();
				targetItems[i] = item;
			}
		}
	}
	
	public void  Show ()
	{
		if (ButtonOLD.menuState)
		{
			Disable();
			tm.SendCancelTrade();
			showType = 0;
		}
		
		switch(showType)
		{
		case 2:
			ShowMainTradeWindow();
			break;
		case 4:
			goldMessageWindow();
			break;
		case 5:
			ShowMainTradeWindow();
			break;
		}
	}
	
	public void  ShowAccepted ()
	{
		showType = 5;
	}
	
	void  itemDelegate ( UIButton sender )
	{
		if(items.Count > 5)
			return;
		if(tm.mainPlayer.inv.currentBag == 0)
		{
			for(i = 0; i < items.Count; i++)
				if(items[i].guid == tm.mainPlayer.itemManager.BaseBagsSlots[sender.info].guid)
					return;
			if(tm.mainPlayer.itemManager.BaseBagsSlots[sender.info].SoulBound() || tm.mainPlayer.itemManager.BaseBagsSlots[sender.info].AccountBound())
			{
				TradeMessage("That item can't be traded!");
				return;
			}
			items.Add(tm.mainPlayer.itemManager.BaseBagsSlots[sender.info]);
			tm.mainPlayer.inv.itemContainer._children[sender.info].color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
			byte bag = 255;
			tm.SendSetTradeItem(slot, bag, (byte)(23 + sender.info));
			slot++;
			showType = 2;
		}
		else
		{
			Item item;
			for(i = 0; i < items.Count; i++)
			{
				item = tm.mainPlayer.itemManager.BaseBags[tm.mainPlayer.inv.currentBag].GetItemFromSlot(sender.info);
				if(item != null && items[i].guid == item.guid)
				{
					return; 
				}
			}
			
			item = tm.mainPlayer.itemManager.BaseBags[tm.mainPlayer.inv.currentBag].GetItemFromSlot(sender.info);
			if(item == null || item.SoulBound() || item.AccountBound())
			{
				TradeMessage("That item can't be traded!");
				return;
			}
			
			items.Add(item);
			tm.mainPlayer.inv.itemContainer._children[sender.info].color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
			tm.SendSetTradeItem(slot, (byte)(tm.mainPlayer.inv.currentBag + 18), (byte)sender.info);
			slot++;
			showType = 2;
		}
	}
	
	public void  showTradeRequest ()
	{
		Enable();
		TradeRequested();
	}
	
	void  TradeDeclineDelegate ( UIButton sender )
	{
		Disable();
		tm.SendCancelTrade();
		DestroyCurrentWindow();
	}
	
	void  TradeAcceptDelegate ( UIButton sender )
	{
		ButtonOLD.chatState = false;
		tm.SendAcceptTrade();
		showType = 2;
	}
	
	public void  openWindow ()
	{	
		tm.mainPlayer.inv.isActive = true;
		tm.mainPlayer.inv.SetDelegate(itemDelegate);
		tm.mainPlayer.inv.ShowBags();
		tm.mainPlayer.inv.ShowItems();
		showType = 2;
	}
	
	public void  closeWindow ()
	{
		if(tm.mainPlayer.inv.isActive)
		{
			tm.mainPlayer.inv.ClearItems();
			tm.mainPlayer.inv.ClearBags();
			tm.mainPlayer.inv.isActive = false;
			tm.mainPlayer.inv.currentBag = 0;
			tm.mainPlayer.inv.SetDelegate(null);
		}
		DestroyCurrentWindow();
		items.Clear();
		targetItems.Clear();
		targetGold = 0;
		myGold = "";
		myGoldInt = 0;
		targetItem = 0;
		slot = 0;
		tm.accepted = false;
	}
	
	void  goldMessageWindow ()
	{
		myGold = GUI.TextField( new Rect(Screen.width*0.32f , Screen.height*0.53f , Screen.width*0.3f , Screen.height*0.06f), myGold);
		myGold = Regex.Replace(myGold, "[^0-9]", "");
	}
	
	void  showMyTradeSlots ()
	{
		string image;
		UIButton tempButton;
		ClearMyTradeSlots();
		i = 0;
		Item item;
		float itmWidth = tm.mainPlayer.inv.itemWidth*0.903f;
		float itmHeight = tm.mainPlayer.inv.itemHeight*0.91f;
		Debug.Log("NUMARUL DE ITEME TRIMISE : " + items.Count);
		UIToolkit tempButtonUIToolkit;
		for(i = 0; i < items.Count; i++)
		{
			item = items[i];
			
			if(item != null && item.entry > 0)
			{
				image = DefaultIcons.GetItemIcon(item.entry);
			}
			else continue;
			
			tempButtonUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
			tempButton = UIButton.create( tempButtonUIToolkit, image, image, 0, 0);
			tempButton.setSize(itmWidth, itmHeight);
			tempButton.position = new Vector3(Screen.width*0.028f + i * itmWidth, -Screen.height*0.281f , 0);
			tempButton.info = i;
			tempButton.onTouchDown += myItemDelegate;
			
			myContainer.addChild(tempButton);
		}
		
		image = "fundal.png";
		
		for(i = 0; i < 6; i++)
		{
			tempButtonUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
			tempButton = UIButton.create( tempButtonUIToolkit, image, image, 0, 0);
			tempButton.setSize(itmWidth, itmHeight);
			tempButton.position = new Vector3(Screen.width*0.028f + i * itmWidth, -Screen.height*0.281f , 1);
			tempButton.onTouchDown += null;
			
			myContainer.addChild(tempButton);
		}
	}
	
	public void  showTargetTradeSlots ()
	{
		string image;
		UIButton tempButton;
		ClearTargetTradeSlots();
		i = 0;
		Item item;
		float itmWidth = tm.mainPlayer.inv.itemWidth*0.903f;
		float itmHeight = tm.mainPlayer.inv.itemHeight*0.91f;
		Debug.Log("NUMARUL DE ITEME PRIMITE : " + targetItems.Count);
		UIToolkit tempButtonUIToolkit;
		for(i = 0; i < targetItems.Count; i++)
		{
			item  = targetItems[i];
			
			if(item != null && item.entry > 0)
			{
				image = DefaultIcons.GetItemIcon(item.entry);
			}
			else
			{
				continue;
			}
			
			tempButtonUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
			tempButton = UIButton.create( tempButtonUIToolkit, image, image, 0, 0);
			tempButton.setSize(itmWidth, itmHeight);
			tempButton.position = new Vector3(Screen.width*0.028f + i * itmWidth, -Screen.height*0.681f , 0);
			tempButton.info = i;
			tempButton.onTouchDown += targetItemDelegate;
			
			targetContainer.addChild(tempButton);
		}
		
		image = "fundal.png";
		
		for(i = 0; i < 6; i++)
		{
			tempButtonUIToolkit = SinglePictureUtils.CreateUIToolkit("Icons/"+image);
			tempButton = UIButton.create( tempButtonUIToolkit, image, image, 0, 0);
			tempButton.setSize(itmWidth, itmHeight);
			tempButton.position = new Vector3(Screen.width*0.028f + i * itmWidth, -Screen.height*0.681f , 1);
			tempButton.onTouchDown += null;
			
			targetContainer.addChild(tempButton);
		}
	}
	
	void  TradeRequested ()
	{
		showType = 0;
		DestroyCurrentWindow();
		
		tradeBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.3f , 10);
		tradeBackground.setSize(Screen.width*0.55f , Screen.height*0.45f);
		
		tradeDecoration = UIPrime31.questMix.addSprite("ornament4.png" , tradeBackground.position.x + tradeBackground.width*0.05f , -tradeBackground.position.y + tradeBackground.height*0.22f , 9);
		tradeDecoration.setSize(tradeBackground.width*0.9f , tradeBackground.height*0.33f);
		
		tradeAccept = UIButton.create(UIPrime31.questMix , "accept_button.png" , "accept_button.png" , tradeBackground.position.x + tradeBackground.width*0.57f , -tradeBackground.position.y + tradeBackground.height*0.7f , 9);
		tradeAccept.setSize(tradeBackground.width*0.33f , tradeBackground.height*0.15f);
		tradeAccept.onTouchDown += TradeAcceptDelegate;
		
		tradeDecline = UIButton.create(UIPrime31.questMix , "decline_button.png" , "decline_button.png" , tradeBackground.position.x + tradeBackground.width*0.1f , -tradeBackground.position.y + tradeBackground.height*0.7f , 9);
		tradeDecline.setSize(tradeBackground.width*0.33f , tradeBackground.height*0.15f);
		tradeDecline.onTouchDown += TradeDeclineDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(tradeBackground.width*0.1f , tradeBackground.width*0.1f);
		closeXButton.position = new Vector3( tradeBackground.position.x + tradeBackground.width - closeXButton.width/1.5f , tradeBackground.position.y + closeXButton.height/5 , 9);
		closeXButton.onTouchDown += TradeDeclineDelegate;
		
		tradeContainer.addChild(tradeBackground , tradeDecoration , tradeAccept , tradeDecline , closeXButton);
		
		tradeTexts[0] = tm.mainPlayer.interf.text3.addTextInstance("Trade requested" , tradeBackground.position.x + tradeBackground.width * 0.57f , -tradeBackground.position.y + tradeBackground.height*0.3f , UID.TEXT_SIZE*0.9f , 9 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
	}
	
	void  ShowMainTradeWindow ()
	{
		wannaBeShowType = showType;
		showType = 0;
		DestroyCurrentWindow();
		showMyTradeSlots();
		showTargetTradeSlots();
		
		tradeBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.48f , Screen.height*0.819f , 5);
		tradeBackground.setSize(Screen.width*0.36f , Screen.height*0.18f);
		
		tradeAccept = UIButton.create(UIPrime31.interfMix , "accept_button2.png" , "accept_button2.png" , tradeBackground.position.x + tradeBackground.width*0.06f , -tradeBackground.position.y + tradeBackground.height*0.29f , 4);
		tradeAccept.setSize(tradeBackground.width*0.4f , tradeBackground.height*0.45f);
		tradeAccept.onTouchDown += MainAcceptDelegate;
		
		tradeDecline = UIButton.create(UIPrime31.interfMix , "cancel_button2.png" , "cancel_button2.png" , tradeBackground.position.x + tradeBackground.width*0.54f , -tradeBackground.position.y + tradeBackground.height*0.29f , 4);
		tradeDecline.setSize(tradeBackground.width*0.4f , tradeBackground.height*0.45f);
		tradeDecline.onTouchDown += MainDeclineDelegate;
		
		myTradePanel = UIPrime31.interfMix.addSprite("trade_panel.png" , 0 , Screen.height*0.2f , 5);
		myTradePanel.setSize(Screen.width*0.47f , Screen.height*0.36f);
		
		targetTradePanel = UIPrime31.interfMix.addSprite("trade_panel.png" , 0 , Screen.height*0.6f , 5);
		targetTradePanel.setSize(Screen.width*0.47f , Screen.height*0.36f);
		
		addGoldButton = UIButton.create(UIPrime31.interfMix , "add_gold_button.png" , "add_gold_button.png" , myTradePanel.position.x + myTradePanel.width*0.05f , -myTradePanel.position.y + myTradePanel.height*0.71f , 4);
		addGoldButton.setSize(myTradePanel.width*0.34f , myTradePanel.height*0.2f);
		addGoldButton.onTouchDown += AddGoldDelegate;
		
		tradeContainer.addChild(tradeBackground , tradeAccept , tradeDecline , myTradePanel , targetTradePanel , addGoldButton);
		
		tradeTexts[0] = tm.mainPlayer.interf.text1.addTextInstance(targetGold.ToString() , targetTradePanel.position.x + targetTradePanel.width * 0.7f , -targetTradePanel.position.y + targetTradePanel.height*0.8f , UID.TEXT_SIZE*0.8f , 3 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		
		tradeTexts[1] = tm.mainPlayer.interf.text1.addTextInstance(myGold , myTradePanel.position.x + myTradePanel.width * 0.7f , -myTradePanel.position.y + myTradePanel.height*0.8f , UID.TEXT_SIZE*0.8f , 3 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);
		
		tradeTexts[2] = tm.mainPlayer.interf.text3.addTextInstance(tm.mainPlayer.name , myTradePanel.position.x + myTradePanel.width * 0.33f , -myTradePanel.position.y + myTradePanel.height*0.01f , UID.TEXT_SIZE*0.8f , 3 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);	
		
		tradeTexts[3] = tm.mainPlayer.interf.text3.addTextInstance(tm.targetPlayer.name , targetTradePanel.position.x + targetTradePanel.width * 0.33f , -targetTradePanel.position.y + targetTradePanel.height*0.01f , UID.TEXT_SIZE*0.8f , 3 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);	
		
		if(wannaBeShowType == 5)
		{
			tradeTexts[4] = tm.mainPlayer.interf.text2.addTextInstance("Trade\naccepted" , targetTradePanel.position.x + targetTradePanel.width * 0.2f , -targetTradePanel.position.y + targetTradePanel.height*0.8f , UID.TEXT_SIZE*0.8f , 3 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);	
		}
	}
	
	void  ShowGoldAddWindow ()
	{
		showType = 4;
		DestroyCurrentWindow();
		
		tradeBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.3f , 5);
		tradeBackground.setSize(Screen.width*0.55f , Screen.height*0.45f);
		
		tradeDecoration = UIPrime31.questMix.addSprite("ornament4.png" , tradeBackground.position.x + tradeBackground.width*0.05f , -tradeBackground.position.y + tradeBackground.height*0.1f , 4);
		tradeDecoration.setSize(tradeBackground.width*0.9f , tradeBackground.height*0.33f);
		
		tradeAccept = UIButton.create(UIPrime31.questMix , "accept_button.png" , "accept_button.png" , tradeBackground.position.x + tradeBackground.width*0.57f , -tradeBackground.position.y + tradeBackground.height*0.8f , 4);
		tradeAccept.setSize(tradeBackground.width*0.33f , tradeBackground.height*0.15f);
		tradeAccept.onTouchDown += GoldAcceptDelegate;
		
		tradeDecline = UIButton.create(UIPrime31.questMix , "decline_button.png" , "decline_button.png" , tradeBackground.position.x + tradeBackground.width*0.1f , -tradeBackground.position.y + tradeBackground.height*0.8f , 4);
		tradeDecline.setSize(tradeBackground.width*0.33f , tradeBackground.height*0.15f);
		tradeDecline.onTouchDown += GoldDeclineDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(tradeBackground.width*0.1f , tradeBackground.width*0.1f);
		closeXButton.position = new Vector3( tradeBackground.position.x + tradeBackground.width - closeXButton.width/1.5f , tradeBackground.position.y + closeXButton.height/5 , 4);
		closeXButton.onTouchDown += GoldDeclineDelegate;
		
		tradeContainer.addChild(tradeBackground , tradeDecoration , tradeAccept , tradeDecline , closeXButton);
		
		tradeTexts[0] = tm.mainPlayer.interf.text3.addTextInstance("Enter gold amount" , tradeBackground.position.x + tradeBackground.width * 0.57f , -tradeBackground.position.y + tradeBackground.height*0.1f , UID.TEXT_SIZE*0.9f , 4 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Top);	
	}
	
	void  ShowItemDetails ()
	{
		FreezeCurrentWindow(true);
		DestroyItemWindow();
		
		tradeBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.1f , Screen.height*0.25f , -3);
		tradeBackground.setSize(Screen.width*0.7f , Screen.height*0.7f);
		
		selectedItem.position = new Vector3( tradeBackground.position.x + tradeBackground.width*0.03f , tradeBackground.position.y - tradeBackground.height*0.17f , -4);
		tradeDecoration = UIPrime31.questMix.addSprite("decoration_case.png" , selectedItem.position.x - selectedItem.width*0.03f , -selectedItem.position.y - selectedItem.height*0.03f , -5);
		tradeDecoration.setSize(selectedItem.width*1.2f , selectedItem.height*1.2f);
		itemContainer.addChild(tradeDecoration);
		
		tradeDecoration = UIPrime31.interfMix.addSprite("vendor_decoration.png" , tradeBackground.position.x + tradeBackground.width*0.025f , -tradeBackground.position.y + tradeBackground.height*0.05f , -4);
		tradeDecoration.setSize(tradeBackground.width*0.95f , tradeBackground.height*0.1f);
		itemContainer.addChild(tradeBackground , tradeDecoration);
		
		tradeDecoration = UIPrime31.interfMix.addSprite("vendor_decoration1.png" , tradeBackground.position.x + tradeBackground.width*0.015f , -tradeBackground.position.y + tradeBackground.height*0.25f , -4);
		tradeDecoration.setSize(tradeBackground.width*0.25f , tradeBackground.height*0.45f);
		itemContainer.addChild(tradeDecoration);
		
		tradeDecoration = UIPrime31.questMix.addSprite("background_case.png" , tradeBackground.position.x + tradeBackground.width*0.27f , -tradeBackground.position.y + tradeBackground.height*0.17f , -4);
		tradeDecoration.setSize(tradeBackground.width*0.7f , tradeBackground.height*0.8f);
		itemContainer.addChild(tradeDecoration);
		
		closeButton = UIButton.create(UIPrime31.questMix , "close_button.png" , "close_button.png" , tradeBackground.position.x + tradeBackground.width*0.03f , -tradeBackground.position.y + tradeBackground.height*0.85f , -4);
		closeButton.setSize(tradeBackground.width*0.22f , tradeBackground.height*0.093f);
		closeButton.onTouchDown += CloseItemDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(tradeBackground.width*0.06f , tradeBackground.width*0.06f);
		closeXButton.position = new Vector3( tradeBackground.position.x + tradeBackground.width - closeXButton.width/1.5f , tradeBackground.position.y + closeXButton.height/5 , -6);
		closeXButton.onTouchDown += CloseItemDelegate;
		
		itemContainer.addChild(tradeBackground , closeButton , closeXButton);
		
		itemTexts[0] = tm.mainPlayer.interf.text3.addTextInstance(targetItems[targetItem].stats.Substring(0 , targetItems[targetItem].stats.IndexOf("\n" , 0)) , tradeBackground.position.x + tradeBackground.width * 0.12f , -tradeBackground.position.y + tradeBackground.height*0.04f , UID.TEXT_SIZE*0.8f , -6 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
		
		itemTexts[1] = tm.mainPlayer.interf.text1.addTextInstance(targetItems[targetItem].stats.Substring(targetItems[targetItem].stats.IndexOf("\n" , 0) + 1 , targetItems[targetItem].stats.Length - targetItems[targetItem].stats.IndexOf("\n" , 0) - 1) , tradeDecoration.position.x + tradeDecoration.width * 0.06f , -tradeDecoration.position.y + tradeDecoration.height*0.05f , UID.TEXT_SIZE*0.7f , -6 , Color.white , UITextAlignMode.Left , UITextVerticalAlignMode.Top);
	}
	
	void  TradeMessage ( string message )
	{
		tm.mainPlayer.inv.Hide();
		DestroyCurrentWindow();
		
		tradeBackground = UIPrime31.myToolkit4.addSprite("popup_background.png" , Screen.width*0.2f , Screen.height*0.3f , -5);
		tradeBackground.setSize(Screen.width*0.55f , Screen.height*0.45f);
		
		tradeDecoration = UIPrime31.questMix.addSprite("ornament4.png" , tradeBackground.position.x + tradeBackground.width*0.03f , -tradeBackground.position.y + tradeBackground.height*0.1f , -6);
		tradeDecoration.setSize(tradeBackground.width*0.9f , tradeBackground.height*0.33f);
		
		closeButton = UIButton.create(UIPrime31.myToolkit3 , "button_OK.png" , "button_OK.png" , tradeBackground.position.x + tradeBackground.width*0.3f , -tradeBackground.position.y + tradeBackground.height*0.75f , -6);
		closeButton.setSize(tradeBackground.width*0.4f , tradeBackground.height*0.16f);
		closeButton.onTouchDown += CloseMessageDelegate;
		
		closeXButton = UIButton.create( UIPrime31.myToolkit4 , "close_button.png", "close_button.png", 0 , 0 , 0);
		closeXButton.setSize(tradeBackground.width*0.1f , tradeBackground.width*0.1f);
		closeXButton.position = new Vector3( tradeBackground.position.x + tradeBackground.width - closeXButton.width/1.5f , tradeBackground.position.y + closeXButton.height/5 , -6);
		closeXButton.onTouchDown += CloseMessageDelegate;
		
		tradeContainer.addChild(tradeBackground , tradeDecoration , closeButton , closeXButton);
		
		tradeTexts[7] = tm.mainPlayer.interf.text2.addTextInstance("Warning!" , tradeBackground.position.x + tradeBackground.width * 0.56f , -tradeBackground.position.y + tradeBackground.height*0.17f , UID.TEXT_SIZE*0.9f , -6 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
		
		tradeTexts[8] = tm.mainPlayer.interf.text3.addTextInstance(message , tradeBackground.position.x + tradeBackground.width * 0.5f , -tradeBackground.position.y + tradeBackground.height*0.59f , UID.TEXT_SIZE*0.8f , -6 , Color.white , UITextAlignMode.Center , UITextVerticalAlignMode.Middle);
	}
	
	void  CloseMessageDelegate ( UIButton sender )
	{
		showType = wannaBeShowType;
		tm.mainPlayer.inv.UnHide();
	}
	
	void  CloseItemDelegate ( UIButton sender )
	{
		selectedItem.position = tempItmPos;
		DestroyItemWindow();
		FreezeCurrentWindow(false);
	}
	
	void  DestroyItemWindow ()
	{
		itemContainer.removeAllChild(true);
		for(i=0 ; i < 3 ; i++)
		{
			if (itemTexts[i] != null)
			{
				itemTexts[i].clear();
				itemTexts[i] = null;
			}
		}
	}
	
	void  GoldAcceptDelegate ( UIButton sender )
	{
		myGoldInt = uint.Parse(myGold);
		if(myGoldInt >= 0)
			tm.SendSetTradeGold(myGoldInt);
		tm.mainPlayer.inv.UnHide();
		showType = 2;
	}
	
	void  GoldDeclineDelegate ( UIButton sender )
	{
		tm.mainPlayer.inv.UnHide();
		showType = 2;
	}
	
	void  MainAcceptDelegate ( UIButton sender )
	{
		tm.SendAcceptTrade();
	}
	
	void  MainDeclineDelegate ( UIButton sender )
	{
		Disable();
		tm.SendCancelTrade();
		showType = 0;
	}
	
	void  AddGoldDelegate ( UIButton sender )
	{
		DestroyCurrentWindow();
		tm.mainPlayer.inv.Hide();
		ShowGoldAddWindow();
	}
	
	void  DestroyCurrentWindow ()
	{
		tradeContainer.removeAllChild(true);
		for (i=0 ; i<10 ; i++)
		{
			if(tradeTexts[i]!=null)
			{
				tradeTexts[i].clear();
				tradeTexts[i] = null;
			}
		}
		ClearTargetTradeSlots();
		ClearMyTradeSlots();
	}
	
	void  FreezeCurrentWindow ( bool val )
	{
		foreach(var child in tradeContainer._children)
		{
			if(child is UIButton)
			{	
				(child as UIButton).disabled = val;
			}
		}
		foreach(var child in targetContainer._children)
		{
			if(child is UIButton)
			{	
				(child as UIButton).disabled = val;
			}
		}
		foreach(var child in myContainer._children)
		{
			if(child is UIButton)
			{	
				(child as UIButton).disabled = val;
			}
		}
		foreach(var child in tm.mainPlayer.inv.itemContainer._children)
		{
			if(child is UIButton)
			{	
				(child as UIButton).disabled = val;
			}
		}
	}
	
	void  ClearMyTradeSlots ()
	{
		if(myContainer != null)
		{
			while(myContainer._children.Count > 0)
			{
				UISprite temp = myContainer._children[0];
				UIToolkit tempUIToolkit = temp.manager;
				myContainer.removeChild(temp, false);
				temp.destroy();
				if(tempUIToolkit)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
		}
		else
		{
			myContainer = new UIAbsoluteLayout();
		}
	}
	
	void  ClearTargetTradeSlots ()
	{
		if(targetContainer != null)
		{
			while(targetContainer._children.Count > 0)
			{
				UISprite temp = targetContainer._children[0];
				UIToolkit tempUIToolkit = temp.manager;
				targetContainer.removeChild(temp, false);
				temp.destroy();
				if(tempUIToolkit)
				{
					GameObject.DestroyImmediate(tempUIToolkit.gameObject, true);
				}
			}
		}
		else
		{
			targetContainer = new UIAbsoluteLayout();
		}
	}
	
	void  myItemDelegate ( UIButton sender )
	{
		tm.SendClearTradeItem((byte)sender.info);
		
		if(tm.mainPlayer.inv.currentBag == 0)
		{
			foreach(KeyValuePair<int, Item> itemSlot in WorldSession.player.itemManager.BaseBagsSlots)
			{
				if(items[sender.info].guid == tm.mainPlayer.itemManager.BaseBagsSlots[itemSlot.Key].guid)
				{
					tm.mainPlayer.inv.itemContainer._children[itemSlot.Key].color = new Color(1, 1, 1, 0.5f);
				}
			}
		}
		else
		{
			Bag bag = WorldSession.player.itemManager.BaseBags[tm.mainPlayer.inv.currentBag];
			for(i = 0; i < bag.GetCapacity(); i++)
			{
				Item item = bag.GetItemFromSlot(i);
				if(item != null && items[sender.info].guid == item.guid)
				{
					tm.mainPlayer.inv.itemContainer._children[i].color = new Color(1, 1, 1, 0.5f);
				}
			}
		}
		items.RemoveAt(sender.info);
		showType = 2;
	}
	
	void  targetItemDelegate ( UIButton sender )
	{
		targetItem = sender.info;
		tempItmPos = sender.position;
		selectedItem = sender;
		ShowItemDetails();
	}
}