using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using System.Collections;

using System.Security.Cryptography;

public class LoginWindow : MonoBehaviour
{
	[SerializeField]
	protected GUISkin MainSkin;

	public static LoginWindow Instance;

	public Sprite loginBackground;
	public Sprite loginBackgroundIOS;

	public Image backgroundImage;

	public GameObject newsPanel;
	public GameObject loginPanel;
	public GameObject registerPanel;
	public GameObject resetPasswordPanel;
	public GameObject kongregatePanel;

	public RegisterHandler registerHandler;

	public InputField emailTextField;
	public InputField passwordTextField;
	public Toggle rememberAccToggle;
	public GameObject quitButtonGO;

	public static bool StopLogin = false;

	public static InventoryMessageWindow msgWindow;

	public static GameObject StoreKitManagerGO = null;
	public static GameObject StoreKitListenerGO = null;
	public static GameObject AmazonManagerGO = null;
	public static GameObject AmazonListenerGO = null;
	public static GameObject GplayManagerGO = null;
	public static GameObject GplayListenerGO = null;
	public static GameObject MusicManagerGO = null;

	private bool checkingVersion = false;
	
	public static string clientVersion = "";
	public static string accountLogin = "";
	public static string accountPassword = "";
	public static string passHash = "";
	
	public static string TEMP_ACC_NAME = "temp_acc_name";
	public static string TEMP_ACC_PASS = "temp_acc_pass";
	
	
	
	private string _updateURL = "";
	
#if UNITY_IPHONE
	private bool canRun;
	private bool needMoreMem;
#endif

	private GameObject InstantiatePrefab(string path, string name, bool replaceName)
	{
		GameObject ret = null;
		GameObject prefabGO = GameResources.Load<GameObject>(path);
		if(prefabGO != null)
		{
			ret = (GameObject)Instantiate(prefabGO, Vector3.zero, Quaternion.identity);
			if(replaceName)
			{
				ret.name = name;
			}
		}
		else
		{
			Debug.LogError("LoadZoneManager prefab missing.");
		}
		return ret;
	}

	public void Awake()
	{
		Instance = this;
#if UNITY_IPHONE || UNITY_STANDALONE_OSX
//		if(StoreKitManagerGO == null)
//		{
//			StoreKitManagerGO = InstantiatePrefab("prefabs/StoreKit/StoreKitManager", "StoreKitManager", false);
//			DontDestroyOnLoad(StoreKitManagerGO);
//		}

		if(StoreKitListenerGO == null)
		{
			StoreKitListenerGO = InstantiatePrefab("prefabs/StoreKit/StoreKitEventListener", "StoreKitEventListener", false);
			DontDestroyOnLoad(StoreKitListenerGO);
		}
#endif
		
#if UNITY_ANDROID
		if(BuildConfig.platformName == "amazon")
		{
			if(AmazonManagerGO == null)
			{
				AmazonManagerGO = InstantiatePrefab("prefabs/Amazon/AmazonIAPManager", "AmazonIAPManager", false);
				DontDestroyOnLoad(AmazonManagerGO);
			}

			if(AmazonListenerGO = null)
			{
				AmazonListenerGO = InstantiatePrefab("prefabs/Amazon/AmazonIAPEventListener", "AmazonIAPEventListener", true);
				DontDestroyOnLoad(AmazonListenerGO);
			}
		}
#endif
		
#if UNITY_IPHONE
		if(UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone ||
		   UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone3G ||
		   UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch1Gen ||
		   UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch2Gen)
		{
			canRun = false;
			needMoreMem = false;
		}
		else if((UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone3GS ||
		         UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch3Gen ||
		         UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch4Gen ||
		         UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad1Gen) &&
		        Input.location.isEnabledByUser)
		{
			canRun = false;
			needMoreMem = true;
		}
		else
		{
			canRun = true;
#endif
			
#if !UNITY_IPHONE	
			MainSkin.textField.fontSize = (int)(Screen.height * 0.07f);
#endif

#if UNITY_IPHONE
		}
#endif
		if(GameObject.Find("LoadZoneManager") == null)
		{
			//InstantiatePrefab("prefabs/loginScene/LoadZoneManager", "LoadZoneManager", true);
			newsPanel.SetActive(true);
		}
		else if(kongregatePanel != null)
		{
			kongregatePanel.SetActive(true);
		}
		else
		{
			loginPanel.SetActive(true);
		}

		//if(MusicManagerGO == null)
		//{
		//	MusicManagerGO = InstantiatePrefab("prefabs/MusicManager", "MusicManager", false);
		//	DontDestroyOnLoad(MusicManagerGO);
		//}
		msgWindow = new InventoryMessageWindow();
	}
	
	public void Start()
	{

#if UNITY_IPHONE
		if(Screen.width == 2048)
		{
			MainSkin = (GUISkin)UnityEngine.Resources.Load("GUI/Skins_2x");
		}
		
		if(!canRun)
		{
			return;
		}
#endif
		CorectionDBC.Init();
#if UNITY_ANDROID && !UNITY_EDITOR
		//init upgrade link
		if(Defines.SPLITBUILD)
		{
			StartCoroutine(PhpJson.phpjversion("https://mithril.worldofmidgard.com/gpurl.txt"));
			_updateURL = PhpJson.showVersion();
			if(_updateURL == "Error! Could not connect.")
			{
				_updateURL = "https://play.google.com/store";
			}
		}
		else
		{
			_updateURL = "http://worldofmidgard.com/womforum/viewtopic.php?f=32&t=3960";
		}
		
#endif
		
#if UNITY_ANDROID && !UNITY_EDITOR || UNITY_IPHONE		
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif	

#if UNITY_IPHONE
		backgroundImage.sprite = loginBackgroundIOS;
#else
		backgroundImage.sprite = loginBackground;
#endif

		UID.skin = MainSkin;
		UID.InitIntefaceDimensions();
		
		Defines.clientVersionInit();
		RefreshConnection();

		rememberAccToggle.isOn = PlayerPrefs.GetInt("SaveAccount") > 0 ? true : false;
		
		if(rememberAccToggle.isOn)
		{
			emailTextField.text = PlayerPrefs.GetString("AccountStorage").ToString();
		}

		LoadSceneManager.LoadingStatus = LoadSceneManager.LOADING.LOADING_IDLE;
	}


	public static void SetInventoryMessage(string message)
	{
		msgWindow.inventoryMessage = message;
		msgWindow.showTime = 3f;
		msgWindow.fontScale = 1f;
		Debug.Log(message);
	}
	public static void SetInventoryMessage(string message, float showTime)
	{
		msgWindow.inventoryMessage = message;
		msgWindow.showTime = showTime;
		msgWindow.fontScale = 1f;
		Debug.Log(message);
	}
	public static void SetInventoryMessage(string message, float showTime, float fontScale)
	{
		msgWindow.inventoryMessage = message;
		msgWindow.showTime = showTime;
		msgWindow.fontScale = fontScale;
		Debug.Log(message);
	}

	private void UpdateUI()
	{
#if !UNITY_IPHONE
		quitButtonGO.SetActive(true);
#else
		quitButtonGO.SetActive(false);
#endif
	}

	public void HandleOnCloseNewsButtonClicked()
	{
		newsPanel.SetActive(false);
		if(kongregatePanel != null)
		{
			kongregatePanel.SetActive(true);
		}
		else
		{
			loginPanel.SetActive(true);
		}
	}

	public void HandleOnRememberAccToggleClicked()
	{
		PlayerPrefs.SetInt("SaveAccount", rememberAccToggle.isOn == true ? 1 : 0);
		PlayerPrefs.Save();
	}

	public void HandleOnRegisterButtonClicked()
	{
		loginPanel.SetActive(false);
		registerPanel.SetActive(true);
	}

	public void HandleOnCloseResisterButtonClicked()
	{
		registerPanel.SetActive(false);
		loginPanel.SetActive(true);
	}

	public void HandleOnLogInButtonClocked()
	{
		AuthorizationSocket.Instance.SetLoginCallback(LogIn, emailTextField.text, passwordTextField.text);
		RefreshConnection();
		
		if(rememberAccToggle.isOn == true)
		{
			PlayerPrefs.SetString("AccountStorage", emailTextField.text);
			PlayerPrefs.Save();
		}
	}

	public void HandleOnResetPasswordButtonClicked()
	{
		loginPanel.SetActive(false);
		resetPasswordPanel.SetActive(true);
	}

	public void HandleOnCloseResetPasswordClicked()
	{
		resetPasswordPanel.SetActive(false);
		loginPanel.SetActive(true);
	}

	public void HandleOnOptionsButtonClicked()
	{

	}

	public void HandleOnNewsButtonClicked()
	{
		loginPanel.SetActive(false);
		newsPanel.SetActive(true);
	}

	public void HandleOnQuitButtonClicked()
	{
		AuthorizationSocket.Instance.CloseSocket();
		Application.Quit();
	}

	public void OnGUI()
	{
#if UNITY_STANDALONE || UNITY_EDITOR
		if(Event.current != null && Event.current.keyCode == KeyCode.Return)
		{
			Event e = Event.current;
			if(e.isKey)
			{
				if(passwordTextField.isFocused)
				{
					HandleOnLogInButtonClocked();
				}
				else if(emailTextField.isFocused)
				{
					passwordTextField.Select();
					passwordTextField.ActivateInputField();
				}
			}
		}
#endif

		switch(msgWindow.inventoryMessage)
		{
		case "Connected":
			if(clientVersion == "" && !checkingVersion)
			{
				checkingVersion = true;
				CheckVersion();
			}
			break;
		case "Invalid login":
			if(StopLogin)
			{
				StopLogin = false;
				DestroyInstantiatedShieldAnimation("logging_in");
			}
			break;
		}
			
		if(!string.IsNullOrEmpty(msgWindow.inventoryMessage))
		{
			msgWindow.ShowInventoryMessage();
		}
	}
//#if UNITY_IPHONE
//		if(needMoreMem)
//		{
//			GUI.DrawTexture(new Rect(0, 0, SW, SH), LoginBackgroundIOS, ScaleMode.ScaleToFit, true);
//			GUI.Label(new Rect(SW * 0.43f, SH * 0.1f, SW * 0.4f, SH * 0.05f), "WARNING", MainSkin.customStyles[2]);
//			GUI.Label(new Rect(SW * 0.15f, SH * 0.3f, SW * 0.7f, SH * 0.48f), "This game requires more free memory than your device has at the moment.\nPlease turn off Location Services to free up memory.\nPlease also close all other running apps.", MainSkin.customStyles[2]);
//			if(GUI.Button(new Rect(SW * 0.4f, SH * 0.7f, SW * 0.2f, SH * 0.1f), "Ok"))
//			{
//				Application.Quit();
//			}
//			return;
//		}
//		else if(!canRun)
//		{
//			GUI.DrawTexture(new Rect(0, 0, SW, SH), LoginBackgroundIOS, ScaleMode.ScaleToFit, true);
//			GUI.Label(new Rect(SW * 0.43f, SH * 0.1f, SW * 0.4f, SH * 0.05f), "WARNING", MainSkin.customStyles[2]);
//			GUI.Label(new Rect(SW * 0.15f, SH * 0.3f, SW * 0.7f, SH * 0.48f), "This game requires 256MB minimum RAM memory and can't be played on your device", MainSkin.customStyles[2]);
//			if(GUI.Button(new Rect(SW * 0.4f, SH * 0.7f, SW * 0.2f, SH * 0.1f), "Ok"))
//			{
//				Application.Quit();
//			}
//			return;
//		}
//#endif
//	}

	
	private void DrawWarningWindow()
	{
//		GUI.Label(new Rect(SW * 0.43f, SH * 0.1f, SW * 0.4f, SH * 0.05f), "WARNING", MainSkin.customStyles[2]);
//		GUI.Label(new Rect(SW * 0.25f, SH * 0.3f, SW * 0.7f, SH * 0.48f), "Upgraded version required.", MainSkin.customStyles[2]);
//		if(GUI.Button(new Rect(SW * 0.35f, SH * 0.7f, SW * 0.279f, SH * 0.1f), "Download upgrade"))
//		{
//			Application.OpenURL(_updateURL);
//		}
	}
	
	//******//
	

	
	private void CheckVersion()
	{
		string url = "";
#if UNITY_ANDROID && !UNITY_EDITOR
		if(Defines.SPLITBUILD)
		{
			url = "https://mithril.worldofmidgard.com/vgplay.txt";
		}
		else
		{
			url = "https://mithril.worldofmidgard.com/vandroid.txt";
		}
#elif UNITY_IPHONE && !UNITY_EDITOR
		url = "https://mithril.worldofmidgard.com/vios.txt";
#elif UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
		url = "https://mithril.worldofmidgard.com/vpcmac.txt";
#endif
		
		StartCoroutine(PhpJson.phpjversion(url));
		string response = PhpJson.showVersion();
		clientVersion = response;	
		
		if(response != "Error! Could not connect." || !Defines.SPLITBUILD)
		{
			if(clientVersion != "v.0.1" && Convert.ToInt32(Defines.version) < Convert.ToInt32(clientVersion))
			{
//				WindowState = LoginWindowState.LOGIN_STATE_OLDVERSION;
			}
		}
	}
	
	public static string CalculateSHA1Hash(string input)
	{
		SHA1CryptoServiceProvider sha = new SHA1CryptoServiceProvider();
		byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
		byte[] hash = sha.ComputeHash(inputBytes);
		StringBuilder sb = new StringBuilder();
		
		for(int index = 0; index < hash.Length; index++) 
		{
			sb.Append(hash[index].ToString("x2"));
		}
		
		return sb.ToString().ToLower();
	}
	
	public void LogIn(string acc, string logPass)
	{
		accountLogin = acc;
		accountPassword = logPass;
		acc = acc.ToUpper();
		
		ByteBuffer packet = new ByteBuffer();
		byte aux = (byte)AuthCmd.AUTH_LOGON_CHALLENGE;
		packet.Append(aux);
		aux = 0x06;
		packet.Append(aux);
		aux = (byte)(acc.Length + 30);
		packet.Append(aux);
		aux = 0x00;
		packet.Append(aux);
		packet.Append("MoW");
		packet.Append(Defines.clientVersion, 3);
		packet.Append(Defines.clientBuild);
		aux = 0x00;
		packet.Append("loL");
		packet.Append("loL");
		
		byte[] info = Encoding.ASCII.GetBytes(Defines.clientLang);
		for(int index = 0; index < 4; index++)
		{
			packet.Append(info[3 - index]);
		}
		
		uint auxInt = 0x3C;
		packet.Append(auxInt);
		auxInt = (uint)AuthorizationSocket.Instance.GetLongLocalIpAddress();
		packet.Append(auxInt);
		aux = (byte)acc.Length;
		packet.Append(aux);
		packet.AppendSTR(acc);
		
		AuthorizationSocket.Instance.SendData(packet);
	}
	
	private GameObject LoadingDummy;
	
	public void InstantiateLoadingShieldAnimation(string name)
	{
		if(LoadingDummy == null)
		{
			LoadingDummy = (GameObject)Instantiate(Resources.Load<GameObject>("GUI/LoadingDummy2"));
		}
		LoadingDummy.name = name;
	}
	
	public void DestroyInstantiatedShieldAnimation(string name)
	{
		if(LoadingDummy != null && LoadingDummy.name == name)
		{
			GameObject.Destroy(LoadingDummy);
		}
	}
	
	private string port = "";

	public void RefreshConnection() 
	{
		Defines.connectIp = "login.worldofmidgard.com";		// Public server adress
//		Defines.connectIp = "209.172.55.145";				// Public server IP adress
//		Defines.connectIp = "184.107.195.34";				// Development server adress
		Defines.connectPort = 3720;							// Public server
//		Defines.connectIp = "192.168.0.8";					// Test server
//		Defines.connectPort = 6878;
		Defines.connectIp = Defines.connectIp.Replace(" ", String.Empty);
		port = "" + Defines.connectPort;
		KongregateManager.Log ("connect to "+Defines.connectIp+":"+Defines.connectPort);
		AuthorizationSocket.Instance.Connect();
	}
}