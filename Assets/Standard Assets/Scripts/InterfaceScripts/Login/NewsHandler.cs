using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NewsHandler : MonoBehaviour
{
	public Text newsTextField;

	private void OnEnable()
	{
		RequestNews();
	}

	private void RequestNews()
	{
		switch(BuildConfig.platformName)
		{
		case "ios":
			StartCoroutine(FetchNews("http://worldofmidgard.com/iosnews.txt"));
			break;
		case "amazon":
			StartCoroutine(FetchNews("http://worldofmidgard.com/amazonnews.txt"));
			break;
		case "googleplay":
			StartCoroutine(FetchNews("http://worldofmidgard.com/androidnews.txt"));
			break;
		case "macstore":
		case "macforum":
			StartCoroutine(FetchNews("http://worldofmidgard.com/macpcnews.txt"));
			break;
		case "pc":
			StartCoroutine(FetchNews("http://worldofmidgard.com/pcnews.txt"));
			break;
		default:
			StartCoroutine(FetchNews("http://worldofmidgard.com/news.txt"));
			break;
		}
	}
	
	private IEnumerator FetchNews(string url) 
	{
		byte[] rand = System.BitConverter.GetBytes(Mathf.Ceil(UnityEngine.Random.Range(0, 99999999)));
		WWW dld = new WWW(url, rand);
		yield return dld;
		
		string wwwData;
		if(!string.IsNullOrEmpty(dld.error))
		{
			wwwData = "Error! Could not connect.\n" + dld.error;
		}
		else
		{
			wwwData = dld.text;
		}	
		newsTextField.text = wwwData;
	}
}