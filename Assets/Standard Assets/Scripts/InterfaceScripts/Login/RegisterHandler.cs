using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RegisterHandler : MonoBehaviour
{
	public LoginWindow login;
	public GameObject messageWindowPanel;

	public InputField emailTextField;
	public InputField passwordTextField;
	public InputField retypePasswordTextField;

	public Toggle acceptTerms;
	public Text messageText;


	public void HandleOnSubmitButtonClicked()
	{
		if(!acceptTerms.isOn)
		{
			messageText.text = "You didn't accept terms of use.";
			messageWindowPanel.SetActive(true);
		}
		else if(emailTextField.text == "" || passwordTextField.text == "" || retypePasswordTextField.text == "" )
		{
			messageText.text = "Invalid fields.";
			messageWindowPanel.SetActive(true);
		}
		else if(string.Compare(passwordTextField.text, retypePasswordTextField.text) != 0)
		{
			messageText.text = "Invalid passwords.";
			messageWindowPanel.SetActive(true);
		}
		else
		{
			StartCoroutine(SendRegisterToServer());
		}
	}

	public void HandleOnTermOfUseButtonClicked()
	{
		Application.OpenURL("https://mithril.worldofmidgard.com/worldofmidgard/pages/terms_of_use.php");
	}

	public void HandleOnPrivatePolicyButtonClicked()
	{
		Application.OpenURL("https://mithril.worldofmidgard.com/worldofmidgard/pages/privacy_policy.php");
	}

	public void HandleOnCloseMessageButtonClicked()
	{
		messageWindowPanel.SetActive(false);
		if(messageText.text == "Registration successful. Check your email.")
		{
			login.HandleOnCloseResisterButtonClicked();
		}
	}

	private IEnumerator SendRegisterToServer()
	{
		string[] paramname = {"email","password"};
		string[] paramdata = {emailTextField.text, passwordTextField.text};
		string url = "https://mithril.worldofmidgard.com/worldofmidgard/functions/free_registration.php";
		
		PhpJson.result = null;
		yield return StartCoroutine(PhpJson.phpj(url, paramname, paramdata));
		if(PhpJson.result.Length > 0)
		{
			for(var _i = 0; _i < PhpJson.result.Length; _i++)
			{
				Debug.Log("result: " + PhpJson.result[_i]);
			}

			if(PhpJson.result[3].Contains("free_registration"))
			{
				messageText.text = "Registration successful. Check your email.";
				messageWindowPanel.SetActive(true);
			}
			else
			{
				messageText.text = "Registration failed: " + PhpJson.result[4].Substring(0, PhpJson.result[4].Length - 2);
				messageWindowPanel.SetActive(true);
			}
		}
		else
		{
			messageText.text = "Registration failed.";
			messageWindowPanel.SetActive(true);
		}
	}

	public void ChangeRegisterState(eAuthResults state)
	{
		switch(state)
		{
		case eAuthResults.REALM_AUTH_SUCCESS:
			messageText.text = "Registration successful. Check your email.";
			messageWindowPanel.SetActive(true);
			break;
		case eAuthResults.REALM_AUTH_ACCOUNT_ALREADY_EXIST:
			messageText.text = "Registration failed: Account already exist.";
			messageWindowPanel.SetActive(true);
			break;
		case eAuthResults.REALM_AUTH_REGISTRATION_NOT_AVALIABLE:
			messageText.text = "Registration failed: Registration not available.";
			messageWindowPanel.SetActive(true);
			break;
		case eAuthResults.REALM_AUTH_REGISTRATION_WRONG_LOGIN_NAME:
			messageText.text = "Registration failed: Wrong email format.";
			messageWindowPanel.SetActive(true);
			break;
		default:
			messageText.text = "Registration failed: Error code: " + (uint)state;
			messageWindowPanel.SetActive(true);
			break;
		}
	}
}
