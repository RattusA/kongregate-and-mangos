using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResetPasswordHandler : MonoBehaviour
{
	public LoginWindow login;
	public GameObject messageWindowPanel;
	
	public InputField emailTextField;
	public Text messageText;

	public void HandleOnSubmitButtonClicked()
	{
		if(emailTextField.text == "")
		{
			messageText.text = "Invalid fields.";
			messageWindowPanel.SetActive(true);
		}
		else
		{
			StartCoroutine(SendResetPasswordRequest());
		}
	}

	public void HandleOnCloseMessageButtonClicked()
	{
		messageWindowPanel.SetActive(false);
		if(messageText.text == "Reset successful.")
		{
			login.HandleOnCloseResetPasswordClicked();
		}
	}

	private IEnumerator SendResetPasswordRequest()
	{
		string[] paramname = {"email"};
		string[] paramdata = {emailTextField.text};
		string url = "https://mithril.worldofmidgard.com/worldofmidgard/functions/reset_password_request.php";
		
		PhpJson.result = null;
		yield return StartCoroutine(PhpJson.phpj(url, paramname, paramdata));
		if(PhpJson.result.Length > 0)
		{
			for(var i = 0; i < PhpJson.result.Length; i++)
			{
				Debug.Log("r: " + PhpJson.result[i]);
			}
			
			if((PhpJson.result[2]).Contains("1") )
			{
				messageText.text = "Reset successful.";
				messageWindowPanel.SetActive(true);
			}
			else
			{
				messageText.text = "Failed: " + PhpJson.result[4].Substring(0, PhpJson.result[4].Length - 2);
				messageWindowPanel.SetActive(true);
			}
		}
		else
		{
			messageText.text = "Send failed.";
			messageWindowPanel.SetActive(true);
		}
	}
}
