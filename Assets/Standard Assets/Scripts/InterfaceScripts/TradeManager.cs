using UnityEngine;

public enum TradeStatusType : uint
{
	TRADE_STATUS_BUSY           = 0,
	TRADE_STATUS_BEGIN_TRADE    = 1,
	TRADE_STATUS_OPEN_WINDOW    = 2,
	TRADE_STATUS_TRADE_CANCELED = 3,
	TRADE_STATUS_TRADE_ACCEPT   = 4,
	TRADE_STATUS_BUSY_2         = 5,
	TRADE_STATUS_NO_TARGET      = 6,
	TRADE_STATUS_BACK_TO_TRADE  = 7,
	TRADE_STATUS_TRADE_COMPLETE = 8,

	TRADE_STATUS_TARGET_TO_FAR  = 10,
	TRADE_STATUS_WRONG_FACTION  = 11,
	TRADE_STATUS_CLOSE_WINDOW   = 12,

	TRADE_STATUS_IGNORE_YOU     = 14,
	TRADE_STATUS_YOU_STUNNED    = 15,
	TRADE_STATUS_TARGET_STUNNED = 16,
	TRADE_STATUS_YOU_DEAD       = 17,
	TRADE_STATUS_TARGET_DEAD    = 18,
	TRADE_STATUS_YOU_LOGOUT     = 19,
	TRADE_STATUS_TARGET_LOGOUT  = 20,
	TRADE_STATUS_TRIAL_ACCOUNT  = 21,			// Trial accounts can not perform that action
	TRADE_STATUS_ONLY_CONJURED  = 22			// You can only trade conjured items... (cross realm BG related).
};

public class TradeManager
{
	public MainPlayer mainPlayer;
	private WorldPacket pkt;
	ulong targetGuid;
	public Player targetPlayer;
	
	public TradeUI tradeUI;
	public bool  trading;
	public bool  accepted;
	
	public TradeManager ( MainPlayer player )
	{
		mainPlayer = player;
		tradeUI = new TradeUI(this);
		trading = false;
		accepted = false;
	}
	
	public void  RequestTrade ( BaseObject target )
	{
		/*if(mainPlayer.group.partyMembers.length > 0)
		{
			mainPlayer.AddChatInput("You can't trade while in a party.");
			return;
		}*/
		targetGuid = target.guid;
		targetPlayer = target as Player;
		Debug.Log("TARGET'S GUID IS : "+targetGuid);
		SendInitiateTrade();
	}
	
	void  SendInitiateTrade ()
	{
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_INITIATE_TRADE);
		pkt.Append(ByteBuffer.Reverse(targetGuid));
		RealmSocket.outQueue.Add(pkt);
	}
	
	void  SendBeginTrade ()
	{
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_BEGIN_TRADE);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendAcceptTrade ()
	{
		accepted = true;
		uint howIAm = 1337;
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_ACCEPT_TRADE);
		pkt.Append(howIAm);
		RealmSocket.outQueue.Add(pkt);
	}
	
	void  SendUnnaceptTrade ()
	{
		accepted = false;
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_UNACCEPT_TRADE);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendCancelTrade ()
	{
		if(accepted)
			SendUnnaceptTrade();
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_CANCEL_TRADE);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendSetTradeItem ( byte tradeSlot ,	  byte bag ,   byte slot )
	{
		if(accepted)
			SendUnnaceptTrade();
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_SET_TRADE_ITEM);
		pkt.Append(tradeSlot);
		pkt.Append(bag);
		pkt.Append(slot);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendClearTradeItem ( byte tradeSlot )
	{
		if(accepted)
			SendUnnaceptTrade();
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_CLEAR_TRADE_ITEM);
		pkt.Append(tradeSlot);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  SendSetTradeGold ( uint gold )
	{
		if(accepted)
			SendUnnaceptTrade();
		pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.CMSG_SET_TRADE_GOLD);
		pkt.Append(ByteBuffer.Reverse(gold));
		Debug.Log("Sent trade gold: " + gold);
		RealmSocket.outQueue.Add(pkt);
	}
	
	public void  HandleTradeStatus ( WorldPacket pkt )
	{
		TradeStatusType status = (TradeStatusType)pkt.ReadReversed32bit();
		Debug.Log("Got SMSG_TRADE_STATUS with status " + status);
		switch(status)
		{
		case TradeStatusType.TRADE_STATUS_BUSY:
			mainPlayer.AddChatInput("Player is currently busy.");
			break;
		case TradeStatusType.TRADE_STATUS_BEGIN_TRADE: // <=
			Debug.Log("TRADE: BEGIN TRADE");
			targetGuid = pkt.ReadReversed64bit();
			targetPlayer = WorldSession.GetRealChar(targetGuid) as Player;
			/*if(mainPlayer.group.partyMembers.length > 0)
				{
					SendCancelTrade();
					return;
				}*/
			tradeUI.showTradeRequest();
			break;
		case TradeStatusType.TRADE_STATUS_OPEN_WINDOW: // <=
			Debug.Log("Open trade window");
			pkt.ReadType(4);
			trading = true;
			tradeUI.Enable();
			tradeUI.openWindow();
			break;
		case TradeStatusType.TRADE_STATUS_TRADE_CANCELED:
			mainPlayer.AddChatInput("Trade canceled.");
			tradeUI.Disable();
			trading = false;
			break;
		case TradeStatusType.TRADE_STATUS_TRADE_ACCEPT:
			Debug.Log("TRADE: TRADE ACCEPTED STATUS " + trading.ToString());
			if(trading)
				tradeUI.ShowAccepted();
			else
				SendBeginTrade();
			break;
		case TradeStatusType.TRADE_STATUS_NO_TARGET:
			mainPlayer.AddChatInput("You have no target to trade with.");
			break;
		case TradeStatusType.TRADE_STATUS_BACK_TO_TRADE:
			Debug.Log("TRADE: BACK TO TRADE");
			break;
		case TradeStatusType.TRADE_STATUS_TRADE_COMPLETE:
			mainPlayer.AddChatInput("Trade completed.");
			tradeUI.Disable();
			trading = false;
			break;
		case TradeStatusType.TRADE_STATUS_TARGET_TO_FAR:
			mainPlayer.AddChatInput("Your target is too far away.");
			break;
		case TradeStatusType.TRADE_STATUS_WRONG_FACTION:
			mainPlayer.AddChatInput("You can't trade with that target.");
			break;
		case TradeStatusType.TRADE_STATUS_CLOSE_WINDOW: // <=
			pkt.ReadType(8);
			pkt.Read();
			Debug.Log("Close trade window");
			tradeUI.closeWindow();
			break;
		case TradeStatusType.TRADE_STATUS_IGNORE_YOU:
			mainPlayer.AddChatInput("Player is ignoring you.");
			break;
		case TradeStatusType.TRADE_STATUS_YOU_STUNNED:
			mainPlayer.AddChatInput("You can't trade while stunned.");
			break;
		case TradeStatusType.TRADE_STATUS_TARGET_STUNNED:
			mainPlayer.AddChatInput("Your target is stunned.");
			break;
		case TradeStatusType.TRADE_STATUS_YOU_DEAD:
			mainPlayer.AddChatInput("You can't trade while dead.");
			break;
		case TradeStatusType.TRADE_STATUS_TARGET_DEAD:
			mainPlayer.AddChatInput("Your target is dead.");
			break;
		case TradeStatusType.TRADE_STATUS_YOU_LOGOUT:
			break;
		case TradeStatusType.TRADE_STATUS_TARGET_LOGOUT:
			break;
		case TradeStatusType.TRADE_STATUS_ONLY_CONJURED: // <=
			pkt.Read();
			mainPlayer.AddChatInput("You can trade only conjured items");
			break;
		}
	}
	
	public void  HandleTradeStatusExtended ( WorldPacket pkt )
	{
		Debug.Log("GOT SMSG TRADE STATUS EXTENDED!");
		byte traderState;
		byte i;
		
		// fakeeeeee
		uint itemEntry;
		uint itemWrapped;
		ulong itemWrapperGuid;
		uint itemEnchantId;
		//===
		ulong itemCreatorGuid;
		uint itemSpellCharges;
		uint itemSuffixFactor;
		uint itemRandomPropertyId;
		uint itemLockId;
		uint itemDurability;
		// fakeeeeee
		traderState = pkt.Read();
		pkt.ReadType(4); // always 0
		pkt.ReadType(4); // max 6 traded items, the 7th is for enchanting
		pkt.ReadType(4); // max 6 traded items, the 7th is for enchanting
		tradeUI.targetGold = pkt.ReadReversed32bit(); // money
		pkt.ReadType(4); // spell from slot 7
		if(tradeUI.targetItems.Count > 0)
			tradeUI.targetItems.Clear();
		for(i = 0; i < 7; i++)
		{
			pkt.Read();
			itemEntry = pkt.ReadReversed32bit();
			if(itemEntry > 0)
			{
				AppCache.sItems.SendQueryItem(itemEntry);
				Item item = AppCache.sItems.GetRecord(itemEntry);
				item.entry = itemEntry;
				item.displayInfoID = pkt.ReadReversed32bit();
				item.count = pkt.ReadReversed32bit();
				itemWrapped = pkt.ReadReversed32bit();
				itemWrapperGuid = pkt.ReadReversed64bit(); // 6
				//===
				itemEnchantId = pkt.ReadReversed32bit();
				for(byte a = 0; a < 3; a++)
				{
					pkt.ReadReversed32bit();
				}
				//==
				itemCreatorGuid = pkt.ReadReversed64bit();
				itemSpellCharges = pkt.ReadReversed32bit();
				itemSuffixFactor = pkt.ReadReversed32bit();
				itemRandomPropertyId = pkt.ReadReversed32bit();
				itemLockId = pkt.ReadReversed32bit();
				itemDurability = pkt.ReadReversed32bit(); // max durability
				pkt.ReadReversed32bit(); // durability
				Debug.Log("Adding target item with entry " + itemEntry);
				tradeUI.targetItems.Add(item);
			}
			else
			{
				for(byte q = 0; q < 17; ++q)
				{
					pkt.ReadType(4); // read 17 uint32(0)
				}
			}
		}
		pkt = null; // the 7th item isn't read for now so better gc that packet
		tradeUI.showType = 2;
	}
}