using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum TrainerSpellState
{
	SPELL_NORMAL,
	SPELL_BLOCKED,
	SPELL_LEARNED
}

public class TrainerSpell
{
	public uint spellID = 0;
	public string spellName = "";
	public string spellRank = "";
	public TrainerSpellState spellState = TrainerSpellState.SPELL_BLOCKED;
	public uint spellCost = 0;

	public byte reqLevel = 0;
	public uint reqSkill = 0;
	public string reqSkillName = "";
	public uint reqSkillValue = 0;

	public uint learnedSpell = 0;

	public TrainerSpell(uint spellID, uint spellCost, uint reqSkill, uint reqSkillValue, byte reqLevel, uint learnedSpell)
	{
		this.spellID = spellID;
		this.spellCost = spellCost;
		this.reqSkill = reqSkill;
		this.reqSkillValue = reqSkillValue;
		this.reqLevel = reqLevel;
		this.learnedSpell = learnedSpell;
		
		reqSkillName = dbs.sSpells.GetRecord(reqSkill).SpellName.Strings[0];
		if(reqSkillName.Equals(string.Empty))
		{
			reqSkillName = "unknown";
		}

		spellName = dbs.sSpells.GetRecord(spellID).SpellName.Strings[0];
		if(spellName.Equals(string.Empty))
		{
			spellName = "unknown";
		}
		else
		{
			spellRank = dbs.sSpells.GetRecord(spellID).Rank.Strings[0];
		}
	}
	
	public TrainerSpell(TrainerSpell trainerSpell)
	{
		this.spellID = trainerSpell.spellID;
		this.spellName = trainerSpell.spellName;
		this.spellRank = trainerSpell.spellRank;
		this.spellCost = trainerSpell.spellCost;

		this.reqLevel = trainerSpell.reqLevel;
		this.reqSkill = trainerSpell.reqSkill;
		this.reqSkillName = trainerSpell.reqSkillName;
		this.reqSkillValue = trainerSpell.reqSkillValue;

		this.learnedSpell = trainerSpell.learnedSpell;
	}

	public TrainerSpell()
	{
		this.spellID = 0;
		this.spellName = "";
		this.spellRank = "";
		this.spellCost = 0;
		
		this.reqLevel = 0;
		this.reqSkill = 0;
		this.reqSkillName = "";
		this.reqSkillValue = 0;
		
		this.learnedSpell = 0;
	}
}

public class TrainerSlotButton
{
	public TrainerSpell trainerSpell;
	public Texture spellIcon;
	public Rect position;
}

public class TrainerManager
{
	public List<TrainerSpell> spellList = new List<TrainerSpell>();
	public List<TrainerSlotButton> spellSlotsList = new List<TrainerSlotButton>();
	protected List<uint> profSpells = new List<uint>{2275, 2020, 7414, 4039, 2372, 45375, 25245, 2155, 2581, 8615, 3911};
	public int page = 0;
	public int maxPage = 0;
	public ulong npcGuid = 0; 

	public void InitTrainerSpellList(WorldPacket pkt)
	{
		uint trainerType = pkt.ReadReversed32bit();
		uint spellListSize = pkt.ReadReversed32bit();

		spellList = new List<TrainerSpell>();
		
		for(int index = 0; index < spellListSize; index++)
		{
			TrainerSpell ts = new TrainerSpell();
			ts.spellID = pkt.ReadReversed32bit();
			ts.spellState = (TrainerSpellState)pkt.Read();
			ts.spellCost = pkt.ReadReversed32bit();
			
			uint canLearnPrimaryProf = pkt.ReadReversed32bit();
			uint primaryProfFirstRank = pkt.ReadReversed32bit();
			
			ts.reqLevel = pkt.Read();
			ts.reqSkill = pkt.ReadReversed32bit();
			ts.reqSkillValue = pkt.ReadReversed32bit();
			pkt.ReadReversed32bit();
			pkt.ReadReversed32bit();
			pkt.ReadReversed32bit();
			
			ts.reqSkillName = "No Skill";
//			try
//			{
//				// Если не равно 0 то значит проффесия
//				if(ts.reqSkill != 0)
//				{
//					ts.reqSkill = dbs.sSpells.GetRecord(ts.reqSkill)
//					ts.reqSkillName = dbs.sSpells.GetRecord(ts.reqSkill).SpellName.Strings[0];
//					if(ts.reqSkillName.Equals(string.Empty))
//					{
//						ts.reqSkillName = "unknown";
//					}
//				}
//			}
//			catch (Exception e)
//			{
//				Debug.LogException(e);
//			}
			
			if(ts.spellID != 0)
			{
				if(dbs.sSpells.GetRecord(ts.spellID).ID != 0)
				{
					ts.spellName = dbs.sSpells.GetRecord(ts.spellID).SpellName.Strings[0];
					ts.spellRank = dbs.sSpells.GetRecord(ts.spellID).Rank.Strings[0];
				}
				else
				{
					ts.spellName = "unknown";
				}
			}
			else
			{
				Debug.Log("NU EXISTA SPELL ID!!!!");
			}
			
			if(ts.spellState == TrainerSpellState.SPELL_NORMAL)
			{	
				spellList.Add(ts);
			}
		}
		maxPage = (spellList.Count - 1) / 24;
		SetSpellSlotListByPage();
	}

	private void SetSpellSlotListByPage()
	{
		spellSlotsList = new List<TrainerSlotButton>();

		if(spellList.Count != 0)
		{
			int startPos = 24 * page;
			int endPos = 24 * (page + 1);
			endPos = (endPos > spellList.Count) ? spellList.Count : endPos;

			float iconSizeX = Screen.width * 0.099f;
			float iconSizeY = Screen.height * 0.1275f;

			for(int index = startPos; index < endPos; index++)
			{
				string imageName = DefaultIcons.GetSpellIcon(spellList[index].spellID);
				imageName = imageName.Substring(0, imageName.LastIndexOf('.'));

				TrainerSlotButton trainerSlotButton = new TrainerSlotButton();
				trainerSlotButton.trainerSpell = spellList[index];
				trainerSlotButton.spellIcon = Resources.Load<Texture> (string.Format ("Icons/{0}", imageName));

				float xPos = Screen.width * 0.205f + iconSizeX * ((index % 24) % 6);
				float yPos = Screen.height * 0.195f + iconSizeY * ((index % 24) / 6);

				trainerSlotButton.position = new Rect(xPos, yPos, iconSizeX, iconSizeY);

				spellSlotsList.Add(trainerSlotButton);
			}
		}
	}

	public void UpdatePage()
	{
		SetSpellSlotListByPage();
	}
	
	/**
	 * 	PACKETS FUNCTIONS
	 */
	
	public void LearnSpellDelegate(TrainerSpell trainerSpell)
	{
		if(profSpells.Contains(trainerSpell.spellID))
		{
			if(CraftManager.mainProfCount >= 2)
			{
				InterfaceManager.Instance.DrawMessageWindow("You already know 2 crafts");
				return;
			}
		}

		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((int)OpCodes.CMSG_TRAINER_BUY_SPELL);
		pkt.Add(npcGuid);
		pkt.Add(trainerSpell.spellID);
		RealmSocket.outQueue.Add(pkt);
	}

	public void SpellBuySucceeded(uint spellID)
	{
		int position = 0;
		for(int index = 0; index < spellList.Count; index++)
		{
			if(spellList[index].spellID == spellID)
			{
				position = index;
				break;
			}
		}
		InterfaceManager.Instance.DrawMessageWindow("Training was successfull!");

		spellList.RemoveAt(position);
		SetSpellSlotListByPage();
	}
}
