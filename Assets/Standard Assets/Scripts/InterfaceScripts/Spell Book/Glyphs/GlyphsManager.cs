﻿using UnityEngine;
using System.Collections.Generic;

public enum GlyphType {

	BIG_GLYPH		= 0,
	SMALL_GLYPH		= 1
}

public class GlyphsManager : MonoBehaviour {

	[SerializeField]
	protected Texture EmptyGlyphTexture;

	public Player PlayerScript;
	protected List<Item> LargeGlyphs;
	protected List<Item> SmallGlyphs;

	protected const int ContainerSize = 12;
	protected const int HalfContainerSize = 6;

	public List<InventoryItemSlot> LargeGlyphsSlots;
	public List<InventoryItemSlot> SmallGlyphsSlots;

	public int LargeGlyphScrollPosition;
	public int SmallGlyphScrollPosition;

	public int MaxLargeGlyphScrollPosition;
	public int MaxSmallGlyphScrollPosition;

	public List<InventoryItemSlot> LargeGlyphsItems;
	public List<InventoryItemSlot> SmallGlyphsItems;

	public Item SelectedGlyph;

	public void OnEnable()
	{
		LargeGlyphScrollPosition = 0;
		SmallGlyphScrollPosition = 0;

		LargeGlyphs = new List<Item>();
		SmallGlyphs = new List<Item>();

		GetSmallAndLargeGlyphs();

		LargeGlyphsItems = new List<InventoryItemSlot>();
		SmallGlyphsItems = new List<InventoryItemSlot>();

		UdpadeLargeGlyphs();
		UdpadeSmallGlyphs();
	}

	public void OnDisable()
	{
		LargeGlyphs.Clear();
		SmallGlyphs.Clear();
	}

	public void GetSmallAndLargeGlyphs()
	{
		GlyphPropertiesEntry propertyEntry;
		int itemClass = (int)ItemClass.ITEM_CLASS_GLYPH;
		int itemSubClass = (int)PlayerScript.playerClass;
		List<Item> GlyphsList = PlayerScript.itemManager.GetItemListByCondition(itemClass, itemSubClass, -1);

		foreach(Item glyph in GlyphsList)
		{
			SpellEntry glyphSpell = dbs.sSpells.GetRecord(glyph.spells[0].ID);
			for(int index = 0; index < 3; index++)
			{
				if(glyphSpell.Effect[index] == 74)
				{
					propertyEntry = dbs.sGlyphProperties.GetRecord(glyphSpell.EffectMiscValue[index]);
					if(propertyEntry != null)
					{
						switch(propertyEntry.glyphType)
						{
						case (int)GlyphType.BIG_GLYPH:
							LargeGlyphs.Add(glyph);
							break;
						case (int)GlyphType.SMALL_GLYPH:
							SmallGlyphs.Add(glyph);
							break;
						}
					}
				}
			}

		}

		MaxLargeGlyphScrollPosition = LargeGlyphs.Count / ContainerSize;
		MaxSmallGlyphScrollPosition = SmallGlyphs.Count / ContainerSize;
	}

	public void UdpadeLargeGlyphs()
	{
		int startIndex = LargeGlyphScrollPosition * ContainerSize;
		int endIndex = LargeGlyphScrollPosition + ContainerSize;
		endIndex = (endIndex <= LargeGlyphs.Count) ? endIndex : LargeGlyphs.Count;

		LargeGlyphsItems.Clear();
		for(int index = startIndex; index < endIndex; index++)
		{
			InventoryItemSlot itemButton = new InventoryItemSlot();
			itemButton.item = LargeGlyphs[index];
			
			string iconName = DefaultIcons.GetItemIcon(LargeGlyphs[index]);
			iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
			itemButton.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));

			float xPos = Screen.width * 0.4815f + (index % HalfContainerSize) * Screen.width * 0.0665f;
			float yPos = Screen.height * 0.3325f + (index / HalfContainerSize) * Screen.height * 0.093f;
			
			itemButton.position = new Rect(xPos, yPos, Screen.width * 0.0665f, Screen.height * 0.093f);
			LargeGlyphsItems.Add(itemButton);
		}
	}

	public void UdpadeSmallGlyphs()
	{
		int startIndex = SmallGlyphScrollPosition * ContainerSize;
		int endIndex = SmallGlyphScrollPosition + ContainerSize;
		endIndex = (endIndex <= SmallGlyphs.Count) ? endIndex : SmallGlyphs.Count;
		
		SmallGlyphsItems.Clear();
		for(int index = startIndex; index < endIndex; index++)
		{
			InventoryItemSlot itemButton = new InventoryItemSlot();
			itemButton.item = SmallGlyphs[index];
			
			string iconName = DefaultIcons.GetItemIcon(SmallGlyphs[index]);
			iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
			itemButton.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
			
			float xPos = Screen.width * 0.4815f + (index % HalfContainerSize) * Screen.width * 0.0665f;
			float yPos = Screen.height * 0.6835f + (index / HalfContainerSize) * Screen.height * 0.0943f;
			
			itemButton.position = new Rect(xPos, yPos, Screen.width * 0.0665f, Screen.height * 0.093f);
			SmallGlyphsItems.Add(itemButton);
		}
	}


	public void SetGlyphsSlots(WorldPacket pkt)
	{
		LargeGlyphsSlots = new List<InventoryItemSlot>();
		SmallGlyphsSlots = new List<InventoryItemSlot>();

		byte openedGlyphsCount = pkt.Read();
		SetGlyphsSlotsByType(pkt.ReadReversed16bit(), GlyphType.BIG_GLYPH);		// Big Slot - 01
		SetGlyphsSlotsByType(pkt.ReadReversed16bit(), GlyphType.SMALL_GLYPH);		// Small Slot - 01
		SetGlyphsSlotsByType(pkt.ReadReversed16bit(), GlyphType.SMALL_GLYPH);		// Small Slot - 02
		SetGlyphsSlotsByType(pkt.ReadReversed16bit(), GlyphType.BIG_GLYPH);		// Big slot - 02
		SetGlyphsSlotsByType(pkt.ReadReversed16bit(), GlyphType.SMALL_GLYPH);		// Small slot -03
		SetGlyphsSlotsByType(pkt.ReadReversed16bit(), GlyphType.BIG_GLYPH);		// Big slot - 03
	}	
	
	public void SetGlyphsSlotsByType(ushort glyphID, GlyphType type)
	{
		InventoryItemSlot itemButton = new InventoryItemSlot();
		if(glyphID > 0)
		{
			GlyphPropertiesEntry propertyEntry = dbs.sGlyphProperties.GetRecord((uint)glyphID);
			if(propertyEntry != null)
			{
				SpellEntry spell = dbs.sSpells.GetRecord(propertyEntry.spellID);
				if(spell != null && spell.ID > 0)
				{
					itemButton.spell = spell;
					string iconName = DefaultIcons.GetSpellIcon(spell.ID);
					iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
					itemButton.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
				}
			}
		}

		if(itemButton.texture == null)
		{
			itemButton.texture = EmptyGlyphTexture;
		}

		float padding;
		switch(type)
		{
		case GlyphType.BIG_GLYPH:
			padding = Screen.width * 0.055f + Screen.width * 0.12f * LargeGlyphsSlots.Count;
			itemButton.position = new Rect(padding, Screen.height * 0.3275f,
			                               Screen.width * 0.11f, Screen.height * 0.145f);
			LargeGlyphsSlots.Add(itemButton);
			break;
		case GlyphType.SMALL_GLYPH:
			padding = Screen.width * 0.055f + Screen.width * 0.12f * SmallGlyphsSlots.Count;
			itemButton.position = new Rect(padding, Screen.height * 0.7275f,
			                               Screen.width * 0.11f, Screen.height * 0.145f);
			SmallGlyphsSlots.Add(itemButton);
			break;
		}
	}

	public void UseGlyphItem(int glyphSlot)
	{
		byte cost_count = 0;
		uint spellID = SelectedGlyph.spells[0].ID;
		ulong itemGuid = SelectedGlyph.GetUInt64Value(0);
		uint glyphIndex = (uint)glyphSlot;
		SpellCastTargets targets = new SpellCastTargets();
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_USE_ITEM);
		pkt.Append(SelectedGlyph.bag);
		pkt.Append(SelectedGlyph.slot);		
		pkt.Append(cost_count);
		pkt.Add(spellID);
		pkt.Add(itemGuid);						//get item guid
		pkt.Add(glyphIndex);
		pkt.Append(cost_count);
		pkt.Append(targets.Write(itemGuid));
		
		RealmSocket.outQueue.Add(pkt);

		SelectedGlyph = null;
	}
}
