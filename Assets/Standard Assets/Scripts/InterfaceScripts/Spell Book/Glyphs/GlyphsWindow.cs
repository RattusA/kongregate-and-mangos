﻿using UnityEngine;

public class GlyphsWindow : MonoBehaviour {

	[SerializeField]
	protected GUISkin Skin;
	[SerializeField]
	public GlyphsManager GlyphsMng;

	protected float SW;
	protected float SH;

	protected uint[] LargeGlyphLevel = {14, 29, 79};
	protected uint[] SmallGlyphLevel = {14, 49, 69};

	protected int[] BigGlyphSlot = {0, 3, 5};
	protected int[] SmallGlyphSlot = {1, 2, 4};

	protected DescriptionWindow PopoutWindow;
	protected bool EquipState = false;

	public void Awake()
	{
		SW = Screen.width;
		SH = Screen.height;

		Skin.button.fontSize = (int)(Screen.height * 0.04f);
		Skin.label.fontSize = (int)(Screen.height * 0.06f);
	}

	public void SetMainPlayer(Player playerScript)
	{
		GlyphsMng.PlayerScript = playerScript;
	}

	public void DrawGUI()
	{
		GUI.skin = Skin;

		GUI.Label(new Rect(SW * 0.2f, SH * 0.21f, SW * 0.6f, SH * 0.1f), "Large Glyphs");
		GUI.Label(new Rect(SW * 0.03f, SH * 0.3f, SW * 0.4f, SH * 0.25f), "",
		          Skin.GetStyle("Small Glyphs Decoration"));

		GUI.Label(new Rect(SW * 0.2f, SH * 0.91f, SW * 0.6f, SH * 0.1f), "Small Glyphs");
		GUI.Label(new Rect(SW * 0.03f, SH * 0.65f, SW * 0.4f, SH * 0.25f), "",
		          Skin.GetStyle("Large Glyphs Decoration"));

		// Large Glyphs
		GUI.Label(new Rect(SW * 0.4575f, SH * 0.3f, SW * 0.45f, SH * 0.25f), "", Skin.GetStyle("Glyphs Inventory"));
		foreach(InventoryItemSlot itemButton in GlyphsMng.LargeGlyphsItems)
		{
			GUI.DrawTexture(itemButton.position, itemButton.texture);
			if(GUI.Button(itemButton.position, "", GUIStyle.none))
			{
				PopoutWindow = new DescriptionWindow();
				PopoutWindow.IconTecture = itemButton.texture;
				PopoutWindow.TitleText = itemButton.item.name;
				PopoutWindow.DescriptionText = itemButton.item.description;

				InfoWindowButton closeButton = PopoutWindow.ButtonManager.AddFindButton(InfoWindowButtonType.Close);
				closeButton.ButtonName = "Close";
				closeButton.onButtonUp += ClosePopupWindow;

				InfoWindowButton equipButton = PopoutWindow.ButtonManager.AddFindButton(InfoWindowButtonType.Close);
				equipButton.ButtonName = "Equip";
				equipButton.onButtonUp += EquipGlyph;

				GlyphsMng.SelectedGlyph = itemButton.item;
			}
		}

		if(GUI.Button(new Rect(SW * 0.9275f, SH * 0.3f, SW * 0.05f, SH * 0.075f), "", Skin.GetStyle("Scroll Up")))
		{
			if(GlyphsMng.LargeGlyphScrollPosition > 0)
			{
				GlyphsMng.LargeGlyphScrollPosition--;
			}
			GlyphsMng.UdpadeLargeGlyphs();
		}

		if(GUI.Button(new Rect(SW * 0.9275f, SH * 0.475f, SW * 0.05f, SH * 0.075f), "", Skin.GetStyle("Scroll Down")))
		{
			if(GlyphsMng.LargeGlyphScrollPosition < GlyphsMng.MaxLargeGlyphScrollPosition)
			{
				GlyphsMng.LargeGlyphScrollPosition--;
			}
			GlyphsMng.UdpadeLargeGlyphs();
		}

		// Small Glyphs
		GUI.Label(new Rect(SW * 0.455f, SH * 0.65f, SW * 0.45f, SH * 0.25f), "", Skin.GetStyle("Glyphs Inventory"));
		foreach(InventoryItemSlot itemButton in GlyphsMng.SmallGlyphsItems)
		{
			GUI.DrawTexture(itemButton.position, itemButton.texture);
			if(GUI.Button(itemButton.position, "", GUIStyle.none))
			{
				PopoutWindow = new DescriptionWindow();
				PopoutWindow.IconTecture = itemButton.texture;
				PopoutWindow.TitleText = itemButton.item.name;
				PopoutWindow.DescriptionText = itemButton.item.description;
				
				InfoWindowButton closeButton = PopoutWindow.ButtonManager.AddFindButton(InfoWindowButtonType.Close);
				closeButton.ButtonName = "Close";
				closeButton.onButtonUp += ClosePopupWindow;
				
				InfoWindowButton equipButton = PopoutWindow.ButtonManager.AddFindButton(InfoWindowButtonType.Close);
				equipButton.ButtonName = "Equip";
				equipButton.onButtonUp += EquipGlyph;

				GlyphsMng.SelectedGlyph = itemButton.item;
			}
		}

		if(GUI.Button(new Rect(SW * 0.9275f, SH * 0.65f, SW * 0.05f, SH * 0.075f), "", Skin.GetStyle("Scroll Up")))
		{
			if(GlyphsMng.SmallGlyphScrollPosition > 0)
			{
				GlyphsMng.SmallGlyphScrollPosition--;
			}
			GlyphsMng.UdpadeSmallGlyphs();
		}

		if(GUI.Button(new Rect(SW * 0.9275f, SH * 0.825f, SW * 0.05f, SH * 0.075f), "", Skin.GetStyle("Scroll Down")))
		{
			if(GlyphsMng.SmallGlyphScrollPosition < GlyphsMng.MaxSmallGlyphScrollPosition)
			{
				GlyphsMng.SmallGlyphScrollPosition--;
			}
			GlyphsMng.UdpadeSmallGlyphs();
		}

		if(!EquipState)
		{
			DrawGlyphsSlots();
			if(PopoutWindow != null)
			{
				GUI.ModalWindow(0, new Rect(0, 0 , SW, SH), PopoutWindow.DrawGUI, "");
			}
		}
		else
		{
			GUI.ModalWindow(1, new Rect(0, 0 , SW, SH), DrawEquipButtons, "");
		}
	}

	public void DrawGlyphsSlots()
	{
		// Large Glyphs
		for(int index = 0; index < LargeGlyphLevel.Length; index++)
		{
			if(GlyphsMng.LargeGlyphsSlots[index] != null)
			{
				DrawGlyphButton(GlyphsMng.LargeGlyphsSlots[index], LargeGlyphLevel[index], BigGlyphSlot[index]);
			}
		}

		// Small Glyphs
		for(int index = 0; index < SmallGlyphLevel.Length; index++)
		{
			if(GlyphsMng.SmallGlyphsSlots[index] != null)
			{
				DrawGlyphButton(GlyphsMng.SmallGlyphsSlots[index], SmallGlyphLevel[index], SmallGlyphSlot[index]);
			}
		}
	}

	protected void DrawGlyphButton(InventoryItemSlot itemButton, uint level, int slot)
	{
		if(GlyphsMng.PlayerScript.GetLevel() < level)
		{
			GUI.Label(itemButton.position, level.ToString());
		}
		else
		{
			if(itemButton.spell != null)
			{
				GUI.DrawTexture(itemButton.position, itemButton.texture);
				if(GUI.Button(itemButton.position, "", GUIStyle.none))
				{
					if(EquipState)
					{
						GlyphsMng.UseGlyphItem(slot);
						EquipState = false;
					}
					else
					{
						PopoutWindow = new DescriptionWindow();
						PopoutWindow.IconTecture = itemButton.texture;
						PopoutWindow.TitleText = itemButton.spell.SpellName.Strings[0];
						PopoutWindow.DescriptionText = itemButton.spell.Description.Strings[0];

						InfoWindowButton closeButton = PopoutWindow.ButtonManager.AddFindButton(InfoWindowButtonType.Close);
						closeButton.ButtonName = "Close";
						closeButton.onButtonUp += ClosePopupWindow;
					}
				}
			}
			else if(EquipState)
			{
				GUI.DrawTexture(itemButton.position, itemButton.texture);
				if(GUI.Button(itemButton.position, "", GUIStyle.none))
				{
					GlyphsMng.UseGlyphItem(slot);
					EquipState = false;
				}
			}
		}
	}

	public void ClosePopupWindow()
	{
		PopoutWindow = null;
		GlyphsMng.SelectedGlyph = null;
	}

	public void EquipGlyph()
	{
		PopoutWindow = null;
		EquipState = true;
	}

	public void DrawEquipButtons(int index)
	{
		DrawGlyphsSlots();

		if(GUI.Button(new Rect(SW * 0.55f, SH * 0.56f, SW * 0.25f, SH * 0.08f), "Cancel"))
		{
			GlyphsMng.SelectedGlyph = null;
			EquipState = false;
		}
	}
}
