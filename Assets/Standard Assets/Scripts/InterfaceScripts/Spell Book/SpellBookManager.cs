﻿using UnityEngine;

public enum SpellBookWindowState {

	SPELL_AND_ITEMS_WINDOW_STATE,
	TRAITS_WINDOW_STATE,
	GLYPHS_WINDOW_STATE,
};

public class SpellBookManager : MonoBehaviour {

	[SerializeField]
	public PlayerMenuManager Parent;
//	[SerializeField]
//	public SpellAndItemsWindow SpellAndItemsWnd;
//	[SerializeField]
//	public TraitsWindow TraitsWnd;
	[SerializeField]
	public GlyphsWindow GlyphsWnd;

	protected SpellBookWindowState WindowState;

	public void SetMainPlayer(Player playerScript)
	{
		GlyphsWnd.SetMainPlayer(playerScript);
	}

	public void OnEnable()
	{
		SetWindowState(SpellBookWindowState.SPELL_AND_ITEMS_WINDOW_STATE);
	}

	public void OnDisable()
	{
		switch(WindowState)
		{
		case SpellBookWindowState.SPELL_AND_ITEMS_WINDOW_STATE:
			break;
		case SpellBookWindowState.TRAITS_WINDOW_STATE:
			break;
		case SpellBookWindowState.GLYPHS_WINDOW_STATE:
			GlyphsWnd.gameObject.SetActive(false);
			break;
		}
	}

	public void SetWindowState(SpellBookWindowState newState)
	{
		OnDisable();
		
		switch(newState)
		{
		case SpellBookWindowState.SPELL_AND_ITEMS_WINDOW_STATE:
			break;
		case SpellBookWindowState.TRAITS_WINDOW_STATE:
			break;
		case SpellBookWindowState.GLYPHS_WINDOW_STATE:
			GlyphsWnd.gameObject.SetActive(true);
			break;
		}
		WindowState = newState;
	}

	public void DrawGUI()
	{
		switch(WindowState)
		{
		case SpellBookWindowState.SPELL_AND_ITEMS_WINDOW_STATE:
			break;
		case SpellBookWindowState.TRAITS_WINDOW_STATE:
			break;
		case SpellBookWindowState.GLYPHS_WINDOW_STATE:
			GlyphsWnd.DrawGUI();
			break;
		}
	}
}
