using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

enum BattleGroundStatus : uint
{
	STATUS_NONE         = 0,                                // first status, should mean bg is not instance
	STATUS_WAIT_QUEUE   = 1,                                // means bg is empty and waiting for queue
	STATUS_WAIT_JOIN    = 2,                                // this means, that BG has already started and it is waiting for more players
	STATUS_IN_PROGRESS  = 3,                                // means bg is running
	STATUS_WAIT_LEAVE   = 4                                 // means some faction has won BG and it is ending
};

enum BattleGroundTypeId : uint
{
	BATTLEGROUND_TYPE_NONE     = 0,
	BATTLEGROUND_AV            = 1,
	BATTLEGROUND_WS            = 2,
	BATTLEGROUND_AB            = 3,
	BATTLEGROUND_NA            = 4,
	BATTLEGROUND_BE            = 5,
	BATTLEGROUND_AA            = 6,                         // all arenas
	BATTLEGROUND_EY            = 7,
	BATTLEGROUND_RL            = 8,
	BATTLEGROUND_SA            = 9,
	BATTLEGROUND_DS            = 10,
	BATTLEGROUND_RV            = 11,
	BATTLEGROUND_IC            = 30,
	BATTLEGROUND_RB            = 32                         // random battleground
};

public class BattleZoneLogData {

	public ulong guid;
	public string name;
	public byte faction;
	public uint killingBlows;
	public uint honorKills;
	public uint deaths;
	public uint bonusHonor;
	public uint damageDone;
	public uint healingDone;
	public uint flagCaptures;
	public uint flagReturns;
}

public class BattleZoneManager : MonoBehaviour {

	[SerializeField]
	public BattleZoneWindow battleZoneWindow;

	public List<BattleZoneLogData> playerList = new List<BattleZoneLogData>();
	public uint timeLeft = 0;

	public void ReadPvPLogData(WorldPacket pkt)
	{
//		bool isArena = (pkt.Read() != 0 ? true : false);
//		byte battleZoneStatus;
//		byte winnerTeam;
//
//		if(isArena)
//		{
//			return;
//		}
//
//		battleZoneStatus = pkt.Read();
//		if(battleZoneStatus == 1)
//		{
//			winnerTeam = pkt.Read();
//		}
//
//		uint playersCount = pkt.ReadReversed32bit();
//
//		for(uint index = 0; index < playersCount; index++)
//		{
//			ulong playerGuid = pkt.GetPacketGuid();
//			uint killingBlows = pkt.ReadReversed32bit();
//
//			if(isArena)
//			{
//
//			}
//			else
//			{
//				uint honorableKills = pkt.ReadReversed32bit();
//				uint deaths = pkt.ReadReversed32bit();
//				uint bonusHonor = pkt.ReadReversed32bit();
//			}
//
//			uint damageDone = pkt.ReadReversed32bit();
//			uint healingDone = pkt.ReadReversed32bit();
//
//			switch(_battleZoneType)
//			{
//			case (uint)BattleGroundTypeId.BATTLEGROUND_WS:
//				uint fieldsCount = pkt.ReadReversed32bit();
//				uint flagCaptures = pkt.ReadReversed32bit();
//				uint flagReturns = pkt.ReadReversed32bit();
//				break;
//			}
//		}
	}

	public void GetBattleZoneList()
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_BATTLEFIELD_LIST, 6);
		uint bgtype = 32;
		byte interactionType = 1;
		byte aux = 0;

		pkt.Add(bgtype);
		pkt.Append(interactionType);
		pkt.Append(aux);
		RealmSocket.outQueue.Add(pkt);
	}

	public void AddToWaitList(ulong guid, uint bgType, uint bgID, byte isGroup)
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_BATTLEMASTER_JOIN, 17);
		pkt.Add(guid);
		pkt.Add(bgType);
		pkt.Add(bgID);
		pkt.Append(isGroup);
		RealmSocket.outQueue.Add(pkt);
	}

	public void LeaveBattleZoneQuerry()
	{
		byte unk1 = 0;
		ushort unk2 = 0;
		uint unk3 = 0;
		
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_LEAVE_BATTLEFIELD, 8);
		pkt.Append(unk1);
		pkt.Append(unk1);
		pkt.Add(unk3);
		pkt.Add(unk2);
		RealmSocket.outQueue.Add(pkt);

		battleZoneWindow.SetWindowState(BattleZoneWindowState.BATTLEZONE_LIST_WINDOW_STATE);
	}

	public void BattleFieldStatus(WorldPacket pkt)
	{
		uint queueSlot = pkt.ReadReversed32bit();
		byte arenaType = pkt.Read();
		byte typeID = pkt.Read();
		pkt.ReadTypeReversed(6);							// Const value = 0x1F90
		pkt.Read();											// Const value = 0
		pkt.Read();											// Const value = 0
		uint clientInstanceID = pkt.ReadReversed32bit();
		byte isRaited = pkt.Read();
		uint status = pkt.ReadReversed32bit();
		switch(status)
		{	
		case (uint)BattleGroundStatus.STATUS_WAIT_QUEUE:
			uint waitTime = pkt.ReadReversed32bit();
			uint queueTime = pkt.ReadReversed32bit();
			break;
		case (uint)BattleGroundStatus.STATUS_WAIT_JOIN:
			uint mapID = pkt.ReadReversed32bit();
			pkt.ReadReversed64bit();						// Const value = 0
			uint timeToJoin = pkt.ReadReversed32bit();

			if(timeToJoin == 40000)
			{
				battleZoneWindow.parent.parent.parentMng.messageWnd.InitTwoButtonWindow("BattleZone is ready.", "Enlist", "Leave");
				battleZoneWindow.parent.parent.parentMng.messageWnd.onOkButton += TeleportToBattleZone;
			}
			break;
		case (uint)BattleGroundStatus.STATUS_IN_PROGRESS:
			uint mapIDBZ = pkt.ReadReversed32bit();
			pkt.ReadReversed64bit();						// Const value = 0
			uint time1 = pkt.ReadReversed32bit();
			uint time2 = pkt.ReadReversed32bit();

			timeLeft = 25 - time2 / 60000;
			pkt.Read();										// Const value = 1

			battleZoneWindow.SetWindowState(BattleZoneWindowState.BATTLEZONE_INFO_WINDOW_STATE);
			break;
		}
	}

	private void TeleportToBattleZone(bool state)
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_BATTLEFIELD_PORT, 9);
		byte arenaType = 0;
		byte unk = 0;
		uint bgID = 2;
		ushort unk2 = 0x1F90;
		byte action = (state == true) ? (byte)1 : (byte)0;
		pkt.Append(arenaType);
		pkt.Append(unk);
		pkt.Add(bgID);
		pkt.Add(unk2);
		pkt.Append(action);
		RealmSocket.outQueue.Add(pkt);

		battleZoneWindow.battleZoneListWindow.joinButtonText = "Enlist";
		battleZoneWindow.battleZoneListWindow.joinGroupButtonText = "Premade";
		battleZoneWindow.parent.parent.parentMng.SetWindowState(InterfaceWindowState.ACTION_BAR_PANEL_STATE);
	}
}
