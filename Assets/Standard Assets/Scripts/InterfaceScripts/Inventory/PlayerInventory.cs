using UnityEngine;
using System.Collections;

public enum PlayerInventoryWindowState
{
	PLAYER_EQUIPMENT_STATE,
	GEM_EQUIP_STATE,
	RE_FORGE_STATE,
	PET_FEED_STATE
}

public class PlayerInventory : MonoBehaviour
{
	public PlayerMenuManager Parent;

	public InventoryWindow InventoryWnd;				// Player Inventory
	public PlayerEquipmentWindow PlayerEquipmentWnd;	// Player Equipment
	public GemWindow GemEquipWnd;						// Gem Equip Window
	public ReForgeWindow ReForgeWnd;					// Re-Forge Window
	public PetFeedWindow PetFeedWnd;					// Pet Feed Window
	
	private PlayerInventoryWindowState WindowState;
	private Player PlayerScript;

	protected float SW;
	protected float SH;

	public void DrawGUI()
	{
		InventoryWnd.DrawGUI();

		switch(WindowState)
		{
		case PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE:
			PlayerEquipmentWnd.DrawGUI();
			break;
		case PlayerInventoryWindowState.GEM_EQUIP_STATE:
			GemEquipWnd.DrawGUI();
			break;
		case PlayerInventoryWindowState.RE_FORGE_STATE:
			ReForgeWnd.DrawGUI();
			break;
		case PlayerInventoryWindowState.PET_FEED_STATE:
			PetFeedWnd.DrawGUI();
			break;
		}

		if(IsDragBegin)
		{
			Rect dragRect = DragItem.position;
			dragRect.center = new Vector2(Input.mousePosition.x, SH - Input.mousePosition.y);
			GUI.DrawTexture(dragRect, DragItem.texture);
		}
	}

	private void OnEnable()
	{
		SW = Screen.width;
		SH = Screen.height;
		PlayerScript = InterfaceManager.Instance.PlayerScript;

		GameObject InventoryGO = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/Inventory"));
		if(InventoryGO != null)
		{
			InventoryWnd = InventoryGO.GetComponent<InventoryWindow>();
			InventoryWnd.transform.parent = this.transform;
			InventoryWnd.parent = this;
		}

		GameObject EquipmentGO = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/Player Equipment"));
		if(EquipmentGO != null)
		{
			PlayerEquipmentWnd = EquipmentGO.GetComponent<PlayerEquipmentWindow>();
			PlayerEquipmentWnd.transform.parent = this.transform;
		}

		WindowState = PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE;
	}

	private void OnDisable()
	{
		DastroyWindowByState(WindowState);
		Destroy(InventoryWnd.gameObject);
		InventoryWnd = null;
	}

	public void SetWindowState(PlayerInventoryWindowState newState)
	{
		DastroyWindowByState(WindowState);
		
		switch(newState)
		{
		case PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE:
			GameObject EquipmentGO = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/Player Equipment"));
			if(EquipmentGO != null)
			{
				PlayerEquipmentWnd = EquipmentGO.GetComponent<PlayerEquipmentWindow>();
				PlayerEquipmentWnd.transform.parent = this.transform;
			}
			break;
		case PlayerInventoryWindowState.GEM_EQUIP_STATE:
			GameObject GemWindowGO = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/Gem Equip Window"));
			if(GemWindowGO != null)
			{
				GemEquipWnd = GemWindowGO.GetComponent<GemWindow>();
				GemEquipWnd.transform.parent = this.transform;
				GemEquipWnd.parent = this;
			}
			break;
		case PlayerInventoryWindowState.RE_FORGE_STATE:
			GameObject ReForgeWindowGO = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/Re-Forge Window"));
			if(ReForgeWindowGO != null)
			{
				ReForgeWnd = ReForgeWindowGO.GetComponent<ReForgeWindow>();
				ReForgeWnd.transform.parent = this.transform;
				ReForgeWnd.parent = this;
			}
			break;
		case PlayerInventoryWindowState.PET_FEED_STATE:
			GameObject PetFeedWindowGO = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/Pet Feed Window"));
			if(PetFeedWindowGO != null)
			{
				PetFeedWnd = PetFeedWindowGO.GetComponent<PetFeedWindow>();
				PetFeedWnd.transform.parent = this.transform;
				PetFeedWnd.parent = this;
			}
			break;
		}
		WindowState = newState;
	}

	public void DastroyWindowByState(PlayerInventoryWindowState state)
	{
		switch(WindowState)
		{
		case PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE:
			Destroy(PlayerEquipmentWnd.gameObject);
			PlayerEquipmentWnd = null;
			break;
		case PlayerInventoryWindowState.GEM_EQUIP_STATE:
			Destroy(GemEquipWnd.gameObject);
			GemEquipWnd = null;
			break;
		case PlayerInventoryWindowState.RE_FORGE_STATE:
			Destroy(ReForgeWnd.gameObject);
			ReForgeWnd = null;
			break;
		case PlayerInventoryWindowState.PET_FEED_STATE:
			Destroy(PetFeedWnd.gameObject);
			PetFeedWnd = null;
			break;
		}
	}

	private ItemButton DragItem;
	private bool IsDragBegin = false;
	
	public void LateUpdate()
	{
		Vector2 touchPosition = new Vector2(Input.mousePosition.x, SH - Input.mousePosition.y);
		if(Input.GetMouseButtonDown(0))
		{
			ItemButton itemButton = InventoryWnd.mng.IsTouchInSlotsContainer(touchPosition);
			if(itemButton != null && itemButton.texture != null && 
			   !InventoryWnd.mng.blockedItems.Contains(itemButton.item.guid))
			{
				DragItem = itemButton;
				IsDragBegin = true;
				return;
			}

			itemButton = InventoryWnd.mng.IsTouchInBagContainer(touchPosition);
			if(itemButton != null)
			{
				if(itemButton.item != null && itemButton.item.entry != 0 && (itemButton.item as Bag).IsEmpty())
				{
					DragItem = itemButton;
					IsDragBegin = true;
				}
				return;
			}

			switch(WindowState)
			{
			case PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE:
				itemButton = PlayerEquipmentWnd.mng.IsTouchInEquipmentSlotsContainer(touchPosition);
				if(itemButton != null)
				{
					DragItem = itemButton;
					IsDragBegin = true;
				}
				break;
			case PlayerInventoryWindowState.GEM_EQUIP_STATE:
				GemSlot gemSlot = GemEquipWnd.mng.IsTouchInGemSlot(touchPosition);
				if(gemSlot != null)
				{
					if(gemSlot.item != null)
					{
						DragItem = new ItemButton();
						DragItem.item = gemSlot.item;
						DragItem.texture = gemSlot.GemIcon;
						DragItem.position = gemSlot.Position;
						IsDragBegin = true;
					}
				}
				break;
			case PlayerInventoryWindowState.RE_FORGE_STATE:
				itemButton = ReForgeWnd.mng.IsTouchInReForgeSlot(touchPosition);
				if(itemButton != null)
				{
					if(itemButton.item != null)
					{
						DragItem = itemButton;
						IsDragBegin = true;
					}
				}
				break;
			case PlayerInventoryWindowState.PET_FEED_STATE:
				itemButton = PetFeedWnd.mng.IsTouchInFoodSlot(touchPosition);
				if(itemButton != null)
				{
					if(itemButton.item != null)
					{
						DragItem = itemButton;
						IsDragBegin = true;
					}
				}
				break;
			}
		}
		
		if(IsDragBegin)
		{
			if(Input.GetMouseButtonUp(0))
			{
				IsDragBegin = false;

				if(DragItem.position.Contains(touchPosition))
				{
					return;
				}

				ItemButton itemButton = InventoryWnd.mng.IsTouchInSlotsContainer(touchPosition);
				if(itemButton != null)
				{
					if(DragItem.item.classType == ItemClass.ITEM_CLASS_PROJECTILE)
					{
						Item.SendEquipAmmo(0);
					}
					else
					{
						byte bag = (byte)itemButton.item.GetSlotByBag();
						byte slot = itemButton.item.slot;
						PlayerScript.itemManager.MoveToSlot(DragItem.item, slot, bag);
						UpdateBlockedItems(DragItem.item, true);
					}
					return;
				}
			
				itemButton = InventoryWnd.mng.IsTouchInBagContainer(touchPosition);
				if(itemButton != null)
				{
					if(DragItem.item.classType == ItemClass.ITEM_CLASS_PROJECTILE)
					{
						Item.SendEquipAmmo(0);
						return;
					}
				
					if(DragItem.item.classType == ItemClass.ITEM_CLASS_CONTAINER &&
					   itemButton.item.bag == 255 && itemButton.item.slot != 255 &&
					   (itemButton.item.entry == 0 || (itemButton.item as Bag).IsEmpty()))
					{
						byte bag = itemButton.item.bag;
						byte slot = itemButton.item.slot;
						PlayerScript.itemManager.MoveToSlot(DragItem.item, slot, bag);
					}
					else
					{
						byte bag = itemButton.item.slot;
						byte slot = PlayerScript.itemManager.GetFirstEmptySlotInBag(itemButton.item.GetBagBySlot());
						PlayerScript.itemManager.MoveToSlot(DragItem.item, slot, bag);
					}
					return;
				}

				switch(WindowState)
				{
				case PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE:
					if(PlayerEquipmentWnd.mng.IsTouchInEquipmentContainer(touchPosition))
					{
						if(!DragItem.item.IsEquiped())
						{
							DragItem.item.EquipItem();
						}
					}
					break;
				case PlayerInventoryWindowState.GEM_EQUIP_STATE:
					GemSlot gemSlot = GemEquipWnd.mng.IsTouchInGemSlot(touchPosition);
					if(gemSlot != null)
					{
						if(GemEquipWnd.mng.InsertGemToSlot(gemSlot, DragItem.item))
						{
							InventoryWnd.mng.blockedItems.Add(DragItem.item.guid);
							UpdateBlockedItems(gemSlot.item, false);
							gemSlot.item = DragItem.item;
						}
					}
					break;
				case PlayerInventoryWindowState.RE_FORGE_STATE:
					itemButton = ReForgeWnd.mng.IsTouchInReForgeSlot(touchPosition);
					if(itemButton != null)
					{
						if(ReForgeWnd.mng.InsertItemToSlot(DragItem.item))
						{
							InventoryWnd.mng.blockedItems.Add(DragItem.item.guid);
							UpdateBlockedItems(itemButton.item, false);
							itemButton.item = DragItem.item;
						}
					}
					break;
				case PlayerInventoryWindowState.PET_FEED_STATE:
					itemButton = PetFeedWnd.mng.IsTouchInFoodSlot(touchPosition);
					if(itemButton != null)
					{
						if(PetFeedWnd.mng.InsertItemToSlot(DragItem.item))
						{
							InventoryWnd.mng.blockedItems.Add(DragItem.item.guid);
							UpdateBlockedItems(itemButton.item, false);
							itemButton.item = DragItem.item;
						}
					}
					break;
				}
			}
		}
	}

	public void UpdateBlockedItems(Item item, bool UpdateWindow)
	{
		if(item != null && InventoryWnd.mng.blockedItems.Contains(item.guid))
		{
			InventoryWnd.mng.blockedItems.Remove(item.guid);
		}

		if(UpdateWindow)
		{
			switch(WindowState)
			{
			case PlayerInventoryWindowState.GEM_EQUIP_STATE:
				GemEquipWnd.mng.UpdateGemSlots(InventoryWnd.mng.blockedItems, item.guid);
				break;
			case PlayerInventoryWindowState.RE_FORGE_STATE:
				ReForgeWnd.mng.UpdateReForgeSlot(InventoryWnd.mng.blockedItems, item.guid);
				break;
			case PlayerInventoryWindowState.PET_FEED_STATE:
				PetFeedWnd.mng.UpdateFoodSlot(InventoryWnd.mng.blockedItems, item.guid);
				break;
			}
		}
	}
}
