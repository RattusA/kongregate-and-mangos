﻿using UnityEngine;
using System.Collections.Generic;

public class GemSlot {

	public Texture GemIcon;
	public SocketColor SocketColorType;
	public string GemDescription;
	public Rect Position;
	public Item item;
	public uint EnchantEntry = 0;
	public string IconName = "";
}

public class GemManager {

	public List<GemSlot> GemList = new List<GemSlot>();
	public string ItemName;
	public Texture ItemTexture;
	public string GemBonus;
	public Item SelectedItem;

	public void GetGemSlotInfo(Item item)
	{
		SelectedItem = item;
		ItemName = item.name;

		string iconName = DefaultIcons.GetItemIcon(item.entry);
		iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
		ItemTexture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));

		GemList = new List<GemSlot>();
		for(int gemSlotIndex = 0; gemSlotIndex < item.socket.Length; gemSlotIndex++)
		{
			ItemSocket socket = item.socket[gemSlotIndex];
			if(socket.color == (uint)SocketColor.SOCKET_COLOR_NONE)
			{
				GemList.Add(null);
				continue;
			}

			GemSlot newGemSlot = new GemSlot();
			newGemSlot.SocketColorType = (SocketColor)socket.color;

			float padding = Screen.height * 0.15f * GemList.Count;
			newGemSlot.Position = new Rect(Screen.width * 0.05f, Screen.height * 0.43f + padding,
			                               Screen.height * 0.12f, Screen.height * 0.12f);

			uint enchantmentID = item.GetEnchantmentId((int)EnchantmentSlot.SOCK_ENCHANTMENT_SLOT + gemSlotIndex);
			if(enchantmentID > 0)
			{
				newGemSlot.EnchantEntry = enchantmentID;
				newGemSlot.GemDescription = GetEnchantmentDescription(enchantmentID);
				SpellItemEnchantmentEntry gemEnchantment = dbs.sSpellItemEnchantment.GetRecord(enchantmentID);
				if(gemEnchantment != null)
				{
					iconName = DefaultIcons.GetItemIcon((uint)gemEnchantment.GemID);
					iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
					newGemSlot.GemIcon = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
					newGemSlot.IconName = iconName;

					newGemSlot.GemDescription = gemEnchantment.description.Strings[0];
				}
			}
			else
			{
				newGemSlot.GemIcon = null;
				newGemSlot.GemDescription = "";
			}
			GemList.Add(newGemSlot);
		}

		GemBonus = GetEnchantmentDescription(item.socketBonus);
		//Check gem slot bonus and change text color to grey if the condition is not correct
		uint bonusEnchantmentID = SelectedItem.GetEnchantmentId((int)EnchantmentSlot.BONUS_ENCHANTMENT_SLOT);
		if( bonusEnchantmentID <= 0 )
		{
			GemBonus = "<color=#404040ff>"+GemBonus+"</color>";
		}
	}

	private string GetEnchantmentDescription(uint enchantmentID)
	{
		string ret = "";
		SpellItemEnchantmentEntry gemEnchantment = dbs.sSpellItemEnchantment.GetRecord(enchantmentID);
		if(gemEnchantment != null)
		{
			ret = gemEnchantment.description.Strings[0];
		}

		return ret;
	}

	public bool InsertGemToSlot(GemSlot gemSlot, Item item)
	{
		if(item.classType != ItemClass.ITEM_CLASS_GEM )
		{
			InterfaceManager.Instance.SetInventoryMessage("You can insert into the slot only gems");
			return false;
		} 
		else if((item.subClass == (int)ItemSubclassGem.ITEM_SUBCLASS_GEM_META && 
		         gemSlot.SocketColorType != SocketColor.SOCKET_COLOR_META))
		{
			InterfaceManager.Instance.SetInventoryMessage("Super gem can only be inserted into a super slot");
			return false;
		}
		else if((item.subClass != (int)ItemSubclassGem.ITEM_SUBCLASS_GEM_META &&
		 		 gemSlot.SocketColorType == SocketColor.SOCKET_COLOR_META))
		{
			InterfaceManager.Instance.SetInventoryMessage("In the super slot can only fit super gem");
			return false;
		}
		else if(item.subClass == (int)ItemSubclassGem.ITEM_SUBCLASS_GEM_SIMPLE || item.gemProperties == 0)
		{
			InterfaceManager.Instance.SetInventoryMessage("In the slot can only be inserted faceted gems");
			return false;
		}

		string iconName = DefaultIcons.GetItemIcon(item.entry);
		iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
		gemSlot.GemIcon = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));

		GemPropertiesEntry gemPropherty = dbs.sGemProperties.GetRecord(item.gemProperties);
		if(gemPropherty != null)
		{
			SpellItemEnchantmentEntry gemEnchantment = dbs.sSpellItemEnchantment.GetRecord(gemPropherty.spellitemenchantement);
			if(gemEnchantment != null)
			{
				gemSlot.GemDescription = gemEnchantment.description.Strings[0];
			}
			else
			{
				gemSlot.GemDescription = "";
			}
		}
		else
		{
			gemSlot.GemDescription = "";
		}
		return true;
	}

	public GemSlot IsTouchInGemSlot(Vector2 touch)
	{
		foreach(GemSlot gemSlot in GemList)
		{
			if(gemSlot != null && gemSlot.Position.Contains(touch))
			{
				return gemSlot;
			}
		}
		return null;
	}

	public void InsertGems()
	{
		bool isHaveChanges = false;

		foreach(GemSlot gemSlot in GemList)
		{
			if(gemSlot != null && gemSlot.item != null)
			{
				isHaveChanges = true;
				break;
			}
		}

		if(!isHaveChanges)
		{
			return;
		}

		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_SOCKET_GEMS);
		pkt.Add(SelectedItem.guid);
		foreach(GemSlot gemSlot in GemList)
		{
			ulong guid = 0;
			if(gemSlot != null && gemSlot.item != null)
			{
				guid = gemSlot.item.guid;
			}
			pkt.Add(guid);
		}
		RealmSocket.outQueue.Add(pkt);
	}

	public void UpdateGemSlots(List<ulong> BlockedItems, ulong guid)
	{
		foreach(GemSlot gemSlot in GemList)
		{
			if(gemSlot != null && gemSlot.item != null && gemSlot.item.guid == guid && 
			   !BlockedItems.Contains(gemSlot.item.guid))
			{
				gemSlot.GemIcon = null;
				gemSlot.item = null;

				int offset = (int)EnchantmentSlot.SOCK_ENCHANTMENT_SLOT + GemList.IndexOf(gemSlot);
				uint enchantmentID = SelectedItem.GetEnchantmentId(offset);
				if(enchantmentID > 0)
				{
					gemSlot.GemDescription = GetEnchantmentDescription(enchantmentID);
					SpellItemEnchantmentEntry gemEnchantment = dbs.sSpellItemEnchantment.GetRecord(enchantmentID);
					if(gemEnchantment != null)
					{
						string iconName = DefaultIcons.GetItemIcon((uint)gemEnchantment.GemID);
						iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
						gemSlot.GemIcon = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
						
						gemSlot.GemDescription = gemEnchantment.description.Strings[0];
					}
				}
				else
				{
					gemSlot.GemIcon = null;
					gemSlot.GemDescription = "";
				}
			}
		}
	}

	void FixedUpdate()
	{
		foreach(GemSlot gemSlot in GemList)
		{
			if(gemSlot != null)
			{
				if(gemSlot.IconName.Equals("noicon") && (gemSlot.item == null) && (gemSlot.EnchantEntry > 0))
				{
					SpellItemEnchantmentEntry gemEnchantment = dbs.sSpellItemEnchantment.GetRecord(gemSlot.EnchantEntry);
					if(gemEnchantment != null)
					{
						string iconName = DefaultIcons.GetItemIcon((uint)gemEnchantment.GemID);
						iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
						gemSlot.GemIcon = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
						gemSlot.IconName = iconName;
					}
				}
			}
		}
	}
}