﻿using UnityEngine;
using System.Collections;

public enum ModalInfoWindow
{
	None,
	Description,
	Destroy
}

public class InventoryWindowBase : MonoBehaviour
{
	public GUISkin skin;

	public ModalInfoWindow modalInfoWindowState;
	public DescriptionWindow descriptionWnd;
	public Item selectedItem;

	public InventoryManager mng;

	const int BAGS_COUNT = 5;

	private Rect _modalRect;
	private Rect _inventoryPanelRect;

	private Rect _destroyWindowBackgroundRect;
	private Rect _destroyWindowMessageLabelRect;
	private Rect _destroyWindowOneButtonRect;
	private Rect _destroyWindowAllButtonRect;
	private Rect _destroyWindowCancelButtonRect;

	public void Awake()
	{
		mng = new InventoryManager();
		mng.InitInventory();

		skin = Resources.Load<GUISkin> ("GUI/InventorySkin");
		
		float SW = Screen.width;
		float SH = Screen.height;
		
		skin.button.fontSize = (int)(SH * 0.04f);
		skin.label.fontSize = (int)(SH * 0.06f);
		
		_modalRect = new Rect(0, 0, SW, SH);
		_inventoryPanelRect = new Rect(SW * 0.75f - SH * 0.25f, SH * 0.25f, SH * 0.55f, SH * 0.545f);
		
		_destroyWindowBackgroundRect = new Rect(SW * 0.2f, SH * 0.35f, SW * 0.6f, SH * 0.3f);
		_destroyWindowMessageLabelRect = new Rect(SW * 0.22f, SH * 0.4f, SW * 0.56f, SH * 0.15f);
		_destroyWindowOneButtonRect = new Rect(SW * 0.22f, SH * 0.56f, SW * 0.18f, SH * 0.07f);
		_destroyWindowAllButtonRect = new Rect(SW * 0.41f, SH * 0.56f, SW * 0.18f, SH * 0.07f);
		_destroyWindowCancelButtonRect = new Rect(SW * 0.6f, SH * 0.56f, SW * 0.18f, SH * 0.07f);
	}

	public void OnDestroy()
	{
		mng = null;

//		Resources.UnloadAsset(skin);
		skin = null;
	}

	public void DrawGUI()
	{
		for(int bagIndex = 0; bagIndex < BAGS_COUNT; bagIndex++)
		{
			ItemButton bagButton;
			mng.inventoryBags.TryGetValue(bagIndex, out bagButton);
			if(bagButton != null)
			{
				DrawBagButton(bagButton.position, bagIndex);
			}
		}

		GUI.Box(_inventoryPanelRect, "", skin.GetStyle("InventorySlotsContainer"));

		for(int slotIndex = 0; slotIndex < mng.bagSlotCount; slotIndex++)
		{
			ItemButton itemButton;
			mng.bagSlots.TryGetValue(slotIndex, out itemButton);
			if(itemButton != null && itemButton.texture != null)
			{
				GUI.DrawTexture(itemButton.position, itemButton.texture);
				if(mng.blockedItems.Contains(itemButton.item.guid))
				{
					GUI.Label(itemButton.position, "", skin.GetStyle("LockedItem"));
				}
				else if(GUI.Button(itemButton.position, "", GUIStyle.none))
				{
					ItemEvent(itemButton);
				}
			}
		}
		
		switch(modalInfoWindowState)
		{
		case ModalInfoWindow.Description:
			GUI.ModalWindow(0, _modalRect, descriptionWnd.DrawGUI, "");
			break;
		case ModalInfoWindow.Destroy:
			GUI.ModalWindow(0, _modalRect, DrawDestoyItemWindow, "");
			break;
		}
	}

	public virtual void ItemEvent(ItemButton itemButton)
	{
		descriptionWnd = new DescriptionWindow();
		descriptionWnd.IconTecture = itemButton.texture;
		descriptionWnd.TitleText = itemButton.item.name;
		descriptionWnd.DescriptionTextValue = itemButton.item.GetItemBasicStatsOutName();

		InfoWindowButton closeButton = descriptionWnd.ButtonManager.AddFindButton(InfoWindowButtonType.Close);
		closeButton.ButtonName = "Close";
		closeButton.onButtonUp += CloseDescriptionWindow;

		InfoWindowButton deleteButton = descriptionWnd.ButtonManager.AddFindButton(InfoWindowButtonType.Delete);
		deleteButton.ButtonName = "Destroy";
		deleteButton.item = itemButton.item;
		deleteButton.onButtonUpWithSender += DestroyItemWindow;

		modalInfoWindowState = ModalInfoWindow.Description;
	}

	public void CloseDescriptionWindow()
	{
		descriptionWnd = null;
		modalInfoWindowState = ModalInfoWindow.None;
	}

	public void DestroyItemWindow(InfoWindowButton sender)
	{
		modalInfoWindowState = ModalInfoWindow.Destroy;
		selectedItem = sender.item;
		descriptionWnd = null;
	}

	protected void DrawDestoyItemWindow(int index)
	{
		GUI.Box(_destroyWindowBackgroundRect, "", skin.GetStyle("DestroyWindowBackground"));
		GUI.Label(_destroyWindowMessageLabelRect, "Do you want to destroy item(s)?", skin.label);
		
		if(GUI.Button(_destroyWindowOneButtonRect, "One", skin.button))
		{
			InterfaceManager.Instance.PlayerScript.itemManager.DestroyItem(selectedItem, 1);
			modalInfoWindowState = ModalInfoWindow.None;
		}
		
		if(selectedItem.count > 1 && GUI.Button(_destroyWindowAllButtonRect, "All", skin.button))
		{
			InterfaceManager.Instance.PlayerScript.itemManager.DestroyItem(selectedItem, 0);
			modalInfoWindowState = ModalInfoWindow.None;
		}
		
		if(GUI.Button(_destroyWindowCancelButtonRect, "Cancel", skin.button))
		{
			modalInfoWindowState = ModalInfoWindow.None;
		}
	}

	private void DrawBagButton(Rect position, int index)
	{
		ItemButton itemButton;
		mng.inventoryBags.TryGetValue(index, out itemButton);
		if(index == 0)
		{
			if(GUI.Button(position, "", skin.GetStyle("DefaultBag")))
			{
				mng.ShowBagSlots(index);
			}
		}
		else if(itemButton.item == null || itemButton.item.guid == 0 || itemButton.item.entry == 0)
		{
			GUI.Label(position, "", skin.GetStyle("EmptyBagSlot"));
		}
		else if(GUI.Button(position, itemButton.texture, GUIStyle.none))
		{
			mng.ShowBagSlots(index);
		}
	}

	public void FixedUpdate()
	{
		if(InterfaceManager.Instance.PlayerScript.itemManager.NeedUpdateInventory)
		{
			mng.InitInventory();
			InterfaceManager.Instance.PlayerScript.itemManager.NeedUpdateInventory = false;
		}
	}
}