﻿using UnityEngine;
using System.Collections.Generic;

public class ReForgeManager {

	public ItemButton ReForgeItem;
	public ItemButton TargetItem;

	public bool ReWriteReForge = false;

	public void GetReForgeInfo(Item item)
	{
		ReForgeItem = new ItemButton();
		ReForgeItem.item = item;

		string iconName = DefaultIcons.GetItemIcon(item.entry);
		iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
		ReForgeItem.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));

		ReForgeItem.position = new Rect(Screen.width * 0.045f, Screen.height * 0.1575f,
		                                Screen.width * 0.09f, Screen.width * 0.09f);

		TargetItem = new ItemButton();
		TargetItem.position = new Rect(Screen.width * 0.045f, Screen.height * 0.435f + Screen.width * 0.1f,
		                               Screen.width * 0.09f, Screen.width * 0.09f);
	}

	public ItemButton IsTouchInReForgeSlot(Vector2 touch)
	{
		if(TargetItem.position.Contains(touch))
		{
			return TargetItem;
		}
		return null;
	}

	public bool InsertItemToSlot(Item item)
	{
		bool ret = false;

		//Check the condition of items that can be enchanted.
		if((item.classType == ItemClass.ITEM_CLASS_ARMOR
		    && item.inventoryType != InventoryType.INVTYPE_NON_EQUIP
		    && item.inventoryType != InventoryType.INVTYPE_BODY
		    && item.inventoryType != InventoryType.INVTYPE_FINGER
		    && item.inventoryType != InventoryType.INVTYPE_NECK
		    && item.inventoryType != InventoryType.INVTYPE_TABARD
		    && item.inventoryType != InventoryType.INVTYPE_TRINKET
		    && item.inventoryType != InventoryType.INVTYPE_RELIC
		    && item.inventoryType != InventoryType.INVTYPE_HOLDABLE)
		   || (item.classType == ItemClass.ITEM_CLASS_WEAPON
		    && item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_obsolete
		    && item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_EXOTIC
		    && item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_EXOTIC2
		    && item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_MISC
		    && item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_THROWN
		    && item.subClass != (int)ItemSubclassWeapon.ITEM_SUBCLASS_WEAPON_WAND))
		{
			string iconName = DefaultIcons.GetItemIcon(item.entry);
			iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
			TargetItem.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
			ReWriteReForge = false;
			ret = true;
		}
		return ret;
	}

	public bool UseReForgeOnItem()
	{
		if(TargetItem.item == null)
		{
			InterfaceManager.Instance.SetInventoryMessage("You have not added a subject for reforging");
			return false;
		}

		if(((ReForgeItem.item.classType == ItemClass.ITEM_CLASS_TRADE_GOODS
		    && ReForgeItem.item.subClass == (int)ItemSubclassTradeGoods.ITEM_SUBCLASS_TRADE_GOODS_OTHER)
		    && (TargetItem.item.GetEnchantmentId((int)EnchantmentSlot.TEMP_ENCHANTMENT_SLOT) > 0))
		   || ((ReForgeItem.item.classType == ItemClass.ITEM_CLASS_CONSUMABLE
		    && ReForgeItem.item.subClass == (int)ItemSubclassConsumable.ITEM_SUBCLASS_ITEM_ENHANCEMENT)
		    && (TargetItem.item.GetEnchantmentId((int)EnchantmentSlot.PERM_ENCHANTMENT_SLOT) > 0)))
		{
			ReWriteReForge = true;
		}
		else
		{
			UseReForge();
			return true;
		}
		return false;
	}

	public void UseReForge()
	{
		byte cast_count = 0, unkFlags = 0;
		uint spellID = 0;
		uint glyphIndex = 0;
		
		SpellCastTargets targets = new SpellCastTargets();
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_USE_ITEM);
		pkt.Append(ReForgeItem.item.GetSlotByBag());
		pkt.Append(ReForgeItem.item.slot);		
		pkt.Append(cast_count);
		pkt.Add(spellID);
		pkt.Add(ReForgeItem.item.guid);						//get item guid
		pkt.Add(glyphIndex);
		pkt.Append(unkFlags);
		pkt.Append(targets.WriteItem(TargetItem.item.guid));
		
		RealmSocket.outQueue.Add(pkt);
	}

	public void UpdateReForgeSlot(List<ulong> BlockedItems, ulong guid)
	{
		if(TargetItem.item != null && TargetItem.item.guid == guid && !BlockedItems.Contains(TargetItem.item.guid))
		{
			TargetItem.item = null;
			TargetItem.texture = null;
		}
	}
}
