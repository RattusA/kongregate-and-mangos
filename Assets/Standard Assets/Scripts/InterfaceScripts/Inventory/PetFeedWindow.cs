using UnityEngine;

public class PetFeedWindow : MonoBehaviour
{
	public GUISkin skin;
	public PlayerInventory parent;
	public PetFeedManager mng;

	private float SW;
	private float SH;
	private ScrollZone TextZone = new ScrollZone();

	public void Awake()
	{
		SW = Screen.width;
		SH = Screen.height;

		skin.button.fontSize = (int)(Screen.height * 0.04f);
		skin.label.fontSize = (int)(Screen.height * 0.04f);
		skin.GetStyle("Description Text").fontSize = (int)(Screen.height * 0.0375f);
	}

	public void InitPetFeedWindow()
	{
		mng = new PetFeedManager();
		mng.PlayerScript = InterfaceManager.Instance.PlayerScript;
		mng.GetPetInfo();

		Rect statsRect = new Rect(SW * 0.05f, SH * 0.305f, SW * 0.4f, SH * 0.24f);
		TextZone.InitSkinInfo(skin.GetStyle("Description Text"));
		TextZone.CreateScrollZoneWithText(statsRect, mng.PetInfo, WorldSession.player.mainTextColor);
	}
	
	public void OnDestroy()
	{
		parent.InventoryWnd.mng.blockedItems.Clear();

		mng = null;
		parent = null;
	}

	public void DrawGUI()
	{
		GUI.skin = skin;
		
		GUI.Box(new Rect(SW * 0.02f, SH * 0.12f, SW * 0.46f, SH * 0.86f), "");
		GUI.Label(new Rect(SW * 0.04f, SH * 0.15f, SW * 0.42f, SW * 0.1f), mng.PetName);
		
		GUI.Box(new Rect(SW * 0.04f, SH * 0.295f, SW * 0.42f, SH * 0.26f), "", skin.GetStyle("ReForge Description"));
		TextZone.DrawTraitsInfo();
		
		GUI.Label(new Rect(SW * 0.04f, SH * 0.58f, SW * 0.42f, SH * 0.05f), "", skin.GetStyle("Decoration"));
		
		if(mng.FoodItem != null && mng.FoodItem.texture != null)
		{
			GUI.DrawTexture(mng.FoodItem.position, mng.FoodItem.texture);
		}
		GUI.Label(new Rect(SW * 0.18f, SH * 0.66f, SW * 0.14f, SW * 0.14f), "", skin.GetStyle("Item Decoration"));

		if(GUI.Button(new Rect(SW * 0.04f, SH * 0.88f, SW * 0.2f, SH * 0.07f), "Feed"))
		{
			mng.FeedPet();
			parent.SetWindowState(PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE);
		}
		
		if(GUI.Button(new Rect(SW * 0.26f, SH * 0.88f, SW * 0.2f, SH * 0.07f), "Close"))
		{
			parent.SetWindowState(PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE);
		}
	}
}
