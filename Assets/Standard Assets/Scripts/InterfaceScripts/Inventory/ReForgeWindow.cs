using UnityEngine;

public class ReForgeWindow : MonoBehaviour
{
	public GUISkin skin;
	public PlayerInventory parent;
	public ReForgeManager mng;

	private float SW;
	private float SH;
	private ScrollZone TextZone = new ScrollZone();

	public void Awake()
	{
		SW = Screen.width;
		SH = Screen.height;

		skin.button.fontSize = (int)(Screen.height * 0.04f);
		skin.label.fontSize = (int)(Screen.height * 0.04f);

		skin.GetStyle("Description Text").fontSize = (int)(Screen.height * 0.0375f);
		skin.GetStyle("Warning Text").fontSize = (int)(Screen.height * 0.035f);
	}

	public void InitReForgeWindow(Item item)
	{
		mng = new ReForgeManager();
		mng.GetReForgeInfo(item);

		Rect statsRect = new Rect(SW * 0.05f, SH * 0.17f + SW * 0.1f, SW * 0.4f, SH * 0.19f);
		TextZone.InitSkinInfo(skin.GetStyle("Description Text"));
		TextZone.CreateScrollZoneWithText(statsRect, mng.ReForgeItem.item.description, WorldSession.player.mainTextColor);
	}

	public void OnDestroy()
	{
		parent.InventoryWnd.mng.blockedItems.Clear();

		mng = null;
		parent = null;
	}

	public void DrawGUI()
	{
		GUI.skin = skin;

		GUI.Box(new Rect(SW * 0.02f, SH * 0.12f, SW * 0.46f, SH * 0.86f), "");
		GUI.DrawTexture(mng.ReForgeItem.position, mng.ReForgeItem.texture);
		GUI.Label(new Rect(SW * 0.04f, SH * 0.15f, SW * 0.1f, SW * 0.1f), "",
		          skin.GetStyle("Item Decoration"));
		GUI.Label(new Rect(SW * 0.16f, SH * 0.15f, SW * 0.3f, SW * 0.1f), mng.ReForgeItem.item.name);

		GUI.Box(new Rect(SW * 0.04f, SH * 0.16f + SW * 0.1f, SW * 0.42f, SH * 0.21f), "",
		        skin.GetStyle("ReForge Description"));
		TextZone.DrawTraitsInfo();

		GUI.Label(new Rect(SW * 0.04f, SH * 0.38f + SW * 0.1f, SW * 0.42f, SH * 0.05f), "",
		          skin.GetStyle("Decoration"));

		if(mng.TargetItem.texture != null)
		{
			GUI.DrawTexture(mng.TargetItem.position, mng.TargetItem.texture);
		}
		GUI.Label(new Rect(SW * 0.04f, SH * 0.43f + SW * 0.1f, SW * 0.1f, SW * 0.1f), "",
		          skin.GetStyle("Item Decoration"));
		if(mng.TargetItem.item != null)
		{
			GUI.Label(new Rect(SW * 0.16f, SH * 0.43f + SW * 0.1f, SW * 0.3f, SW * 0.1f),
			          mng.TargetItem.item.name);
		}

		if(mng.ReWriteReForge)
		{
			GUI.Label(new Rect(SW * 0.04f, SH * 0.72f, SW * 0.42f, SH * 0.15f),
			          "Warning: This item is already Re-forged. Do you want to overwrite previous Re-forge?",
			          skin.GetStyle("Warning Text"));

			if(GUI.Button(new Rect(SW * 0.04f, SH * 0.88f, SW * 0.2f, SH * 0.07f), "Yes"))
			{
				mng.UseReForge();
				mng.ReWriteReForge = false;
			}
			
			if(GUI.Button(new Rect(SW * 0.26f, SH * 0.88f, SW * 0.2f, SH * 0.07f), "No"))
			{
				mng.ReWriteReForge = false;
			}
		}
		else
		{
			if(GUI.Button(new Rect(SW * 0.04f, SH * 0.88f, SW * 0.2f, SH * 0.07f), "Re-Forge"))
			{
				if(mng.UseReForgeOnItem())
				{
					parent.SetWindowState(PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE);
				}
			}
			
			if(GUI.Button(new Rect(SW * 0.26f, SH * 0.88f, SW * 0.2f, SH * 0.07f), "Close"))
			{
				parent.SetWindowState(PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE);
			}
		}
	}
}