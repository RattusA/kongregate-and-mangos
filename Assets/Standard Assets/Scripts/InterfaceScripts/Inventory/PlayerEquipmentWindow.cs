﻿using UnityEngine;
using System.Collections.Generic;

public enum PlayerEquipWindowState
{
	DEFAULT_INFO,
	PLAYER_CAMERA,
	FULL_INFO
}

public class PlayerEquipmentWindow : MonoBehaviour
{
	public GUISkin skin;
	public PlayerEquipmentManager mng;
	public Texture characterPortrait;

	private float SW;
	private float SH;

	private PlayerEquipWindowState _windowState;
	private DescriptionWindow _descriptionWnd;
	private ScrollZone TextZone = new ScrollZone();

	public void Awake()
	{
		SW = Screen.width;
		SH = Screen.height;

		skin.label.fontSize = (int)(Screen.height * 0.05f);
		skin.GetStyle("Scroll Zone").fontSize = (int)(Screen.height * 0.03f);
	}

	public void Start()
	{
		mng = new PlayerEquipmentManager();
		mng.PlayerScript = InterfaceManager.Instance.PlayerScript;
		mng.InitEquipmentSlots();
		mng.GetCharacterStats();

		Rect statsRect = new Rect(SW * 0.106f, SH * 0.781f, SW * 0.3375f, SH * 0.2075f);
		TextZone.InitSkinInfo(skin.GetStyle("Scroll Zone"));
		TextZone.CreateScrollZoneWithText(statsRect, mng.PlayerStats, WorldSession.player.mainTextColor);
		_windowState = PlayerEquipWindowState.DEFAULT_INFO;
	}

	public void DrawGUI()
	{
		GUI.skin = skin;

		GUI.Box(new Rect(0, SH * 0.105f, SW * 0.55f, SH * 0.90f), "", skin.GetStyle("Equipment background"));
		DrawEquipmentSlots();

		switch(_windowState)
		{
		case PlayerEquipWindowState.DEFAULT_INFO:
			DrawDefaultInfoWindow();
			break;
		case PlayerEquipWindowState.PLAYER_CAMERA:
			DrawPlayerWindow();
			break;
		case PlayerEquipWindowState.FULL_INFO:
			DrawFullInfoWindow();
			break;
		}

		if(GUI.Button(new Rect(SW * 0.355f, SH * 0.125f, SW * 0.06f, SW * 0.06f), "", skin.GetStyle("Switch Button")))
		{
			Rect statsRect;
			switch(_windowState)
			{
			case PlayerEquipWindowState.DEFAULT_INFO:
				_windowState = PlayerEquipWindowState.PLAYER_CAMERA;
				break;
			case PlayerEquipWindowState.PLAYER_CAMERA:
				_windowState = PlayerEquipWindowState.FULL_INFO;
				statsRect = new Rect(SW * 0.106f, SH * 0.1175f, SW * 0.334f, SH * 0.875f);
				TextZone.CreateScrollZoneWithText(statsRect, mng.PlayerStats, WorldSession.player.mainTextColor);
				break;
			case PlayerEquipWindowState.FULL_INFO:
				_windowState = PlayerEquipWindowState.DEFAULT_INFO;
				statsRect = new Rect(SW * 0.106f, SH * 0.781f, SW * 0.3375f, SH * 0.2075f);
				TextZone.CreateScrollZoneWithText(statsRect, mng.PlayerStats, WorldSession.player.mainTextColor);
				break;
			}
		}

		if(_descriptionWnd != null)
		{
			GUI.ModalWindow(0, new Rect(0, 0 , SW, SH), _descriptionWnd.DrawGUI, "");
		}
	}

	private void DrawEquipmentSlots()
	{
		mng.UpdateAmmoSlot();
		foreach(KeyValuePair<int, ItemButton> EquipSlot in mng.EquipmentSlotsList)
		{
			if(EquipSlot.Value != null && EquipSlot.Value.item != null)
			{
				GUI.DrawTexture(EquipSlot.Value.position, EquipSlot.Value.texture);
				if(GUI.Button(EquipSlot.Value.position, "", GUIStyle.none))
				{
					_descriptionWnd = new DescriptionWindow();
					_descriptionWnd.IconTecture = EquipSlot.Value.texture;
					_descriptionWnd.TitleText = EquipSlot.Value.item.name;
					_descriptionWnd.DescriptionTextValue = EquipSlot.Value.item.GetItemBasicStatsOutName();
					_descriptionWnd.ButtonManager.FillButtons(EquipSlot.Value.item, InfoWindowState.Inventory_PlayerEquipment);

					foreach(InfoWindowButton button in _descriptionWnd.ButtonManager.ButtonList)
					{
						switch(button.type)
						{
						case InfoWindowButtonType.Close:
							button.onButtonUpWithSender += ClosePopupWindow;
							break;
						case InfoWindowButtonType.EquipUnequip:
							button.item = EquipSlot.Value.item;
							button.onButtonUpWithSender += UnEquipWindow;
							break;
						}
					}
				}
			}
		}
	}

	private void ClosePopupWindow(InfoWindowButton sender)
	{
		_descriptionWnd = null;
	}

	private void UnEquipWindow(InfoWindowButton sender)
	{
		if(sender.item.classType == ItemClass.ITEM_CLASS_PROJECTILE)
		{
			Item.SendEquipAmmo(0);
		}
		else
		{
			mng.PlayerScript.itemManager.MoveToFirstEmptySlot(sender.item);
		}
		_descriptionWnd = null;
	}

	private void DrawDefaultInfoWindow()
	{
		GUI.Box(new Rect(SW * 0.101f, SH * 0.1125f, SW * 0.349f, SH * 0.665f), "",
		        skin.GetStyle("Character Background"));
		GUI.DrawTexture(new Rect(SW * 0.101f, SH * 0.16f, SW * 0.349f, SH * 0.45f), characterPortrait);

		GUI.Label(new Rect(SW * 0.11f, SH * 0.58f, SW * 0.11f, SH * 0.1f), "",
		          skin.GetStyle("Exp Decoration"));

		GUI.Label(new Rect(SW * 0.11f, SH * 0.68f, SW * 0.33f, SH * 0.08f), "",
		          skin.GetStyle("Exp Background"));
		GUI.Label(new Rect(SW * 0.1275f, SH * 0.6875f, SW * 0.295f * mng.PlayerExpCoef, SH * 0.061f), "",
		          skin.GetStyle("Exp Progress Bar"));
		GUI.Label(new Rect(SW * 0.11f, SH * 0.68f, SW * 0.33f, SH * 0.08f), mng.PlayerExp);

		GUI.Box(new Rect(SW * 0.101f, SH * 0.776f, SW * 0.3525f, SH * 0.2225f), "",
		        skin.GetStyle("Stats Background Small"));
		TextZone.DrawTraitsInfo();
	}

	private void DrawPlayerWindow()
	{
		GUI.Box(new Rect(SW * 0.101f, SH * 0.1125f, SW * 0.349f, SH * 0.89f), "", skin.GetStyle("Character Background"));
		GUI.DrawTexture(new Rect(SW * 0.101f, SH * 0.1625f, SW * 0.349f, SH * 0.79f), characterPortrait);
	}

	private void DrawFullInfoWindow()
	{
		GUI.Box(new Rect(SW * 0.101f, SH * 0.1125f, SW * 0.349f, SH * 0.89f), "", skin.GetStyle("Stats Background"));
		TextZone.DrawTraitsInfo();
	}

	public void FixedUpdate()
	{
		if(mng.PlayerScript.itemManager.NeedUpdateEquipment)
		{
			mng.InitEquipmentSlots();
			mng.GetCharacterStats();
			mng.PlayerScript.itemManager.NeedUpdateEquipment = false;
		}
	}

	public void OnDestroy()
	{
		mng.PlayerScript = null;
		mng = null;
	}
}