﻿using UnityEngine;
using System.Collections;

public class InventoryMessageWindow {

	public string inventoryMessage = "";
	private GUISkin newskin;
	private Rect inventoryMessageRect = new Rect(0, Screen.height * 0.4f, Screen.width, Screen.height * 0.2f);
	public float startDraw = 0f;
	public float showTime = 3f;
	public float fontScale = 1f;

	public InventoryMessageWindow()
	{
		if(dbs._sizeMod == 2)
		{
			newskin = Resources.Load<GUISkin> ("GUI/Skins_2x");
		}
		else
		{
			newskin = Resources.Load<GUISkin> ("GUI/Skins");
		}
		newskin.GetStyle("Element 31 (inventoryError)").fontSize = (int)(Screen.height * 0.08f * fontScale);
	}

	public void ShowInventoryMessage()
	{	
		if(startDraw == 0f)
		{
			startDraw = Time.realtimeSinceStartup;
		}

		if(Time.realtimeSinceStartup - startDraw < showTime)
		{
			if(inventoryMessage == "Connected")
			{
				return;
			}
			int oldGUIdepth = GUI.depth;
			GUI.depth = -100;
			GUI.Label(inventoryMessageRect, inventoryMessage, newskin.GetStyle("Element 31 (inventoryError)"));
			GUI.depth = oldGUIdepth;
		}
		else
		{
			startDraw = 0f;
			fontScale = 1f;
			inventoryMessage = "";
		}
	}
}
