﻿using UnityEngine;
using System.Collections.Generic;

public class ItemButton
{
	public Item item;
	public Texture texture;
	public Rect position;
}

public class InventoryManager
{
	public Dictionary<int, ItemButton> inventoryBags = new Dictionary<int, ItemButton>();
	public Dictionary<int, ItemButton> bagSlots = new Dictionary<int, ItemButton>();
	public List<ulong> blockedItems = new List<ulong>();
	public int bagSlotCount = 16;

	private byte _currentBag = 255;

	public void InitInventory()
	{
		float SW = Screen.width;
		float SH = Screen.height;

		int firstBagID = (int)InventorySlots.INVENTORY_SLOT_BAG_START;
		int lastBagID = (int)InventorySlots.INVENTORY_SLOT_BAG_END;

		inventoryBags.Clear();
		ItemButton bagButton = new ItemButton();
		bagButton.item = new Item();
		bagButton.item.bag = 255;
		bagButton.item.slot = 255;
		bagButton.position = new Rect(SW * 0.8f - SH * 0.32f, SH * 0.13f, SH * 0.105f, SH * 0.10f);
		inventoryBags[0] = bagButton;

		for(int bagSlotIndex = firstBagID; bagSlotIndex < lastBagID; bagSlotIndex++)
		{
			bagButton = new ItemButton();
			int index = bagSlotIndex - (int)InventorySlots.INVENTORY_SLOT_BAG_START + 1;
			int indexValue = (int)UpdateFields.EUnitFields.PLAYER_FIELD_INV_SLOT_HEAD + bagSlotIndex * 2;
			ulong bagGUID = InterfaceManager.Instance.PlayerScript.GetUInt64Value(indexValue);
			if(bagGUID > 0)
			{
				Item bagItem = InterfaceManager.Instance.PlayerScript.itemManager.GetItemByGuid(bagGUID);
				if(bagItem != null)
				{
					bagButton.item = bagItem;

					string iconName = DefaultIcons.GetItemIcon(bagItem);
					iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
					bagButton.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
				}
			}
			else
			{
				bagButton.item = new Item();
				bagButton.item.bag = 255;
				bagButton.item.slot = (byte)bagSlotIndex;
			}

			float xPos = SW * 0.8f - SH * 0.32f + (SH * 0.11f * index);
			bagButton.position = new Rect(xPos, SH * 0.13f, SH * 0.105f, SH * 0.10f);

			inventoryBags[index] = bagButton;
		}
		ShowBagSlots(_currentBag);
	}

	public void ShowBagSlots(int bagIndex)
	{
		float SW = Screen.width;
		float SH = Screen.height;

		if(bagIndex == 0 || bagIndex == 255)
		{
			bagSlots.Clear();
			bagSlotCount = 16;
			_currentBag = 255;
			for(int slotIndex = 0; slotIndex < bagSlotCount; slotIndex++)
			{
				int indexValue = (int)UpdateFields.EUnitFields.PLAYER_FIELD_PACK_SLOT_1 + slotIndex * 2;
				ulong itemGUID = InterfaceManager.Instance.PlayerScript.GetUInt64Value(indexValue);
				ItemButton itemButton = new ItemButton();
				if(itemGUID > 0)
				{
					Item item = InterfaceManager.Instance.PlayerScript.itemManager.GetItemByGuid(itemGUID);
					if(item != null)
					{
						itemButton.item = item;
						
						string iconName = DefaultIcons.GetItemIcon(item);
						iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
						itemButton.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
					}
					else
					{
						itemButton.item = new Item();
						itemButton.item.bag = _currentBag;
						itemButton.item.slot = (byte)(slotIndex + 23);
					}
				}
				else
				{
					itemButton.item = new Item();
					itemButton.item.bag = _currentBag;
					itemButton.item.slot = (byte)(slotIndex + 23);
				}

				float xPos = (SW * 0.75f - SH * 0.2075f) + (slotIndex % 4) * SH * 0.11575f;
				float yPos = SH * 0.2925f + (slotIndex / 4) * SH * 0.11375f;
				
				itemButton.position = new Rect(xPos, yPos, SH * 0.11575f, SH * 0.11375f);
				itemButton.item.slotType = SlotType.SLOTTYPE_MAIN_PLAYER_INVENTORY;

				bagSlots[slotIndex] = itemButton;
			}
		}
		else
		{
			Bag selectedBag;
			InterfaceManager.Instance.PlayerScript.itemManager.BaseBags.TryGetValue(bagIndex, out selectedBag);
			if(selectedBag == null)
			{
				return;
			}

			_currentBag = (byte)bagIndex;
			bagSlots.Clear();
			bagSlotCount = selectedBag.GetCapacity();
			for(int slotIndex = 0; slotIndex < bagSlotCount; slotIndex++)
			{
				Item item = selectedBag.GetItemFromSlot(slotIndex);
				ItemButton itemButton = new ItemButton();

				if(item != null)
				{
					itemButton.item = item;
					
					string iconName = DefaultIcons.GetItemIcon(item);
					iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
					itemButton.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
				}
				else
				{
					itemButton.item = new Item();
					itemButton.item.bag = _currentBag;
					itemButton.item.slot = (byte)slotIndex;
				}

				float xPos = (SW * 0.75f - SH * 0.2075f) + (slotIndex % 4) * SH * 0.11575f;
				float yPos = SH * 0.2925f + (slotIndex / 4) * SH * 0.11375f;
				
				itemButton.position = new Rect(xPos, yPos, SH * 0.11575f, SH * 0.11375f);
				itemButton.item.slotType = SlotType.SLOTTYPE_PLAYER_INVENTORY;

				bagSlots[slotIndex] = itemButton;
			}
		}
	}

	public ItemButton IsTouchInSlotsContainer(Vector2 touch)
	{
		foreach(KeyValuePair<int, ItemButton> itemButton in bagSlots)
		{
			if(itemButton.Value != null && itemButton.Value.position.Contains(touch))
			{
				return itemButton.Value;
			}
		}
		return null;
	}

	public ItemButton IsTouchInBagContainer(Vector2 touch)
	{
		foreach(KeyValuePair<int, ItemButton> itemButton in inventoryBags)
		{
			if(itemButton.Value != null && itemButton.Value.position.Contains(touch))
			{
				return itemButton.Value;
			}
		}
		return null;
	}
}