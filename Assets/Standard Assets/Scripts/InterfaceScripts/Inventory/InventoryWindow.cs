using UnityEngine;
using System;

public class InventoryItemSlot
{
	public Item item;
	public Texture texture;
	public Rect position;
	public SpellEntry spell;
}

public class InventoryWindow : InventoryWindowBase
{
	public PlayerInventory parent;	

	public void DrawGUI()
	{
		base.DrawGUI();

		if(InterfaceManager.Instance.PlayerScript.playerClass == Classes.CLASS_HUNTER
		   && InterfaceManager.Instance.PlayerScript.pet != null
		   && InterfaceManager.Instance.PlayerScript.playerSpellManager.GetSpellByID(6991) != null)
		{
			if(GUI.Button(new Rect(Screen.width * 0.775f - Screen.height * 0.15f, Screen.height * 0.775f,
			                       Screen.height * 0.3f, Screen.height * 0.14f),
			              "", skin.GetStyle("Feed Button")))
			{
				parent.SetWindowState(PlayerInventoryWindowState.PET_FEED_STATE);
				parent.PetFeedWnd.InitPetFeedWindow();
			}
		}
	}

	public override void ItemEvent(ItemButton itemButton)
	{
		descriptionWnd = new DescriptionWindow();
		descriptionWnd.IconTecture = itemButton.texture;
		descriptionWnd.TitleText = itemButton.item.name;
		descriptionWnd.DescriptionTextValue = itemButton.item.GetItemBasicStatsOutName();
		descriptionWnd.ButtonManager.FillButtons(itemButton.item, InfoWindowState.Inventory_PlayerInventory);

		foreach(InfoWindowButton button in descriptionWnd.ButtonManager.ButtonList)
		{
			switch(button.type)
			{
			case InfoWindowButtonType.Close:
				button.onButtonUpWithSender += ClosePopupWindow;
				break;
			case InfoWindowButtonType.Delete:
				button.item = itemButton.item;
				button.onButtonUpWithSender += DestroyItemWindow;
				break;
			case InfoWindowButtonType.EquipUnequip:
				button.onButtonUpWithSender += ClosePopupWindow;
				break;
			case InfoWindowButtonType.Use:
				button.onButtonUpWithSender += ClosePopupWindow;
				break;
			case InfoWindowButtonType.Open:
				button.onButtonUpWithSender += ClosePopupWindow;
				break;
			case InfoWindowButtonType.Gem:
				button.item = itemButton.item;
				button.onButtonUpWithSender += EquipGemWindow;
				break;
			case InfoWindowButtonType.ReForge:
				button.item = itemButton.item;
				button.onButtonUpWithSender += OpenReForgeWindow;
				break;
			}
		}

		modalInfoWindowState = ModalInfoWindow.Description;
	}

	public void ClosePopupWindow(InfoWindowButton sender)
	{
		descriptionWnd = null;
		modalInfoWindowState = ModalInfoWindow.None;
	}

	public void EquipGemWindow(InfoWindowButton sender)
	{
		ClosePopupWindow(sender);
		parent.SetWindowState(PlayerInventoryWindowState.GEM_EQUIP_STATE);
		parent.GemEquipWnd.InitGemWindow(sender.item);
	}

	public void OpenReForgeWindow(InfoWindowButton sender)
	{
		ClosePopupWindow(sender);
		parent.SetWindowState(PlayerInventoryWindowState.RE_FORGE_STATE);
		parent.ReForgeWnd.InitReForgeWindow(sender.item);
	}

	public void OnDestroy()
	{
		base.OnDestroy();
		parent = null;
	}
}