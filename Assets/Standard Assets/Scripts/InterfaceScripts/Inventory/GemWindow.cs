using UnityEngine;
using System.Collections;

public class GemWindow : MonoBehaviour
{
	public GUISkin skin;
	public GemManager mng;
	public PlayerInventory parent;

	private float SW;
	private float SH;

	public void Awake()
	{
		SW = Screen.width;
		SH = Screen.height;

		skin.button.fontSize = (int)(Screen.height * 0.04f);
		skin.label.fontSize = (int)(Screen.height * 0.04f);

		skin.GetStyle("Yellow Text").fontSize = (int)(Screen.height * 0.04f);
		skin.GetStyle("Blue Text").fontSize = (int)(Screen.height * 0.04f);
		skin.GetStyle("Red Text").fontSize = (int)(Screen.height * 0.04f);
	}

	public void InitGemWindow(Item item)
	{
		mng = new GemManager();
		mng.GetGemSlotInfo(item);
	}

	public void OnDestroy()
	{
		parent.InventoryWnd.mng.blockedItems.Clear();

		mng = null;
		parent = null;
	}

	public void DrawGUI()
	{
		GUI.skin = skin;

		GUI.Box(new Rect(SW * 0.02f, SH * 0.12f, SW * 0.46f, SH * 0.86f), "");

		GUI.DrawTexture(new Rect(SW * 0.055f, SH * 0.1575f, SH * 0.105f, SH * 0.105f), mng.ItemTexture);
		GUI.Label(new Rect(SW * 0.05f, SH * 0.15f, SH * 0.12f, SH * 0.12f), "", skin.GetStyle("GemSlot Icon"));
		GUI.Label(new Rect(SW * 0.16f, SH * 0.15f, SW * 0.3f, SW * 0.1f), mng.ItemName);

		GUI.Label(new Rect(SW * 0.04f, SH * 0.29f, SW * 0.42f, SH * 0.05f), "", skin.GetStyle("Decoration"));
		GUI.Label(new Rect(SW * 0.04f, SH * 0.33f, SW * 0.42f, SH * 0.08f), "Socet Bonus: " + mng.GemBonus);

		// SOCKETS
		foreach(GemSlot gemSlot in mng.GemList)
		{
			if(gemSlot != null)
			{
				DrawGemSlotsInfo(gemSlot);
			}
		}

		// BUTTONS
		if(GUI.Button(new Rect(SW * 0.04f, SH * 0.88f, SW * 0.2f, SH * 0.07f), "Insert"))
		{
			mng.InsertGems();
			parent.SetWindowState(PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE);
		}

		if(GUI.Button(new Rect(SW * 0.26f, SH * 0.88f, SW * 0.2f, SH * 0.07f), "Close"))
		{
			parent.SetWindowState(PlayerInventoryWindowState.PLAYER_EQUIPMENT_STATE);
		}
	}

	private void DrawGemSlotsInfo(GemSlot gemSlot)
	{
		if(gemSlot == null)
		{
			return;
		}

		Rect slotIconRect = new Rect(gemSlot.Position.x, gemSlot.Position.y,
		                             gemSlot.Position.width - SH * 0.005f, gemSlot.Position.height - SH * 0.005f);
		Rect slotColorLabelRect = new Rect(gemSlot.Position.x + SW * 0.11f, gemSlot.Position.y,
		                                   SW * 0.3f, SW * 0.04f);
		Rect slorDescriprionLabelRect = new Rect(gemSlot.Position.x + SW * 0.11f, gemSlot.Position.y + SW * 0.04f,
		                                         SW * 0.3f, SW * 0.04f);
		GUIStyle slotColorStyle;
		string socketColorText;

		if(gemSlot.GemIcon != null)
		{
			GUI.DrawTexture(slotIconRect, gemSlot.GemIcon);
		}
		GUI.Label(gemSlot.Position, "", skin.GetStyle("GemSlot Icon"));


		switch(gemSlot.SocketColorType)
		{
		case SocketColor.SOCKET_COLOR_BLUE:
			slotColorStyle = skin.GetStyle("Red Text");
			socketColorText = "Red Socket";
			break;
		case SocketColor.SOCKET_COLOR_YELLOW:
			slotColorStyle = skin.GetStyle("Blue Text");
			socketColorText = "Blue Socket";
			break;
		case SocketColor.SOCKET_COLOR_RED:
			slotColorStyle = skin.GetStyle("Yellow Text");
			socketColorText = "Yellow Socket";
			break;
		default:
			slotColorStyle = skin.label;
			socketColorText = "Super Socket";
			break;
		}

		GUI.Label(slotColorLabelRect, socketColorText, slotColorStyle);
		GUI.Label(slorDescriprionLabelRect, gemSlot.GemDescription);
	}
}