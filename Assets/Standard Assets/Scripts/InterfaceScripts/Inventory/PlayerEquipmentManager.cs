﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerEquipmentManager {

	public Player PlayerScript;
	public string PlayerStats;

	public string PlayerExp = "0 / 0";
	public float PlayerExpCoef = 0f;

	public Dictionary<int, ItemButton> EquipmentSlotsList = new Dictionary<int, ItemButton>();

	public void InitEquipmentSlots()
	{
		string iconName;
		ItemButton itemButton;

		EquipmentSlotsList.Clear();

		foreach(KeyValuePair<int, Item> EquipSlot in PlayerScript.itemManager.EquippedSlots)
		{
			if(EquipSlot.Value != null)
			{
				itemButton = new ItemButton();
				itemButton.item = EquipSlot.Value;
				
				iconName = DefaultIcons.GetItemIcon(EquipSlot.Value);
				iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
				itemButton.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));

				float xPos = Screen.width * 0.0395f + (EquipSlot.Key / 10) * Screen.width * 0.4125f;
				float yPos = Screen.height * 0.11325f +
					((EquipSlot.Key % 10) + (EquipSlot.Key / 10)) * Screen.height * 0.08825f;

				itemButton.position = new Rect(xPos, yPos, Screen.width * 0.0605f, Screen.height * 0.087f);
				
				EquipmentSlotsList[EquipSlot.Key] = itemButton;
			}
		}

		uint ammoEntry = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_AMMO_ID);
		Item ammoItem = PlayerScript.itemManager.GetItemByID(ammoEntry);

		if(ammoItem != null)
		{
			Item prototypeItem = CreateAmmoPrototype(ammoItem);

			itemButton = new ItemButton();
			itemButton.item = prototypeItem;

			iconName = DefaultIcons.GetItemIcon(prototypeItem);
			iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
			itemButton.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));

			itemButton.position = new Rect(Screen.width * 0.452f, Screen.height * 0.11325f,
			                               Screen.width * 0.0605f, Screen.height * 0.087f);

			EquipmentSlotsList[(int)EquipmentSlots.EQUIPMENT_SLOT_END] = itemButton;
		}
	}

	public void GetCharacterStats()
	{
		PlayerStats = string.Empty;
		uint firstUIntValue;
		uint secondUIntValue;
		int offset;

		float firstFloatValue;
		float secondFloatValue;

		uint currentXp = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_XP);
		uint maxXp = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_NEXT_LEVEL_XP);

		PlayerExp = currentXp + " / " + maxXp;
		PlayerExpCoef = (maxXp == 0) ? 0 : (currentXp * 1.0f) / (maxXp * 1.0f);

		// Healths
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_HEALTH);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXHEALTH);
		PlayerStats += "<b>HP</b>: " + firstUIntValue + "/" + secondUIntValue + "\n";

		// Mana
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POWER1);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXPOWER1);
		PlayerStats += "<b>MP</b>: " + firstUIntValue + "/" + secondUIntValue + "\n";

		GetStatsInfo();
		GetResistanceInfo();


		// ----- MELEE -----
		// Melee Damage
		firstFloatValue = PlayerScript.GetFloatValue((int)UpdateFields.EUnitFields.UNIT_FIELD_MINDAMAGE);
		secondFloatValue = PlayerScript.GetFloatValue((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXDAMAGE);
		PlayerStats += "<b>Damage</b>: " + Mathf.Floor(firstFloatValue) + " - " + Mathf.Ceil(secondFloatValue) + "\n";

		// Haste
		offset = (int)WeaponAttackType.BASE_ATTACK;
		firstFloatValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_BASEATTACKTIME + offset) / 1000.0f;
		PlayerStats += "<b>Haste</b>: " + firstFloatValue.ToString("0.00") + " attacks/sec\n";

		// Melee Strength
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_ATTACK_POWER);
		PlayerStats += "<b>Melee Strength</b>: " + firstUIntValue + "\n";

		// Melee Crit
		offset = (int)CombatRating.CR_HIT_MELEE;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		PlayerStats += "<b>Melee Strike Score</b>: " + firstUIntValue + "\n";

		// Melee Crit Percent
		firstFloatValue = PlayerScript.GetFloatValue((int)UpdateFields.EUnitFields.PLAYER_CRIT_PERCENTAGE);
		PlayerStats += "<b>Melee Chance to Crit</b>: " + firstFloatValue.ToString("0.00") + "%\n";

		// Mastery
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_EXPERTISE);
		PlayerStats += "<b>Mastery</b>: " + firstUIntValue + "\n";
	

		// ----- RANGED -----
		// Ranged Damage
		firstFloatValue = PlayerScript.GetFloatValue((int)UpdateFields.EUnitFields.UNIT_FIELD_MINRANGEDDAMAGE);
		secondFloatValue = PlayerScript.GetFloatValue((int)UpdateFields.EUnitFields.UNIT_FIELD_MAXRANGEDDAMAGE);
		PlayerStats += "<b>Ranged Damage</b>: " + Mathf.Floor(firstFloatValue) + " - " + Mathf.Ceil(secondFloatValue) + "\n";

		// Ranged Strength
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RANGED_ATTACK_POWER);
		PlayerStats += "<b>Ranged Strength</b>: " + firstUIntValue + "\n";

		// Ranged Strike Score
		offset = (int)CombatRating.CR_HIT_RANGED;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		PlayerStats += "<b>Ranged Strike Score</b>: " + firstUIntValue + "\n";

		// Ranged Chance to Crit
		firstFloatValue = PlayerScript.GetFloatValue((int)UpdateFields.EUnitFields.PLAYER_RANGED_CRIT_PERCENTAGE);
		PlayerStats += "<b>Ranged Chance to Crit</b>: " + firstFloatValue .ToString("0.00") + "%\n";


		// ----- SPELL -----
		// Extra Damage
		offset = (int)SpellSchools.SPELL_SCHOOL_NORMAL;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_POS + offset);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_DAMAGE_DONE_NEG + offset);
		PlayerStats += "<b>Extra Damage</b>: " + (firstUIntValue + secondUIntValue) + "\n";

		// Extra Alleviate
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_MOD_HEALING_DONE_POS);
		PlayerStats += "<b>Extra Damage</b>: " + firstUIntValue + "\n";

		// Spell Strike Score
		offset = (int)CombatRating.CR_HIT_SPELL;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		PlayerStats += "<b>Spell Strike Score</b>: " + firstUIntValue + "\n";

		// Spell Chance to Crit
		offset = (int)SpellSchools.SPELL_SCHOOL_NORMAL;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_SPELL_CRIT_PERCENTAGE1 + offset);
		PlayerStats += "<b>Spell Chance to Crit</b>: " + firstUIntValue.ToString("0.00") + "%\n";

		// Spell Rush Score
		offset = (int)CombatRating.CR_HASTE_SPELL;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		PlayerStats += "<b>Spell Rush Score</b>: " + firstUIntValue + "\n";


		// ----- DEFENCE -----
		// Aegis
		offset = (int)CombatRating.CR_DEFENSE_SKILL;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		PlayerStats += "<b>Aegis</b>: " + firstUIntValue + "\n";

		// Chance to Dodge
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_DODGE_PERCENTAGE);
		PlayerStats += "<b>Chance to Dodge</b>: " + firstUIntValue.ToString("0.00") + "%\n";

		// Chance to Deflect
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_PARRY_PERCENTAGE);
		PlayerStats += "<b>Chance to Deflect</b>: " + firstUIntValue.ToString("0.00") + "%\n";

		// Chance to Block
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_BLOCK_PERCENTAGE);
		PlayerStats += "<b>Chance to Block</b>: " + firstUIntValue.ToString("0.00") + "%\n";

		// Flexibility
		offset = (int)CombatRating.CR_CRIT_TAKEN_MELEE;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		offset = (int)CombatRating.CR_CRIT_TAKEN_RANGED;
		firstUIntValue += PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		offset = (int)CombatRating.CR_CRIT_TAKEN_SPELL;
		firstUIntValue += PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COMBAT_RATING_1 + offset);
		PlayerStats += "<b>Flexibility</b>: " + Mathf.Ceil(firstUIntValue / 3.0f) + "\n";

		// ----- OTHER -----
		// Gold
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_COINAGE);
		PlayerStats += "<b>Gold</b>: " + firstUIntValue + "\n";

		// Mithril Coins
		PlayerStats += "<b>Mithril Coins</b>: " + PlayerScript.mithrilCoins + "\n";


		for(int valueIndex = 0; valueIndex < 32; valueIndex++)
		{
			offset = valueIndex * 2;
			ulong itemGuid = PlayerScript.GetUInt64Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_CURRENCYTOKEN_SLOT_1 + offset);
			if(itemGuid != 0)
			{
				Item item = PlayerScript.itemManager.GetItemByGuid(itemGuid);
				if(item != null && item.entry == 43016)
				{
					firstUIntValue = item.GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT);
					PlayerStats += "<b>Amber</b>: "+ firstUIntValue + "\n";
					continue;
				}
				
				if(item != null && item.entry == 29434)
				{
					firstUIntValue = item.GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT);
					PlayerStats += "<b>Mark of Dokk</b>: "+ firstUIntValue + "\n";
					continue;
				}
			}
		}

		// Prestige
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_HONOR_CURRENCY);
		PlayerStats += "<b>Prestige</b>: " + firstUIntValue + "\n";

		// PVP Kills
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_FIELD_LIFETIME_HONORBALE_KILLS);
		PlayerStats += "<b>PVP Kills</b>: " + firstUIntValue + "\n";

		// Gear Rating
		PlayerStats += "<b>Gear Rating</b>: " + PlayerScript.gearRating + "\n";


		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_DEFENSE);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_UNARMED);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_DAGGERS);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_STAVES);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_SWORDS);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_2H_SWORDS);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_AXES);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_2H_AXES);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_MACES);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_2H_MACES);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_POLEARMS);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_WANDS);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_THROWN);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_BOWS);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_CROSSBOWS);
		PlayerStats += PlayerSkills.GetSkillInfo(SkillType.SKILL_GUNS);

//		theStats += "\n" + Fractions.GetReputationInfo();
//		
//		return theStats;
	}

	private void GetStatsInfo()
	{
		uint firstUIntValue;
		uint secondUIntValue;
		uint thirdUIntValue;

		// Might
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT0);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POSSTAT0);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_NEGSTAT0);
		PlayerStats += "<b>Might</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
		
		// Dexterity
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT1);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POSSTAT1);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_NEGSTAT1);
		PlayerStats += "<b>Dexterity</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
		
		// Vitality
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT2);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POSSTAT2);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_NEGSTAT2);
		PlayerStats += "<b>Vitality</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
		
		// Wisdom
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT3);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POSSTAT3);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_NEGSTAT3);
		PlayerStats += "<b>Wisdom</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
		
		// Will
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_STAT4);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_POSSTAT4);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_NEGSTAT4);
		PlayerStats += "<b>Will</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
	}

	private void GetResistanceInfo()
	{
		uint firstUIntValue;
		uint secondUIntValue;
		uint thirdUIntValue;
		int offset;

		// Armour
		offset = (int)SpellSchools.SPELL_SCHOOL_NORMAL;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		PlayerStats += "<b>Armour</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
		
		// White Magic Deterrance
		offset = (int)SpellSchools.SPELL_SCHOOL_HOLY;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		PlayerStats += "<b>White Magic Deterrance</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
		
		// Black Magic Deterrance
		offset = (int)SpellSchools.SPELL_SCHOOL_SHADOW;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		PlayerStats += "<b>Black Magic Deterrance</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
		
		// Spirit Magic Deterrance
		offset = (int)SpellSchools.SPELL_SCHOOL_ARCANE;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		PlayerStats += "<b>Spirit Magic Deterrance</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
		
		// Inferno Magic Deterrance
		offset = (int)SpellSchools.SPELL_SCHOOL_FIRE;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		PlayerStats += "<b>Inferno Magic Deterrance</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
		
		// Ice Magic Deterrance
		offset = (int)SpellSchools.SPELL_SCHOOL_FROST;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		PlayerStats += "<b>Ice Magic Deterrance</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
		
		// Earth Magic Deterrance
		offset = (int)SpellSchools.SPELL_SCHOOL_NATURE;
		firstUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCES + offset);
		secondUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSPOSITIVE + offset);
		thirdUIntValue = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.UNIT_FIELD_RESISTANCEBUFFMODSNEGATIVE + offset);
		PlayerStats += "<b>Earth Magic Deterrance</b>: " + firstUIntValue + "(" + (secondUIntValue + thirdUIntValue) + ")\n";
	}

	public bool IsTouchInEquipmentContainer(Vector2 touch)
	{
		Rect equipmentContainer = new Rect(Screen.width * 0.101f, Screen.height * 0.1625f,
		                                   Screen.width * 0.349f, Screen.height * 0.79f);
		if(equipmentContainer.Contains(touch))
		{
			return true;
		}
		return false;
	}

	public ItemButton IsTouchInEquipmentSlotsContainer(Vector2 touch)
	{
		foreach(KeyValuePair<int, ItemButton> itemButton in EquipmentSlotsList)
		{
			if(itemButton.Value != null && itemButton.Value.position.Contains(touch))
			{
				return itemButton.Value;
			}
		}
		return null;
	}

	public void UpdateAmmoSlot()
	{
		ItemButton ammoSlot;
		EquipmentSlotsList.TryGetValue((int)EquipmentSlots.EQUIPMENT_SLOT_END, out ammoSlot);
		uint ammoEntry = PlayerScript.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_AMMO_ID);
		if((ammoSlot == null && ammoEntry > 0)
		   || (ammoSlot != null && ammoSlot.item.entry != ammoEntry))
		{
			Item ammoItem = AppCache.sItems.GetItem(ammoEntry);
			if(ammoItem != null)
			{
				if(ammoSlot == null)
				{
					ammoSlot = new ItemButton();
				}
				Item prototypeItem = CreateAmmoPrototype(ammoItem);

				ammoSlot.item = prototypeItem;
				
				string iconName = DefaultIcons.GetItemIcon(prototypeItem);
				iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
				ammoSlot.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));
				
				ammoSlot.position = new Rect(Screen.width * 0.452f, Screen.height * 0.11325f,
				                               Screen.width * 0.0605f, Screen.height * 0.087f);
			}
		}
		else if(ammoSlot != null && ammoEntry <= 0)
		{
			ammoSlot = null;
		}
		else if(ammoSlot != null && ammoEntry > 0)
		{
			UpdateCount(ammoSlot.item);
		}
		EquipmentSlotsList[(int)EquipmentSlots.EQUIPMENT_SLOT_END] = ammoSlot;
	}

	private void UpdateCount(Item itemProt)
	{
		uint counter = 0;
		foreach(Item item in PlayerScript.itemManager.Items)
		{
			if (item.entry == itemProt.entry)
			{
				counter += item.GetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT);
			}
		}
		if(itemProt.count != counter)
		{
			itemProt.count = counter;
			itemProt.SetUInt32Value((int)UpdateFields.EItemFields.ITEM_FIELD_STACK_COUNT, counter);
			itemProt.stats = itemProt.GetItemBasicStats();
		}
	}

	private Item CreateAmmoPrototype(Item ammoItem)
	{
		Item prototypeItem = new Item();
		prototypeItem.entry = ammoItem.entry;
		prototypeItem.classType = ammoItem.classType;
		prototypeItem.subClass = ammoItem.subClass;
		prototypeItem.name = ammoItem.name;
		prototypeItem.quality = ammoItem.quality;
		prototypeItem.description = ammoItem.description;
		prototypeItem.displayInfoID = ammoItem.displayInfoID;
		prototypeItem.socket = ammoItem.socket;
		prototypeItem.stackable = ammoItem.stackable;
		prototypeItem.reqLvl = ammoItem.reqLvl;
		UpdateCount(prototypeItem);
		return prototypeItem;
	}
}
