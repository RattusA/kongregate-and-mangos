﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PetFeedManager {

	public Player PlayerScript;
	public Texture PetIcon;
	public string PetName;
	public string PetInfo;

	public ItemButton FoodItem;

	public void GetPetInfo()
	{
		PetName = PlayerScript.pet.name;
		string petID = (PlayerScript.petFamily).ToString();
		string perFamily = DefaultTabel.petFamilyName[petID].ToString();

		PetInfo = "Pet Name : " + PetName + "\n";
		PetInfo += "Family : " + perFamily + "\n";
		PetInfo += "Level : " + PlayerScript.petLvl + "\n";
		PetInfo += "Happines : " + PlayerScript.petHappines + "\n";
		PetInfo += "Food : ";

		string food = "";
		IDictionary dictionary = (IDictionary)DefaultTabel.petFoodName[perFamily];
		foreach(string foodNumber in dictionary.Keys)
		{			
			food = dictionary[foodNumber].ToString();			
			PetInfo += food;
		}

		FoodItem = new ItemButton();
		FoodItem.position = new Rect(Screen.width * 0.185f, Screen.height * 0.6675f,
		                             Screen.width * 0.13f, Screen.width * 0.13f);
	}

	public ItemButton IsTouchInFoodSlot(Vector2 touch)
	{
		if(FoodItem.position.Contains(touch))
		{
			return FoodItem;
		}
		return null;
	}

	public bool InsertItemToSlot(Item item)
	{
		bool ret = false;
		//Check the condition of items that can be enchanted.
		if((item.classType == ItemClass.ITEM_CLASS_TRADE_GOODS &&
		    item.subClass == (int)ItemSubclassTradeGoods.ITEM_SUBCLASS_MEAT) ||
		   (item.classType == ItemClass.ITEM_CLASS_CONSUMABLE &&
		 	item.subClass == (int)ItemSubclassConsumable.ITEM_SUBCLASS_FOOD))
		{
			string iconName = DefaultIcons.GetItemIcon(item.entry);
			iconName = iconName.Substring(0, iconName.LastIndexOf('.'));
			FoodItem.texture = Resources.Load<Texture> (string.Format ("Icons/{0}", iconName));

			ret = true;
		}
		return ret;
	}

	public void FeedPet()
	{
		if(FoodItem.item == null)
		{
			Debug.LogWarning("TODO!!!");
			Debug.LogWarning("Food item not selected!");
			return;
		}

		byte aux8 = 0;
		uint spellID = 6991;									// Feed Spell
		SpellCastTargets targets = new SpellCastTargets();

		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_CAST_SPELL);
		pkt.Append(aux8);
		pkt.Add(spellID);
		pkt.Append(aux8);
		pkt.Append(targets.WriteItem(FoodItem.item.guid));
		RealmSocket.outQueue.Add(pkt);
	}

	public void UpdateFoodSlot(List<ulong> BlockedItems, ulong guid)
	{
		if(FoodItem.item != null && FoodItem.item.guid == guid && !BlockedItems.Contains(FoodItem.item.guid))
		{
			FoodItem.item = null;
			FoodItem.texture = null;
		}
	}
}
