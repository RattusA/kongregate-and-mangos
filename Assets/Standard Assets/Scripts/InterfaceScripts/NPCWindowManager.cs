﻿using UnityEngine;
using System.Collections;

public enum NpcWindowState {

	NONE,
	GOSSIP_WINDOIW,
	TRAINER_WINDOW,
	BANKER_WINDOW
}

public class NPCWindowManager : MonoBehaviour
{

	public InterfaceManager Parent;

	public GossipWindow gossipWnd;
	public TrainerWindow trainerWnd;
	public BankerWindow bankerWnd;

	private NpcWindowState windowState;

	public void DrawGUI()
	{
		switch(windowState)
		{
		case NpcWindowState.GOSSIP_WINDOIW:
			if(gossipWnd != null)
			{
				gossipWnd.DrawGUI();
			}
			break;
		case NpcWindowState.TRAINER_WINDOW:
			if(trainerWnd != null)
			{
				trainerWnd.DrawGUI();
			}
			break;
		case NpcWindowState.BANKER_WINDOW:
			if(bankerWnd != null)
			{
				bankerWnd.DrawGUI();
			}
			break;
		}
	}

	public void InitGossipWindow(GossipManager gossipManager)
	{
		GameObject gameObject = (GameObject)Instantiate(new GameObject("Gossip"));
		gameObject.transform.parent = this.transform;
		
		gossipWnd = gameObject.AddComponent<GossipWindow>();
		gossipWnd.parent = this;
		gossipWnd.mng = gossipManager;

		windowState = NpcWindowState.GOSSIP_WINDOIW;
	}

	public void InitTrainerWindow(ulong guid, WorldPacket pkt)
	{
		GameObject gameObject = (GameObject)Instantiate(new GameObject("Trainer"));
		gameObject.transform.parent = this.transform;

		trainerWnd = gameObject.AddComponent<TrainerWindow>();
		trainerWnd.parent = this;
		trainerWnd.mng.npcGuid = guid;
		trainerWnd.mng.InitTrainerSpellList(pkt);
	
		windowState = NpcWindowState.TRAINER_WINDOW;
	}


	public void SetWindowState(NpcWindowState newState)
	{
		switch(newState)
		{
		case NpcWindowState.GOSSIP_WINDOIW:
			break;
		case NpcWindowState.TRAINER_WINDOW:
			break;
		case NpcWindowState.BANKER_WINDOW:
			GameObject bankerGO = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/Banker"));
			if(bankerGO != null)
			{
				bankerWnd = bankerGO.GetComponent<BankerWindow>();
				if(bankerWnd == null)
				{
					Debug.LogError("BankerWindow script missing.");
				}
				else
				{
					bankerWnd.transform.parent = this.transform;
					bankerWnd.Parent = this;
				}
			}
			else
			{
				Debug.LogError("Can't load Banker Window prefab.");
			}
			break;
		}
		windowState = newState;
	}

	public void CloseNPCDialog()
	{
		SetWindowState(NpcWindowState.NONE);
		Parent.SetWindowState(InterfaceWindowState.ACTION_BAR_PANEL_STATE);
		InterfaceManager.Instance.PlayerScript.SendMessage("UpdateStateManager");
	}
}
