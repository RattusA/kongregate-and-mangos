using UnityEngine;
using System.Collections;

public enum PlayerMenuWindowState {

	PLAYER_MENU_WINDOW_STATE,
	INVENTORY_WINDOW_STATE,
	QUEST_WINDOW_STATE,
	SPELL_WINDOW_STATE,
	EXTRA_WINDOW_STATE,
	SOCIAL_WINDOW_STATE,
	OPTION_WINDOW_STATE
}

public class PlayerMenuManager : MonoBehaviour
{
	public PlayerInventory InventoryWnd;
	public SpellBookManager SpellBookMng;
	public ExtraMenuWindow extraMenuWindow;
	public OptionsWindow optionsWindow;
	public InterfaceManager parentMng;

	protected PlayerMenuWindowState WindowState = PlayerMenuWindowState.PLAYER_MENU_WINDOW_STATE;

	public void SetMainPlayer(Player playerScript)
	{
		SpellBookMng.SetMainPlayer(playerScript);
	}

	public void OnEnable()
	{
		WindowState = PlayerMenuWindowState.PLAYER_MENU_WINDOW_STATE;
	}

	public void OnDisable()
	{
		switch(WindowState)
		{
		case PlayerMenuWindowState.PLAYER_MENU_WINDOW_STATE:
			break;
		case PlayerMenuWindowState.INVENTORY_WINDOW_STATE:
			InventoryWnd.gameObject.SetActive(false);
			break;
		case PlayerMenuWindowState.QUEST_WINDOW_STATE:
			break;
		case PlayerMenuWindowState.SPELL_WINDOW_STATE:
			SpellBookMng.gameObject.SetActive(false);
			break;
		case PlayerMenuWindowState.EXTRA_WINDOW_STATE:
			extraMenuWindow.gameObject.SetActive(false);
			break;
		case PlayerMenuWindowState.SOCIAL_WINDOW_STATE:
			break;
		case PlayerMenuWindowState.OPTION_WINDOW_STATE:
			Destroy(optionsWindow.gameObject);
			break;
		}
	}

	public void DrawGUI()
	{
		switch(WindowState)
		{
		case PlayerMenuWindowState.INVENTORY_WINDOW_STATE:
			InventoryWnd.DrawGUI();
			break;
		case PlayerMenuWindowState.SPELL_WINDOW_STATE:
			SpellBookMng.DrawGUI();
			break;
		case PlayerMenuWindowState.EXTRA_WINDOW_STATE:
			extraMenuWindow.DrawGUI();
			break;
		case PlayerMenuWindowState.OPTION_WINDOW_STATE:
			optionsWindow.DrawGUI();
			break;
		}
	}

	public void SetWindowState(PlayerMenuWindowState newState)
	{
		if(WindowState == newState)
		{
			switch(newState)
			{
			case PlayerMenuWindowState.SPELL_WINDOW_STATE:
				SpellBookMng.SetWindowState(SpellBookWindowState.SPELL_AND_ITEMS_WINDOW_STATE);
				break;
			case PlayerMenuWindowState.EXTRA_WINDOW_STATE:
				extraMenuWindow.OnDisable();
				extraMenuWindow.windowState = ExatraMenuWindowState.EXTRA_WINDOW_STATE;
				break;
			}
		}
		else
		{
			OnDisable();

			switch(newState)
			{
			case PlayerMenuWindowState.INVENTORY_WINDOW_STATE:
				InventoryWnd.gameObject.SetActive(true);
				break;
			case PlayerMenuWindowState.SPELL_WINDOW_STATE:
				SpellBookMng.gameObject.SetActive(true);
				break;
			case PlayerMenuWindowState.EXTRA_WINDOW_STATE:
				extraMenuWindow.gameObject.SetActive(true);
				break;
			case PlayerMenuWindowState.OPTION_WINDOW_STATE:
				GameObject go = Instantiate<GameObject> (Resources.Load<GameObject> ("Interface Prefabs/Options"));
				if(go != null)
				{
					optionsWindow = go.GetComponent<OptionsWindow>();
					if(optionsWindow == null)
					{
						Debug.LogError("OptionsWindow script missing.");
					}
					else
					{
						optionsWindow.transform.parent = this.transform;
						optionsWindow.parent = this;
					}
				}
				else
				{
					Debug.LogError("Can't load Options prefab.");
				}
				break;
			}
			WindowState = newState;
		}
	}
}
