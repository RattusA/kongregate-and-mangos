﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GossipMenuItem
{
	public byte m_Icon;
	public byte m_Coded;
	public string m_Message;
	public uint m_Sender;
	public uint m_OptionId;
	public string m_BoxMessage;
	public uint m_BoxMoney;
}

public class QEmote
{
	public uint m_emote; 
	public uint m_delay;
}

public class GossipTextOption
{
	public string m_text_0;
	public string m_text_1;
	public uint m_language;
	public float m_probability;
	public QEmote[] m_qEmote = new QEmote[3];
	
	public string Print()
	{
		string ret = "";
		ret += "Text1: " + m_text_0;
		ret += ", Text2: " + m_text_1;
		ret += ", language: " + m_language;
		ret += ", probability: " + m_probability;
		return ret;
	}
}

public class GossipManager
{
	private ulong npcGuid;	

	private uint _m_TitleId;
	private uint _m_MenuId;
	private List<GossipMenuItem> _m_MenuItems = new List<GossipMenuItem>();
	private List<GossipTextOption> _m_Options = new List<GossipTextOption>();

	private GossipWindow _gossipWindow;
	public int MAX_GOSSIP_TEXT_OPTIONS = 8;
	public int menuCount = 0;

	public GossipManager(ulong guid)
	{
		npcGuid = guid;
	}

	public void SetMenuId(uint m_MenuId)
	{
		_m_MenuId = m_MenuId;
	}
	
	public void SetTitleId(uint m_TitleId)
	{
		_m_TitleId = m_TitleId;
		GetGossipText();
	}
	
	public void AddMenuItems(List<GossipMenuItem> m_MenuItems)
	{
		_m_MenuItems = m_MenuItems;
		menuCount = _m_MenuItems.Count;
	}
	
	public void UpdateTextOptions(WorldPacket pkt)
	{
		uint titleID = (uint)pkt.ReadReversed32bit();
		
		if(titleID != _m_TitleId)
		{
			return;
		}

		for(int index = 0; index < MAX_GOSSIP_TEXT_OPTIONS; index++)
		{
			GossipTextOption option = new GossipTextOption();
			option.m_probability = pkt.ReadFloat();
			option.m_text_0 = pkt.ReadString();
			option.m_text_1 = pkt.ReadString();
			option.m_language = (uint)pkt.ReadReversed32bit();
			
			for(int emoteIndex = 0; emoteIndex < 3; emoteIndex++)
			{
				option.m_qEmote[emoteIndex] = new QEmote();
				option.m_qEmote[emoteIndex].m_delay = (uint)pkt.ReadReversed32bit();
				option.m_qEmote[emoteIndex].m_emote = (uint)pkt.ReadReversed32bit();							
			}
			_m_Options.Add(option);
		}
	}

	public string GetGossipInfoText()
	{
		string ret = "";
		if(_m_Options.Count > 0)
		{
			ret =  _m_Options[0].m_text_0;
		}
		return ret;
	}

	public string GetMenuItemText(int index)
	{
		return _m_MenuItems[index].m_Message;
	}

	public string GetMenuItemMessageBox(int index)
	{
		return _m_MenuItems[index].m_BoxMessage;
	}

	/*	============	WORLD PACKET	============	*/

	/**
	 *	Sending request to the server to get the list menu items.
	 */
	public void GetGossipMenu()
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_GOSSIP_HELLO, 8);
		pkt.Add(npcGuid);
		RealmSocket.outQueue.Add(pkt);
	}

	/**
	 *	Sending request to the server to get the menu items texts.
	 */
	private void GetGossipText()
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_NPC_TEXT_QUERY, 8+4);
		pkt.Add(_m_TitleId);
		pkt.Add(npcGuid);
		RealmSocket.outQueue.Add(pkt); 	
	}
	
	/**
	 *	Sending action request to the server for selected menu item.
	 */
	public void SendGossipSelectedOption(int gossipListId)
	{
		WorldPacket pkt = new WorldPacket(OpCodes.CMSG_GOSSIP_SELECT_OPTION, 8+4+4);
		pkt.Add(npcGuid);
		pkt.Add(_m_MenuId);
		pkt.Add((uint)gossipListId);
		RealmSocket.outQueue.Add(pkt); 
	}
}
