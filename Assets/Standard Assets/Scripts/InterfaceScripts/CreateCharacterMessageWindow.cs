﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreateCharacterMessageWindow : MonoBehaviour
{
	public Text label;
	public GameObject oneButtonGO;
	public GameObject twoButtonGO;
	public InputField input;

	public void ShowResultMessageWindow(string message)
	{
		label.text = message;
		oneButtonGO.SetActive(true);
		twoButtonGO.SetActive(false);
	}

	public void ShowDeleteMessageWindow(string name)
	{
		input.text = string.Empty;
		label.text = "Are you sure you want to delete [" + name + "] character?";
		label.text += "\nTo confirm delete please type character name in to the box below.";
		oneButtonGO.SetActive(false);
		twoButtonGO.SetActive(true);
	}

	public void OnDisable()
	{
		oneButtonGO.SetActive(false);
		twoButtonGO.SetActive(false);
	}
}