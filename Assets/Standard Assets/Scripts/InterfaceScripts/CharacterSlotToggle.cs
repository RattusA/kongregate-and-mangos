﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterSlotToggle : MonoBehaviour
{
	public Toggle toggle;
	public Image image;
	public Image checkmark;
	public Text label;
}
