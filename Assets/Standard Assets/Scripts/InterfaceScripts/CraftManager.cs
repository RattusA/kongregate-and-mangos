using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum CraftSkill
{
	SKILL_FIRST_AID 				= 129,
	SKILL_BLACKSMITHING				= 164,
	SKILL_LEATHERWORKING			= 165,
	SKILL_ALCHEMY					= 171,
	SKILL_HERBALISM					= 182,
	SKILL_COOKING					= 185,
	SKILL_MINING					= 186,
	SKILL_TAILORING					= 197,
	SKILL_ENGINERING				= 202,
	SKILL_ENCHANTING				= 333,
	SKILL_FISHING 					= 356,
	SKILL_SKINNING					= 393,
	SKILL_JEWELCRAFTING				= 755,
	SKILL_RIDING					= 762,
	SKILL_INSCRIPTION				= 773
}

public class CraftProfession
{
	public List<CraftSpell> spellList;
	public List<string> sortList;
	public uint skillID;
	public string professionName;
	public string skillValue;
	public uint CurrentSkillValue;

	public CraftProfession(uint skillID, string skillValue, uint currentSkillValue)
	{
		this.skillID = skillID;
		this.skillValue = skillValue;
		CurrentSkillValue = currentSkillValue;

		professionName = GetProfessionName((CraftSkill)skillID);
		GetCraftSpellsList();
	}

	public static string GetProfessionName(CraftSkill skill)
	{
		string ret = "";
		switch(skill)
		{
		case CraftSkill.SKILL_FIRST_AID:
			ret = "Bandaging";
			break;
		case CraftSkill.SKILL_BLACKSMITHING:
			ret = "Metalcrafting";
			break;
		case CraftSkill.SKILL_LEATHERWORKING:
			ret = "Leathercrafting";
			break;
		case CraftSkill.SKILL_ALCHEMY:
			ret = "Witchcrafting";
			break;
		case CraftSkill.SKILL_HERBALISM:
			ret = "Gathering";
			break;
		case CraftSkill.SKILL_COOKING:
			ret = "Cooking";
			break;
		case CraftSkill.SKILL_MINING:
			ret = "Prospecting";
			break;
		case CraftSkill.SKILL_TAILORING:
			ret = "Outfitting";
			break;
		case CraftSkill.SKILL_ENGINERING:
			ret = "Inventing";
			break;
		case CraftSkill.SKILL_ENCHANTING:
			ret = "Re-forging";
			break;
		case CraftSkill.SKILL_FISHING:
			ret = "Fishing";
			break;
		case CraftSkill.SKILL_SKINNING:
			ret = "Scalping";
			break;
		case CraftSkill.SKILL_JEWELCRAFTING:
			ret = "Gemcrafing";
			break;
		case CraftSkill.SKILL_RIDING:
			ret = "Riding";
			break;
		case CraftSkill.SKILL_INSCRIPTION:
			ret = "Scrollcrafting";
			break;
		default:
			ret = "Unknown";
			break;
		}
		return ret;
	}

	private void GetCraftSpellsList()
	{
		spellList = new List<CraftSpell>();
		sortList = new List<string>();
		sortList.Add("All Spots");

		foreach(SkillLineAbilityEntry record in dbs.sSkillLIneAbility.colection)
		{
			if(record.m_skillLine == skillID)
			{
				SpellEntry spEntry = dbs.sSpells.GetRecord(record.m_spell);
				if(spEntry.ID != 0 && spEntry.Reagent[0] != 0 && spEntry.ReagentCount[0] != 0)
				{
					CraftSpell craftSpell = new CraftSpell((uint)record.m_spell);
					craftSpell.spellID = spEntry.ID;
					craftSpell.spellName = spEntry.SpellName.Strings[0];

					craftSpell.grayValue = (uint)(record.m_trivialSkillLineRankHigh * 2);
					craftSpell.yellowValue = (uint)(record.m_trivialSkillLineRankLow * 2);

					for(int index = 0; index < 8; index++)
					{
						if(spEntry.Reagent[index] != 0 && spEntry.ReagentCount[index] != 0)
						{
							CraftReagent craftReagent = new CraftReagent((uint)spEntry.Reagent[index], 
							                                             (uint)spEntry.ReagentCount[index]);
							craftSpell.reagents.Add(craftReagent);
						}
					}
					craftSpell.SetResultItem((uint)spEntry.EffectItemType[0]);
					spellList.Add(craftSpell);
				}
			}
		}
	}
}

public class CraftSpell
{
	public uint spellID;
	public string spellName;
	public List<CraftReagent> reagents;
	public uint resultItemID;
	public uint countInInventory;
	public string itemName;
	public string itemIcon;

	public uint grayValue;
	public uint yellowValue;

	public CraftSpell(uint spellID)
	{
		this.spellID = spellID;
		reagents = new List<CraftReagent>();
	}

	public void SetResultItem(uint resultItemID)
	{
		this.resultItemID = resultItemID;
		ItemEntry itemEntry = AppCache.sItems.GetItemEntry(resultItemID);
		if(itemEntry.ID <= 0)
		{
			dbs.sItems.GetRecord(resultItemID);
		}
		itemName = itemEntry.Name;
		itemIcon = dbs.sItemDisplayInfo.GetRecord((int)itemEntry.DisplayID, false).InventoryIcon;
	}

	public int GetMaxCraftCount()
	{
		int maxCraft = 999;
		foreach(CraftReagent craftReagen in reagents)
		{
			int temp = (int)(craftReagen.countInInventory / craftReagen.itemsCount);
			maxCraft = (int)Mathf.Min(maxCraft, temp);
		}

		return maxCraft;
	}
}

public class CraftReagent
{
	public string itemName;
	public string itemIcon;
	public uint itemID;
	public uint itemsCount;
	public uint countInInventory;

	public CraftReagent(uint itemID, uint itemsCount)
	{
		this.itemID = itemID;
		this.itemsCount = itemsCount;

		ItemEntry itemEntry = AppCache.sItems.GetItemEntry(itemID);
		if(itemEntry.ID <= 0)
		{
			itemEntry = dbs.sItems.GetRecord(itemID);
		}

		itemName = itemEntry.Name;
		itemIcon = dbs.sItemDisplayInfo.GetRecord((int)itemEntry.DisplayID, false).InventoryIcon;
	}
}

public class CraftManager
{
	public List<CraftProfession> mainCraftProfession = new List<CraftProfession>();
	public List<CraftProfession> secondaryCraftProfession = new List<CraftProfession>();
	public CraftProfession selectedProfession;

	public Player mainPlayer;

	public CraftSpell selectedCraftSpell = null;
	public uint spellToCraft = 0;
	public int countCraft = 0;

	public static int mainProfCount = 0;

	public CraftManager(Player playerScript)
	{
		mainPlayer = playerScript;

		mainCraftProfession = new List<CraftProfession>();
		secondaryCraftProfession = new List<CraftProfession>();
		selectedProfession = null;
	}

	public void UnlearnSkill(uint skillID)
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_UNLEARN_SKILL);
		pkt.Add(skillID);
		RealmSocket.outQueue.Add(pkt);
	}

	public void ResetCraftInfo()
	{
		mainProfCount = 0;
		mainCraftProfession.Clear();
		secondaryCraftProfession.Clear();
	}

	public IEnumerator UpdateCraftInfo(uint skillID, bool isMainCraft)
	{
		int craftIndex = GetIndexOfCraftByID(skillID);

		SkillInfo skillInfoValue = PlayerSkills.GetSkillInfoRecord((SkillType)skillID);
		if(skillInfoValue != null)
		{
			if(craftIndex != -1)
			{
				if(isMainCraft)
				{
					mainCraftProfession[craftIndex].skillValue = skillInfoValue.SkillValue;
				}
				else
				{
					secondaryCraftProfession[craftIndex].skillValue = skillInfoValue.SkillValue;
				}
			}
			else
			{
				CraftProfession craft = new CraftProfession(skillID, skillInfoValue.SkillValue, skillInfoValue.StartValue);
				List<CraftSpell> spellList = new List<CraftSpell>();
				foreach(CraftSpell craftSpell in craft.spellList)
				{
					Spell spell = mainPlayer.playerSpellManager.GetSpellByID(craftSpell.spellID);
					if(spell != null)
					{
						spellList.Add(craftSpell);
					}
				}
				craft.spellList = spellList;

				if(isMainCraft)
				{
					mainCraftProfession.Add(craft);
					mainProfCount++;
				}
				else
				{
					secondaryCraftProfession.Add(craft);
				}
			}
		}
		else if(craftIndex != -1)
		{
			mainCraftProfession[craftIndex] = null;
			mainProfCount--;
		}

		yield return new WaitForEndOfFrame();
	}

	public void UpdateCraftReagentsCount()
	{
		foreach(CraftSpell craftSpell in selectedProfession.spellList)
		{
			if(craftSpell != null)
			{
				UpdateCraftReagentsCount(craftSpell);
			}
		}
	}

	public void UpdateCraftReagentsCount(CraftSpell craftSpell)
	{
		Spell spell = mainPlayer.playerSpellManager.GetSpellByID(craftSpell.spellID);
		if(spell != null)
		{
			Item item = mainPlayer.itemManager.GetItemByID(craftSpell.resultItemID);
			if(item == null)
			{
				craftSpell.countInInventory = 0;
			}
			else
			{
				craftSpell.countInInventory = mainPlayer.itemManager.GetItemsTotalCount(craftSpell.resultItemID);
			}
			
			foreach(CraftReagent craftReagent in craftSpell.reagents)
			{
				Item itemReagent = mainPlayer.itemManager.GetItemByID(craftReagent.itemID);
				if(itemReagent == null)
				{
					craftReagent.countInInventory = 0;
				}
				else
				{
					craftReagent.countInInventory = mainPlayer.itemManager.GetItemsTotalCount(craftReagent.itemID);
				}
			}
		}
	}

	public uint GetMainCraftLevelByID(uint skillID)
	{
		uint ret = 0;
		int craftIndex = GetIndexOfCraftByID(skillID);
		if(craftIndex >= 0)
		{
			ret = mainCraftProfession[craftIndex].CurrentSkillValue;
		}
		return ret;
	}

	
	public int GetIndexOfCraftByID(uint skillID)
	{
		int ret = -1;
		CraftProfession craft = mainCraftProfession.Find(x => (x != null && x.skillID == skillID));
		if(craft != null)
		{
			ret = mainCraftProfession.IndexOf(craft);
		}
		else
		{
			craft = secondaryCraftProfession.Find(x => (x != null && x.skillID == skillID));
			if(craft != null)
			{
				ret = secondaryCraftProfession.IndexOf(craft);
			}
		}
		return ret;
	}

	public bool IsSpellRelevantCraft(SpellEntry spell, CraftSkill skill)
	{
		bool ret = false;
		for(int inx = 0; inx < 3; inx++)
		{
			if(spell.Effect[inx] == (int)__SpellEffects.SPELL_EFFECT_SKILL && spell.EffectMiscValue[inx] == (int)skill)
			{
				ret = true;
				break;
			}
		}
		return ret;
	}

	public void CraftToItem(uint spellID, ulong targetGuid)
	{
		mainPlayer.playerSpellManager.CastSpell(spellID, targetGuid, mainPlayer.TransformParent.position);
	}

	public void FixedUpdate()
	{
		List<CraftProfession>.Enumerator CraftProfessionEnumerator = mainCraftProfession.GetEnumerator();
		while (CraftProfessionEnumerator.MoveNext())
		{
			CraftProfession craftProf = CraftProfessionEnumerator.Current;
			if(craftProf != null)
			{
				UpdateCraftInfo(craftProf.skillID, true);
			}
		}
	}
}