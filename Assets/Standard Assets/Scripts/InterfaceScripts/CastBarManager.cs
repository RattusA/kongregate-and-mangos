﻿using UnityEngine;
using System.Collections;

public class CastBarManager : MonoBehaviour {

	[SerializeField]
	private Texture _progressBarTexture;
	[SerializeField]
	private Texture _progressBarBackTexture;

	public bool flag = false;

	public float castTime;
	public float castTimeTotal;
	public bool isConductedSpell;
	
	public void SetCastInfo(float castTime, float maxCastTime, bool isConductiveSpell)
	{
		this.castTime = castTime;
		this.castTimeTotal = maxCastTime;
		this.isConductedSpell = isConductiveSpell;
	}

	public void FixedUpdate()
	{
		if(castTime == 0 && castTimeTotal == 0)
		{
			return;
		}

		if(isConductedSpell)
		{
			if(castTime < castTimeTotal)
			{
				castTime += Time.fixedDeltaTime;
			}
			else
			{
				this.gameObject.SetActive(false);
			}
		}
		else 
		{
			if(castTime > 0)
			{
				castTime -= Time.fixedDeltaTime;
			}
			else
			{
				this.gameObject.SetActive(false);
			}
		}
	}

	public void DrawGUI()
	{
		float SW = Screen.width;
		float SH = Screen.height;
		
		float castTimeResult = castTime / castTimeTotal;
		GUI.DrawTexture(new Rect(SW * 0.4f, SH * 0.8f, SW * 0.2f, SH * 0.03f), _progressBarBackTexture);
		GUI.DrawTexture(new Rect(SW * 0.4055f, SH * 0.8105f, 
		                         SW * 0.189f * castTimeResult, SH * 0.01f), _progressBarTexture);
	}

	private void OnDisable()
	{
		flag = false;
		castTime = 0;
		castTimeTotal = 0;
	}
}
