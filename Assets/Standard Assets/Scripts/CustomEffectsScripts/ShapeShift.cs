using UnityEngine;

public class ShapeShift : MonoBehaviour {
	private Animation _animHolder;
	private string _currentAnimation = "";
	
	
	public Animation AnimHolder{
		set {
			_animHolder = value;
			ActivateMesh(_animHolder.transform, false);
			ActivateMesh(transform, true);
		}
		get {
			return _animHolder;
		}		
	}
	
	public void ActivateMesh (Transform root, bool val) {
		foreach(MeshRenderer go in root.GetComponentsInChildren(typeof(MeshRenderer))){
			go.enabled = val;
		}
		foreach(SkinnedMeshRenderer go in root.GetComponentsInChildren(typeof(SkinnedMeshRenderer))){
			go.enabled = val;
		}		
	}
	
	//brodcast message recievers
	public void PlayMirroredMovementAnimation (string val) {
		//Debug.Log ("ANIMATION!!!!! " + val);
		_currentAnimation = val;
	}
	
	public void DestroyShapeShift () {
		ActivateMesh(_animHolder.transform, true);
		Destroy(transform.parent.gameObject);
	}
	
	//end broadcast message reviecers
	
	public void ShowMessage () {
		Debug.Log("*********************NICU RULLEZZZ!!!***********************");
	}
	
	
	void LateUpdate () {
		if(_currentAnimation == "" || _currentAnimation == "die"){
			return;
		}
		
		try{
			transform.GetComponent<Animation>().Play(_currentAnimation);
		}
		catch(System.Exception exception){
			Debug.Log ("EXCEPTION " + exception);
		}
	}

	public void SetAnimHolder(Animation anim)
	{
		AnimHolder = anim;
	}
}