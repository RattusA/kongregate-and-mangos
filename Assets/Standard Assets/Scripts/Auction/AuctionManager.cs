﻿using UnityEngine;
using System.Collections.Generic;

public class AuctionManager
{
	public ulong auctioneerGuid;
	private List<AuctionItem> itemList = new List<AuctionItem>();
	
	private bool  updateActiveListingCount = true;
	private uint activeListingCount = 0;
	
//	private bool  isStartAuction = false;
//	private bool  isSelectItemToSell = false;
//	private bool  isOwnLots = false;
	
	// ++++++++++++Create Lot
//	bool  isCreateLot = false;
	public Item selectedItem;
	public uint bundlesCount = 1;
	public uint stackSize = 1;
	public uint startCost = 1;
	public uint finishCost = 0;
	public uint lotTime; // in minutes 720, 1440, 2880
	public AuctionLotTime lotTimeID = AuctionLotTime.SHORT_TIME; // in minutes 720, 1440, 2880
	// ------------Create Lot
	
	// ++++++++++++Search and Sort
//	bool  isSearch = false;
	string filterByName = "";
	uint filterListFrom = 0;
	byte filterByMinLevel = 0;
	byte filterByMaxLevel = 0;
	uint filterByAuctionSlotId = 0xffffffff; 		// inventoryType
	public uint filterByAuctionMainCategory = 0xffffffff;	// class
	public uint filterByAuctionSubCategory = 0xffffffff;	// subclass
	public uint filterByQuality = 0xffffffff;
	byte filterByUsable = 0;
	byte filterByIsFull = 0; 						// 1 - ignorig all filters
	byte MAX_AUCTION_SORT = 12;
	// fields for sort unknown
	byte sortCount = 0;
	// ------------Search and Sort
	
	// ++++++++++++PlaceBid
	bool  isBuyLot = false;
	public uint newBid = 0;
	uint numberOfInterestedLot = 0;
	// ------------PlaceBid
	
	public string itemClassText = "All";
	public string filterByQualityText;
	public AuctionItem selectedLot;

	public AuctionManager()
	{
		filterByQualityText = ItemFlags.GetQualityName((ItemQualities)filterByQuality);
	}
	
	public void  StartAuction ()
	{
		OwnLotsRequest();
//		isStartAuction = true;
	}
	
	void  Clear ()
	{
		itemList.Clear();
	}
	
	void  Add ( AuctionItem item )
	{
		itemList.Add(item);
	}
	
	public void  RequestAuctionListOfBidderItems ()
	{
		WorldPacket packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_AUCTION_LIST_BIDDER_ITEMS);
		uint listfrom = 1;
		uint outbiddedCount = 0;
		packet.Append(ByteBuffer.Reverse(auctioneerGuid));
		packet.Append(ByteBuffer.Reverse(listfrom));
		packet.Append(ByteBuffer.Reverse(outbiddedCount));
		uint outbiddedAuctionId = 0;
		for(uint count = 0; count < outbiddedCount; count++)
		{
			packet.Append(ByteBuffer.Reverse(outbiddedAuctionId));
		}
		RealmSocket.outQueue.Add(packet);
//		isStartAuction = true;
	}
	
	public void  OwnLotsRequest ()
	{
		WorldPacket packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_AUCTION_LIST_OWNER_ITEMS);
		packet.Append(ByteBuffer.Reverse(auctioneerGuid));
		uint listfrom = 0;
		packet.Append(ByteBuffer.Reverse(listfrom));
		RealmSocket.outQueue.Add(packet);
	}
	
	public void  AuctionListOfItemsResponse ( WorldPacket paket )
	{
		Clear();
		uint itemCount = paket.ReadReversed32bit();
		for(uint count = 0; count < itemCount; count++)
		{
			AuctionItem auctionItem= new AuctionItem();
			auctionItem.auctionId = paket.ReadReversed32bit();
			auctionItem.itemId = paket.ReadReversed32bit();
			if(auctionItem.itemId != 0 && !AppCache.sItems.HaveRecord(auctionItem.itemId))
			{
				AppCache.sItems.SendQueryItem(auctionItem.itemId);
			}
			for(byte i = 0; i < AuctionItem.ENCHANTMENT_COUNT; i++)
			{
				auctionItem.enchants[i] = new Enchantment();
				auctionItem.enchants[i].id = paket.ReadReversed32bit();
				auctionItem.enchants[i].duration = paket.ReadReversed32bit();;
				auctionItem.enchants[i].charges = paket.ReadReversed32bit();;
			}
			
			auctionItem.itemRandomPropertyId = paket.ReadReversed32bit();
			auctionItem.suffixFactor = paket.ReadReversed32bit();;
			auctionItem.itemCount = paket.ReadReversed32bit();
			auctionItem.itemCharge = paket.ReadReversed32bit();
			auctionItem.itemFlags = paket.ReadReversed32bit();
			auctionItem.ownerGuid = paket.ReadReversed64bit();
			auctionItem.startBid = paket.ReadReversed32bit();
			auctionItem.minimalOutBid = paket.ReadReversed32bit();
			auctionItem.buyout = paket.ReadReversed32bit();
			auctionItem.timeLeft = paket.ReadReversed32bit();
			auctionItem.currentBidderGuid = paket.ReadReversed64bit();
			auctionItem.currentBid = paket.ReadReversed32bit();
			Add(auctionItem);
		}
		uint totalCount = paket.ReadReversed32bit();
		uint dalayForNextRequest = paket.ReadReversed32bit();
		
		if(updateActiveListingCount)
		{
			updateActiveListingCount = false;
			activeListingCount = itemCount;
		}
		else
		{	
			NPC npc = WorldSession.player.target as NPC;
			npc.auctioneer.ChangeState(AuctionWindiwState.AUCTION_SEARCH_RESULT_WINDOW);
		}
	}
	
	public void  SellItem ()
	{
		WorldPacket packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_AUCTION_SELL_ITEM);
		packet.Append(ByteBuffer.Reverse(auctioneerGuid));
		packet.Append(ByteBuffer.Reverse(bundlesCount));
		for(uint i = 0; i < bundlesCount; i++)
		{
			packet.Append(ByteBuffer.Reverse(selectedItem.guid));
			packet.Append(ByteBuffer.Reverse(stackSize));
		}
		packet.Append(ByteBuffer.Reverse(startCost));
		packet.Append(ByteBuffer.Reverse(finishCost));
		packet.Append(ByteBuffer.Reverse(lotTime));
		RealmSocket.outQueue.Add(packet);
		
		NPC npc = WorldSession.player.target as NPC;
		npc.auctioneer.ChangeState(AuctionWindiwState.AUCTION_SEARCH_RESULT_WINDOW);
	}
	
	public void  CancelLot ()
	{
		WorldPacket packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_AUCTION_REMOVE_ITEM);
		packet.Append(ByteBuffer.Reverse(auctioneerGuid));
		packet.Append(ByteBuffer.Reverse(selectedLot.auctionId));
		RealmSocket.outQueue.Add(packet);
		OwnLotsRequest();
	}
	
	public void  SearchedAndSortedListItemsRequest ()
	{
		WorldPacket packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_AUCTION_LIST_ITEMS);
		packet.Append(ByteBuffer.Reverse(auctioneerGuid));
		packet.Append(ByteBuffer.Reverse(filterListFrom));
		packet.Append(filterByName);
		packet.Append(filterByMinLevel);
		packet.Append(filterByMaxLevel);
		packet.Append(ByteBuffer.Reverse(filterByAuctionSlotId));
		packet.Append(ByteBuffer.Reverse(filterByAuctionMainCategory));
		packet.Append(ByteBuffer.Reverse(filterByAuctionSubCategory));
		packet.Append(ByteBuffer.Reverse(filterByQuality));
		packet.Append(filterByUsable);
		packet.Append(filterByIsFull);
		packet.Append(sortCount);
		for(byte i = 0; i < sortCount; i++)
		{
			// TODO sortting
		}
		RealmSocket.outQueue.Add(packet);
	}
	
	void  ListOfPendingSalesRequest ()
	{
		WorldPacket packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_AUCTION_LIST_PENDING_SALES);
		packet.Append(ByteBuffer.Reverse(auctioneerGuid));
		RealmSocket.outQueue.Add(packet);
	}
	
	public void  ListOfPendingSalesResponse ( WorldPacket paket )
	{
		uint count = paket.ReadReversed32bit();
		for(uint i = 0; i < count; i++)
		{
			string str1 = paket.ReadString();
			string str2 = paket.ReadString();
			uint unk1 = paket.ReadReversed32bit(); // Constant 97250
			uint unk2 = paket.ReadReversed32bit(); // Constant 68
			float timeLeft = paket.ReadFloat(); // In day
		}
	}
	
	public void  PlaceBidRequest ()
	{
		if(newBid < selectedLot.currentBid)
			newBid = selectedLot.currentBid+1;
		
		MainPlayer mainPlayer = WorldSession.player;
		WorldPacket packet = new WorldPacket();
		packet.SetOpcode(OpCodes.CMSG_AUCTION_PLACE_BID);
		packet.Append(ByteBuffer.Reverse(auctioneerGuid));
		packet.Append(ByteBuffer.Reverse(selectedLot.auctionId));
		packet.Append(ByteBuffer.Reverse(newBid));
		RealmSocket.outQueue.Add(packet);
	}
	
	
	public string GetActiveListingCount ()
	{
		return activeListingCount.ToString();
	}
	
	public uint GetItemListCount ()
	{
		return (uint)itemList.Count;
	}
	
	public AuctionItem GetItemByIndex ( byte index )
	{
		return itemList[index];
	}
	
	
	private Vector2 scrollViewVector;
	void  ItemsList ( int windowID )
	{
		GUI.Box( new Rect(10, 30, Screen.width-200, Screen.height-60), "");
		GUILayout.BeginArea( new Rect(10, 30, Screen.width-200, Screen.height-60));
		scrollViewVector = GUILayout.BeginScrollView(scrollViewVector);
		for(uint lotNumber = 0; lotNumber < itemList.Count; lotNumber++)
		{
			Item item = AppCache.sItems.GetRecord(itemList[(int)lotNumber].itemId);
			if(GUILayout.Button(item.name + " count = " + itemList[(int)lotNumber].itemCount))
			{
				switch(windowID)
				{
				case 0:
					numberOfInterestedLot = lotNumber;
					isBuyLot = true;
					Debug.Log("You buy item - " + item.name);
					break;
				case 1:
					//CancelLot(lotNumber);
					Debug.Log("You canceled lot with item - " + item.name);
					break;
				}
			}
		}
		GUILayout.EndScrollView();
		GUILayout.EndArea();
		
		if(GUI.Button( new Rect(Screen.width-190, 30, 180, 20), "Search"))
		{
//			isStartAuction = false;
//			isOwnLots = false;
//			isSearch = true;
		}
		if(GUI.Button( new Rect(Screen.width-190, 70, 180, 20), "Reset"))
		{
			ResetSearch();
			SearchedAndSortedListItemsRequest();
//			isStartAuction = true;
//			isOwnLots = false;
		}
		if(GUI.Button( new Rect(Screen.width-190, 110, 180, 20), "Own Lots"))
		{
			OwnLotsRequest();
//			isStartAuction = false;
//			isOwnLots = true;
//			isSearch = false;
		}
		if(GUI.Button( new Rect(Screen.width-190, 250, 180, 20), "Add to Auction"))
		{
			ShowInventorySlots();
//			isStartAuction = false;
//			isOwnLots = false;
//			isSelectItemToSell = true;
		}
		
		if(GUI.Button( new Rect(Screen.width-190, 200, 180, 30), "Exit"))
		{
//			isStartAuction = false;
//			isOwnLots = false;
		}
	}
	
	public void  ShowInventorySlots ()
	{
		MainPlayer mainPlayer = WorldSession.player;
		mainPlayer.inv.isActive = true;
		mainPlayer.inv.SetDelegate(ItemDelegate);
		mainPlayer.inv.ShowBags();
		mainPlayer.inv.ShowItems();
	}
	
	public void  DestroyInventorySlots ()
	{
		MainPlayer mainPlayer = WorldSession.player;
		mainPlayer.inv.ClearItems();
		mainPlayer.inv.ClearBags();
		mainPlayer.inv.isActive = false;
		mainPlayer.inv.currentBag = 0;
		mainPlayer.inv.SetDelegate(null);
	}
	
	void  ItemDelegate ( UIButton sender )
	{
		MainPlayer mainPlayer = WorldSession.player;
		Item item;
		if(mainPlayer.inv.currentBag == 0)
			item = mainPlayer.itemManager.BaseBagsSlots[sender.info];
		else
			item = mainPlayer.itemManager.BaseBags[(int)mainPlayer.inv.currentBag].GetItemFromSlot(sender.info);
		selectedItem = item;
//		isCreateLot = true;
		DestroyInventorySlots();
	}
	
	void  CreateLotWindow ( int windowID )
	{
		if(selectedItem == null)
		{
			Debug.LogError("Not selected item for lot");
			return;
		}
		Item item = AppCache.sItems.GetRecord(selectedItem.entry);
		int maxItemsInStack = item.stackable;
		uint maxItems;
		if(maxItemsInStack <= 1)
		{
			maxItems = 1;
		}
		else
		{
			maxItems = GetCountByItem(item);
		}
		GUI.Label( new Rect(10, 20, Screen.width-20, 20), item.name + " count = " + maxItems);
		if(maxItemsInStack > 1)
		{
			bundlesCount = System.Convert.ToUInt32(GUI.TextField( new Rect(10, 60, 100, 20), bundlesCount.ToString(), 3));
			if(bundlesCount == 0) bundlesCount = 1;
			if(GUI.Button( new Rect(130, 60, 100, 20), "Max"))
			{
				bundlesCount = maxItems/stackSize;
			}
			if(bundlesCount > 999)
				bundlesCount = 999;
			
			stackSize = System.Convert.ToUInt32(GUI.TextField( new Rect(10+Screen.width/2+20, 60, 100, 20), stackSize.ToString(), 3));
			if(stackSize == 0) stackSize = 1;
			if(GUI.Button( new Rect(10+Screen.width/2+20+130, 60, 100, 20), "Max"))
			{
				bundlesCount = 1;
				stackSize = (uint)maxItemsInStack;
			}
			if(stackSize > 999)
				stackSize = 999;
		}
		startCost = System.Convert.ToUInt32(GUI.TextField( new Rect((Screen.width-100)/2, 100, 100, 20), startCost.ToString(), 3));
		if(startCost == 0)
		{
			startCost = 1;
		}
		
		finishCost = System.Convert.ToUInt32(GUI.TextField( new Rect((Screen.width-100)/2, 140, 100, 20), finishCost.ToString(), 3));
		
		lotTime = System.Convert.ToUInt32(GUI.TextField( new Rect((Screen.width-100)/2, 180, 100, 20), lotTime.ToString(), 3));
		if(lotTime == 0 || lotTime != 720 || lotTime != 1440 || lotTime != 2880) 
		{
			lotTime = 720;
		}
		if(GUI.Button( new Rect((Screen.width-100)/2, 220, 100, 20), "Create Lot"))
		{
			SellItem();
			OwnLotsRequest();
//			isStartAuction = false;
//			isOwnLots = true;
//			isCreateLot = false;
		}
		if(GUI.Button( new Rect((Screen.width-100)/2, 260, 100, 30), "Exit"))
		{
//			isStartAuction = false;
//			isOwnLots = true;
//			isCreateLot = false;
		}
	}
	
	public uint GetCountByItem ( Item item )
	{
		MainPlayer mainPlayer = WorldSession.player;
		Item curentItem;
		uint count = 0;
		for(byte slotInMainBag = 0; slotInMainBag < 16; slotInMainBag++)
		{
			curentItem = mainPlayer.itemManager.BaseBagsSlots[slotInMainBag];
			if((curentItem != null) && (curentItem.entry == item.entry))
			{
				count += curentItem.count;
			}
		}
		foreach(KeyValuePair<int, Bag> bag in mainPlayer.itemManager.BaseBags)
		{
			for(byte slotInBag = 0; slotInBag < bag.Value.GetCapacity(); slotInBag++)
			{
				curentItem = bag.Value.GetItemFromSlot(slotInBag);
				if((curentItem != null) && (curentItem.entry == item.entry))
				{
					count += curentItem.count;
				}
			}
		}
		return count;
	}
	
	void  SearchForm ()
	{
		filterByName = GUI.TextField( new Rect(20, 100, Screen.width - 40, 20), filterByName, 256);
		
		if(GUI.Button( new Rect((Screen.width-100)/2, 220, 100, 20), "Search"))
		{
			SearchedAndSortedListItemsRequest();
//			isStartAuction = true;
//			isOwnLots = false;
//			isSearch = false;
		}
		
		if(GUI.Button( new Rect((Screen.width-100)/2, 260, 100, 30), "Exit"))
		{
//			isStartAuction = true;
//			isOwnLots = false;
//			isSearch = false;
		}
	}
	
	void  BuyLot ()
	{
		AuctionItem auctionItem = itemList[(int)numberOfInterestedLot];
//		isStartAuction = false;
//		isOwnLots = false;
		newBid = System.Convert.ToUInt32(GUI.TextField( new Rect((Screen.width-100)/2, 100, 100, 20), newBid.ToString()));
		if(newBid < auctionItem.currentBid)
			newBid = auctionItem.currentBid+1;
		
		if(GUI.Button( new Rect((Screen.width-100)/2, 220, 100, 20), "Place Bid"))
		{
			PlaceBidRequest();
			SearchedAndSortedListItemsRequest();
		}
		
		if(GUI.Button( new Rect((Screen.width-100)/2, 260, 100, 30), "Exit"))
		{
//			isStartAuction = true;
			isBuyLot = false;
		}		
	}
	
	void  ResetSearch ()
	{
		filterByName = "";
		filterListFrom = 0;
		filterByMinLevel = 0;
		filterByMaxLevel = 0;
		filterByAuctionSlotId = 0xffffffff; // inventoryType
		filterByAuctionMainCategory = 0xffffffff;
		filterByAuctionSubCategory = 0xffffffff;
		filterByQuality = 0xffffffff;
		filterByUsable = 0;
		filterByIsFull = 0;
	}
	
	public void  ChangeTime ()
	{
		switch(lotTimeID)
		{
		case AuctionLotTime.SHORT_TIME:
			lotTimeID = AuctionLotTime.MEDIUM_TIME;
			break;
		case AuctionLotTime.MEDIUM_TIME:
			lotTimeID = AuctionLotTime.LONG_TIME;
			break;
		case AuctionLotTime.LONG_TIME:
			lotTimeID = AuctionLotTime.SHORT_TIME;
			break;
		}
	}
	
	public uint GetLotTime ()
	{
		uint ret;
		switch(lotTimeID)
		{
		case AuctionLotTime.SHORT_TIME:
			ret = 720;
			break;
		case AuctionLotTime.MEDIUM_TIME:
			ret = 1440;
			break;
		case AuctionLotTime.LONG_TIME:
			ret = 2880;
			break;
		default:
			ret = 720;
			break;
		}
		return ret;
	}
	
	public string GetLotTimeStr ()
	{
		string ret;
		switch(lotTimeID)
		{
		case AuctionLotTime.SHORT_TIME:
			ret = "12 hours";
			break;
		case AuctionLotTime.MEDIUM_TIME:
			ret = "24 hours";
			break;
		case AuctionLotTime.LONG_TIME:
			ret = "48 hours";
			break;
		default:
			ret = "12 hours";
			break;
		}
		return ret;
	}
}