﻿using UnityEngine;

public class AuctionSearchWindow
{
	protected Auctioneer auctioneer;
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Rect decorationRect;
	private Rect backRect;
	private Rect topMenuRect;
	private Rect topMenuTextRect;
	
	private Rect textFieldLabelRect;
	private Rect textFieldRect;
	private Rect textFieldAreaRect;
	private string textField = "";
	
	private Rect minQualityLabelRect;
	private Rect minQualityTextRect;
	private Rect minQualityRect;
	
	private Rect itemTypeTextRect;
	private Rect itemTypeRect;
	
	private Rect clearRect;
	private Rect searchRect;
	
	private GUIStyle mcTextStyle;
	private GUIStyle mlTextStyle;
	private GUIStyle textAreaStyle;

	public AuctionSearchWindow()
	{
		decorationRect = new Rect(sw * 0.02f, sh * 0.01f, sw * 0.24f, sh * 0.85f);
		backRect = new Rect(sw * 0.03f, sh * 0.89f, sw * 0.075f, sw * 0.06f);
		topMenuRect = new Rect(sw * 0.27f, sh * 0.02f, sw * 0.47f, sw * 0.058f);
		topMenuTextRect = new Rect(sw * 0.31f, sh * 0.02f, sw * 0.4f, sw * 0.058f);
		textFieldLabelRect = new Rect(sw * 0.07f, sh * 0.15f, sw * 0.9f, sh * 0.08f);
		textFieldRect = new Rect(sw * 0.07f, sh * 0.25f, sw * 0.9f, sh * 0.08f);
		textFieldAreaRect = new Rect(sw * 0.075f, sh * 0.25f, sw * 0.9f, sh * 0.08f);
		minQualityLabelRect = new Rect(sw * 0.13f, sh * 0.37f, sw * 0.35f, sh * 0.08f);
		minQualityTextRect = new Rect(sw * 0.11f, sh * 0.45f, sw * 0.35f, sh * 0.08f);
		minQualityRect = new Rect(sw * 0.09f, sh * 0.45f, sw * 0.37f, sh * 0.08f);
		itemTypeTextRect = new Rect(sw * 0.38f, sh * 0.61f, sw * 0.35f, sh * 0.1f);
		itemTypeRect = new Rect(sw * 0.09f, sh * 0.72f, sw * 0.86f, sh * 0.08f);
		clearRect = new Rect(sw * 0.2f, sh * 0.89f, sw * 0.24f, sh * 0.1f);
		searchRect = new Rect(sw * 0.75f, sh * 0.89f, sw * 0.24f, sh * 0.1f);
	}
	
	public void  InitData ()
	{
		NPC npc = WorldSession.player.target as NPC;
		auctioneer = npc.auctioneer;
		
		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		mcTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.06f, 
		                                      Color.black, true, FontStyle.Bold);
		mlTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.06f, 
		                                      Color.white, true, FontStyle.Bold);
		textAreaStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.06f, 
		                                        Color.black, true, FontStyle.Bold);
	}
	
	public void  Show ()
	{		
		GUI.DrawTexture(auctioneer.backgroudRect, auctioneer.backgroundImage);
		auctioneer.auctionAtlas.DrawTexture(decorationRect, "connerDecorationBig.png");
		
		auctioneer.auctionAtlas.DrawTexture(backRect, "btnBack.png");
		if(GUI.Button(backRect, "", GUIStyle.none))
		{
			auctioneer.ChangeState(AuctionWindiwState.AUCTION_MAIN_WINDOW);
		}
		
		auctioneer.auctionAtlas.DrawTexture(topMenuRect, "selectTopMenu.png");
		GUI.Label(topMenuTextRect, "Browse listing", mcTextStyle);
		
		GUI.Label(textFieldLabelRect, "Item Name", mlTextStyle);
		auctioneer.auctionAtlas.DrawTexture(textFieldRect, "searchLine.png");
		textField = GUI.TextArea(textFieldAreaRect, textField, textAreaStyle);
		
		GUI.Label(minQualityLabelRect, "Min. Quality:", mlTextStyle);
		auctioneer.auctionAtlas.DrawTexture(minQualityRect, "selectMenu.png");
		if(GUI.Button(minQualityRect, auctioneer.auctionManager.filterByQualityText, mcTextStyle))
		{
			auctioneer.ChangeState(AuctionWindiwState.AUCTION_ITEM_QUALITY_WINDOW);
		}
		
		GUI.Label(itemTypeTextRect, "Item Types:", mlTextStyle);
		auctioneer.auctionAtlas.DrawTexture(itemTypeRect, "selectedMenuBig.png");
		if(GUI.Button(itemTypeRect, auctioneer.auctionManager.itemClassText, mcTextStyle))
		{
			auctioneer.ChangeState(AuctionWindiwState.AUCTION_ITEM_TYPE_WINDOW);
		}
		
		auctioneer.auctionAtlas.DrawTexture(clearRect, "btnClear.png");
		if(GUI.Button(clearRect, "", GUIStyle.none))
		{
			ClearData();
		}
		
		auctioneer.auctionAtlas.DrawTexture(searchRect, "btnSearch.png");
		if(GUI.Button(searchRect, "", GUIStyle.none))
		{
			SearchLots();
		}
	}
	
	private void  ClearData ()
	{
		auctioneer.auctionManager.filterByQuality = 0xffffffff;
		auctioneer.auctionManager.filterByQualityText = "All";
		auctioneer.auctionManager.filterByAuctionMainCategory = 0xffffffff;
		auctioneer.auctionManager.filterByAuctionSubCategory = 0xffffffff;
		auctioneer.auctionManager.itemClassText = "All";
		textField = "";
	}
	
	private void  SearchLots ()
	{
		auctioneer.windowResultState = 0;
		auctioneer.auctionManager.SearchedAndSortedListItemsRequest();
	}
}