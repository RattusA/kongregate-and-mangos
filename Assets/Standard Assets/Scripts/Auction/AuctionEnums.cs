﻿
public enum AuctionAction
{
	AUCTION_STARTED     = 0,                                // ERR_AUCTION_STARTED
	AUCTION_REMOVED     = 1,                                // ERR_AUCTION_REMOVED
	AUCTION_BID_PLACED  = 2                                 // ERR_AUCTION_BID_PLACED
};

public enum AuctionError
{
	AUCTION_OK                          = 0,                // depends on enum AuctionAction
	AUCTION_ERR_INVENTORY               = 1,                // depends on enum InventoryChangeResult
	AUCTION_ERR_DATABASE                = 2,                // ERR_AUCTION_DATABASE_ERROR (default)
	AUCTION_ERR_NOT_ENOUGH_MONEY        = 3,                // ERR_NOT_ENOUGH_MONEY
	AUCTION_ERR_ITEM_NOT_FOUND          = 4,                // ERR_ITEM_NOT_FOUND
	AUCTION_ERR_HIGHER_BID              = 5,                // ERR_AUCTION_HIGHER_BID
	AUCTION_ERR_BID_INCREMENT           = 7,                // ERR_AUCTION_BID_INCREMENT
	AUCTION_ERR_BID_OWN                 = 10,               // ERR_AUCTION_BID_OWN
	AUCTION_ERR_RESTRICTED_ACCOUNT      = 13                // ERR_RESTRICTED_ACCOUNT
};

// enum EnchantmentSlot have in itemFlags.js
