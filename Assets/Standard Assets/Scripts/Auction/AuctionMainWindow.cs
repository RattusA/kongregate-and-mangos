using UnityEngine;
using System.Collections;

public class AuctionMainWindow
{
	protected Auctioneer auctioneer;
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Rect backRect;
	private Rect topDecorationRect;
	private Rect bottomDecorationRect;
	private Rect myBidsRect;
	private Rect listingRect;
	private Rect browseRect;
	private Rect createRect;
	private Rect activeListingRect;
	private Rect activeListingLabelRect;
	
	private GUIStyle activeListingStyle;

	public AuctionMainWindow()
	{
		backRect = new Rect(sw * 0.03f, sh * 0.06f, sw * 0.07f, sw * 0.0607f);
		topDecorationRect = new Rect(sw * 0.12f, sh * 0.03f, sw * 0.85f, sw * 0.1288f);
		bottomDecorationRect = new Rect(sw * 0.05f, sh * 0.78f, sw * 0.9f, sw * 0.1074f);
		myBidsRect = new Rect(sw * 0.05f, sh * 0.3f, sw * 0.19f, sw * 0.184f);
		listingRect = new Rect(sw * 0.29f, sh * 0.3f, sw * 0.19f, sw * 0.184f);
		browseRect = new Rect(sw * 0.53f, sh * 0.3f, sw * 0.19f, sw * 0.184f);
		createRect = new Rect(sw * 0.77f, sh * 0.3f, sw * 0.19f, sw * 0.184f);
		activeListingRect = new Rect(sw * 0.3f, sh * 0.65f, sw * 0.4f, sw * 0.0554f);
		activeListingLabelRect = new Rect(sw * 0.62f, sh * 0.65f, sw * 0.08f, sw * 0.0554f);
	}

	public void  InitData ()
	{
		NPC npc = WorldSession.player.target as NPC;
		auctioneer = npc.auctioneer;
		
		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		activeListingStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, (uint)(sh * 0.06f), 
		                                             Color.black, true, FontStyle.Bold);
	}
	
	public void  Show ()
	{
		GUI.DrawTexture(auctioneer.backgroudRect, auctioneer.backgroundImage);
		
		auctioneer.auctionAtlas.DrawTexture(topDecorationRect, "top_decoration.png");
		auctioneer.auctionAtlas.DrawTexture(bottomDecorationRect, "bottom_decoration.png");
		
		auctioneer.auctionAtlas.DrawTexture(backRect, "btnBack.png");
		if(GUI.Button(backRect, "", GUIStyle.none))
		{
			CloseAuctionWindow();
			return;
		}
		
		auctioneer.auctionAtlas.DrawTexture(myBidsRect, "btnMyBids.png");
		if(GUI.Button(myBidsRect, "", GUIStyle.none))
		{
			ShowMyBids();
		}
		
		auctioneer.auctionAtlas.DrawTexture(listingRect, "btnMyListing.png");
		if(GUI.Button(listingRect, "", GUIStyle.none))
		{
			ShowMyLots();
		}
		
		auctioneer.auctionAtlas.DrawTexture(browseRect, "btnBrowse.png");
		if(GUI.Button(browseRect, "", GUIStyle.none))
		{
			ShowSeatchWindow();
		}
		
		auctioneer.auctionAtlas.DrawTexture(createRect, "btnCreateAuction.png");
		if(GUI.Button(createRect, "", GUIStyle.none))
		{
			ShowCreateLotWindow();
		}
		
		auctioneer.auctionAtlas.DrawTexture(activeListingRect, "active_listing.png");
		GUI.Label(activeListingLabelRect, auctioneer.auctionManager.GetActiveListingCount(), activeListingStyle);
	}
	
	private void  ShowMyBids ()
	{
		auctioneer.windowResultState = 1;
		auctioneer.auctionManager.RequestAuctionListOfBidderItems();
	}
	
	private void  ShowMyLots ()
	{
		auctioneer.windowResultState = 2;
		auctioneer.auctionManager.OwnLotsRequest();
	}
	
	private void  ShowSeatchWindow ()
	{
		auctioneer.ChangeState(AuctionWindiwState.AUCTION_SEARCH_WINDOW);
	}
	
	private void  ShowCreateLotWindow ()
	{
		if(auctioneer.auctionManager.selectedItem != null)
		{
			auctioneer.auctionManager.ShowInventorySlots();
		}
		
		auctioneer.ChangeState(AuctionWindiwState.AUCTION_CREATE_LOT_WINDOW);
	}
	
	private void  CloseAuctionWindow ()
	{
		MainPlayer mainPlayer = WorldSession.player;
		mainPlayer.blockTargetChange = false;
		WorldSession.interfaceManager.hideInterface = false;
		mainPlayer.interf.stateManager();
		
		auctioneer.ChangeState(AuctionWindiwState.AUCTION_EXIT);
	}
}