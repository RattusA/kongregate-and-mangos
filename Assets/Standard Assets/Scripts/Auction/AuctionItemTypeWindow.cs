﻿using UnityEngine;
using System.Collections;

public class AuctionItemTypeWindow
{
	// Item Class name and ID
	protected string[] itemClassName=	{"All", "Weapon", "Armor", "Bag", "Usable", "Merchandise",
	                                     "Ammo & Arrows", "Ammo Bag & Quiver", "Blueprint",
										"Various", "For Quest"};
	protected int[] itemClassID=	{255, 2, 4, 1, 0, 7, 6, 11, 9, 15, 12};
	
	// Subclass Name
	protected string[] weaponClassName		=	{"All", "1H Axe", "2H Axe", "1H Mace", "2H Mace", "1H Sword", 
												"2H Sword", "Claw", "Dagger", "Hurn", "Polearm",
												"Stave", "Wand", "Bow", "Crossbow", "Gun"};
	protected string[] tradegoodsClassName	=	{"All", "Cloth", "Component", "Flesh", "Invention", 
												"Leather", "Mats", "Ore and Ingot", "Other",
												"Plant", "Primal", "Re-forge", "TNT"};
	protected string[] armourClassName		=	{"All", "Cloth", "Leather", "Mail", "Plate", 
												"Shield", "Artifact", "Various"};
	protected string[] consumableClassName	=	{"All", "Drink & Food", "Elixir", "Extract", "Potion", 
												"Wound Binding", "Other"};
	protected string[] recipeClassName		=	{"All", "Cooking", "Inventing", "Leathercrafting", "Outfitting", 
												"Metalcrafting", "Re-forging", "Witchcrafting", "Other"};
	protected string[] projectileClassName	=	{"All", "Ammo", "Arrow"};
	protected string[] quiverClassName		=	{"All", "Ammo Bag", "Quiver"};
	
	// SubClass ID
	protected int[] weaponClassID			=	{255, 0, 1, 4, 5, 7, 8, 13, 15, 16, 6, 10, 19, 2, 18, 3};
	protected int[] tradegoodsClassID		=	{255, 5, 1, 8, 3, 6, 13, 7, 11, 9, 10, 12, 2};
	protected int[] armourClassID			=	{255, 1, 2, 3, 4, 6, 7, 0};
	protected int[] consumableClassID		=	{255, 5, 1, 4, 2, 7, 8};
	protected int[] recipeClassID			=	{255, 5, 3, 1, 2, 4, 8, 6, 0};
	protected int[] projectileClassID		=	{255, 3, 2};
	protected int[] quiverClassID			=	{255, 3, 2};
	
	
	protected Auctioneer auctioneer;
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	protected Vector2 scrollPosition = Vector2.zero;
	protected Rect scrollZone;
	protected Rect fullScrollZone;
	
	private Rect windowRect;
	private Rect closeRect;
	private Rect decorationRect;
	
//	private Rect classRect;
//	private Rect subclassRect;
	
	private byte selectedItemClass = 255;
	private byte selectedItemSubClass = 255;
	private byte subgroupIndex = 0;
	
	private GUIStyle mcTextStyle;
	
	private string itemTypeText = "All";

	public AuctionItemTypeWindow()
	{
		scrollZone = new Rect(sw * 0.36f, sh * 0.24f, sw * 0.42f, sh * 0.6f);
		fullScrollZone = new Rect(0, 0, sw * 0.4f, sh);
		
		windowRect = new Rect(sw * 0.20f, sh * 0.15f, sw * 0.6f, sh * 0.73f);
		closeRect = new Rect(sw * 0.775f, sh * 0.13f, sw * 0.04f, sw * 0.04f);
		decorationRect = new Rect(sw * 0.22f, sh * 0.18f, sw * 0.46f, sh * 0.65f);
		
//		classRect = new Rect(sw * 0.01f, sh * 0.01f, sw * 0.34f, sh * 0.085f);
//		subclassRect = new Rect(sw * 0.01f, sh * 0.01f, sw * 0.24f, sh * 0.085f);
	}
	
	public void  InitData ()
	{
		NPC npc = WorldSession.player.target as NPC;
		auctioneer = npc.auctioneer;
		
		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		mcTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, (uint)(sh * 0.045f), 
		                                      Color.black, true, FontStyle.Bold);
	}
	
	public void  Show ()
	{		
		GUI.DrawTexture(auctioneer.backgroudRect, auctioneer.backgroundImage);
		auctioneer.auctionAtlas.DrawTexture(windowRect, "background.png");
		auctioneer.auctionAtlas.DrawTexture(decorationRect, "decorationBig.png");
		
		auctioneer.auctionAtlas.DrawTexture(closeRect, "btnClose.png");
		if(GUI.Button(closeRect, "", GUIStyle.none))
		{
			CloseWindow();
		}
		
		scrollPosition = GUI.BeginScrollView(scrollZone, scrollPosition, fullScrollZone, false, true);
		DrawButtons();
		GUI.EndScrollView();
	}
	
	private void  DrawButtons ()
	{
		for(byte index = 0; index < 10; index++)
		{
			DrawClassButtons(index);
		}
	}
	
	private void  DrawClassButtons ( byte index )
	{
		int padding;
		
		if(index > selectedItemClass)
		{
			padding = (int)((index + subgroupIndex) * (sh * 0.1f));
		}
		else
		{
			padding = (int)(index * (sh * 0.1f));
		}
		
		Rect positionRect = new Rect(sw * 0.01f, sh * 0.01f + padding, sw * 0.34f, sh * 0.085f);
		switch(index)
		{
		case 0:
		case 3:
		case 9:
		case 10:
			DrawItemClassButton(positionRect, itemClassName[index], false, index);
			break;
		default:
			DrawItemClassButton(positionRect, itemClassName[index], true, index);
			break;
		}
	}
	
	private void  DrawItemClassButton ( Rect positionRect ,   string buttonText ,   bool haveSubClass ,    byte index )
	{
		GUIAtlas.kitchenSinkSheet.DrawTexture(positionRect, "btn_1_normal (2).png");
		if(haveSubClass)
		{			
			if(GUI.Button(positionRect, buttonText, mcTextStyle))
			{
				CheckSelectedClass(index, buttonText);
			}
			if(selectedItemClass == index)
			{
				ChangeScrollZoneSize(index);
				DrawSubClassButtons();
			}
		}
		else
		{
			if(GUI.Button(positionRect, buttonText, mcTextStyle))
			{
				CheckSelectedClass(index, buttonText);
				subgroupIndex = 0;
				fullScrollZone = new Rect(0, 0, sw * 0.49f, sh);
				CloseWindow();
			}
		}
	}
	private void  ChangeScrollZoneSize ( byte index )
	{
		switch(index)
		{
		case 1:	// Weapon
			subgroupIndex = (byte)weaponClassName.Length;
			fullScrollZone = new Rect(0, 0, sw * 0.4f, sh * 2.5f);
			break;
		case 2:	// Armour
			subgroupIndex = (byte)armourClassName.Length;
			fullScrollZone = new Rect(0, 0, sw * 0.4f, sh * 1.7f);
			break;
		case 4:	// Consumable
			subgroupIndex = (byte)consumableClassName.Length;
			fullScrollZone = new Rect(0, 0, sw * 0.4f, sh * 1.6f);
			break;
		case 5:	// Trade Goods
			subgroupIndex = (byte)tradegoodsClassName.Length;
			fullScrollZone = new Rect(0, 0, sw * 0.4f, sh * 2.2f);
			break;
		case 6:	// Projectiles
			subgroupIndex = (byte)projectileClassName.Length;
			fullScrollZone = new Rect(0, 0, sw * 0.4f, sh * 1.2f);
			break;
		case 7:	// Quiver and Ammo bags
			subgroupIndex = (byte)quiverClassName.Length;
			fullScrollZone = new Rect(0, 0, sw * 0.4f, sh * 1.2f);
			break;
		case 8:	// Recipe
			subgroupIndex = (byte)recipeClassName.Length;
			fullScrollZone = new Rect(0, 0, sw * 0.4f, sh * 1.8f);
			break;
		}
	}
	
	private void  CheckSelectedClass ( byte index ,   string text )
	{		
		if(selectedItemClass != index)
		{
			selectedItemClass = index;
			itemTypeText = text;
			selectedItemSubClass = 255;
		}
		else
		{
			selectedItemClass = 255;
			subgroupIndex = 0;
			fullScrollZone = new Rect(0, 0, sw * 0.4f, sh);
			itemTypeText = "All";
		}
	}
	
	private void  DrawSubClassButtons ()
	{
		byte i = 0;
		switch(selectedItemClass)
		{
		case 1:	// Weapon SubClass elements
			for(i = 0; i < weaponClassName.Length; i++)
			{
				DrawItemSubClassButton(weaponClassName[i], i, weaponClassID[i]);
			}
			break;
		case 2:	// Armour SubClass elements
			for(i = 0; i < armourClassName.Length; i++)
			{
				DrawItemSubClassButton(armourClassName[i], i, armourClassID[i]);
			}
			break;
		case 4:	// Consumable SubClass elements
			for(i = 0; i < consumableClassName.Length; i++)
			{
				DrawItemSubClassButton(consumableClassName[i], i, consumableClassID[i]);
			}
			break;
		case 5:	// Trade Goods SubClass elements
			for(i = 0; i < tradegoodsClassName.Length; i++)
			{
				DrawItemSubClassButton(tradegoodsClassName[i], i, tradegoodsClassID[i]);
			}
			break;
		case 6:	// Projectile SubClass elements
			for(i = 0; i < projectileClassName.Length; i++)
			{
				DrawItemSubClassButton(projectileClassName[i], i, projectileClassID[i]);
			}
			break;
		case 7:	// Quiver SubClass elements
			for(i = 0; i < quiverClassName.Length; i++)
			{
				DrawItemSubClassButton(quiverClassName[i], i, quiverClassID[i]);
			}
			break;
		case 8:	// Recipe SubClass elements
			for(i = 0; i < recipeClassName.Length; i++)
			{
				DrawItemSubClassButton(recipeClassName[i], i, recipeClassID[i]);
			}
			break;
		}
	}
	
	private void  DrawItemSubClassButton ( string subClassText ,   byte index ,   int originalIndex )
	{
		uint padding = (uint)((selectedItemClass + index + 1) * (sh * 0.1f));
		Rect positionRect = new Rect(sw * 0.04f, sh * 0.01f + padding, sw * 0.34f, sh * 0.085f);
		GUIAtlas.kitchenSinkSheet.DrawTexture(positionRect, "btn_1_normal (2).png");
		if(GUI.Button(positionRect, subClassText, mcTextStyle))
		{
			CheckSelectedSubClass(originalIndex, subClassText);
		}
	}
	
	private void  CheckSelectedSubClass ( int index ,   string text )
	{
		selectedItemSubClass = (byte)index;
		itemTypeText = itemTypeText + "-" + text;
		
		CloseWindow();
	}
	
	private void  CloseWindow ()
	{
		if(selectedItemClass != 255)
		{
			auctioneer.auctionManager.filterByAuctionMainCategory = (uint)itemClassID[selectedItemClass];
		}
		else
		{
			auctioneer.auctionManager.filterByAuctionMainCategory = 0xffffffff;
		}
		
		if(selectedItemSubClass != 255)
		{
			auctioneer.auctionManager.filterByAuctionSubCategory = selectedItemSubClass;
		}
		else
		{
			auctioneer.auctionManager.filterByAuctionSubCategory = 0xffffffff;
		}
		
		selectedItemClass = 255;
		selectedItemSubClass = 255;
		
		auctioneer.auctionManager.itemClassText = itemTypeText;
		itemTypeText = "All";
		auctioneer.ChangeState(AuctionWindiwState.AUCTION_SEARCH_WINDOW);
	}
}