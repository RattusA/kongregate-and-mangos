﻿using UnityEngine;

public class AuctionSearchResultWindow
{
	protected Auctioneer auctioneer;
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private TexturePacker mix1Atlas;
	private TexturePacker mix7Atlas;
	
	private Vector2 scrollPosition = Vector2.zero;
	private Rect scrollZone;
	private Rect fullScrollZone;
	private Rect decor1;
	private Rect decor2;
	private Rect decor3;
	private Rect decor4;
	private Rect decor5;
	private Rect decor6;
	private Rect decor7;
	private Rect decor8;
	private Rect decor9;
	private uint padding;
	
	private GUIStyle mcTextStyle;
	private GUIStyle mlWhiteTextStyle;
	private GUIStyle mlBlackTextStyle;

	public AuctionSearchResultWindow()
	{
		scrollZone = new Rect(sw * 0.0125f, sh * 0.4167f, sw * 0.975f, sh * 0.55f);
		fullScrollZone = new Rect(0, 0, sw * 0.95f, sh * 0.5167f);
		decor1 = new Rect(sw * 0.0125f, sh * 0.0167f, sw * 0.1500f, sh * 0.2000f);
		decor2 = new Rect(sw * 0.1875f, sh * 0.0167f, sw * 0.1000f, sh * 0.1083f);
		decor3 = new Rect(sw * 0.3125f, sh * 0.0167f, sw * 0.4125f, sh * 0.0917f);
		decor4 = new Rect(sw * 0.3125f, sh * 0.1250f, sw * 0.4125f, sh * 0.0917f);
		decor5 = new Rect(sw * 0.7375f, sh * 0.0167f, sw * 0.2000f, sh * 0.0917f);
		decor6 = new Rect(sw * 0.7375f, sh * 0.1250f, sw * 0.2000f, sh * 0.0917f);
		decor7 = new Rect(sw * 0.0125f, sh * 0.2500f, sw * 0.0813f, sh * 0.1000f);
		decor8 = new Rect(sw * 0.9063f, sh * 0.2500f, sw * 0.0813f, sh * 0.1000f);
		decor9 = new Rect(sw * 0.0125f, sh * 0.3833f, sw * 0.9750f, sh * 0.0333f);
		padding = (uint)(sh * 0.3f);
	}
	
	public void  InitData ()
	{
		NPC npc = WorldSession.player.target as NPC;
		auctioneer = npc.auctioneer;
		
		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		mcTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, sh * 0.06f, 
		                                      Color.black, true, FontStyle.Bold);
		mlWhiteTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.05f, 
		                                           Color.white, true, FontStyle.Bold);
		mlBlackTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, sh * 0.05f, 
		                                           Color.black, true, FontStyle.Bold);
		
		mix1Atlas = new TexturePacker("Mix1");
		mix7Atlas = new TexturePacker("Mix7");
	}
	
	public void  Show ()
	{		
		string windowName = "";
		switch(auctioneer.windowResultState)
		{
		case 0:
			windowName = "Browse listings";
			break;
		case 1:
			windowName = "My bids";
			break;
		case 2:
			windowName = "My Listings";
			break;
		}
		
		GUI.DrawTexture(auctioneer.backgroudRect, auctioneer.backgroundImage);
		
		auctioneer.auctionAtlas.DrawTexture(decor1, "decorationSmall.png");
		auctioneer.auctionAtlas.DrawTexture(decor2, "btnBack.png");
		if(GUI.Button(decor2, "", GUIStyle.none))
		{
			UnloadResources();
			auctioneer.ChangeState(AuctionWindiwState.AUCTION_MAIN_WINDOW);
		}
		
		auctioneer.auctionAtlas.DrawTexture(decor3, "selectTopMenu.png");
		GUI.Label(decor3, windowName, mcTextStyle);
		
		auctioneer.auctionAtlas.DrawTexture(decor4, "selectTopMenu.png");
		GUI.Label(decor4, "All", mcTextStyle);
		
		auctioneer.auctionAtlas.DrawTexture(decor5, "btnSearch.png");
		if(GUI.Button(decor5, "", GUIStyle.none))
		{
			auctioneer.ChangeState(AuctionWindiwState.AUCTION_SEARCH_WINDOW);
			UnloadResources();
		}
		
		auctioneer.auctionAtlas.DrawTexture(decor6, "btnRefresh.png");
		if(GUI.Button(decor6, "", GUIStyle.none))
			UpdateWindowInfo();
		
		auctioneer.auctionAtlas.DrawTexture(decor9, "decor6.png");
		
		scrollPosition = GUI.BeginScrollView(scrollZone, scrollPosition, fullScrollZone, false, false);
		DrawSearchResults();
		GUI.EndScrollView();
	}
	
	void  DrawSearchResults ()
	{
		uint itemsCount = auctioneer.auctionManager.GetItemListCount();
		fullScrollZone = new Rect(0, 0, sw * 0.95f, padding * itemsCount + sh * 0.0333f);
		
		for(byte i = 0; i < itemsCount; i++)
			DrawItemLotInfo(i);
	}
	
	void  DrawItemLotInfo ( byte position )
	{
		AuctionItem auctionItem = auctioneer.auctionManager.GetItemByIndex(position);
		uint entry = auctionItem.itemId;
		Item item = AppCache.sItems.GetRecord(entry);
		
		uint pdd = position * padding;
		Rect decor11 = new Rect(0, sh * 0.05f + pdd, sw * 0.1125f, sw * 0.1125f);
		Rect decor12 = new Rect(0, sh * 0.2133f + pdd, sw * 0.975f, sh * 0.0833f);
		Rect decor13 = new Rect(sw * 0.6375f, sh * 0.05f + pdd, sw * 0.3125f, sh * 0.2333f);
		Rect decor14 = new Rect(sw * 0.005f, sh * 0.0633f + pdd, sw * 0.095f, sw * 0.1f);
		if(GUI.Button(decor13, "", GUIStyle.none))
		{
			auctioneer.auctionManager.selectedLot = auctionItem;
			auctioneer.ChangeState(AuctionWindiwState.AUCTION_ITEM_INFO_WINDOW);
			UnloadResources();
		}		
		
		DrawItemIcon(item, decor14);
		auctioneer.auctionAtlas.DrawTexture(decor11, "itemDecoration.png");
		auctioneer.auctionAtlas.DrawTexture(decor12, "decor7.png");
		auctioneer.auctionAtlas.DrawTexture(decor13, "decor8.png");
		
		Rect label1 = new Rect(sw * 0.14f, sh * 0.01f + pdd, sw * 0.6f, sh * 0.07f);
		Rect label2 = new Rect(sw * 0.14f, sh * 0.08f + pdd, sw * 0.6f, sh * 0.07f);
		Rect label3 = new Rect(sw * 0.14f, sh * 0.15f + pdd, sw * 0.6f, sh * 0.07f);
		Rect label4 = new Rect(sw * 0.14f, sh * 0.22f + pdd, sw * 0.6f, sh * 0.07f);
		
		Rect label5 = new Rect(sw * 0.66f, sh * 0.06f + pdd, sw * 0.26f, sh * 0.055f);
		Rect label6 = new Rect(sw * 0.66f, sh * 0.11f + pdd, sw * 0.26f, sh * 0.055f);
		Rect label7 = new Rect(sw * 0.66f, sh * 0.16f + pdd, sw * 0.26f, sh * 0.055f);
		Rect label8 = new Rect(sw * 0.66f, sh * 0.21f + pdd, sw * 0.26f, sh * 0.055f);
		
		GUI.Label(label1, item.name, mlWhiteTextStyle);
		GUI.Label(label2, "Time Left: " + Global.GetTimeAsString(auctionItem.timeLeft / 1000, false), mlWhiteTextStyle);
		GUI.Label(label3, "Count: " + auctionItem.itemCount, mlWhiteTextStyle);
		GUI.Label(label4, "Min. level: " + item.reqLvl, mlWhiteTextStyle);
		
		
		GUI.Label(label5, "Active bid:", mlBlackTextStyle);
		GUI.Label(label6, "" + auctionItem.currentBid, mlBlackTextStyle);
		GUI.Label(label7, "Buy it Now:", mlBlackTextStyle);
		GUI.Label(label8, "" + auctionItem.buyout, mlBlackTextStyle);
	}
	
	private void  UpdateWindowInfo ()
	{
		switch(auctioneer.windowResultState)
		{
		case 0:
			auctioneer.auctionManager.SearchedAndSortedListItemsRequest();
			break;
		case 1:
			auctioneer.auctionManager.RequestAuctionListOfBidderItems();
			break;
		case 2:
			auctioneer.auctionManager.OwnLotsRequest();
			break;
		}
	}
	
	private void  DrawItemIcon ( Item item ,   Rect itemIcon )
	{
		string iconName = DefaultIcons.GetItemIcon(item);
		if(mix1Atlas.IsAtlasHaveTexture(iconName))
		{
			mix1Atlas.DrawTexture(itemIcon, iconName);
		}
		else
		{
			mix7Atlas.DrawTexture(itemIcon, iconName);
		}
	}
	
	private void  UnloadResources ()
	{
		mix1Atlas.Destroy();
		mix1Atlas = null;
		mix7Atlas.Destroy();
		mix7Atlas = null;
	}
}