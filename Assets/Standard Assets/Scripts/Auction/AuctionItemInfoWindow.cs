﻿using UnityEngine;

public class AuctionItemInfoWindow
{
	protected Auctioneer auctioneer;
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private GUIStyle style;
	
	private Vector2 scrollPosition = Vector2.zero;
	private Rect scrollZone;
	private Rect fullScrollZone;
	
	private Rect decor1;
	private Rect decor2;
	private Rect decor3;

	private Rect decor5;
	private Rect decor8;
	private Rect decor9;

	private Rect label1;

	private Rect label5;
	private Rect label6;
	private Rect label7;
	private Rect label8;

	private Rect label9;
	private string newBid = "0";

	public AuctionItemInfoWindow()
	{
		scrollZone = new Rect(sw * 0.013f, sh * 0.21f, sw * 0.7f, sh * 0.62f);
		fullScrollZone = new Rect(0, 0, sw * 0.68f, sh * 0.59f);
		
		decor1 = new Rect(sw * 0.0125f, sh * 0.0167f, sw * 0.15f, sh * 0.2f);
		decor2 = new Rect(sw * 0.1875f, sh * 0.0167f, sw * 0.1f, sh * 0.1083f);
		decor3 = new Rect(sw * 0.3125f, sh * 0.0167f, sw * 0.4375f, sh * 0.0917f);
		
		decor5 = new Rect(sw * 0.0125f, sh * 0.2f, sw * 0.975f, sh * 0.64f);
		decor8 = new Rect(sw * 0.2250f, sh * 0.8667f, sw * 0.35f, sh * 0.1f);
		decor9 = new Rect(sw * 0.6250f, sh * 0.8667f, sw * 0.35f, sh * 0.1f);
		
		label1 = new Rect(sw * 0.02f, sh * 0.02f, sw * 0.65f, sh * 0.65f);
		
		label5 = new Rect(sw * 0.73f, sh * 0.23f, sw * 0.23f, sh * 0.055f);
		label6 = new Rect(sw * 0.73f, sh * 0.29f, sw * 0.23f, sh * 0.055f);
		label7 = new Rect(sw * 0.73f, sh * 0.35f, sw * 0.23f, sh * 0.055f);
		label8 = new Rect(sw * 0.73f, sh * 0.41f, sw * 0.23f, sh * 0.055f);
		
		label9 = new Rect(sw * 0.05f, sh * 0.8667f, sw * 0.17f, sh * 0.1f);
	}
	
	public void  InitData ()
	{
		NPC npc = WorldSession.player.target as NPC;
		auctioneer = npc.auctioneer;
		style = GUIStyle.none;
		
		uint price = auctioneer.auctionManager.selectedLot.currentBid + 1;
		newBid = "" + price;
	}
	
	public void  Show ()
	{		
		GUI.DrawTexture(auctioneer.backgroudRect, auctioneer.backgroundImage);
		
		auctioneer.auctionAtlas.DrawTexture(decor1, "decorationSmall.png");
		auctioneer.auctionAtlas.DrawTexture(decor2, "btnBack.png");
		if(GUI.Button(decor2, "", style))
			CloseLotWindow();
		
		auctioneer.auctionAtlas.DrawTexture(decor3, "selectTopMenu.png");
		GUI.Label(decor3, "Item info", style);
		
		auctioneer.auctionAtlas.DrawTexture(decor5, "decor8.png");
		
		//auctionAtlas.DrawTexture(decor8, "btnBuyItNow.png");
		//if(GUI.Button(decor2, "", style))
		//	auctioneer.ChangeState(7);
		
		
		if(auctioneer.windowResultState == 2)
		{	
			auctioneer.auctionAtlas.DrawTexture(decor9, "btnEmpty.png");
			GUI.Label(decor9, "Cancel Lot", style);
			if(GUI.Button(decor9, "", style))
				CancelLot();
		}
		else
		{	
			auctioneer.auctionAtlas.DrawTexture(decor9, "btnBid.png");
			if(GUI.Button(decor9, "", style))
				BidItem();
			
			GUI.Label(label9, "New Bid:", style);
			auctioneer.auctionAtlas.DrawTexture(decor8, "selectTopMenu.png");		
			newBid = GUI.TextArea(decor8, newBid, style);
			
		}
		
		Item item = AppCache.sItems.GetRecord(auctioneer.auctionManager.selectedLot.itemId);
		
		scrollPosition = GUI.BeginScrollView(scrollZone, scrollPosition, fullScrollZone, false, false);
		GUI.Label(label1, item.GetItemBasicStats(), style);
		GUI.EndScrollView();
		
		GUI.Label(label5, "Active bid:", style);
		GUI.Label(label6, "" + auctioneer.auctionManager.selectedLot.currentBid, style);
		
		GUI.Label(label7, "Buy it Now:", style);
		GUI.Label(label8, "" + auctioneer.auctionManager.selectedLot.buyout, style);
	}
	
	private void  BidItem ()
	{
		auctioneer.auctionManager.newBid = System.Convert.ToUInt32(newBid);
		auctioneer.auctionManager.PlaceBidRequest();
		auctioneer.auctionManager.SearchedAndSortedListItemsRequest();
	}
	
	private void  CancelLot ()
	{
		auctioneer.auctionManager.CancelLot();
		auctioneer.auctionManager.SearchedAndSortedListItemsRequest();
	}
	
	private void  CloseLotWindow ()
	{
		auctioneer.ChangeState(AuctionWindiwState.AUCTION_SEARCH_RESULT_WINDOW);
	}
}