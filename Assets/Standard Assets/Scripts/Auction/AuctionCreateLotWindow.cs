﻿using UnityEngine;

public class AuctionCreateLotWindow
{
	protected Auctioneer auctioneer;
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private TexturePacker mix1Atlas;
	private TexturePacker mix7Atlas;
	
	private Rect connerDecorationRect;
	
	private Rect btnBackRect;
	private Rect btnBackRect2;
	private Rect itemNameRect;
	
	private Rect itemDecoration;
	private Rect itemIcon;
	private Rect itemDecorationLine;
	private Rect topMenuRect;
	
	private Rect numberOfBundlesRect;
	private Rect itemsInBundleRect;
	
	private Rect stackCountRect;
	private Rect itemsInStackRect;
	private uint stackCount = 1;
	private uint itemsInBundle = 1;
	
	private Rect maxStackRect;
	private Rect maxItemsRect;
	
	private Rect listingDurationTextRect;
	private Rect buyItNowPriceTextRect;
	private Rect startingPriceTextRect;
	private Rect retainerTextRect;
	
	private Rect listingDurationRect;
	private Rect buyItNowPriceRect;
	private Rect startingPriceRect;
	private Rect retainerRect;
	
	private string buyItNowPriceText = "0";
	private string startingPriceText = "100";
	private string retainerText = "0";
	
	private uint startPrice;
	private uint buyItNowPrice;
	private uint retainerPrice;
	
	private bool  initItemInfo = false;
	private string iconName;
	private Item item;
	
	private GUIStyle mcTextStyle;
	private GUIStyle mlWhiteStyle;
	private GUIStyle mrWhiteStyle;
	
	private Rect clearAllRect;
	private Rect approveRect;

	public AuctionCreateLotWindow()
	{
		connerDecorationRect = new Rect(sw * 0.0125f, sh * 0.01667f, sw * 0.2625f, sh * 0.4667f);
		
		btnBackRect = new Rect(sw * 0.2875f, sh * 0.01667f, sw * 0.0875f, sh * 0.1f);
		btnBackRect2 = new Rect(sw * 0.625f, sh * 0.85f, sw * 0.0875f, sh * 0.1f); 		//sw * 0.375f, sh * 0.18f
		itemNameRect = new Rect(sw * 0.25f, sh * 0.2083f, sw * 0.625f, sh * 0.125f);
		
		itemDecoration = new Rect(sw * 0.125f, sh * 0.1667f, sw * 0.1125f, sw * 0.1125f);
		itemIcon = new Rect(sw * 0.13f, sh * 0.18f, sw * 0.095f, sw * 0.1f);
		itemDecorationLine = new Rect(sw * 0.05f, sh * 0.3183f, sw * 0.9375f, sh * 0.05f);
		topMenuRect = new Rect(sw * 0.4f, sh * 0.01667f, sw * 0.475f, sh * 0.0917f);
		
		numberOfBundlesRect = new Rect(sw * 0.1f, sh * 0.34f, sw * 0.4f, sh * 0.08f);
		itemsInBundleRect = new Rect(sw * 0.65f, sh * 0.34f, sw * 0.4f, sh * 0.08f);
		
		stackCountRect = new Rect(sw * 0.1125f, sh * 0.4167f, sw * 0.125f, sh * 0.1167f);
		itemsInStackRect = new Rect(sw * 0.65f, sh * 0.4167f, sw * 0.125f, sh * 0.1167f);

		maxStackRect = new Rect(sw * 0.2625f, sh * 0.4167f, sw * 0.1875f, sh * 0.1167f);
		maxItemsRect = new Rect(sw * 0.8f, sh * 0.4167f, sw * 0.1875f, sh * 0.1167f);
		
		listingDurationTextRect = new Rect(sw * 0.0125f, sh * 0.5667f, sw * 0.375f, sh * 0.0833f);
		buyItNowPriceTextRect = new Rect(sw * 0.0125f, sh * 0.6667f, sw * 0.375f, sh * 0.0833f);
		startingPriceTextRect = new Rect(sw * 0.0125f, sh * 0.7667f, sw * 0.375f, sh * 0.0833f);
		retainerTextRect = new Rect(sw * 0.0125f, sh * 0.8667f, sw * 0.375f, sh * 0.0833f);
		
		listingDurationRect = new Rect(sw * 0.4f, sh * 0.5667f, sw * 0.3375f, sh * 0.0833f);
		buyItNowPriceRect = new Rect(sw * 0.4f, sh * 0.6667f, sw * 0.3375f, sh * 0.0833f);
		startingPriceRect = new Rect(sw * 0.4f, sh * 0.7667f, sw * 0.3375f, sh * 0.0833f);
		retainerRect = new Rect(sw * 0.4f, sh * 0.8667f, sw * 0.3375f, sh * 0.0833f);

		clearAllRect = new Rect(sw * 0.7625f, sh * 0.7667f, sw * 0.225f, sh * 0.0833f);
		approveRect = new Rect(sw * 0.7625f, sh * 0.8667f, sw * 0.225f, sh * 0.0833f);
	}
	
	public void  InitData ()
	{
		NPC npc = WorldSession.player.target as NPC;
		auctioneer = npc.auctioneer;
		
		Font font = Resources.Load<Font>("fonts/Fontin-iRegular PC");
		mcTextStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleCenter, (uint)(sh * 0.06f), 
		                                      Color.black, true, FontStyle.Bold);
		mlWhiteStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleLeft, (uint)(sh * 0.06f), 
		                                       Color.white, true, FontStyle.Bold);
		mrWhiteStyle = GUIAtlas.CreateGuiStyle(font, TextAnchor.MiddleRight, (uint)(sh * 0.06f), 
		                                       Color.white, true, FontStyle.Bold);
		
		mix1Atlas = new TexturePacker("Mix1");
		mix7Atlas = new TexturePacker("Mix7");
	}
	
	public void  Show ()
	{
		if(auctioneer.auctionManager.selectedItem == null)
		{
			auctioneer.auctionAtlas.DrawTexture(btnBackRect2, "btnBack.png");
			if(GUI.Button(btnBackRect2, "", GUIStyle.none))
			{
				UnloadResources();
				auctioneer.auctionManager.DestroyInventorySlots();				
				auctioneer.ChangeState(AuctionWindiwState.AUCTION_MAIN_WINDOW);
			}
			return;
		}
		
		if(!initItemInfo)
			iconName = DefaultIcons.GetItemIcon(auctioneer.auctionManager.selectedItem);
		
		startPrice = uint.Parse(startingPriceText);
		buyItNowPrice = uint.Parse(buyItNowPriceText);
		
		retainerPrice = (uint)(auctioneer.auctionManager.selectedItem.sellPrice * 0.75f * (int)auctioneer.auctionManager.lotTimeID);
		retainerText = retainerPrice.ToString();
		
		if(startPrice <= 0)
		{
			startPrice = 100;
			startingPriceText = startPrice.ToString();
		}
		
		if(buyItNowPrice != 0 && buyItNowPrice < startPrice)
		{
			buyItNowPrice = startPrice;
			retainerText = startPrice.ToString();
		}
		
		GUI.DrawTexture(auctioneer.backgroudRect, auctioneer.backgroundImage);
		
		auctioneer.auctionAtlas.DrawTexture(connerDecorationRect, "connerupDecorationMedium.png");
		auctioneer.auctionAtlas.DrawTexture(btnBackRect, "btnBack.png");
		if(GUI.Button(btnBackRect, "", GUIStyle.none))
		{
			UnloadResources();
			auctioneer.ChangeState(AuctionWindiwState.AUCTION_MAIN_WINDOW);
			return;
		}
		
		DrawItemIcon();
		auctioneer.auctionAtlas.DrawTexture(itemDecoration, "itemDecoration.png");
		
		GUI.Label(itemNameRect, auctioneer.auctionManager.selectedItem.name, mlWhiteStyle);
		
		auctioneer.auctionAtlas.DrawTexture(itemDecorationLine, "decorationMid.png");
		auctioneer.auctionAtlas.DrawTexture(topMenuRect, "selectTopMenu.png");
		GUI.Label(topMenuRect, "Create listing", mcTextStyle);
		
		if(auctioneer.auctionManager.selectedItem.stackable > 1)
		{
			Item item = AppCache.sItems.GetRecord(auctioneer.auctionManager.selectedItem.entry);
			
			int maxItemsInStack = item.stackable;
			uint maxItems;
			
			if(maxItemsInStack <= 1)
			{
				maxItems = 1;
			}
			else
			{
				maxItems = auctioneer.auctionManager.GetCountByItem(item);
			}
			
			GUI.Label(numberOfBundlesRect, "Number of Bundles:", mlWhiteStyle);		
			auctioneer.auctionAtlas.DrawTexture(stackCountRect, "countImage.png");
			stackCount = System.Convert.ToUInt32(GUI.TextArea(stackCountRect, stackCount.ToString(), mcTextStyle));
			if(stackCount == 0)
				stackCount = 1;
			
			auctioneer.auctionAtlas.DrawTexture(maxStackRect, "btnMax.png");
			if(GUI.Button(maxStackRect, "", GUIStyle.none))
			{
				stackCount = maxItems/itemsInBundle;
			}
			if(stackCount > 999)
				stackCount = 999;	
			
			
			
			GUI.Label(itemsInBundleRect, "Items in Bundle:", mlWhiteStyle);
			auctioneer.auctionAtlas.DrawTexture(itemsInStackRect, "countImage.png");
			itemsInBundle = System.Convert.ToUInt32(GUI.TextArea(itemsInStackRect, itemsInBundle.ToString(), mcTextStyle));
			if(itemsInBundle == 0)
				itemsInBundle = 1;			
			
			auctioneer.auctionAtlas.DrawTexture(maxItemsRect, "btnMax.png");
			if(GUI.Button(maxItemsRect, "", GUIStyle.none))
			{
				itemsInBundle = 1;
				itemsInBundle = (uint)maxItemsInStack;
			}
			if(itemsInBundle > 999)
				itemsInBundle = 999;
		}
		else
		{
			stackCount = 1;
			itemsInBundle = 1;
		}
		
		GUI.Label(listingDurationTextRect, "Listing duration:", mrWhiteStyle);
		GUI.Label(buyItNowPriceTextRect, "Buy It Now Price:", mrWhiteStyle);
		GUI.Label(startingPriceTextRect, "Starting price:", mrWhiteStyle);
		GUI.Label(retainerTextRect, "Retainer:", mrWhiteStyle);
		
		auctioneer.auctionAtlas.DrawTexture(listingDurationRect, "btnEmpty.png");
		auctioneer.auctionAtlas.DrawTexture(buyItNowPriceRect, "btnEmpty.png");
		auctioneer.auctionAtlas.DrawTexture(startingPriceRect, "btnEmpty.png");
		auctioneer.auctionAtlas.DrawTexture(retainerRect, "btnEmpty.png");
		
		if(GUI.Button(listingDurationRect, auctioneer.auctionManager.GetLotTimeStr(), mcTextStyle))
		{
			auctioneer.auctionManager.ChangeTime();
		}
		
		buyItNowPriceText = GUI.TextArea(buyItNowPriceRect, buyItNowPriceText, mcTextStyle);
		startingPriceText = GUI.TextArea(startingPriceRect, startingPriceText, mcTextStyle);
		retainerText = GUI.TextArea(retainerRect, retainerText, mcTextStyle);
		
		auctioneer.auctionAtlas.DrawTexture(clearAllRect, "btnClearAll.png");
		auctioneer.auctionAtlas.DrawTexture(approveRect, "btnApprove.png");
		if(GUI.Button(approveRect, "", GUIStyle.none))
		{
			UnloadResources();
			CreateLot();
		}
	}
	
	private void  CreateLot ()
	{
		auctioneer.auctionManager.startCost = startPrice;
		auctioneer.auctionManager.finishCost = buyItNowPrice;
		auctioneer.auctionManager.bundlesCount = itemsInBundle;
		auctioneer.auctionManager.stackSize = stackCount;
		auctioneer.auctionManager.lotTime = auctioneer.auctionManager.GetLotTime();
		
		auctioneer.auctionManager.SellItem();
		auctioneer.auctionManager.OwnLotsRequest();
		auctioneer.ChangeState(AuctionWindiwState.AUCTION_MAIN_WINDOW);
	}
	
	private void  DrawItemIcon ()
	{
		if(mix1Atlas.IsAtlasHaveTexture(iconName))
		{
			mix1Atlas.DrawTexture(itemIcon, iconName);
		}
		else
		{
			mix7Atlas.DrawTexture(itemIcon, iconName);
		}
	}
	
	private void  UnloadResources ()
	{
		mix1Atlas.Destroy();
		mix1Atlas = null;
		mix7Atlas.Destroy();
		mix7Atlas = null;
	}
}