﻿using UnityEngine;

public class AuctionItemQualityWindow
{
	protected Auctioneer auctioneer;
	protected int sw = Screen.width;
	protected int sh = Screen.height;
	
	private Rect windowRect;
	private Rect closeRect;
	private Rect decorationRect;
	
	private Rect allRect;
	private Rect inferiorRect;
	private Rect normalRect;
	private Rect rareRect;
	private Rect epicRect;
	private Rect legendaryRect;

	public AuctionItemQualityWindow()
	{
		windowRect = new Rect(sw * 0.20f, sh * 0.15f, sw * 0.6f, sh * 0.73f);
		closeRect = new Rect(sw * 0.775f, sh * 0.13f, sw * 0.04f, sw * 0.04f);
		decorationRect = new Rect(sw * 0.22f, sh * 0.18f, sw * 0.46f, sh * 0.65f);
		
		allRect = new Rect(sw * 0.37f, sh * 0.25f, sw * 0.34f, sh * 0.085f);
		inferiorRect = new Rect(sw * 0.37f, sh * 0.35f, sw * 0.34f, sh * 0.085f);
		normalRect = new Rect(sw * 0.37f, sh * 0.45f, sw * 0.34f, sh * 0.085f);
		rareRect = new Rect(sw * 0.37f, sh * 0.55f, sw * 0.34f, sh * 0.085f);
		epicRect = new Rect(sw * 0.37f, sh * 0.65f, sw * 0.34f, sh * 0.085f);
		legendaryRect = new Rect(sw * 0.37f, sh * 0.75f, sw * 0.34f, sh * 0.085f);
	}
	
	public void  InitData ()
	{
		NPC npc = WorldSession.player.target as NPC;
		auctioneer = npc.auctioneer;
	}
	
	public void  Show ()
	{		
		GUI.DrawTexture(auctioneer.backgroudRect, auctioneer.backgroundImage);
		auctioneer.auctionAtlas.DrawTexture(windowRect, "background.png");
		auctioneer.auctionAtlas.DrawTexture(decorationRect, "decorationBig.png");
		
		auctioneer.auctionAtlas.DrawTexture(closeRect, "btnClose.png");
		if(GUI.Button(closeRect, "", GUIStyle.none))
		{
			CloseWindow();
		}
		
		auctioneer.auctionAtlas.DrawTexture(allRect, "btnAll.png");
		if(GUI.Button(allRect, "", GUIStyle.none))
		{
			ChangeQuality(ItemQualities.ITEM_QUALITY_ALL);
		}
		
		auctioneer.auctionAtlas.DrawTexture(inferiorRect, "btnInferior.png");
		if(GUI.Button(inferiorRect, "", GUIStyle.none))
		{
			ChangeQuality(ItemQualities.ITEM_QUALITY_POOR);
		}
		
		auctioneer.auctionAtlas.DrawTexture(normalRect, "btnNormal.png");
		if(GUI.Button(normalRect, "", GUIStyle.none))
		{
			ChangeQuality(ItemQualities.ITEM_QUALITY_NORMAL);
		}
		
		auctioneer.auctionAtlas.DrawTexture(rareRect, "btnRare.png");
		if(GUI.Button(rareRect, "", GUIStyle.none))
		{
			ChangeQuality(ItemQualities.ITEM_QUALITY_UNCOMMON);
		}
		
		auctioneer.auctionAtlas.DrawTexture(epicRect, "btnEpic.png");
		if(GUI.Button(epicRect, "", GUIStyle.none))
		{
			ChangeQuality(ItemQualities.ITEM_QUALITY_RARE);
		}
		
		auctioneer.auctionAtlas.DrawTexture(legendaryRect, "btnLegendary.png");
		if(GUI.Button(legendaryRect, "", GUIStyle.none))
		{
			ChangeQuality(ItemQualities.ITEM_QUALITY_EPIC);
		}
	}
	
	private void  CloseWindow ()
	{
		auctioneer.ChangeState(AuctionWindiwState.AUCTION_SEARCH_WINDOW);
	}
	
	private void  ChangeQuality ( ItemQualities quality )
	{
		uint _quality = (uint)quality;
		if(_quality == 255)
		{
			auctioneer.auctionManager.filterByQuality = 0xffffffff;
		}
		else
		{
			auctioneer.auctionManager.filterByQuality = _quality;
		}
		
		auctioneer.auctionManager.filterByQualityText = ItemFlags.GetQualityName(quality);
		auctioneer.ChangeState(AuctionWindiwState.AUCTION_SEARCH_WINDOW);
	}
}