﻿using UnityEngine;

public enum AuctionWindiwState
{
	AUCTION_MAIN_WINDOW				= 1,
	AUCTION_SEARCH_WINDOW			= 4,
	AUCTION_ITEM_QUALITY_WINDOW		= 5,
	AUCTION_ITEM_TYPE_WINDOW		= 6,
	AUCTION_SEARCH_RESULT_WINDOW	= 7,
	AUCTION_CREATE_LOT_WINDOW		= 8,
	AUCTION_ITEM_INFO_WINDOW		= 9,
	AUCTION_EXIT					= 0,
}

public class Auctioneer
{
	private NPC npc;
	public AuctionManager auctionManager;
	
	private AuctionMainWindow auctionMainWindow = new AuctionMainWindow();
	private AuctionSearchWindow auctionSearchWindow = new AuctionSearchWindow();
	private AuctionItemQualityWindow auctionItemQualityWindow = new AuctionItemQualityWindow();
	private AuctionItemTypeWindow auctionItemTypeWindow = new AuctionItemTypeWindow();
	private AuctionSearchResultWindow auctionSearchResultWindow = new AuctionSearchResultWindow();
	private AuctionCreateLotWindow auctionCreateLotWindow = new AuctionCreateLotWindow();
	private AuctionItemInfoWindow auctionItemInfoWindow = new AuctionItemInfoWindow();
	
	public Rect backgroudRect = new Rect(0, 0, Screen.width, Screen.height);
	public Texture2D backgroundImage;
	public TexturePacker auctionAtlas;
	
	private AuctionWindiwState windowState = 0;
	public byte windowResultState = 0;
	
	public Auctioneer ( NPC auctioneerNPC )
	{
		npc = auctioneerNPC;
		auctionManager = new AuctionManager();
		GetAuctioneerInfo();
		
		backgroundImage = Resources.Load<Texture2D>("GUI/InGame/basic_background");
		auctionAtlas = new TexturePacker("Auction");
	}
	
	public void  ChangeState ( AuctionWindiwState newState )
	{
		windowState = newState;
		switch(windowState)
		{
		case AuctionWindiwState.AUCTION_MAIN_WINDOW:
			auctionMainWindow.InitData();
			break;
		case AuctionWindiwState.AUCTION_SEARCH_WINDOW:
			auctionSearchWindow.InitData();
			break;
		case AuctionWindiwState.AUCTION_ITEM_QUALITY_WINDOW:
			auctionItemQualityWindow.InitData();
			break;
		case AuctionWindiwState.AUCTION_ITEM_TYPE_WINDOW:
			auctionItemTypeWindow.InitData();
			break;
		case AuctionWindiwState.AUCTION_SEARCH_RESULT_WINDOW:
			auctionSearchResultWindow.InitData();
			break;
		case AuctionWindiwState.AUCTION_CREATE_LOT_WINDOW:
			auctionCreateLotWindow.InitData();
			break;
		case AuctionWindiwState.AUCTION_ITEM_INFO_WINDOW:
			auctionItemInfoWindow.InitData();
			break;
		case AuctionWindiwState.AUCTION_EXIT:
			DestroyTexrures();
			break;
		}
	}
	
	public void  Display ()
	{
		switch(windowState)
		{
		case AuctionWindiwState.AUCTION_MAIN_WINDOW:
			auctionMainWindow.Show();
			break;
		case AuctionWindiwState.AUCTION_SEARCH_WINDOW:
			auctionSearchWindow.Show();
			break;
		case AuctionWindiwState.AUCTION_ITEM_QUALITY_WINDOW:
			auctionItemQualityWindow.Show();
			break;
		case AuctionWindiwState.AUCTION_ITEM_TYPE_WINDOW:
			auctionItemTypeWindow.Show();
			break;
		case AuctionWindiwState.AUCTION_SEARCH_RESULT_WINDOW:
			auctionSearchResultWindow.Show();
			break;
		case AuctionWindiwState.AUCTION_CREATE_LOT_WINDOW:
			auctionCreateLotWindow.Show();
			break;
		case AuctionWindiwState.AUCTION_ITEM_INFO_WINDOW:
			auctionItemInfoWindow.Show();
			break;
		}
	}
	
	private void  DestroyTexrures()
	{
		backgroundImage = null;
		auctionAtlas.Destroy();
		auctionAtlas = null;
		
		Resources.UnloadUnusedAssets();
	}
	
	/***************/
	/* Packet Data */
	/***************/
	
	// Send request for auctioneer info
	private void  GetAuctioneerInfo()
	{
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode(OpCodes.MSG_AUCTION_HELLO);
		pkt.Add(npc.guid);
		RealmSocket.outQueue.Add(pkt);
	}
	
	// Receive ansver from server
	public void  AuctioneerHelloResponse ( WorldPacket pkt)
	{
		ulong auctioneerGuid = pkt.ReadReversed64bit();
		uint auctionHouseId = pkt.ReadReversed32bit();
		byte constValue = pkt.Read();
		
		WorldSession.player.blockTargetChange = true;
		WorldSession.interfaceManager.hideInterface = true;
		WorldSession.player.interf.stateManager();
		
		auctionManager.auctioneerGuid = auctioneerGuid;
		auctionManager.StartAuction();
		ChangeState(AuctionWindiwState.AUCTION_MAIN_WINDOW);
	}
}