﻿
public enum AuctionLotTime
{
	SHORT_TIME		=	1,
	MEDIUM_TIME		=	2,
	LONG_TIME		=	4
}

public class AuctionItem
{
	public const byte ENCHANTMENT_COUNT = 7;
	public uint auctionId;
	public uint itemId;
	public Enchantment[] enchants = new Enchantment[ENCHANTMENT_COUNT];
	public uint itemRandomPropertyId;
	public uint suffixFactor;
	public uint itemCount;
	public uint itemCharge;
	public uint itemFlags = 0;
	public ulong ownerGuid;
	public uint startBid;
	public uint minimalOutBid;
	public uint buyout;
	public uint timeLeft;
	public ulong currentBidderGuid;
	public uint currentBid;
}

public class Enchantment
{
	public uint id;
	public uint duration;
	public uint charges;
}