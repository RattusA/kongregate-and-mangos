using UnityEngine;

public class AnimatedTexture : MonoBehaviour
{
	public bool HasAlpha = false;								//if the material uses alpha/is transparent
	public string PathToResources = "";							//the exact path in Resources/animated_textures/...
	public float AnimationSpeed = 0f;							//the speed at which we change the textures
	public float XTiling = 0f;
	public float YTiling = 0f;
	public bool PlayAnimationBackwards = false;

	//private vars, needed for 'animating' part	
	private string _pathToResources = "animated_textures/";		//default starting of path to resources
	[SerializeField]
	private Texture[] _textures;								//array of textures found in that path
	private Material _material;									//material of the object
	private float _timer;										//timer for countdown
	private int _currentImage = 0;								//index of current image from the texture array

	void Start ()
	{
		if(XTiling == 0)
		{
			XTiling = 1;
		}
		if(YTiling == 0)
		{
			YTiling = 1;
		}
		
		
		//we set the default path to resources if there is none set
		if(PathToResources == "")
		{
			PathToResources += "default";
		}
		_pathToResources += PathToResources;
		
		//we set animation speed to 0.1 if we have none set
		if(AnimationSpeed == 0)
		{
			AnimationSpeed = 0.075f;
		}
		
		_timer = 0f;
//		_textures = Resources.LoadAll<Texture>(_pathToResources);
		
		gameObject.GetComponent<Renderer>().material = HasAlpha ? new Material(Shader.Find("DifusePaintTransNoZ")) : new Material(Shader.Find("DifusePaint"));
		
		//setting material properties
		_material = gameObject.GetComponent<Renderer>().material;
		_material.mainTexture = _textures [0];
		_material.SetTextureScale("_MainTex", new Vector2(XTiling, YTiling));
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(_timer >= AnimationSpeed)
		{
			_timer = 0f;
			if(PlayAnimationBackwards == false)
			{
				_currentImage ++;
				if(_currentImage >= _textures.Length)
				{
					_currentImage = 0;
				}
			}
			else
			{
				_currentImage --;
				if( _currentImage < 0 )
				{
					_currentImage = _textures.Length-1;
				}
			}
			
			_material.mainTexture = _textures [_currentImage];
		}
		
		_timer += Time.deltaTime; 
	
	}
}
