using UnityEngine;
using System;
using System.Collections;

public class MusicManager : MonoBehaviour 
{
	//public variables
	public float PauseBetweenSongsInMinutes = 1.0f;/*0f;*/
	public float MusicVolume;
	public int MusicEnable;
	//for effects
	public static float EffectVolume;
	public static bool EffectsEnabled;
	
	//private variables
	private AudioSource _audioSource;
	private AudioClip _currentClip;
	private int _currentClipIndex;
//	private Object[] _clips;
	private float _timer;
	//for modified music manager (less memory usage)
	private string[] _clipNames = new string[]{
		"1 - TheBeginning33s",
		"2 - BattleHymn",
		"Catacombs1-08",
		"ChariotsOfThePast1-06",
		"DanceOfTheLost1-23",
		"DesertDwellers58s",
		"EchoesOfThePast49s",
		"LamentOfTheBattle1-36",
		"Lost30s",
		"LostInTheWorld1-13",
		"MarchingThoughts2-27",
		"MysticPast51s",
		"PartingStone1-22",
		"SacredTime1-01",
		"SongOfTheBanshee38s",
		"SongOfTheNorth1-16",
		"SpiritBells1-17",
		"StormIsComing9-1-05",
		"TimePassingBy38s",
		"TombOfHeroes1-03"};

	private Transform _mainPlayerTransform;
	private bool isAudioDownloaded = true;
	private string BaseDownloadingURL = string.Empty;

	void Start () 
	{
		try
		{
			GameObject player = GameObject.Find ("player");
			if(player != null)
				_mainPlayerTransform = player.transform;
		}
		catch (System.Exception e)
		{
			Debug.LogException(e);
		}

		Debug.Log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ \n we do the initialisations here \n^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");

		_currentClipIndex = 0;
		_timer = 0f;

		string relativePath = GetRelativePath();
		BaseDownloadingURL = WWW.UnEscapeURL(relativePath + "/");
		StartCoroutine(ChangeAudioClip());
	}

	void Update () 
	{
		//EnableDisableSound(WorldSession.player.Fog);

		try
		{
			if (_mainPlayerTransform != null)
			{
				transform.position = _mainPlayerTransform.position;
				transform.eulerAngles = _mainPlayerTransform.eulerAngles;
			}
			else
			{
				GameObject player = GameObject.Find ("player");
				if(player != null)
					_mainPlayerTransform = player.transform;
			}
		}
		catch (System.Exception e)
		{
			Debug.LogException(e);
		}
		
		if(!_audioSource.isPlaying)
		{
			_timer += Time.deltaTime;
		}
		
		if(_timer >= PauseBetweenSongsInMinutes/* * 60*/ && isAudioDownloaded)
		{
			StartCoroutine(ChangeAudioClip());
		}
		
		if(Application.isLoadingLevel && /*Application.loadedLevel != 0 && */isAudioDownloaded && !_audioSource.isPlaying)
		{
			//print("se incarca nivelul");
			StartCoroutine(ChangeAudioClip());
		}
	}

#region FUNCTIONS
	//enable disable soundFX
	public void EnableDisableSoundFX(bool val)
	{
		EffectsEnabled = val;
	}

	//this changes the music that is playing
	IEnumerator ChangeAudioClip()
	{
		isAudioDownloaded = false;
		_audioSource = gameObject.GetComponent<AudioSource>();
		#region ADD SOUND SOURCE AND LISTENER 
		if(_audioSource == null)
		{
			print("no audio source on this object, so we add one...");
			_audioSource = gameObject.AddComponent<AudioSource>();
		}
		if(!gameObject.GetComponent<AudioListener>())
		{
			gameObject.AddComponent<AudioListener>();
		}
		#endregion		
		
		if(_audioSource.enabled)
		{
			string path;
			//set a random range value here
			_currentClipIndex = UnityEngine.Random.Range(0, _clipNames.Length);
			_timer = 0;
			
			if(Application.loadedLevel == 0 || Application.loadedLevel == 4)
			{
				_currentClipIndex = 0;			
			}
			if(BuildConfig.platformName.Equals("streamed"))
			{
				path = BaseDownloadingURL + "Music/" + _clipNames[_currentClipIndex] + ".ogg";
				using(WWW download = new WWW(path))
				{
					yield return download;
					if (!string.IsNullOrEmpty(download.error))
					{
//						Debug.Log (string.Format ("Music loading has Error: url = {0}\n{1}", path, download.error));
					}
					else
					{
						_currentClip = download.GetAudioClip(false, true);
					}
				}
			}
			else
			{
				path = "Sounds/Music/" + _clipNames[_currentClipIndex];
				_currentClip = Resources.Load<AudioClip>(path);
				yield return true;
			}
			//setting the clip to play in the AudioSource
			_audioSource.clip = _currentClip;
			
			MusicVolume = PlayerPrefs.GetFloat("musicVolumeLevel", 0.8f);
			MusicEnable = PlayerPrefs.GetInt("musicButtonOption", 1);
			
			EffectVolume = PlayerPrefs.GetFloat("soundFXVolumeLevel", 1.0f);
			EffectsEnabled = System.Convert.ToBoolean(PlayerPrefs.GetInt("soundFXButtonOption", 1));
			
			if(MusicEnable != 0)
			{
				_audioSource.volume = MusicVolume;
			}
			else
			{
				_audioSource.volume = 0;
			}
			
			_audioSource.Play();
			isAudioDownloaded = true;
		}
	}

	//function to enable/disable sound
	public void EnableDisableSound(bool val)
	{		
		if(_audioSource.enabled && !val)
		{
			Debug.Log("DISABLE SOUND");
			_currentClip = null;
			_audioSource.clip = null;
			_audioSource.enabled = false;
			MusicVolume = 0;
		}
		else if (val)
		{
			Debug.Log("ENABLE SOUND");
			_audioSource.enabled = true;
			StartCoroutine(ChangeAudioClip());
		}
	}
	
	public void MusicVolumeChange(float val)
	{
		MusicVolume = val;
		_audioSource.volume = MusicVolume;
	}
	
	public void EffectVolumeChange(float val)
	{
		EffectVolume = val;
	}

	public string GetRelativePath()
	{
		if (Application.isEditor)
			return "file://" + Application.streamingAssetsPath.Replace("\\", "/"); // Use the build output folder directly.
		else if (Application.isWebPlayer)
			return System.IO.Path.GetDirectoryName(Application.absoluteURL).Replace("\\", "/")+ "/StreamingAssets";
		else if (Application.isMobilePlatform || Application.isConsolePlatform)
			return Application.streamingAssetsPath;
		else // For standalone player.
			return "file://" +  Application.streamingAssetsPath;
	}
#endregion
}