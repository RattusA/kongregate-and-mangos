using UnityEngine;
using System.Collections;

public class dbs : MonoBehaviour
{
	public static DBC dbc;
	public static AreaTriggerDBS sAreaTrigger;
	public static ItemDBS sItems ;  
	//public static var sItemClass : ItemClassDBS;
	//public static var sItemSubClass : ItemSubClassDBS; 
	public static ItemDisplayInfoDBS sItemDisplayInfo ;
	//public static var sItemInventoryType : ItemInventoryTypeDBS;

	public static CreatureDisplayInfoDBS sCreatureDisplayInfo ;
	//public static var sCreatureType : CreatureTypeDBS;

	public static SpellDBS sSpells;
	public static SpellIconDBS sSpellIcons;
	
	public static FactionDBS fFaction;

	public static SkillLineAbilityDBS sSkillLIneAbility;

	//public static var Debug : debug = new debug(); 

	public static GameObjectDisplayInfoDBS sGameObjectDisplayInfo;
	public static CharacterRaceInfoDBS sCharacterRace;
	public static CharacterClassInfoDBS sCharacterClass;
	public static CharacterGenderInfoDBS sCharacterGender;
	public static TalentDBS sTalents;
	public static TalentTabDBS sTalentTab;
	public static CustomFaceDBS sCustomFace;
	public static CustomHairStyleDBS sCustomHairStyle;
	public static CustomFacialHairDBS sCustomFacialHair;
	public static CustomHairColorDBS sCustomHairColor;
	public static CustomSkinColorDBS sCustomSkinColor;

	public static SpellItemEnchantmentDBS sSpellItemEnchantment;  
	public static LockDBS sLock;  

	//WorldMapArea
	public static WorldMapAreaDBS sWorldMapArea;
	// This DBC stores Glyph IDs used in Character_glyphs glyphs table.
	public static GlyphPropertiesDBS sGlyphProperties;

	public static GemPropertiesDBS sGemProperties;

	//public static byte LANGUAGE = 0;//LANGUAGE.LANGUAGE_ENGLISH; to be done
	public static int _sizeMod = 1;
	public static bool loadAllDBS = true;

	public static uint staticWOWToWOM (uint WOW)
	{
		if (WOW > 0) {
			return (WOW << 2) ^ 243;
		}
	
		return 0; 
	}

	public static uint staticWOMToWOW (uint WOM)
	{
		if (WOM > 0) {
			return (WOM ^ 243) >> 2;
		}
	
		return 0; 
	}

	//public static var DebaddTextInstanceug : debug = new debug(); 

	void Awake ()
	{
#if UNITY_EDITOR
		Application.targetFrameRate = -1;
#elif UNITY_ANDROID
		Application.targetFrameRate = 30;
#endif

#if UNITY_IPHONE
		if(Screen.width >= 2048)
		{
			_sizeMod = 2;
		}
#endif
			
		dbc = new DBC ("");

		sItemDisplayInfo = new ItemDisplayInfoDBS();
		
		sCharacterRace = new CharacterRaceInfoDBS();
		sCharacterClass = new CharacterClassInfoDBS();
		sCharacterGender = new CharacterGenderInfoDBS();
		
		sCustomFace = new CustomFaceDBS();
		sCustomHairStyle = new CustomHairStyleDBS();
		sCustomFacialHair = new CustomFacialHairDBS();
		sCustomHairColor = new CustomHairColorDBS();
		sCustomSkinColor = new CustomSkinColorDBS();

		sWorldMapArea = new WorldMapAreaDBS();
		DontDestroyOnLoad(this);
	}

	public static void LoadGameDBS()
	{
		if(!loadAllDBS)
		{
			return;
		}

		loadAllDBS = false;

		sAreaTrigger = new AreaTriggerDBS ();
		sCreatureDisplayInfo = new CreatureDisplayInfoDBS ();

		sItems = new ItemDBS();
		sSpells = new SpellDBS ();
		sSpellIcons = new SpellIconDBS ();
		sSkillLIneAbility = new SkillLineAbilityDBS ();

		sTalents = new TalentDBS();
		sTalentTab = new TalentTabDBS();
		
		fFaction = new FactionDBS ();
		sGameObjectDisplayInfo = new GameObjectDisplayInfoDBS ();
		
		sSpellItemEnchantment = new SpellItemEnchantmentDBS ();
		sLock = new LockDBS ();

		sGlyphProperties = new GlyphPropertiesDBS();
		sGemProperties = new GemPropertiesDBS();
	}
	
	
	//=========FOR SOUND FX MANAGER===========//
	private Transform _mainPlayerTransform;
	
	void Start ()
	{
		try
		{
			GameObject player = GameObject.Find ("player");
			if(player != null)
				_mainPlayerTransform = player.transform;
		}
		catch (System.Exception e)
		{
			Debug.LogException(e);
		}
	}
	
	void Update ()
	{
		try
		{
			if (_mainPlayerTransform != null)
			{
				transform.position = _mainPlayerTransform.position;
				transform.eulerAngles = _mainPlayerTransform.eulerAngles;
			}
			else
			{
				GameObject player = GameObject.Find ("player");
				if(player != null)
					_mainPlayerTransform = player.transform;
			}
		}
		catch (System.Exception e)
		{
			Debug.LogException(e);
		}
	}
}