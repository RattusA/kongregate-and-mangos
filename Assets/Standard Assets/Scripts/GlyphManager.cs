﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlyphManager
{
	Player player;
	int firstGlyphSlot = 21;
	int[] bigGlyphSlot = {21, 24, 26};
	int[] smallGlyphSlot = {22, 23, 25};

	public GlyphManager(Player mainPlayer)
	{
		player = mainPlayer;
	}

	public bool EquipGlyph(Item item)
	{
		bool ret = false;
		if(item.subClass == (uint)player.playerClass)
		{
			GlyphPropertiesEntry propertyEntry = dbs.sGlyphProperties.colection.Find(property => (property.spellID == item.spells[0].ID));
			if(propertyEntry != null)
			{
				List<int> slotList;
				switch(propertyEntry.glyphType)
				{
				case (int)GlyphType.BIG_GLYPH:
					slotList = new List<int>(bigGlyphSlot);
					break;
				case (int)GlyphType.SMALL_GLYPH:
					slotList = new List<int>(smallGlyphSlot);
					break;
				default:
					slotList = new List<int>();
					break;
				}
				int emptySlot = 0;
				int glyphSlotsMask = (int)player.GetUInt32Value((int)UpdateFields.EUnitFields.PLAYER_GLYPHS_ENABLED);
				foreach(int glyphSlot in slotList)
				{
					if( ((glyphSlotsMask >> (glyphSlot - firstGlyphSlot)) & 1) > 0)
					{
						emptySlot = glyphSlot;
					}
				}
				if(emptySlot > 0)
				{
					UseGlyphItem(item, emptySlot);
					ret = true;
				}
				else
				{
					Debug.Log ("No appropriate empty slots");
				}
			}
		}
		return ret;
	}

	private bool UseGlyphItem(Item item, int glyphSlot)
	{
		byte cost_count = 0;
		uint spellID = item.spells[0].ID;
		ulong itemGuid = item.GetUInt64Value(0);
		uint glyphIndex = (uint)glyphSlot;
		SpellCastTargets targets = new SpellCastTargets();
		
		WorldPacket pkt = new WorldPacket();
		pkt.SetOpcode((ushort)OpCodes.CMSG_USE_ITEM);
		pkt.Append(item.bag);
		pkt.Append(item.slot);		
		pkt.Append(cost_count);
		pkt.Add(spellID);
		pkt.Add(itemGuid);						//get item guid
		pkt.Add(glyphIndex);
		pkt.Append(cost_count);
		pkt.Append(targets.Write(itemGuid));
		
		RealmSocket.outQueue.Add(pkt);
		return true;

		// Ответ серера в классе TraitsManager строка 183
	}

}
