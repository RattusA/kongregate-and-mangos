﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RealmWindow : MonoBehaviour
{
	static GameObject DBSHolderGO = null;
	
	private ArrayList realms;
	private bool  loadDBS = false;
	
	public Texture2D realmBackground;
	public GUIStyle realmSkin;
	[SerializeField]
	private Buttons BlueButtons;
	
	private Rect fullScreenRect;
	private Rect quitButtonRext;
	
	private GameObject loadingDummy;
	
	void  Awake ()
	{	
		#if UNITY_ANDROID && !UNITY_EDITOR
		//		CallJavaCode.SetUserID(LoginWindow.emailText);
		//#elif UNITY_IPHONE
		//		TapjoyPlugin.SetUserID(LoginWindow.emailText);
		#endif
	}
	
	void  Start ()
	{
		if(!GameObject.Find("DBSHolder"))
		{
			loadDBS = true;
		}
		else
		{
			DestroyShieldAnimation();
		}
		
		fullScreenRect = new Rect(0, 0, Screen.width, Screen.height);
		quitButtonRext = new Rect(Screen.width * 0.01f, Screen.height * 0.87f, Screen.width * 0.199f, Screen.height * 0.1f);
		
		realms = RealmSession.Realms;
		realmSkin.fontSize = (int)(Screen.height * 0.06f);
	}	
	
	void  Update ()
	{
		if(loadDBS)
		{
			StartCoroutine(LoadDBSGO());
			loadDBS = false;
		}
	}
	
	IEnumerator LoadDBSGO ()
	{	
		AsyncOperation async = Application.LoadLevelAdditiveAsync("DBSScene");
		yield return async;
		
		if(async.isDone)
		{
			DestroyShieldAnimation();
		}
	}
	
	private void  DestroyShieldAnimation ()
	{
		GameObject loadDummy = GameObject.Find("logging_in");
		
		if(loadDummy && loadDummy.name == "logging_in")
		{
			GameObject.Destroy(loadDummy);
		}
	}
	
	void  OnGUI ()
	{
		GUI.DrawTexture(fullScreenRect, realmBackground);
		
		byte index = 0;
		
		foreach(SRealmInfo realm in realms)
		{
			string realmInfo = "";
			string realmInfoChars = "";
			string realmInfoPopulation = "";
			string realmInfoType = "";
			
			RealmFlags flag;
			
			flag = RealmFlags.REALM_FLAG_INVALID;
			if((realm.flags & flag) == flag)
			{
				realmInfo += "Invalid ";
			}
			
			flag = RealmFlags.REALM_FLAG_OFFLINE;
			if((realm.flags & flag) == flag)
			{
				realmInfo += "Offline ";
			}
			
			flag = RealmFlags.REALM_FLAG_FULL;
			if((realm.flags & flag) == flag)
			{
				realmInfo += "Full ";
			}
			
			if (realmInfo == "")
			{
				realmInfoChars += realm.chars_here;
				realmInfoPopulation += (realm.population * 100) + "%";
				
				switch (realm.icon)
				{
				case 4:
				case 0:
					realmInfoType += "PvE";
					break;
				case 1:
					realmInfoType += "PvE + PvP";
					break;
				case 6:
					realmInfoType += "RP";
					break;
				case 8:
					realmInfoType += "RPPVP";
					break;
				}
			}
			
			Rect realmButtonRect = new Rect(Screen.width * 0.02f, Screen.height * 0.17f + Screen.height * 0.14f * index,
			                                Screen.width * 0.96f, Screen.height * 0.1f);
			if(GUI.Button(realmButtonRect, realm.name, realmSkin))
			{
				if(realmInfoType != "") // don't attempt to connect to offline servers
				{
					InstantiateLoadingShieldAnimation("realm_select");
					
					RealmSocket rs = GameObject.Find("realmReceiver").GetComponent<RealmSocket>();
					WorldSession.setParent(rs);				
					if(realm.name == PlayerPrefs.GetString("lastRealm"))
					{
						if(LoginWindow.accountLogin == PlayerPrefs.GetString("lastAccount"))
						{
							Debug.Log("we read from PlayerPrefs... ");
							AccountCharactersList.selected = PlayerPrefs.GetInt("charNumber", -1);
							//CheckCharacterSlotStatus(selected);//check if the selected slot if a valid one
						}
						else
						{
							PlayerPrefs.SetString("lastAccount", LoginWindow.accountLogin);
							PlayerPrefs.SetInt("charNumber", AccountCharactersList.selected);
						}
					}
					else
					{
						PlayerPrefs.SetString("lastRealm", realm.name);
						PlayerPrefs.SetString("lastAccount", LoginWindow.accountLogin);
						PlayerPrefs.SetInt("charNumber", AccountCharactersList.selected);
					}
					rs.Connect(realm.addr_port);
				}
			}
			
			GUI.Label( new Rect(Screen.width*0.55f,Screen.height*0.17f + Screen.height*0.14f*index,Screen.width*0.4f, Screen.height*0.1f), realmInfo, realmSkin);
			GUI.Label( new Rect(Screen.width*0.55f,Screen.height*0.17f + Screen.height*0.14f*index,Screen.width*0.2f, Screen.height*0.1f), realmInfoChars, realmSkin);
			GUI.Label( new Rect(Screen.width*0.85f,Screen.height*0.17f + Screen.height*0.14f*index,Screen.width*0.2f, Screen.height*0.1f), realmInfoPopulation, realmSkin);
			GUI.Label( new Rect(Screen.width*0.25f,Screen.height*0.17f + Screen.height*0.14f*index,Screen.width*0.2f, Screen.height*0.1f), realmInfoType, realmSkin);
			index++;
		}
		
		if(GUI.Button(quitButtonRext, "", BlueButtons.Quit))
		{
			DestroyShieldAnimation();
//			LoginWindow.WindowState = LoginWindowState.LOGIN_STATE_LOGIN;
			if(GameManager.Instance != null)
			{
				GameManager.Instance.LoadScene("LoginWindow");
			}
			else
			{
				Application.LoadLevel("LoginWindow");
			}
		}
	}
	
	public void  InstantiateLoadingShieldAnimation ( string name  )
	{
		if(loadingDummy == null)
		{
			loadingDummy = GameObject.Instantiate(Resources.Load<GameObject> ("GUI/LoadingDummy2"));
		}
		loadingDummy.name = name;
	}
	
	public void  DestroyInstantiatedShieldAnimation ( string name  )
	{
		if(loadingDummy && loadingDummy.name == name)
		{
			GameObject.Destroy(loadingDummy);
		}
	}
	
	IEnumerator  CheckCharacterSlotStatus ( int selectedSlot )
	{
		if(selectedSlot < 2)
		{
			AccountCharactersList.selected = selectedSlot;
			Debug.Log("SELECTED: " + AccountCharactersList.selected + " " + selectedSlot);
			if(AccountCharactersList.selected > -1)
			{
				if(GameManager.Instance != null)
				{
					GameManager.Instance.LoadScene("SelectCharacterWindow");
				}
				else
				{
					Application.LoadLevel("SelectCharacterWindow");
				}
			}
			DestroyInstantiatedShieldAnimation("char_select");
			return false;
		}
		
		AccountCharactersList.selected = -1;
		string url="https://mithril.worldofmidgard.com/worldofmidgard/functions/get_unlocked_slot_list.php";
		string[] argname = new string[]{"account_email", "password", "slot_type"};
		string[] args = new string[]{LoginWindow.accountLogin, LoginWindow.passHash, "3"};
		yield return StartCoroutine(PhpJson.phpj(url,argname,args));
		List<int> response = new List<int>();
		response = PhpJson.unlockSlots();
		PlayerPrefs.SetInt("charNumber", AccountCharactersList.selected);
		if(AccountCharactersList.selected > -1)
		{
			if(GameManager.Instance != null)
			{
				GameManager.Instance.LoadScene("SelectCharacterWindow");
			}
			else
			{
				Application.LoadLevel("SelectCharacterWindow");
			}
		}
		DestroyInstantiatedShieldAnimation("char_select");
	}
}