using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreateCharacterSelectStyleWindow : MonoBehaviour
{
	public Text ButtonName;

	public void OnEnable()
	{
		if(CreateCharacterWindow.instance.characterInfo.newCust.charGender == Gender.GENDER_FEMALE)
		{
			ButtonName.text = "Tattoos";
		}
		else
		{
			ButtonName.text = "Facial Hair";
		}
	}

	public void OnStyleButtonClick(string StyleName)
	{
		switch(StyleName)
		{
		case "Face":
			CreateCharacterWindow.instance.characterInfo.newCust.charFace++;
			break;
		case "Facial Hair":
			CreateCharacterWindow.instance.characterInfo.newCust.charFacialHair++;
			break;
		case "Skin Color":
			CreateCharacterWindow.instance.characterInfo.newCust.charSkinColor++;
			break;
		case "Hair Style":
			CreateCharacterWindow.instance.characterInfo.newCust.charHairStyle++;
			break;
		case "Hair Color":
			CreateCharacterWindow.instance.characterInfo.newCust.charHairColor++;
			break;
		case "Random":
			CreateCharacterWindow.instance.characterInfo.RandomCustomizations();
			break;		
		}

		CreateCharacterWindow.instance.characterInfo.UpdateCharacter();
	}
}