using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class SelectCharacterClassImage
{
	public Sprite Orc;
	public Sprite BloodDrake;
	public Sprite DarkElf;
	public Sprite Dwarf;
	public Sprite Human;
	public Sprite Elf;
}

public class CreateCharacterSelectRaceWindow : MonoBehaviour
{
	public SelectCharacterClassImage MaleImageList;
	public SelectCharacterClassImage FemaleImageList;

	public Image OrcImage;
	public Image BloodDrakeImage;
	public Image DarkElfImage;
	public Image DwarfImage;
	public Image HumanImage;
	public Image ElfImage;

	public void OnPortrainToggleClick(string RaceName)
	{
		switch(RaceName)
		{
		case "Orc":
			CreateCharacterWindow.instance.characterInfo.newCust.charRace = Races.RACE_ORC;
			CreateCharacterWindow.instance.characterInfo.newCust.charFaction = FACTION.FURY;
			break;
		case "BloodDrake":
			CreateCharacterWindow.instance.characterInfo.newCust.charRace = Races.RACE_TROLL;
			CreateCharacterWindow.instance.characterInfo.newCust.charFaction = FACTION.FURY;
			break;
		case "DarkElf":
			CreateCharacterWindow.instance.characterInfo.newCust.charRace = Races.RACE_BLOODELF;
			CreateCharacterWindow.instance.characterInfo.newCust.charFaction = FACTION.FURY;
			break;
		case "Dwarf":
			CreateCharacterWindow.instance.characterInfo.newCust.charRace = Races.RACE_DWARF;
			CreateCharacterWindow.instance.characterInfo.newCust.charFaction = FACTION.ALLIANCE;
			break;
		case "Human":
			CreateCharacterWindow.instance.characterInfo.newCust.charRace = Races.RACE_HUMAN;
			CreateCharacterWindow.instance.characterInfo.newCust.charFaction = FACTION.ALLIANCE;
			break;
		case "Elf":
			CreateCharacterWindow.instance.characterInfo.newCust.charRace = Races.RACE_NIGHTELF;
			CreateCharacterWindow.instance.characterInfo.newCust.charFaction = FACTION.ALLIANCE;
			break;
		}

		CreateCharacterWindow.instance.characterInfo.UpdateCharacter();
	}

	public void OnMaleGenderButtonClick()
	{
		CreateCharacterWindow.instance.characterInfo.newCust.charGender = Gender.GENDER_MALE;
		CreateCharacterWindow.instance.characterInfo.UpdateCharacter();

		OrcImage.sprite = MaleImageList.Orc;
		BloodDrakeImage.sprite = MaleImageList.BloodDrake;
		DarkElfImage.sprite = MaleImageList.DarkElf;
		DwarfImage.sprite = MaleImageList.Dwarf;
		HumanImage.sprite = MaleImageList.Human;
		ElfImage.sprite = MaleImageList.Elf;
	}

	public void OnFemaleGenderButtonClick()
	{
		CreateCharacterWindow.instance.characterInfo.newCust.charGender = Gender.GENDER_FEMALE;
		CreateCharacterWindow.instance.characterInfo.UpdateCharacter();

		OrcImage.sprite = FemaleImageList.Orc;
		BloodDrakeImage.sprite = FemaleImageList.BloodDrake;
		DarkElfImage.sprite = FemaleImageList.DarkElf;
		DwarfImage.sprite = FemaleImageList.Dwarf;
		HumanImage.sprite = FemaleImageList.Human;
		ElfImage.sprite = FemaleImageList.Elf;
	}
}