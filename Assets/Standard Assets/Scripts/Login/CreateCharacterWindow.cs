﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum CreateCharacterState
{
	SELECT_RACE,
	SELECT_CLASS,
	SELECT_STYLE,
	INPUT_NAME
}

public class CreateCharacterWindow : MonoBehaviour
{
	private CreateCharacterState _windowState = CreateCharacterState.SELECT_RACE;

	public GameObject ccSelectRaceWindow;
	public GameObject ccSelectClassWindow;
	public GameObject ccSelectStyleWindow;
	public GameObject ccInputNameWindow;

	public GameObject backButton;
	public InputField characterName;

	public static CreateCharacterWindow instance;
	public CharacterCreation characterInfo;

	public void Awake()
	{
		instance = this;
		characterInfo = new CharacterCreation();
	}

	public void OnEnable()
	{
		_windowState = CreateCharacterState.SELECT_RACE;
		ccSelectRaceWindow.SetActive(true);
		ccSelectClassWindow.SetActive(false);
		ccSelectStyleWindow.SetActive(false);
		ccInputNameWindow.SetActive(false);
		backButton.SetActive(AccountCharactersList.instance.GetCharactersCount() > 0);

		characterInfo.newCust.charGender = Gender.GENDER_MALE;
		characterInfo.newCust.charRace = Races.RACE_ORC;
		characterInfo.newCust.charFaction = FACTION.FURY;
		characterInfo.newCust.charClass = Classes.CLASS_PRIEST;

		characterName.text = "";

		characterInfo.LoadPlayer();
		characterInfo.InitMaxStylesAndColors();
	}

	public void OnNextButtonClick()
	{
		switch(_windowState)
		{
		case CreateCharacterState.SELECT_RACE:
			_windowState = CreateCharacterState.SELECT_CLASS;
			ccSelectRaceWindow.SetActive(false);
			ccSelectClassWindow.SetActive(true);
			backButton.SetActive(true);
			break;
		case CreateCharacterState.SELECT_CLASS:
			_windowState = CreateCharacterState.SELECT_STYLE;
			ccSelectClassWindow.SetActive(false);
			ccSelectStyleWindow.SetActive(true);
			break;
		case CreateCharacterState.SELECT_STYLE:
			_windowState = CreateCharacterState.INPUT_NAME;
			ccSelectStyleWindow.SetActive(false);
			ccInputNameWindow.SetActive(true);
			break;
		case CreateCharacterState.INPUT_NAME:
			AccountCharactersList.instance.CreateCharacter(characterInfo.playerEnum);
			break;
		}
	}

	public void OnBackButtonClick()
	{
		switch(_windowState)
		{
		case CreateCharacterState.SELECT_RACE:
			GameObject.Destroy(characterInfo.player.transform.parent.gameObject);
			AccountCharactersList.instance.ShowSelectCharacterWindow();
			break;
		case CreateCharacterState.SELECT_CLASS:
			_windowState = CreateCharacterState.SELECT_RACE;
			ccSelectClassWindow.SetActive(false);
			ccSelectRaceWindow.SetActive(true);
			backButton.SetActive(AccountCharactersList.instance.GetCharactersCount() > 0);
			break;
		case CreateCharacterState.SELECT_STYLE:
			_windowState = CreateCharacterState.SELECT_CLASS;
			ccSelectStyleWindow.SetActive(false);
			ccSelectClassWindow.SetActive(true);
			break;
		case CreateCharacterState.INPUT_NAME:
			_windowState = CreateCharacterState.SELECT_STYLE;
			ccInputNameWindow.SetActive(false);
			ccSelectStyleWindow.SetActive(true);
			break;
		}
	}
}