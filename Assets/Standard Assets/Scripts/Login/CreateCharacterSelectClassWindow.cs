using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreateCharacterSelectClassWindow : MonoBehaviour
{
	public Text DescriptionText;
	public Text RoleText;
	public Classes Class = Classes.CLASS_PRIEST;

	public void Awake()
	{
		DescriptionText.text = "Confessor is a master of healing and damage.";
		RoleText.text = "Roles: Healer and DPS";
	}

	public void OnClassButtonClick(string ClassName)
	{
		switch(ClassName)
		{
		case "Confessor":
			CreateCharacterWindow.instance.characterInfo.newCust.charClass = Classes.CLASS_PRIEST;
			DescriptionText.text = "Confessor is a master of healing and damage.";
			RoleText.text = "Roles: Healer and DPS";
			break;
		case "Fighter":
			CreateCharacterWindow.instance.characterInfo.newCust.charClass = Classes.CLASS_WARRIOR;
			DescriptionText.text = "Fighter is a heavily armed melee soldier.";
			RoleText.text = "Roles: Tank and DPS";
			break;
		case "Mage":
			CreateCharacterWindow.instance.characterInfo.newCust.charClass = Classes.CLASS_MAGE;
			DescriptionText.text = "Mage is a caster with ranged burst and area of effect damage.";
			RoleText.text = "Role: DPS";
			break;
		case "Rogue":
			CreateCharacterWindow.instance.characterInfo.newCust.charClass = Classes.CLASS_ROGUE;
			DescriptionText.text = "Rogue is a prime melee damage dealer.";
			RoleText.text = "Role: DPS";
			break;
		case "Templar":
			CreateCharacterWindow.instance.characterInfo.newCust.charClass = Classes.CLASS_PALADIN;
			DescriptionText.text = "Templar is a soldier devotee of a sacred order.";
			RoleText.text = "Roles: Tank, Healer and DPS";
			break;
		case "Necromancer":
			CreateCharacterWindow.instance.characterInfo.newCust.charClass = Classes.CLASS_WARLOCK;
			DescriptionText.text = "Necromancer is Master of Black & Inferno Magic. Overlord with minions.";
			RoleText.text = "Role: DPS";
			break;
		case "Ranger":
			CreateCharacterWindow.instance.characterInfo.newCust.charClass = Classes.CLASS_HUNTER;
			DescriptionText.text = "Ranger is an expert in shooting weapons and Master Of Beasts.";
			RoleText.text = "Role: DPS";
			break;
		}

		CreateCharacterWindow.instance.characterInfo.UpdateCharacter();
	}
}