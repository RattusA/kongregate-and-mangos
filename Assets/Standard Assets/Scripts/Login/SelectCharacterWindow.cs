using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class SelectCharacterWindow : MonoBehaviour
{
	[System.Serializable]
	public class SelectCharacterButton
	{
		public Image image;
		public Button button;
	}

	public ButtonsTexActive allianceTexturesInfo;
	public ButtonsTexActive furyTexturesInfo;

	public Image backgroundPanel;

	public SelectCharacterButton quitButton;
	public SelectCharacterButton unstuckCharButton;
	public SelectCharacterButton switchRealmButton;
	public SelectCharacterButton newsButton;
	public SelectCharacterButton itemStoreButton;

	public SelectCharacterButton deleteButton;
	public SelectCharacterButton playButton;
	public SelectCharacterButton createButton;

	public SelectCharacterButton closeButton;

	public GameObject getFreeMithrilButton;
	public GameObject buyMithrilWebButton;

	public Image raceImage;
	public Image classImage;
	public Image genderImage;
	public Image levelHighImage;
	public Image levelLowImage;

	public GameObject characterList;
	public GameObject characterSlot;
	public ToggleGroup toggleGroup;

	public GameObject newsPanel;
	public static WorldPacket tutorialPacketFWs;

	public SelectCharacterText imageText;
	public Sprite[] levelNumberTextures;

	
#if UNITY_IPHONE
	bool  canRun;
	bool  needMoreMem;
#endif
	
	public int tmpSelected;
	private bool  isCharLoad = false;
	private int maxCharParts = 7; //EquipmentSlots.EQUIPMENT_SLOT_END - 3;
	private float progressScale = 0;
	private Texture charLoadingBack;
	private Texture charLoadingFilled;
	private Rect charLoadRect;

	public bool  charCreationState = false;
	
	private GameObject loadingDummy;

	public void OnQuitButtonClick()
	{
		AccountCharactersList.instance.StopCharLoading();
		AuthorizationSocket.Instance.CloseSocket();
		Destroy(LoadingScreen.purchaseGO);
		if(GameManager.Instance != null)
		{
			GameManager.Instance.LoadScene("LoginWindow");
		}
		else
		{
			Application.LoadLevel("LoginWindow");
		}
	}

	public void OnUnstuckCharButtonClick()
	{
		string _url = "https://mithril.worldofmidgard.com/worldofmidgard/pages/unstack_character_login.php";
		Application.OpenURL(_url);
	}

	public void OnSwitchRealmButtonClick()
	{
		AccountCharactersList.instance.StopCharLoading();
		AuthorizationSocket.Instance.CloseSocket();
		if(GameManager.Instance != null)
		{
			GameManager.Instance.LoadScene("RealmWindow");
		}
		else
		{
			Application.LoadLevel("RealmWindow");
		}
	}

	public void OnNewsButtonClick()
	{
		newsPanel.SetActive(true);
	}

	public void OnCloseNewsButtonClick()
	{
		newsPanel.SetActive(false);
	}

	public void OnGetFreeMithrilButtonClick()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		TrialpayIntegration.openOfferwall("fd745515f05c3b668cd77edecd619634", LoginWindow.accountLogin);	//trialpay
#elif UNITY_IPHONE
//		TapjoyPlugin.Screen.heightowOffers();
#endif
	}

	public void OnBuyMithrilWebButtonClick()
	{
#if !UNITY_IPHONE
		if(BuildConfig.platformName != "amazon")
		{
			Application.OpenURL("https://mithril.worldofmidgard.com/worldofmidgard/pages/direct_sales_login.php");
		}
#endif
	}

	public void OnItemStoreButtonClick()
	{
		Application.OpenURL("https://mithril.worldofmidgard.com/worldofmidgard/pages/direct_sales_login.php");
	}

	public void OnDeleteButtonClick()
	{
		if(AccountCharactersList.selected != -1)
		{
			AccountCharactersList.instance.ShowDeleteMessage(AccountCharactersList.instance.GetPlayerBySelectedIndex().name);
		}
	}

	public void OnPlayButtonClick()
	{
		if(AccountCharactersList.selected != -1 && !AssetBundleManager.isStarted)
		{
			StartCoroutine(StartGame());
		}
	}

	public void OnCreateButtonClick()
	{		
		AccountCharactersList.instance.StopCharLoading();
		AccountCharactersList.instance.ShowCreateCharacterWindow();
	}

	public void OnCharacterSlotButtonClick(int index)
	{
		if(index != AccountCharactersList.selected)
		{
			InstantiateLoadingShieldAnimation("char_select");
			AccountCharactersList.selected = index;					
			CheckCharacterSlotStatus(index);
		}
	}

	public void OnConfirmDeleteButtonClick()
	{
		PlayerEnum playerEnum = AccountCharactersList.instance.GetPlayerBySelectedIndex();
		if(AccountCharactersList.instance.messageWindow.input.text == playerEnum.name)
		{
 			DeleteAllPlayerPrefs(playerEnum.name);

			WorldPacket pkt = new WorldPacket(OpCodes.CMSG_CHAR_DELETE, 8);
			pkt.Append(playerEnum.guid);
			RealmSocket.outQueue.Add(pkt);

			if(AccountCharactersList.instance.GetCharactersCount() > 1)
			{
				AccountCharactersList.selected = 0;
			}
			else
			{
				OnCreateButtonClick();
				AccountCharactersList.selected = -1;
			}
			AccountCharactersList.instance.OnCloseMessageWindowButtonClick();

		}
		else
		{
			AccountCharactersList.instance.messageWindow.ShowResultMessageWindow("Character name does not match the name of character you want to delete.");
		}
	}
	
	private IEnumerator StartGame()
	{
		LoginWindow.SetInventoryMessage("Loading...please wait", 999);
		AccountCharactersList.instance.StopCharLoading();
		Swipe.tutorialPacketInSwipe = tutorialPacketFWs;
		Swipe.playerEnum = AccountCharactersList.instance.GetPlayerBySelectedIndex();
		InstantiateLoadingShieldAnimation("LoadingDummy1(Clone)");
		yield return new WaitForSeconds(1);
		dbs.LoadGameDBS();
		WorldSession.EnterWorldWithCharacter(Swipe.playerEnum.name, Swipe.playerEnum.guid, Swipe.playerEnum._mapId);
		PlayerPrefs.SetInt("charNumber", AccountCharactersList.selected);
	}
	
	void  DeleteAllPlayerPrefs ( string name )
	{
		try
		{
			PlayerPrefs.DeleteKey(name + "$channels$count");
			PlayerPrefs.DeleteKey(name + "$channels$tostring");
			PlayerPrefs.DeleteKey(name + "HasEnterWorldBefore");
			
			for(int i = 0; i < 26 ;i++) // 26 == player equipSlot
			{
				if(PlayerPrefs.HasKey(name + "$" + i.ToString() + "$" + "1"))
				{
					PlayerPrefs.DeleteKey(name + "$" + i.ToString() + "$" + "1");
				}
				if(PlayerPrefs.HasKey(name + "$" + i.ToString() + "$" + "2"))
				{
					PlayerPrefs.DeleteKey(name + "$" + i.ToString() + "$" + "2");
				}
			}
		}
		catch( Exception exception )
		{
			Debug.Log(exception);
		}
	}
	
	private void InstantiateLoadingShieldAnimation ( string name )
	{
		if(loadingDummy == null)
		{
			loadingDummy = GameObject.Instantiate(UnityEngine.Resources.Load<GameObject>("GUI/LoadingDummy2"));
		}
		loadingDummy.name = name;
	}
	
	public void DestroyInstantiatedShieldAnimation ( string name )
	{
		if(loadingDummy && loadingDummy.name == name)
		{
			GameObject.Destroy(loadingDummy);
		}
	}

	private void CheckCharacterSlotStatus(int selectedSlot)
	{
		AccountCharactersList.selected = selectedSlot;
		
		PlayerPrefs.SetInt("charNumber", selectedSlot);
		if(AccountCharactersList.selected > -1 && AccountCharactersList.selected < AccountCharactersList.instance.GetCharactersCount())
		{
			AccountCharactersList.instance.LoadPlayer();
			SetBackground(AccountCharactersList.instance.GetPlayerBySelectedIndex());		
		}
		
		DestroyInstantiatedShieldAnimation("char_select");
	}
	
	public void SetBackground(PlayerEnum player)
	{
		if(player == null)
		{
			return;
		}

		switch(player.race)
		{
		case Races.RACE_HUMAN :
			raceImage.sprite = imageText.Human;
			break;
		case Races.RACE_NIGHTELF :
			raceImage.sprite = imageText.Elf;
			break;
		case Races.RACE_DWARF:
			raceImage.sprite = imageText.Dwarf;
			break;
		case Races.RACE_BLOODELF:
			raceImage.sprite = imageText.DarkElf;
			break;
		case Races.RACE_ORC:
			raceImage.sprite = imageText.Orc;
			break;
		case Races.RACE_TROLL:
			raceImage.sprite = imageText.BloodDrak;
			break;
		}

		switch(player.classType)
		{
		case Classes.CLASS_WARRIOR:
			classImage.sprite = imageText.Fighter;
			break;
		case Classes.CLASS_PALADIN:
			classImage.sprite = imageText.Templar;
			break;
		case Classes.CLASS_ROGUE:
			classImage.sprite = imageText.Rogue;
			break;
		case Classes.CLASS_MAGE:
			classImage.sprite = imageText.Mage;
			break;
		case Classes.CLASS_PRIEST:
			classImage.sprite = imageText.Confessor;
			break;
		case Classes.CLASS_WARLOCK :
			classImage.sprite = imageText.Necromancer;
			break;
		case Classes.CLASS_HUNTER :
			classImage.sprite = imageText.Ranger;
			break;
		}
		
		if(player.gender == Gender.GENDER_MALE)
		{
			genderImage.sprite = imageText.Male;
		}
		else 
		{
			genderImage.sprite = imageText.Female;
		}

		if(player._level < 10)
		{
			levelHighImage.sprite = levelNumberTextures[player._level];
			levelLowImage.color = new Color(1, 1, 1, 0);
		}
		else
		{
			levelHighImage.sprite = levelNumberTextures[player._level / 10];
			levelLowImage.sprite = levelNumberTextures[player._level % 10];
			levelLowImage.color = new Color(1, 1, 1, 1);
		}
		
		FACTION factionPlayer = player.GetPlayerFaction();
		switch (factionPlayer)
		{
		case FACTION.ALLIANCE:
			backgroundPanel.sprite = allianceTexturesInfo.Background;

			quitButton.image.sprite = allianceTexturesInfo.Quit;
			quitButton.button.spriteState = GetNewSpriteState(allianceTexturesInfo.QuitPressed);

			unstuckCharButton.image.sprite = allianceTexturesInfo.Unstuck;
			unstuckCharButton.button.spriteState = GetNewSpriteState(allianceTexturesInfo.UnstuckPressed);

			switchRealmButton.image.sprite = allianceTexturesInfo.SwitchRealm;
			switchRealmButton.button.spriteState = GetNewSpriteState(allianceTexturesInfo.SwitchRealmPressed);

			newsButton.image.sprite = allianceTexturesInfo.News;
			newsButton.button.spriteState = GetNewSpriteState(allianceTexturesInfo.NewsPressed);

			itemStoreButton.image.sprite = allianceTexturesInfo.ItemStore;
			itemStoreButton.button.spriteState = GetNewSpriteState(allianceTexturesInfo.ItemStorePressed);
			
			deleteButton.image.sprite = allianceTexturesInfo.Delete;
			deleteButton.button.spriteState = GetNewSpriteState(allianceTexturesInfo.DeletePressed);

			playButton.image.sprite = allianceTexturesInfo.Play;
			playButton.button.spriteState = GetNewSpriteState(allianceTexturesInfo.PlayPressed);

			createButton.image.sprite = allianceTexturesInfo.Create;
			createButton.button.spriteState = GetNewSpriteState(allianceTexturesInfo.CreatePressed);

			closeButton.image.sprite = allianceTexturesInfo.Close;
			closeButton.button.spriteState = GetNewSpriteState(allianceTexturesInfo.ClosePressed);
			break;
		case FACTION.FURY:
			backgroundPanel.sprite = furyTexturesInfo.Background;

			quitButton.image.sprite = furyTexturesInfo.Quit;
			quitButton.button.spriteState = GetNewSpriteState(furyTexturesInfo.QuitPressed);

			unstuckCharButton.image.sprite = furyTexturesInfo.Unstuck;
			unstuckCharButton.button.spriteState = GetNewSpriteState(furyTexturesInfo.UnstuckPressed);

			switchRealmButton.image.sprite = furyTexturesInfo.SwitchRealm;
			switchRealmButton.button.spriteState = GetNewSpriteState(furyTexturesInfo.SwitchRealmPressed);

			newsButton.image.sprite = furyTexturesInfo.News;
			newsButton.button.spriteState = GetNewSpriteState(furyTexturesInfo.NewsPressed);

			itemStoreButton.image.sprite = furyTexturesInfo.ItemStore;
			itemStoreButton.button.spriteState = GetNewSpriteState(furyTexturesInfo.ItemStorePressed);
			
			deleteButton.image.sprite = furyTexturesInfo.Delete;
			deleteButton.button.spriteState = GetNewSpriteState(furyTexturesInfo.DeletePressed);

			playButton.image.sprite = furyTexturesInfo.Play;
			playButton.button.spriteState = GetNewSpriteState(furyTexturesInfo.PlayPressed);

			createButton.image.sprite = furyTexturesInfo.Create;
			createButton.button.spriteState = GetNewSpriteState(furyTexturesInfo.CreatePressed);

			closeButton.image.sprite = furyTexturesInfo.Close;
			closeButton.button.spriteState = GetNewSpriteState(furyTexturesInfo.ClosePressed);
			break;
		}
	}

	private SpriteState GetNewSpriteState(Sprite sprite)
	{
		SpriteState ret = new SpriteState();
		ret.pressedSprite = sprite;
		return ret;
	}

	public void InitCharacterList(PlayerEnum[] chars)
	{
		foreach(Transform child in characterList.transform)
		{
			if(child.gameObject.activeInHierarchy)
			{
				Destroy(child.gameObject);
			}
		}
		
		if(characterSlot != null && characterList != null)
		{
			for(int index = 0; index < chars.Length && index < 10; index++)
			{
				GameObject go = Instantiate<GameObject>(characterSlot);
				go.transform.SetParent(characterList.transform, false);
				go.SetActive(true);

				CharacterSlotToggle component = go.GetComponent<CharacterSlotToggle>();

				if(component != null)
				{
					int i = index;
					component.toggle.group = toggleGroup;
					component.toggle.onValueChanged.AddListener((value) => OnCharacterSlotButtonClick(i));
					component.label.text = chars[index].name;
					if(index == AccountCharactersList.selected)
					{
						component.toggle.isOn = true;
					}
					
					SpriteState spriteState = new SpriteState();
					if(chars[index].GetPlayerFaction() == FACTION.FURY)
					{
						component.image.sprite = furyTexturesInfo.CharacterSlot;
						component.checkmark.sprite = furyTexturesInfo.CharacterSlotPressed;
					}
					else
					{
						component.image.sprite = allianceTexturesInfo.CharacterSlot;
						component.checkmark.sprite = allianceTexturesInfo.CharacterSlotPressed;
					}
				}
			}
		}
	}
}

[System.Serializable]
public class SelectCharacterText
{
	public Sprite Level;
	
	public Sprite Male;
	public Sprite Female;
	
	public Sprite Human;
	public Sprite Elf;
	public Sprite Dwarf;
	public Sprite DarkElf;
	public Sprite BloodDrak;
	public Sprite Orc;
	
	public Sprite Fighter;
	public Sprite Templar;
	public Sprite Confessor;
	public Sprite Rogue;
	public Sprite Mage;
	public Sprite Necromancer;
	public Sprite Ranger;
}

[System.Serializable]
public class OtherButtons
{
	public GUIStyle Back;
	public GUIStyle Next;
	public GUIStyle LogIn;
	public GUIStyle Refresh;
	public GUIStyle Options;
	public GUIStyle Register;
	public GUIStyle Submit;
	public GUIStyle Cancel;
	public GUIStyle Ok;
	public GUIStyle regOnline;
	public GUIStyle terms;
	public GUIStyle ResetPassword;
	public GUIStyle Quit;
	public GUIStyle Privacy;
	public GUIStyle News;
	public GUIStyle CloseNews;
	public GUIStyle Try;
	public GUIStyle Buy_unlock;
	public GUIStyle Cancel2;
	public GUIStyle OK2;
}

[System.Serializable]
public class Buttons
{
	public GUIStyle Create;
	public GUIStyle Delete;
	public GUIStyle Play;
	public GUIStyle Quit;
	public GUIStyle Settings;
	public GUIStyle OK;
	public GUIStyle Cancel;
	public GUIStyle Back;
	public GUIStyle Next;
	public GUIStyle CreateSecond;
	public GUIStyle Fighter;
	public GUIStyle Rogue;
	public GUIStyle Confessor;
	public GUIStyle Templar;
	public GUIStyle Mage;
	public GUIStyle Necromancer;
	public GUIStyle Ranger;
	public GUIStyle Human;
	public GUIStyle Elf;
	public GUIStyle Dwarf;
	public GUIStyle DarkElf;
	public GUIStyle Orc;
	public GUIStyle BloodDrak;
	public GUIStyle Male;
	public GUIStyle Female;
	public GUIStyle Unstuck;
	public GUIStyle SwitchRealm;
	public GUIStyle FreeMithril;
	public GUIStyle MithrilIAP;
	public GUIStyle MithrilWeb;
	public GUIStyle ItemStore;
	public GUIStyle News;
	public GUIStyle NewsClose;
}

[System.Serializable]
public class ButtonsTexActive
{
	public Sprite Background;
	public Sprite Close;
	public Sprite ClosePressed;
	public Sprite Create;
	public Sprite CreatePressed;
	public Sprite Delete;
	public Sprite DeletePressed;
	public Sprite Play;
	public Sprite PlayPressed;
	public Sprite Quit;
	public Sprite QuitPressed;
	public Sprite Unstuck;
	public Sprite UnstuckPressed;
	public Sprite SwitchRealm;
	public Sprite SwitchRealmPressed;
	public Sprite ItemStore;
	public Sprite ItemStorePressed;
	public Sprite News;
	public Sprite NewsPressed;
	public Sprite CharacterSlot;
	public Sprite CharacterSlotPressed;
}