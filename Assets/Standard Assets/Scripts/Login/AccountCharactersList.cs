﻿using UnityEngine;
using System;
using System.Collections;

public class AccountCharactersList : MonoBehaviour
{
	public static AccountCharactersList instance;
	public static int selected = -1;
	public Camera mainCamera;

	public SelectCharacterWindow selectCharacterWindow;
	public CreateCharacterWindow createCharacterWindow;
	public CreateCharacterMessageWindow messageWindow;

	private GameObject _player;
	private PlayerEnum[] _plr = null;
	private AssambleCharacter _assambleCharacter;
	private bool _isCharLoad = false;
	private float _progressScale = 0;
	private int _maxCharParts = 7;


	[SerializeField]
	private Texture _characterLoadingBack;
	[SerializeField]
	private Texture _characterLoadingFilled;
	private Rect _characterLoadRect = new Rect();

    public void Awake()
	{
		instance = this;
		_assambleCharacter = new AssambleCharacter();

		float textureScale = _characterLoadingBack.width / (_characterLoadingBack.height * 1.0f);
		_characterLoadRect.x = (Screen.width - (Screen.height * 0.6f * textureScale)) * 0.5f;
		_characterLoadRect.y = Screen.height * 0.08f;
		_characterLoadRect.width = Screen.height * 0.6f * textureScale;
		_characterLoadRect.height = Screen.height * 0.6f;
	}

	void Start ()
	{
#if UNITY_IPHONE
		dbs._sizeMod = (Screen.width == 2048) ? 2 : 1;
#endif
		GameObject loadDummy = GameObject.Find("realm_select");
		if(loadDummy && loadDummy.name == "realm_select")
		{
			GameObject.Destroy(loadDummy);
		}
		
		LoadSceneManager.LoadingStatus = LoadSceneManager.LOADING.LOADING_IDLE;
		
		selectCharacterWindow.getFreeMithrilButton.SetActive(BuildConfig.hasFreeMithrilButton);
		selectCharacterWindow.buyMithrilWebButton.SetActive(BuildConfig.hasPurchaseMithrilButton);
		selectCharacterWindow.itemStoreButton.button.gameObject.SetActive(BuildConfig.hasItemShopButton);
		
		selected = PlayerPrefs.GetInt("charNumber");
		WorldSession.istance.RequestCharactersList();
	}

	private void Update()
	{
		if(_assambleCharacter.isNeedHideVisualSlots)
		{
			if(_progressScale >= 1)
			{
				_assambleCharacter.EnableAllParts();
				_isCharLoad = false;
				_assambleCharacter.UpdateAnimation("idle");
			}
			else
			{
				UpdateCharLoadingProgress(_assambleCharacter);
			}
		}
		
		if(createCharacterWindow.gameObject.activeInHierarchy &&
		   createCharacterWindow.characterInfo.isNeedHideVisualSlots &&
		   createCharacterWindow.characterInfo.IsAllLoaded())
		{
			createCharacterWindow.characterInfo.EnableAllParts();
		}
	}

	public void OnCloseMessageWindowButtonClick()
	{
		messageWindow.gameObject.SetActive(false);

		if(createCharacterWindow.gameObject.activeInHierarchy)
		{
			createCharacterWindow.characterInfo.player.SetActive(true);
		}
	}
	
	public void ShowMessage(string text)
	{
		messageWindow.gameObject.SetActive(true);
		messageWindow.ShowResultMessageWindow(text);
	}

	public void ShowDeleteMessage(string text)
	{
		messageWindow.gameObject.SetActive(true);
		messageWindow.ShowDeleteMessageWindow(text);
	}

	public int GetCharactersCount()
	{
		return (_plr == null) ? 0 : _plr.Length;
	}

	public PlayerEnum GetPlayerBySelectedIndex()
	{
		return _plr[selected];
	}

	public void OnGUI()
	{
		
		if(!messageWindow.gameObject.activeInHierarchy)
		{
			DrawCharLoadingProgress();
		}
	}

	private void UpdateCharLoadingProgress (AssambleCharacter charAssambler)
	{
		int counter = 0;
		for(EquipmentSlots slot = EquipmentSlots.EQUIPMENT_SLOT_START; slot < EquipmentSlots.EQUIPMENT_SLOT_END; slot++)
		{
			if(slot != EquipmentSlots.EQUIPMENT_SLOT_HEAD &&
			   slot != EquipmentSlots.EQUIPMENT_SLOT_CHEST &&
			   slot != EquipmentSlots.EQUIPMENT_SLOT_LEGS &&
			   slot != EquipmentSlots.EQUIPMENT_SLOT_FEET &&
			   slot != EquipmentSlots.EQUIPMENT_SLOT_HANDS &&
			   slot != EquipmentSlots.EQUIPMENT_SLOT_BACK &&
			   slot != EquipmentSlots.EQUIPMENT_SLOT_TABARD)
			{
				continue;
			}

			if(charAssambler.loadedVisualSlots[(int)slot])
			{
				counter++; 
			}
		}
        for (int inx = (int)CustomizationSlot.CUSTOM_FACE; inx <= (int)CustomizationSlot.CUSTOM_FACIALHAIR; inx++)
        {
            if(charAssambler.loadedFaceParts[inx - (int)CustomizationSlot.CUSTOM_FACE])
            {
                counter++;
            }
        }
        _progressScale = counter / ((_maxCharParts + (int)CustomizationSlot.MAX_CUSTOMIZATIONS) * 1.0f);
	}

	public void ReadPacketData()
	{
		byte num;
		byte dummy8;
		uint dummy32;
		
		num = WorldSession.charEnumPkt.Read();
		try
		{
			if(num == 0)
			{
				_plr = null;

				StopCharLoading();
				ShowCreateCharacterWindow();
			}
			else if(num > 0 && num <= 10)
			{
				_plr = new PlayerEnum[num];
				for(int i = 0;i<num;i++)
				{
					_plr[i] = new PlayerEnum();
					_plr[i].guid = WorldSession.charEnumPkt.ReadType(8);
					_plr[i].name = WorldSession.charEnumPkt.ReadString();
					_plr[i].race = (Races)WorldSession.charEnumPkt.Read();
					_plr[i].classType = (Classes)WorldSession.charEnumPkt.Read();
					_plr[i].gender = (Gender)WorldSession.charEnumPkt.Read();
					_plr[i].skinColor = (int)WorldSession.charEnumPkt.Read();//skin
					_plr[i].face = (int)WorldSession.charEnumPkt.Read();//face
					_plr[i].hairStyle = (int)WorldSession.charEnumPkt.Read();//hairstyle
					_plr[i].hairColor = (int)WorldSession.charEnumPkt.Read();//haircolor
					_plr[i].facialHair = (int)WorldSession.charEnumPkt.Read();//facialhair
					_plr[i]._level = WorldSession.charEnumPkt.Read();
					_plr[i]._zoneId = (uint)WorldSession.charEnumPkt.ReadType(4);
					_plr[i]._mapId = WorldSession.charEnumPkt.ReadReversed32bit();
					_plr[i]._x = WorldSession.charEnumPkt.ReadFloat();
					_plr[i]._y = WorldSession.charEnumPkt.ReadFloat();
					_plr[i]._z = WorldSession.charEnumPkt.ReadFloat();
					_plr[i]._guildId = (uint)WorldSession.charEnumPkt.ReadType(4);
					_plr[i]._flags = (uint)WorldSession.charEnumPkt.ReadType(4);
					dummy32 = (uint)WorldSession.charEnumPkt.ReadType(4);//char customization flags
					dummy8 = WorldSession.charEnumPkt.Read();  // at login flags - first 
					_plr[i]._petInfoId = (uint)WorldSession.charEnumPkt.ReadType(4);
					_plr[i]._petLevel = (uint)WorldSession.charEnumPkt.ReadType(4);
					_plr[i]._petFamilyId = (uint)WorldSession.charEnumPkt.ReadType(4);
					for(int inv = 0; inv < 19; inv++)
					{
						PlayerEnumItem playerEnumItem = new PlayerEnumItem();
						playerEnumItem.displayId = WorldSession.charEnumPkt.ReadReversed32bit();
						playerEnumItem.inventoryType = (InventoryType)WorldSession.charEnumPkt.Read();
						playerEnumItem.subClass = WorldSession.charEnumPkt.ReadReversed32bit();
						_plr[i].items[inv] = playerEnumItem;
					}
					for(byte bag = 0; bag < 4; bag++)
					{
						dummy32 = (uint)WorldSession.charEnumPkt.ReadType(4);
						dummy8 = WorldSession.charEnumPkt.Read();
						dummy32 = (uint)WorldSession.charEnumPkt.ReadType(4);
					}
					
					if(!PlayerPrefs.HasKey(_plr[i].name + "HasEnterWorldBefore"))
					{
						PlayerPrefs.SetInt(_plr[i].name + "HasEnterWorldBefore", 0);
					}
				}
			}

			if(createCharacterWindow.gameObject.activeInHierarchy)
			{
				GameObject.Destroy(createCharacterWindow.characterInfo.player.transform.parent.gameObject);
				ShowSelectCharacterWindow();
			}

			SetPlayers();
			selectCharacterWindow.InitCharacterList(_plr);
		}
		catch(Exception exception)
		{
			Debug.Log(exception);
			_plr = null; 
			num = 0;
		}
	}

	private void SetPlayers()
	{
		if(selected >= _plr.Length || selected < 0)
		{
			selected = 0;
		}
		
		selectCharacterWindow.SetBackground(GetPlayerBySelectedIndex());
		LoadPlayer();
	}

	public void LoadPlayer()
	{	
		if(selected < 0 || selected >= _plr.Length)
		{
			return;
		}
		
		StartLoadingProgress();
		_player = _assambleCharacter.Assemble(_plr[selected]);
	}

	void StartLoadingProgress()
	{
		_isCharLoad = true;
		_progressScale = 0;
	}

	public void StopCharLoading()
	{
		_isCharLoad = false;
	}

	void  DrawCharLoadingProgress ()
	{
		if(_isCharLoad && _characterLoadingBack != null && _characterLoadingFilled != null)
		{
			Rect groupRect = new Rect(_characterLoadRect.x, _characterLoadRect.y, _characterLoadRect.width, _characterLoadRect.height * _progressScale);
			GUI.DrawTexture(_characterLoadRect, _characterLoadingBack);
			GUI.BeginGroup (groupRect);
			GUI.DrawTexture(new Rect(0, 0, _characterLoadRect.width, _characterLoadRect.height), _characterLoadingFilled);
			GUI.EndGroup();
		}
	}

	public void CreateCharacter(PlayerEnum playerEnum)
	{		
		CreateChar(createCharacterWindow.characterName.text,
		           playerEnum.race,
		           playerEnum.classType,
		           playerEnum.gender,
		           playerEnum.skinColor,
		           playerEnum.face,
		           playerEnum.hairStyle,
		           playerEnum.hairColor,
		           playerEnum.facialHair,
		           1);	
		
		//== destroying loading Screen.heightield
//		DestroyInstantiatedShieldAnimation("char_created");
		
	}
	
	private void CreateChar(string name, Races race, Classes Class, Gender gender, int skin, int face, int hairStyle, int hairColor, int facialHair, int outfitId)
	{
		WorldPacket wp = new WorldPacket(OpCodes.CMSG_CHAR_CREATE, 8);
		
		wp.Append(name);
		wp.Append((byte)race);
		wp.Append((byte)Class);
		wp.Append((byte)gender);
		wp.Append((byte)skin);
		wp.Append((byte)face);
		wp.Append((byte)hairStyle);
		wp.Append((byte)hairColor);
		wp.Append((byte)facialHair);
		wp.Append((byte)outfitId);
		RealmSocket.outQueue.Add(wp);
	}

	public void ShowCreateCharacterWindow()
	{
		_player.SetActive(false);
		selectCharacterWindow.gameObject.SetActive(false);
		createCharacterWindow.gameObject.SetActive(true);
	}

	public void ShowSelectCharacterWindow()
	{
		_player.SetActive(true);
		createCharacterWindow.gameObject.SetActive(false);
		selectCharacterWindow.gameObject.SetActive(true);
	}
}