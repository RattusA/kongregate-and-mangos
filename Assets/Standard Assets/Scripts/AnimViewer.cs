﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class AnimViewer : MonoBehaviour
{
	public class mob
	{
		public string nameid;
		public string name_str;
		public  mob ( string id, string str )
		{
			nameid = id;
			name_str = str;
		}
	}

	public GameObject[] Characters = null;
	public GameObject[] Mobs = null;
	public static GameObject[] Weapons = null;
	
	//all the weapons in the game
	public GameObject[] onehandaxes = null;
	public GameObject[] twohandsaxes = null;
	
	public GameObject[] twohandshammers = null;
	public GameObject[] onehandhammers = null;
	
	public GameObject[] bows = null;
	public GameObject[] onehandswords = null;
	public GameObject[] twohandswords = null;
	public GameObject[] staves = null;
	public GameObject[] basic = null;
	public GameObject[] spears = null;
	public GameObject[] crossbows = null;
	public GameObject[] dual = null;
	public GameObject[] shields = null;
	
	public List<GameObject> weapons = new List<GameObject>();
	
//	int selected = 0;
	static public GameObject current_obj = null;
	byte stage1 = 1;
	byte stage2 = 0;
	bool  ischar;
//	Camera cam;//the camera object
//	float rot = 0;
	GameObject main_player;
	int current_weapon_type = -1;
	int current_armor_type = -1;
	TestPlayer test_player_script;
	static List<mob> mobs = new List<mob>();
	List<string> char_names = new List<string>();
	
	int curr_anim_mode = 0;
//	Vector3 addPos;
	float camRadius = 3.0f;
	Transform leftHand;
	Transform rightHand;
	Transform rightFinger;
	Transform leftFinger;
	Transform rightFinger1;
	Transform leftFinger1;
	
//	Transform rightThumb1;
//	Transform leftThumb1;

	Vector2 startPos;
	Animation anim = null;
	string current_anim = null;
	
	static string[] weaponAnmiations = new string[]
	{
		 "onehandaxe",
		 "twohandsaxe",
		 "onehandhamers",
		 "twohandshammer",
		 "bow",
		 "onehandsword",
		 "twohandsword",
		 "staff",
		 "basic",
		 "spear",
		 "crossbow",
		 "shields"
	};
	string current_weapon_anim = null;
	static string[] basic_anim = new string[]
	{
		 "run",
		 "idle",
		 "hit1",
		 "die",
		 "walk",
		 "swim",
		 "swim_idle",
		 "swim_back",
		 "jump",
		 "mount"
	};
	
	string[] AnimationMods = new string[]
	{
		 "Default",
		 "Once",
		 "Loop",
		 "Stop"
	};
	string[] ArmorsSets = new string[]
	{
		 "Standard",
		 "Recruit",
		 "Trainee",
		 "Elite",
		 "Veteran",
		 "Officer"
	};

	static string getMobName ( string id  )
	{
		//foreach(mob field in mobs)
		for(int m =0;m<mobs.Count;m++)
		{
			mob mm = mobs[m];
			if(mm.nameid == id)
				return mm.name_str;
		}
		return "undefined";
	}
	void  Start ()
	{
		Characters = Resources.LoadAll<GameObject>("prefabs/chars");
		Mobs = Resources.LoadAll<GameObject>("prefabs/mobs");
		Weapons = Resources.LoadAll<GameObject>("prefabs/weapons");
		
		onehandaxes = Resources.LoadAll<GameObject>("prefabs/weapons/Axes/OneHand");
		weapons.AddRange(onehandaxes);
		
		twohandsaxes = Resources.LoadAll<GameObject>("prefabs/weapons/Axes/TwoHands");
		weapons.AddRange(twohandsaxes);
		
		onehandhammers = Resources.LoadAll<GameObject>("prefabs/weapons/Maces/OneHand");
		weapons.AddRange(onehandhammers);
		
		twohandshammers = Resources.LoadAll<GameObject>("prefabs/weapons/Maces/TwoHands");
		weapons.AddRange(twohandshammers);
		
		bows = Resources.LoadAll<GameObject>("prefabs/weapons/Bows");
		weapons.AddRange(bows);
		
		onehandswords = Resources.LoadAll<GameObject>("prefabs/weapons/Swords/OneHand");
		weapons.AddRange(onehandswords);
		
		twohandswords = Resources.LoadAll<GameObject>("prefabs/weapons/Swords/TwoHands");
		weapons.AddRange(twohandswords);
		
		staves = Resources.LoadAll<GameObject>("prefabs/weapons/Staves");
		weapons.AddRange(staves);
		
		basic = Resources.LoadAll<GameObject>("prefabs/weapons/Claws");
		weapons.AddRange(basic);
		
		spears = Resources.LoadAll<GameObject>("prefabs/weapons/Polearms");
		weapons.AddRange(spears);
		
		crossbows = Resources.LoadAll<GameObject>("prefabs/weapons/CrossBows");
		weapons.AddRange(crossbows);
		
		shields = Resources.LoadAll<GameObject>("prefabs/weapons/Shields");
		weapons.AddRange(shields);
		
		//Debug.Log(Weapons.Length);
//		addPos = new Vector3(0.0f,0.0f,camRadius);
		GameObject obj = GameObject.FindWithTag("MainCamera");
//		cam = obj.GetComponent<Camera>();
		main_player = GameObject.FindWithTag("player");
		
		for(byte u =0;u<Characters.Length;u++)
		{
			GameObject curr = Characters[u];
			string s = curr.name;
			//Debug.Log("dupa aia: "+s);
			char_names.Add(s);
			//Debug.Log("dupa aia1: "+s);
			//GameObject.DestroyImmediate(curr, true);
			//Debug.Log("dupa aia1: "+s);
		}
		
		mobs.Add(new mob("1", "Ice Golem"));
		mobs.Add(new mob("169", "Trainer"));
		mobs.Add(new mob("175","Bat King"));
		mobs.Add(new mob("200","Goblin Cutter"));
		mobs.Add(new mob("365","Anteus"));
		mobs.Add(new mob("367","Troll Boss"));
		mobs.Add(new mob("368","Slimyr"));
		mobs.Add(new mob("373","Dark Orc"));
		mobs.Add(new mob("381", "Magma Golem"));
		mobs.Add(new mob("382", "Wolf Dog"));
		mobs.Add(new mob("389", "Rostoceros"));
		mobs.Add(new mob("441", "Feeler"));
		mobs.Add(new mob("503", "Rock Golem"));
		//    mobs.Add(new mob("607", "Rock Golem"));
		//   mobs.Add(new mob("617", "Ancient Ent"));
		mobs.Add(new mob("1100", "WereWolf"));
		mobs.Add(new mob("1287", "Gnoll Mace Fighter"));
		mobs.Add(new mob("1289", "Boar"));
		mobs.Add(new mob("1298","Larvum"));
		mobs.Add(new mob("1995", "Mekum Canibal"));
		mobs.Add(new mob("2153", "Armored Soul"));
		mobs.Add(new mob("2336", "Dark Orc"));
		mobs.Add(new mob("2361", "Grull"));
		mobs.Add(new mob("3167", "Scheletal Fighter"));
		mobs.Add(new mob("3320", "Richrous Naga"));
		mobs.Add(new mob("5035", "Scorpidus"));
		mobs.Add(new mob("7156", "Serpahumen"));
		mobs.Add(new mob("11415", "Flying Antaeus"));
		//GameObject pl = GameObject.Instantiate(Resources.Load("prefabs/objects/plane"));
	}
	
	void  EquipArmor ( string val ,   GameObject obj  )
	{
		Transform obj_transf= obj.transform.Find("obj");
		if(!obj_transf)
			return;
		
		obj_transf.gameObject.SetActive(false);
		obj_transf.gameObject.SetActive(true);
		
		Transform transf = obj.transform.Find("obj/Bip01");
		if(transf)
			transf.gameObject.SetActive(true);
		
		transf = obj.transform.Find("obj/Standard Face");
		if(transf)
			transf.gameObject.SetActive(true);
		
		transf = obj.transform.Find("obj/Standard Hair");
		if(transf)
			transf.gameObject.SetActive(true);
		try
		{
			transf = obj.transform.Find("obj/Standard Body");
			if(transf)
				transf.gameObject.SetActive(true);
			
			transf = obj.transform.Find("obj/Standard Arms");
			if(transf)
				transf.gameObject.SetActive(true);
			
			transf = obj.transform.Find("obj/Standard Belt");
			if(transf)
				transf.gameObject.SetActive(true);
			
			transf = obj.transform.Find("obj/Standard Belt1");
			if(transf)
				transf.gameObject.SetActive(true);
			transf = obj.transform.Find("obj/Standard Belt2");
			if(transf)
				transf.gameObject.SetActive(true);
		}catch( Exception e  ){Debug.Log(e);}
		try{
			obj.transform.Find("obj/Armory/"+val).gameObject.SetActive(true);
		}catch( Exception e  ){obj.transform.Find("obj/"+val).gameObject.SetActive(true); Debug.LogException(e);}
		//object.transform.Find("obj/Standard Body").gameObject.SetActive(true);
		//object.transform.Find("obj/Standard Arms").gameObject.SetActive(true);
	}

	void  EquipWeapon ( Transform _trLeftHand ,   Transform _trRightHand ,   GameObject item  )
	{
		GameObject go = null;
		//	Quaternion rot= Quaternion.identity;
		//rot.eulerAngles = Vector3.zero;

		//Loading Weapons
		go = GameObject.Instantiate(item);
		if(!go)
			return;	
		
		if(_trLeftHand)
			go.transform.parent = _trLeftHand;  
		else
			if(_trRightHand)  
				go.transform.parent = _trRightHand;  
		
		go.transform.localPosition = (2*rightFinger.localPosition+rightFinger1.localPosition + 2*leftFinger.localPosition+leftFinger1.localPosition)*0.25f;
		//go.transform.localPosition = rightFinger.localPosition + rightFinger1.localPosition;
		//go.transform.localPosition = (rightFinger.localPosition+rightFinger1.localPosition+rightThumb1.localPosition + 2*leftFinger.localPosition+leftFinger1.localPosition+leftThumb1.localPosition)*0.25f;
		//go.transform.localRotation = rot;
	}
//	void  LateUpdate ()
//	{
//		if(Input.GetMouseButtonDown(0))
//	{
//		startPos = Input.mousePosition;
//		//Debug.Log("startpos: "+startPos);
//	}
//	else
//	if(Input.GetMouseButton(0))
//	{
//		if(current_obj)
//		{
//			Vector2 dPos = startPos - Input.mousePosition;
//			rot -= dPos.x * Mathf.PI/180;
//			//Debug.Log("moving"+rot+"dpos: "+dPos+" startpos: "+startPos);
//			startPos = Input.mousePosition;
//			//Debug.Log("mp: "+ startPos);
//		}
//	}
//	addPos = Vector3(Mathf.Cos(rot)*camRadius,1.5f,Mathf.Sin(rot)*camRadius);
//	//cam.transform.Rotate(0,rot,0);//rotation.y = 180+rot;
//	//cam.transform.RotateAround();
//	cam.transform.position = addPos;
//	cam.transform.LookAt(Vector3(0,1,0));
//	
//	if(current_obj && anim && current_anim)
//	{
//		//anim[current_anim].wrapMode = current_anim_mode;
//		//anim.CrossFadeQueued(current_anim, 0.3f, QueueMode.PlayNow);
//		//Debug.Log(current_anim);
//	}
//	
//	
//		if(Input.GetAxis("Mouse ScrollWheel"))
//		{
//			if(Input.GetAxis("Mouse ScrollWheel") > 0)
//			{
//				camRadius-=0.05f;
//				if(camRadius<2.0f)
//				camRadius = 2.0f;
//			}
//			else
//			if(Input.GetAxis("Mouse ScrollWheel") < 0)
//			{
//				camRadius+=0.05f;
//			}
//		}
//	}
	
	void  OnGUI ()
	{
		byte i = 0;
		if(stage1 > 0)
		{
			GUI.Label( new Rect(Screen.width*0.79f,Screen.height*0.04f*2,Screen.width*0.2f, Screen.height*0.4f),"Monsters and NPC-s:");
			CapsuleCollider col;
			//Rigidbody rb;
			string mobName = ""; 
			int row = 0;
			
			for(i = 0;i<Mobs.Length;i++)
			{
				GameObject obj = Mobs[i];
				mobName = getMobName(obj.name);
				
				if(mobName == "undefined")
					continue;
				
				if(row<16)
				{
					if(GUI.Button( new Rect(Screen.width*0.79f,Screen.height*0.04f*(row+3),Screen.width*0.2f, Screen.height*0.04f),mobName))
					{
//						selected = i;
						if(current_obj)
							GameObject.Destroy(current_obj);
						current_obj = GameObject.Instantiate(Mobs[i]);
						col = current_obj.GetComponent<CapsuleCollider>();
						//rb = current_obj.GetComponent<Rigidbody>();
						//rb.useGravity = false;
						camRadius += col.radius;
						Destroy(current_obj.transform.Find("obj").gameObject.GetComponent<NPC>());
						test_player_script = current_obj.AddComponent<TestPlayer>();
						test_player_script.object_name = obj.name;
						test_player_script.ischar = false;
						current_obj.transform.position = main_player.transform.position + new Vector3(2,2,2);
						//	test_player_script.SendCreatedMessage();
						//cam.transform.position.z = camRadius;
						
						stage2 = 1;
						stage1 = 0;
						ischar = false;
					}
				}
				else
				{
					if(GUI.Button( new Rect(Screen.width*0.59f,Screen.height*0.04f*(row-15+2),Screen.width*0.2f, Screen.height*0.04f),mobName))
					{
//						selected = i;
						if(current_obj)
							GameObject.Destroy(current_obj);
						current_obj = GameObject.Instantiate(Mobs[i]);
						col = current_obj.GetComponent<CapsuleCollider>();
						//rb = current_obj.GetComponent<Rigidbody>();
						//rb.useGravity = false;
						camRadius += col.radius;
						Destroy(current_obj.transform.Find("obj").gameObject.GetComponent<NPC>());
						test_player_script = current_obj.AddComponent<TestPlayer>();
						test_player_script.object_name = obj.name;
						test_player_script.ischar = false;
						current_obj.transform.position = main_player.transform.position + new Vector3(2,2,2);
						//	test_player_script.SendCreatedMessage();
						
						//cam.transform.position.z = camRadius;
						
						stage2 = 1;
						stage1 = 0;
						ischar = false;
					}
				}
				
				row++;
				//if(selected==i)
				//	GUI.Label( new Rect(Screen.width*0.75f,Screen.height*0.1f*(i+1),Screen.width*0.2f, Screen.height*0.1f),"->");
			}
			
			GUI.Label( new Rect(0.0f,Screen.height*0.04f*2,Screen.width*0.2f, Screen.height*0.04f),"Characters:");
			for(i = 0;i<char_names.Count;i++)
			{
				//GameObject objc = Characters[i];
				string char_name = char_names[i];
				if(GUI.Button( new Rect(0.0f,Screen.height*0.04f*(i+3),Screen.width*0.2f, Screen.height*0.04f),char_name))
				{
//					selected = i;
					if(current_obj)
					{
						GameObject.Destroy(current_obj);
					}
					current_obj = GameObject.Instantiate<GameObject> (GameResources.Load<GameObject> ("prefabs/chars/"+char_name));
					col = current_obj.GetComponent<CapsuleCollider>();
					OtherPlayer ff = current_obj.transform.Find("obj").gameObject.GetComponent<OtherPlayer>();
					if(ff != null)
						Destroy(ff);
					current_obj.transform.position = main_player.transform.position + new Vector3(2,2,2);
					
					
					//camRadius += col.radius;
					//cam.transform.position.z = camRadius;
					
					stage2 = 1;
					stage1 = 0;
					ischar = true;
					test_player_script = current_obj.AddComponent<TestPlayer>();
					if(test_player_script)
					{
						test_player_script.object_name = char_name;
						test_player_script.ischar = true;
						//test_player_script.SendCreatedMessage();
						//test_player_script.EquipArmor("Standard");
						//test_player_script.SendUpdateData();
					}
					//GameObject obj = current_obj.transform.Find("obj/Officer").gameObject;
					//obj.SetActive(false);
					try
					{
						Transform transf = current_obj.transform.Find("obj/Armory/Officer");
						if(transf)
							transf.gameObject.SetActive(false);
						transf = current_obj.transform.Find("obj/Armory/Veteran");
						if(transf)
							transf.gameObject.SetActive(false);
						transf = current_obj.transform.Find("obj/Armory/Trainee");
						if(transf)
							transf.gameObject.SetActive(false);
						transf = current_obj.transform.Find("obj/Armory/Recruit");
						transf.gameObject.SetActive(false);
						transf = current_obj.transform.Find("obj/Armory/Elite");
						if(transf)
							transf.gameObject.SetActive(false);
					}catch( Exception e  ){Debug.Log(e);}
					//Transform[] allChildren = current_obj.GetComponentsInChildren<Transform>();
					leftHand = current_obj.transform.Find    ("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L ForeArm/Bip01 L Hand");
					if(!leftHand)//unele modele au denumita altfel structura de oase
						leftHand = current_obj.transform.Find("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Neck/Bip01 L Clavicle/Bip01 L UpperArm/Bip01 L Forearm/Bip01 L Hand");
					
					rightHand = current_obj.transform.Find("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Spine2/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R ForeArm/Bip01 R Hand");
					if(!rightHand)
						rightHand = current_obj.transform.Find("obj/Bip01/Bip01 Pelvis/Bip01 Spine/Bip01 Spine1/Bip01 Neck/Bip01 R Clavicle/Bip01 R UpperArm/Bip01 R Forearm/Bip01 R Hand");
					rightFinger = rightHand.Find("Bip01 R Finger0");
					leftFinger = rightHand.Find("Bip01 R Finger1");
					rightFinger1 = rightFinger.Find("Bip01 R Finger01");
					leftFinger1 = leftFinger.Find("Bip01 R Finger11");
					
//					rightThumb1 = rightFinger1.Find("Bip01 R Finger0Nub");
//					leftThumb1 = leftFinger1.Find("Bip01 R Finger1Nub");
				}
				//if(selected==i)
				//	GUI.Label( new Rect(Screen.width*0.75f,Screen.height*0.1f*(i+1),Screen.width*0.2f, Screen.height*0.1f),"->");
			}
		}
		else if(stage2 > 0)
		{
			//current_anim_mode = EditorGUI.Popup( new Rect(Screen.width*0.4f,Screen.height*0.2f,Screen.width*0.1f, Screen.height*0.08f),current_anim_mode,AnimMods);
			//current_anim_mode = GUI.SelectionGrid( new Rect(Screen.width*0.2f,Screen.height*0.1f,Screen.width*0.7f, Screen.height*0.08f),current_anim_mode,AnimationMods,5);
			int anim_mode = GUI.SelectionGrid( new Rect(Screen.width*0.4f,Screen.height*0.1f,Screen.width*0.4f, Screen.height*0.06f),curr_anim_mode,AnimationMods,5);
			anim = current_obj.GetComponentInChildren<Animation>();
			if(anim_mode != curr_anim_mode)
			{
				if(anim != null && current_anim != null)
				{
					if(anim_mode == 3)
					{
						anim_mode = 2;
						current_anim = anim.clip.name;
					}
					curr_anim_mode = anim_mode;
					Debug.Log("anim mode: "+curr_anim_mode);
					anim[current_anim].wrapMode = (WrapMode)curr_anim_mode;
					//anim.CrossFadeQueued(current_anim, 0.3f, QueueMode.PlayNow);
					test_player_script.ChangeAnimation(current_anim, curr_anim_mode);
					//	test_player_script.SendUpdateData();
					//Debug.Log("cur anim: "+current_anim+" "+curr_anim_mode);
				}
			}
			//Debug.Log("anim: "+current_anim_mode);
			if(!ischar)
			{
				GUI.Label( new Rect(0.0f,Screen.height*0.04f*2,Screen.width*0.2f, Screen.height*0.04f),"Monster Animations:");
				foreach(AnimationState state in anim) 
				{
					i++;
					if(GUI.Button( new Rect(0.0f,Screen.height*0.04f*(i+2),Screen.width*0.2f, Screen.height*0.04f),state.name))
					{
						anim[state.name].wrapMode = (WrapMode)curr_anim_mode;
						//anim.CrossFadeQueued(state.name, 0.3f, QueueMode.PlayNow);
						test_player_script.ChangeAnimation(state.name, curr_anim_mode);
						//test_player_script.SendUpdateData();
						current_anim = state.name;
					}
					//Debug.Log(state.name+" ");
				}
			}
			else
			{
				/*GUI.Label( new Rect(Screen.width*0.79f,Screen.height*0.2f,Screen.width*0.2f, Screen.height*0.4f),"Armors:");
			GUI.Label( new Rect(Screen.width*0.79f,Screen.height*0.25f,Screen.width*0.2f, Screen.height*0.4f),"Chest:");
			GUI.SelectionGrid( new Rect(Screen.width*0.4f,Screen.height*0.1f,Screen.width*0.7f, Screen.height*0.08f),curr_anim_mode,AnimationMods,5);
			GUI.Label( new Rect(Screen.width*0.79f,Screen.height*0.4f,Screen.width*0.2f, Screen.height*0.4f),"Shoulders:");
			GUI.Label( new Rect(Screen.width*0.79f,Screen.height*0.5f,Screen.width*0.2f, Screen.height*0.4f),"Pants:");
			GUI.Label( new Rect(Screen.width*0.79f,Screen.height*0.6f,Screen.width*0.2f, Screen.height*0.4f),"Boots:");*/
				int armor_type= GUI.SelectionGrid( new Rect(Screen.width*0.2f,Screen.height*0.85f,Screen.width*0.7f, Screen.height*0.04f),current_armor_type,ArmorsSets,6);
				int weapon_type= GUI.SelectionGrid( new Rect(Screen.width*0.0f,Screen.height*0.9f,Screen.width, Screen.height*0.1f),current_weapon_type,weaponAnmiations,5);
				
				if(armor_type != current_armor_type)
				{
					test_player_script.EquipArmor(ArmorsSets[armor_type]);
					//	test_player_script.SendUpdateData();
					current_armor_type = armor_type;
				}
				if(weapon_type != current_weapon_type)
				{
					current_weapon_anim = weaponAnmiations[weapon_type];
					current_weapon_type = weapon_type;
				}
				
				/*if(!current_weapon_anim)
			{
				GUI.Label( new Rect(0.0f,0.0f,Screen.width*0.2f, Screen.height*0.06f),"Weapon animations:");
				for(i =0;i<weaponAnmiations.length;i++)
				{
					if(GUI.Button( new Rect(0.0f,Screen.height*0.06f*(i+1),Screen.width*0.2f, Screen.height*0.06f),weaponAnmiations[i]))
					{
						
						current_weapon_anim = weaponAnmiations[i];
						for(byte ww=0;ww<Weapons.length;ww++)
						{
							//Debug.Log(current_weapon_anim+" "+Weapons[ww].name);
							GameObject objw = Weapons[ww];
							if(current_weapon_anim == objw.name)
								EquipWeapon(null,rightHand,Weapons[ww]);
						}
						//anim.CrossFadeQueued(state.name, 0.3f, QueueMode.PlayNow);
					}
				}
			}*/
				if(current_weapon_anim != null)
				{
					GUI.Label( new Rect(Screen.width*0.7f,Screen.height*0.03f*3,Screen.width*0.3f, Screen.height*0.06f),current_weapon_anim+" weapons : ");
					//switch(current_weapon_type)
					//{
					//case 0 : 
					int k= 0;
					GameObject[] curr_weapons = new GameObject[]{weapons[current_weapon_type]};
					for(int ww=0; ww<curr_weapons.Length;ww++)
					{
						GameObject weapon = curr_weapons[ww];
						try
						{
							string name= ItemDBC.getName(uint.Parse(weapon.name)) ;
							
							if(name == "Undefined")
								continue;
							
							if(GUI.Button( new Rect(Screen.width*0.8f,Screen.height*0.03f*(k+4),Screen.width*0.2f, Screen.height*0.03f), name))
							{
								test_player_script.current_weapon = weapon.name;
								
								if(int.Parse(weapon.name)>=22 && int.Parse(weapon.name)<=28)
								{
									test_player_script.EquipWeapon(leftHand,null,weapon);
								}
								else
								{
									test_player_script.EquipWeapon(null,rightHand,weapon);
								}
								//	test_player_script.SendUpdateData();
							}
							k++;
						}catch( Exception e  ){Debug.Log(e);}
					}//break;
					
					//}
					
					byte j = 3;
					GUI.Label( new Rect(0.0f,Screen.height*0.03f*j,Screen.width*0.3f, Screen.height*0.06f),current_weapon_anim+" animations: ");
					
					foreach(AnimationState state in anim) 
					{
						if(state.name.StartsWith(current_weapon_anim))
						{
							//Debug.Log(state.name+" ");
							j++;
							if(GUI.Button( new Rect(0.0f,Screen.height*0.03f*j,Screen.width*0.3f, Screen.height*0.03f),state.name))
							{
								anim[state.name].wrapMode = (WrapMode)curr_anim_mode;
								//anim.CrossFadeQueued(state.name, 0.3f, QueueMode.PlayNow);
								test_player_script.ChangeAnimation(state.name, curr_anim_mode);
								///	test_player_script.SendUpdateData();
								current_anim = state.name;
							}
						}
					}
					if(current_weapon_anim == "basic")
					{
						for(int b=0; b<basic_anim.Length;b++)
						{
							if(anim[basic_anim[b]])
								if(GUI.Button( new Rect(0.0f,Screen.height*0.03f*(b+j+1),Screen.width*0.3f, Screen.height*0.03f),basic_anim[b]))
							{
								anim[basic_anim[b]].wrapMode = (WrapMode)curr_anim_mode;
								//anim.CrossFadeQueued(basic_anim[b], 0.3f, QueueMode.PlayNow);
								test_player_script.ChangeAnimation(basic_anim[b], curr_anim_mode);
								//	test_player_script.SendUpdateData();
								current_anim = basic_anim[b];
							}
						}
					}
				}
			}
			if(GUI.Button( new Rect(Screen.width*0.9f,Screen.height*0.7f,Screen.width*0.1f, Screen.height*0.08f),"Back"))
			{
				if(stage2 > 0)
				{
					if(current_obj)
					{
						GameObject.Destroy(current_obj);
						anim = null;
					}
					stage1 = 1;
					stage2 = 0;
					current_weapon_anim = null;
				}
			}
			
		}
		if(GUI.Button( new Rect(Screen.width*0.9f,Screen.height*0.8f,Screen.width*0.1f, Screen.height*0.08f),"Exit"))
		{
			Destroy(gameObject);
			if(current_obj)
				GameObject.Destroy(current_obj);
			/*byte start_state = 0;
		logIn.state = start_state;
		cam.transform.position.z = -10;
		cam.transform.position.y = 1;
		cam.transform.position.x = 0;
		Quaternion rot= Quaternion.identity;
		rot.eulerAngles = Vector3.zero;
		cam.transform.rotation = rot;*/
		}
	}
}