using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;


public class DBC
{
	public static String pathh = "";
	public static bool needShift = true;
	public static void SetPath(String gg) {pathh = gg;}
	
	//var list = new System.Collections.Generic.List.<String>();
	
	public DBC(String path)
	{
		DBC.SetPath(path);
	}
}

public class AreaTriggerDBS : _DBC<AreaTriggerEntry>{}

public class ItemDBS : _DBC<ItemEntry>
{
	public ItemDBS (): base()
	{}
	public ItemDBS (String fileName): base(fileName){}
	public ItemDBS (String fileName, String path): base(fileName, path){}
}
public class ServerItemDBS : _DBC<ServerItemEntry>
{
	public ServerItemDBS (): base()
	{}
	public ServerItemDBS (String fileName): base(fileName){}
	public ServerItemDBS (String fileName, String path): base(fileName, path){}
}
//public class ItemClassDBS : _DBC<ItemClassEntry>{}
//public class ItemSubClassDBS : _DBC<ItemSubClassEntry>{}
public class ItemDisplayInfoDBS : _DBC<ItemDisplayInfoEntry>
{
	public ItemDisplayInfoDBS (): base()
	{}
	public ItemDisplayInfoDBS (String fileName): base(fileName){}
	public ItemDisplayInfoDBS (String fileName, String path): base(fileName, path){}
}
//public class ItemInventoryTypeDBS : _DBC<ItemInventoryTypeEntry>{}

public class CreatureTypeDBS : _DBC<CreatureTypeEntry>{}
public class CreatureDisplayInfoDBS : _DBC<CreatureDisplayInfoEntry>
{
	public CreatureDisplayInfoDBS (): base()
	{}
	public CreatureDisplayInfoDBS (String fileName): base(fileName){}
	public CreatureDisplayInfoDBS (String fileName, String path): base(fileName, path){}
}

public class SpellDBS : _DBC<SpellEntry>
{
	public SpellDBS (): base()
	{}
	public SpellDBS (String fileName): base(fileName){}
	public SpellDBS (String fileName, String path): base(fileName, path){}
}
//public class SpellDurationDBS:_DBC<SpellDurationEntry>{}
//public class SpellCastTimesDBS:_DBC<SpellCastTimesEntry>{}
public class SpellIconDBS:_DBC<SpellIconEntry>
{
	public SpellIconDBS (): base()
	{}
	public SpellIconDBS (String fileName): base(fileName){}
	public SpellIconDBS (String fileName, String path): base(fileName, path){}
}

public class FactionDBS:_DBC<FactionEntry>{}
public class SkillLineAbilityDBS:_DBC<SkillLineAbilityEntry>
{
	public SkillLineAbilityDBS (): base()
	{}
	public SkillLineAbilityDBS (String fileName): base(fileName){}
	public SkillLineAbilityDBS (String fileName, String path): base(fileName, path){}
}

public class GameObjectDisplayInfoDBS:_DBC<GameObjectDisplayInfoEntry>
{
	public GameObjectDisplayInfoDBS (): base()
	{}
	public GameObjectDisplayInfoDBS (String fileName): base(fileName){}
	public GameObjectDisplayInfoDBS (String fileName, String path): base(fileName, path){}
}

public class CharacterRaceInfoDBS:_DBC<CharacterRaceEntry>{}
public class CharacterClassInfoDBS:_DBC<CharacterClassEntry>{}
public class CharacterGenderInfoDBS:_DBC<CharacterGenderEntry>{}
public class TalentDBS:_DBC<TalentEntry>{}
public class TalentTabDBS:_DBC<TalentTabEntry>{}

public class CustomFaceDBS:_DBC<CustomFaceEntry>{}
public class CustomHairStyleDBS:_DBC<CustomHairStyleEntry>{}
public class CustomFacialHairDBS:_DBC<CustomFacialHairEntry>{}
public class CustomHairColorDBS:_DBC<CustomHairColorEntry>{}
public class CustomSkinColorDBS:_DBC<CustomSkinColorEntry>{}

public class SpellItemEnchantmentDBS:_DBC<SpellItemEnchantmentEntry>{}
public class LockDBS:_DBC<LockEntry>{}

public class GemPropertiesDBS:_DBC<GemPropertiesEntry>
{
	public GemPropertiesDBS (): base()
	{}
	public GemPropertiesDBS (String fileName): base(fileName){}
	public GemPropertiesDBS (String fileName, String path): base(fileName, path){}
}

//WorldMapArea
public class WorldMapAreaDBS:_DBC<WorldMapAreaEntry> {}
// This DBC stores Glyph IDs used in Character_glyphs glyphs table.
public class GlyphPropertiesDBS:_DBC<GlyphPropertiesEntry>
{
	public GlyphPropertiesDBS (): base()
	{}
	public GlyphPropertiesDBS (String fileName): base(fileName){}
	public GlyphPropertiesDBS (String fileName, string path): base(fileName, path){}
}

public class _DBC<T> where T : new()
{
	public List<T> colection = new List<T>();
	protected List<uint> Key = new List<uint>();
	
	protected List<string> stri = new List<string>();
	protected List<int> offsets = new List<int>();
	/*
1	Signature 	  String 	  (4-bytes) string, always 'WDBC'
2	Records 	  Integer	(4-bytes) number of records in the file
3	Fields 		 Integer	(4-bytes) number of fields per record
4	Record Size 	  Integer	(4-bytes) Fields*FieldSize (FieldSize is usually 4, but not always)
5	String Block Size	Integer	Size of the string block
	 */
	protected uint Signature;
	public  uint Records;
	protected uint Fields;
	protected uint RecordWidth;
	protected uint StringSize;
	protected byte[] stringBuffer;

	protected uint StringOffset;
	public String fName; 
	
	protected BinaryReader reader;

	protected  _DBC(): this(null){}

	/*
Meniu  display interface
	*/
	protected static float WIDTH = 0.8F;
	protected static float HEIGTH = 0.8F;
	protected byte mod = 0;
	protected Vector2 scrollPos = Vector2.zero;
	protected Vector2 scrollPos2 = Vector2.zero;
	protected String[] toolBarStrings;
	protected int toolbarInt = -1;
	protected int editMod = -1;
	protected List<bool> toggles = new List<bool>();
	public String[] fields;
	protected Dictionary<int, string> readStringCollection = new Dictionary<int, string>(); // Dictionari<offset, string>
	protected Dictionary<string, uint> writeStringCollection = new Dictionary<string, uint>(); // Dictionari<string, offset>

	protected void CheckInterval(ref int from, ref int to)
	{
 		if(from < 0 )
			from = 0;
			
		if(to < 0)
			to = 0;
			
		if(to >= colection.Count)
	  to= colection.Count-1; 
			
		if(from > to)
			from = to;
	}

	public _DBC (String fileName, String path = "")
	{
		fName = "";
//			FileStream stream;
		Stream stream;
		try
		{
			if (String.IsNullOrEmpty( fileName ))
			{
				String aux = this.GetType().Name;
				fileName = aux.Substring (0, aux.Length - 3);//removes "DBS" substring
				fName = fileName;
				TextAsset asset = Resources.Load<TextAsset> ("DBS/" + fileName);
				stream = new MemoryStream(asset.bytes);
				asset = null;
//				stream = new FileStream (DBC.pathh + "/" + fileName + ".dbs", FileMode.Open, FileAccess.Read);
			}
			else
			{
#if UNITY_EDITOR && !UNITY_WEBPLAYER && !UNITY_WEBGL
				if (path.Equals(String.Empty))
				{
#endif
					TextAsset asset = Resources.Load<TextAsset> ("DBS/" + fileName);
					stream = new MemoryStream(asset.bytes);
					asset = null;
//					stream = new FileStream (fileName, FileMode.Open, FileAccess.Read);
#if UNITY_EDITOR && !UNITY_WEBPLAYER && !UNITY_WEBGL
				}
				else
				{
					Debug.Log (path +"   " + fileName);
					byte[] array = File.ReadAllBytes(path + fileName);
					stream = new MemoryStream(array);
				}
#endif
			}

			//Debug.Log ("************Loading " + FileName + ".dbs");
	
			fName = fileName;
			reader = new BinaryReader (stream);
				
			Signature = reader.ReadUInt32 ();
			Records = reader.ReadUInt32 ();
			Fields = reader.ReadUInt32 ();
			RecordWidth = reader.ReadUInt32 ();
			StringSize = reader.ReadUInt32 ();

			StringOffset = Records * RecordWidth + 20;//gets the position from the binary file from were we can start reading strings
			// Debug.Log ("Read - sig: " + Signature + " rec: " + Records + " flds: " + Fields + " RecSz: " + RecordWidth + " Str: " + StringSize);

//				byte[] strings = new byte[StringSize + 1];
			stringBuffer = new byte[StringSize + 1];

			reader.BaseStream.Position = StringOffset;//moves the cursor to the beging of the string zone
			reader.BaseStream.Read (stringBuffer, 0, (int)StringSize);//reads all strings
			reader.BaseStream.Position = 20;//moves cursor back; 20 is the header size.
				
			var enc = new System.Text.UTF8Encoding ();
				
			String[] str = enc.GetString (stringBuffer).Split ('\0');//splits the sting buffer into strings base on the '\0' character
			int oo = 1;
			if(str.Length > 0)
			{
				for (int k = 0; k < str.Length; k++)
				{
					//if(fName == "CharacterRaceInfo")
					//{

					stri.Add (str[k]);
					offsets.Add (oo);
					readStringCollection[oo] = str[k];
					oo += str[k].Length + 1;
					str[k] = null;
					//}
				}
			}
		  
			uint key=0;   
			uint n;

//			if(Records > 80000)
//		   	  n= 80000;
//			else
			  n = Records;   
				
			for (int i = 0; i < n; i++)
			{
				MoveToRecord(i);
				colection.Add(Read(ref key));
				Key.Add(key);
				
				toggles.Add(false);
			}
			stream.Close();

			stri = null;
			offsets = null;
//			readStringCollection.Clear ();

		   	fields = new String[Fields];
			for(int j=0;j<Fields;j++)
				fields[j] = "";
			
			toolBarStrings = new String[4];
			toolBarStrings[0] = "Delete";
			toolBarStrings[1] = "Edit";
			toolBarStrings[2] = "Save";
			toolBarStrings[3] = "Add";

			stream = null;
					
		}
		catch(Exception e)
		{
			Debug.Log(e+"		 "+fileName +"  not found!");
			fName = "";
			stream = null;
		};


   }

	private void MoveToRecord(int i)
	{
		if (reader.BaseStream.Position != 20 + i * RecordWidth) {
			reader.BaseStream.Position = 20 + i * RecordWidth;
		}
	}

	private string ReadString(int offset)
	{
		string result = "";
		if(readStringCollection.ContainsKey(offset))
		{
			result = readStringCollection[offset];
		}
		if(string.IsNullOrEmpty(result) && readStringCollection.ContainsKey(offset+1))
		{
			result = readStringCollection[offset+1];
		}
		return result;
//		if (offset > 0 && offset < StringSize)
//		{
//			int j=0;
//			for(;j<offsets.Count;j++)
//			{
//				if(offsets[j] == offset)
//					break;
//			}
//			if(j >= 0)
//				return stri[j];
//			else
//				return "";
//		}
//		else
//		{
//			return "";
//		}
	}
 
	private T Read(ref uint key)
	{
		T o = new T();
		foreach (FieldInfo v in typeof(T).GetFields())
		{
 			if (v.FieldType == typeof(byte))
 			{
 				byte bt = reader.ReadByte();
 				 v.SetValue(o, bt);
 			}
 			else if (v.FieldType == typeof(string))
			{
				int offset = reader.ReadInt32();
				//Debug.Log(" reading offset  "+offset);
				string str = ReadString(offset);
			  //  Debug.Log(str+" reading offset  "+offset);

				v.SetValue(o, str);
			}
			else if (v.FieldType == typeof(LocalizedString))
			{
				LocalizedString ls ;//= new LocalizedString();
				ls.Strings = new string[16];
				ls.stringOffset = new int[16];
			   
				for (int i=0;i<16; i++)
				{   
					ls.stringOffset[i] = reader.ReadInt32();
					ls.Strings[i] = ReadString(ls.stringOffset[i]);
				}
				ls.mask = reader.ReadInt32();
			   
				v.SetValue(o, ls);
			}
			else if (v.FieldType == typeof(float))
			{
				Single s = reader.ReadSingle();
				v.SetValue(o, s);
			}
			else if (v.FieldType == typeof(int))
			{
				Int32 i = reader.ReadInt32();
				 v.SetValue(o, i);
			
				if(v.Name == "ID")
				{	
//						Debug.Log("textid: " + i);
					key = (uint)i; 
				}
				if(v.Name == "area_id")
				{
					key = (uint)i;
				}
				
			}
			else if(v.FieldType == typeof(Int3))
			{
				Int3 ii =  new Int3();
				ii.array  = new int[3];
				
				for(int i =0;i<3;i++)
				{
					ii.array[i] = reader.ReadInt32();
				}
				v.SetValue(o, ii);
			}
			else if(v.FieldType == typeof(Int2))
			{
				Int2 ii = new Int2();
				ii.array  = new int[2];
				
				for(int i =0;i<2;i++)
				{
			   		ii.array[i] = reader.ReadInt32();
				}
				v.SetValue(o, ii);
			}
			else if(v.FieldType == typeof(Int8))
			{
				Int8 ii = new Int8();
				ii.array  = new int[8];
				
				for(int i =0;i<8;i++)
				{
					ii.array[i] = reader.ReadInt32();
				}
				v.SetValue(o, ii);
			}
			else if(v.FieldType == typeof(Float3))
			{
				Float3 ii = new Float3();
				ii.array  = new float[3];
				
				for(int i =0;i<3;i++)
				{
					ii.array[i] = reader.ReadSingle();
				}
				v.SetValue(o, ii);
			}
			else if (v.FieldType == typeof(uint))
			{
				UInt32 i = reader.ReadUInt32();
				if(DBC.needShift)
					i = ((i^243)>>2);
				v.SetValue(o, i);
				
				if(v.Name == "ID")
				{	
					key = i; 
				}
				if(v.Name == "area_id")
				{
					key = i; 
				}
				
			}
			else if (v.FieldType == typeof(bool))
			{
				UInt32 i = reader.ReadUInt32();
				v.SetValue(o, (i > 0));
			}/*
			else if (v.FieldType.BaseType == typeof(Enum))
			{
				if (Enum.GetUnderlyingType(v.FieldType) == typeof(UInt32))
				{
					UInt32 i = reader.ReadUInt32();
					v.SetValue(o, i);
				}
				else if (Enum.GetUnderlyingType(v.FieldType) == typeof(int))
				{
					int i = reader.ReadInt32();
					v.SetValue(o, i);
				}
				else
				{
					reader.ReadBytes(4);
				}
			}
			else if (v.FieldType.BaseType == typeof(int))
			{
				int i = reader.ReadInt32();
				v.SetValue(o, i);
			}
			else
			{
			   reader.ReadBytes(4);
			}*/
		}

		return o;
	}
	
#if UNITY_EDITOR
	public void Write()
	{
		Write(DBC.pathh+"/DBS/"+fName+".dbs");
	}

	public void Write(string fileName)
	{
		FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
		//Debug.Log("************Writing "+fName+".dbs");

		byte[] buffer;	
		
		Signature = 1128416343;
		buffer =  BitConverter.GetBytes((int)Signature);
		stream.Write(buffer,0,4);

		Records = (uint)colection.Count;
		buffer =  BitConverter.GetBytes((int)Records);
		stream.Write(buffer,0,4);
		
		buffer =  BitConverter.GetBytes((int)Fields);
		stream.Write(buffer,0,4);

   		buffer =  BitConverter.GetBytes((int)RecordWidth);
		stream.Write(buffer,0,4);

		
		buffer =  BitConverter.GetBytes((int)StringSize);
		stream.Write(buffer,0,4);

		writeStringCollection = new Dictionary<string, uint>();
		writeStringCollection.Add ("\0", 0);
		stringBuffer = new byte[0];
		stringBuffer = ConcatArrays(stringBuffer, System.Text.Encoding.UTF8.GetBytes ("\0"));
		off = 1;
		for(int i = 0;i < colection.Count;i++)
		{
			write(colection[i],stream);
		}

		stream.Write(stringBuffer, 0, stringBuffer.Length);
	
	
		// temporary commented code, becouse we just need to rewrite original strings
		stream.Position = 16;
		buffer = BitConverter.GetBytes(stringBuffer.Length);
		stream.Write(buffer,0,buffer.Length);
		
		//Debug.Log("Write - sig: "+Signature+" rec: "+Records+" flds: "+Fields+" RecSz: "+RecordWidth+" Str: "+(offsets[offsets.Count-1]));
		stream.Close();
		
		//stri = null;
		//offsets = null;
	}
	
//	public void GetsOffsetsAndStrings(T obj,ref int offset)
//	{
//  		foreach (FieldInfo r in typeof(T).GetFields())
//		{
//			if(r.FieldType == typeof(string))
//			{
//				int i = offset;
//				Debug.Log("nooffset "+i+" Adding "+r.GetValue(obj));
//				offsets.Add(i);					
//				stri.Add((string) (r.GetValue(obj)+"\0"));
//				offset += ( (string) (r.GetValue(obj)) ).Length+1;
//				
//			}
//			else if(r.FieldType == typeof(LocalizedString))
//			{
//				LocalizedString ii = (LocalizedString)r.GetValue(obj);
//				
//				for(int k = 0;k<16;k++)
//				{	
//					int i = offset;
//					//Debug.Log(k+"offset "+i+" Adding "+ii.Strings[k]);
//					offsets.Add(i);					
//					stri.Add(((string)(ii.Strings[k])+"\0"));
//					offset += (ii.Strings[k].Length+1);
//				}
//			}					
//		}
//	}
	
	uint off = 1;
	private void write(T obj, FileStream stream)
	{
//			try{
		byte[] buffer;
		
		foreach (FieldInfo r in typeof(T).GetFields())
		{
			if(r.FieldType == typeof(int))
			{
			//	Debug.Log(""+(int)(r.GetValue(obj)));
				buffer = BitConverter.GetBytes((int)(r.GetValue(obj)));
				stream.Write(buffer,0,buffer.Length);
			}
			else if(r.FieldType == typeof(uint))
			{
				buffer = BitConverter.GetBytes((uint)(r.GetValue(obj)));
				stream.Write(buffer,0,buffer.Length);
			}
			else if(r.FieldType == typeof(float))
			{
				buffer = BitConverter.GetBytes((float)(r.GetValue(obj)));
				stream.Write(buffer,0,buffer.Length);				  	
			}
			else 
			  
			if(r.FieldType == typeof(string))
			{
//				try{
//				buffer = BitConverter.GetBytes((int)(offsets[stri.IndexOf((string)r.GetValue(obj))]));
//				stream.Write(buffer,0,buffer.Length);  
//				off++;
//				}catch(Exception e){Debug.LogException(e);}
				string value;
				if( (obj != null) && (r.GetValue(obj) != null) )
				{
				   value = (string)r.GetValue(obj);
				}
				else
				{
					value = "";
				}
				value += "\0";
				uint offset = off;
				if(writeStringCollection.ContainsKey(value))
				{
					offset = writeStringCollection[value];
				}
				else
				{
					byte[] utf8Buffer = System.Text.Encoding.UTF8.GetBytes (value);
					stringBuffer = ConcatArrays(stringBuffer, utf8Buffer);
					writeStringCollection[value] = off;
					off += (uint)utf8Buffer.Length;
				}
				buffer = BitConverter.GetBytes((int)offset);
				stream.Write(buffer,0,buffer.Length);
			}
			else if(r.FieldType == typeof(byte))
			{
				buffer = BitConverter.GetBytes((byte) r.GetValue(obj));
				stream.Write(buffer,0,1);	  
				   		  	
			}
			else if(r.FieldType == typeof(Int2))
			{
				try
				{
					Int2 ii = (Int2)r.GetValue(obj);
					for(int k =0;k<2;k++)
					{
						buffer = BitConverter.GetBytes((int)ii.array[k]);
						stream.Write(buffer,0,buffer.Length); 
					}
				}
				catch(Exception e)
				{
					SpellEntry spellPrototype = new SpellEntry();
					spellPrototype = obj as SpellEntry;
					Int2 ii = (Int2)r.GetValue(obj);
					Debug.LogError(""+spellPrototype.ID+" "+spellPrototype.SpellName.Strings[0]+" "+r.Name+" "+
				               ii.array[0]+" "+ii.array[1]);
				}
			}
			else if(r.FieldType == typeof(Int3))
			{
				Int3 ii = (Int3)r.GetValue(obj);
				for(int k =0;k<3;k++)
			  	{
					buffer = BitConverter.GetBytes((int)ii.array[k]);
					stream.Write(buffer,0,buffer.Length); 
			  	}
			}
			else if(r.FieldType == typeof(Int8))
			{
			  	Int8 ii = (Int8)r.GetValue(obj);
			  	for(int k =0;k<8;k++)
			  	{
					buffer = BitConverter.GetBytes((int)ii.array[k]);
					stream.Write(buffer,0,buffer.Length); 
			  	}
			}
			else if(r.FieldType == typeof(Float3))
			{
			  	Float3 ii = (Float3)r.GetValue(obj);
			  	for(int k =0;k<3;k++)
			  	{
					buffer = BitConverter.GetBytes((float)ii.array[k]);
					stream.Write(buffer,0,buffer.Length); 
			  	}
			}
			else if(r.FieldType == typeof(LocalizedString))
			{
			  	LocalizedString ii = (LocalizedString)r.GetValue(obj);
			  	
			  	for(int k =0;k<16;k++)
			  	{
//				  		Debug.Log(ii.Strings[k]+"   "+(int)(offsets[off]));
//						buffer = BitConverter.GetBytes((int)(offsets[off]));
//					buffer = BitConverter.GetBytes(ii.stringOffset[k]);
//					stream.Write(buffer,0,buffer.Length);  
//					off++;
					string value;
					if( (obj != null) && !string.IsNullOrEmpty(ii.Strings[k]) )
					{
						value = ii.Strings[k];
					}
					else
					{
						value = "";
					}
					value += "\0";
					uint offset = off;
					if(writeStringCollection.ContainsKey(value))
					{
						offset = writeStringCollection[value];
					}
					else
					{
						byte[] utf8Buffer = System.Text.Encoding.UTF8.GetBytes (value);
						stringBuffer = ConcatArrays(stringBuffer, utf8Buffer);
						writeStringCollection[value] = off;
						off += (uint)utf8Buffer.Length;
					}
					buffer = BitConverter.GetBytes((int)offset);
					stream.Write(buffer,0,buffer.Length);
			  	} 
				buffer = BitConverter.GetBytes((int)(100));
				stream.Write(buffer,0,buffer.Length);   		  	
			}
		}

//			}catch(Exception e){Debug.LogException(e);}

	}
	
	private void SetTheEditRecord(ref T obj)
	{
		int i=0;
		foreach (FieldInfo r in typeof(T).GetFields())
		{
			if(r.FieldType == typeof(int))
			  {
			  	try{
					r.SetValue(obj, int.Parse(fields[i]));
			  	}catch(Exception e){r.SetValue(obj, (int)(0)); Debug.LogException(e);}
			  	
			  	i++;
			  }
			  else if(r.FieldType == typeof(uint))
			  {
				try{
					r.SetValue(obj, (uint)int.Parse(fields[i]));
			  	}catch(Exception e){r.SetValue(obj, (uint)(0)); Debug.LogException(e);}
			  	
			  	i++;
			  }else if(r.FieldType == typeof(float))
			  {
   		  		try{
					r.SetValue(obj, float.Parse(fields[i]));
			  	}catch(Exception e){r.SetValue(obj, (float)(0.0)); Debug.LogException(e);}
			  	
			  	i++;
			  }else if(r.FieldType == typeof(string))
			  {
				//if(fields[i].Length > 0)
			  	//{
					r.SetValue(obj, ""+(fields[i]));
				//}
				//else
				//{
				//	r.SetValue(obj, "null");
				//}
				
				i++;						
			  }else if(r.FieldType == typeof(byte))
			  {
				try{
					r.SetValue(obj, byte.Parse(fields[i]));
			  	}catch(Exception e){r.SetValue(obj, (byte)(0)); Debug.LogException(e);}
			  	
			  	i++;
			  }else if(r.FieldType == typeof(Int2))
			  {
			  	Int2 ii = new Int2();
			  	ii.array = new int[2];
			  	
			  	for(int k=0;k<2;k++)
			  	{
					try{
						ii.array[k] = int.Parse(fields[i]);
			  		}catch(Exception e){ii.array[k] = (int)(0); Debug.LogException(e);}
			  	 i++;
			  	}
			  	r.SetValue(obj, ii);
			  }else if(r.FieldType == typeof(Int3))
			  {
			  	Int3 ii = new Int3();
			  	ii.array = new int[3];
			  	
			  	for(int k=0;k<3;k++)
			  	{
					try{
						ii.array[k] = int.Parse(fields[i]);
			  		}catch(Exception e){ii.array[k] = (int)(0); Debug.LogException(e);}
			  	 i++;
			  	}
			  	
			  	r.SetValue(obj, ii);
			  }else if(r.FieldType == typeof(Int8))
			  {
			  	Int8 ii = new Int8();
			  	ii.array = new int[8];
			  	
			  	for(int k=0;k<8;k++)
			  	{
					try{
						ii.array[k] = int.Parse(fields[i]);
			  		}catch(Exception e){ii.array[k] = (int)(0); Debug.LogException(e);}
			  	 	i++;
			  	}
			  	
			  	r.SetValue(obj, ii);
			  }else if(r.FieldType == typeof(Float3))
			  {
			  	Float3 ii = new Float3();
			  	ii.array = new float[3];
			  	
			  	for(int k=0;k<3;k++)
			  	{
					try{
						ii.array[k] = float.Parse(fields[i]);
			  		}catch(Exception e){ii.array[k] = (float)(0); Debug.LogException(e);}
			  	 i++;
			  	}
			  	
			  	r.SetValue(obj, ii);
			  }else if(r.FieldType == typeof(LocalizedString))
			  {
			  	LocalizedString ii = new LocalizedString();
			  	ii.Strings = new string[16];
			  	
			  	for(int k=0;k<16;k++)
			  	{
  					//if(fields[i].Length>0)
  		  			//{
						ii.Strings[k]= ""+fields[i];
					//}
					//else
					//{
					//	ii.Strings[k]= "null";
					//}
					
				//	Debug.Log(ii.Strings[k]+"   "+fields[i]);		
					i++;
			  	}
			  	ii.mask = 999;
			  	i++;
			  	r.SetValue(obj, ii);
			  }
		}
	}
	
	private void SetTheEditRecord(ref T obj, T newRow)
	{
		int i=0;
		foreach (FieldInfo r in typeof(T).GetFields())
		{
			if(r.FieldType == typeof(int))
			{
				try{
					r.SetValue(obj, (int)r.GetValue(newRow));
				}catch(Exception e){r.SetValue(obj, (int)(0)); Debug.LogException(e);}
				
				i++;
			}
			else if(r.FieldType == typeof(uint))
			{
				try{
					r.SetValue(obj,  (uint)r.GetValue(newRow));
				}catch(Exception e){r.SetValue(obj, (uint)(0)); Debug.LogException(e);}
				
				i++;
			}else if(r.FieldType == typeof(float))
			{
				try{
					r.SetValue(obj,  (float)r.GetValue(newRow));
				}catch(Exception e){r.SetValue(obj, (float)(0.0)); Debug.LogException(e);}
				
				i++;
			}else if(r.FieldType == typeof(string))
			{
				//if(fields[i].Length > 0)
				//{
					r.SetValue(obj, (string)r.GetValue(newRow));
				//}
				//else
				//{
				//	r.SetValue(obj, "null");
				//}
				
				i++;						
			}else if(r.FieldType == typeof(byte))
			{
				try{
					r.SetValue(obj,  (byte)r.GetValue(newRow));
				}catch(Exception e){r.SetValue(obj, (byte)(0)); Debug.LogException(e);}
				
				i++;
			}else if(r.FieldType == typeof(Int2))
			{
				Int2 ii = new Int2();
				ii.array = new int[2];
				
				for(int k=0;k<2;k++)
				{
					try{
						ii.array[k] = ((Int2)r.GetValue(newRow))[k];
					}catch(Exception e){ii.array[k] = (int)(0); Debug.LogException(e);}
					i++;
				}
				r.SetValue(obj, ii);
			}else if(r.FieldType == typeof(Int3))
			{
				Int3 ii = new Int3();
				ii.array = new int[3];
				
				for(int k=0;k<3;k++)
				{
					try{
						ii.array[k] = ((Int3)r.GetValue(newRow))[k];
					}catch(Exception e){ii.array[k] = (int)(0); Debug.LogException(e);}
					i++;
				}
				
				r.SetValue(obj, ii);
			}else if(r.FieldType == typeof(Int8))
			{
				Int8 ii = new Int8();
				ii.array = new int[8];
				
				for(int k=0;k<8;k++)
				{
					try{
						ii.array[k] = ((Int8)r.GetValue(newRow))[k];
					}catch(Exception e){ii.array[k] = (int)(0); Debug.LogException(e);}
					i++;
				}
				
				r.SetValue(obj, ii);
			}else if(r.FieldType == typeof(Float3))
			{
				Float3 ii = new Float3();
				ii.array = new float[3];
				
				for(int k=0;k<3;k++)
				{
					try{
						ii.array[k] = ((Float3)r.GetValue(newRow))[k];
					}catch(Exception e){ii.array[k] = (float)(0); Debug.LogException(e);}
					i++;
				}
				
				r.SetValue(obj, ii);
			}else if(r.FieldType == typeof(LocalizedString))
			{
				LocalizedString ii = new LocalizedString();
				ii.Strings = new string[16];
				ii = (LocalizedString)r.GetValue(newRow);
//					for(int k=0;k<16;k++)
//					{
//						//if(fields[i].Length>0)
//						//{
//						ii.Strings[k]= ""+fields[i];
//						//}
//						//else
//						//{
//						//	ii.Strings[k]= "null";
//						//}
//						
//						//	Debug.Log(ii.Strings[k]+"   "+fields[i]);		
//						i++;
//					}
				ii.mask = 999;
				i++;
				r.SetValue(obj, ii);
			}
		}
	}

	private void GetTheEditRecord(T obj)
	{
		int i=0;
		foreach (FieldInfo r in typeof(T).GetFields())
		{
			if(r.FieldType == typeof(LocalizedString))
			{
				//Debug.Log("LOcal get "+i+" pos");
				LocalizedString ii = (LocalizedString)r.GetValue(obj);
				for(int k=0;k<16;k++)
				{
				//	Debug.Log(""+ii.Strings[k]);
					fields[i] = ""+ii.Strings[k];
					i++;
				}
				
				fields[i] = ""+ii.mask;
				i++;
				
			}else if(r.FieldType == typeof(Int2))
			{  
				//Debug.Log("Int2 get "+i+" pos");
				Int2 ii = (Int2)r.GetValue(obj);
				for(int k=0;k<2;k++)
				{
					fields[i] = ""+ii.array[k];
					i++;
				}
			}
			else if(r.FieldType == typeof(Int3))
			{
			//	Debug.Log("Int3 get "+i+" pos");
				Int3 ii = (Int3)r.GetValue(obj);
				for(int k=0;k<3;k++)
				{
					fields[i] = ""+ii.array[k];
					i++;
				}
			}else if(r.FieldType == typeof(Int8))
			{
			//	Debug.Log("Int8get "+i+" pos");
				Int8 ii = (Int8)r.GetValue(obj);
				for(int k=0;k<8;k++)
				{
					fields[i] = ""+ii.array[k];
					i++;
				}
			}else if(r.FieldType == typeof(Float3))
			{
			//	Debug.Log("Float3 get "+i+" pos");
				Float3 ii = (Float3)r.GetValue(obj);
				for(int k=0;k<3;k++)
				{
					fields[i] = ""+ii.array[k];
					i++;
				}
			}else if(r.FieldType == typeof(string) || r.FieldType == typeof(uint) || r.FieldType == typeof(int) || r.FieldType == typeof(float) || r.FieldType == typeof(byte))
			{
			//	Debug.Log("Other get "+i+" pos");
				fields[i] = ""+r.GetValue(obj);
				i++;
			}
		}
	}
	
   private void DisplayToolBar(int x)
   {
		toolbarInt = GUI.Toolbar (new Rect(x, 2, 300, 23), toolbarInt, toolBarStrings);
		
		if(toolbarInt == 0)
		{
			for(int i=0;i<toggles.Count;++i)
			{
				if(toggles[i] == true)
				{
   					colection.RemoveAt(i);
					Key.RemoveAt(i);
					toggles.RemoveAt(i);
				}
   			}

			toolbarInt = -1;
		}
		else if(toolbarInt == 2)
		{
			Write();
			toolbarInt = -1;
		}
		else if(toolbarInt == 1)
		{
			if(editMod == -1)
			{
				for(int i=0;i<toggles.Count;i++)
				  if(toggles[i] == true)
				  {
					editMod = i;
					GetTheEditRecord(colection[i]);							
				break;
				  }
			}
						
			if(GUI.Button(new Rect(x+305,2,100,23),"Save Changes") )
			{
				if(editMod != -1)
				{
					toggles[editMod] = false;
					T obj = colection[editMod];
					SetTheEditRecord(ref obj);
				}
							
				editMod = -1;
				toolbarInt = -1;
			}
		}
		else if(toolbarInt == 3)
		{
			if(fields[0].Length == 0)
				fields[0] = "0";
			
			uint id = uint.Parse(fields[0]);
				
			/*if(Key.Contains(id))
			{
				toolbarInt = -1;
				Debug.Log("Record "+id+" duplicate");
			}
			else*/
			//{
				T obj = new T();
				SetTheEditRecord(ref obj);
				colection.Add(obj);
				Key.Add(id);
				toggles.Add(false);
				
				toolbarInt = -1;					
			//}
   		}
	}

	private void _DisplayRecord(int x, int y, T o, int index)
	{
		int height = 20;
		int width = 0;
		int totalSize = 0;
		
		toggles[index] = GUI.Toggle(new Rect(x,y,25,height), toggles[index], "");
		
		totalSize += 25;
		foreach (FieldInfo v in typeof(T).GetFields())
		{
			if (v.FieldType == typeof(string))
			{
	 		GUI.TextField(new Rect(x+totalSize,y,300,height), ""+v.GetValue(o), 200);	
	 		totalSize += 300;
		   	}
		   	else if(v.FieldType == typeof(Int2))
		   	{
		   		Int2 ii = (Int2)v.GetValue(o);
		   		for(int i=0;i<2;i++)
		   		{
	 			GUI.TextField(new Rect(x+totalSize,y,300,height), ""+ii.array[i], 200);	
	 			totalSize += 300;
	 		}	
		   	}
		   	else if(v.FieldType == typeof(Int3))
		   	{
		   		Int3 ii = (Int3)v.GetValue(o);
		   		for(int i=0;i<3;i++)
		   		{
	 			GUI.TextField(new Rect(x+totalSize,y,300,height), ""+ii.array[i], 200);	
	 			totalSize += 300;	
		   		}
		   	}
		   	else if(v.FieldType == typeof(Int8))
		   	{
		   		Int8 ii = (Int8)v.GetValue(o);
		   		for(int i=0;i<8;i++)
		   		{
	 			GUI.TextField(new Rect(x+totalSize,y,300,height), ""+ii.array[i], 200);	
	 			totalSize += 300;
		   		}	
		   	}
		   	else if(v.FieldType == typeof(Float3))
		   	{
		   		Float3 ii = (Float3)v.GetValue(o);
		   		for(int i=0;i<3;i++)
		   		{
	 			GUI.TextField(new Rect(x+totalSize,y,300,height), ""+ii.array[i], 200);	
	 			totalSize += 300;
		   		}	
		   	}
		   	else if(v.FieldType == typeof(LocalizedString))
		   	{
		   		LocalizedString ii = (LocalizedString)v.GetValue(o);
		   		for(int i=0;i<16;i++)
		   		{
	 			GUI.TextField(new Rect(x+totalSize,y,300,height), ""+ii.Strings[i], 200);	
	 			totalSize += 300;
		   		}	
		   	}			   				   				   				   	
		   	else if(v.FieldType == typeof(uint) || v.FieldType == typeof(int) || v.FieldType == typeof(float) || v.FieldType == typeof(byte))
		   	{
		   	 	GUI.TextField(new Rect(x+totalSize,y,200,height), ""+v.GetValue(o), 100);	
		   		totalSize += 200;
		   	}
		}
	}
	
	public void Display(int x, int y, int f, int t)
	{
		int from = f;
		int to = t;
		DisplayToolBar(x*2);
		CheckInterval(ref from, ref to);
  
 		scrollPos = GUI.BeginScrollView(new Rect(x,y,Screen.width*WIDTH ,Screen.height*HEIGTH), scrollPos,new Rect(x,y,x*Fields,	y*(to-from+2)));
  		 int k=1;
			DisplayHeader(x,y*k++);
 		try{
			if(colection.Count>0)
			for(int i = from ;i<=to;i++)
	{
				_DisplayRecord(x,y*(k++),colection[i],i);
			}
 		}catch(Exception e){Debug.Log(e);}
	  	GUI.EndScrollView();

	  	scrollPos2 = GUI.BeginScrollView(new Rect(x,(int)(Screen.height*HEIGTH),Screen.width*WIDTH ,Screen.height*0.2F), scrollPos2,new Rect(x,(int)(Screen.height*HEIGTH),x*Fields,y*(1)));
 	  	 DisplayHeader(x,(int)(Screen.height*HEIGTH*1.08F));
   		 DisplayFooter(x,(int)(Screen.height*HEIGTH*1.04F));	  
 		GUI.EndScrollView();
	}
	
	protected void DisplayHeader(int x, int y)
	{
		int totalSize = 25;
		int height = 20;
	//	if(GUI.Button(new Rect(x,y,27,20),"All"));
		
		T o = new T();
		foreach (FieldInfo v in typeof(T).GetFields())
		{
			if (v.FieldType == typeof(string))
			{
	 		GUI.Button(new Rect(x+totalSize,y,300,height), ""+v.Name);	
	 		totalSize += 300;
		   	}
		   	else if(v.FieldType == typeof(Int2))
		   	{
		   		for(int i=0;i<2;i++)
		   		{
	 			GUI.Button(new Rect(x+totalSize,y,300,height), ""+v.Name+""+i);	
	 			totalSize += 300;
	 		}	
		   	}
		   	else if(v.FieldType == typeof(Int3))
		   	{
		   		for(int i=0;i<3;i++)
		   		{
	 			GUI.Button(new Rect(x+totalSize,y,300,height), ""+v.Name+""+i);	
	 			totalSize += 300;	
		   		}
		   	}
		   	else if(v.FieldType == typeof(Int8))
		   	{
		   		for(int i=0;i<8;i++)
		   		{
	 			GUI.Button(new Rect(x+totalSize,y,300,height), ""+v.Name+""+i);	
	 			totalSize += 300;
		   		}	
		   	}
		   	else if(v.FieldType == typeof(Float3))
		   	{
		   		for(int i=0;i<3;i++)
		   		{
	 			GUI.Button(new Rect(x+totalSize,y,300,height), ""+v.Name+""+i);	
	 			totalSize += 300;
		   		}	
		   	}
		   	else if(v.FieldType == typeof(LocalizedString))
		   	{
		   		for(int i=0;i<16;i++)
		   		{
	 			GUI.Button(new Rect(x+totalSize,y,300,height), ""+v.Name+""+i);	
	 			totalSize += 300;
		   		}	
		   	}			   				   				   				   	
		   	else if(v.FieldType == typeof(uint) || v.FieldType == typeof(int) || v.FieldType == typeof(float) || v.FieldType == typeof(byte))
		   	{
		   	 	GUI.Button(new Rect(x+totalSize,y,200,height), ""+v.Name);	
		   		totalSize += 200;
		   	}	
		}
		
	}
	
	protected void DisplayFooter(int x, int y)
	{
	  int totalSize = +25;
	  int j=0;
	  foreach (FieldInfo v in typeof(T).GetFields())
		{
			if (v.FieldType == typeof(string))
			{
	 		fields[j] = GUI.TextField(new Rect(x+totalSize,y*1.1F,300,20), fields[j],100);
	 		totalSize += 300;
	 		j++;
		   	}
		   	else if(v.FieldType == typeof(Int2))
		   	{
		   		for(int i=0;i<2;i++)
		   		{
	 			fields[j] = GUI.TextField(new Rect(x+totalSize,y*1.1F,300,20), fields[j],100);
	 			totalSize += 300;
	 			j++;
	 		}	
		   	}
		   	else if(v.FieldType == typeof(Int3))
		   	{
		   		for(int i=0;i<3;i++)
		   		{
	 			fields[j] = GUI.TextField(new Rect(x+totalSize,y*1.1F,300,20), fields[j],100);
	 			totalSize += 300;
	 			j++;	
		   		}
		   	}
		   	else if(v.FieldType == typeof(Int8))
		   	{
		   		for(int i=0;i<8;i++)
		   		{
	 			fields[j] = GUI.TextField(new Rect(x+totalSize,y*1.1F,300,20), fields[j],100);	
	 			totalSize += 300;
	 			j++;
		   		}	
		   	}
		   	else if(v.FieldType == typeof(Float3))
		   	{
		   		for(int i=0;i<3;i++)
		   		{
	 			fields[j] = GUI.TextField(new Rect(x+totalSize,y*1.1F,300,20), fields[j],100);	
	 			totalSize += 300;
	 			j++;
		   		}	
		   	}
		   	else if(v.FieldType == typeof(LocalizedString))
		   	{
		   		for(int i=0;i<16;i++)
		   		{
	 			fields[j] = GUI.TextField(new Rect(x+totalSize,y*1.1F,300,20), fields[j],100);	
	 			totalSize += 300;
	 			j++;
		   		}	
		   		j++;
		   	}
   				   				   	
		   	else if(v.FieldType == typeof(uint) || v.FieldType == typeof(int) || v.FieldType == typeof(float) || v.FieldType == typeof(byte))
		   	{
		   	 	fields[j] = GUI.TextField(new Rect(x+totalSize,y*1.1F,200,20), fields[j],100);	
		   		totalSize += 200;
		   		j++;
		   	}	
		   	
		   	
		}			
		
	}
#endif
	public T GetRecordFromIndex(int index)
	{
		if(index > Key.Count-1)
			return new T();
		
		return colection[index];
	}
	
	public T GetRecord(int ID)//return a certain record base on the id
	{
		return GetRecord(ID, true);
	}
	public T GetRecord(uint ID)//return a certain record base on the id
	{
		return GetRecord((int)ID, true);
	}
	
	public T GetRecord(int ID, bool isWOWID)//return a certain record base on the id
	{
		try{
		if(colection.Count == Key.Count)
		{
		//	Debug.Log("Get "+ID);	
			for(int i = 0;i<colection.Count;i++)
				if(Key[i]==ID)
					return colection[i];
		}		
		return new T();		
		}catch(Exception e){ Debug.Log(e); return new T();}
	}
	
	public void AddKey(uint key)
	{
		if(!Key.Contains(key))
		{
			Key.Add (key);
		}
	}

	private byte[] ConcatArrays(byte[] left, byte[] right)
	{
		var result = new byte[left.Length + right.Length];
		left.CopyTo(result, 0);
		right.CopyTo(result, left.Length);
		return result;
	}
}
