using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class UIDragDropAbsoluteLayout : UIAbstractContainer
{	
	float startDragCount; 
	bool goDrag = false;
	bool _drag = false;
	bool m_activate = false;
	UICell dragItem;
	
	public UIDragDropAbsoluteLayout(UIToolkit mixToolkit) : base()
	{
		dragItem = UICell.create(mixToolkit,null);
		dragItem.setSize(UID.CELL_SIZE, UID.CELL_SIZE);
		dragItem.position = new Vector3(1000.0f,-1000.0f,0.0f);
		
		activate = true;
	}
	
	public void Update()
	{
		if(m_activate == false)
			return;
	   //if ( timert < Time.time ) // if it's trigger time
	   // do nothing - > this will cause a trigger and be updated elsewhere (in targetTab.js)
		
	   //Debug.Log("drag ? "+_drag);
		if(goDrag)
		{
			startDragCount -= Time.deltaTime;
			if(startDragCount < 0.0)
			{
				if(Input.GetMouseButton(0))
				{
					_drag = true;
					goDrag = false;
				}
				else
				{
					_drag = false;
					goDrag = false;
				}
			}
		}

		if(_drag && Input.GetMouseButton(0))//works faster here than in fixeupdate
		{
			if(dragItem.Obj._entry==0)
			{
				//dragSpell.Obj = new OBJ();
			}
			dragItem.position = new Vector3((Input.mousePosition.x - UID.CELL_SIZE*0.5f)*1.0f, (-Screen.height + Input.mousePosition.y+UID.CELL_SIZE*0.5f)*1.0f , 0.0f);
		}
		else if(_drag)
		{
			
			Debug.Log("Drop "+Input.mousePosition);
			for(int i = 0; i < _children.Count; i++)
			{
				if(new Rect(_children[i].position.x, _children[i].position.y , _children[i].width, _children[i].height).Contains(Input.mousePosition))
				{
					(_children[i] as UICell).Obj = dragItem.Obj;
					break;
				}
			}
			
			dragItem.position = new Vector3(1000.0f,-1000.0f,0.0f);
		}
	}
	
	public bool activate 
	{
		set { activate = value; }
	}
		
	public void clear()
	{
		_children.Clear();
	}
	
/*	
#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
	public override void onTouchMoved( UIFakeTouch touch, Vector2 touchPos )
#else
	public override void onTouchMoved( Touch touch, Vector2 touchPos )
#endif
	{
		Debug.Log("Moveeeed");
	}*/
}
