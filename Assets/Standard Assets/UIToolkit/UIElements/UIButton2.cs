using UnityEngine;
using System.Collections;

public class UIButton2 : UIButton 
{
	
	public int talentID;
	public int spellID;
	public int rank = -1;
	public short max_rank;
	
	private UIButton2( UIToolkit manager, Rect frame, int depth, UIUVRect uvFrame, UIUVRect highlightedUVframe)
		: base (manager, frame, depth, uvFrame,highlightedUVframe)
	{
		
	}

	public static UIButton2 create( string filename, string highlightedFilename, int xPos, int yPos )
	{
		return UIButton2.create( UIPrime31.firstToolkit, filename, highlightedFilename, xPos, yPos );
	}
	
	
	public static UIButton2 create( UIToolkit manager, string filename, string highlightedFilename, int xPos, int yPos )
	{
		return UIButton2.create( manager, filename, highlightedFilename, xPos, yPos, 1 );
	}

	
	public static UIButton2 create( UIToolkit manager, string filename, string highlightedFilename, int xPos, int yPos, int depth )
	{
		UIToolkit mix1 = manager;
		manager = manager.GetUIToolkitManager(filename, manager);
		if(!manager.hasTextureInfo( filename, manager ))
		{
			manager = mix1;
			filename = "noicon.png";
			highlightedFilename = "noicon.png";
		}
		// grab the texture details for the normal state
		UITextureInfo normalTI = manager.textureInfoForFilename( filename, manager );
		var frame = new Rect( xPos, yPos, normalTI.frame.width, normalTI.frame.height );
		
		// get the highlighted state
		var highlightedTI = manager.textureInfoForFilename( highlightedFilename, manager );
		
		// create the button
		return new UIButton2( manager, frame, depth, normalTI.uvRect, highlightedTI.uvRect );
	}


		
}