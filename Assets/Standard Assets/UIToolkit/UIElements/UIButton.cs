using UnityEngine;
using System;


public class UIButton : UITouchableSprite
{
	public event Action<UIButton> onTouchUpInside;
	public event Action<UIButton> onTouchDown;

	public UIUVRect highlightedUVframe;
	public AudioClip touchDownSound;
	public Vector2 initialTouchPosition;
	public int info = 0;
	public int oInfo = 0;

	#region Constructors/Destructor
	
	public static UIButton create( string filename, string highlightedFilename, int xPos, int yPos )
	{
		return UIButton.create( UIPrime31.firstToolkit, filename, highlightedFilename, xPos, yPos );
	}

	public static UIButton create( UIToolkit manager, string filename, string highlightedFilename, float xPos, float yPos )
	{
		return UIButton.create( manager, filename, highlightedFilename, (int)xPos, (int)yPos, 1);
	}

	public static UIButton create( UIToolkit manager, string filename, string highlightedFilename, int xPos, int yPos )
	{
		return UIButton.create( manager, filename, highlightedFilename, xPos, yPos, 1 );
	}


	public static UIButton create( UIToolkit manager, string filename, string highlightedFilename, float xPos, float yPos, int depth )
	{
		return UIButton.create( manager, filename, highlightedFilename, (int)xPos, (int)yPos, depth );
	}

	public static UIButton create( UIToolkit manager, string filename, string highlightedFilename, int xPos, int yPos, int depth )
	{
		// grab the texture details for the normal state
		//filename = filename.Substring(0, filename.LastIndexOf('.'))+".png";
		//highlightedFilename = highlightedFilename.Substring(0, highlightedFilename.LastIndexOf('.'))+".png";
		UIToolkit mix1 = manager;
		manager = manager.GetUIToolkitManager(filename, manager);
		if(!manager.hasTextureInfo( filename, manager ))
		{
			manager = mix1;
			filename = "noicon.png";
			highlightedFilename = "noicon.png";
		}
		UITextureInfo normalTI = manager.textureInfoForFilename( filename , manager );
		var frame = new Rect( xPos, yPos, normalTI.frame.width, normalTI.frame.height );
		
		// get the highlighted state
		var highlightedTI = manager.textureInfoForFilename( highlightedFilename , manager );
		
		// create the button
		return new UIButton( manager, frame, depth, normalTI.uvRect, highlightedTI.uvRect );
	}

	public static UIButton create( UIToolkit manager, string filename, string highlightedFilename, Rect frame, int depth )
	{
		// grab the texture details for the normal state
//		filename = filename.Substring(0, filename.LastIndexOf('.'))+".png";
//		highlightedFilename = highlightedFilename.Substring(0, highlightedFilename.LastIndexOf('.'))+".png";
		UIToolkit mix1 = manager;
		manager = manager.GetUIToolkitManager(filename, manager);
		if(!manager.hasTextureInfo( filename, manager ))
		{
			manager = mix1;
			filename = "noicon.png";
			highlightedFilename = "noicon.png";
		}
		UITextureInfo normalTI = manager.textureInfoForFilename( filename , manager );
		
		// get the highlighted state
		var highlightedTI = manager.textureInfoForFilename( highlightedFilename , manager );
		
		// create the button
		return new UIButton( manager, frame, depth, normalTI.uvRect, highlightedTI.uvRect );
	}

	public UIButton( UIToolkit manager, Rect frame, int depth, UIUVRect uvFrame, UIUVRect highlightedUVframe ):base( frame, depth, uvFrame )
	{
		// If a highlighted frame has not yet been set use the normalUVframe
		if( highlightedUVframe == UIUVRect.zero )
			highlightedUVframe = uvFrame;
		
		this.highlightedUVframe = highlightedUVframe;
		
		manager.addTouchableSprite( this );
	}

	#endregion;


	// Sets the uvFrame of the original UISprite and resets the _normalUVFrame for reference when highlighting
	public override UIUVRect uvFrame
	{
		get { return _clipped ? _uvFrameClipped : _uvFrame; }
		set
		{
			_uvFrame = value;
			manager.updateUV( this );
		}
	}

	
	public override bool highlighted
	{
		set
		{
			// Only set if it is different than our current value
			if( _highlighted != value )
			{			
				_highlighted = value;
				
				if ( value )
					base.uvFrame = highlightedUVframe;
					
				else
					base.uvFrame = _tempUVframe;
			}
		}
	}


	// Touch handlers
#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
	public override void onTouchBegan( UIFakeTouch touch, Vector2 touchPos )
#else
	public override void onTouchBegan( Touch touch, Vector2 touchPos )
#endif
	{
		//Debug.Log("Button down "+info);
		highlighted = true;	
		initialTouchPosition = touch.position;
		
		if( touchDownSound != null )
			UIPrime31.instance.playSound( touchDownSound );
		
		if( onTouchDown != null )
			onTouchDown( this );
	}

#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
	public override void onTouchEnded( UIFakeTouch touch, Vector2 touchPos, bool touchWasInsideTouchFrame )
#else
	public override void onTouchEnded( Touch touch, Vector2 touchPos, bool touchWasInsideTouchFrame )
#endif
	{
		//Debug.Log("Button up "+this.index);
		highlighted = false;
		// If the touch was inside our touchFrame and we have an action, call it
		if( touchWasInsideTouchFrame && onTouchUpInside != null )
			onTouchUpInside( this );
	}
	
	public override void destroy()
    {
        base.destroy();

        highlighted = false;
    }
}