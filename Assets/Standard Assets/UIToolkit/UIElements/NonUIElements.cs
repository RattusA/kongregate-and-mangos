using UnityEngine;
using System.Collections;

namespace NonUIElements
{
	public class ReviveWindow
	{	
		UIText text = new UIText (UIPrime31.myToolkit1, "fontiny simple", "fontiny simple.png");
		UIButton uiBoxButton = null;
		UIButton uiReleaseButton;
		//UITextInstance uiReleaseText  = null; 	
		public UIButton uiReviveButton;
		//UITextInstance uiReviveText  = null; 
		UITextInstance uiGhostMessageText1 = null;
		UITextInstance uiGhostMessageText2 = null;
		UITextInstance uiCountText = null;
		float menuButtonWidth = Screen.width * 0.30f;
		float menuButtonHeight = Screen.height * 0.30f;
		float reviveWidth = Screen.width * 0.25f;
		float reviveHeight = Screen.height * 0.1f;
		function releaseSpiritDelegate;
		function reclaimCorpseDelegate;
	
		public ReviveWindow (function _Release, function _Reclaim)
		{
			releaseSpiritDelegate = new function (_Release);
			reclaimCorpseDelegate = new function (_Reclaim);
		
			uiReleaseButton = UIButton.create (UIPrime31.questMix, "unbind.png", "unbind.png", 0, 0);
			uiReleaseButton.centerize ();
			uiReleaseButton.setSize (reviveWidth, reviveHeight);
			uiReleaseButton.position = new Vector3 (Screen.width * 0.50f, -Screen.height * 0.45f, 0);
			uiReleaseButton.onTouchDown += ReleaseSpirit;
			//uiReleaseText = text.addTextInstance("Unbind", uiReleaseButton.position.x,-uiReleaseButton.position.y,0.5f * dbs._sizeMod,-1,Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);
		
			uiReviveButton = UIButton.create (UIPrime31.questMix, "revive.PNG", "revive.PNG", 0, 5);
			uiReviveButton.centerize ();
			uiReviveButton.setSize (reviveWidth, reviveHeight);
			uiReviveButton.position = new Vector3 (Screen.width * 0.50f, -Screen.height * 0.45f, 5);
			uiReviveButton.onTouchDown += ReclaimCorpse;
			//uiReviveText = text.addTextInstance("Revive", uiReleaseButton.position.x,-uiReleaseButton.position.y,0.5f * dbs._sizeMod,-1,Color.white,UITextAlignMode.Center,UITextVerticalAlignMode.Middle);

			uiGhostMessageText1 = text.addTextInstance ("You Are Dead!", Screen.width * 0.5f, Screen.height * 0.33f, 0.8f * dbs._sizeMod, -1, Color.red, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
			uiGhostMessageText2 = text.addTextInstance ("Find your body or use Revive Altar", Screen.width * 0.5f, (Screen.height * 0.4f), 0.6f * dbs._sizeMod, -1, Color.red, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
		
			uiCountText = text.addTextInstance (" ", Screen.width * 0.5f, (Screen.height * 0.24f), 0.9f * dbs._sizeMod, -1, Color.red, UITextAlignMode.Center, UITextVerticalAlignMode.Middle);
			reset ();
		}
	
		public string updateTimer {
			set {
				uiCountText.clear ();
				uiCountText.text = "" + value;
			}
		}
	
		public void reset ()
		{
			_hidden = false;
		
			hideRevive = true;
			hideRelease = true;
			hideGhost = true;
		
			uiCountText.clear ();
			uiCountText.text = " ";		
			uiCountText.hidden = true;
		
		}
	
		bool reviveState = true;
		bool releaseState = true;
		bool ghostState = true;
		bool _hidden = false;

		public bool hidden {
			set {	
				if (_hidden == value)
					return;
				
				if (value == true) {
					//save current state

					{
						releaseState = hideRelease;
						reviveState = hideRevive;
						ghostState = hideGhost;
					
						//Debug.Log("Save "+releaseState + reviveState + ghostState);
					}
					//hide all
					uiReleaseButton.hidden = value;
					//uiReleaseText.hidden = value;
					uiGhostMessageText1.hidden = value;
					uiGhostMessageText2.hidden = value;
					uiReviveButton.hidden = value;
					//uiReviveText.hidden = value;
				} else {
					//restore old states
					hideRelease = releaseState;
					hideRevive = reviveState;
					hideGhost = ghostState;
					//	_hidden = value;
					//Debug.Log("Restore"+releaseState + reviveState + ghostState);
				}
			
				uiCountText.hidden = value;			
				_hidden = value;
				//	Debug.Log(""+releaseState + reviveState + ghostState);
			}
		
			get { return _hidden; }
		}
	
		public bool hideRelease {
			set {
				if (value == false) {
					hideGhost = true;
					hideRevive = true;
				}
			
				uiReleaseButton.hidden = value;
				//uiReleaseText.hidden = value;
				releaseState = value;
	
			}
		
			get {
				return uiReleaseButton.hidden;
			}
		}
	
		public bool hideGhost {
			set {
				if (value == false) {
					hideRelease = true;
					hideRevive = true;
				}
						
				uiGhostMessageText1.hidden = value;
				uiGhostMessageText2.hidden = value;
				ghostState = value;
			}
		
			get {
				return uiGhostMessageText1.hidden;
			}
		}
	
		public bool hideRevive {
			set {
				if (value == false) {
					hideRelease = true;
					hideGhost = true;
				}
				uiReviveButton.hidden = value;
				//uiReviveText.hidden = value;
				reviveState = value;

			}
		
			get {
				return uiReviveButton.hidden;
			}
		}
	
		public void ReleaseSpirit (UIButton sender)
		{
			releaseSpiritDelegate ();
		}
	
		public void ReclaimCorpse (UIButton sender)
		{
			reclaimCorpseDelegate ();
		}
	
		public delegate void function ();
	}
	
	public class DialogBox // currently used for destroy dialog, not generalised
	{
		UIButton m_panel = null;
		UIButton m_uiYes = null;
		UIButton m_uiNo = null;
		UIText m_uiText;
		UITextInstance yesText = null;
		UITextInstance noText = null;
		UITextInstance askText = null;
		Vector3 m_position;
		Vector2 m_size;
		function m_yes;
		function m_no;
	
		public DialogBox (string message, Vector3 position, Vector2 size, function yes, function no) //constructor called in Button.js
		{
			m_position = position;
			m_size = size;
			m_yes = new function (yes);
			m_no = new function (no);
			
			m_uiText = new UIText (UIPrime31.myToolkit1, "prototype", "prototype.png");
			m_uiText.alignMode = UITextAlignMode.Center;	
			
			m_panel = UIButton.create (UIPrime31.interfMix, "toggle_inactive_normal.png", "toggle_inactive_normal.png", 0, 0);
			m_panel.position = m_position;
			m_panel.setSize (m_size.x, m_size.y);
			m_panel.color = new Color (0.7f, 0.2f, 0.1f, 0.99f);
			m_panel.hidden = true;
			
			askText = m_uiText.addTextInstance ("DESTR0Y item ?!", m_panel.position.x + m_size.x * 0.16f, -m_panel.position.y + m_size.y * 0.32f, UID.TEXT_SIZE * 2.0f, (int)m_panel.position.z - 1, Color.yellow, UITextAlignMode.Left, UITextVerticalAlignMode.Middle);
			askText.hidden = true;	
			
			m_uiYes = UIButton.create (UIPrime31.interfMix, "toggle_inactive_normal.png", "toggle_inactive_normal.png", 0, 0);
			m_uiYes.centerize ();
			m_uiYes.position = new Vector3 (m_position.x + m_size.x * 0.28f, m_position.y - m_size.y * 0.68f, m_position.z - 1.0f);
			m_uiYes.setSize (m_size.x * 0.3f, m_size.y * 0.3f);
			m_uiYes.onTouchDown += yesDestroy;
			m_uiYes.hidden = true;
			
			yesText = m_uiText.addTextInstance ("Yes", m_uiYes.position.x - m_size.x * 0.08f, -m_uiYes.position.y + m_size.y * 0.05f, UID.TEXT_SIZE * 2.0f, (int)m_uiYes.position.z - 1, Color.black, UITextAlignMode.Left, UITextVerticalAlignMode.Middle);
			yesText.hidden = true;	
			
			m_uiNo = UIButton.create (UIPrime31.interfMix, "toggle_inactive_normal.png", "toggle_inactive_normal.png", 0, 0);
			m_uiNo.centerize ();
			m_uiNo.position = new Vector3 (m_position.x + m_size.x * 0.72f, m_position.y - m_size.y * 0.68f, m_position.z - 1.0f);
			m_uiNo.setSize (m_size.x * 0.3f, m_size.y * 0.3f);
			m_uiNo.onTouchDown += noDestroy;
			m_uiNo.hidden = true;
			
			noText = m_uiText.addTextInstance ("No", m_uiNo.position.x - m_size.x * 0.06f, -m_uiNo.position.y + m_size.y * 0.05f, UID.TEXT_SIZE * 2.0f, (int)m_uiNo.position.z - 1, Color.black, UITextAlignMode.Left, UITextVerticalAlignMode.Middle);
			noText.hidden = true;
			
			
		}

		public bool hidden { // used to "display" or "hide"/deactivate the dialog, used in MainPlayer.js and Button.js
		//get{ return m_hidden; }
			set {
				m_panel.hidden = value;
				m_uiYes.hidden = value;
				m_uiNo.hidden = value;
			
				
				
				yesText.hidden = value;
				noText.hidden = value;
				askText.hidden = value;
			}

		}
		
		public void yesDestroy (UIButton sender)
		{
			m_yes ();
		}
	
		public void noDestroy (UIButton sender)
		{
			m_no ();
		}
						
		public delegate void function ();	
	}

	public class TextBox
	{	
		public bool doTextSplitting;
		UIText m_uiText;
		Vector3 m_position;
		Vector2 m_size;
		uint m_lines;
		string m_text = "";
		public float m_textSize;
		Color m_color;
		string font;
		public uint linesNumber = 0;
		public UITextInstance m_textInstances = null;//new UITextInstance();
		
		public TextBox (string _text, Vector3 position, Vector2 _size, uint lines, Color color, float textSize)
		{
			m_uiText = new UIText (UIPrime31.myToolkit1, "prototype", "prototype.png");
			doTextSplitting = true;
			text = _text;
			m_position = position;
			m_size = _size;
			m_lines = lines;
			m_textSize = textSize;
			m_color = color;
		
		
			m_uiText.alignMode = UITextAlignMode.Center;	
		}
		//-Sebek 100%
		public TextBox (string _text, Vector3 position, Vector2 _size, uint lines, Color color, float textSize, string font)
		{
			m_uiText = new UIText (UIPrime31.myToolkit1, font, font + ".png");
			doTextSplitting = true;
			text = _text;
			m_position = position;
			m_size = _size;
			m_lines = lines;
			m_textSize = textSize;
			m_color = color;
		
		
			m_uiText.alignMode = UITextAlignMode.Center;
		}
	
		public Vector3 position {
			get{ return m_position;	}
			set {
				m_position = value;
				//m_uiText.manager.
				//m_uiText.updateText(m_uiText);
				text = m_text;
			}
		}
		
		public Vector2 size {
			get { return m_size; }
			set { 
				text = m_text;
				m_size = value; 
			}
		}
	
		public string text 
		{
			get
			{
				return m_text;
			}
		
			set
			{
				m_text = value;

				float letterWidth = m_uiText.GetLetterWitdh();
				
				string[] _tempText = m_text.Split("\n" [0]);
				if (_tempText.Length > 1 && doTextSplitting)
				{
					m_text = "";
					for (int i = 0; i < _tempText.Length; i++)
					{
						string[] _lines = _tempText[i].Split(" " [0]);
						_tempText[i] = _lines[0];
						
						float _width = m_size.x;
						float _calculatedWidth = _lines[0].Length * m_textSize * letterWidth;
					
						for (int _i = 1; _i < _lines.Length; _i ++)
						{
							_calculatedWidth += (_lines[_i].Length + 1) * m_textSize * letterWidth;
							if (_calculatedWidth > _width)
							{
								_tempText[i] += "\n" + _lines[_i];
								_calculatedWidth = (_lines[_i].Length + 1) * m_textSize *letterWidth;
							}
							else
							{
								_tempText[i] += " " + _lines[_i];
							}
						}
						
						m_text += _tempText[i] + "\n";
					}
				}
				
				
				string[] _textLines = m_text.Split("\n" [0]);
				while (linesNumber<_textLines.Length)
				{
					linesNumber++;
				}
				clear();
				m_textInstances = m_uiText.addTextInstance (m_text, m_position.x, m_position.y, m_textSize, -1, m_color, UITextAlignMode.Left, UITextVerticalAlignMode.Top);
				m_textInstances.hidden = false;
			}
		}
	
		public void clear ()
		{
		
			if (m_textInstances != null) {
			
				m_textInstances.clear ();
			}
		}
	
		public void destroy ()
		{
			clear ();
			m_uiText = null;
		}

	}//end Textbox


	public class UIToolbar
	{
		UIButton[] m_tabs;
		int m_selectedTabNew = -2;
		int m_selectedTabCurrent = -2;
		Vector3 m_position;
		function m_function;
		bool m_hidden = true;
		int m_nrTabs;
		public System.Collections.Generic.List<UIButton> m_currentVisibleList = new System.Collections.Generic.List<UIButton> ();
		public System.Collections.Generic.List<UIAbstractContainer> m_currentVisibleContainer = new System.Collections.Generic.List<UIAbstractContainer> ();
		public System.Collections.Generic.List<UITextInstance> m_currentVisibleText = new System.Collections.Generic.List<UITextInstance> ();
		UIToolkit m_UIBtn ;
		UIToolkit m_UITxt ;
	
		UIToolbar (UIToolkit UIBtn, UIToolkit UITxt, Vector3 position, int nr, Vector2  tabSize, function _function, System.Collections.Generic.List<int> flagList)
		{
			m_UIBtn = UIBtn;
			m_UITxt = UITxt;
		
			m_function = new function (_function);
			m_position = position;
			m_nrTabs = nr;
			m_tabs = new UIButton[m_nrTabs];

			for (int i=0; i<0; i++) {
				m_tabs [i] = UIButton.create (UIPrime31.interfMix, "toggle_inactive_normal.png", "toggle_inactive_normal.png", 0, 0);
				m_tabs [i].setSize (tabSize.x, tabSize.y);
				m_tabs [i].position = new Vector3 (m_position.x + tabSize.x * i, m_position.y + 0, m_position.z + 0);
				m_tabs [i].hidden = true;
				m_tabs [i].info = flagList [i];
				m_tabs [i].onTouchUpInside += Click;
			}
			m_hidden = true;
		}
	
		public static UIToolbar create (UIToolkit UIBtn, UIToolkit UITxt, Vector3 position, int nr, Vector2  tabSize, function _function, System.Collections.Generic.List<int> flagList)
		{
			return new UIToolbar (UIBtn, UITxt, position, nr, tabSize, _function, flagList);
		}

		private void Click (UIButton sender)
		{
			m_selectedTabNew = sender.info;
			if (m_selectedTabNew != m_selectedTabCurrent)
			{
				clear ();
				m_selectedTabCurrent = m_selectedTabNew;
				m_function (m_selectedTabNew);
			}
		}
	
		private void clear ()
		{
			UIAbstractContainer aux;
			for (int i = m_currentVisibleContainer.Count - 1; i >= 0; i++)
			{
				aux = m_currentVisibleContainer [i];
				aux.removeAllChild (true);
				(aux as UIScrollableVerticalLayout).setSize (0, 0);
				//GameObject.Destroy(aux.client);//not sure if this is secure
			}
			m_currentVisibleContainer.Clear ();
			
			UIButton btn;				
			for (int i = m_currentVisibleList.Count - 1 ; i >= 0; i--)
			{
				btn = m_currentVisibleList [i];
				btn.destroy ();
				m_currentVisibleList.RemoveAt (i);
			}
		
			UITextInstance txt;
			for (int i = m_currentVisibleText.Count - 1; i >= 0; i++)
			{
				txt = m_currentVisibleText [i];
				txt.clear ();
				m_currentVisibleText.RemoveAt (i);
			}
		}
	
		public void destroy ()
		{
			clear ();
		
			for (int j = 0; j<0; j++)
			{
				//m_tabs[j].destroy();
				//m_tabs[j] = null;
			}
		
			m_UIBtn = null;
			m_UITxt = null;
			m_function = null;
		}
	
		public bool hidden
		{
			get { return m_hidden; }
			set {
				for (int i = 0; i<m_nrTabs; i++)
				{
					m_tabs [i].hidden = value;
				}
				m_hidden = value;
			}
		}
//	
		public delegate void function (int tab);	
	}//Toolbar


}//end namespace