using UnityEngine;
using System.Collections;

public class UIButtonText : UIButton
{
	
	public string text;
	private UIText uiText = null ;
	private UITextInstance uiTextInstance = null;
	
	private UIButtonText( UIToolkit manager, Rect frame, int depth, UIUVRect uvFrame, UIUVRect highlightedUVframe, string str)
		: base (manager, frame, depth, uvFrame,highlightedUVframe)
	{
		text = str;
		uiText = new UIText( UIPrime31.myToolkit1, "prototype", "prototype.png" );
		uiText.alignMode = UITextAlignMode.Left;
		
		uiTextInstance = uiText.addTextInstance(""+text ,position.x*0.1f ,height*0.6f , 0.5f * dbs._sizeMod,-1,Color.red, UITextAlignMode.Left,UITextVerticalAlignMode.Middle);
		uiTextInstance.hidden = false;
	}

	public static UIButtonText create( UIToolkit manager, string filename, string highlightedFilename, string str)
	{
		string[] btnTextures = {"fundal.png", "fundal.png"}; 

		// grab the texture details for the normal state
		var normalTI = manager.textureInfoForFilename(btnTextures[0], manager);
		var frame = new Rect( 0, 0, normalTI.frame.width, normalTI.frame.height );
		
		// get the highlighted state
		var highlightedTI = manager.textureInfoForFilename(btnTextures[0], manager);
		
		// create the button
		return new UIButtonText( manager, frame, 0, normalTI.uvRect, highlightedTI.uvRect, str);
	}
	
	public void SetSize( float width, float height )
    {
    	setSize(width, height);
    	
    	uiTextInstance = uiText.addTextInstance(""+text ,position.x*0.1f ,height*0.6f * dbs._sizeMod, 0.5f,-1,Color.red, UITextAlignMode.Left,UITextVerticalAlignMode.Middle);

    }
	
	// override hidden so we can set the touch frame to dirty when being shown
	public override bool hidden
    {
        get { return ___hidden; }
        set
        {
            base.hidden = value;
			
			if( value )
				touchFrameIsDirty = true;
				
			uiTextInstance.hidden = value;
        }
    }
    
    // Removes the sprite from the mesh and destroys it's client GO
	public override void destroy()
	{
		base.destroy();
		uiTextInstance.clear();
		uiText = null;
		uiTextInstance = null;
	}
	
// Touch handlers
#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
	public override void onTouchBegan( UIFakeTouch touch, Vector2 touchPos )
#else
	public override void onTouchBegan( Touch touch, Vector2 touchPos )
#endif
	{
		highlighted = true;
			
		base.onTouchBegan(touch, touchPos);
	}


#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
	public override void onTouchEnded( UIFakeTouch touch, Vector2 touchPos, bool touchWasInsideTouchFrame )
#else
	public override void onTouchEnded( Touch touch, Vector2 touchPos, bool touchWasInsideTouchFrame )
#endif
	{

		highlighted = false;
		// If the touch was inside our touchFrame and we have an action, call it
		base.onTouchEnded( touch,  touchPos,  touchWasInsideTouchFrame );
	}


}
