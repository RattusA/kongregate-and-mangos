//////////////////////////////////////////////////
//
//	Emil
//
/////////////////////////////////////////////////
using UnityEngine;
using System;

public class OBJ
{
		public enum CELLTYPE
	{
	  CELLTYPE_EMPTY   		= 0,
  	  CELLTYPE_SPELL		= 1,
  	  CELLTYPE_ITEM			= 2,
  	  CELLTYPE_MOUNT		= 3
	};
	
	public CELLTYPE _type;
	public uint _entry;
	
	public OBJ(CELLTYPE _type , uint entry)
	{
		this._type = _type;
		this._entry = entry;
	}
}

public class UICell : UIButton
{
	public event Action<UICell> onTouchDown;
	public event Action<UICell> onTouchUp;

	private OBJ m_obj;
	public OBJ Obj
	{
		set 
		{
			if(value==null) 
				m_obj = new OBJ(0,0); 
			m_obj = value;	
			//setObj();
		}
		get
		{
			if(m_obj != null) 
				return m_obj; 
			return new OBJ(0,0); 
		}
	}
	
	private  string[] m_textures  = new string[2];
	public string[] textures
	{
		get{ return m_textures; }
	}
	
		
	private UICell( UIToolkit manager, Rect frame, int depth, UIUVRect uvFrame, UIUVRect highlightedUVframe, OBJ obj)
		: base (manager, frame, depth, uvFrame, highlightedUVframe)
	{
		if(obj == null)
			Obj = new OBJ(0,0);
		else
			Obj = obj;
	}
	

	public static string[] getTextureName(OBJ obj)
	{
		string[] textures = new string[2];
		
		if(obj == null || obj._entry  == 0)
		{
			textures[0] = textures[1] = "fundal.png";
			//return textures;
		}
		
		OBJ.CELLTYPE _type = obj._type;
//		Debug.Log("obj type: " + _type);
		switch(_type)
		{
			case OBJ.CELLTYPE.CELLTYPE_ITEM:
						ItemEntry itm  = dbs.sItems.GetRecord(obj._entry);
						ItemDisplayInfoEntry display = dbs.sItemDisplayInfo.GetRecord(itm.DisplayID);					
						textures[0] = ""+display.InventoryIcon;
					break;
					
			case OBJ.CELLTYPE.CELLTYPE_SPELL:
						SpellEntry sp  = dbs.sSpells.GetRecord(obj._entry);
						SpellIconEntry icon   = dbs.sSpellIcons.GetRecord(sp.SpellIconID);
					//	Debug.Log("Set Spell Icon " + obj._entry+"  "+sp.ID+"  "+icon.Icon);
						textures[0] = ""+icon.Icon;
						
						if(textures[0] == "")
							textures[0] = "noicon.png";
								
					break;
			
			case OBJ.CELLTYPE.CELLTYPE_MOUNT:
						SpellEntry spp  = dbs.sSpells.GetRecord(obj._entry);
						SpellIconEntry iconn   = dbs.sSpellIcons.GetRecord(spp.SpellIconID);
						Debug.Log("Set mount Icon " + obj._entry+"  "+spp.ID+"  "+iconn.Icon);
						textures[0] = ""+iconn.Icon;
						
						if(textures[0] == "")
							textures[0] = "noicon.png";
								
					break;
			
			case OBJ.CELLTYPE.CELLTYPE_EMPTY:
						textures[0] = "fundal.png";
					break;
					
			//default: textures[0] = "fundal.png";
		};
		
		if(textures[0].Length==0)
		{
			textures[0] = textures[1] = "fundal.png";
			return textures;
		}
		
		return textures;
	}
	
	private void setObj()
	{	
		string tex = ""+getTextureName(m_obj)[0];
		
		//Debug.Log("Modify Cell "+m_obj._type+"  "+m_obj._entry+"  "+tex + "  "+getTextureName(m_obj)[1]);
       
		// grab the texture details for the normal state
		UIToolkit mix1 = manager;
		manager = manager.GetUIToolkitManager(tex, manager);
		if(!manager.hasTextureInfo( tex, manager ))
		{
			manager = mix1;
			tex = "noicon.png";
		}
		var normalTI = manager.textureInfoForFilename(tex, manager);
		var frame = new Rect( 0, 0, normalTI.frame.width, normalTI.frame.height );
		// get the highlighted state
		var highlightedTI = normalTI;//manager.textureInfoForFilename(tex);	
		
	//	Debug.Log("Modify Cell "+m_obj._type+"  "+m_obj._entry+"  "+tex);
		//base 4
		client.transform.position = new Vector3( frame.x, -frame.y, 1 ); // Depth will affect z-index
		// Save these for later.  The manager will call initializeSize() when the UV's get setup
		_width = frame.width;
		_height = frame.height;
		
		uvFrame = normalTI.uvRect;
		
		//base2
		_tempUVframe = uvFrame;
		
		//base1
		if( highlightedUVframe == UIUVRect.zero )
			highlightedUVframe = normalTI.uvRect;
		
		this.highlightedUVframe = highlightedTI.uvRect;
		
	}
	
	public static UICell create( UIToolkit manager, OBJ obj)
	{
		string[] btnTextures = {"fundal.png", "fundal.png"}; 
		
		if(obj != null)
		{
			btnTextures = getTextureName(obj);		
		}
		
		// grab the texture details for the normal state
		UIToolkit mix1 = manager;
		manager = manager.GetUIToolkitManager(btnTextures[0], manager);
		if(!manager.hasTextureInfo( btnTextures[0], manager ))
		{
			manager = mix1;
		}
		var normalTI = manager.textureInfoForFilename(btnTextures[0], manager);
		var frame = new Rect( 0, 0, normalTI.frame.width, normalTI.frame.height );
		
		// get the highlighted state
		var highlightedTI = normalTI;//manager.textureInfoForFilename(btnTextures[0]);
		
		// create the button
		return new UICell( manager, frame, 0, normalTI.uvRect, highlightedTI.uvRect, obj);
	}
	
// Touch handlers
#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
	public override void onTouchBegan( UIFakeTouch touch, Vector2 touchPos )
#else
	public override void onTouchBegan( Touch touch, Vector2 touchPos )
#endif
	{
		highlighted = true;	
		initialTouchPosition = touch.position;
		
		if( touchDownSound != null )
			UIPrime31.instance.playSound( touchDownSound );
		
		if( onTouchDown != null )
			onTouchDown( this );
	}


#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_WEBPLAYER
	public override void onTouchEnded( UIFakeTouch touch, Vector2 touchPos, bool touchWasInsideTouchFrame )
#else
	public override void onTouchEnded( Touch touch, Vector2 touchPos, bool touchWasInsideTouchFrame )
#endif
	{

		highlighted = false;
		// If the touch was inside our touchFrame and we have an action, call it
		if(  onTouchUp != null )
			onTouchUp( this );
	}
	
	public static UICell create( UIToolkit manager, OBJ obj, string cellTexture)
	{
		string[] btnTextures = {cellTexture, cellTexture}; 
		
		// grab the texture details for the normal state
		UIToolkit mix1 = manager;
		manager = manager.GetUIToolkitManager(btnTextures[0], manager);
		if(!manager.hasTextureInfo( btnTextures[0], manager ))
		{
			manager = mix1;
		}
		var normalTI = manager.textureInfoForFilename(btnTextures[0], manager);
		var frame = new Rect( 0, 0, normalTI.frame.width, normalTI.frame.height );
		
		// get the highlighted state
		var highlightedTI = normalTI;//manager.textureInfoForFilename(btnTextures[0]);
		
		// create the button
		UICell newCell = new UICell( manager, frame, 0, normalTI.uvRect, highlightedTI.uvRect, obj);
		newCell.setObj(cellTexture);
		
		return newCell;
	}
	
	private void setObj(string cellTexture)
	{	
		string tex = cellTexture;
		
		//Debug.Log("Modify Cell "+m_obj._type+"  "+m_obj._entry+"  "+tex + "  "+getTextureName(m_obj)[1]);
       
		// grab the texture details for the normal state
		var normalTI = manager.textureInfoForFilename(tex, manager);
		var frame = new Rect( 0, 0, normalTI.frame.width, normalTI.frame.height );
		// get the highlighted state
		var highlightedTI = normalTI;//manager.textureInfoForFilename(tex);	
		
	//	Debug.Log("Modify Cell "+m_obj._type+"  "+m_obj._entry+"  "+tex);
		//base 4
		client.transform.position = new Vector3( frame.x, -frame.y, 1 ); // Depth will affect z-index
		// Save these for later.  The manager will call initializeSize() when the UV's get setup
		_width = frame.width;
		_height = frame.height;
		
		uvFrame = normalTI.uvRect;
		
		//base2
		_tempUVframe = uvFrame;
		
		//base1
		if( highlightedUVframe == UIUVRect.zero )
			highlightedUVframe = normalTI.uvRect;
		
		this.highlightedUVframe = highlightedTI.uvRect;
		
	}
}
